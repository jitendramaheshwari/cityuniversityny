package com.cuny.edu.ad;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Hashtable;
import java.util.HashMap; 
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Enumeration;

import Thor.API.Exceptions.*;
import com.thortech.xl.util.config.ConfigurationClient; 
import Thor.API.Operations.tcUserOperationsIntf; 
import Thor.API.Operations.tcReconciliationOperationsIntf;
import Thor.API.Operations.tcFormInstanceOperationsIntf;
import Thor.API.Operations.tcGroupOperationsIntf;
import Thor.API.Operations.tcITResourceInstanceOperationsIntf;
import Thor.API.Operations.tcLookupOperationsIntf; 
import Thor.API.Operations.tcObjectOperationsIntf;
import Thor.API.Operations.tcOrganizationOperationsIntf; 
import Thor.API.Operations.tcProvisioningOperationsIntf; 
import Thor.API.Base.tcUtilityOperationsIntf;
import Thor.API.Exceptions.tcAPIException;
import Thor.API.tcResultSet;
import Thor.API.tcUtilityFactory;
import com.thortech.util.logging.Logger;
import com.thortech.xl.crypto.tcCryptoException;
import com.thortech.xl.dataaccess.*;
//import com.thortech.xl.util.adapters.tcUtilXellerateOperations;



//import com.thortech.xl.scheduler.tasks.SchedulerBaseTask;


public class CunyProvADClient { 


	String fileName;
	String password;
	List<String> ls=new ArrayList<String>();
	protected tcUtilityFactory ioUtilityFactory;
	protected tcReconciliationOperationsIntf reconIntf;
	protected tcUserOperationsIntf userIntf;
	protected tcFormInstanceOperationsIntf formIntf; 
	protected tcGroupOperationsIntf groupIntf; 
	protected tcITResourceInstanceOperationsIntf itresourceIntf;
	protected tcLookupOperationsIntf lookupIntf; 
	protected tcObjectOperationsIntf objectIntf;
	protected tcOrganizationOperationsIntf orgIntf; 
	protected tcProvisioningOperationsIntf provIntf; 
	protected tcUtilityOperationsIntf tcUtilIntf;

	private static Logger log = Logger.getLogger("CUNY.CUSTOM");



	public static void main(String[] args) { 
		CunyProvADClient cpf = new CunyProvADClient();
		cpf.execute();

	} 

	public void execute() {

		try {

			log.debug("***CunyProvADClient.execute() Entered ..");
			log.debug("Startup..."); 

			log.debug("Getting configuration..."); 
			ConfigurationClient.ComplexSetting config = 
				ConfigurationClient.getComplexSettingByPath("Discovery.CoreServer"); 
			log.debug("Login...");

			Hashtable  env = config.getAllSettings(); 
			log.debug(System.getProperty("login.username"));
			log.debug(System.getProperty("login.password"));
			Enumeration en = env.keys();
			while (en.hasMoreElements()) {
				String keyer = (String)en.nextElement();
				log.debug(keyer + " : " + env.get(keyer));
			}

			ioUtilityFactory = new tcUtilityFactory(env,System.getProperty("login.username"),System.getProperty("login.password"));
			log.debug("Login...finished");  
			log.debug("Getting utility interfaces..."); 

			userIntf = (tcUserOperationsIntf)ioUtilityFactory.getUtility("Thor.API.Operations.tcUserOperationsIntf"); 
			objectIntf = (tcObjectOperationsIntf)ioUtilityFactory.getUtility("Thor.API.Operations.tcObjectOperationsIntf");
			formIntf = (tcFormInstanceOperationsIntf)ioUtilityFactory.getUtility("Thor.API.Operations.tcFormInstanceOperationsIntf");
			log.debug("Executing AD provisioning task from a file");

			fileName = getAttribute("FileName");
			if (getTaskAttributes()) {
				readFile();
			}
			log.debug("Exiting main task");
		} catch(Exception e){
			e.printStackTrace();
		}
		Iterator it=ls.iterator();

		while(it.hasNext())
		{
			String useridvalue=(String)it.next();
			log.debug("Value :"+useridvalue);
			try {
				process(useridvalue);
			} catch(Exception e){
				e.printStackTrace();
			} finally {
				try {
					//ioUtilityFactory.close(orgIntf);
					//ioUtilityFactory.close(lookupIntf);
					ioUtilityFactory.close(objectIntf);
					ioUtilityFactory.close(formIntf);
					ioUtilityFactory.close(userIntf);
					ioUtilityFactory.close();
				} catch(Exception e) {
					log.debug("An exception has occurred when closing connections : " + e.getMessage());
					e.printStackTrace();
				}
			}
		}

		System.exit(0);
	}


	public void readFile() throws Exception {
		log.debug("Reading File " + fileName);
		Hashtable userHash = new Hashtable();

		File f = new File (fileName);
		FileReader fr = new FileReader(f);	
		BufferedReader in = new BufferedReader(new FileReader(f));
		String str;
		int counter = 1;

		while ((str = in.readLine()) != null) {
			log.debug("String from file : " + " at [" + counter + "] : " + str);
			ls.add(str);
			counter++;
		}		
		in.close();
		fr.close();
	}

	public boolean getTaskAttributes() throws Exception { 

		fileName = getAttribute("FileName");
		if (fileName.trim().length()==0) {
			log.debug("You must supply a value for all fields(example,FileName).");
			return false;
		}
		log.debug("getTaskAttributes() Parameter Variables passed are:" + fileName);
		return true;
	}


	public String getAttribute(String arg) {

		return "a.txt";

	}

	public boolean setCampus(long userKey) throws tcAPIException, tcCryptoException, tcUserAccountDisabledException, tcPasswordResetAttemptsExceededException, tcLoginAttemptsExceededException, tcUserAccountInvalidException, tcUserAlreadyLoggedInException, tcNotAtomicProcessException, tcFormNotFoundException, tcProcessNotFoundException, tcInvalidValueException, tcRequiredDataMissingException {


		log.debug((new StringBuilder("setCampus() Parameter Variables passed are:userKey=[")).append(userKey).append("]").toString());
		boolean response = false;


		String[] EMPCAMPUSES = { "USR_UDF_BMC01_EMP", "USR_UDF_MEC01_EMP", "USR_UDF_NYT01_EMP", "USR_UDF_HTR01_EMP", "USR_UDF_QNS01_EMP", "USR_UDF_LEH01_EMP", "USR_UDF_SPS01_EMP", "USR_UDF_SOJ01_EMP", "USR_UDF_COSEN_EMP", "USR_UDF_GRD01_EMP", "USR_UDF_YRK01_EMP", "USR_UDF_KCC01_EMP", "USR_UDF_HOS01_EMP", "USR_UDF_BAR01_EMP", "USR_UDF_BKL01_EMP", "USR_UDF_HCS01_EMP", "USR_UDF_CTY01_EMP", "USR_UDF_LAW01_EMP", "USR_UDF_LAG01_EMP", "USR_UDF_QCC01_EMP", "USR_UDF_CSI01_EMP", "USR_UDF_BCC01_EMP", "USR_UDF_JJC01_EMP", "USR_UDF_GUTTMANCCEMP", "USR_UDF_FACULTY", "USR_UDF_GLBL_EMP_STATUS", "Organizations.Organization Name" };

		String[] STUCAMPUSES = { "USR_UDF_BMC01_STU", "USR_UDF_MEC01_STU", "USR_UDF_NYT01_STU", "USR_UDF_HTR01_STU", "USR_UDF_QNS01_STU", "USR_UDF_LEH01_STU", "USR_UDF_SPS01_STU", "USR_UDF_SOJ01_STU",  "USR_UDF_GRD01_STU", "USR_UDF_YORKCOLLEGESTU", "USR_UDF_KCC01_STU", "USR_UDF_HOS01_STU", "USR_UDF_BAR01_STU", "USR_UDF_BKL01_STU", "USR_UDF_HCS01_STU", "USR_UDF_CTY01_STU", "USR_UDF_LAW01_STU", "USR_UDF_LAG01_STU", "USR_UDF_QCC01_STU", "USR_UDF_CSI01_STU", "USR_UDF_BCC01_STU", "USR_UDF_JJC01_STU",  "USR_UDF_GUTTMANCCSTU", "USR_UDF_GLBL_STU_STATUS", "Organizations.Organization Name" };	

		String[] EMPCAMPUSESAD = { "UD_ADUSER_BMCC_EMP", "UD_ADUSER_MEDGAREVERS_EMP", "UD_ADUSER_CITYTECH_EMP", "UD_ADUSER_HUNTERCOLLEGE_EMP", "UD_ADUSER_QUEENSCOLLEGE_EMP", "UD_ADUSER_LEHMANCOLLEGE_EMP", "UD_ADUSER_SPS_EMP", "UD_ADUSER_JSCHOOL_EMP", "UD_ADUSER_CENTRALOFFICE_EMP", "UD_ADUSER_GRADUATECENTER_EMP", "UD_ADUSER_YORKCOLLEGE_EMP", "UD_ADUSER_KINGBOROUGHCC_EMP", "UD_ADUSER_HOSTOSCC_EMP", "UD_ADUSER_BARUCHCOLLEGE_EMP", "UD_ADUSER_BROOKLYNCOLLEGE_EMP", "UD_ADUSER_HUNTERSCHOOLS_EMP", "UD_ADUSER_CITYCOLLEGE_EMP", "UD_ADUSER_LAWSCHOOL_EMP", "UD_ADUSER_LAGUARDIACC_EMP", "UD_ADUSER_QUEENSBOROUGHCC_EMP", "UD_ADUSER_CSI_EMP", "UD_ADUSER_BRONXCC_EMP", "UD_ADUSER_JOHNJAYCOLLEGE_EMP", "UD_ADUSER_NEWCOMMUNITYCOLLEGE_EMP", "UD_ADUSER_FACULTY", "UD_ADUSER_GLOBAL_EMP_STATUS", "UD_ADUSER_PRIMARYAFFILIATION" };	

		String[] STUCAMPUSESAD = { "UD_ADUSER_BMCC_STU", "UD_ADUSER_MEDGAREVERS_STU", "UD_ADUSER_CITYTECH_STU", "UD_ADUSER_HUNTERCOLLEGE_STU", "UD_ADUSER_QUEENSCOLLEGE_STU", "UD_ADUSER_LEHMANCOLLEGE_STU", "UD_ADUSER_SPS_STU", "UD_ADUSER_JSCHOOL_STU",  "UD_ADUSER_GRADUATECENTER_STU", "UD_ADUSER_YORKCOLLEGE_STU", "UD_ADUSER_KINGBOROUGHCC_STU", "UD_ADUSER_HOSTOSCC_STU", "UD_ADUSER_BARUCHCOLLEGE_STU", "UD_ADUSER_BROOKLYNCOLLEGE_STU", "UD_ADUSER_HUNTERSCHOOLS_STU", "UD_ADUSER_CITYCOLLEGE_STU", "UD_ADUSER_LAWSCHOOL_STU", "UD_ADUSER_LAGUARDIACC_STU", "UD_ADUSER_QUEENSBOROUGHCC_STU", "UD_ADUSER_CSI_STU", "UD_ADUSER_BRONXCC_STU", "UD_ADUSER_JOHNJAYCOLLEGE_STU", "UD_ADUSER_NEWCOMMUNITYCOLLEGE_STU", "UD_ADUSER_GLOBAL_STU_STATUS", "UD_ADUSER_PRIMARYAFFILIATION" };	

		try {   

			Hashtable userHash = new Hashtable();
			tcDataSet dsList = new tcDataSet();
			userHash.put("Users.Key", String.valueOf(userKey));
			tcResultSet userSet = userIntf.findAllUsers(userHash);
			userSet.goToRow(0);
			String[] userColumnNames = userSet.getColumnNames();
			for(int n = 0; n < userColumnNames.length; n++) {
				log.debug(userColumnNames[n] + " = " + userSet.getStringValue(userColumnNames[n]));
			} 


			Hashtable employeeStatusHash = new Hashtable();
			for(int i = 0; i < EMPCAMPUSES.length; i++){                
				employeeStatusHash.put(EMPCAMPUSES[i], userSet.getStringValue(EMPCAMPUSES[i]));
				log.debug("Hashed Employee Campus Status : " + EMPCAMPUSES[i] + " | " +  userSet.getStringValue(EMPCAMPUSES[i]));
			}	            



			Hashtable studentStatusHash = new Hashtable();
			for(int i = 0; i < STUCAMPUSES.length; i++){                
				studentStatusHash.put(STUCAMPUSES[i], userSet.getStringValue(STUCAMPUSES[i]));
				log.debug("Hashed Student Campus Status : " + STUCAMPUSES[i] + " | " +  userSet.getStringValue(STUCAMPUSES[i]));
			}	            


			Hashtable employeeADHash = new Hashtable();
			for(int i = 0; i < EMPCAMPUSES.length; i++){                
				employeeADHash.put(EMPCAMPUSES[i], EMPCAMPUSESAD[i]);
				log.debug("Hashed Employee AD Campus mapping : " + EMPCAMPUSES[i] + " | " +  EMPCAMPUSESAD[i]);
			}	  

			Hashtable studentADHash = new Hashtable();
			for(int i = 0; i < STUCAMPUSES.length; i++){                
				studentADHash.put(STUCAMPUSES[i], STUCAMPUSESAD[i]);
				log.debug("Hashed Student AD Campus mapping : " + STUCAMPUSES[i] + " | " +  STUCAMPUSESAD[i]);
			}	


			Hashtable employeeAttributeADHash = null;
			Hashtable studentAttributeADHash = null;

			String employeeStatus = null;
			log.debug("If statement for employees");
			if(employeeStatusHash.containsValue("A") || employeeStatusHash.containsValue("L") || employeeStatusHash.containsValue("P") || employeeStatusHash.containsValue("S") || employeeStatusHash.containsValue("W"))
				employeeStatus = "Active";
			else if(employeeStatusHash.containsValue("R") || employeeStatusHash.containsValue("Q"))
				employeeStatus = "Retired";
			else if(employeeStatusHash.containsValue("T") || employeeStatusHash.containsValue("D") || employeeStatusHash.containsValue("U") || employeeStatusHash.containsValue("V") || employeeStatusHash.containsValue("X"))
				employeeStatus = "Inactive";


			String studentStatus = null;
			log.debug("If statement for students");
			if(studentStatusHash.containsValue("AC") || studentStatusHash.containsValue("AD") || studentStatusHash.containsValue("AP") || studentStatusHash.containsValue("CM") || studentStatusHash.containsValue("CN") || studentStatusHash.containsValue("DC") || studentStatusHash.containsValue("DM") || studentStatusHash.containsValue("LA") || studentStatusHash.containsValue("PM") || studentStatusHash.containsValue("SP") || studentStatusHash.containsValue("WT"))
				studentStatus = "Active";
			else
				if(studentStatusHash.containsValue("DE"))
					studentStatus = "Inactive";
			log.debug((new StringBuilder("studentStatus:")).append(studentStatus).toString());



			//Obtain Process Instance Key
			tcResultSet objs = userIntf.getObjects(userKey);
			boolean pikb = false;
			long pik = 0000000000;
			log.debug("** Now try to obtain UD_ADUSER");
			for (int l=0; l<objs.getRowCount(); l++) {
				objs.goToRow(l);
				if ( objs.getStringValue("Process.Process Definition.Process Form Name").equals("UD_ADUSER") ) {
					pik = objs.getLongValue("Process Instance.Key");
					log.debug("    ***Got Process Instance Key " + pik);
					pikb = true;              											
					}
			}


			// Now that we have process instance key for AD_USER and we know if the user is an EMP or STU

			if (employeeStatus != null && pikb) {

				log.debug("**Received an Employee type user with Status : " + employeeStatus);
				employeeAttributeADHash = new Hashtable();

				// MAP EMPUSR hash to EMPAD hash
				Enumeration iter = employeeStatusHash.keys();
				while(iter.hasMoreElements()) {
					String key = (String)iter.nextElement();
					log.debug("**" + key + "|" + employeeStatusHash.get(key));
					log.debug("***USR to AD_USER : " + key + " : " + employeeADHash.get(key));
					// Populate AD employeestatushash key-values
					employeeAttributeADHash.put(employeeADHash.get(key), employeeStatusHash.get(key)); 				
				} //end while

				tcResultSet pfdrs = formIntf.getProcessFormData(pik);
				String[] columnNames = null;
				log.debug("***Obtain UD_AD form data and dump before updating ..");
				for (int m=0; m<pfdrs.getRowCount(); m++) {
					pfdrs.goToRow(m);
					columnNames = pfdrs.getColumnNames();
					for(int n = 0; n < columnNames.length; n++) {
						log.debug("****" + columnNames[n] + " = " + pfdrs.getStringValue(columnNames[n]));
					}
				}

				Enumeration empstatus_iter = employeeAttributeADHash.keys();
				HashMap hm = new HashMap();
				while (empstatus_iter.hasMoreElements()) {
					String campus_key = (String)empstatus_iter.nextElement();
					String ad_key = campus_key;
					String ad_value = (String)employeeAttributeADHash.get(campus_key);
					hm.put(ad_key, ad_value);
					log.debug("       ad_key : ad_value - " + ad_key + " : " + ad_value);
				}

				formIntf.setProcessFormData(pik, hm);

			} // end if

			if (studentStatus != null && pikb) {

				log.debug("**Received a Student type user with Status : " + studentStatus);
				studentAttributeADHash = new Hashtable();

				Enumeration iterstu = studentStatusHash.keys();
				while(iterstu.hasMoreElements()) {
					String keystu = (String)iterstu.nextElement();
					log.debug("**" + keystu + "|" + studentStatusHash.get(keystu));
					log.debug("***USR to AD_USER : " + keystu + " : " + studentADHash.get(keystu));
					// Populate AD studentstatushash key-values
					studentAttributeADHash.put(studentADHash.get(keystu), studentStatusHash.get(keystu)); 				
				} //end while

				tcResultSet pfdrsstu = formIntf.getProcessFormData(pik);
				String[] columnNamesStu = null;
				log.debug("***Obtain UD_AD form data and dump before updating ..");
				for (int m=0; m<pfdrsstu.getRowCount(); m++) {
					pfdrsstu.goToRow(m);
					columnNamesStu = pfdrsstu.getColumnNames();
					for(int n = 0; n < columnNamesStu.length; n++) {
						log.debug("****"+ columnNamesStu[n] + " = " + pfdrsstu.getStringValue(columnNamesStu[n]));
					}
				}				

				Enumeration stustatus_iter = studentAttributeADHash.keys();
				while (stustatus_iter.hasMoreElements()) {
					String campus_key_stu = (String)stustatus_iter.nextElement();
					HashMap hmstu = new HashMap();
					String ad_key_stu = campus_key_stu;
					String ad_value_stu = (String)studentAttributeADHash.get(campus_key_stu);
					hmstu.put(ad_key_stu, ad_value_stu);
					log.debug("       ad_key : ad_value - " + ad_key_stu + " : " + ad_value_stu);
					formIntf.setProcessFormData(pik, hmstu);
				}
			} // end if

			response = true;	    
			log.debug("User Campus Status Values and other attributes Updated");
		}
		catch(tcColumnNotFoundException e)
		{
			e.printStackTrace();
		}
		catch(tcUserNotFoundException e)
		{
			e.printStackTrace();
		}
		
		return response;
	}

	public void process(String userId) throws Exception {

		log.debug("Start Find User Operations .. "); 
		Hashtable mhSearchCriteria = new Hashtable(); 
		long userLongKey = 0000000000;
		mhSearchCriteria.put("Users.User ID", userId);

		tcResultSet moResultSet = userIntf.findUsers(mhSearchCriteria); 
		if(moResultSet.getRowCount() != 0) {
			moResultSet.goToRow(0);
			userLongKey = moResultSet.getLongValue("Users.Key");
			setCampus(userLongKey);			
		} else {
			log.debug("  " + userId + " NOT FOUND.");
		}

	}


}
