package com.cuny.edu.ad;

import java.util.Hashtable;
import java.util.HashMap; 
import java.util.Enumeration;

import Thor.API.Exceptions.*;
import Thor.API.Operations.tcUserOperationsIntf; 
import Thor.API.Operations.tcFormInstanceOperationsIntf;
import Thor.API.Exceptions.tcAPIException;
import Thor.API.tcResultSet;
import com.thortech.util.logging.Logger;
import com.thortech.xl.crypto.tcCryptoException;

import oracle.iam.platform.Platform;

public class CunyProvAD 
{ 
	private static Logger logger = Logger.getLogger("CUNY.CUSTOM");
	public String setCampus(long userKey) throws tcAPIException, tcCryptoException, tcUserAccountDisabledException, tcPasswordResetAttemptsExceededException, tcLoginAttemptsExceededException, tcUserAccountInvalidException, tcUserAlreadyLoggedInException, tcNotAtomicProcessException, tcFormNotFoundException, tcProcessNotFoundException, tcInvalidValueException, tcRequiredDataMissingException {
		tcUserOperationsIntf userIntf = Platform.getService(Thor.API.Operations.tcUserOperationsIntf.class);
		tcFormInstanceOperationsIntf formIntf = Platform.getService(Thor.API.Operations.tcFormInstanceOperationsIntf.class);


		logger.debug((new StringBuilder("CunyProvAD : setCampus() Parameter Variables passed are:userKey=[")).append(userKey).append("]").toString());
		String response = "AD_COPY_ATTRS_FAILED";
		String[] EMPCAMPUSES = { "USR_UDF_BMC01_EMP", "USR_UDF_MEC01_EMP", "USR_UDF_NYT01_EMP", "USR_UDF_HTR01_EMP", "USR_UDF_QNS01_EMP", "USR_UDF_LEH01_EMP", "USR_UDF_SPS01_EMP", "USR_UDF_SOJ01_EMP", "USR_UDF_COSEN_EMP", "USR_UDF_GRD01_EMP", "USR_UDF_YRK01_EMP", "USR_UDF_KCC01_EMP", "USR_UDF_HOS01_EMP", "USR_UDF_BAR01_EMP", "USR_UDF_BKL01_EMP", "USR_UDF_HCS01_EMP", "USR_UDF_CTY01_EMP", "USR_UDF_LAW01_EMP", "USR_UDF_LAG01_EMP", "USR_UDF_QCC01_EMP", "USR_UDF_CSI01_EMP", "USR_UDF_BCC01_EMP", "USR_UDF_JJC01_EMP" };
		String[] STUCAMPUSES = { "USR_UDF_BMC01_STU", "USR_UDF_MEC01_STU", "USR_UDF_NYT01_STU", "USR_UDF_HTR01_STU", "USR_UDF_QNS01_STU", "USR_UDF_LEH01_STU", "USR_UDF_SPS01_STU", "USR_UDF_SOJ01_STU",  "USR_UDF_GRD01_STU", "USR_UDF_YORKCOLLEGESTU", "USR_UDF_KCC01_STU", "USR_UDF_HOS01_STU", "USR_UDF_BAR01_STU", "USR_UDF_BKL01_STU", "USR_UDF_HCS01_STU", "USR_UDF_CTY01_STU", "USR_UDF_LAW01_STU", "USR_UDF_LAG01_STU", "USR_UDF_QCC01_STU", "USR_UDF_CSI01_STU", "USR_UDF_BCC01_STU", "USR_UDF_JJC01_STU" };	
		String[] EMPCAMPUSESAD = { "UD_ADUSER_BMCC_EMP", "UD_ADUSER_MEDGAREVERS_EMP", "UD_ADUSER_CITYTECH_EMP", "UD_ADUSER_HUNTERCOLLEGE_EMP", "UD_ADUSER_QUEENSCOLLEGE_EMP", "UD_ADUSER_LEHMANCOLLEGE_EMP", "UD_ADUSER_SPS_EMP", "UD_ADUSER_JSCHOOL_EMP", "UD_ADUSER_CENTRALOFFICE_EMP", "UD_ADUSER_GRADUATECENTER_EMP", "UD_ADUSER_YORKCOLLEGE_EMP", "UD_ADUSER_KINGBOROUGHCC_EMP", "UD_ADUSER_HOSTOSCC_EMP", "UD_ADUSER_BARUCHCOLLEGE_EMP", "UD_ADUSER_BROOKLYNCOLLEGE_EMP", "UD_ADUSER_HUNTERSCHOOLS_EMP", "UD_ADUSER_CITYCOLLEGE_EMP", "UD_ADUSER_LAWSCHOOL_EMP", "UD_ADUSER_LAGUARDIACC_EMP", "UD_ADUSER_QUEENSBOROUGHCC_EMP", "UD_ADUSER_CSI_EMP", "UD_ADUSER_BRONXCC_EMP", "UD_ADUSER_JOHNJAYCOLLEGE_EMP" };	
		String[] STUCAMPUSESAD = { "UD_ADUSER_BMCC_STU", "UD_ADUSER_MEDGAREVERS_STU", "UD_ADUSER_CITYTECH_STU", "UD_ADUSER_HUNTERCOLLEGE_STU", "UD_ADUSER_QUEENSCOLLEGE_STU", "UD_ADUSER_LEHMANCOLLEGE_STU", "UD_ADUSER_SPS_STU", "UD_ADUSER_JSCHOOL_STU",  "UD_ADUSER_GRADUATECENTER_STU", "UD_ADUSER_YORKCOLLEGE_STU", "UD_ADUSER_KINGBOROUGHCC_STU", "UD_ADUSER_HOSTOSCC_STU", "UD_ADUSER_BARUCHCOLLEGE_STU", "UD_ADUSER_BROOKLYNCOLLEGE_STU", "UD_ADUSER_HUNTERSCHOOLS_STU", "UD_ADUSER_CITYCOLLEGE_STU", "UD_ADUSER_LAWSCHOOL_STU", "UD_ADUSER_LAGUARDIACC_STU", "UD_ADUSER_QUEENSBOROUGHCC_STU", "UD_ADUSER_CSI_STU", "UD_ADUSER_BRONXCC_STU", "UD_ADUSER_JOHNJAYCOLLEGE_STU" };	

		try {   
			Hashtable userHash = new Hashtable();
			userHash.put("Users.Key", String.valueOf(userKey));
			tcResultSet userSet = userIntf.findAllUsers(userHash);
			userSet.goToRow(0);
			String[] userColumnNames = userSet.getColumnNames();
			for(int n = 0; n < userColumnNames.length; n++) {
				logger.debug("CunyProvAD : " + userColumnNames[n] + " = " + userSet.getStringValue(userColumnNames[n]));
			} 
			Hashtable employeeStatusHash = new Hashtable();
			for(int i = 0; i < EMPCAMPUSES.length; i++){                
				employeeStatusHash.put(EMPCAMPUSES[i], userSet.getStringValue(EMPCAMPUSES[i]));
				logger.debug("CunyProvAD : Hashed Employee Campus Status : " + EMPCAMPUSES[i] + " | " +  userSet.getStringValue(EMPCAMPUSES[i]));
			}	            
			Hashtable studentStatusHash = new Hashtable();
			for(int i = 0; i < STUCAMPUSES.length; i++){                
				studentStatusHash.put(STUCAMPUSES[i], userSet.getStringValue(STUCAMPUSES[i]));
				logger.debug("CunyProvAD : Hashed Student Campus Status : " + STUCAMPUSES[i] + " | " +  userSet.getStringValue(STUCAMPUSES[i]));
			}
			Hashtable employeeADHash = new Hashtable();
			for(int i = 0; i < EMPCAMPUSES.length; i++){                
				employeeADHash.put(EMPCAMPUSES[i], EMPCAMPUSESAD[i]);
				logger.debug("CunyProvAD : Hashed Employee AD Campus mapping : " + EMPCAMPUSES[i] + " | " +  EMPCAMPUSESAD[i]);
			}	  
			Hashtable studentADHash = new Hashtable();
			for(int i = 0; i < STUCAMPUSES.length; i++){                
				studentADHash.put(STUCAMPUSES[i], STUCAMPUSESAD[i]);
				logger.debug("CunyProvAD : Hashed Student AD Campus mapping : " + STUCAMPUSES[i] + " | " +  STUCAMPUSESAD[i]);
			}	

			Hashtable employeeAttributeADHash = null;
			Hashtable studentAttributeADHash = null;
			String employeeStatus = null;
			logger.debug("CunyProvAD : If statement for employees");
			if(employeeStatusHash.containsValue("A") || employeeStatusHash.containsValue("L") || employeeStatusHash.containsValue("P") || employeeStatusHash.containsValue("S") || employeeStatusHash.containsValue("W"))
				employeeStatus = "Active";
			else if(employeeStatusHash.containsValue("R") || employeeStatusHash.containsValue("Q"))
				employeeStatus = "Retired";
			else if(employeeStatusHash.containsValue("T") || employeeStatusHash.containsValue("D") || employeeStatusHash.containsValue("U") || employeeStatusHash.containsValue("V") || employeeStatusHash.containsValue("X"))
				employeeStatus = "Inactive";

			String studentStatus = null;
			logger.debug("CunyProvAD : If statement for students");
			if(studentStatusHash.containsValue("AC") || studentStatusHash.containsValue("AD") || studentStatusHash.containsValue("AP") || studentStatusHash.containsValue("CM") || studentStatusHash.containsValue("CN") || studentStatusHash.containsValue("DC") || studentStatusHash.containsValue("DM") || studentStatusHash.containsValue("LA") || studentStatusHash.containsValue("PM") || studentStatusHash.containsValue("SP") || studentStatusHash.containsValue("WT"))
				studentStatus = "Active";
			else
				if(studentStatusHash.containsValue("DE"))
					studentStatus = "Inactive";
			logger.debug((new StringBuilder("CunyProvAD : studentStatus:")).append(studentStatus).toString());

			//Obtain AD Form Process Instance Key
			tcResultSet objs = userIntf.getObjects(userKey);
			boolean pikb = false;
			long pik = 0000000000;
			logger.debug("CunyProvAD : Obtain UD_ADUSER process Instance Key for the user " + userKey);
			for (int l=0; l<objs.getRowCount(); l++) {
				objs.goToRow(l);
				logger.debug(" Process Instance.Key = " + objs.getLongValue("Process Instance.Key"));
				logger.debug(" Process.Process Definition.Process Form Key = " + objs.getLongValue("Process.Process Definition.Process Form Key")); 
				logger.debug(" Process.Process Definition.Process Form Name = " + objs.getStringValue("Process.Process Definition.Process Form Name"));
				logger.debug(" Process Instance.Process Form Entries = " + objs.getStringValue("Process Instance.Process Form Entries"));
				if ( objs.getStringValue("Process.Process Definition.Process Form Name").equals("UD_ADUSER") ) {
					pik = objs.getLongValue("Process Instance.Key");
					logger.debug(" Process Instance Key for " + userKey+ " is " + pik );
					pikb = true;              											
				}
			}

			// Employee and he/she has a AD process form key
			if (employeeStatus != null && pikb) {
				logger.debug("An Employee type user with Status : " + employeeStatus);
				employeeAttributeADHash = new Hashtable();
				// MAP EMPUSR hash to EMPAD hash
				Enumeration iter = employeeStatusHash.keys();
				while(iter.hasMoreElements()) {
					String key = (String)iter.nextElement();
					logger.debug(key + "|" + employeeStatusHash.get(key));
					logger.debug("USR to AD_USER : " + key + " : " + employeeADHash.get(key));
					// Populate AD employeestatushash key-values
					employeeAttributeADHash.put(employeeADHash.get(key), employeeStatusHash.get(key));
				} //end while
				tcResultSet pfdrs = formIntf.getProcessFormData(pik);
				String[] columnNames = null;
				logger.debug("Obtain form data to dump ..");
				for (int m=0; m<pfdrs.getRowCount(); m++) {
					pfdrs.goToRow(m);
					columnNames = pfdrs.getColumnNames();
					for(int n = 0; n < columnNames.length; n++) {
						logger.debug(columnNames[n] + " = " + pfdrs.getStringValue(columnNames[n]));
					}
				}
				logger.debug("rowcount : " + pfdrs.getRowCount() + " colnamescount : " + columnNames.length);
				Enumeration empstatus_iter = employeeAttributeADHash.keys();
				HashMap hm = new HashMap();
				while (empstatus_iter.hasMoreElements()) {
					String campus_key = (String)empstatus_iter.nextElement();
					String ad_key = campus_key;
					String ad_value = (String)employeeAttributeADHash.get(campus_key);
					hm.put(ad_key, ad_value);
					logger.debug("   ad_key : ad_value - " + ad_key + " : " + ad_value);
				}
				formIntf.setProcessFormData(pik, hm);
			} // end if

			// Student and he/she has a AD process form key
			if (studentStatus != null && pikb) {
				logger.debug("A Student type user with Status : " + studentStatus);
				studentAttributeADHash = new Hashtable();
				Enumeration iterstu = studentStatusHash.keys();
				while(iterstu.hasMoreElements()) {
					String keystu = (String)iterstu.nextElement();
					logger.debug(keystu + "|" + studentStatusHash.get(keystu));
					logger.debug("Status found .. AD field mapping is " + keystu + " : " + studentADHash.get(keystu));
					// Populate AD studentstatushash key-values
					studentAttributeADHash.put(studentADHash.get(keystu), studentStatusHash.get(keystu));
				} //end while

				tcResultSet pfdrsstu = formIntf.getProcessFormData(pik);
				String[] columnNamesStu = null;
				for (int m=0; m<pfdrsstu.getRowCount(); m++) {
					pfdrsstu.goToRow(m);
					columnNamesStu = pfdrsstu.getColumnNames();
					for(int n = 0; n < columnNamesStu.length; n++) {
						logger.debug(columnNamesStu[n] + " = " + pfdrsstu.getStringValue(columnNamesStu[n]));
					}
				}
				logger.debug("AD_USER form rowcount : " + pfdrsstu.getRowCount() + " colnamescount : " + columnNamesStu.length);
				Enumeration stustatus_iter = studentAttributeADHash.keys();
				while (stustatus_iter.hasMoreElements()) {
					String campus_key_stu = (String)stustatus_iter.nextElement();
					HashMap hmstu = new HashMap();
					String ad_key_stu = campus_key_stu;
					String ad_value_stu = (String)studentAttributeADHash.get(campus_key_stu);
					hmstu.put(ad_key_stu, ad_value_stu);
					logger.debug("       ***setProcessFormData " + ad_key_stu + " : " + ad_value_stu);
					formIntf.setProcessFormData(pik, hmstu);
				}
			} // end if
			response = "AD_COPY_ATTRS_SUCCESS";
			logger.debug("User Campus Status Values Updated");
		} catch(tcColumnNotFoundException e)
		{
			logger.error("An exception has occurred : ", e);
		}
		catch(tcUserNotFoundException e) 
		{
			logger.error("An exception has occurred : ", e);
		}
		return response;
	}
}