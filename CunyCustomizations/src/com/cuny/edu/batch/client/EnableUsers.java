package com.cuny.edu.batch.client;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.rmi.RemoteException;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;

import Thor.API.Exceptions.*;

import com.thortech.xl.util.config.ConfigurationClient; 
import Thor.API.Operations.tcUserOperationsIntf; 
import Thor.API.Operations.tcReconciliationOperationsIntf;
import Thor.API.Operations.tcFormInstanceOperationsIntf;
import Thor.API.Operations.tcGroupOperationsIntf;
import Thor.API.Operations.tcITResourceInstanceOperationsIntf;
import Thor.API.Operations.tcLookupOperationsIntf; 
import Thor.API.Operations.tcObjectOperationsIntf;
import Thor.API.Operations.tcOrganizationOperationsIntf; 
import Thor.API.Operations.tcProvisioningOperationsIntf; 
import Thor.API.Base.tcUtilityOperationsIntf;
import Thor.API.Exceptions.tcAPIException;
import Thor.API.tcResultSet;
import Thor.API.tcUtilityFactory;
import com.thortech.util.logging.Logger;

public class EnableUsers {

String fileName;
String password;
List<String> ls=new ArrayList<String>();
protected tcUtilityFactory ioUtilityFactory;
protected tcReconciliationOperationsIntf reconIntf;
protected tcUserOperationsIntf userIntf;
protected tcFormInstanceOperationsIntf formIntf; 
protected tcGroupOperationsIntf groupIntf; 
protected tcITResourceInstanceOperationsIntf itresourceIntf;
protected tcLookupOperationsIntf lookupIntf; 
protected tcObjectOperationsIntf objectIntf;
protected tcOrganizationOperationsIntf orgIntf; 
protected tcProvisioningOperationsIntf provIntf; 
protected tcUtilityOperationsIntf tcUtilIntf;

private static Logger logger = Logger.getLogger("CUNY.CUSTOM");
	
	/**
	 * @param args
	 */
public static void main(String[] args) {
	EnableUsers enableusers = new EnableUsers();
	try {
		enableusers.execute();	
	} catch (Exception e){
		e.printStackTrace();
	}
	}

public void execute()throws Exception {
try {
	logger.debug("***EnableUsers.execute() Entered ..");
	logger.debug("Startup..."); 
	logger.debug("Executing EnableUsers task from a file");
	fileName = getAttribute("FileName");
	if (getTaskAttributes()) {
			readFile();
			}
	logger.debug("Exiting main task");
	} catch(Exception e){
					 e.printStackTrace();
					}
	
	Iterator it=ls.iterator();
	getConns();	  
	while(it.hasNext())
	{
		String useridvalue=(String)it.next();
		logger.debug("EMPLD ID from file : " + useridvalue);
		try {
			
			process(useridvalue);
			
			} catch(Exception e){
				e.printStackTrace();
				}
			}
	closeConns();
	System.exit(0);
		}


public void getConns() throws tcAPIException, tcChallengeNotSetException, tcLoginAttemptsExceededException, tcPasswordResetAttemptsExceededException, tcPasswordExpiredException, tcUserAccountDisabledException, tcUserAccountInvalidException, tcUserAlreadyLoggedInException {
	
	logger.debug("Getting configuration..."); 
	ConfigurationClient.ComplexSetting config = 
	ConfigurationClient.getComplexSettingByPath("Discovery.CoreServer"); 
	logger.debug("Login...");
	 
	Hashtable  env = config.getAllSettings(); 
	logger.debug(System.getProperty("login.username"));
	logger.debug(System.getProperty("login.password"));
	/*
	Enumeration en = env.keys();
	while (en.hasMoreElements()) {
		String keyer = (String)en.nextElement();
		logger.debug(keyer + " : " + env.get(keyer));
	} 
	*/

	ioUtilityFactory = new tcUtilityFactory(env,System.getProperty("login.username"),System.getProperty("login.password"));
	logger.debug("Login...finished");  
	logger.debug("Getting utility interfaces..."); 

	userIntf = (tcUserOperationsIntf)ioUtilityFactory.getUtility("Thor.API.Operations.tcUserOperationsIntf"); 
	objectIntf = (tcObjectOperationsIntf)ioUtilityFactory.getUtility("Thor.API.Operations.tcObjectOperationsIntf");
	formIntf = (tcFormInstanceOperationsIntf)ioUtilityFactory.getUtility("Thor.API.Operations.tcFormInstanceOperationsIntf");
	logger.debug("getConns .. DONE");
}

public void closeConns() {
	try {
		ioUtilityFactory.close(objectIntf);
		ioUtilityFactory.close(formIntf);
		ioUtilityFactory.close(userIntf);
		ioUtilityFactory.close();
		logger.debug("closeConns .. DONE");
		} catch (RemoteException e) {
			e.printStackTrace();
			}
}


public boolean enableUser(long userKey, tcResultSet userSet) throws tcAPIException, tcUserAccountDisabledException, tcPasswordResetAttemptsExceededException, tcLoginAttemptsExceededException, tcUserAccountInvalidException, tcUserAlreadyLoggedInException, tcNotAtomicProcessException, tcFormNotFoundException, tcProcessNotFoundException, tcInvalidValueException, tcRequiredDataMissingException 
{
logger.debug((new StringBuilder("enableUser() Parameter Variables passed are:userKey=[")).append(userKey).append("]").toString());
boolean response = false;

String USR_STATUS = "Users.Status";
        
try {
	logger.debug( "In enableUser with user status : " + userSet.getStringValue(USR_STATUS));	
	//userIntf.disableUser(userKey);
	userIntf.enableUser(userKey);
	response = true;
      } catch(tcUserNotFoundException e)  {
    	  logger.debug("User not found");
    	  e.printStackTrace();
    	      	  } 
      	catch (tcColumnNotFoundException e) {
				e.printStackTrace();				
				}  
 logger.debug("enableUser .. DONE");     	
 return response;
}

public void readFile() throws Exception {
    logger.debug("Reading File " + fileName);
    Hashtable userHash = new Hashtable();
    
    File f = new File (fileName);
    FileReader fr = new FileReader(f);	
    BufferedReader in = new BufferedReader(new FileReader(f));
    String str;
    int counter = 1;
    
    while ((str = in.readLine()) != null) {
	logger.debug("String from file : " + " at [" + counter + "] : " + str);
	ls.add(str);
	counter++;
	}		
    in.close();
    fr.close();
}

public boolean getTaskAttributes() throws Exception { 

   fileName = getAttribute("FileName");
   if (fileName.trim().length()==0) {
   	logger.debug("You must supply a value for all fields(example,FileName).");
   return false;
	}
   logger.debug("getTaskAttributes() Parameter Variables passed are:" + fileName);
   return true;
}


public String getAttribute(String arg) {

return "enableusers_list.txt";

}

public void process(String userId) throws tcAPIException, tcChallengeNotSetException, tcLoginAttemptsExceededException, tcPasswordResetAttemptsExceededException, tcPasswordExpiredException, tcUserAccountDisabledException, tcUserAccountInvalidException, tcUserAlreadyLoggedInException, tcNotAtomicProcessException, tcFormNotFoundException, tcProcessNotFoundException, tcInvalidValueException, tcRequiredDataMissingException 
{
	logger.debug("Start Find User Operations .. "); 
	Hashtable mhSearchCriteria = new Hashtable(); 
	long userLongKey = 0000000000;
	mhSearchCriteria.put("Users.User ID", userId);

	try {
	tcResultSet moResultSet = userIntf.findUsers(mhSearchCriteria);
	
	if(moResultSet.getRowCount() != 0) {
		moResultSet.goToRow(0);
		userLongKey = moResultSet.getLongValue("Users.Key");
		
		logger.debug(userId + " is FOUND with long key " + userLongKey + " disabling user ..");
		long startTime = System.currentTimeMillis();		 
		enableUser(userLongKey, moResultSet);
		long endTime = System.currentTimeMillis();		
		logger.debug("Total elapsed time in execution of method enableUser() is : "+ (endTime-startTime));
		
	} else {
		logger.debug(userId + " NOT FOUND.");
	}
	
		
	} catch (tcColumnNotFoundException e) {
			e.printStackTrace();
		} 
			   
		   }
	}	

	
	

