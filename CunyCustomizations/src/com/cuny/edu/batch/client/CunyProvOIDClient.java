package com.cuny.edu.batch.client;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.rmi.RemoteException;
import java.util.Hashtable;
import java.util.HashMap; 
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;

import Thor.API.Exceptions.*;

import com.thortech.xl.util.config.ConfigurationClient; 
import Thor.API.Operations.tcUserOperationsIntf; 
import Thor.API.Operations.tcReconciliationOperationsIntf;
import Thor.API.Operations.tcFormInstanceOperationsIntf;
import Thor.API.Operations.tcGroupOperationsIntf;
import Thor.API.Operations.tcITResourceInstanceOperationsIntf;
import Thor.API.Operations.tcLookupOperationsIntf; 
import Thor.API.Operations.tcObjectOperationsIntf;
import Thor.API.Operations.tcOrganizationOperationsIntf; 
import Thor.API.Operations.tcProvisioningOperationsIntf; 
import Thor.API.Base.tcUtilityOperationsIntf;
import Thor.API.Exceptions.tcAPIException;
import Thor.API.tcResultSet;
import Thor.API.tcUtilityFactory;
import com.thortech.util.logging.Logger;

public class CunyProvOIDClient {

	String fileName;
	String password;
	String ORGANIZATION_KEY = "Organizations.Key";
	String ORGANIZATION_NAME = "Organizations.Organization Name";
	String USER_FIRST_NAME = "Users.First Name";
	String USER_LAST_NAME = "Users.Last Name";
	String USER_STU_STATUS = "USR_UDF_GLBL_STU_STATUS";
	String USER_EMP_STATUS = "USR_UDF_GLBL_EMP_STATUS";
	String USER_FACULTY = "USR_UDF_FACULTY";
	List<String> ls=new ArrayList<String>();
	protected tcUtilityFactory ioUtilityFactory;
	protected tcReconciliationOperationsIntf reconIntf;
	protected tcUserOperationsIntf userIntf;
	protected tcFormInstanceOperationsIntf formIntf; 
	protected tcGroupOperationsIntf groupIntf; 
	protected tcITResourceInstanceOperationsIntf itresourceIntf;
	protected tcLookupOperationsIntf lookupIntf; 
	protected tcObjectOperationsIntf objectIntf;
	protected tcOrganizationOperationsIntf orgIntf; 
	protected tcProvisioningOperationsIntf provIntf; 
	protected tcUtilityOperationsIntf tcUtilIntf;

	private static Logger logger = Logger.getLogger("CUNY.CUSTOM");

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		CunyProvOIDClient cupwd = new CunyProvOIDClient();
		try {
			cupwd.execute();	
		} catch (Exception e){
			e.printStackTrace();
		}
	}

	public void execute()throws Exception {
		try {
			logger.debug("***CunyProvOIDClient.execute() Entered ..");
			logger.debug("Startup..."); 
			logger.debug("Executing CunyProvOIDClient task from a file");
			fileName = getAttribute("FileName");
			if (getTaskAttributes()) {
				readFile();
			}
			logger.debug("Exiting main task");
		} catch(Exception e){
			e.printStackTrace();
		}

		Iterator it=ls.iterator();
		getConns();
		int rec_cnt = 0;
		while(it.hasNext())
		{
			rec_cnt++;
			String emplidvalue=(String)it.next();
			logger.debug("Value :" + emplidvalue);
			process(emplidvalue);
		}

		closeConns();
		System.exit(0);
	}

	public void doIt(String emplid, String colkey, String colname) {
		try {		
			process(emplid, colkey, colname);		
		} catch(Exception e){
			e.printStackTrace();
		}
	}


	public void getConns() throws tcAPIException, tcChallengeNotSetException, tcLoginAttemptsExceededException, tcPasswordResetAttemptsExceededException, tcPasswordExpiredException, tcUserAccountDisabledException, tcUserAccountInvalidException, tcUserAlreadyLoggedInException {

		logger.debug("Getting configuration..."); 
		ConfigurationClient.ComplexSetting config = 
			ConfigurationClient.getComplexSettingByPath("Discovery.CoreServer"); 
		logger.debug("Login...");

		Hashtable  env = config.getAllSettings(); 
		logger.debug(System.getProperty("login.username"));
		logger.debug(System.getProperty("login.password"));
		/*
	Enumeration en = env.keys();
	while (en.hasMoreElements()) {
		String keyer = (String)en.nextElement();
		logger.debug(keyer + " : " + env.get(keyer));
	} 
		 */

		ioUtilityFactory = new tcUtilityFactory(env,System.getProperty("login.username"),System.getProperty("login.password"));
		logger.debug("Login...finished");  
		logger.debug("Getting utility interfaces..."); 

		userIntf = (tcUserOperationsIntf)ioUtilityFactory.getUtility("Thor.API.Operations.tcUserOperationsIntf"); 
		objectIntf = (tcObjectOperationsIntf)ioUtilityFactory.getUtility("Thor.API.Operations.tcObjectOperationsIntf");
		formIntf = (tcFormInstanceOperationsIntf)ioUtilityFactory.getUtility("Thor.API.Operations.tcFormInstanceOperationsIntf");
	}

	public void closeConns() {
		try {
			ioUtilityFactory.close(objectIntf);
			ioUtilityFactory.close(formIntf);
			ioUtilityFactory.close(userIntf);
			ioUtilityFactory.close();
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}




	public boolean provisionReqAttrs(long userKey, tcResultSet userSet) throws tcAPIException, tcUserAccountDisabledException, tcPasswordResetAttemptsExceededException, tcLoginAttemptsExceededException, tcUserAccountInvalidException, tcUserAlreadyLoggedInException, tcNotAtomicProcessException, tcFormNotFoundException, tcProcessNotFoundException, tcInvalidValueException, tcRequiredDataMissingException 
	{
		
		boolean response = false;

		try {


			//Obtain Process Instance Key
			tcResultSet objs = userIntf.getObjects(userKey);
			boolean pikb = false;
			long pik = 0000000000;
			logger.debug("** Now try to obtain UD_OID_USR form instance");
			for (int l=0; l<objs.getRowCount(); l++) {
				objs.goToRow(l);
				if ( objs.getStringValue("Process.Process Definition.Process Form Name").equals("UD_OID_USR") ) {
					pik = objs.getLongValue("Process Instance.Key");
					logger.debug("    ***Got Process Instance Key for UD_OID_USR " + pik);
					pikb = true;              											
				}
			}

					
			
			if (pikb) {
				HashMap hmstu = new HashMap();			
				hmstu.put("UD_OID_USR_GLOBAL_STU_STATUS", userSet.getStringValue(USER_STU_STATUS));
				hmstu.put("UD_OID_USR_GLOBAL_EMP_STATUS", userSet.getStringValue(USER_EMP_STATUS));
				hmstu.put("UD_OID_USR_FACULTY_FLAG", userSet.getStringValue(USER_FACULTY));
				formIntf.setProcessFormData(pik, hmstu);
//				tcResultSet pfdrs = formIntf.getProcessFormData(pik);
//				String[] columnNames = null;
//				logger.debug("***Obtain UD_OID_USR form data and dump after updating ..");				
//				for (int m=0; m<pfdrs.getRowCount(); m++) {
//					pfdrs.goToRow(m);
//					columnNames = pfdrs.getColumnNames();
//					for(int n = 0; n < columnNames.length; n++) {
//						logger.debug("****" + columnNames[n] + " = " + pfdrs.getStringValue(columnNames[n]));
//					}
//				}
			}


		} 
		catch(tcUserNotFoundException e) {
			logger.debug("  User not found");
			e.printStackTrace();
		} 
		catch (tcColumnNotFoundException e) {		
			e.printStackTrace();
		}


		return response;
	}



	public boolean dumpReqAttrs(long userKey, tcResultSet userSet) throws tcAPIException, tcUserAccountDisabledException, tcPasswordResetAttemptsExceededException, tcLoginAttemptsExceededException, tcUserAccountInvalidException, tcUserAlreadyLoggedInException, tcNotAtomicProcessException, tcFormNotFoundException, tcProcessNotFoundException, tcInvalidValueException, tcRequiredDataMissingException 
	{
		logger.debug((new StringBuilder("dumpReqAttrs() : userKey=[")).append(userKey).append("]").toString());
		boolean response = false;

		try {
			if (userSet != null) {	
				logger.debug("  User [" + userSet.getStringValue(USER_FIRST_NAME) + " " + userSet.getStringValue(USER_LAST_NAME) + "] has " + USER_STU_STATUS +"[" + userSet.getStringValue(USER_STU_STATUS) + "] and " + USER_EMP_STATUS + "[" + userSet.getStringValue(USER_EMP_STATUS) + "]" + " and " + USER_FACULTY + "[" + userSet.getStringValue(USER_FACULTY) + "]");   	
			}
		} catch(tcColumnNotFoundException e) {
			e.printStackTrace();
		}

		return response;
	}

	public void readFile() throws Exception {
		logger.debug("Reading File " + fileName);
		Hashtable userHash = new Hashtable();

		File f = new File (fileName);
		FileReader fr = new FileReader(f);	
		BufferedReader in = new BufferedReader(new FileReader(f));
		String str;
		int counter = 1;

		while ((str = in.readLine()) != null) {
			logger.debug("Record from file : " + " at [" + counter + "] : " + str);
			ls.add(str);
			counter++;
		}		
		in.close();
		fr.close();
	}

	public boolean getTaskAttributes() throws Exception { 

		fileName = getAttribute("FileName");
		if (fileName.trim().length()==0) {
			logger.debug("You must supply a value for all fields(example,FileName).");
			return false;
		}
		logger.debug("getTaskAttributes() Parameter Variables passed are:" + fileName);
		return true;
	}


	public String getAttribute(String arg) {

		return "a.txt";

	}


	public void process(String emplid, String colkey, String colname) throws tcAPIException, tcChallengeNotSetException, tcLoginAttemptsExceededException, tcPasswordResetAttemptsExceededException, tcPasswordExpiredException, tcUserAccountDisabledException, tcUserAccountInvalidException, tcUserAlreadyLoggedInException, tcNotAtomicProcessException, tcFormNotFoundException, tcProcessNotFoundException, tcInvalidValueException, tcRequiredDataMissingException 
	{
		logger.debug("  Searching User with EMPLID [" + emplid + "]"); 
		Hashtable mhSearchCriteria = new Hashtable(); 
		long userLongKey = 0000000000;
		mhSearchCriteria.put("Users.User ID", emplid);

		try {
			tcResultSet moResultSet = userIntf.findUsers(mhSearchCriteria);

			if(moResultSet.getRowCount() != 0) {
				moResultSet.goToRow(0);
				userLongKey = moResultSet.getLongValue("Users.Key");


				logger.debug("  " + emplid + " is FOUND with long key " + userLongKey);
				long startTime = System.currentTimeMillis();
				dumpReqAttrs(userLongKey, moResultSet);

		//		provisionReqAttrs(userLongKey, moResultSet, colkey, colname);
				long endTime = System.currentTimeMillis();		
				logger.debug("  Total elapsed time in execution of method CunyProvOIDClient() : "+ (endTime-startTime));	

			} else {
				logger.debug("  " + emplid + " NOT FOUND.");
			}


		} catch (tcColumnNotFoundException e) {
			e.printStackTrace();
		} 

	}

	public void process(String emplid) throws tcAPIException, tcChallengeNotSetException, tcLoginAttemptsExceededException, tcPasswordResetAttemptsExceededException, tcPasswordExpiredException, tcUserAccountDisabledException, tcUserAccountInvalidException, tcUserAlreadyLoggedInException, tcNotAtomicProcessException, tcFormNotFoundException, tcProcessNotFoundException, tcInvalidValueException, tcRequiredDataMissingException 
	{
		logger.debug("  Searching User with EMPLID [" + emplid + "]");
		Hashtable mhSearchCriteria = new Hashtable(); 
		long userLongKey = 0000000000;
		mhSearchCriteria.put("Users.User ID", emplid);

		try {
			tcResultSet moResultSet = userIntf.findUsers(mhSearchCriteria);

			if(moResultSet.getRowCount() != 0) {
				moResultSet.goToRow(0);
				userLongKey = moResultSet.getLongValue("Users.Key");


				logger.debug("  " + emplid + " is FOUND with long key " + userLongKey);
				long startTime = System.currentTimeMillis();
				dumpReqAttrs(userLongKey, moResultSet);

				provisionReqAttrs(userLongKey, moResultSet);
				long endTime = System.currentTimeMillis();		
				logger.debug("  Total elapsed time in execution of provision attributes to OID took for EMPLID " + "[" + emplid + "] " + (endTime-startTime) + " ms.");	

			} else {
				logger.debug(" ERROR : " + emplid + " NOT FOUND.");
			}


		} catch (tcColumnNotFoundException e) {
			e.printStackTrace();
		} 

	}
}	




