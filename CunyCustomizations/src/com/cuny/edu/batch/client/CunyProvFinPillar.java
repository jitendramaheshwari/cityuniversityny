package com.cuny.edu.batch.client;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.rmi.RemoteException;
import java.util.Hashtable;
import java.util.HashMap; 
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;


import com.thortech.xl.util.config.ConfigurationClient; 
import Thor.API.Operations.tcUserOperationsIntf; 
import Thor.API.Operations.tcReconciliationOperationsIntf;
import Thor.API.Operations.tcFormInstanceOperationsIntf;
import Thor.API.Operations.tcGroupOperationsIntf;
import Thor.API.Operations.tcITResourceInstanceOperationsIntf;
import Thor.API.Operations.tcLookupOperationsIntf; 
import Thor.API.Operations.tcObjectOperationsIntf;
import Thor.API.Operations.tcOrganizationOperationsIntf; 
import Thor.API.Operations.tcProvisioningOperationsIntf; 
import Thor.API.Exceptions.tcAPIException;
import Thor.API.Exceptions.tcChallengeNotSetException;
import Thor.API.Exceptions.tcColumnNotFoundException;
import Thor.API.Exceptions.tcFormNotFoundException;
import Thor.API.Exceptions.tcInvalidValueException;
import Thor.API.Exceptions.tcLoginAttemptsExceededException;
import Thor.API.Exceptions.tcNotAtomicProcessException;
import Thor.API.Exceptions.tcObjectNotFoundException;
import Thor.API.Exceptions.tcPasswordExpiredException;
import Thor.API.Exceptions.tcPasswordResetAttemptsExceededException;
import Thor.API.Exceptions.tcProcessNotFoundException;
import Thor.API.Exceptions.tcProvisioningNotAllowedException;
import Thor.API.Exceptions.tcRequiredDataMissingException;
import Thor.API.Exceptions.tcUserAccountDisabledException;
import Thor.API.Exceptions.tcUserAccountInvalidException;
import Thor.API.Exceptions.tcUserAlreadyLoggedInException;
import Thor.API.Exceptions.tcUserNotFoundException;
import Thor.API.Exceptions.tcVersionNotDefinedException;
import Thor.API.Exceptions.tcVersionNotFoundException;
import Thor.API.tcResultSet;
import Thor.API.tcUtilityFactory;
import com.thortech.util.logging.Logger;


public class CunyProvFinPillar { 
	String fileName;
	String password;
	List<String> ls=new ArrayList<String>();
	protected tcUtilityFactory ioUtilityFactory;
	protected tcReconciliationOperationsIntf reconIntf;
	protected tcUserOperationsIntf userIntf;
	protected tcFormInstanceOperationsIntf formIntf;
	protected tcGroupOperationsIntf groupIntf;
	protected tcITResourceInstanceOperationsIntf itresourceIntf;
	protected tcLookupOperationsIntf lookupIntf;
	protected tcObjectOperationsIntf objectIntf;
	protected tcOrganizationOperationsIntf orgIntf;
	protected tcProvisioningOperationsIntf provIntf;
	private static Logger logger = Logger.getLogger("CUNY.CUSTOM");
	
public static void main(String[] args) {
	CunyProvFinPillar cpf = new CunyProvFinPillar();
	try {
		cpf.execute();
		} catch (Exception e) {
			e.printStackTrace();
			}
		} 

public boolean provisionToFinancialPillar(long userKey, tcResultSet userSet) throws tcAPIException, tcUserNotFoundException, tcObjectNotFoundException, tcColumnNotFoundException, tcProvisioningNotAllowedException, tcUserAccountDisabledException, tcPasswordResetAttemptsExceededException, tcLoginAttemptsExceededException, tcUserAccountInvalidException, tcUserAlreadyLoggedInException, tcNotAtomicProcessException, tcFormNotFoundException, tcProcessNotFoundException, tcInvalidValueException, tcRequiredDataMissingException, tcVersionNotDefinedException, tcVersionNotFoundException 
{
	boolean response = false;
	try {
		logger.debug("Start, provisioning of the given ID ..");
		HashMap objmap = new HashMap();
		long psoftpillarkey = 0000000000;
		long psftfinformkey = 000000000;
		long plChildFormDefinitionKey = 00000000;
		String res_Name = "Peoplesoft Financials";
		
		objmap.put("Objects.Name", res_Name);
		tcResultSet objmapset;
		objmapset = objectIntf.findObjects(objmap);
		for (int z=0; z<objmapset.getRowCount(); z++) {
			objmapset.goToRow(z);
			psoftpillarkey = objmapset.getLongValue("Objects.Key");				
			}
		
		/*
		 * The following is the logic to populate and provision child tables
		 * 
		
		tcResultSet objmapset1;
		objmapset1 = userIntf.getObjects(userKey);
		for(int i = 0; i < objmapset1.getRowCount(); i++)
		{ 
			objmapset1.goToRow(i);
			String objectName = objmapset1.getStringValue("Objects.Name");
			logger.debug("---------Object name " + objectName);
			if(res_Name.equalsIgnoreCase(objectName)) 
			{
				logger.debug(" Looks like we found our Pillar .. ");
				String status = objmapset1.getStringValue("Objects.Object Status.Status");
				status = status.toLowerCase();
				logger.debug( "   Status of the user at our pillar is : " + status);
				psftfinformkey = objmapset1.getLongValue("Process Instance.Key");
				logger.debug("      Process Instance Key of our pillar is : "+ psftfinformkey);
				}
			if (psftfinformkey != 000000000) 
			{
				tcResultSet childFormDef = formIntf.getChildFormDefinition(formIntf.getProcessFormDefinitionKey(psftfinformkey),formIntf.getProcessFormVersion(psftfinformkey));
				String[] childColNames = childFormDef.getColumnNames();
				for (int j=0; j < childColNames.length; j++)
				{
					logger.debug("        Child Form Column : " + childColNames[j] + " : " + childFormDef.getStringValue(childColNames[j]));
					//CU_PEOPLESOFT_USER
					//EOPP_USER
					//PAPP_USER
					}
				plChildFormDefinitionKey = childFormDef.getLongValue("Structure Utility.Child Tables.Child Key");
				tcResultSet childFormData = formIntf.getProcessFormChildData(plChildFormDefinitionKey, psftfinformkey);
				if (!(childFormData.isEmpty()))
				{
					logger.debug("            Searching child table current values");
					for (int z=0; z < childFormData.getRowCount(); z++)
					{
						childFormData.goToRow(z);
						String[] colnames = childFormData.getColumnNames();
						for (int k=0; k < colnames.length; k++)
						{
							logger.debug("             " + z + " : " + colnames[k] + " : " + childFormData.getStringValue(colnames[k]));
							}
						}
					}
				else
				{
					//Hashtable childFormHash = new Hashtable();
					//childFormHash.put("UD_FIN_ROLE_ROLENAME", "CU_PEOPLESOFT_USER");
					//childFormHash.put("UD_FIN_ROLE_ROLENAME", "EOPP_USER");
					//childFormHash.put("UD_FIN_ROLE_ROLENAME", "PAPP_USER");
					//formIntf.addProcessFormChildData(plChildFormDefinitionKey, psftfinformkey, childFormHash);
					
					Map userAttribute = new HashMap();
				    userAttribute.put("UD_FIN_ROLE_ROLENAME", "CU_PEOPLESOFT_USER");
				    logger.debug(" CALLING : " + plChildFormDefinitionKey + " : " + psftfinformkey + " : " + userAttribute);
				    formIntf.addProcessFormChildData(plChildFormDefinitionKey, psftfinformkey, userAttribute);				
					
					}
				break;
				}
			
			}
			*/
		logger.debug("      *** Provisioning : " + userKey + " to " + psoftpillarkey);
		userIntf.provisionObject(userKey,psoftpillarkey);
		logger.debug("End, provisioning of " + userKey + "ID to a given pillar.");
		logger.debug("***CunyProvFinPillar.execute() Exited.");
		}
	catch (tcProvisioningNotAllowedException e)
	{
		e.printStackTrace();
		}
	return response;
	}


public void getConns() throws tcAPIException, tcChallengeNotSetException, tcLoginAttemptsExceededException, tcPasswordResetAttemptsExceededException, tcPasswordExpiredException, tcUserAccountDisabledException, tcUserAccountInvalidException, tcUserAlreadyLoggedInException {
	
	logger.debug("Getting configuration..."); 
	ConfigurationClient.ComplexSetting config = 
	ConfigurationClient.getComplexSettingByPath("Discovery.CoreServer"); 
	logger.debug("Login...");
	 
	Hashtable  env = config.getAllSettings(); 
	logger.debug(System.getProperty("login.username"));
	logger.debug(System.getProperty("login.password"));
	
	ioUtilityFactory = new tcUtilityFactory(env,System.getProperty("login.username"),System.getProperty("login.password"));
	logger.debug("Login...finished");  
	logger.debug("Getting utility interfaces..."); 

	userIntf = (tcUserOperationsIntf)ioUtilityFactory.getUtility("Thor.API.Operations.tcUserOperationsIntf"); 
	objectIntf = (tcObjectOperationsIntf)ioUtilityFactory.getUtility("Thor.API.Operations.tcObjectOperationsIntf");
	formIntf = (tcFormInstanceOperationsIntf)ioUtilityFactory.getUtility("Thor.API.Operations.tcFormInstanceOperationsIntf");
}

public void closeConns() {
	try {
		ioUtilityFactory.close(objectIntf);
		ioUtilityFactory.close(formIntf);
		ioUtilityFactory.close(userIntf);
		ioUtilityFactory.close();
		} catch (RemoteException e) {
			e.printStackTrace();
			}
}


public void execute()throws Exception {
	try {
		logger.debug("***CunyProvFinPillar.execute() Entered ..");
		logger.debug("Startup...");
		logger.debug("Executing provisionToFinancialPillar task from a file");
		fileName = getAttribute("FileName");
		if (getTaskAttributes()) {
			readFile();
			}
		logger.debug("Exiting main task");
		} catch(Exception e){
			e.printStackTrace();
			}
		Iterator it=ls.iterator();
		getConns();
		while(it.hasNext())
		{
			String useridvalue=(String)it.next();
			logger.debug("EMPLD ID from file : " + useridvalue);
			try {
				process(useridvalue);
				} catch(Exception e) {
					e.printStackTrace();
					}
				}
		closeConns();
		System.exit(0);
		}



public void readFile() throws Exception {
	logger.debug("Reading File " + fileName);
	Hashtable userHash = new Hashtable();
	File f = new File (fileName);
	FileReader fr = new FileReader(f);
	BufferedReader in = new BufferedReader(new FileReader(f));
	String str;
	int counter = 1;
	while ((str = in.readLine()) != null) {
		logger.debug("String from file : " + " at [" + counter + "] : " + str);
		ls.add(str);
		counter++;
		}
	in.close();
	fr.close();
	}

public boolean getTaskAttributes() throws Exception { 
	fileName = getAttribute("FileName");
	if (fileName.trim().length()==0) {
		logger.debug("You must supply a value for all fields(exmaple,FileName).");
		return false;
		}
	logger.debug("getTaskAttributes() Parameter Variables passed are:" + fileName);
	return true;
	}


public String getAttribute(String arg) {
	return "a.txt";
	}


public void process(String userId) throws Exception {
	logger.debug("Start Find User Operations .. ");
	Hashtable mhSearchCriteria = new Hashtable();
	long userLongKey = 0000000000;
	mhSearchCriteria.put("Users.User ID", userId);
	tcResultSet moResultSet = userIntf.findUsers(mhSearchCriteria);
	
	if(moResultSet.getRowCount() != 0) {
		for (int i=0; i<moResultSet.getRowCount(); i++) {
			moResultSet.goToRow(i);
			userLongKey = moResultSet.getLongValue("Users.Key");
			logger.debug( "   Users.Key - Long : " + userLongKey );
			logger.debug( "   Users.User ID - String : " + moResultSet.getStringValue("Users.User ID"));
			logger.debug("    Start List All pillars eligible for this user");
			tcResultSet userObjects = userIntf.getObjects(userLongKey);
			for (int j=0; j<userObjects.getRowCount(); j++) {
				userObjects.goToRow(j);
				logger.debug( "     This user is eligible for " + userObjects.getStringValue("Objects.Name") + " with objectlongkey : " + userObjects.getLongValue("Objects.Key"));
				}
			logger.debug("    End List All pillars eligible for this user");
			}
		logger.debug("End Find User Operations.");
		logger.debug("Start, List All available Provisioning Objects .. ");
		HashMap mam = new HashMap();
		mam.put("Objects.Name", "*");
		tcResultSet objset;
		objset = objectIntf.findObjects(mam);
		for (int k=0; k<objset.getRowCount(); k++) {
			objset.goToRow(k);
			}
		logger.debug("   Provision Pillar :  " + objset.getStringValue("Objects.Name") + " objectlongkey : " + objset.getLongValue("Objects.Key"));
		logger.debug("End List All available Provisioning Objects.");
		provisionToFinancialPillar(userLongKey, moResultSet);
		} else {
			logger.debug(userId + " NOT FOUND.");
			}
	}

}
