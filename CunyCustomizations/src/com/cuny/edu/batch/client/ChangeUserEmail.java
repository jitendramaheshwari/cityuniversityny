package com.cuny.edu.batch.client;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.rmi.RemoteException;
import java.util.Hashtable;
import java.util.HashMap; 
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;

import Thor.API.Exceptions.*;

import com.thortech.xl.util.config.ConfigurationClient; 
import Thor.API.Operations.tcUserOperationsIntf; 
import Thor.API.Operations.tcReconciliationOperationsIntf;
import Thor.API.Operations.tcFormInstanceOperationsIntf;
import Thor.API.Operations.tcGroupOperationsIntf;
import Thor.API.Operations.tcITResourceInstanceOperationsIntf;
import Thor.API.Operations.tcLookupOperationsIntf; 
import Thor.API.Operations.tcObjectOperationsIntf;
import Thor.API.Operations.tcOrganizationOperationsIntf; 
import Thor.API.Operations.tcProvisioningOperationsIntf; 
import Thor.API.Base.tcUtilityOperationsIntf;
import Thor.API.Exceptions.tcAPIException;
import Thor.API.tcResultSet;
import Thor.API.tcUtilityFactory;
import com.thortech.util.logging.Logger;

public class ChangeUserEmail {

String fileName;
String password;
String ORGANIZATION_KEY = "Organizations.Key";
String ORGANIZATION_NAME = "Organizations.Organization Name";
String USER_FIRST_NAME = "Users.First Name";
String USER_LAST_NAME = "Users.Last Name";
String USER_EMAIL = "Users.Email";
List<String> ls=new ArrayList<String>();
protected tcUtilityFactory ioUtilityFactory;
protected tcReconciliationOperationsIntf reconIntf;
protected tcUserOperationsIntf userIntf;
protected tcFormInstanceOperationsIntf formIntf; 
protected tcGroupOperationsIntf groupIntf; 
protected tcITResourceInstanceOperationsIntf itresourceIntf;
protected tcLookupOperationsIntf lookupIntf; 
protected tcObjectOperationsIntf objectIntf;
protected tcOrganizationOperationsIntf orgIntf; 
protected tcProvisioningOperationsIntf provIntf; 
protected tcUtilityOperationsIntf tcUtilIntf;


private static Logger log = Logger.getLogger("CUNY.CUSTOM");
	
	/**
	 * @param args
	 */
public static void main(String[] args) {
	ChangeUserEmail cuemail = new ChangeUserEmail();
	try {
		cuemail.execute();	
	} catch (Exception e){
		e.printStackTrace();
	}
	}

public void execute()throws Exception {
try {
	log.debug("***ChangeUserEmail.execute() Entered ..");
	log.debug("Startup..."); 
	log.debug("Executing ChangeUserEmail task from a file");
	fileName = getAttribute("FileName");
	if (getTaskAttributes()) {
			readFile();
			}
	log.debug("Exiting main task");
	} catch(Exception e){
					 e.printStackTrace();
					}
	
	Iterator it=ls.iterator();
	getConns();
	int rec_cnt = 0;
	while(it.hasNext())
	{
		rec_cnt++;
		String record = (String)it.next();
		if (record.trim() != null && record.trim() != "" && record.trim().split(",").length == 2) {
			String[] values = record.trim().split(",");
			String EMPLID = values[0].trim();
			String USR_EMAIL = values[1].trim();
						
			log.debug("EMPLID -> " + EMPLID + " USR_EMAIL -> " + USR_EMAIL);
			if (EMPLID != "" && USR_EMAIL != "") {
				doIt(EMPLID, USR_EMAIL);
			} else {
				log.debug("  ERROR : FOUND invalid values found in the RECORD at position [" + rec_cnt + "] SKIPPING");
			}
			} else {
				log.debug("ERROR : Invalid RECORD [" + record + "] found at position [" + rec_cnt + "] SKIPPING.");
			}
	}
		
	closeConns();
	System.exit(0);
		}

public void doIt(String emplid, String usremail) {
	try {		
		process(emplid, usremail);		
		} catch(Exception e){
			e.printStackTrace();
			}
		}


public void getConns() throws tcAPIException, tcChallengeNotSetException, tcLoginAttemptsExceededException, tcPasswordResetAttemptsExceededException, tcPasswordExpiredException, tcUserAccountDisabledException, tcUserAccountInvalidException, tcUserAlreadyLoggedInException {
	
	log.debug("Getting configuration..."); 
	ConfigurationClient.ComplexSetting config = 
	ConfigurationClient.getComplexSettingByPath("Discovery.CoreServer"); 
	log.debug("Login...");
	 
	Hashtable  env = config.getAllSettings(); 
	log.debug(System.getProperty("login.username"));
	log.debug(System.getProperty("login.password"));
	/*
	Enumeration en = env.keys();
	while (en.hasMoreElements()) {
		String keyer = (String)en.nextElement();
		log.debug(keyer + " : " + env.get(keyer));
	} 
	*/

	ioUtilityFactory = new tcUtilityFactory(env,System.getProperty("login.username"),System.getProperty("login.password"));
	log.debug("Login...finished");  
	log.debug("Getting utility interfaces..."); 

	userIntf = (tcUserOperationsIntf)ioUtilityFactory.getUtility("Thor.API.Operations.tcUserOperationsIntf"); 
	objectIntf = (tcObjectOperationsIntf)ioUtilityFactory.getUtility("Thor.API.Operations.tcObjectOperationsIntf");
	formIntf = (tcFormInstanceOperationsIntf)ioUtilityFactory.getUtility("Thor.API.Operations.tcFormInstanceOperationsIntf");
}

public void closeConns() {
	try {
		ioUtilityFactory.close(objectIntf);
		ioUtilityFactory.close(formIntf);
		ioUtilityFactory.close(userIntf);
		ioUtilityFactory.close();
		} catch (RemoteException e) {
			e.printStackTrace();
			}
}

public boolean changeOrg(long userKey, tcResultSet userSet) throws tcAPIException, tcUserAccountDisabledException, tcPasswordResetAttemptsExceededException, tcLoginAttemptsExceededException, tcUserAccountInvalidException, tcUserAlreadyLoggedInException, tcNotAtomicProcessException, tcFormNotFoundException, tcProcessNotFoundException, tcInvalidValueException, tcRequiredDataMissingException 
{
log.debug((new StringBuilder("  changeOrg() Parameter Variables passed are:userKey=[")).append(userKey).append("]").toString());
boolean response = false;


String org_key = "27";
String org_name = "HTR01";
String status = "Active";


try {
            
    HashMap fieldsToUpdate = new HashMap();
    fieldsToUpdate.put(ORGANIZATION_KEY, org_key);
    fieldsToUpdate.put(ORGANIZATION_NAME, org_name);

    if (userSet != null) { 
    	log.debug("  Updating " +  userKey + " with new ORG Key [" + org_key + "] and ORG Name [" +   org_name + "]");
    	userIntf.updateUser(userSet, fieldsToUpdate);    	
    }
        
      } catch(tcUserNotFoundException e)
      {
    	  log.debug("  User not found");
    	  e.printStackTrace();
    	  } 
      catch (tcStaleDataUpdateException e) {
		  e.printStackTrace();
		  }
         
return response;
}

public boolean changeUsrEmail(long userKey, tcResultSet userSet, String usremail) throws tcAPIException, tcUserAccountDisabledException, tcPasswordResetAttemptsExceededException, tcLoginAttemptsExceededException, tcUserAccountInvalidException, tcUserAlreadyLoggedInException, tcNotAtomicProcessException, tcFormNotFoundException, tcProcessNotFoundException, tcInvalidValueException, tcRequiredDataMissingException 
{
log.debug((new StringBuilder("  changeUsrEmail() Parameter Variables passed are:userKey=[")).append(userKey).append("]").toString());

boolean response = false;

String usr_email = usremail;

try {
            
    HashMap fieldsToUpdate = new HashMap();    
    fieldsToUpdate.put(USER_EMAIL, usr_email);
    if (userSet != null) { 
    	log.debug("  Updating " +  userKey + " to new EMAIL [" + usr_email + "]");
    	userIntf.updateUser(userSet, fieldsToUpdate);    	
    }
        
      } catch(tcUserNotFoundException e)
      {
    	  log.debug("  User not found");
    	  e.printStackTrace();
    	  } 
      catch (tcStaleDataUpdateException e) {
		  e.printStackTrace();
		  }
         
return response;
}

public boolean dumpUsrEmail(long userKey, tcResultSet userSet) throws tcAPIException, tcUserAccountDisabledException, tcPasswordResetAttemptsExceededException, tcLoginAttemptsExceededException, tcUserAccountInvalidException, tcUserAlreadyLoggedInException, tcNotAtomicProcessException, tcFormNotFoundException, tcProcessNotFoundException, tcInvalidValueException, tcRequiredDataMissingException 
{
log.debug((new StringBuilder("dumpUsrEmail() : userKey=[")).append(userKey).append("]").toString());
boolean response = false;

try {
    if (userSet != null) {	
    	log.debug("  User [" + userSet.getStringValue(USER_FIRST_NAME) + " " + userSet.getStringValue(USER_LAST_NAME) + "] has EMAIL Address[" + userSet.getStringValue(USER_EMAIL) + "]");
    	
//    	String[] userColumnNames = userSet.getColumnNames();
//        for(int n = 0; n < userColumnNames.length; n++) {
//    		log.debug(userColumnNames[n] + " = " + userSet.getStringValue(userColumnNames[n]));
//    		}
        }
    } catch(tcColumnNotFoundException e) {
    	  e.printStackTrace();
      }
         
return response;
}


public void readFile() throws Exception {
    log.debug("Reading File " + fileName);
    Hashtable userHash = new Hashtable();
    
    File f = new File (fileName);
    FileReader fr = new FileReader(f);	
    BufferedReader in = new BufferedReader(new FileReader(f));
    String str;
    int counter = 1;
    
    while ((str = in.readLine()) != null) {
	log.debug("Record from file : " + " at [" + counter + "] : " + str);
	ls.add(str);
	counter++;
	}		
    in.close();
    fr.close();
}

public boolean getTaskAttributes() throws Exception { 

   fileName = getAttribute("FileName");
   if (fileName.trim().length()==0) {
   	log.debug("You must supply a value for all fields(example,FileName).");
   return false;
	}
   log.debug("getTaskAttributes() Parameter Variables passed are:" + fileName);
   return true;
}


public String getAttribute(String arg) {

return "a.txt";

}


public void process(String emplid, String usremail) throws tcAPIException, tcChallengeNotSetException, tcLoginAttemptsExceededException, tcPasswordResetAttemptsExceededException, tcPasswordExpiredException, tcUserAccountDisabledException, tcUserAccountInvalidException, tcUserAlreadyLoggedInException, tcNotAtomicProcessException, tcFormNotFoundException, tcProcessNotFoundException, tcInvalidValueException, tcRequiredDataMissingException 
{
	log.debug("  Searching User with EMPLID [" + emplid + "]"); 
	Hashtable mhSearchCriteria = new Hashtable(); 
	long userLongKey = 0000000000;
	mhSearchCriteria.put("Users.User ID", emplid);

	try {
	tcResultSet moResultSet = userIntf.findUsers(mhSearchCriteria);
	
	if(moResultSet.getRowCount() != 0) {
		moResultSet.goToRow(0);
		userLongKey = moResultSet.getLongValue("Users.Key");
		
		
		log.debug("  " + emplid + " is FOUND with long key " + userLongKey);
		long startTime = System.currentTimeMillis();
		dumpUsrEmail(userLongKey, moResultSet);
		
		changeUsrEmail(userLongKey, moResultSet, usremail);
		long endTime = System.currentTimeMillis();		
		log.debug("  Total elapsed time in execution of method ChangeUserEmail() : "+ (endTime-startTime));	
		
	} else {
		log.debug("  " + emplid + " NOT FOUND.");
	}
	
		
	} catch (tcColumnNotFoundException e) {
			e.printStackTrace();
		} 
			   
		   }

public void process(String emplid) throws tcAPIException, tcChallengeNotSetException, tcLoginAttemptsExceededException, tcPasswordResetAttemptsExceededException, tcPasswordExpiredException, tcUserAccountDisabledException, tcUserAccountInvalidException, tcUserAlreadyLoggedInException, tcNotAtomicProcessException, tcFormNotFoundException, tcProcessNotFoundException, tcInvalidValueException, tcRequiredDataMissingException 
{
	log.debug("  Searching User with EMPLID [" + emplid + "]");
	Hashtable mhSearchCriteria = new Hashtable(); 
	long userLongKey = 0000000000;
	mhSearchCriteria.put("Users.User ID", emplid);

	try {
	tcResultSet moResultSet = userIntf.findUsers(mhSearchCriteria);
	
	if(moResultSet.getRowCount() != 0) {
		moResultSet.goToRow(0);
		userLongKey = moResultSet.getLongValue("Users.Key");
		
		
		log.debug("  " + emplid + " is FOUND with long key " + userLongKey);
		long startTime = System.currentTimeMillis();
		dumpUsrEmail(userLongKey, moResultSet);
		
		changeOrg(userLongKey, moResultSet);
		long endTime = System.currentTimeMillis();		
		log.debug("  Total elapsed time in execution of method ChangeUserEmail() : "+ (endTime-startTime));	
		
	} else {
		log.debug("  " + emplid + " NOT FOUND.");
	}
	
		
	} catch (tcColumnNotFoundException e) {
			e.printStackTrace();
		} 
			   
		   }
	}	

	
	

