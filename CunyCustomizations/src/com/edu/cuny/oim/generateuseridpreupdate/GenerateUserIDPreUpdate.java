package com.edu.cuny.oim.generateuseridpreupdate;


import Thor.API.Operations.tcITResourceInstanceOperationsIntf;
import Thor.API.Operations.tcUserOperationsIntf;
import Thor.API.tcResultSet;

import com.sun.jndi.ldap.ctl.PagedResultsControl;

import com.thortech.util.logging.Logger;
import com.thortech.xl.util.logging.LoggerMessages;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.Control;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;

import oracle.iam.platform.Platform;


public class GenerateUserIDPreUpdate {
	
	
	 com.thortech.xl.dataaccess.tcDataProvider poDataBase;
	  private final int maxNameLength = 255;
	  
	  private static final String XL_CHARS_NOT_ALLOWED_IN_DN    = "[\' ();!@#$%^&:\"<>*+=\\|?/,]";              //",| |\'|-";
	    private static final String XL_NUMERIC_CHARS              = "[1234567890]";
	    private static final String XL_SPECIAL_CHARS              = "[!@#$%^&*()]";
	    
		private static final String CURRENT_CLASS_NAME = "CUNY:GenerateUserID";
		private static final String GETUSERLOGIN_METHOD = "Get_User_Login:--> ";
		private static final String FIND_OID_USER = "Find_OID_User:--> ";
		
		private tcUserOperationsIntf userOps;
	  
		
    private static Logger log = Logger.getLogger("CUNY.CUSTOM");
    
    
    
	  public String getUserLogin (String oidserver, String fName, String lName, String emplID, 
			  String containerDN,String userID,String userKey) {
		    
		  /*Compare First Name & Last Name from Entity adapter and USR table. If (FNEntity!=FNUSR or LNEntity!=LNUSR) , 
          then only go ahead. Otherwise no need to execute this entity adapter at all */
   
   long lUserKey = Long.valueOf(userKey).longValue();
   String fNameExisting = getUserAttrValue(lUserKey, "Users.First Name");
   log.info(CURRENT_CLASS_NAME+GETUSERLOGIN_METHOD+"------> -->fNameExisting "+fNameExisting);
   String lNameExisting = getUserAttrValue(lUserKey, "Users.Last Name");
   log.info(CURRENT_CLASS_NAME+GETUSERLOGIN_METHOD+"------> -->lNameExisting "+lNameExisting);
   log.info(CURRENT_CLASS_NAME+GETUSERLOGIN_METHOD+"------> -->fName "+fName);
   log.info(CURRENT_CLASS_NAME+GETUSERLOGIN_METHOD+"------> -->lName "+lName);
   
   if((!fNameExisting.equalsIgnoreCase(fName))||(!lNameExisting.equalsIgnoreCase(lName))){
		  
		  String uniqueUserID = new String();
	        String oidSearchResult = new String();
	        
	        try {
	            log.info(CURRENT_CLASS_NAME+GETUSERLOGIN_METHOD+"(I)================= BEGIN GenerateUserID.getUserLogin ( Last 2 digits) ==============================");
	            
	            log.info(CURRENT_CLASS_NAME+GETUSERLOGIN_METHOD+"GenerateUserID.getUserLogin------>Input-->oidserver"+oidserver);
	            log.info(CURRENT_CLASS_NAME+GETUSERLOGIN_METHOD+"GenerateUserID.getUserLogin------>Input-->fName"+fName);
	            log.info(CURRENT_CLASS_NAME+GETUSERLOGIN_METHOD+"GenerateUserID.getUserLogin------>Input-->lName"+lName);
	            log.info(CURRENT_CLASS_NAME+GETUSERLOGIN_METHOD+"GenerateUserID.getUserLogin------>Input-->emplID"+emplID);
	            log.info(CURRENT_CLASS_NAME+GETUSERLOGIN_METHOD+"GenerateUserID.getUserLogin------>Input-->containerDN"+containerDN);
	            log.info(CURRENT_CLASS_NAME+GETUSERLOGIN_METHOD+"GenerateUserID.getUserLogin------>Input-->userID"+userID);
	            Hashtable resourceHash = getITResource(oidserver);
	 
	            String serverAddress    = resourceHash.get("Server Address").toString();
	            String rootContext      = resourceHash.get("Root DN").toString();
	            String adminID          = resourceHash.get("Admin Id").toString();
	            String adminPWD         = resourceHash.get("Admin Password").toString();
	            String port             = resourceHash.get("Port").toString();
	            String sslFlag          = resourceHash.get("SSL").toString();
	            
	            
	                       
	             
	            uniqueUserID                    = trimString (fName + "." + lName, maxNameLength);
	                                                    
	            String firstNamedotlastName     = uniqueUserID;   
	            int startingSeqNoOfEmplID       = 2;       
	            int maxEmplIDLengthConsidered   = 11;      

	            boolean userExists  = true;     
	            int idx      = startingSeqNoOfEmplID;               
	            int idxMax   = emplID.length();            
	            if  ( emplID.length() > maxEmplIDLengthConsidered) { idxMax = maxEmplIDLengthConsidered; }         
	            
	           
		          
	             
	           uniqueUserID = firstNamedotlastName.concat( emplID.substring(emplID.length()-idx) );    
	           
	           log.info(CURRENT_CLASS_NAME+GETUSERLOGIN_METHOD+"------>userid -->uniqueUserID 1"+uniqueUserID);
	           
	           	           
	           
	           if(!userID.equals(""))
	           {
	           if(uniqueUserID.equalsIgnoreCase(userID))
	           {
	        	   
	        	    log.info(CURRENT_CLASS_NAME+GETUSERLOGIN_METHOD+"------------------------------------------");
					log.info(CURRENT_CLASS_NAME+GETUSERLOGIN_METHOD+"Generated Userid equals to existing userid");
					log.info(CURRENT_CLASS_NAME+GETUSERLOGIN_METHOD+"------------------------------------------");
	
					log.info(CURRENT_CLASS_NAME+GETUSERLOGIN_METHOD+"---Inside equal case1--->Input-->userID"+userID);
					log.info(CURRENT_CLASS_NAME+GETUSERLOGIN_METHOD+"---uniqueUserID case1--->GenerateduniqueUserID"+uniqueUserID);
	        	   
	        	   return uniqueUserID;
	           }
	           }
	           
	           
	           
	           oidSearchResult =  findOIDUser( serverAddress,port,rootContext,adminID,adminPWD,sslFlag,uniqueUserID);	           
	           
	                      
	           if ((oidSearchResult != null) && (!oidSearchResult.trim().equals("")))  {
	        	   
	        	   
	                    if (oidSearchResult.equals("USER_NOT_FOUND")) {
	                        log.info(CURRENT_CLASS_NAME+GETUSERLOGIN_METHOD+"  OID Userid:"+ uniqueUserID + " Not exists in OID"); 
	                    }
	                    else {
	                    	
	                    	 log.info(CURRENT_CLASS_NAME+GETUSERLOGIN_METHOD+"User already exist in OID");
	                    	 log.info(CURRENT_CLASS_NAME+GETUSERLOGIN_METHOD+"Generating Unique userID");
	                    	
	                        idx++;
	                        while ((userExists) && ( idx <= idxMax)){   
	                        
	                               uniqueUserID = firstNamedotlastName.concat(  emplID.substring(emplID.length()- idx)  ) ;   
	                               
	                               log.info(CURRENT_CLASS_NAME+GETUSERLOGIN_METHOD+"GenerateUserID.getUserLogin------>userid -->uniqueUserID 2"+uniqueUserID);
	                	           

	            
	                               if(!userID.equals(""))
	                	           {
	                               if(uniqueUserID.equalsIgnoreCase(userID))
	                	        	   
	                               {
	                	        	
	                            	log.info(CURRENT_CLASS_NAME+GETUSERLOGIN_METHOD+"------------------------------------------");
	               					log.info(CURRENT_CLASS_NAME+GETUSERLOGIN_METHOD+"Generated Userid equals to existing userid");
	               					log.info(CURRENT_CLASS_NAME+GETUSERLOGIN_METHOD+"------------------------------------------");

	            					log.info(CURRENT_CLASS_NAME+GETUSERLOGIN_METHOD+"GenerateUserID.getUserLogin---Inside equal case2--->Input-->userID"+userID);
	            					log.info(CURRENT_CLASS_NAME+GETUSERLOGIN_METHOD+"GenerateUserID.getUserLogin---uniqueUserID case2--->uniqueUserID"+uniqueUserID);
	                            	   
	                            	   return uniqueUserID;
	                               }
	                	           }
	                              
	                               
	                                if (  ( findOIDUser( serverAddress,port,rootContext,adminID,adminPWD,sslFlag,uniqueUserID) ).equals("USER_FOUND"  )) { 
	                                    idx++;
	                                } else {
	                                     userExists = false;
	                                }
	                        }
	                 
	                        
	                        if (userExists) {
	                            uniqueUserID = firstNamedotlastName.concat(".").concat("00000000000"); 
	                        }
	                    }
	            } 
	            else {
	             
	            }
	            log.info(CURRENT_CLASS_NAME+GETUSERLOGIN_METHOD+"(I)================= END ==============================");
	            
	            log.info(CURRENT_CLASS_NAME+GETUSERLOGIN_METHOD+"------>userid -->uniqueUserID Before return"+uniqueUserID);
		           
	                
	                
	        } catch (Exception e) {
	            log.error(LoggerMessages.getMessage(CURRENT_CLASS_NAME+GETUSERLOGIN_METHOD+"ErrorMethodDebug", "GenerateUserID/getUserLogin: Exception occured- ",  e.getMessage()),  e);
	            e.printStackTrace();
	        }
	  
	        return uniqueUserID;
	    }
	  return userID;
	  }

	  

	    /**
	     * This method truncates the inputStr per length specified, trim off spaces, apostrophe ('), dashes
	     * @param inputStr
	     * @param length
	     * @return
	     */
	    private String trimString ( String inputStr, int length) {
	        
	        String tmp = this.getRidOfSomeChars(inputStr,GenerateUserIDPreUpdate.XL_CHARS_NOT_ALLOWED_IN_DN);
	        
	        if ((tmp!=null) && (tmp.length()>length)) {
	            tmp = tmp.substring(0,length);    
	        }
	        
	        return tmp;
	    }
    private Hashtable getITResource(String itResource) {
        Hashtable resource = new Hashtable();
        try {
            tcITResourceInstanceOperationsIntf itResOper
                    = Platform.getService(tcITResourceInstanceOperationsIntf.class);
            HashMap<String, String> searchcriteria
                    = new HashMap<String, String>();
            searchcriteria.put("IT Resources.Name", itResource);
            log.info("IT Resource Name " + itResource);
            tcResultSet resultSet
                    = itResOper.findITResourceInstances(searchcriteria);
            log.info("Result Set Count " + resultSet.getTotalRowCount());
            tcResultSet itResSet
                    = itResOper.getITResourceInstanceParameters(resultSet.getLongValue("IT Resources.Key"));
            log.info("itResuletSet Count " + itResSet.getRowCount());
            for (int i = 0; i < itResSet.getRowCount(); i++) {
                itResSet.goToRow(i);
                String paramName
                        = itResSet.getStringValue("IT Resources Type Parameter.Name");
                String paramVal
                        = itResSet.getStringValue("IT Resource.Parameter.Value");
                log.info("Parameter Name" + paramName + " VALUE: " + paramVal);
                resource.put(paramName, paramVal);
            }
        } catch (Exception e) {
            log.error("Exception ", e);

        }
        return resource;
    }

	    private String getRidOfSomeChars(String original, String regExp) {
	        Pattern p = Pattern.compile(regExp);
	        Matcher m = p.matcher("");
	        m.reset(original);
	        return m.replaceAll("");
	    }
	    
	    private String findOIDUser(String serverName,String portNo,String rootContext,String principalDN,String principalPass,String sslFlag,String uniqueUserID)
	    {
	    	
	    	String response = "";
	    	
			try
			{
			
			log.info(CURRENT_CLASS_NAME+GETUSERLOGIN_METHOD+"------->serverName = " + serverName);
			log.info(CURRENT_CLASS_NAME+GETUSERLOGIN_METHOD+"------->portNo = " + portNo);
			log.info(CURRENT_CLASS_NAME+GETUSERLOGIN_METHOD+"------->rootContext = " + rootContext);
			log.info(CURRENT_CLASS_NAME+GETUSERLOGIN_METHOD+"------->principalDN = " + principalDN);
			log.info(CURRENT_CLASS_NAME+GETUSERLOGIN_METHOD+"------->uniqueUserID = " + uniqueUserID);
			
			
			String pSearchBase =new String();
			
			pSearchBase = rootContext;
		
			
			log.info(CURRENT_CLASS_NAME+GETUSERLOGIN_METHOD+"------->sslFlag = " + sslFlag);
			log.info(CURRENT_CLASS_NAME+GETUSERLOGIN_METHOD+"===================================\n");

			String contextFactory = "com.sun.jndi.ldap.LdapCtxFactory";

			String  providerURL = new String("ldap://" + serverName + ":" + portNo);


			Hashtable oEnv = new Hashtable();
			oEnv.put(Context.PROVIDER_URL,providerURL);
			oEnv.put(Context.INITIAL_CONTEXT_FACTORY,contextFactory);
			oEnv.put(Context.SECURITY_PRINCIPAL, principalDN);
			oEnv.put(Context.SECURITY_CREDENTIALS, principalPass);

			oEnv.put(Context.SECURITY_AUTHENTICATION, "simple");

			if (sslFlag.equalsIgnoreCase("true")){
				oEnv.put(Context.SECURITY_PROTOCOL,"ssl");
			}
			DirContext ctx = new InitialDirContext(oEnv);
			
			 LdapContext ldapCtx;
				
				ldapCtx = new InitialLdapContext(oEnv, null);
			
			SearchControls searchcontrols = new SearchControls();
			searchcontrols.setCountLimit(0); // to return all the results
			searchcontrols.setSearchScope(SearchControls.SUBTREE_SCOPE);
						
				
				
				Vector vector = new Vector();
				SearchResult searchresult;
				
				
				
				
				String searchFilter = "(&(objectClass=person)(uid="+uniqueUserID+") )";
				
				
log.info(CURRENT_CLASS_NAME+GETUSERLOGIN_METHOD+"rootcontext-------->"+rootContext);
log.info(CURRENT_CLASS_NAME+GETUSERLOGIN_METHOD+"searchFilter-------->"+searchFilter);
log.info(CURRENT_CLASS_NAME+GETUSERLOGIN_METHOD+"searchcontrols-------->"+searchcontrols);


String searchAttributes[] = { "uid" };

	searchcontrols.setReturningAttributes(searchAttributes);


	int pageSize = 10000;
	byte[] cookie = null;
	Control[] ctls = new PagedResultsControl[] { new PagedResultsControl(pageSize) };
	ldapCtx.setRequestControls(ctls);
				
				for (NamingEnumeration namingenumeration = ldapCtx.search(rootContext,
						searchFilter, searchcontrols); namingenumeration
						.hasMore(); vector.addElement(searchresult)) {
					searchresult = (SearchResult) namingenumeration.next();
					searchresult.setRelative(true);

				

				}// end of for
				
				log.info(CURRENT_CLASS_NAME+GETUSERLOGIN_METHOD+"vector values------>"+vector);
				
				if(vector.size()>0)
					response = "USER_FOUND";
				else
					response = "USER_NOT_FOUND";
				
				ldapCtx.close();
				ctx.close();
			}
			catch (Exception e)
			{
				log.error(CURRENT_CLASS_NAME+GETUSERLOGIN_METHOD+"Error in searching ldap::"+e.getMessage());
				e.printStackTrace();
				
			}

	    	
	    	
	    	return response;
	    }
	    
	   /* public static Connection getDatabaseConnection() throws InstantiationException, IllegalAccessException, ClassNotFoundException {

			final String methodName = "getDatabaseConnection";
			Connection connection = null;
			try {
				Class.forName("oracle.jdbc.driver.OracleDriver").newInstance();
				connection = DriverManager.getConnection("jdbc:oracle:thin:@sdimoim.cuny.edu:1521:ebsid2",GenericConstants.OIM_ORACLE_DATABASE_USER_NAME, GenericConstants.OIM_ORACLE_DATABASE_PASSWORD);
				//connection = Platform.getOperationalDS().getConnection();
				LOGGER.info(className,methodName,"Got Oracle JDBC Connection for OIM Database");
				return connection;
			} catch (SQLException e) {
				LOGGER.error(className,methodName,"Error while getting OIM Database Connection -"+ e.getMessage());

			}
			return connection;
		}*/
	    
	    private String getUserAttrValue(long userKey, String UDFFieldName){
	        
	        String retString = null;
	        try {
	        
	            HashMap hm = new HashMap();
	            hm.put ("Users.Key", new Long (userKey).toString());
	            tcResultSet userRS = userOps.findUsers(hm);
	            if ( userRS.getRowCount()>0){
	               retString = userRS.getStringValue(UDFFieldName);
	            }
	        }
	        catch(Exception e){
	            log.error("OIDUtils.getUserAttrValue: Exception occured ", e);
	        }
	        return retString;
	    }

}
