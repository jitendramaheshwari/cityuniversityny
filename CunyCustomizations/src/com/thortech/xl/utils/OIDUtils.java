package com.thortech.xl.utils;

import Thor.API.Operations.tcFormInstanceOperationsIntf;
import Thor.API.Operations.tcITResourceInstanceOperationsIntf;
import Thor.API.Operations.tcLookupOperationsIntf;
import Thor.API.Operations.tcObjectOperationsIntf;
import Thor.API.Operations.tcUserOperationsIntf;
import Thor.API.tcResultSet;

import com.thortech.util.logging.Logger;
import com.thortech.xl.util.logging.LoggerMessages;

import edu.cuny.oim.OIDFunctions;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import oracle.iam.platform.Platform;


/**
 *	OID Utility class
 */

public class OIDUtils {

    
    private tcLookupOperationsIntf lookupOps;
    private tcUserOperationsIntf userOps;
    private tcObjectOperationsIntf objIntf;
    private tcFormInstanceOperationsIntf formOps;
    private tcITResourceInstanceOperationsIntf itrOps;
	
    private static final String XL_OBJECT_STATUS_PROVISIONED  = "Provisioned";
    private static final String XL_OBJECT_STATUS_ENABLED      = "Enabled";  
    private final static String XL_OIDUSER_RO                 = "OID User";
        
    private static final String XL_CHARS_NOT_ALLOWED_IN_DN    = "[\' ();!@#$%^&:\"<>*+=\\|?/,]";              //",| |\'|-";
    private static final String XL_NUMERIC_CHARS              = "[1234567890]";
    private static final String XL_SPECIAL_CHARS              = "[!@#$%^&*()]";
    
    private final int maxNameLength = 255;
    
    private static Logger log = Logger.getLogger("CUNY.CUSTOM");
        
    /**
    * This method retrieves the value of the specified UDF form field
    *
    * @param userKey               :User Key from User Definition Form
    * @param UDFFieldName          :User Field name from User Definition Form
    * @return
    *
    */
    private String getUserAttrValue(long userKey, String UDFFieldName){
    
        String retString = null;
        try {
        
            HashMap hm = new HashMap();
            hm.put ("Users.Key", new Long (userKey).toString());
            tcResultSet userRS = userOps.findUsers(hm);
            if ( userRS.getRowCount()>0){
               retString = userRS.getStringValue(UDFFieldName);
            }
        }
        catch(Exception e){
            log.error("OIDUtils.getUserAttrValue: Exception occured " + e);
        }
        return retString;
    }
    
    /**
     * This method truncates the inputStr per length specified, trim off spaces, apostrophe ('), dashes
     * @param inputStr
     * @param length
     * @return
     */
    private String trimString ( String inputStr, int length) {
        
        String tmp = this.getRidOfSomeChars(inputStr,OIDUtils.XL_CHARS_NOT_ALLOWED_IN_DN);
        
        if ((tmp!=null) && (tmp.length()>length)) {
            tmp = tmp.substring(0,length);    
        }
        
        return tmp;
    }


    private String getRidOfSomeChars(String original, String regExp) {
        Pattern p = Pattern.compile(regExp);
        Matcher m = p.matcher("");
        m.reset(original);
        return m.replaceAll("");
    }
    

    /**
     * Last Updated 1/29/09 
     * - Append last 2 digits of emplID first, then do collision check, increments if necessary
     * - remove 'dot' after last name
     * 
     * Last Updated 1/14/09 - renamed to getUserLogin_startwithlast4digits() - to be removed later
     * - do collision check, then append last 4 digits of emplID, increments if necessary
     * - leave 'dot' after last name
     * 
     * This method is called by the (pre-insert) entity adapter of the Users Object
     * This program generates a unique user id by querying the OID repository for any collisions.
     * 
     * 
     * Unique user id generation algorithm:
     * 
     * This method composes a unique user id in the format of firstname.lastnamecollisionNo  (e.g. Jason.Stills78)
     *      firstname:
     *          is the entire length of the parameter firstname 
     *      lastname:
     *          is the entire length of the parameter lastname,, up to 256 characters
     *          space, apostrophe(or single quote), dash are trimmed from last name- see trimString()
     *      collisionNo: 
     *          initial attempt: use the last 2 digits of the emplID
     *          then use the last 3 digits of the emplID if collision occurs 
     *          then use the last 4 digits and so forth.
     *          
     *     Exceptions:          
     *         If the length of  'emplID' is greater than 11 characters in length,
     *              use only the first 11 characters.
     *              
     *         If the length of  'emplID' is less than 2 characters in length,
     *               set currID = 'cn=John.SmithemplID' if it is unique, else
     *               set currID = 'cn=John.Smith0000' 
     *               
     *         IF emplID is 2 digits in length 
     *               set currID = 'cn=John.SmithemplID' if it is unique, else                  
     *               set currID = 'cn=John.Smith00000000000'    
     *               
     *         When all fails, use 'firstname.lastname.00000000000'
     *               
    * 
     * @param containerDN
     *   DEV: cn=John.Smith.012, cn=public, cn=Users, dc=cuny, dc=edu ->  input parameter expected ->  pContainerDN =  "cn=public" 
     *   TST: cn=John.Smith.012, cn=Users, dc=cuny, dc=edu            ->  input parameter expected ->  pContainerDN =  ""      
     * @param 
     *  
     * @return uniqueUserID
     *
     **/ 
    public String getUserLogin (String oidserver, String fName, String lName, String emplID, String containerDN) {
    
        String uniqueUserID = new String();   
        try {
            log.info("(I)================= BEGIN OIDUtils.getUserLogin ( Last 2 digits) ==============================");
            
            Hashtable resourceHash = getITResource(oidserver);
 
            String serverAddress    = resourceHash.get("Server Address").toString();
            String rootContext      = resourceHash.get("Root DN").toString();
            String adminID          = resourceHash.get("Admin Id").toString();
            String adminPWD         = resourceHash.get("Admin Password").toString();
            String port             = resourceHash.get("Port").toString();
            String sslFlag          = resourceHash.get("SSL").toString(); 
            OIDFunctions oid;                              
             
            uniqueUserID                    = trimString (fName + "." + lName, maxNameLength);
            //fName.concat(".").concat(trimString (lName, maxNameLength));  //  john.smith                                        
            String firstNamedotlastName     = uniqueUserID;   
            String ldapUserDNPrefix         = "cn";        
            String existingEmplId           = null;
            int startingSeqNoOfEmplID       = 2;       // The first fragment of emplID to be considered: last 2 digits
            int maxEmplIDLengthConsidered   = 11;      // The max fragment of emplID to be considered: last 11 digits

            boolean userExists  = true;     
            int idx      = startingSeqNoOfEmplID;               
            int idxMax   = emplID.length();            // The minimum of (maxEmplIDLengthConsidered=11 or (emplID.length())
            if  ( emplID.length() > maxEmplIDLengthConsidered) { idxMax = maxEmplIDLengthConsidered; }         
            
            
            // Here, emplID is at least 2 digits in length
            // First: just do collision check of 'firstNamedotLastNamelast2digits', use this as unique user id if no collision  
            //      : increments if necessary
            //   IF emplID is 2 digit  & collision occurs for 'firstNamedotLastNamelast2digits'  
            //                          -> set currID = 'cn=John.Smith00000000000'
           uniqueUserID = firstNamedotlastName.concat( emplID.substring(emplID.length()-idx) );    
           oid = new OIDFunctions(serverAddress, port, rootContext, adminID, adminPWD, sslFlag);   
           existingEmplId = oid.getEmplId(containerDN, ldapUserDNPrefix+"="+uniqueUserID);
                       
           if ((existingEmplId != null) && (!existingEmplId.trim().equals("")))  {
                    if (existingEmplId.equals(emplID)) {
                        log.info("  emplid:"+ emplID + " already exists in OID"); 
                    }
                    else {
                        idx++;
                        while ((userExists) && ( idx <= idxMax)){   
                        
                               uniqueUserID = firstNamedotlastName.concat(  emplID.substring(emplID.length()- idx)  ) ;              
                               oid = new OIDFunctions(serverAddress, port, rootContext, adminID, adminPWD, sslFlag);  
                                if (  ( oid.getAttribute(containerDN,ldapUserDNPrefix+"="+uniqueUserID) ).equals("USER_FOUND"  )) { 
                                    //log.info ("  getUserLogin : (COLLISION FOUND!!) : idx=" + idx + "  uniqueUserID=" + uniqueUserID);  
                                    idx++;
                                } else {
                                    //log.info ("  getUserLogin : (NO COLLISION!!) : idx=" + idx + "  uniqueUserID=" + uniqueUserID);                                 
                                     userExists = false;
                                }
                        }
                 
                        // On the very rare occasion, there's a collisiion of firstname+lastname+emplID, or emplID.length>11
                        if (userExists) {
                            uniqueUserID = firstNamedotlastName.concat(".").concat("00000000000"); 
                        }
                    }
            }// Collision found 
            else {
                //log.info ("  getUserLogin :(NO COLLISION!!) (idx=2)  uniqueUserID=" + uniqueUserID);
            }
            log.info("  generated user id :"+ uniqueUserID + " for FN<"+fName+"> LN<"+lName+">  emplID <"+emplID+">" 
                    +idx +" <"+ idx +">" );
            log.info("(I)================= END OIDUtils.getUserLogin==============================");
                
                
        } catch (Exception e) {
            log.error(LoggerMessages.getMessage("ErrorMethodDebug", "OIDUtils/getUserLogin: Exception occured- ",  e.getMessage()),  e);
            e.printStackTrace();
        }
        return uniqueUserID;
    }

    /* Todo: Remove the following program  */
    public String getUserLogin_startwithlast4digits ( String oidserver, String fName, String lName, String emplID, String containerDN
                             ) {                                 
        String uniqueUserID = new String();   
        try {
            log.info("(I)================= BEGIN OIDUtils.getUserLogin==============================");
            
            Hashtable resourceHash = getITResource(oidserver);

            String serverAddress    = resourceHash.get("Server Address").toString();
            String rootContext      = resourceHash.get("Root DN").toString();
            String adminID          = resourceHash.get("Admin Id").toString();
            String adminPWD         = resourceHash.get("Admin Password").toString();
            String port             = resourceHash.get("Port").toString();
            String sslFlag          = resourceHash.get("SSL").toString();

            OIDFunctions oid = new OIDFunctions(serverAddress, port, rootContext, adminID, adminPWD, sslFlag);
            // tcUtilOIDUserOperations oidObj = new tcUtilOIDUserOperations(server, "389", rootContext, acctDN, passwd);
            // OIDFunctions oid = new OIDFunctions("oiddev1.cuny.edu", "389", "cn=users,dc=cuny,dc=edu", "cn=orcladmin", "Passw0rd", "false");
                              
            log.info ("      OID Validation sucessful   ");
             
            lName = trimString (lName, maxNameLength);
            uniqueUserID                    = fName.concat(".").concat(lName);  //  john.smith                                        
            String firstNamedotlastName     = uniqueUserID;   
            String ldapUserDNPrefix         = "cn";        
            int startingSeqNoOfEmplID       = 4;                     // The first fragment of emplID to be considered: last 4 digits
            int maxEmplIDLengthConsidered   = 11;                    // The max fragment of emplID to be considered: last 11 digits
                                                                     //   If emplID > 11, use fragment '00000000000' instead
                                                                     //   If emplID < 4 , use emplid OR '0000'
            String USERID_FOUND_AT_OID      = "USER_FOUND";          // Do Not change! 
            boolean userExists  = true;     
            int idx      = startingSeqNoOfEmplID;               
            int idxMax   = emplID.length();         // The minimum of (maxEmplIDLengthConsidered=11 or (emplID.length())
            if  ( emplID.length() > maxEmplIDLengthConsidered) { idxMax = maxEmplIDLengthConsidered; }                

             
            // First: just do collision check of 'firstNamedotLastName', use this as unique user id if no collision
            uniqueUserID = firstNamedotlastName;               
            
            // Second: Collision found for 'firstNamedotLastName'
            if ( ( oid.getAttribute(containerDN, ldapUserDNPrefix+"="+uniqueUserID) ).equals(USERID_FOUND_AT_OID))    {                    
                    // If the length of emplID is less than 4
                    //        set currID = 'cn=John.Smith.emplID' if it is unique, else
                    //        set currID = 'cn=John.Smith.0000' 
                    //        Skip the 'while' loop
                    if ( emplID.length() < startingSeqNoOfEmplID ) {
                        userExists = false; 
                        uniqueUserID = firstNamedotlastName.concat(".").concat(emplID);
                        oid = new OIDFunctions(serverAddress, port, rootContext, adminID, adminPWD, sslFlag);
                        if (  ( oid.getAttribute(containerDN,ldapUserDNPrefix+"="+uniqueUserID) ).equals(USERID_FOUND_AT_OID))    { 
                                    uniqueUserID = firstNamedotlastName.concat(".").concat("0000");
                        }                                                     
                    }
            
                    // At this point, the length of emplID is greater than 4, e.g. emplid=987654321
                    // While ( (userExists) && (idx < idxMax))
                    //      idx = 4 : set currID = 'cn=John.Smith.4321' if it is unique, else
                    //      idx = 5 : set currID = 'cn=John.Smith.54321' if it is unique, else
                    //      ..
                    //      idx = 9 : set currID = 'cn=John.Smith.987654321' if it is unique, else
                    while ((userExists) && ( idx <= idxMax)){   
                            uniqueUserID = firstNamedotlastName.concat(".").concat(  emplID.substring(emplID.length()- idx)  ) ;
                            oid = new OIDFunctions(serverAddress, port, rootContext, adminID, adminPWD, sslFlag);
                            if (  ( oid.getAttribute(containerDN,ldapUserDNPrefix+"="+uniqueUserID) ).equals(USERID_FOUND_AT_OID)) { 
                                idx++;
                            } else {
                                 userExists = false;
                            }
                    }
            
                    // On the very rare occasion, there's a collisiion of firstname+lastname+emplID, or emplID.length>11
                    if (userExists) {
                        uniqueUserID = firstNamedotlastName.concat(".").concat("00000000000"); 
                    }
            }// Collision found 
            log.info("                     generated user id :"+ uniqueUserID   );
            log.info("(I)================= END OIDUtils.getUserLogin==============================");
                
        } catch (Exception e) {
            log.error(LoggerMessages.getMessage("ErrorMethodDebug", "OIDUtils/getUserLogin: Exception occured- ",  e.getMessage()),  e);
            e.printStackTrace();
        }
        return uniqueUserID;
} 


    /**
    * This program is called by the Pre-Update adapter for the User object.
    * 
    * This password validation supplements the 'base password policy' with the additional requirement of 
    * having at least one numeric/or special character.
    * 
    * Notes: Make sure the special character set matches that of the 'SPL_CHARS' in the PasswordGenerator.java
    *        String SPL_CHARS = "!@#$%^&*():.<>?~";
    * 
    * @param pwdtext : password for validation
    * 
    * @return 
    *
    */  
    public boolean hasChar(String pwdtext) {
    
        boolean matchPwd = false;
        
        // First, if the password contains at least one number, we are done.
        Pattern special = Pattern.compile(OIDUtils.XL_NUMERIC_CHARS);                
        Matcher matcher = special.matcher(pwdtext);
        matchPwd = matcher.find();
        
        // Since the password does not have any number, it MUST have at least one special char then.
        if (! matchPwd) {    
        
            special = Pattern.compile(OIDUtils.XL_SPECIAL_CHARS);                
            matcher = special.matcher(pwdtext);
            matchPwd = matcher.find();    // true        
        }
        
        return matchPwd;
    }   
    
    



    /**
     * This method returns (boolean) true if the date passed is before today  
     * 
     * @param userID
     * @return
     */
    public String chkPhase1ProvDate (String userID) {
    
        String provisionNow = "FALSE";
        
        String query = "select USR_KEY from USR " + 
                       "where USR_UDF_PHASE1_DATE is not null  " +
                       "and USR_UDF_PHASE1_DATE < SYSDATE " +
                       "and USR_LOGIN = '" + userID + "' ";
        
        log.info("   chkPhase1ProvDate: " + query);   
        ResultSet queryResult = null;
        Statement queryStmt = null;
        Connection oimDBConnection = null;

        try {
            oimDBConnection = Platform.getOperationalDS().getConnection();
            queryStmt = oimDBConnection.createStatement();
            queryResult = queryStmt.executeQuery(query);
                if ( queryResult.next()) {
                    provisionNow = "TRUE";
                    log.info ("---- OIDUtils: chkPhase1ProvDate  Now return TRUE for provisionNow ");
                }
            
        }
        catch (Exception e) {
            e.printStackTrace();
            provisionNow = "FALSE";
        }   finally {
            try {
                if (queryResult != null) {
                    queryResult.close();
                }
            } catch (Exception e) {
                log.error("Exception ", e);
            }
            try {
                if (queryStmt != null) {
                    queryStmt.close();
                }
            } catch (Exception e) {
                log.error("Exception ", e);
            }
            try {
                if (oimDBConnection != null) {
                    oimDBConnection.close();
                }
            } catch (Exception e) {
                log.error("Exception ", e);
            }
        }     
        return provisionNow;
    }   


    /**
     * 2/2/09 Not Used.
     * 
     * This program is called by the 'Users' Entity Adapter.
     * 
     * This program returns true for the (UDF) global status if at least one secondary affiliation is chosen.
     * else, returns false.
     *  
     * @param uKey          :User Key from User Definition Form
     *          
     * @return true/false
     *
     */  
    public void updateUDFGlobalStatus( String uKey)   {
    
       String globalStatus = "False";
       ArrayList roleList = new ArrayList(); 
            try {                
                    log.info("================= BEGIN OIDUtils.updateUDFGlobalStatus==============================");
                  
                    long lUserKey = Long.valueOf(uKey).longValue();
                   
                    log.info(" User Key:"+ new Long(uKey).toString() + "User Name:"+ this.getUserAttrValue(lUserKey,"Users.User ID") );
     
                    tcResultSet     tcUDFList = lookupOps.getLookupValues("Lookup.OID.UserRoleUDF");
                    String sObjCode = "";    String sObjDecode = "";
                    int objRows = tcUDFList.getRowCount();
                    if (objRows == 0) { log.error("Lookup.OID.UserRoleUDF not found"); }
                    for (int k = 0; k < objRows; k++) {
                            tcUDFList.goToRow(k);
                            sObjCode = tcUDFList.getStringValue("Lookup Definition.Lookup Code Information.Code Key").trim();
                            sObjDecode = tcUDFList.getStringValue("Lookup Definition.Lookup Code Information.Decode").trim();
                            String currUDFValue = getUserAttrValue ( lUserKey, sObjCode);                                                                                                                                            
                            if ( currUDFValue.trim().length()>0 ) {
                                    log.info(" User role: ("+sObjDecode.trim() +") has been selected .............. ");
                                    roleList.add(sObjDecode);
                            }                      
                     }                       
                    if ( roleList.size() > 0 ) { 
                            globalStatus = "True";
                            log.info ("OIDUtils.updateUDFGlobalStatus: Setting globalStatus to True ");
                    }
                    
                    
                    // Now Update global status
                     //userRetrieval.put("Users.Last Name", lName);
                     
                     log.info("------------ Now retrieve user (uKey):" + uKey +",  name:" + this.getUserAttrValue(lUserKey,"Users.User ID"));
                     tcResultSet userData;
                     HashMap userRetrieval = new HashMap();
                     userRetrieval.put("USR_KEY", uKey);
                     userData = userOps.findUsers(userRetrieval);
                     
                    log.info("-------    then update user (uKey):" + uKey + " with status:" + globalStatus);
                    HashMap newUserData = new HashMap();
                    newUserData.put("USR_UDF_GLBL_STATUS", globalStatus);
                    userOps.updateUser(userData , newUserData);
                    
                    log.info("================= END OIDUtils.updateUDFGlobalStatus==============================");

        }
        catch(Exception e){
                log.error(LoggerMessages.getMessage("ErrorMethodDebug", "OIDUtils/addLDAPChileValues: Exception occured- ",  e.getMessage()),  e);
                e.printStackTrace();
        }
         
     
    }
             
  


/**
 * 2/2/09 Not Used.
 * 
 * This method is attached to the entity adapter 'CUNY Update Secondary Affiliation' for the 'Users' object (Post-update)
 * 
 * This method is being called whenever changes is made to any of the secondary affiliation on the user form.
 * 
 * This method populates the child table 'OID Cuny Roles' (UD_OID_CROL) if the corresponding field 
 * is marked on the user form.
 *
 * This method updates the 1st occurrence of the resource object 'OID User' that the child table belongs to in status of 'Provisioned', 'Enabled'
 * 
 * Notes: Called as an entity adapter for the 'Users' form, on 'POST'-events only
 * Notes: Called by post-change (Lookup.USR_PROCESS_TRIGGERS) of each user fields 
 * 
 * Notes: Only IE displays child tables correctly, not with firefox
 * Notes: During testing, may need to log out and in of OIM Web console to see the changes in the child table.
 * 
 *  
 * To Do: Error Messages to be shown on user form
 * 
 * Notes: This method updates the child form for the first 'OID User' resource object found for the user. Remove the 'break' if we want to update all 'OID User' resoure objects.
 * 
 * 
 * 
 * @param uKey          :User Key from User Definition Form
 * 
 *
 * @return 
 *
 */  
    public void addLDAPChildValues( String uKey) {

        
        ArrayList roleList = new ArrayList(); // this list will be populated with 'assigned' secondary roles.
       
        try { 
        
            log.info("(I)================= BEGIN OIDUtils.addLDAPChildValues==============================");
                  
            long lUserKey = Long.valueOf(uKey).longValue();
            tcResultSet userObjectsSet = userOps.getObjects(lUserKey);   
            
            log.info(" -  User Key:"+ new Long(uKey).toString());
            log.info(" -  User Name:"+ this.getUserAttrValue(lUserKey,"Users.User ID"));
            log.info(" -  User has " + userObjectsSet.getRowCount()+ " objects!");
 
            if ( userObjectsSet.getRowCount()>0 ) {
                 
                // Loop through every resource objects that the user has, we are looking for 'OID User' resource object.
                for ( int j =0; j < userObjectsSet.getRowCount(); j++ ){
                    
                    userObjectsSet.goToRow(j);
                    
                    long procInst            = userObjectsSet.getLongValue("Process Instance.Key");
                    String objStatus         = userObjectsSet.getStringValue("Objects.Object Status.Status");
                    String procDescStatus    = userObjectsSet.getStringValue("Process Instance.Descriptive Data");
                    
                    if ( objStatus.equalsIgnoreCase( XL_OIDUSER_RO )) {       
                        
                        if ((objStatus.equalsIgnoreCase(XL_OBJECT_STATUS_PROVISIONED)) ||(objStatus.equalsIgnoreCase(XL_OBJECT_STATUS_ENABLED))) {
                         
                            log.info(" -  User has process description: " + procDescStatus);
                                                  
                            // The user has an 'OID User' resource object ..of status 'provisioned' or 'enabled'.
                            long plParentFormDefinitionKey  = userObjectsSet.getLongValue("Process.Process Definition.Process Form Key");
                            int pnParentFormVersion         = formOps.getProcessFormVersion(procInst );
                            tcResultSet childFormResultSet  = formOps.getChildFormDefinition(plParentFormDefinitionKey, pnParentFormVersion);
                                                       
                            // First, populate the list 'tcUDFList' with all possible lookup values from Lookup.OID.UserRoleUDF
                            // Loop through the list, if the corresponding UDF field (2nd affiliation) is selected
                            //    , then populate the list 'roleList' with the UDF field
                            //
                            tcResultSet     tcUDFList = lookupOps.getLookupValues("Lookup.OID.UserRoleUDF");
                            String sObjCode = "";    String sObjDecode = "";
                            int objRows = tcUDFList.getRowCount();
                            
                            if (objRows == 0) { 
                                log.error("Lookup.OID.UserRoleUDF not found"); break; 
                            }
                            
                            for (int k = 0; k < objRows; k++) {

                                tcUDFList.goToRow(k);
            
                                sObjCode = tcUDFList.getStringValue("Lookup Definition.Lookup Code Information.Code Key").trim();
                                sObjDecode = tcUDFList.getStringValue("Lookup Definition.Lookup Code Information.Decode").trim();
                                String currUDFValue = getUserAttrValue ( lUserKey, sObjCode);                
                                                                                                                                                                     
                                if ( currUDFValue.trim().length()>0 ) {
                                       log.info ("OIDUtils: User role: ("+sObjDecode.trim() +") has been selected .............. ");
                                        roleList.add(sObjDecode);
                                }           
                                // Now 'roleList' has all UDF fields (2nd affiliation) that the user was assigned to.
                            }
                                

                            //  Now locate the child table 'OID Cuny Roles' (UD_OID_CROL) if it exists
                            //   1. Remove all entries
                            //   2. Populate with values from 'roleList'
                            // 
                            for (int i=0;i<childFormResultSet.getTotalRowCount();i++){
                                
                                childFormResultSet.goToRow(i);
                                long plChildFormDefinitionKey = childFormResultSet.getLongValue("Structure Utility.Child Tables.Child Key");
                                String plChildTableName = childFormResultSet.getStringValue("Structure Utility.Table Name");
                                if (plChildTableName.equalsIgnoreCase("UD_OID_CROL")){
                                                
                                    // 1. Remove all entries
                                    tcResultSet childFormData = formOps.getProcessFormChildData(plChildFormDefinitionKey, procInst);
                                     if (childFormData.isEmpty()){ 
                                        log.info (" Child table UD_OID_CROL was already empty!");
                                    }  
                                    else { 
                                        for ( int jj=0; jj<childFormData.getRowCount(); jj++){
                                            childFormData.goToRow(jj);
                                            long plChildFormPrimaryKey      = childFormData.getLongValue(plChildTableName + "_KEY");
                                            String childFormExistingValue   = childFormData.getStringValue(plChildTableName + "_NAME");
                                            formOps.removeProcessFormChildData (plChildFormDefinitionKey,plChildFormPrimaryKey );
                                            log.info("--  Removed Child Table Value:(" + childFormExistingValue +")");
                                        }
                                     }
                                                 
                                     // 2. Populate with values from 'roleList'
                                     if ( roleList.size()>0 ) {
                                        Iterator e = roleList.iterator();
                                        while (e.hasNext()){                         
                                            String currVal = (String)e.next();
                                            if ( currVal.trim().length()>0 ){
                                                Hashtable childFormHash = new Hashtable();                                                           
                                                childFormHash.put(plChildTableName + "_NAME", currVal);      
                                                formOps.addProcessFormChildData(plChildFormDefinitionKey, procInst , childFormHash);
                                                log.info("--   Added Child Table Value:("+currVal+")" );
                                            }
                                        }
                                     }
                                    break;  
                                }  
                            }
                           
                            break; // Now we are updating the first OID User object found. Remove this break to update all OID User obj
                            //}
                         }
                    }
                }
            }
            log.info("(I)================= END OIDUtils.addLDAPChildValues==============================");
        }
        catch(Exception e){
             log.error(LoggerMessages.getMessage("ErrorMethodDebug", "OIDUtils/addLDAPChileValues: Exception occured- ",  e.getMessage()),  e);
             e.printStackTrace();
        }
    }


    private Hashtable getITResource(String itResource) {
        Hashtable resource = new Hashtable();
        try {
            tcITResourceInstanceOperationsIntf itResOper
                    = Platform.getService(tcITResourceInstanceOperationsIntf.class);
            HashMap<String, String> searchcriteria
                    = new HashMap<String, String>();
            searchcriteria.put("IT Resources.Name", itResource);
            log.info("IT Resource Name " + itResource);
            tcResultSet resultSet
                    = itResOper.findITResourceInstances(searchcriteria);
            log.info("Result Set Count " + resultSet.getTotalRowCount());
            tcResultSet itResSet
                    = itResOper.getITResourceInstanceParameters(resultSet.getLongValue("IT Resources.Key"));
            log.info("itResuletSet Count " + itResSet.getRowCount());
            for (int i = 0; i < itResSet.getRowCount(); i++) {
                itResSet.goToRow(i);
                String paramName
                        = itResSet.getStringValue("IT Resources Type Parameter.Name");
                String paramVal
                        = itResSet.getStringValue("IT Resource.Parameter.Value");
                log.info("Parameter Name" + paramName + " VALUE: " + paramVal);
                resource.put(paramName, paramVal);
            }
        } catch (Exception e) {
            log.error("Exception ", e);

        }
        return resource;
    }

    public static void main(String args[]) {
        OIDUtils u = new OIDUtils();
        String whatever = "OIM-!@# $%^&dude.te sti;ng07 ";
        //u.getUserLogin("oimdev1.cuny.edu","John","Smith2630","10839124","");
        
        //=.Smith2630124
        //10839124
        log.debug(u.getRidOfSomeChars(whatever,OIDUtils.XL_CHARS_NOT_ALLOWED_IN_DN));
        ;
    }
}