/*******************************************************************************
 * PasswordGenerator
 * 
 ******************************************************************************/

package com.thortech.xl.utils;

import com.thortech.util.logging.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Random;

import com.thortech.xl.dataaccess.tcDataProvider;
import com.thortech.xl.dataaccess.tcDataSet;
import com.thortech.xl.util.logging.LoggerMessages;


/**
 * 
 *
 */
public class PasswordGenerator {

        private static Logger log = Logger.getLogger("CUNY.CUSTOM");
        String[] masRulesData = null; // Password Policy Rules
        
        // CUNY: Max. Password length =10 , this will override that of password policy if exists
        // CUNY: Default Password length =7 if none is specified in the password policy
        private static int XL_MAXPWDLENGTH=10;   
        private static int XL_DEFAULTPWDLENGTH=7;  
        
        // In addition to the 'base password policy' defined in OIM
        // CUNY has an additional password requirement to generate at least one numeric/or special character
        // The sum of XL_GENERATE_SPECIAL_CHAR, and XL_GENERATE_NUMBER must be at least 1, and less than max
        private static int XL_GENERATE_MIN_SPECIAL_CHAR=2;
        private static int XL_GENERATE_MIN_NUMBER=1; 
        
        private static String testonly = "true";
									 

	tcDataProvider xlDataProv = null;

	/*
	 * Constructor - 
	 */
	 public PasswordGenerator() {
	 	log.info(" --- Should not see this: Constructor called----------");
		// Get Password Policy
	 	masRulesData = getPWDPolData();

	 }

	/**
	 * Constructor
	 */
	public PasswordGenerator(tcDataProvider pDataProvider, String pPwdPolicy) {
		xlDataProv = pDataProvider;
		// Get Password Policy
		masRulesData = fetchPasswordPolicy(pPwdPolicy);
	}

	/**
	 * This method generates a random password based on the rule (base password policy) read from
	 * It also uses firstname, lastname as seed
         * 
         * Notes:  Modify the list of special characters (SPL_CHARS) if needed
	 *
	 * @param sUserID         
	 * @param sFirstName
	 * @param sLastName
         * 
	 * @return < generated password >
	 *  
	 */
	public String generatePassword(String sUserID, String sFirstName,
			String sLastName) {
                log.info("(I)================= BEGIN PasswordGenerator==============================");
                log.info("   for user id:" + sUserID);
                
		int iMinLength = 0;
		int iMaxLength = 0;
		int iMinAlpha = 0;
		int iMinNumbers = 0;
		int iMinAlphaNum = 0;
		int iMinSpecial = 0;
		int iMaxSpecial = 0;
		int iMaxRepeated = 0;
		int iMinUnique = 0;
		int iMinUpperCase = 0;
		int iMinLowerCase = 0;
		String sCharsRequired = null;
		String sCharsNotAllowed = null;
		String sCharsAllowed = null;
		String sStringNotAllowed = null;
		boolean bStartWithChar = false;
		boolean bDisallowUserID = false;
		boolean bDisallowFirstName = false;
		boolean bDisallowLastName = false;

		String UCASE_ALPHABETS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		String LCASE_ALPHABETS = "abcdefghijklmnopqrstuvwxyz";
                // Please verify that the following 2 variables match that in OIDUtils.java -> hasChar()
		String NUMBERS = "1234567890";
		//String SPL_CHARS = "!@#$%^&*():.<>?~";
                String SPL_CHARS = "!@#$%^&*()";
                 
		//log.info("     policy read for : ");
		//for (int i = 1; i < masRulesData.length; i++) {
		//	log.info(" masRulesData[" + i + "] : " + masRulesData[i]);
		//}

		// Reading Policy Rule data
		if (masRulesData[1] != null && !masRulesData[1].equals("")) {
			iMinLength = Integer.parseInt(masRulesData[1]);
		}

		if (masRulesData[2] != null && !masRulesData[2].equals("")) {
			iMaxLength = Integer.parseInt(masRulesData[2]);
		} else {
			iMaxLength = XL_DEFAULTPWDLENGTH;
			if (iMinLength > iMaxLength) {
				iMaxLength = iMinLength;
			}
		}

		if (iMaxLength > 10) {
			iMaxLength = XL_MAXPWDLENGTH;
		}

	       //log.info(" Min. Pwd Length :" + iMinLength + ",  Max. Pwd Length :" + iMaxLength);
            
		if (masRulesData[3] != null && !masRulesData[3].equals("")) {
			iMinAlpha = Integer.parseInt(masRulesData[3]);
		}

                // CUNY: Override the default min. numeric in generated pwd
		//if (masRulesData[4] != null && !masRulesData[4].equals("")) {
		//	iMinNumbers = Integer.parseInt(masRulesData[4]);
		//}
                iMinNumbers = XL_GENERATE_MIN_NUMBER;
                
		if (masRulesData[5] != null && !masRulesData[5].equals("")) {
			iMinAlphaNum = Integer.parseInt(masRulesData[5]);
		}

	       // CUNY: Override the default min. special in generated pwd
		//if (masRulesData[6] != null && !masRulesData[6].equals("")) {
		//	iMinSpecial = Integer.parseInt(masRulesData[6]);
		//}
                iMinSpecial = XL_GENERATE_MIN_SPECIAL_CHAR;
                

		if (masRulesData[7] != null && !masRulesData[7].equals("")) {
			iMaxSpecial = Integer.parseInt(masRulesData[7]);
		}
                
                
		if (masRulesData[8] != null && !masRulesData[8].equals("")) {
			iMaxRepeated = Integer.parseInt(masRulesData[8]);
		} else if (masRulesData[8] == null || masRulesData[8].equals("")) {
			iMaxRepeated = 0;
		}

		if (masRulesData[9] != null && !masRulesData[9].equals("")) {
			iMinUnique = Integer.parseInt(masRulesData[9]);
		} else if (masRulesData[9] == null || masRulesData[9].equals("")) {
			iMinUnique = -1;
		}
		if (masRulesData[10] != null && !masRulesData[10].equals("")) {
			iMinUpperCase = Integer.parseInt(masRulesData[10]);
		}
		if (masRulesData[11] != null && !masRulesData[11].equals("")) {
			iMinLowerCase = Integer.parseInt(masRulesData[11]);
		}

		sCharsRequired = masRulesData[12];
		sCharsNotAllowed = masRulesData[13];

		if (null == sCharsRequired) {
			sCharsRequired = "";
		}
		if (null == sCharsNotAllowed) {
			sCharsNotAllowed = "";
		}

		if (masRulesData[14] != null && !masRulesData[14].equals("")) {
			sCharsAllowed = masRulesData[14];
		}
		sStringNotAllowed = masRulesData[15];


		if (masRulesData[16] != null && masRulesData[16].equals("1")) {
			bStartWithChar = true;
		}

		if (masRulesData[17] != null && masRulesData[17].equals("1")) {
			bDisallowUserID = true;
		}

		if (masRulesData[18] != null && masRulesData[18].equals("1")) {
			bDisallowFirstName = true;
		}

		if (masRulesData[19] != null && masRulesData[19].equals("1")) {
			bDisallowLastName = true;
		}

		// Define a password map
		int PASS_MAP[] = new int[iMaxLength];
		// Define Password Data Holder
		StringBuffer sbPassword = new StringBuffer(iMaxLength);

		//log.info("Before Applying Rules>>>>>>>>>>");
		//for (int i = 0; i < iMaxLength; i++) {
		//	log.info(" PASS_MAP[" + i + "] : " + PASS_MAP[i]);
		//}

		// Apply Policy to generate the password map

		// Applying Rule 3 : Minimum Alphabet Characters
		if (iMinAlpha > 0) {
                       //log.info("Applying Rule  3: Minimum alpha character:" +iMinAlpha );
			fillPasswordMap(1, 3, iMinAlpha, PASS_MAP, iMaxLength);
		}

		// Applying Rule 4 : Minimum Numeric Numbers
		if (iMinNumbers > 0) {
			//log.info("Applying Rule  4: Minimum numeric numbers(Overrided):" +iMinNumbers );
			fillPasswordMap(1, 4, iMinNumbers, PASS_MAP, iMaxLength);
			//log.info("- Rule 4 applied");
		}

		// Applying Rule 5 : Minimum Alphanumeric
		int iDiff = 0;
		if (iMinAlphaNum > (iMinAlpha + iMinNumbers)) {
			//log.info("Applying Rule  5: Minimum alpha numbers:" + iMinAlphaNum);
			iDiff = iMinAlphaNum - iMinAlpha - iMinNumbers;
			fillPasswordMap(1, 5, iDiff, PASS_MAP, iMaxLength);
			//log.info("- Rule 5 applied");
		}

		// Applying Rule 6 : Minimum Special Characters

		if (iMinSpecial > 0) {
			//log.info("Applying Rule  6: Minimum. special numbers(Overrided):" + iMinSpecial);
			//log.info("- Rule 5 applied");
			fillPasswordMap(1, 6, iMinSpecial, PASS_MAP, iMaxLength);
		}

		// Filling empty cells
		for (int iMapInd = 0; iMapInd < iMaxLength; iMapInd++) {
			if (PASS_MAP[iMapInd] == 0) {
				PASS_MAP[iMapInd] = 3;
			}
		}

		// Applying Rule 10 : Minimum Uppercase Letters
		if (iMinUpperCase > 0) {
		        //log.info("Applying Rule  10: Minimum. Upper Case:" + iMinUpperCase);
			fillPasswordMap(2, 10, iMinUpperCase, PASS_MAP, iMaxLength);
		}

		// Applying Rule 11 : Minimum Uppercase Letters
		if (iMinLowerCase > 0) {
		        //log.info("Applying Rule  11: Minimum. Lower Case:" + iMinLowerCase);
			fillPasswordMap(2, 11, iMinLowerCase, PASS_MAP, iMaxLength);
		}

		//log.info("=====After Applying Rules MAP Values:");
                
                
		//for (int i = 0; i < iMaxLength; i++) {
		//	log.info(" PASS_MAP[" + i + "] : " + PASS_MAP[i]);
		//}

		// Apply Policy To Generate Password

		// Applying 13 and 14 : Characters allowed and not allowed
		if (null == sCharsAllowed) {
			sCharsAllowed = UCASE_ALPHABETS + LCASE_ALPHABETS + NUMBERS
					+ SPL_CHARS;
		}

		// Generating random number seed
		String sFullName=sFirstName + sLastName;
		int iRand = ((sUserID != null) ? sUserID.length() : 0)
				+ ((sFirstName != null) ? sFirstName.length() : 0)
				+ ((sLastName != null) ? sLastName.length() : 0)
				+ (sFullName.length())
				+ (int) System.currentTimeMillis() % 100000;
		//		iRand = (int) ((iRand + sCharsAllowed.length()) % sCharsAllowed
		//				.length());

		iRand = (new Random(iRand)).nextInt() % sCharsAllowed.length() + sLastName.length();
		iRand = Math.abs(iRand);

		//log.info("generatePassword  : Password Length :" + iMaxLength);
		//log.info("generatePassword  : Random Seed :" + iRand);

		// Initialize Password Holder
		for (int iCnt = 0; iCnt < iMaxLength; iCnt++) {
			//log.info("generatePassword -Loop");
			sbPassword.append('\0');
		}

		// Applying Rule 12 : Required Characters
		//log.info("generatePassword : Applying Rule 12 : num required characters");
		if (null != sCharsRequired) {
			sbPassword = addRequiredCharacters(sCharsRequired, PASS_MAP,
					iMaxLength, iRand);
		}
		//log.info("generatePassword : After Applying Rule 12 : <"
		//		+ "****"  + ">");

		// Applying Rule 13&14 : Allowed Characters and Not alowed characters
		addAllowedCharacters(sbPassword, PASS_MAP, sCharsAllowed,
				sCharsNotAllowed, iMaxLength, iRand);

		String sPasswordValue = sbPassword.toString();
		//log.info("generatePassword : After Applying Rule 13 (allowed char) and 14 (not allowed characters) : <"+ "****" + ">");

		// Applying Rule 8 :Maximum Repeated Characters
		// Applying Rule 9 :Minimum Unique Characters
		//log.debug("generatePassword :iMaxRepeated : " + iMaxRepeated);
		//log.debug("generatePassword :iMinUnique : " + iMinUnique);

		if (iMaxRepeated != iMaxLength && iMinUnique != -1) {
			resolveRepeations(sbPassword, PASS_MAP, sCharsAllowed,
					sCharsNotAllowed, iMaxLength, iRand, iMaxRepeated,
					iMinUnique);
		}

		// Applying Rule 15 : Strings not allowed (assuming only one string)
		// Applying Rule 17 : Disallow User ID
		// Applying Rule 18 : Disallow First Name
		// Applying Rule 19 : Disallow Last Name

		if ((null != sStringNotAllowed && sStringNotAllowed.equals("") && (sPasswordValue
				.indexOf(sStringNotAllowed) > -1))
				|| (bDisallowUserID == true && (sPasswordValue.indexOf(sUserID) > -1))
				|| (bDisallowFirstName == true && (sPasswordValue
						.indexOf(sFirstName) > -1))
				|| (bDisallowLastName == true && (sPasswordValue
						.indexOf(sLastName) > -1))) {
			sPasswordValue = flipPassword(sbPassword);
		}

		sPasswordValue = sbPassword.toString();

		if (bStartWithChar){
			Character cStartChar =new Character(sPasswordValue.charAt(0));
			if (cStartChar.isDigit(sPasswordValue.charAt(0))){
				//log.info("First letter is a digit  " + "****" );
				sPasswordValue=sFirstName.substring(0,1) + sPasswordValue.substring(1,sPasswordValue.length());
				//log.info("AFter  " + "****" );
			}
		}

               
	       log.info("(I)================= END PasswordGenerator==============================");
		//Always return the password wuth a special character and a number
		sPasswordValue = sPasswordValue + "@1";
		return sPasswordValue;
	}

	/*
	 * addAllowedCharacters
	 *
	 */
	private void resolveRepeations(StringBuffer sbPassword, int PASS_MAP[],
			String sCharsAllowed, String sCharsNotAllowed, int iMaxLength,
			int iRand, int iMaxRepeated, int iMinUnique) {
                        
		//printLog("resolveRepeations called");
		int iRepetitions[][] = new int[iMaxLength][2];
		int j = 0;
		for (int i = 0; i < sbPassword.length(); i++) {
			int iCurr = sbPassword.charAt(i);
			boolean bUpdate = false;
			for (j = 0; (iRepetitions[j][0] != 0 && j < iMaxLength); j++) {
				if (iRepetitions[j][0] == iCurr) {
					iRepetitions[j][1] = iRepetitions[j][1] + 1;
					bUpdate = true;
				}
			}
			if (bUpdate == false) {
				iRepetitions[j][0] = iCurr;
				iRepetitions[j][1] = 1;
				bUpdate = true;
			}
		}

		// Find out repeated characters
		int iRep = 0;
		int iTotalRepeations = 0;
		StringBuffer sbRepeatedChars = new StringBuffer();
		int iRepeatedCharCount[] = new int[j];
		for (int k = 0; k <= j; k++) {
			if (iRepetitions[k][1] > 1) {
				iTotalRepeations = iTotalRepeations + iRepetitions[k][1];
				iRep = iRep + 1;
				sbRepeatedChars.append((char) iRepetitions[k][0]);
				iRepeatedCharCount[iRep - 1] = iRepetitions[k][1];
			}
		}
		//printLog("resolveRepeations  :No of Repeated chars : " + iRep);
		//printLog("resolveRepeations  :Repeated Characters : "	+ sbRepeatedChars.toString());
		//printLog("resolveRepeations  :Total Repeated chars : "	+ iTotalRepeations);

		String sPassword = sbPassword.toString();
		if (iMinUnique > (iMaxLength - iTotalRepeations)
				|| iMaxRepeated < iTotalRepeations) {
			int iNeedToResolve = iTotalRepeations - iMaxRepeated;
			//printLog("resolveRepeations  :Need To Resolve : " + iNeedToResolve);
			int iCnt = 0;
			int iTotCnt = 0;
			for (;;) {
				char cCurr = sbRepeatedChars.charAt(iCnt);
				char cCurr1 = cCurr;
				int iPos = -1;
				//printLog("resolveRepeations  number of repetitions for : "
				//		+ cCurr + " : is ->" + iRepeatedCharCount[iCnt]);
				String sNotAllowedChars = sCharsNotAllowed
						+ sbPassword.toString();
				for (int iInd = 0; iInd < iRepeatedCharCount[iCnt] - 1; iInd++) {
					iPos = sPassword.indexOf(cCurr, iPos + 1);
					sNotAllowedChars = sNotAllowedChars + cCurr1;
					//printLog("resolveRepeations  sNotAllowedChars for : "
					//		+ cCurr + "[" + iInd + "] at Pos[" + iPos
					//		+ "] is ->" + sNotAllowedChars);
					cCurr1 = resolveCharAt(cCurr, PASS_MAP[iPos], iPos,
							sbPassword, sCharsAllowed, sNotAllowedChars, iRand);
				}
				iTotCnt = (iTotCnt + iRepeatedCharCount[iCnt]);

				if (iTotCnt > (iNeedToResolve - 1)) {
					break;
				}
				iCnt++;
			}
		}
	}

	/*
	 * resolveCharAt
	 *
	 */
	private char resolveCharAt(char cCurr, int iRuleCode, int iPosition,
			StringBuffer sbPassword, String sCharsAllowed,
			String sNotAllowedChars, int iRand) {
		//printLog("resolveCharAt called");
		char cChosen = '\0';
		int iLooped = 0;
		for (;;) {
			iLooped++;
			cChosen = pickupAtRandom(sCharsAllowed, iRuleCode, iRand);
			//printLog("addAllowedCharacters cChosen :" + cChosen);
			if (null != sNotAllowedChars
					&& sNotAllowedChars.indexOf(cChosen) > -1) {
				iRand = iRand + cChosen
						+ (int) (System.currentTimeMillis() % 100000);
				cChosen = pickupAtRandom(sCharsAllowed, iRuleCode, iRand);
			} else {
				break;
			}

			if (iLooped > 3) {
				iRand = 3 * iRand + cChosen
						+ (int) (System.currentTimeMillis() % 100000);
			}

		}
		sbPassword.setCharAt(iPosition, cChosen);
		return cChosen;
	}

	/*
	 * addAllowedCharacters
	 *
	 */
	private void addAllowedCharacters(StringBuffer sbPassword, int PASS_MAP[],
			String sCharsAllowed, String sCharsNotAllowed, int iMaxLength,
			int iRand) {
		//printLog("addAllowedCharacters called");
		for (int iIndex = 0; iIndex < iMaxLength; iIndex++) {
			int iCurr = PASS_MAP[iIndex];
			char cChosen = '\0';
			if ('\0' == sbPassword.charAt(iIndex)) {
				int iLooped = 0;
				for (;;) {
					iLooped++;
					iRand = iRand + cChosen
							+ (int) (System.currentTimeMillis() % 100000);
					cChosen = pickupAtRandom(sCharsAllowed, iCurr, iRand);
					//printLog("addAllowedCharacters cChosen :" + cChosen);
					if (null != sCharsNotAllowed
							&& sCharsNotAllowed.indexOf(cChosen) > -1) {
						cChosen = pickupAtRandom(sCharsAllowed, iCurr, iRand);
					} else {
						break;
					}

					if (iLooped > 3) {
						iRand = 3 * iRand + cChosen
								+ (int) (System.currentTimeMillis() % 100000);
					}

					if (iLooped > 1000) {
						break;
					}

				}
				sbPassword.setCharAt(iIndex, cChosen);
			}
		}
		//printLog("addAllowedCharacters exit");
	}

	/*
	 * pickupAtRandom
	 *
	 */
	private char pickupAtRandom(String sSource, int iRuleCode, int iRand) {
		//printLog("pickupAtRandom called ");
		int iSourceLen = sSource.length();
		int iRand2;
		char cCurr;
		int iLooped = 0;
		if (iRuleCode == 13) {
			// Uppercase
			for (;;) {
				iLooped++;
				// System.out.print("13");
				if (iRand < 1)
					iRand = (int) (System.currentTimeMillis() % 100000);
					iRand = (iRand + iSourceLen) % iSourceLen;

				cCurr = sSource.charAt(iRand);
				if (Character.isUpperCase(cCurr)) {
					return cCurr;
				} else {
					// iRand2 = (3*iRand)*(iRand+1);
					iRand2 = (3 * iRand) * (cCurr)
							+ (int) (System.currentTimeMillis() % 100000);
					if (iRand2 == iRand) {
						iRand2 = iRand2 + 1;
					}
					iRand = iRand2;
				}
				if (iLooped > 1000) {
					// printLog("Could Not Find Capital Letter, using
					// 'A'");
					return 'A';
				}
			}
		} else if (iRuleCode == 14) {
			// Lowercase
			for (;;) {
				iLooped++;
				if (iRand < 1)
					iRand = (int) (System.currentTimeMillis() % 100000);
				iRand = (iRand + iSourceLen) % iSourceLen;
				cCurr = sSource.charAt(iRand);
				if (Character.isLowerCase(cCurr)) {
					return cCurr;
				} else {
					// iRand2 = (3*iRand)*(iRand+1);
					iRand2 = (3 * iRand) * (cCurr)
							+ (int) (System.currentTimeMillis() % 100000);
					if (iRand2 == iRand) {
						iRand2 = iRand2 + 1;
					}
					iRand = iRand2;
				}
				if (iLooped > 1000) {
					// printLog("Could Not Find Small Letter, using
					// 'a'");
					return 'a';
				}
			}
		} else if (iRuleCode == 4) {
			// Numeric
			for (;;) {
				iLooped++;

				if (iRand < 1)
					iRand = (int) (System.currentTimeMillis() % 100000);
				iRand = (iRand + iSourceLen) % iSourceLen;

				cCurr = sSource.charAt(iRand);
				if (Character.isDigit(cCurr)) {
					return cCurr;
				} else {
					iRand2 = (3 * iRand) * (cCurr)
							+ (int) (System.currentTimeMillis() % 100000);
					iRand2 = (iRand2 + iSourceLen) % iSourceLen;
					if (iRand2 == iRand) {
						iRand2 = iRand2 + 1;
					}
					iRand = iRand2;
				}
				if (iLooped > 1000) {
					// printLog("Could Not Find Number, using 0");
					return '0';
				}
			}
		} else if (iRuleCode == 5) {
			// AlphaNumeric
			for (;;) {
				iLooped++;
				if (iRand < 1)
					iRand = (int) (System.currentTimeMillis() % 100000);
				iRand = (iRand + iSourceLen) % iSourceLen;
				cCurr = sSource.charAt(iRand);
				if (Character.isLetterOrDigit(cCurr)) {
					return cCurr;
				} else {
					iRand2 = (3 * iRand) * (cCurr)
							+ (int) (System.currentTimeMillis() % 100000);
					if (iRand2 == iRand) {
						iRand2 = iRand2 + 1;
					}
					iRand = iRand2;
				}
				if (iLooped > 1000) {
					// printLog("Could Not Find Alphanumeric, using
					// 'a'");
					return 'a';
				}
			}
		} else if (iRuleCode == 3) {
			// Letter
			for (;;) {
				iLooped++;
				if (iRand < 1)
					iRand = (int) (System.currentTimeMillis() % 100000);
				iRand = (iRand + iSourceLen) % iSourceLen;

				cCurr = sSource.charAt(iRand);
				if (Character.isLetterOrDigit(cCurr)) {
					return cCurr;
				} else {
					iRand2 = (3 * iRand) * (cCurr)
							+ (int) (System.currentTimeMillis() % 100000);
					if (iRand2 == iRand) {
						iRand2 = iRand2 + 1;
					}
					iRand = iRand2;
				}
				if (iLooped > 1000) {
					// printLog("Could Not Find a Letter, using 'a'");
					return 'a';
				}
			}
		} else { // if (iRuleCode == 6) {
			// Special
			for (;;) {
				iLooped++;
				if (iRand < 1)
					iRand = (int) (System.currentTimeMillis() % 100000);
				iRand = (iRand + iSourceLen) % iSourceLen;

				cCurr = sSource.charAt(iRand);
				if (Character.isLetterOrDigit(cCurr) == false) {
					return cCurr;
				} else {
					iRand2 = (3 * iRand) * (cCurr)
							+ (int) (System.currentTimeMillis() % 100000);
					if (iRand2 == iRand) {
						iRand2 = iRand2 + 1;
					}
					iRand = iRand2;
				}
				if (iLooped > 1000) {
					// printLog("Could Not Find Special, using '*'");
					return '*';
				}
			}
		}
	}

	/*
	 * addRequiredCharacters
	 *
	 */
	private StringBuffer addRequiredCharacters(String sCharsRequired,
			int[] PASS_MAP, int iMaxLength, int iRand) {
		//printLog("addRequiredCharacters called : " + sCharsRequired);
		int iReqNum = sCharsRequired.length();
		StringBuffer sbPassword = new StringBuffer(iMaxLength);
		for (int iCnt = 0; iCnt < iMaxLength; iCnt++) {
			sbPassword.append('\0');
		}

		for (int i = 0; i < iReqNum; i++) {
			char cCurr = sCharsRequired.charAt(i);
			//printLog("addRequiredCharacters cCurr : " + cCurr);
			int iRuleCode = 3;
			if (Character.isUpperCase(cCurr)) {
				iRuleCode = 13;
			} else if (Character.isLowerCase(cCurr)) {
				iRuleCode = 14;
			} else if (Character.isDigit(cCurr)) {
				iRuleCode = 4;
			} else if (!Character.isLetterOrDigit(cCurr)) {
				iRuleCode = 6;
			}
			int iRandPos = pickupRandomPosition(iRuleCode, PASS_MAP,
					iMaxLength, sbPassword, iRand);
			//printLog("addRequiredCharacters : iRandPos :" + iRandPos);
			sbPassword.setCharAt(iRandPos, cCurr);
		}
		//printLog("addRequiredCharacters exit");
		return sbPassword;
	}

	/*
	 * pickupRandomPosition
	 *
	 */
	private int pickupRandomPosition(int iRuleCode, int[] iMap, int iMaxLength,
			StringBuffer sbPassword, int iRand) {
		//printLog("pickupRandomPosition called");
		StringBuffer sbRuleDomain = new StringBuffer();
		for (int i = 0; i < iMaxLength; i++) {
			if (iRuleCode == 13 && (iMap[i] == 13 || iMap[i] == 3)) {
				sbRuleDomain.append(i);
			} else if (iRuleCode == 14 && (iMap[i] == 14 || iMap[i] == 3)) {
				sbRuleDomain.append(i);
			} else if (iRuleCode == 4 && (iMap[i] == 4 || iMap[i] == 5)) {
				sbRuleDomain.append(i);
			} else if (iRuleCode == 6 && iMap[i] == 6) {
				sbRuleDomain.append(i);
			}
		}
		if (iRand < 1) {
			iRand = (int) (System.currentTimeMillis() % 100000);
		}
		iRand = ((iRand + sbRuleDomain.length()) % sbRuleDomain.length());
		//printLog("pickupRandomPosition exit : " + iRand);
		return iRand;
	}

	/*
	 * flipPassword
	 *
	 */
	private String flipPassword(StringBuffer sbPassword) {
		StringBuffer sbTemp = new StringBuffer(sbPassword.substring(1));
		return sbPassword.charAt(0) + sbTemp.reverse().toString();

	}

	/*
	 * fillPasswordMap
	 *
	 */
	private void fillPasswordMap(int iOpCode, int iRuleID, int iRuleValue,
			int[] MAP, int iMapSize) {
		//log.info("fillPasswordMap called");
		//log.info(" >> iRuleID :" + iRuleID);
		//log.info(" >> iRuleValue :" + iRuleValue);
		int iIndex = 0;
		// int MAP[] = (int []) oMAP;
		for (int i = 0; i < iRuleValue; i++) {
			if (iOpCode == 1) {
				if (MAP[iIndex] > 0 || (iIndex == 0 && iRuleID == 4))
					iIndex = findNextCell(iIndex, iOpCode, MAP, iMapSize);
				MAP[iIndex] = iRuleID;
				//log.info("fillPasswordMap MAP[" + iIndex + "]->" + MAP[iIndex]);
			} else if (iOpCode == 2) {
				if (MAP[iIndex] != 3)
					iIndex = findNextCell(iIndex, iOpCode, MAP, iMapSize);
				MAP[iIndex] = MAP[iIndex] + iRuleID;
			}
			iIndex = iIndex + 2;
			if (iIndex > (iMapSize - 1))
				iIndex = 0;
		}
		//log.info("fillPasswordMap exit");
	}

	/*
	 * findNextCell
	 *
	 */
	private int findNextCell(int iIndex, int iOpCode, int[] MAP, int iMapSize) {
		for (;;) {
			iIndex = iIndex + 1;
			if (iIndex > iMapSize - 1)
				iIndex = 0;
			if (iOpCode == 1) {
				if (MAP[iIndex] == 0) {
					return iIndex;
				}
			} else {
				if (iOpCode == 2)
					if (MAP[iIndex] == 3) {
						return iIndex;
					}
			}
		}
	}

	/*
	 * fetchPasswordPolicy
	 *
	 */
	private String[] fetchPasswordPolicy(String pPwdPolicy) {
		try {
			//log.info("fetchPasswordPolicy called");
			// printLog("pPwdPolicy -- " + pPwdPolicy);
			// Get Database Connection and execute the SQL Query to fetch the
			// Password Policy Rules
			String[] masPolicyData = new String[20];
			StringBuffer msbQuery = new StringBuffer();
			msbQuery.append("SELECT ");
			msbQuery
					.append("PWR_MIN_LENGTH, PWR_MAX_LENGTH, PWR_MIN_ALPHA, PWR_MIN_NUMBER, ");
			msbQuery
					.append("PWR_MIN_ALPHANUM, PWR_MIN_SPECIALCHAR, PWR_MAX_SPECIALCHAR, ");
			msbQuery
					.append("PWR_MAX_REPEATED, PWR_MIN_UNIQUE, PWR_MIN_UPPERCASE, PWR_MIN_LOWERCASE, ");
			msbQuery
					.append("PWR_REQD_CHARS, PWR_INVALID_CHARS, PWR_VALID_CHARS, PWR_INVALID_STRINGS, ");
			msbQuery.append("PWR_STARTS_WITH_CHAR, PWR_DISALLOW_USERID, ");
			msbQuery.append("PWR_DISALLOW_FNAME, PWR_DISALLOW_LNAME ");
			msbQuery.append("FROM PWR ");
			msbQuery.append("WHERE PWR_NAME = '" + pPwdPolicy + "'");
			tcDataSet moSvpDs = new tcDataSet();
			//log.info("fetchPasswordPolicy : msbQuery : " + msbQuery.toString());
			moSvpDs.setQuery(getDataBase(), msbQuery.toString());
			//log.info("fetchPasswordPolicy : Executing Query");
			moSvpDs.executeQuery();

			// Build a list of Policy Rules
			if (moSvpDs.getRowCount() == 0) {
				throw new Exception("Password Policy is not defined");
			} else {
				for (int iCount = 0; iCount < 19; iCount++)
					masPolicyData[iCount + 1] = moSvpDs.getString(iCount);
			}
			moSvpDs = null;
			//log.info("fetchPasswordPolicy exit");

			return masPolicyData;
		} catch (Exception oexcp) {
			oexcp.printStackTrace();
			//log.info("Exception : " + oexcp.getMessage());
			return null;
		}
	}

	public String[] getPWDPolData() {
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection conn = DriverManager.getConnection(
					"jdbc:oracle:thin:@172.16.206.221:1521:ddOAMAud",
					"oimuser", "abcd1234");
			String[] masPolicyData = new String[20];
			StringBuffer msbQuery = new StringBuffer();
			msbQuery.append("SELECT ");
			msbQuery
					.append("PWR_MIN_LENGTH, PWR_MAX_LENGTH, PWR_MIN_ALPHA, PWR_MIN_NUMBER, ");
			msbQuery
					.append("PWR_MIN_ALPHANUM, PWR_MIN_SPECIALCHAR, PWR_MAX_SPECIALCHAR, ");
			msbQuery
					.append("PWR_MAX_REPEATED, PWR_MIN_UNIQUE, PWR_MIN_UPPERCASE, PWR_MIN_LOWERCASE, ");
			msbQuery
					.append("PWR_REQD_CHARS, PWR_INVALID_CHARS, PWR_VALID_CHARS, PWR_INVALID_STRINGS, ");
			msbQuery.append("PWR_STARTS_WITH_CHAR, PWR_DISALLOW_USERID, ");
			msbQuery.append("PWR_DISALLOW_FNAME, PWR_DISALLOW_LNAME ");
			msbQuery.append("FROM PWR ");
			msbQuery.append("WHERE PWR_NAME = 'PsftPasswordPolicy'");

			//log.info("      fetchPasswordPolicy : Executing Query " + msbQuery.toString());
			PreparedStatement stmt = conn.prepareStatement(msbQuery.toString());

			ResultSet rs = stmt.executeQuery();
			rs.next();
			// Build a list of Policy Rules
			for (int iCount = 1; iCount < 19; iCount++){
				masPolicyData[iCount] = rs.getString(iCount);
			}

			return masPolicyData;

		} catch (Exception ex) {
		  
                  
		    log.error(LoggerMessages.getMessage("ErrorMethodDebug", "OIDUtils/getUserLogin: Exception occured- ",  ex.getMessage()),  ex);
		    ex.printStackTrace();
                  
		  return null;
		}
	}

	/*
	 * isUserPresent
	 *
	 */
	public boolean isUserPresent(String sUserID) throws Exception {
		try {
			//printLog("isUserPresent called");
			// Get Database Connection and execute the SQL Query
			// to check the User
			String sSQL = "SELECT USR_LOGIN FROM USR WHERE USR_LOGIN='"
					+ sUserID + "'";
			tcDataSet moSvpDs = new tcDataSet();
			//printLog("isUserPresent : msbQuery : " + sSQL);
			moSvpDs.setQuery(getDataBase(), sSQL);
			//printLog("isUserPresent : Executing Query");
			moSvpDs.executeQuery();
			if (moSvpDs.getRowCount() == 0) {
				return false;
			} else {
				return true;
			}
		} catch (Exception oexcp) {
			oexcp.printStackTrace();
			printLog("Exception : " + oexcp.getMessage());
			throw oexcp;
		}
	}

	/*
	 * printLog
	 */
	public static void printLog(String message) {
		//printLog(message);
	} // printLog

	private tcDataProvider getDataBase() {
		return xlDataProv;
	}



	/*
	 * main - Entry Point used For testing Purpose
	 */
	public static void main(String[] args) {
		try {
			printLog("Came here");
			PasswordGenerator passGentr = new PasswordGenerator();
			printLog("PasswordGenerator Called");
			String firstName="";
			String[] lastNames = { "Abell", "Ackworth", "Adams",
				"Addicock", "Alban", "Aldebourne", "Alfray", "Alicock", "Allard",
				"Allen", "Allington", "Amberden", "Amcotts", "Amondsham",
				"Andrews", "Annesley", "Ansty", "Archer", "Ardall", "Ardern",
				"Argentein", "Arnold", "Arthur", "Asger", "Ashby", "Ashcombe",
				"Ashenhurst", "Ashton", "Askew", "Asplin", "Astley", "Atherton",
				"Atkinson", "Atlee", "Attilburgh", "Aubrey", "Audeley",
				"Audlington", "Ayde", "Ayleward", "Aylmer", "Aynesworth", "Babham",
				"Babington", "Badby", "Bailey", "Baker", "Balam", "Baldwin",
				"Ballard", "Ballett", "Bammard", "Barber", "Bardolf", "Barefoot",
				"Barker", "Barnes", "Barre", "Barrentine", "Barrett", "Barstaple",
				"Bartelot", "Barton", "Basset", "Bathurst", "Battersby", "Battle",
				"Baynton", "Beauchamp", "Beaumont", "Beaurepaire", "Beckingham",
				"Bedell", "Bedgberry", "Bedingfeld", "Beer", "Beeton", "Bell",
				"Bend", "Bennet", "Benthey", "Berdwell", "Berecraft", "Beresford",
				"Berkhead", "Bernard", "Bernewelt", "Berry", "Berwick", "Best",
				"Bettsthorne", "Bewforest", "Bewley", "Bexley", "Bigley",
				"Billing", "Billingford", "Bingham", "Bird", "Bishop", "Bishopson",
				"Bishoptree", "Blacknall", "Blackwell", "Bladewell", "Blakeley",
				"Blennerhayset", "Blexham", "Blodwell", "Bloom", "Blount",
				"Blundell", "Boddenham", "Bohan", "Boote", "Boothe", "Borrell",
				"Borrow", "Bosby", "Bost", "Bostock", "Boston", "Bothy",
				"Botteler", "Boulder", "Bourne", "Boville", "Bowcer", "Bowett",
				"Bownell", "Bowyar", "Bradbridge", "Bradshawe", "Bradstone",
				"Bramfield", "Brampton", "Branch", "Branwhaite", "Brassie",
				"Braunstone", "Bray", "Brayles", "Brecknock", "Bredham", "Brent",
				"Brett", "Brewiss", "Brewster", "Bridgeman", "Briggs",
				"Brinkhurst", "Bristow", "Brocksby", "Brodeway", "Brodnax",
				"Brokehill", "Brome", "Brook", "Brougham", "Broughton", "Browett",
				"Brown", "Brownflet", "Browning", "Brudenell", "Bryan",
				"Buckingham", "Bulkeley", "Bulstrode", "Burgess", "Burgh",
				"Burghill", "Burgoyne", "Burington", "Burlton", "Burnell",
				"Burney", "Burton", "Bushbury", "Buslingthorpe", "Butler",
				"Byfield", "Caley", "Callthorpe", "Campden", "Canon", "Cantilupe",
				"Carbonall", "Cardiff", "Carew", "Carlyle", "Carter", "Cassy",
				"Castell", "Castletown", "Catesby", "Cavell", "Caxton", "Cely",
				"Chamberlain", "Champneys", "Chanceller", "Chandler", "Chapman",
				"Charles", "Chase", "Chatwyn", "Chauncey", "	", "Cheddar",
				"Chelsey", "Chernock", "Chester", "Chetwood", "Cheverell",
				"Cheyne", "Chichester", "Child", "Chilton", "Chowne", "Chudderley",
				"Church", "Churmond", "Clark", "Clavell", "Claybrook", "Clement",
				"Clerk", "Clifford", "Clifton", "Clitherow", "Clopton", "Cobb",
				"Cobham", "Cobley", "Cockayne", "Cod", "Coddington", "Coffin",
				"Coggshall", "Colby", "Cole", "Colkins", "Collard", "Colmer",
				"Colt", "Colthurst", "Complin", "Compton", "Conquest", "Cooke",
				"Coorthopp", "Coppinger", "Corbett", "Corby", "Cossington",
				"Cosworth", "Cotton", "Courtenay", "Covert", "Cowill", "Cox",
				"Crane", "Cranford", "Crawley", "Cressy", "Crickett", "Cripps",
				"Crisp", "Cristemas", "Crocker", "Crugg", "Cuddon", "Culpepper",
				"Cunningham", "Curtis", "Curzon", "Dagworth", "Dale",
				"Dalingridge", "Damsell", "Danett", "Danvers", "Darcy", "Darley",
				"Daubernon", "Daunce", "Dauncey", "Daundelyon", "Davy", "Dawne",
				"Day", "Deacons", "Dean", "Deering", "Delabere", "Delamere",
				"Delly", "Dencourt", "Dennis", "Denton", "Dericott", "Derington",
				"Desford", "Digby", "Dimmock", "Dinley", "Dixton", "Doddle",
				"Dogmersfield", "Donnett", "Doreward", "Dormer", "Dove", "Dow",
				"Downer", "Draper", "Draw", "Drayton", "Dryden", "Dryland",
				"Dunch", "Duncombe", "Dunham", "Duredent", "Dusteby", "Dye",
				"Dynham", "Edgar", "Edgcombe", "Edgerton", "Edwards", "Eggerley",
				"Eglisfeld", "Elliot", "Ellis", "Elmebrigge", "Emerson", "Engham",
				"Engleford", "Englisch", "Epworth", "Erewaker", "Ermin", "Ertham",
				"Esmund", "Estbury", "Eston", "Etchingham", "Etton", "Everard",
				"Everdon", "Evingar", "Eyer", "Eyston", "Fabian", "Faldo", "Fane",
				"Farindon", "Fayneman", "Felbrigg", "Feld", "Fenton", "Ferrer",
				"Feversham", "Fienley", "Finch", "Findern", "Fineux", "Fisher",
				"Fitton", "Fitzgeoffrey", "Fitzherbert", "Fitzlewis", "Fitzralph",
				"Fitzwarren", "Fitzwilliam", "Fleet", "Fleetwood", "Fleming",
				"Flexney", "Flower", "Fodd", "Fogg", "Foliot", "Foljambe",
				"Follon", "Follywolle", "Folsham", "Fonteyn", "Ford", "Forder",
				"Fortescue", "Fortey", "Fowler", "Fox", "Francey", "Franklin",
				"Fraunces", "Freen", "Freer", "Freville", "Frewell", "Frilende",
				"Frilleck", "Frith", "Froggenhall", "Fromond", "Froste",
				"Frowseloure", "Frye", "Fulburne", "Fulmer", "Furnace", "Gage",
				"Gainsford", "Galey", "	", "Gardiner", "Gare", "Garnis", "Garrard",
				"Garret", "Gascoigne", "Gasper", "Gavell", "Gedding", "Geoffrey",
				"George", "Gerard", "Gerville", "Geste", "Gibbs", "Gifford",
				"Gilbert", "Gill", "Ginter", "Gisborne", "Gittens", "Glennon",
				"Glover", "Gobberd", "Goddam", "Godfrey", "Gold", "Golding",
				"Goldwell", "Gomershall", "Gomfrey", "Gonson", "Good",
				"Goodenouth", "Gooder", "Goodluck", "Goodnestone", "Goodrick",
				"Goodrington", "Goodwin", "Goring", "Gorney", "Gorst",
				"Gosebourne", "Grafton", "Gray", "Greene", "Greenway", "Grenefeld",
				"Greville", "Grey", "Grimbald", "Grobbam", "Grofhurst", "Groston",
				"Grove", "Guildford", "Hackman", "Haddock", "Haddon", "Hadresham",
				"Hakebourne", "Hale", "Hall", "Halley", "Hambard", "Hammer",
				"Hammond", "Hampden", "Hancock", "Hansart", "Harbird", "Harbottle",
				"Harcourt", "Hardy", "Harewell", "Hargreve", "Harlakinden",
				"Harleston", "Harley", "Harpeden", "Harper", "Harris", "Harte",
				"Harvard", "Harwood", "Hasard", "Hatch", "Hatcliff", "Hautreeve",
				"Hawkins", "Hawksworth", "Hawtrey", "Haye", "Hayes", "Hayton",
				"Helm", "Henshawe", "Herleston", "Heron", "Hertcomb", "Hervey",
				"Heydon", "Heywood", "Heyworth", "Higden", "Highgate", "Hilderley",
				"Hill", "Hinson", "Hitchcock", "Hoare", "Hobart", "Hodgson",
				"Holbrook", "Holcott", "Holland", "Holsey", "Holt", "Holton",
				"Hopton", "Horman", "Hornebolt", "Hornley", "Horsey", "Horthall",
				"Horton", "Hosteler", "Hotham", "Howard", "Huddleston", "Hugeford",
				"Hughes", "Hungate", "Hurst", "Hussey", "Hutchinson", "Hyde",
				"Inwood", "Isley", "Jackmann", "Jackson", "James", "Janner",
				"Jarman", "Jay", "Jendring", "Jenney", "Johnson", "Jordan",
				"Joslin", "Joulon", "Jowchet", "Keckilpenny", "Kellett", "Kelly",
				"Kemp", "Kent", "Keriell", "Kesteven", "Key", "Kidwelly",
				"Killigrew", "Killingworth", "Kinge", "Kirkeby", "Kitson",
				"Knighton", "Knivetton", "Knody", "Knoyll", "Knyvett", "Kottow",
				"la Barre", "la Hale", "la Penne", "Lacy", "Lambert", "Lambton",
				"Langston", "Lappage", "Latham", "Latton", "Launceleyn", "Lave",
				"Lawnder", "le Bone", "Leech", "Leeds", "Lehenard", "Leigh",
				"Leighlin", "Leman", "Lenton", "Lestrange", "Letterford",
				"Leventhorpe", "Leverer", "Leveson", "Lewis", "Leynham",
				"Leynthall", "Limsey", "Lind", "Liripine", "Lisle", "Litchfield",
				"	", "Litcott", "Littlebury", "Litton", "Liverich", "Livesey",
				"Lloyd", "Lockton", "Loddington", "Lond", "London", "Long",
				"Longton", "Lovell", "Loveney", "Lowth", "Lucy", "Ludsthorp",
				"Luke", "Lumbard", "Lupton", "Lyfeld", "Lyon", "Makepiece",
				"Malemayns", "Malins", "Malster", "Maltoun", "Manfield", "Manston",
				"Mapilton", "Marcheford", "Markeley", "Marris", "Marsham",
				"Marten", "Mason", "Massingberd", "Maudit", "Mauntell", "Maycott",
				"Maydestone", "Mayne", "Maynwaring", "Mead", "Medeley", "Merden",
				"Mereworth", "Merstun", "Merton", "Metcalf", "Michelgrove",
				"Middleton", "Mill", "Millet", "Millis", "Milner", "Milsent",
				"Moland", "Molins", "Molyngton", "Monde", "Montacute", "Montagu",
				"Moore", "More", "Morecott", "Morley", "Morris", "Mortimer",
				"Moryet", "Motesfont", "Mowfurth", "Mugg", "Mullens", "Muston",
				"Narbridge", "Nash", "Neale", "Nevinson", "Newdegate", "Newman",
				"Noke", "Norbury", "Norden", "Norris", "North", "Northwood",
				"Norton", "Norwich", "Norwood", "Notfeld", "Nottingham", "Nysell",
				"Obson", "Oke", "Oken", "Olingworth", "Oliver", "Osborne",
				"Osillbury", "Osteler", "Outlawe", "Oxenbrigg", "Page", "Pagg",
				"Palmer", "Panshawe", "Papley", "Parker", "Parrett", "Parris",
				"Parsons", "Paston", "Payne", "Peacock", "Peck", "Peckham", "Peel",
				"Pelletoot", "Peltie", "Pemberton", "Penhallick", "Pennebrygg",
				"Perchehay", "Perris", "Perrot", "Perryvall", "Petham", "Petley",
				"Pettit", "Pettwood", "Peyton", "Philips", "Piggott", "Pinnock",
				"Pinty", "Playters", "Plessey", "Plimmswood", "Poff", "Pole",
				"Polsted", "Polton", "Porter", "Portington", "Potter", "Powlett",
				"Pownder", "Pratt", "Pray", "Prelate", "Prophet", "Prowd",
				"Purles", "Pursglove", "Quintin", "Radley", "Rampston", "Ramsey",
				"Ratcliff", "Raudell", "Rawlin", "Rawson", "Raynsford", "Redman",
				"Reed", "Reeve", "Reynes", "Richeman", "Rickhill", "Rickworth",
				"Ringer", "Rippringham", "Risley", "Robbins", "Roberts",
				"Robertson", "Robinson", "Rochester", "Rochforth", "Roland",
				"Rolleston", "Rondel", "Ront", "Roper", "Rous", "Rowdon", "Rowe",
				"Rowlett", "Rowley", "Rudhall", "Rufford", "Ruggenall", "Ruggwain",
				"Rusch", "Russell", "Ryall", "Sacheverell", "Sackville", "Sadler",
				"Saintaubin", "Saintjohn", "Salford", "Salman", "Salter",
				"Saltonstall", "Sampson", "Samuell", "	", "Sanburne", "Sandys",
				"Saunders", "Saunterton", "Savill", "Sayer", "Saynsberry",
				"Scarcliff", "Scobahull", "Scolfield", "Scott", "Scroggs",
				"Scrope", "Sedley", "Selwyn", "Serche", "Sever", "Seymour", "Seys",
				"Sharman", "Shawe", "Sheffield", "Sheraton", "Sherbourne",
				"Sherman", "Shern", "Shevington", "Shilton", "Shingleton",
				"Shipwash", "Shiveley", "Shoesmith", "Shorditch", "Shotbolt",
				"Sibbell", "Silvester", "Simeon", "Simmons", "Sinclair", "Skern",
				"Skipwith", "Sleaford", "Slyfield", "Smith", "Snayth", "Snell",
				"Snelling", "Sotton", "Sparrow", "Spebbington", "Speir", "Spelman",
				"Spencer", "Spettell", "Spicer", "Sprottle", "Sprunt", "Stacy",
				"Stanbury", "Standon", "Stanley", "Stanwix", "Staple", "Staunton",
				"Staverton", "Stepney", "Steven", "Steward", "Stille", "Stockton",
				"Stoddeley", "Stoke", "Stokerton", "Stokes", "Stokey", "Stone",
				"Stoner", "Stoughton", "Strachleigh", "Strader", "Strangewayes",
				"Street", "Strelley", "Stubb", "Styles", "Sulyard", "Sumner",
				"Swan", "Sweetecok", "Swetenham", "Switt", "Tabard", "Tame",
				"Taylor", "Tedcastle", "Thomas", "Thornburgh", "Thorne",
				"Thornton", "Thorpe", "Throckmorton", "Thursby", "Tibbord",
				"Tilghman", "Tindall", "Tiploft", "Topsfield", "Torrington",
				"Totthill", "Town", "Tregonwell", "Trenowyth", "Trevett",
				"Trumpington", "Tubney", "Turner", "Twarby", "Tweedy", "Tyrell",
				"Ufford", "Underhill", "Unton", "Upton", "Urswick", "Vaughan",
				"Vawdrey", "Veldon", "Verney", "Vernon", "Vinter", "Wade",
				"Wadham", "Wake", "Waldegrave", "Waldeley", "Walden", "Walford",
				"Walker", "Wallace", "Walrond", "Walsch", "Waltham", "Walton",
				"Wantell", "Warbulton", "Warde", "Wardyworth", "Warner", "Warren",
				"Wayte", "Webb", "Weeks", "Welbeck", "Welby", "Wellins", "Wenman",
				"Wensley", "West", "Westbrook", "Westlake", "Weston", "Wetherden",
				"Wexcombe", "White", "Whitewood", "Whitton", "Whowood", "Whyting",
				"Widdowson", "Wightman", "Wilkins", "Willardsey", "Willcotts",
				"Williams", "Willis", "Willmer", "Wilmot", "Wilson", "Windham",
				"Wingfield", "Winkle", "Winston", "Winstringham", "Winter",
				"Wiseman", "Withinghall", "Wolfden", "Wolstonton", "Wolton",
				"Wood", "Woodbrygg", "Woodward", "Worsley", "Wotton", "Wreke",
				"Wrenn", "Wright", "Wyard", "Wyatt", "Wyghtham", "Wylde", "Wymer",
				"Wyville", "Yate", "Yaxley", "Yelverton", "Yornold", "Young" };


			String[] firstNames = {	"Thornton", "Thorpe", "Throckmorton", "Thursby", "Tibbord",
			"Tilghman", "Tindall", "Tiploft", "Topsfield", "Torrington",
			"Totthill", "Town", "Tregonwell", "Trenowyth", "Trevett",
			"Trumpington", "Tubney", "Turner", "Twarby", "Tweedy", "Tyrell",
			"Ufford", "Underhill", "Unton", "Upton", "Urswick", "Vaughan",
			"Vawdrey", "Veldon", "Verney", "Vernon", "Vinter", "Wade",
			"Wadham", "Wake", "Waldegrave", "Waldeley", "Walden", "Walford",
			"Walker", "Wallace", "Walrond", "Walsch", "Waltham", "Walton",
			"Wantell", "Warbulton", "Warde", "Wardyworth", "Warner", "Warren",
			"Wayte", "Webb", "Weeks", "Welbeck", "Welby", "Wellins", "Wenman",
			"Wensley", "West", "Westbrook", "Westlake", "Weston", "Wetherden",
			"Wexcombe", "White", "Whitewood", "Whitton", "Whowood", "Whyting",
			"Widdowson", "Wightman", "Wilkins", "Willardsey", "Willcotts",
			"Williams", "Willis", "Willmer", "Wilmot", "Wilson", "Windham",
			"Wingfield", "Winkle", "Winston", "Winstringham", "Winter",
			"Wiseman", "Withinghall", "Wolfden", "Wolstonton", "Wolton",
			"Wood", "Woodbrygg", "Woodward", "Worsley", "Wotton", "Wreke",
			"Wrenn", "Wright", "Wyard", "Wyatt", "Wyghtham", "Wylde", "Wymer",
			"Wyville", "Yate", "Yaxley", "Yelverton", "Yornold", "Young" };


			// printLog("User : " + passGentr.isUserPresent("xelsysadm"));
			long lStart = System.currentTimeMillis();

			String sUserId = "";
			for (int i = 0; i < 100; i++) {

				sUserId = "R" + lastNames[i];
				log.debug("PASSWORD for " + firstNames[i] + " "  + lastNames[i] + "--> "
						+ passGentr.generatePassword(sUserId, firstNames[i],
								lastNames[i]));
			}
			long lEnd = System.currentTimeMillis();
			printLog("Total  Time in Millis : "
					+ (new Long(lEnd - lStart)).toString());

		} catch (Exception excp) {
		}

	} // Main

} // End of Class
