package edu.cuny.oim.scheduletasks;

import Thor.API.Exceptions.tcAPIException;
import Thor.API.Exceptions.tcColumnNotFoundException;

import com.thortech.util.logging.Logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import java.util.HashMap;
import java.util.Vector;

import oracle.iam.identity.rolemgmt.api.RoleManager;
import oracle.iam.identity.rolemgmt.api.RoleManagerConstants;
import oracle.iam.identity.usermgmt.api.UserManagerConstants;
import oracle.iam.platform.Platform;
import oracle.iam.scheduler.vo.TaskSupport;


public class GroupLoad extends TaskSupport {
    /**
     * Logger
     */
    private static Logger log = Logger.getLogger("CUNY.CUSTOM");

    String group;
    String fileName;
    private Vector users = null;

    public void execute(HashMap hashMap) {
        try {
            log.debug("Executing main task");
            users = new Vector();
            //Check to see if all the values in the get attributes are not blank
            if (getTaskAttributes(hashMap)) {
                readFile();
                addUsersToGroup();

            }
            log.debug("Exiting main task");
        } catch (Exception e) {
            log.error("Exception ", e);

        }
    }

    public void addUsersToGroup() throws tcAPIException,
                                         tcColumnNotFoundException {
        log.info("addUsersToGroup() Parameter Variables passed are:" +
                 "roleName=[" + group + "]");
        RoleManager roleMgr = Platform.getService(RoleManager.class);
        for (int i = 0; i < users.size(); i++) {
            try {
                roleMgr.grantRole(RoleManagerConstants.ROLE_NAME, group,
                                  UserManagerConstants.AttributeName.USER_LOGIN.getId(),
                                  users.get(i).toString());
                log.debug("User " + "[" + users.get(i) + "] added to group " +
                          group);
            } catch (Exception ex) {
                log.error("Exception ", ex);
            }
        }
    }


    /**
     * Sets all the attributes from the scheduled task
     * @return
     * @throws Exception
     */
    public boolean getTaskAttributes(HashMap hashMap) throws Exception {

        fileName = (String)hashMap.get("FileName");
        group = (String)hashMap.get("Group Name");

        if (fileName.trim().length() == 0 || group.trim().length() == 0) {
            log.error("You must supply a value for all fields.");
            return false;
        }

        log.info("getTaskAttributes() Parameter Variables passed are:" +
                 "FileName=[" + fileName + "]" + "Group Name=[" + group + "]");

        return true;
    }

    /**
     * Reads the file
     * @throws Exception
     */
    public void readFile() throws Exception {
        log.info("Reading File " + fileName);
        File f = new File(fileName);

        BufferedReader in = new BufferedReader(new FileReader(f));
        String str = "";

        int counter = 0;
        while ((str = in.readLine()) != null) {
            log.debug("***** Reading line " + counter + " *****");
            counter++;
            String user = str.trim();
            if (user.length() != 0) {
                log.debug("User Id:" + user);
                users.add(user);
            }
        }
    }

    @Override
    public HashMap getAttributes() {
        return null;
    }

    @Override
    public void setAttributes() {
    }
}
