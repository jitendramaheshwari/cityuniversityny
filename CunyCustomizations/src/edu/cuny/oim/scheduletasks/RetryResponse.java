package edu.cuny.oim.scheduletasks;

import java.util.Vector;

import com.thortech.util.logging.Logger;

import Thor.API.Operations.tcProvisioningOperationsIntf;

import Thor.API.Exceptions.tcAPIException;
import Thor.API.Exceptions.tcTaskNotFoundException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import oracle.iam.platform.Platform;
import oracle.iam.scheduler.vo.TaskSupport;

public class RetryResponse extends TaskSupport {

    public final String CLASS_NAME = "RetryScheduledTasks()";
    private String resObj = "";
    private Vector taskKeys = null;
    /**
     * Logger
     */

    private static Logger log = Logger.getLogger("CUNY.CUSTOM");

    //triggered task when performing a reconciliation event
    public void execute(HashMap hashMap) {
        try {
            log.debug("Starting Scheduled Task ----- " + CLASS_NAME + " ----->");
            log.debug("Executing main task");
            taskKeys = new Vector();
            String taskName = (String) hashMap.get("Task Name");
            String resourceObject = (String) hashMap.get("Resource Object");
            String responseCode = (String) hashMap.get("Response Code");
            String nullValue = (String) hashMap.get("Null Value");

            resObj = resourceObject;

            findTasks(taskName, resourceObject, responseCode, nullValue);

            if (taskKeys.size() != 0) {
                retryRejectedTasks();
            }

            log.debug("Ending main task");

        } catch (Exception e) {
            log.error("Exception", e);
        }

    }

    /**
     * Searches for all tasks that are in a rejected state Reassigns all tasks
     * to appropriate admin group for organization
     *
     * @throws Exception
     */
    public void findTasks(String taskName, String resourceObject, String responseCode, String nullValue) {
        log.info("Running " + CLASS_NAME + ".findTasks() ----->");
        log.debug("taskName:" + taskName);
        log.debug("resourceObject:" + resourceObject);
        log.debug("responseCode:" + responseCode);
        Connection oimDBConnection = null;
        ResultSet queryResult = null;
        Statement queryStmt = null;

        String query = "";

        query = "select oti.sch_key "
                + "from oti, mil, obj, pkg, orc, usr, sch "
                + "where oti.mil_key=mil.mil_key "
                + "and oti.pkg_key=pkg.pkg_key "
                + "and pkg.obj_key=obj.obj_key "
                + "and oti.orc_key=orc.orc_key "
                + "and orc.usr_key=usr.usr_key "
                + "and oti.sch_key=sch.sch_key "
                + "and oti.sch_status='R' "
                + "and mil.mil_name='" + taskName + "' "
                + "and obj.obj_name like '" + resourceObject + "' ";

        if (responseCode.indexOf("(") != -1) {
            if (nullValue.equalsIgnoreCase("TRUE")) {
                query += "AND (sch.sch_data in " + responseCode + " or sch.sch_data is null) ";
            } else {
                query += "and sch.sch_data in " + responseCode + " ";
            }
        } else {
            if (nullValue.equalsIgnoreCase("TRUE")) {
                query += "AND (sch.sch_data like '" + responseCode + "' or sch.sch_data is null) ";
            } else {
                query += "and sch.sch_data like '" + responseCode + "' ";
            }
        }

        log.debug(query);

        try {
            oimDBConnection = Platform.getOperationalDS().getConnection();
            queryStmt = oimDBConnection.createStatement();
            queryResult = queryStmt.executeQuery(query);

            while (queryResult.next()) {
                log.debug("Task Key: " + queryResult.getString("SCH_KEY"));
                taskKeys.add(String.valueOf(queryResult.getString("SCH_KEY")));
            }

        } catch (SQLException ex) {
            log.error("Exception ", ex);
        } finally {
            try {
                if (queryResult != null) {
                    queryResult.close();
                }
            } catch (Exception e) {
                log.error("Exception ", e);
            }
            try {
                if (queryStmt != null) {
                    queryStmt.close();
                }
            } catch (Exception e) {
                log.error("Exception ", e);
            }
            try {
                if (oimDBConnection != null) {
                    oimDBConnection.close();
                }
            } catch (Exception e) {
                log.error("Exception ", e);
            }
        }

    }

    public void retryRejectedTasks() {
        log.info("Running " + CLASS_NAME + ".retryRejectedTasks() ----->");
        tcProvisioningOperationsIntf provIntf = Platform.getService(Thor.API.Operations.tcProvisioningOperationsIntf.class);
        log.debug("Total Tasks to retry:" + taskKeys.size());

        for (int i = 0; i < taskKeys.size(); i++) {
            long key = Long.parseLong(taskKeys.get(i).toString());
            try {
                provIntf.retryTask(key);
                log.debug("Task key " + key + " retrying(" + i + " " + resObj + ")");
            } catch (tcAPIException e) {
                log.error("Exception ", e);
            } catch (tcTaskNotFoundException e) {
                log.error("Exception ", e);
            }

        }
    }

    @Override
    public HashMap getAttributes() {
        return null;
    }

    @Override
    public void setAttributes() {
    }
}
