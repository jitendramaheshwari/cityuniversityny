/**
 * 
 */
package edu.cuny.oim.scheduletasks;

import java.util.Vector;

import Thor.API.Base.tcUtilityOperationsIntf;
import Thor.API.Exceptions.tcAPIException;
import Thor.API.Exceptions.tcBulkException;
import Thor.API.Exceptions.tcUserNotFoundException;
import Thor.API.Operations.tcFormInstanceOperationsIntf;
import Thor.API.Operations.tcGroupOperationsIntf;
import Thor.API.Operations.tcLookupOperationsIntf;
import Thor.API.Operations.tcUserOperationsIntf;

import com.thortech.util.logging.Logger;
import com.thortech.xl.dataaccess.tcDataSet;
import com.thortech.xl.dataaccess.tcDataSetException;
import com.thortech.xl.scheduler.tasks.SchedulerBaseTask;

/**
 * @author kpinsky
 *
 */

public class DeprovisionDateUpdate extends SchedulerBaseTask{
	public final String CLASS_NAME = "DeprovisionDateUpdate()";
	private Vector users = null;
	
	/**
	 * Logger
	 */
	
	public static Logger log = Logger.getLogger("CUNY.CUSTOM");
	
	
	//triggered task when performing a reconciliation event
	public void execute(){
			
			boolean result = false;
			System.out.println("Starting Scheduled Task ----- " + CLASS_NAME  + " ----->");
			System.out.println("Executing main task");				
			users = new Vector();
			log.debug("Finding users with Terminated Disbale Date");
			result = query("USR_UDF_TERM_DATE");
			if (result){
				disableUsers(users);
			}
			
			System.out.println("Ending main task");			



	}
	
	/**
	 * Performs a query against the OIM database
	 * @param dateField - user defined field for the date
	 * @return - vector containing list of users who match criteria
	 */
	public boolean query(String dateField){
		log.info("Running " + CLASS_NAME  + ".query() ----->");
		boolean result = false;	
		tcDataSet dsList = new tcDataSet();			 
		String query = "select USR_KEY from USR " + 
			"where USR_STATUS='Active' " +
			"and " + dateField + " < SYSDATE";
		log.debug(query);

		
		try {
			dsList.setQuery(getDataBase(), query);		
			dsList.executeQuery();
			
			if (!dsList.isEmpty()){
				log.debug("Total Rows Found:" + dsList.getTotalRowCount());
				result = true;				
				for (int i = 0; i < dsList.getTotalRowCount(); i++) {
					dsList.goToRow(i); 	
					System.out.println(dsList.getLong("USR_KEY"));
					users.add(dsList.getLong("USR_KEY"));
					log.debug("User Key: " + dsList.getLong("USR_KEY"));
				}				
			}else{
				log.debug("No Users Found");
				result = false;
			}
		} catch (tcDataSetException e) {
			e.printStackTrace();
			result = false;
		}
		
		return result;
			
	}
	
	/**
	 * Disables users
	 * @param users - list of users
	 */
	public void disableUsers(Vector users)  {
		log.info("Running " + CLASS_NAME  + ".disableUsers() ----->");
		userIntf = (tcUserOperationsIntf)getUtilityOps("Thor.API.Operations.tcUserOperationsIntf");
		long[] keys =  new long[users.size()];
		for (int i=0;i<users.size();i++){
			keys[i] =   Long.parseLong(String.valueOf(users.get(i)));
		}		
		
		try {
			userIntf.disableUsers(keys);
			log.debug("Users disabled[" + users.size() + "] successfully");
		} catch (tcAPIException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (tcUserNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (tcBulkException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
			
	}
	
	//required to connect to the database without a login
	public tcUtilityOperationsIntf getUtilityOps(String utilityName){
		try {
			return (tcUtilityOperationsIntf)super.getUtility(utilityName);
		}
		catch (tcAPIException e){
			e.printStackTrace();
		}
		return null;
	}
	
	protected tcUserOperationsIntf userIntf;

}