package edu.cuny.oim.scheduletasks;

import Thor.API.Exceptions.tcAPIException;

import com.thortech.util.logging.Logger;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import java.util.HashMap;
import java.util.Vector;

import oracle.iam.platform.Platform;
import oracle.iam.reconciliation.api.ReconOperationsService;
import oracle.iam.scheduler.vo.TaskSupport;


public class ReapplyMatchingRules extends TaskSupport{
	private final String CLASS_NAME = "ReapplyMatchingRules()";
	private Vector events = null;

	/**
	 * Logger
	 */
	
	private static Logger log = Logger.getLogger("CUNY.CUSTOM");
	
	//triggered task when performing a reconciliation event
	public void execute(HashMap hashMap){
		try{
			log.debug("Starting Scheduled Task ----- " + CLASS_NAME  + " ----->");
			log.debug("Executing main task");		
			events = new Vector();
		
			log.debug("Recon List:" + events.size());
			findEvents();
			if (isStop()){
				return;
			}
			if (events.size()!=0){
				if (isStop()){
					return;
				}
				for (int i=0;i<events.size();i++){
					if (isStop()){
						return;
					}
					reApplyEvent(events.get(i).toString());
				}
			}
			
			log.debug("Ending main task");			

		}catch(Exception e){
		    log.error("Exception ", e);
		}

	}
	
	public void findEvents() throws tcAPIException{
		log.debug("findEvents() Parameter Variables passed are:");

		    Connection oimDBConnection = null;
		    ResultSet queryResult = null;
		    Statement queryStmt = null;
		
		try{
				
			String query = "Select DISTINCT rcd.rcd_value " +
				" ,rce.rce_key " +
				" ,rcd.rcd_create " +
				" ,obj.obj_name " +
				" ,rce.rce_status " +
				" From rce, obj, rcd, orf " +
				" Where obj.obj_key = rce.obj_key " +
				" AND rcd.orf_key = orf.orf_key " +
				" AND rce.rce_key = rcd.rce_key " +
				" AND rce_status in ('Event Received') " +
				" AND orf.orf_fieldname in ('Users.EmplId') " +
				" order by obj.obj_name, rce.rce_key";
			
				log.debug("Query:" + query);
			    oimDBConnection = Platform.getOperationalDS().getConnection();
			    queryStmt = oimDBConnection.createStatement();
			    queryResult = queryStmt.executeQuery(query);

				while (queryResult.next()) {
					if (isStop()){
						return;
					}
					String reconKey = queryResult.getString("RCE_KEY");
					log.debug("ReconKey:" + reconKey);
					events.add(reconKey);	
				}
			} catch (Exception e) {
			    log.error("Exception ", e);
			}  finally {
            try {
                if (queryResult != null) {
                    queryResult.close();
                }
            } catch (Exception e) {
                log.error("Exception ", e);
            }
            try {
                if (queryStmt != null) {
                    queryStmt.close();
                }
            } catch (Exception e) {
                log.error("Exception ", e);
            }
            try {
                if (oimDBConnection != null) {
                    oimDBConnection.close();
                }
            } catch (Exception e) {
                log.error("Exception ", e);
            }
        }
				
		}
	
	public void reApplyEvent(String reconKey){
		log.debug("reApplyEvent() Parameter Variables passed are:" +
				"reconKey=[" + reconKey + "]" );
		ReconOperationsService reconIntf = Platform.getService(ReconOperationsService.class);
		long key = Long.parseLong(reconKey);
		try {
			if (isStop()){
				return;
			}
			reconIntf.processReconciliationEvent(key);
			log.debug("Successfully processed event:" + key);
		} catch (tcAPIException e) {
			log.debug("Error processing event:" + key);
		}
		
	}
    @Override
    public HashMap getAttributes() {
        return null;
    }

    @Override
    public void setAttributes() {
    }
}