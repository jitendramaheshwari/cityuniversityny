package edu.cuny.oim.scheduletasks;

import Thor.API.Operations.tcUserOperationsIntf;
import Thor.API.tcResultSet;
import Thor.API.Exceptions.tcAPIException;
import Thor.API.Exceptions.tcColumnNotFoundException;
import Thor.API.Exceptions.tcUserNotFoundException;
import Thor.API.Exceptions.tcBulkException;


import com.thortech.util.logging.Logger;
import com.thortech.xl.scheduler.tasks.SchedulerBaseTask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class UnlockUsersTask extends SchedulerBaseTask {
    
    public static final String K_LOCK_STATUS_ATTR_NAME = "Users.Lock User";
    public static final String K_USER_KEY_ATTR_NAME = "Users.Key";
    public static final String K_LOCK_DATE_ATTR_NAME   = "USR_UDF_LOCK_TS";
    
    static Logger logger = Logger.getLogger("CUNY.CUSTOM");
    
    private int foundUsers = 0;
    private volatile boolean mustStop = false;

    public UnlockUsersTask() {}
    
    public void execute() {
        
        logger.info("Starting unlock users task");
        
        long currentTime = System.currentTimeMillis();
        List userKeysList = new ArrayList();
        
        try {
            tcUserOperationsIntf userOper = (tcUserOperationsIntf)this.getUtility("Thor.API.Operations.tcUserOperationsIntf");
            
            Map searchMap = new HashMap();
            searchMap.put(K_LOCK_STATUS_ATTR_NAME,"1");
            
            tcResultSet rs = userOper.findAllUsers(searchMap);
            foundUsers = rs.getTotalRowCount();
            
            logger.debug(foundUsers+" locked users found");
            
            for (int i=0; i<foundUsers; i++) {
                if (this.mustStop) {
                    break;
                }
                rs.goToRow(i);
                long lockedTime = rs.getTimestamp(K_LOCK_DATE_ATTR_NAME).getTime();
                logger.debug(String.valueOf(lockedTime));
                
                if ((currentTime-lockedTime) > (1*1000*60*60)) {
                    userKeysList.add(Long.parseLong(rs.getStringValue(K_USER_KEY_ATTR_NAME)));
                }
            }
            
            if (!this.mustStop && userKeysList.size()>0) {
                long[] usersKey = new long[userKeysList.size()];
                logger.debug("Users to unlock --> "+userKeysList.toString());
                for (int i=0;i<userKeysList.size();i++) {
                    usersKey[i] = ((Long)userKeysList.get(i)).longValue();
                }
                logger.debug("Trying to unlock "+usersKey.length+" users");
                userOper.unlockUsers(usersKey);
            }
            else {
                if (userKeysList.size()==0) {
                    logger.info("No users to unlock");
                }
                if (this.mustStop) {
                    logger.info("Stop signal received, finishing task");
                }
            }
            
        }
        catch (tcColumnNotFoundException e) {
            logger.error("Error while trying to unlock users"+ e.getMessage(),e);
        }
        catch (tcUserNotFoundException e) {
            logger.error("Error while trying to unlock users"+ e.getMessage(),e);
        }
        catch (tcBulkException e) {
            logger.error("Error while trying to unlock users"+ e.getMessage(),e);
        }        
        catch (tcAPIException e) {
            logger.error("Error while trying to unlock users"+ e.getMessage(),e);
        }
        
        logger.debug("Finishing unlock users task");        
    }
}
