package edu.cuny.oim.scheduletasks;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Hashtable;

import com.thortech.util.logging.Logger;
import Thor.API.Operations.tcReconciliationOperationsIntf;
import Thor.API.Operations.tcFormInstanceOperationsIntf;
import Thor.API.Operations.tcUserOperationsIntf;
import Thor.API.Operations.tcGroupOperationsIntf;
import Thor.API.Operations.tcITResourceInstanceOperationsIntf;
import Thor.API.Operations.tcLookupOperationsIntf; 
import Thor.API.Operations.tcObjectOperationsIntf;
import Thor.API.Operations.tcOrganizationOperationsIntf; 
import Thor.API.Operations.tcProvisioningOperationsIntf; 	
import com.thortech.xl.scheduler.tasks.SchedulerBaseTask;
import Thor.API.Base.tcUtilityOperationsIntf;
import Thor.API.Exceptions.tcAPIException;
import Thor.API.tcResultSet;

public class FlatFile extends SchedulerBaseTask{
	/**
	 * Logger
	 */
	String fileName;
	String resourceObject;
	String useLookup;
	String lookup;
	String organizationName;
	String password;
	String role;
	String type;
	String[] headers;
	       
	public static Logger log = Logger.getLogger("CUNY.CUSTOM");
	
	/*public static void main(String[] args) throws Exception{
		FlatFile read = new FlatFile();
		read.readFile();
	}*/
	
	public void execute(){
		try{
			System.out.println("Executing main task");
			//Check to see if all the values in the get attributes are not blank
			if (getTaskAttributes()){
				readFile();
			}
			System.out.println("Exiting main task");
		}catch(Exception e){

		}
	}
	
	/**
	 * Sets all the attributes from the scheduled task
	 * @return
	 * @throws Exception
	 */
	public boolean getTaskAttributes() throws Exception{
		
		fileName = getAttribute("FileName");
		resourceObject = getAttribute("Resource Object");
		lookup = getAttribute("Lookup");
		useLookup = getAttribute("Use Lookup");
		organizationName = getAttribute("Organization");
		password = getAttribute("Password");
		role = getAttribute("Users.Role");
		type = getAttribute("Users.Xellerate Type");

		if (fileName.trim().length()==0 || resourceObject.trim().length()==0 || 
				lookup.trim().length()==0 || useLookup.trim().length()==0 || 
			organizationName.trim().length()==0 || password.trim().length()==0 || 
			role.trim().length()==0 || type.trim().length()==0){
			
			System.out.println("You must supply a value for all fields.");
			return false;
		}
		
		System.out.println("getTaskAttributes() Parameter Variables passed are:" + 
				"FileName=[" + fileName + "]" + 
				"Resource Object=[" + resourceObject + "]" +
				"Lookup=[" + lookup + "]" + 
				"Use Lookup=[" + useLookup + "]" + 
				"Organization=[" + organizationName + "]" +
				"Password=[" + "********" + "]" +
				"Users.Role=[" + role + "]" +				
				"Users.Xellerate Type=[" + type + "]");
		
		return true;
	}
	
	/**
	 * Reads the file
	 * @throws Exception
	 */
	public void readFile() throws Exception{
		System.out.println("Reading File " + fileName);
		Hashtable userHash = new Hashtable();
		File f = new File (fileName);
		
	    BufferedReader in = new BufferedReader(new FileReader(f));
	    String str = in.readLine();
	    
	    //Headers
	    headers = str.split("\t");
    	for (int i=0;i<headers.length;i++){
    		log.debug("Header:" + headers[i]);	    		
    	}
    	
		if (useLookup.equalsIgnoreCase("true")){
			lookupIntf = (tcLookupOperationsIntf)getUtilityOps("Thor.API.Operations.tcLookupOperationsIntf");
			Hashtable lookupHash = new Hashtable();
			//Lookup Definition.Lookup Code Information.Code Key
		
			for (int i=0;i<headers.length;i++){
				log.debug(headers[i]);
				lookupHash.clear();
				lookupHash.put("Lookup Definition.Lookup Code Information.Code Key", headers[i]);
				tcResultSet lookupSet = lookupIntf.getLookupValues(lookup, lookupHash);
				
				String newHeader = lookupSet.getStringValue("Lookup Definition.Lookup Code Information.Decode");
				log.debug("Old Header[" + headers[i] + "] New Header[" + newHeader + "]");
				headers[i] = newHeader;
			}
		}

		int counter = 1;
	    while ((str = in.readLine()) != null) {	
	    	log.debug("***** Reading line " + counter + " *****");
	    	counter++;
	    	String[] line = str.split("\t");
	    	userHash.clear();
	    	for (int i=0;i<line.length;i++){
	    		//Populate Hashtable
	    		if (line[i].trim().length()!=0){
	    			//log.debug(headers[i] + ":" + line[i]);
	    			userHash.put(headers[i].trim(), line[i].trim());	    			
	    		}
	    	}

	    	//Check to see if user exists
	    	if (resourceObject.equalsIgnoreCase("Xellerate User")){
		    	boolean exists = checkIfExists(userHash.get("Users.User ID").toString());
		    	if (exists){
		    		userHash.remove("Organizations.Name");
		    		userHash.remove("Password");	
		    		updateUser(userHash);	    		
		    	}else{
		    		createUser(userHash);
		    	}   
	    	}else{
	    		long event = reconEvent(userHash);
	    		log.debug("New Recon event: " + event);
	    	}
	    	
	    	//long event = reconEvent(userHash);
    		//log.debug("New Recon event: " + event);
	    }
	}
	
	/**
	 * Translates the headers from encoded values to decoded
	 * @param headers - array value of all the headers from the file
	 * @return
	 * @throws Exception
	 */	
	public String[] useLookupHeaders (String[] headers) throws Exception{
		log.debug("Changing column headers to columns in lookup");
		
		lookupIntf = (tcLookupOperationsIntf)getUtilityOps("Thor.API.Operations.tcLookupOperationsIntf");
		Hashtable lookupHash = new Hashtable();
		//Lookup Definition.Lookup Code Information.Code Key
		
		for (int i=0;i<headers.length;i++){
			log.debug(headers[i]);
			lookupHash.clear();
			lookupHash.put("Lookup Definition.Lookup Code Information.Code Key", headers[i]);
			tcResultSet lookupSet = lookupIntf.getLookupValues(lookup, lookupHash);
			
			String newHeader = lookupSet.getStringValue("Lookup Definition.Lookup Code Information.Decode");
			log.debug("Old Header[" + headers[i] + "] New Header[" + newHeader + "]");
			headers[i] = newHeader;
		}
		
		return headers;	
	}
	
	/**
	 * Check if user already exists
	 * @param userID - user id field
	 * @return
	 * @throws Exception
	 */
	public boolean checkIfExists(String userID) throws Exception{
		userIntf = (tcUserOperationsIntf)getUtilityOps("Thor.API.Operations.tcUserOperationsIntf");
		Hashtable newUserHash = new Hashtable();
		newUserHash.put("Users.User ID", userID);
		tcResultSet userSet = userIntf.findAllUsers(newUserHash);
		if (userSet.isEmpty()){
			log.debug("User does not exist");
			return false;
		}else{
			log.debug("User exists already");
			return true;
		}		
	}
	
	/**
	 * Creates OIM User
	 * @param userHash - hash containing all the entries from the flat file
	 * for an individual line
	 * @throws Exception
	 */
	public void createUser(Hashtable userHash) throws Exception{
		userIntf = (tcUserOperationsIntf)getUtilityOps("Thor.API.Operations.tcUserOperationsIntf");
		orgIntf = (tcOrganizationOperationsIntf)getUtilityOps("Thor.API.Operations.tcOrganizationOperationsIntf");
		
		userHash.put("Users.Password", password);
		userHash.put("Users.Xellerate Type", type);
		userHash.put("Users.Role", role);
		userHash.put("Organizations.Organization Name", organizationName);
				
    	if(!userHash.containsKey("Users.User ID") || !userHash.containsKey("Users.Password") ||
    			!userHash.containsKey("Users.First Name") || !userHash.containsKey("Users.Last Name") ||
    			!userHash.containsKey("Users.Xellerate Type") || !userHash.containsKey("Users.Role") ||
    			!userHash.containsKey("Organizations.Organization Name")){
    		log.debug("!!!   Missing required fields   !!!");
    		return;
    	}		
		
		if (userHash.containsKey("Users.Manager Login")){
			String managerKey = getManager(userHash.get("Users.Manager Login").toString());
			userHash.remove("Users.Manager Login");
			if (managerKey.length()!=0){
				userHash.put("Users.Manager Key", managerKey);				
			}
			
		}
    	
		//Find the organization key
		Hashtable orgHash = new Hashtable();
		orgHash.put("Organizations.Organization Name", userHash.get("Organizations.Organization Name"));
		tcResultSet orgSet = orgIntf.findOrganizations(orgHash);
		
		//Verifies the organization is valid
		if (orgSet.isEmpty()){
			log.debug("Organization " + userHash.get("Organizations.Name") + " does not exist");
		}else{
			userHash.remove("Organizations.Name");
			String organizationKey = String.valueOf(orgSet.getLongValue("Organizations.Key"));
			log.debug("Organization Key: " + organizationKey);
			userHash.put("Organizations.Key", organizationKey);
			try{
				long userKey = userIntf.createUser(userHash);
				log.debug("User created successfully with userKey " + userKey);
			}catch(Exception e){
				log.debug("User not created due to errors");
			}
		}		
	}
	

	/**
	 * Updates OIM User
	 * @param userHash
	 * @throws Exception
	 */
	public void updateUser(Hashtable userHash) throws Exception{
		userIntf = (tcUserOperationsIntf)getUtilityOps("Thor.API.Operations.tcUserOperationsIntf");
				
		if (userHash.containsKey("Users.Manager Login")){
			String managerKey = getManager(userHash.get("Users.Manager Login").toString());
			userHash.remove("Users.Manager Login");
			userHash.put("Users.Manager Key", managerKey);
		}
		
		Hashtable usrSearch = new Hashtable();
		usrSearch.put("Users.User ID", userHash.get("Users.User ID").toString());
		tcResultSet userSet = userIntf.findAllUsers(usrSearch);
				
		try{
			userIntf.updateUser(userSet, userHash);
			log.debug("User " + userHash.get("Users.User ID") + " updated successfully");
			//return true;
		}catch(Exception e){
			log.debug("Error updating user " + userHash.get("Users.User ID"));
			//e.printStackTrace();
			//return false;			
		}		
	}
	
	public String getManager(String managerLogin) throws Exception{
		userIntf = (tcUserOperationsIntf)getUtilityOps("Thor.API.Operations.tcUserOperationsIntf");
		
		Hashtable usrSearch = new Hashtable();
		usrSearch.put("Users.User ID", managerLogin);
		tcResultSet userSet = userIntf.findAllUsers(usrSearch);
		if (!userSet.isEmpty()){
			return String.valueOf(userSet.getLongValue("Users.Key"));			
		}else{
			log.debug("Manager is not a valid OIM user.");
			return "";
		}
	}
	
	public long reconEvent(Hashtable userHash) throws Exception{
		reconIntf = (tcReconciliationOperationsIntf)getUtilityOps("Thor.API.Operations.tcReconciliationOperationsIntf");
		long event = 0;
		
		boolean reconEvent = reconIntf.ignoreEvent(resourceObject, userHash);
		log.debug("reconEvent: " + reconEvent);
		if (!reconEvent){
			event = reconIntf.createReconciliationEvent(resourceObject, userHash, true);
		}
		return event;		
	}
	
	//required to connect to the database without a login
	public tcUtilityOperationsIntf getUtilityOps(String utilityName){
		try {
			return (tcUtilityOperationsIntf)super.getUtility(utilityName);
		}
		catch (tcAPIException e){
			e.printStackTrace();
		}
		return null;
	}
	
	//examples of how to create an instance for a specific operation
	protected tcReconciliationOperationsIntf reconIntf;
	//reconIntf = (tcReconciliationOperationsIntf)getUtilityOps("Thor.API.Operations.tcReconciliationOperationsIntf");
	protected tcUserOperationsIntf userIntf;
	//userIntf = (tcUserOperationsIntf)getUtilityOps("Thor.API.Operations.tcUserOperationsIntf");
	protected tcFormInstanceOperationsIntf formIntf; 
	//userIntf = (tcUserOperationsIntf)getUtilityOps("Thor.API.Operations.tcFormInstanceOperationsIntf");
	protected tcGroupOperationsIntf groupIntf; 
	//groupIntf = (tcGroupOperationsIntf)getUtilityOps("Thor.API.Operations.tcGroupOperationsIntf");
	protected tcITResourceInstanceOperationsIntf itresourceIntf;
	//itresourceIntf = (tcITResourceInstanceOperationsIntf)getUtilityOps("Thor.API.Operations.tcITResourceInstanceOperationsIntf");
	protected tcLookupOperationsIntf lookupIntf; 
	//lookupIntf = (tcLookupOperationsIntf)getUtilityOps("Thor.API.Operations.tcLookupOperationsIntf");
	protected tcObjectOperationsIntf objectIntf;
	//objectIntf = (tcObjectOperationsIntf)getUtilityOps("Thor.API.Operations.tcObjectOperationsIntf");
	protected tcOrganizationOperationsIntf orgIntf; 
	//orgIntf = (tcOrganizationOperationsIntf)getUtilityOps("Thor.API.Operations.tcOrganizationOperationsIntf");
	protected tcProvisioningOperationsIntf provIntf; 
	//provIntf = (tcProvisioningOperationsIntf)getUtilityOps("Thor.API.Operations.tcProvisioningOperationsIntf");
	//reqIntf = (tcRequestOperationsIntf)getUtilityOps("Thor.API.Operations.tcRequestOperationsIntf");	
}