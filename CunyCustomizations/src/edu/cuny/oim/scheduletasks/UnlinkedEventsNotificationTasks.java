package edu.cuny.oim.scheduletasks;

import Thor.API.Base.tcUtilityOperationsIntf;
import Thor.API.Exceptions.tcAPIException;
import Thor.API.Operations.tcGroupOperationsIntf;
import Thor.API.Operations.tcUserOperationsIntf;
import Thor.API.tcResultSet;

import com.thortech.util.logging.Logger;
import com.thortech.xl.dataaccess.tcDataSet;
import com.thortech.xl.dataobj.util.tcEmailNotificationUtil;
import com.thortech.xl.scheduler.tasks.SchedulerBaseTask;

import java.util.Hashtable;
import java.util.Vector;

public class UnlinkedEventsNotificationTasks extends SchedulerBaseTask{

    public final String CLASS_NAME = "UnlinkedEventsNotificationTasks()";

    private String adminGroupName  = "";
    private String url = "";
    private String from = "";
    private tcUserOperationsIntf userIntf;
    private tcGroupOperationsIntf groupIntf;

    private static Logger log = Logger.getLogger("CUNY.CUSTOM");

    public void execute(){

        try{

            log.info("Starting Scheduled Task ----- " + CLASS_NAME  + " ----->");
            log.info("Executing main task");

            adminGroupName = this.getAttribute("Admin Group");
        	url = getAttribute("OIM URL");
        	from = getAttribute("From Address");
        	log.debug("Admin Group=[" + adminGroupName + "]OIM URL[" + url + "]From Address[" + from + "]");

            if (adminGroupName!=null && !adminGroupName.equals("")) {
                verifyEvents();
            }
            else {
                log.info("Admin Group not informed, task can not run");
            }

            log.info("Ending main task");

        } catch(Exception e){
            log.error("Error executing the task "+e.getMessage());
            log.error("",e);
        }

    }

    //required to connect to the database without a login
    public tcUtilityOperationsIntf getUtilityOps(String utilityName) throws tcAPIException{
        return super.getUtility(utilityName);
    }



    /**
     * Searches for all tasks that are in a rejected state
     * Reassigns all tasks to appropriate admin group for organization
     * @throws Exception
     */
    public void verifyEvents() throws Exception{

        log.info("Running " + CLASS_NAME  + ".verifyEvents() ----->");

        groupIntf = (tcGroupOperationsIntf)getUtilityOps("Thor.API.Operations.tcGroupOperationsIntf");
        userIntf = (tcUserOperationsIntf)getUtilityOps("Thor.API.Operations.tcUserOperationsIntf");

        Hashtable groupHash = new Hashtable();
        groupHash.put("Groups.Group Name", this.adminGroupName);
        tcResultSet groupSet = groupIntf.findGroups(groupHash);

        if (!groupSet.isEmpty()) {
            tcDataSet dsList = new tcDataSet();

            String query =  " SELECT RCE.RCE_KEY, RCE.RCE_STATUS, ORF.ORF_FIELDNAME, RCD.RCD_VALUE, TO_CHAR(RCE.RCE_CREATE,'dd-MON-yyyy hh:mi PM') CREATION_DATE " +
                            "  FROM ORF, RCE, RCD " +
                            " WHERE RCE.RCE_KEY=RCD.RCE_KEY " +
                            "   AND ORF.ORF_KEY=RCD.ORF_KEY " +
                            "   AND RCE.RCE_STATUS = 'Event Received' " +
                            "   AND ORF.ORF_FIELDNAME in ('EmplId', 'FirstName','LastName', 'ManagerEmplID', 'PrimaryAffiliation') " +
                            "   AND trunc(RCE.RCE_CREATE) = trunc(sysdate-1) " +
                            "   ORDER BY RCE.RCE_KEY, ORF.ORF_FIELDNAME";


            dsList.setQuery(getDataBase(), query);
            dsList.executeQuery();

            Vector emailList = new Vector();

            if (!dsList.isEmpty()){

                log.debug("Total Rows: " + dsList.getTotalRowCount());

                String lastKey = "";
                String eventKey = "";
                String eventStatus = "";
                String fieldName = "";
                String fieldValue = "";
                String eventCreationDate = "";

                StringBuilder strBuilder = new StringBuilder();

                for (int k = 0; k < dsList.getRowCount(); k++) {

                    dsList.goToRow(k);

                    eventKey = dsList.getString("RCE_KEY");
                    eventStatus = dsList.getString("RCE_STATUS");
                    fieldName = dsList.getString("ORF_FIELDNAME");
                    fieldValue = dsList.getString("RCD_VALUE");
                    eventCreationDate = dsList.getString("CREATION_DATE");

                    if (!lastKey.equals(eventKey)) {
                        strBuilder.append("\n");
                        strBuilder.append("Event Key: "+eventKey+"\n");
                        strBuilder.append("Event Status: "+eventStatus+"\n");
                    }
                    strBuilder.append("        "+fieldName+": "+fieldValue+"\n");
                    lastKey=eventKey;
                }

                tcResultSet memberList = groupIntf.getAllMembers(groupSet.getLongValue("Groups.Key"));
                if (!memberList.isEmpty()){
                    Hashtable userHash = new Hashtable();
                    for (int i=0;i<memberList.getTotalRowCount();i++){
                        userHash.clear();
                        memberList.goToRow(i);
                        userHash.put("Users.Key", String.valueOf(memberList.getLongValue("Users.Key")));
                        tcResultSet userSet = userIntf.findAllUsers(userHash);
                        String email = userSet.getStringValue("Users.Email");
                        if (email.trim().length()!=0){
                                emailList.add(userSet.getStringValue("Users.Email"));
                        }
                    }
                }

                String subject = "CUNYFirst - Recon Events not linked";
                String body = "The following reconciliation events have not been linked to any OIM user:\n";
                body+= "Possible causes are null or invalid entries, or manager looping.\n";
                body+=strBuilder.toString();
                body+= "\n";
                body+= "Please review the events and ....\nThank you.\n-Security Admins";
                body+= "\n\n" + url;

                log.debug("************************** Email Subject **************************");
                log.debug(subject);
                log.debug("************************** Email Body *****************************");
                log.debug(body);
                log.debug("************************** Email To *******************************");
                log.debug(emailList.toString());
                log.debug("************************** Email End**** **************************");

                if (getAttribute("Send Email").equalsIgnoreCase("TRUE")){
	                tcEmailNotificationUtil sendMail = new tcEmailNotificationUtil(getDataBase());
	                sendMail.setSubject(subject);
		         	sendMail.setBody(body);
		        	sendMail.setFromAddress(from);

	                for (int i=0;i<emailList.size();i++){
	                	String to = emailList.get(i).toString();
	                	log.debug("Sending Email to user[" + to + "]");
	                	sendMail.sendEmail(to);
	                }
			    }else{
			    	log.debug("Ignore Sending Email");
			    }

            }
            else {
                log.info("No pending reconciliation events");
            }
        }
        else {
            log.info("No such group available: "+this.adminGroupName);
        }

        userIntf.close();
        groupIntf.close();
    }

}