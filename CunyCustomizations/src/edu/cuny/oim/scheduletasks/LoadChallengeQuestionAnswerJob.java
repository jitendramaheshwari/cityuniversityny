package edu.cuny.oim.scheduletasks;


import com.thortech.util.logging.Logger;
import com.thortech.xl.crypto.tcCryptoUtil;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.HashMap;

import oracle.iam.identity.usermgmt.api.UserManager;
import oracle.iam.platform.Platform;
import oracle.iam.scheduler.vo.TaskSupport;


public class LoadChallengeQuestionAnswerJob extends TaskSupport {

    public final String CLASS_NAME = "LoadChallengeQuestionAnswerJob";
    private static final String secretKey = "DBSecretKey";

    /**
     * Logger
     */
    private static Logger log = Logger.getLogger("CUNY.CUSTOM");
    private final static String SEPARATOR = "~";

    public void execute(HashMap map) {
        log.debug("Starting Scheduled Task ----- " + CLASS_NAME + " ----->");
        log.debug("Executing main task");
        try {
            loadChallenegeQuestionANswerFile("/opt/oracle/javaproj/password.csv");
        } catch (Exception e) {
            log.error("Exception ", e);
        }
    }

    public String getUserKeyForLogin(String userLogin, Connection conn) {
        String sql =
            "select usr_key from usr where UPPER(usr.usr_login) = ?";
        PreparedStatement stmt = null;
        ResultSet rs = null;
        String usr_key = null;
        try {
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, userLogin.toUpperCase());
            rs = stmt.executeQuery();
            if (rs.next())
                usr_key = rs.getString("usr_key");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null)
                    rs.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (stmt != null)
                    stmt.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return usr_key;
    }

    public String getEncryptedinDBPassword(String userKey, Connection conn) {
        String sql =
            "select usr_password from usr where usr_key = ?";
        PreparedStatement stmt = null;
        ResultSet rs = null;
        String usr_key = null;
        try {
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, userKey);
            rs = stmt.executeQuery();
            if (rs.next())
                usr_key = rs.getString("usr_password");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null)
                    rs.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (stmt != null)
                    stmt.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return usr_key;
    }


    public HashMap getAttributes() {
        return null;
    }

    /**
     * from parent class
     */
    public void setAttributes() {

    }
    public String encrypt(String val) throws Exception {

        String encrypted = null;
            String encstr = val;
            encrypted = tcCryptoUtil.encrypt(encstr, secretKey, "UTF-8");
            return encrypted;
    }

    /**
     * Decrypt a string
     * @param val
     * @return
     * @throws Exception 
     */
    public String decrypt(String val) throws Exception {
        String encrypted = null;
            String encstr = val;
            encrypted = tcCryptoUtil.decrypt(encstr, secretKey, "UTF-8");
            return encrypted;
    }
    public boolean checkIfQuestionLoaded(String userLogin, Connection conn) {
        String sql =
            "select count(*) as counter from pcq, usr where usr.usr_key = pcq.usr_key and UPPER(usr.usr_login) = ?";
        PreparedStatement stmt = null;
        ResultSet rs = null;
        int counter = 0;
        try {
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, userLogin.toUpperCase());
            rs = stmt.executeQuery();
            if (rs.next())
                counter = rs.getInt("counter");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null)
                    rs.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (stmt != null)
                    stmt.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return (counter == 5);
    }
    public void loadChallenegeQuestionANswerFile(String fileName) throws Exception {
        
        Connection conn = Platform.getOperationalDS().getConnection();
        
        File outputFile = new File("/opt/oracle/javaproj/Report_Challenge.txt");
        if (outputFile.exists())
            outputFile.delete();
        PrintWriter outputWriter = null;


        BufferedReader saveFile = null;
        try {
            outputFile.createNewFile();
            outputWriter = new PrintWriter(new FileWriter(outputFile), true);

            saveFile = new BufferedReader(new FileReader(fileName));
            String line = null;
            saveFile.readLine();
            while ((line = saveFile.readLine()) != null) {
                /*
                LoadChallengeQuestionsInternal thread1 = new LoadChallengeQuestionsInternal(line, outputWriter, conn);
                Thread thra1 = new Thread(thread1);
                thra1.start();
                while (thra1.activeCount() > 40) {
                    Thread.sleep(500);
                }
                outputWriter.println("Active: " + thra1.activeCount());
*/
                loadQuestionAnswers(line, outputWriter, conn);
                
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            saveFile.close();
            outputWriter.close();
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void updateUserPassword(String userLogin,
                                         String userPassword) {
            UserManager usrmgr =
                Platform.getService(UserManager.class);
        try {
            usrmgr.changePassword(userLogin, userPassword.toCharArray(),
                                  true, java.util.Locale.US, false, false);
        } catch (Exception e) {
            log.error("Exception ", e);
        }
    }
    public void loadQuestionAnswers(String line,
                                          PrintWriter outputWriter,
                                          Connection conn) throws UnsupportedEncodingException {
        String userLogin = null;
        String userPassword = null;
        String question1 = null;
        String answer1 = null;
        String question2 = null;
        String answer2 = null;
        String question3 = null;
        String answer3 = null;
        String question4 = null;
        String answer4 = null;
        String question5 = null;
        String answer5 = null;

        long timeStart = System.currentTimeMillis();
        if (line != null && !line.isEmpty()) {
            String[] parts =
                line.split(SEPARATOR); //changing separator to #
            outputWriter.println(line);
            outputWriter.println(parts.length);
            if (parts.length == 2) {
                userLogin = parts[0];
                userPassword =
                        java.net.URLDecoder.decode(parts[1], "UTF-8");
            } else if (parts.length == 12) {
                userLogin = parts[0];

                userPassword =
                        java.net.URLDecoder.decode(parts[1], "UTF-8");

                question1 = java.net.URLDecoder.decode(parts[2], "UTF-8");
                answer1 = java.net.URLDecoder.decode(parts[3], "UTF-8");


                question2 = java.net.URLDecoder.decode(parts[4], "UTF-8");

                answer2 = java.net.URLDecoder.decode(parts[5], "UTF-8");

                question3 = java.net.URLDecoder.decode(parts[6], "UTF-8");
                answer3 = java.net.URLDecoder.decode(parts[7], "UTF-8");

                question4 = java.net.URLDecoder.decode(parts[8], "UTF-8");
                answer4 = java.net.URLDecoder.decode(parts[9], "UTF-8");

                question5 = java.net.URLDecoder.decode(parts[10], "UTF-8");
                answer5 = java.net.URLDecoder.decode(parts[11], "UTF-8");
            }
            String userKey = getUserKeyForLogin(userLogin, conn);
            updateUserPassword(userLogin, userPassword);

            if (checkIfQuestionLoaded(userLogin, conn))
                outputWriter.println("Already Loaded  " + userLogin);
            else {
                    updateQuestionAnswer(userKey, question1, answer1, conn);
                    updateQuestionAnswer(userKey, question2, answer2, conn);
                    updateQuestionAnswer(userKey, question3, answer3, conn);
                    updateQuestionAnswer(userKey, question4, answer4, conn);
                    updateQuestionAnswer(userKey, question5, answer5, conn);
                outputWriter.println("Done  " + userLogin);
            }
        }
        long EndStart = System.currentTimeMillis();
        outputWriter.println(userLogin + " Time Taken" +
                             (EndStart - timeStart));

    }

    private void updateQuestionAnswer(String userKey, String question, String answer, Connection conn){
        String sql = "insert into PCQ(pcq_key, usr_key, pcq_question, pcq_answer, pcq_createby, pcq_updateby) values (pcq_seq.nextval, ?, ?, ?, ?, ?)";
        PreparedStatement stmt = null;
        try {
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, userKey);
            stmt.setString(2, encrypt(question));
            stmt.setString(3, encrypt(answer));
            stmt.setString(4, userKey);
            stmt.setString(5, userKey);

            
            stmt.execute();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }


    class LoadChallengeQuestionsInternal implements Runnable {
        public String line = null;
        public PrintWriter outputWriter = null;
        public Connection conn = null;

        public LoadChallengeQuestionsInternal(String line,
                                              PrintWriter outputWriter,
                                              Connection conn) {
            this.line = line;
            this.outputWriter = outputWriter;
            this.conn = conn;
        }

        public void run() {
            try {
                loadQuestionAnswers(line,
                                              outputWriter,
                                              conn);
            } catch (Exception e) {
                outputWriter.println(" Encoding issue. " + e.getMessage());
            }
        }




    }

}
