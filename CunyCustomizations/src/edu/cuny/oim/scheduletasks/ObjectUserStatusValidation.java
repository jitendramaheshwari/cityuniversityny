package edu.cuny.oim.scheduletasks;

import java.util.Hashtable;
import java.util.Vector;
import java.io.*;
import java.util.*;

import com.thortech.util.logging.Logger;

import Thor.API.Operations.tcITResourceDefinitionOperationsIntf;
import Thor.API.Operations.tcReconciliationOperationsIntf;
import Thor.API.Operations.tcFormInstanceOperationsIntf;
import Thor.API.Operations.tcUserOperationsIntf;
import Thor.API.Operations.tcGroupOperationsIntf;
import Thor.API.Operations.tcITResourceInstanceOperationsIntf;
import Thor.API.Operations.tcLookupOperationsIntf; 
import Thor.API.Operations.tcObjectOperationsIntf;
import Thor.API.Operations.tcOrganizationOperationsIntf; 
import Thor.API.Operations.tcProvisioningOperationsIntf; 

import com.thortech.xl.dataaccess.tcDataSet;
import com.thortech.xl.dataaccess.tcDataSetException;
import com.thortech.xl.dataobj.util.tcEmailNotificationUtil;
import com.thortech.xl.scheduler.tasks.SchedulerBaseTask;
import Thor.API.Base.tcUtilityOperationsIntf;
import Thor.API.Exceptions.tcAPIException;
import Thor.API.Exceptions.tcAwaitingApprovalDataCompletionException;
import Thor.API.Exceptions.tcAwaitingObjectDataCompletionException;
import Thor.API.Exceptions.tcObjectNotFoundException;
import Thor.API.Exceptions.tcTaskNotFoundException;
import Thor.API.Exceptions.tcUserNotFoundException;
import Thor.API.tcResultSet;
import Thor.API.tcUtilityFactory;

public class ObjectUserStatusValidation extends SchedulerBaseTask{
	public final String CLASS_NAME = "ObjectUserStatusValidation()";
	private Vector instanceList;
	private String debug;
	
	/**
	 * Logger
	 */
	
	public static Logger log = Logger.getLogger("CUNY.CUSTOM");
	
	public boolean stop(){
	    log.debug("tcTskTimedRetry:stop:Task being stopped");
	    return true;
  	}
	
	//triggered task when performing a reconciliation event
	public void execute(){

			System.out.println("Starting Scheduled Task ----- " + CLASS_NAME  + " ----->");
			System.out.println("Executing main task");				
			debug = getAttribute("Debug");
			instanceList = new Vector();
			getInvalidList();
			if (isStopped()){
				return;
			}
			log.debug("Instances to Disable[" + instanceList.size() + "]");
			for (int i=0;i<instanceList.size();i++){
				if (isStopped()){
					return;
				}
				String[] temp = instanceList.get(i).toString().split("=");
				disableInstance(Long.parseLong(temp[0]), Long.parseLong(temp[1]));
			}
			
			System.out.println("Ending main task");			
	}
	
	//required to connect to the database without a login
	public tcUtilityOperationsIntf getUtilityOps(String utilityName){
		try {
			return (tcUtilityOperationsIntf)super.getUtility(utilityName);
		}
		catch (tcAPIException e){
			e.printStackTrace();
		}
		return null;
	}

	public void disableInstance(long objectInstanceKey, long userKey){
		log.info("Running " + CLASS_NAME  + ".disableInstance() ----->");
		userIntf = (tcUserOperationsIntf)getUtilityOps("Thor.API.Operations.tcUserOperationsIntf"); 
		try {
			if (!debug.equalsIgnoreCase("true")){
				userIntf.disableAppForUser(userKey, objectInstanceKey);
			}
			log.debug("Successfully disabled instance [" + objectInstanceKey + "] for user[" + userKey + "]");
		} catch (tcAPIException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (tcObjectNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (tcUserNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	public void getInvalidList(){
		log.info("Running " + CLASS_NAME  + ".getInvalidList() ----->");
		tcDataSet dsList = new tcDataSet();
		String query = "select oiu.usr_key, oiu.oiu_key " +
			"from obj, oiu, ost, obi, usr " +
			"where oiu.obi_key = obi.obi_key " +
			"and obi.obj_key = obj.obj_key " +
			"and oiu.ost_key = ost.ost_key " +
			"and oiu.usr_key = usr.usr_key " + 
			"and obj.obj_name in ('Peoplesoft EPM', 'Peoplesoft Financials', 'Peoplesoft CRM') " +
			"and usr.usr_udf_glbl_emp_status in ('Inactive', 'Retired') " +
			"and ost.ost_status in ('Provisioned', 'Enabled')";
		
		log.debug(query);
		
		dsList.setQuery(getDataBase(), query);		
		try {
			dsList.executeQuery();
			log.debug("Rows Found[" + dsList.getTotalRowCount() + "]");
			
			if (!dsList.isEmpty()){
				for (int i=0;i<dsList.getTotalRowCount();i++){
					if (isStopped()){
						return;
					}
					dsList.goToRow(i);
					log.debug("User Key[" + dsList.getString("USR_KEY") +
							"]Object Instace Key[" + dsList.getString("OIU_KEY"));
					instanceList.add(dsList.getString("OIU_KEY") + "=" + dsList.getString("USR_KEY"));
				}
			}	
		} catch (tcDataSetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
	}
	
	//examples of how to create an instance for a specific operation
	protected tcReconciliationOperationsIntf reconIntf;
	//reconIntf = (tcReconciliationOperationsIntf)getUtilityOps("Thor.API.Operations.tcReconciliationOperationsIntf");
	protected tcUserOperationsIntf userIntf;
	//userIntf = (tcUserOperationsIntf)getUtilityOps("Thor.API.Operations.tcUserOperationsIntf");
	protected tcFormInstanceOperationsIntf formIntf; 
	//userIntf = (tcUserOperationsIntf)getUtilityOps("Thor.API.Operations.tcFormInstanceOperationsIntf");
	protected tcGroupOperationsIntf groupIntf; 
	//groupIntf = (tcGroupOperationsIntf)getUtilityOps("Thor.API.Operations.tcGroupOperationsIntf");
	protected tcITResourceInstanceOperationsIntf itresourceIntf;
	//itresourceIntf = (tcITResourceInstanceOperationsIntf)getUtilityOps("Thor.API.Operations.tcITResourceInstanceOperationsIntf");
	protected tcLookupOperationsIntf lookupIntf; 
	//lookupIntf = (tcLookupOperationsIntf)getUtilityOps("Thor.API.Operations.tcLookupOperationsIntf");
	protected tcObjectOperationsIntf objectIntf;
	//objectIntf = (tcObjectOperationsIntf)getUtilityOps("Thor.API.Operations.tcObjectOperationsIntf");
	protected tcOrganizationOperationsIntf orgIntf; 
	//orgIntf = (tcOrganizationOperationsIntf)getUtilityOps("Thor.API.Operations.tcOrganizationOperationsIntf");
	protected tcProvisioningOperationsIntf provIntf; 
	//provIntf = (tcProvisioningOperationsIntf)getUtilityOps("Thor.API.Operations.tcProvisioningOperationsIntf");
	//reqIntf = (tcRequestOperationsIntf)getUtilityOps("Thor.API.Operations.tcRequestOperationsIntf");
}