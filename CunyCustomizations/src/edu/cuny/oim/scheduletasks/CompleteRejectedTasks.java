package edu.cuny.oim.scheduletasks;

import Thor.API.Exceptions.tcAPIException;
import Thor.API.Exceptions.tcAwaitingApprovalDataCompletionException;
import Thor.API.Exceptions.tcAwaitingObjectDataCompletionException;
import Thor.API.Exceptions.tcTaskNotFoundException;
import Thor.API.Operations.tcProvisioningOperationsIntf;

import com.thortech.util.logging.Logger;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;

import oracle.iam.platform.Platform;
import oracle.iam.scheduler.vo.TaskSupport;


public class CompleteRejectedTasks extends TaskSupport {
    public final String CLASS_NAME = "CompleteRejectedTasks()";
    private Vector taskKeys = null;

    /**
     * Logger
     */

    private static Logger log = Logger.getLogger("CUNY.CUSTOM");


    //triggered task when performing a reconciliation event

    public void execute(HashMap hashMap) {
        try {
            log.debug("Starting Scheduled Task ----- " + CLASS_NAME +
                      " ----->");
            log.debug("Executing main task");

            String taskName = (String)hashMap.get("Task Name");
            String resourceObject = (String)hashMap.get("Resource Object");
            String responseCode = (String)hashMap.get("Response Code");
            taskKeys = new Vector();
            findTasks(taskName, resourceObject, responseCode);
            if (taskKeys.size() != 0) {
                updateTasks();
            }


            log.debug("Ending main task");

        } catch (Exception e) {
            log.error("An exception has occurred : ", e);
        }

    }

    @Override
    public HashMap getAttributes() {
        return null;
    }

    @Override
    public void setAttributes() {
    }

    /**
     * Searches for all tasks that are in a rejected state
     * Reassigns all tasks to appropriate admin group for organization
     * @throws Exception
     */
    public void findTasks(String taskName, String resourceObject,
                          String responseCode) throws Exception {
        log.info("Running " + CLASS_NAME + ".findTasks() ----->");
        log.debug("taskName:" + taskName);
        log.debug("resourceObject:" + resourceObject);
        log.debug("responseCode:" + responseCode);

        Connection oimDBConnection = null;
        ResultSet queryResult = null;
        Statement queryStmt = null;

        String query = "";

        query =
                "select oti.sch_key " + "from oti, mil, obj, pkg, orc, usr, sch " +
                "where oti.mil_key=mil.mil_key " +
                "and oti.pkg_key=pkg.pkg_key " +
                "and pkg.obj_key=obj.obj_key " +
                "and oti.orc_key=orc.orc_key " +
                "and orc.usr_key=usr.usr_key " +
                "and oti.sch_key=sch.sch_key " + "and oti.sch_status='R' " +
                "and mil.mil_name='" + taskName + "' " +
                "and obj.obj_name like '" + resourceObject + "' ";

        if (responseCode.indexOf("(") != -1) {
            query += "and sch.sch_data in " + responseCode + " ";
        } else {
            query += "and sch.sch_data like '" + responseCode + "' ";
        }

        /*query = "SELECT oti.sch_key " +
		"FROM sch, osi, mil, pkg, req, usr, ugp, obj, oiu, orc, usr usr1, act, usr usr2, oti " +
		"WHERE sch.sch_status in ('R') " +
		"AND oti.sch_key = sch.sch_key " +
		"AND sch.sch_key = osi.sch_key " +
		"AND osi.mil_key = mil.mil_key "  +
		"AND pkg.pkg_key = osi.pkg_key " +
		"AND req.req_key(+) = osi.req_key " +
		"AND osi.osi_assigned_to_usr_key = usr.usr_key(+) " +
		"AND osi.osi_assigned_to_ugp_key = ugp.ugp_key(+) " +
		"AND oiu.usr_key=usr1.usr_key " +
		"AND act.act_key = usr1.act_key " +
		"AND obj.obj_key = pkg.obj_key " +
		"AND pkg.pkg_type = 'Provisioning' " +
		"AND oiu.orc_key = osi.orc_key " +
		"AND orc.orc_key = osi.orc_key " +
		"AND osi.osi_assigned_to_usr_key = usr2.usr_key " +
		"AND mil.mil_name='" + taskName + "' " +
		"AND obj.obj_name like '" + resourceObject + "' " +
		"AND sch.sch_data='" + responseCode + "'";
		*/
        try {
            oimDBConnection = Platform.getOperationalDS().getConnection();
            queryStmt = oimDBConnection.createStatement();
            queryResult = queryStmt.executeQuery(query);

            while (queryResult.next()) {
                log.debug("Task Key: " + queryResult.getString("SCH_KEY"));
                taskKeys.add(String.valueOf(queryResult.getString("SCH_KEY")));
            }

        } catch (SQLException ex) {
            log.error("Exception ", ex);
        } finally {
            try {
                if (queryResult != null) {
                    queryResult.close();
                }
            } catch (Exception e) {
                log.error("Exception ", e);
            }
            try {
                if (queryStmt != null) {
                    queryStmt.close();
                }
            } catch (Exception e) {
                log.error("Exception ", e);
            }
            try {
                if (oimDBConnection != null) {
                    oimDBConnection.close();
                }
            } catch (Exception e) {
                log.error("Exception ", e);
            }
        }
    }

    public void updateTasks() {
        log.info("Running " + CLASS_NAME + ".updateTasks() ----->");
        tcProvisioningOperationsIntf provIntf =
            Platform.getService(Thor.API.Operations.tcProvisioningOperationsIntf.class);
        log.debug("Total Tasks to complete:" + taskKeys.size());
        for (int i = 0; i < taskKeys.size(); i++) {
            Hashtable taskHash = new Hashtable();
            taskHash.put("Process Instance.Task Details.Status", "UC");
            taskHash.put("Process Instance.Task Details.Note",
                         "Task Auto Completed via Scheduled Task");
            long key = Long.parseLong(taskKeys.get(i).toString());
            try {
                provIntf.updateTask(key, taskHash);
                log.debug("Task key " + key + " marked as completed");
            } catch (tcAPIException e) {
                log.error("Exception ", e);
            } catch (tcTaskNotFoundException e) {
                log.error("Exception ", e);
            } catch (tcAwaitingObjectDataCompletionException e) {
                log.error("Exception ", e);
            } catch (tcAwaitingApprovalDataCompletionException e) {
                log.error("Exception ", e);
            }

        }
    }
}
