/**
 *
 */
package edu.cuny.oim.scheduletasks;

import java.util.Vector;

import com.thortech.util.logging.Logger;
import com.thortech.xl.crypto.tcCryptoUtil;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import oracle.iam.identity.usermgmt.api.UserManager;
import oracle.iam.identity.usermgmt.vo.User;
import oracle.iam.platform.Platform;
import oracle.iam.scheduler.vo.TaskSupport;

/**
 * @author kpinsky
 *
 */
public class PhaseUpdate extends TaskSupport {

    public final String CLASS_NAME = "PhaseUpdate()";
    private static final String secretKey = "DBSecretKey";

    /**
     * Logger
     */
    private static Logger log = Logger.getLogger("CUNY.CUSTOM");

    public void execute(HashMap map) {
        log.debug("Starting Scheduled Task ----- " + CLASS_NAME + " ----->");
        log.debug("Executing main task");
        try {
            String encSt = encrypt("JITENDRA");

            log.debug("Doing the encrypt of Jitendra " + encSt);
            log.debug("Doing the decrypt of  " + decrypt(encSt));

        } catch (Exception e) {
            log.error("Exception ", e);

        }
        String sqlUpdateQuery = "update usr set usr_udf_phase1_status = 'TRUE' where USR_STATUS='Active' " +
            "and usr_udf_phase1_date  < SYSDATE and (UPPER(usr_udf_phase1_status) != 'TRUE' " +
            "or usr_udf_phase1_status is null)";
        executeUpdate(sqlUpdateQuery);
        log.debug("Ending main task");

    }

    public void executeUpdate(String sqlUpdateQuery) {
        String METHOD_NAME = " executeUpdate ";
        log.info("Running " + CLASS_NAME + METHOD_NAME +".query() ----->" + sqlUpdateQuery);
        Connection oimDBConnection = null;
        Statement queryStmt = null;

        log.debug(sqlUpdateQuery);

        try {
            oimDBConnection = Platform.getOperationalDS().getConnection();
            queryStmt = oimDBConnection.createStatement();
            queryStmt.executeUpdate(sqlUpdateQuery);
        } catch (Exception e) {
            log.error("Exception ", e);
        } finally {
            try {
                if (queryStmt != null) {
                    queryStmt.close();
                }
            } catch (Exception e) {
                log.error("Exception ", e);
            }
            try {
                if (oimDBConnection != null) {
                    oimDBConnection.close();
                }
            } catch (Exception e) {
                log.error("Exception ", e);
            }
        }
    }



    public HashMap getAttributes() {
        return null;
    }

    /**
     * from parent class
     */
    public void setAttributes() {

    }
    public String encrypt(String val) throws Exception {

        String encrypted = null;
            String encstr = val;
            encrypted = tcCryptoUtil.encrypt(encstr, secretKey, "UTF-8");
            return encrypted;
    }

    /**
     * Decrypt a string
     * @param val
     * @return
     * @throws Exception 
     */
    public String decrypt(String val) throws Exception {
        String encrypted = null;
            String encstr = val;
            encrypted = tcCryptoUtil.decrypt(encstr, secretKey, "UTF-8");
            return encrypted;
    }

}
