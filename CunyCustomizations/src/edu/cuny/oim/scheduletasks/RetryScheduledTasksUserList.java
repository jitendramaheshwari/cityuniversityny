package edu.cuny.oim.scheduletasks;

import java.util.Hashtable;
import java.util.Vector;
import java.io.*;
import java.util.*;

import com.thortech.util.logging.Logger;

import Thor.API.Operations.tcITResourceDefinitionOperationsIntf;
import Thor.API.Operations.tcReconciliationOperationsIntf;
import Thor.API.Operations.tcFormInstanceOperationsIntf;
import Thor.API.Operations.tcUserOperationsIntf;
import Thor.API.Operations.tcGroupOperationsIntf;
import Thor.API.Operations.tcITResourceInstanceOperationsIntf;
import Thor.API.Operations.tcLookupOperationsIntf; 
import Thor.API.Operations.tcObjectOperationsIntf;
import Thor.API.Operations.tcOrganizationOperationsIntf; 
import Thor.API.Operations.tcProvisioningOperationsIntf; 

import com.thortech.xl.dataaccess.tcDataSet;
import com.thortech.xl.dataaccess.tcDataSetException;
import com.thortech.xl.dataobj.util.tcEmailNotificationUtil;
import com.thortech.xl.scheduler.tasks.SchedulerBaseTask;
import Thor.API.Base.tcUtilityOperationsIntf;
import Thor.API.Exceptions.tcAPIException;
import Thor.API.Exceptions.tcAwaitingApprovalDataCompletionException;
import Thor.API.Exceptions.tcAwaitingObjectDataCompletionException;
import Thor.API.Exceptions.tcColumnNotFoundException;
import Thor.API.Exceptions.tcInvalidLookupException;
import Thor.API.Exceptions.tcTaskNotFoundException;
import Thor.API.tcResultSet;
import Thor.API.tcUtilityFactory;

public class RetryScheduledTasksUserList extends SchedulerBaseTask{
	public final String CLASS_NAME = "RetryScheduledTasks()";
	private String resObj = "";
	private Vector taskKeys = null;
	private Vector userList = null;
	private Hashtable lookupHash = null;
	/**
	 * Logger
	 */
	
	public static Logger log = Logger.getLogger("CUNY.CUSTOM");
	
	public boolean stop(){
	    log.debug("tcTskTimedRetry:stop:Task being stopped");
	    return true;
  	}
	
	//triggered task when performing a reconciliation event
	public void execute(){
		try{
			System.out.println("Starting Scheduled Task ----- " + CLASS_NAME  + " ----->");
			System.out.println("Executing main task");		
			taskKeys = new Vector();
			userList = new Vector();
			lookupHash = new Hashtable();
			String code = "";
			String decode = "";
			
			lookupIntf = (tcLookupOperationsIntf)getUtilityOps("Thor.API.Operations.tcLookupOperationsIntf");
			
			String taskName = getAttribute("Task Name");
			String resourceObject = getAttribute("Resource Object");
			String responseCode = getAttribute("Response Code");
			String lookupCode = getAttribute("Lookup");
			resObj = resourceObject;
			log.info("Running " + CLASS_NAME  + ".execute() ----->" +
					"taskName=[" + taskName + "]" +
					"resourceObject=[" + resourceObject + "]" +
					"responseCode=[" + responseCode + "]" +
					"lookupCode=[" + lookupCode + "]");

			//Retrieves list of users who do not have NULL or 0 in decode column
			getUsers(lookupCode);			
			if (isStopped()){
				return;
			}
			log.debug("Total List of Users:" + userList.size());
			
			for (int i=0;i<userList.size();i++){
				if (isStopped()){
					return;
				}
				//Set the EMPLID value
				code = userList.get(i).toString();
				taskKeys.clear();
				//Retrieves list of rejected tasks for the user
				findTasks(taskName, resourceObject, responseCode, code);
				if (isStopped()){
					return;
				}
				lookupHash.clear();
				//Only if the list is not 0
				if (taskKeys.size()!=0){
					if (isStopped()){
						return;
					}
					//Retries list of tasks
					retryRejectedTasks();
					taskKeys.clear();
					//Find all tasks again to update the lookup
					findTasks(taskName, resourceObject, responseCode, userList.get(i).toString());
					if (isStopped()){
						return;
					}
					decode = String.valueOf(taskKeys.size());					
					//Updates the lookup with new values
					lookupHash.clear();
					lookupHash.put("Lookup Definition.Lookup Code Information.Decode", decode);
					lookupIntf.updateLookupValue(lookupCode, code, lookupHash);
					log.debug("Updated lookup:code[" + code + "]decode[" + decode + "]");
				}else{
					if (isStopped()){
						return;
					}
					//If no tasks were found for the user, it updates the decode to 0
					decode = "0";					
					lookupHash.put("Lookup Definition.Lookup Code Information.Decode", decode);
					lookupIntf.updateLookupValue(lookupCode, code, lookupHash);
					log.debug("Updated lookup:code[" + code + "]decode[" + decode + "]");
				}
			}
			
			System.out.println("Ending main task");			

		}catch(Exception e){

		}

	}
	
	//required to connect to the database without a login
	public tcUtilityOperationsIntf getUtilityOps(String utilityName){
		try {
			return (tcUtilityOperationsIntf)super.getUtility(utilityName);
		}
		catch (tcAPIException e){
			e.printStackTrace();
		}
		return null;
	}

	public void getUsers(String lookupCode) throws tcAPIException, tcInvalidLookupException, tcColumnNotFoundException{
		log.info("Running " + CLASS_NAME  + ".getUsers() ----->" +
				"lookupCode=[" + lookupCode + "]");		
		lookupIntf = (tcLookupOperationsIntf)getUtilityOps("Thor.API.Operations.tcLookupOperationsIntf");
		tcResultSet lookupSet = lookupIntf.getLookupValues(lookupCode);
		if (lookupSet.isEmpty()) return;
		for (int i=0;i<lookupSet.getTotalRowCount();i++){
			if (isStopped()){
				return;
			}
			lookupSet.goToRow(i);
			String retryCount = lookupSet.getStringValue("Lookup Definition.Lookup Code Information.Decode");
			if (retryCount.equalsIgnoreCase("NULL") || !retryCount.equals("0")){
				String userID = lookupSet.getStringValue("Lookup Definition.Lookup Code Information.Code Key");
				userList.add(userID);
			}
		}		
	}
	/**
	 * Searches for all tasks that are in a rejected state
	 * Reassigns all tasks to appropriate admin group for organization
	 * @throws Exception
	 */
	public void findTasks(String taskName, String resourceObject, String responseCode, String emplId) throws Exception{
		log.info("Running " + CLASS_NAME  + ".findTasks() ----->" +
				"taskName=[" + taskName + "]" +
				"resourceObject=[" + resourceObject + "]" +
				"responseCode=[" + responseCode + "]" +
				"emplId=[" + emplId + "]");

		provIntf = (tcProvisioningOperationsIntf)getUtilityOps("Thor.API.Operations.tcProvisioningOperationsIntf");
		groupIntf = (tcGroupOperationsIntf)getUtilityOps("Thor.API.Operations.tcGroupOperationsIntf");
		userIntf = (tcUserOperationsIntf)getUtilityOps("Thor.API.Operations.tcUserOperationsIntf");

		tcDataSet dsList = new tcDataSet();		
		
		String query = "SELECT oti.sch_key " +
		"FROM sch, osi, mil, pkg, req, usr, ugp, obj, oiu, orc, usr usr1, act, usr usr2, oti " +
		"WHERE sch.sch_status in ('R') " +
		"AND oti.sch_key = sch.sch_key " +
		"AND sch.sch_key = osi.sch_key " +
		"AND osi.mil_key = mil.mil_key "  +
		"AND pkg.pkg_key = osi.pkg_key " +
		"AND req.req_key(+) = osi.req_key " +
		"AND osi.osi_assigned_to_usr_key = usr.usr_key(+) " +
		"AND osi.osi_assigned_to_ugp_key = ugp.ugp_key(+) " +
		"AND oiu.usr_key=usr1.usr_key " +
		"AND act.act_key = usr1.act_key " +
		"AND obj.obj_key = pkg.obj_key " +
		"AND pkg.pkg_type = 'Provisioning' " +
		"AND oiu.orc_key = osi.orc_key " +
		"AND orc.orc_key = osi.orc_key " +
		"AND osi.osi_assigned_to_usr_key = usr2.usr_key " +
		"AND mil.mil_name='" + taskName + "' " +
		"AND obj.obj_name like '" + resourceObject + "' " +
		"AND (sch.sch_data in " + responseCode + " or sch.sch_data is null) " +
		"AND usr1.usr_login = '" + emplId + "'"; 
	
		log.debug(query);   	
		dsList.setQuery(getDataBase(), query);	
		dsList.executeQuery();	
			
		if (!dsList.isEmpty()){		
			if (isStopped()){
				return;
			}
			log.debug("Total Rows: " + dsList.getTotalRowCount());				
			
			for (int k = 0; k < dsList.getTotalRowCount(); k++) {
	    		dsList.goToRow(k);  
	    		log.debug("Task Key: " + dsList.getLong("SCH_KEY"));
	    		taskKeys.add(String.valueOf(dsList.getLong("SCH_KEY")));
			}
		}	    
	    
	}
	
	public void retryRejectedTasks(){
		log.info("Running " + CLASS_NAME  + ".retryRejectedTasks() ----->");
		provIntf = (tcProvisioningOperationsIntf)getUtilityOps("Thor.API.Operations.tcProvisioningOperationsIntf");
		log.debug("Total Tasks to retry:" + taskKeys.size());
		
		for (int i=0;i<taskKeys.size();i++){	
			if (isStopped()){
				return;
			}			

			long key = Long.parseLong(taskKeys.get(i).toString());			
			try {
				provIntf.retryTask(key);
				log.debug("Task key " + key + " retrying(" + i + " " + resObj + ")");
			} catch (tcAPIException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (tcTaskNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}
	
	//examples of how to create an instance for a specific operation
	protected tcReconciliationOperationsIntf reconIntf;
	//reconIntf = (tcReconciliationOperationsIntf)getUtilityOps("Thor.API.Operations.tcReconciliationOperationsIntf");
	protected tcUserOperationsIntf userIntf;
	//userIntf = (tcUserOperationsIntf)getUtilityOps("Thor.API.Operations.tcUserOperationsIntf");
	protected tcFormInstanceOperationsIntf formIntf; 
	//userIntf = (tcUserOperationsIntf)getUtilityOps("Thor.API.Operations.tcFormInstanceOperationsIntf");
	protected tcGroupOperationsIntf groupIntf; 
	//groupIntf = (tcGroupOperationsIntf)getUtilityOps("Thor.API.Operations.tcGroupOperationsIntf");
	protected tcITResourceInstanceOperationsIntf itresourceIntf;
	//itresourceIntf = (tcITResourceInstanceOperationsIntf)getUtilityOps("Thor.API.Operations.tcITResourceInstanceOperationsIntf");
	protected tcLookupOperationsIntf lookupIntf; 
	//lookupIntf = (tcLookupOperationsIntf)getUtilityOps("Thor.API.Operations.tcLookupOperationsIntf");
	protected tcObjectOperationsIntf objectIntf;
	//objectIntf = (tcObjectOperationsIntf)getUtilityOps("Thor.API.Operations.tcObjectOperationsIntf");
	protected tcOrganizationOperationsIntf orgIntf; 
	//orgIntf = (tcOrganizationOperationsIntf)getUtilityOps("Thor.API.Operations.tcOrganizationOperationsIntf");
	protected tcProvisioningOperationsIntf provIntf; 
	//provIntf = (tcProvisioningOperationsIntf)getUtilityOps("Thor.API.Operations.tcProvisioningOperationsIntf");
	//reqIntf = (tcRequestOperationsIntf)getUtilityOps("Thor.API.Operations.tcRequestOperationsIntf");
}