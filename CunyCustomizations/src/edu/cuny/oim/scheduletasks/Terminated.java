/**
 *
 */
package edu.cuny.oim.scheduletasks;

import Thor.API.Exceptions.tcAPIException;
import oracle.iam.scheduler.vo.TaskSupport;

import com.thortech.util.logging.Logger;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import oracle.iam.identity.usermgmt.api.UserManager;
import oracle.iam.identity.usermgmt.vo.User;
import oracle.iam.platform.Platform;

/**
 * @author kpinsky // PERFORMANCE issue.
 *
 */
public class Terminated extends TaskSupport {

    private final String CLASS_NAME = "Terminated()";

    /**
     * Logger
     */
    private static Logger log = Logger.getLogger("CUNY.CUSTOM");

    /**
     *
     * It's a single query code, not sure why coded this way -Jitendra
     * 
     * update usr set USR_UDF_TERM = 'TRUE' where
     * USR_STATUS='Active' and (upper(USR_UDF_TERM) !='TRUE' or USR_UDF_TERM is
     * null) and USR_UDF_TERM_DATE < SYSDATE Why to iterate over list of users
     * @param map
     */
    //triggered task when performing a reconciliation event
    public void execute(HashMap map) {

        log.debug("Starting Scheduled Task ----- " + CLASS_NAME + " ----->");
        log.debug("Executing main task");
        log.debug("Finding users with Terminated Disbale Date");
        updateUsers();
        log.debug("Ending main task");

    }

    public HashMap getAttributes() {
        return null;
    }

    /**
     * from parent class
     */
    public void setAttributes() {

    }

    /**
     * Performs a query against the OIM database
     *
     * @return - vector containing list of users who match criteria
     */
    public void updateUsers() {
        log.info("Running " + CLASS_NAME + ".query() ----->");
        Connection oimDBConnection = null;
        String query = "UPDATE USR SET USR_UDF_TERM ='TRUE' "
                + "where USR_STATUS='Active' "
                + "and (upper(USR_UDF_TERM) !='TRUE' "
                + "or USR_UDF_TERM is null) "
                + "and USR_UDF_TERM_DATE < SYSDATE";
        log.debug(query);
        Statement queryStmt = null;
        try {
            oimDBConnection = Platform.getOperationalDS().getConnection();
            queryStmt = oimDBConnection.createStatement();
            queryStmt.executeUpdate(query);
        } catch (Exception e) {
            log.error("Exception ", e);
        } finally {
            try {
                if (queryStmt != null) {
                    queryStmt.close();
                }
            } catch (Exception e) {
                log.error("Exception ", e);
            }
            try {
                if (oimDBConnection != null) {
                    oimDBConnection.close();
                }
            } catch (Exception e) {
                log.error("Exception ", e);
            }
        }
    }

}
