package edu.cuny.oim.scheduletasks;

import com.thortech.util.logging.Logger;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import oracle.iam.notification.api.NotificationService;
import oracle.iam.notification.vo.NotificationEvent;
import oracle.iam.platform.Platform;
import oracle.iam.scheduler.vo.TaskSupport;

public class PasswordWarning extends TaskSupport {

    public final String CLASS_NAME = "PasswordWarning()";
    private static Logger logger = Logger.getLogger("CUNY.CUSTOM");

    final String subject = "CUNYFirst - Password Expiration Warning";
    String fromAddress;
    String selfService;

    public void execute(HashMap hashMap) {
        logger.debug("Starting Scheduled Task ----- " + CLASS_NAME + " ----->");
        logger.debug("Executing main task");
        fromAddress = (String) hashMap.get("From User"); //user-login name
        selfService = (String) hashMap.get("Self Service URL");
        logger.debug("From Address[" + fromAddress + "]Self Service URL[" + selfService + "]");
        if (isStop()) {
            return;
        }
        Connection oimDBConnection = null;
        ResultSet queryResult = null;
        Statement queryStmt = null;
        String query = "select usr_key, usr_login, usr_email, (trunc(usr_pwd_expire_date) - trunc(sysdate)) as \"EXPIRE\""
                + " from usr"
                + " where usr_pwd_warn_date < sysdate"
                + " and usr_status='Active'"
                + " and (USR_PWD_EXPIRED = 0 or USR_PWD_EXPIRED is null)";

        try {
            oimDBConnection = Platform.getOperationalDS().getConnection();
            queryStmt = oimDBConnection.createStatement();
            queryResult = queryStmt.executeQuery(query);

            while (queryResult.next()) {
                String user = queryResult.getString("USR_LOGIN");
                String email = queryResult.getString("USR_EMAIL");
                String days = queryResult.getString("EXPIRE");
                logger.debug("User[" + user + "]Email[" + email + "]days[" + days + "]");
                if (email.trim().length() > 0) {
                    sendEmail(user, email, days);
                } else {
                    logger.error("Email does not exist for user");
                }

            }

        } catch (SQLException ex) {
            logger.error("Exception ", ex);
        } finally {
            try {
                if (queryResult != null) {
                    queryResult.close();
                }
            } catch (Exception e) {
                logger.error("Exception ", e);
            }
            try {
                if (queryStmt != null) {
                    queryStmt.close();
                }
            } catch (Exception e) {
                logger.error("Exception ", e);
            }
            try {
                if (oimDBConnection != null) {
                    oimDBConnection.close();
                }
            } catch (Exception e) {
                logger.error("Exception ", e);
            }
        }

    }

    public HashMap getAttributes() {
        return null;
    }

    /**
     * from parent class
     */
    public void setAttributes() {

    }

    /**
     * Set subject in the template.
     *
     * @param userName
     * @param email
     * @param days
     */
    /*
        String body = "Your password for CUNYFirst will expire in " + days + " days.  Please click the link below to change your password.  You may also copy the link below into your browser.";
        body = body + "\n\n" + selfService;
        Subject= CUNYFirst - Password Expiration Warning
        FromAddress= is a User-login
    
     */
    private void sendEmail(String userName, String email, String days) {
        String METHOD_NAME = " sendNotificationEmail ";
        logger.info(CLASS_NAME + METHOD_NAME + " Entering with userName: "
                + userName + " email: " + email + " days: " + days);

        NotificationService notificationService = Platform
                .getService(oracle.iam.notification.api.NotificationService.class);
        NotificationEvent event = new NotificationEvent();
        event.setTemplateName("CUNYFirstPasswordExpirationWarningTemplate"); // Template

        event.setSender(fromAddress);

        String[] users = new String[1];
        users[0] = userName;
        event.setUserIds(users);

        HashMap<String, Object> notificationData = new HashMap<String, Object>();
        notificationData.put("days", days);
        notificationData.put("selfService", selfService);

        event.setParams(notificationData);
        try {
            notificationService.notify(event);
            logger.info(CLASS_NAME + METHOD_NAME + " Notification sent event: "
                    + event);
        } catch (Exception e) {
            logger.error(CLASS_NAME + METHOD_NAME + "Exception", e);
        }

    }
}
