/**
 *
 */
package edu.cuny.oim.scheduletasks;

import Thor.API.Base.tcUtilityOperationsIntf;
import Thor.API.Exceptions.tcAPIException;
import Thor.API.Exceptions.tcColumnNotFoundException;
import Thor.API.Exceptions.tcStaleDataUpdateException;
import Thor.API.Exceptions.tcUserNotFoundException;
import Thor.API.Operations.tcUserOperationsIntf;
import Thor.API.tcResultSet;

import com.thortech.util.logging.Logger;
import com.thortech.xl.dataaccess.tcDataSet;
import com.thortech.xl.dataaccess.tcDataSetException;
import com.thortech.xl.scheduler.tasks.SchedulerBaseTask;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;

import oracle.iam.identity.usermgmt.api.UserManager;
import oracle.iam.identity.usermgmt.vo.User;
import oracle.iam.platform.Platform;


/**
 * @author kpinsky
 *
 */
// Modified in April 2015 by Ray Ramos

public class Terminator extends SchedulerBaseTask{
	public final String CLASS_NAME = "Terminator()";
	private Vector users = null;

	/**
	 * Logger
	 */

	public static Logger log = Logger.getLogger("CUNY.CUSTOM");


	//triggered task when performing a reconciliation event
	public void execute(){

		boolean result = false;
		System.out.println("Starting Scheduled Task ----- " + CLASS_NAME  + " ----->");
		System.out.println("Executing main task");
		//users = new Vector();
		log.debug("Finding users with Terminated Disbale Date");
		//result = query();
		//if (result){
		//	updateUsers(users);
		//}
		updateUsers();
		System.out.println("Ending main task");
	} // end of execute


	public void updateUsers() {
	    final int GRACE_PERIOD_IN_DAYS = 60; // Termination Grace Period
	    log.info("Running " + CLASS_NAME + ".updateUsers() ----->");
	    Connection oimDBConnection = null;
            ResultSet rs = null;
	    String query = "Select usr_key from usr where USR_STATUS='Active' and (upper(USR_UDF_TERM) !='TRUE' or USR_UDF_TERM is null) and (usr_udf_term_effect_date + " +
	            GRACE_PERIOD_IN_DAYS +
	            ") < SYSDATE";


	    log.debug(query);
	    Statement queryStmt = null;
            String userKey = null;
            UserManager usrManager = null;
	    try {
	        usrManager = Platform.getService(UserManager.class);
	        oimDBConnection = Platform.getOperationalDS().getConnection();
	        queryStmt = oimDBConnection.createStatement();
	        rs = queryStmt.executeQuery(query);
	        HashMap<String, Object> atrrMap = new HashMap<String, Object>();
	        atrrMap.put("TERM", "TRUE");
                
                while(rs.next()){
                    userKey = rs.getString("usr_key");  
                    try {
                        User user = new User(userKey, atrrMap);
                        usrManager.modify(user);
                        log.debug(CLASS_NAME + "updateUsers" + "User Updated: atrrMap[" +atrrMap + "] UserKey[" + userKey +"]");
                    } catch (Exception e1) {
                        log.error("Exception in updating user", e1);
                    }
                }
	    } catch (Exception e) {
	        log.error("Exception ", e);
	    } finally {
	        try {
	            if (rs != null) {
	                queryStmt.close();
	            }
	        } catch (Exception e) {
	            log.error("Exception ", e);
	        }
	        try {
	            if (queryStmt != null) {
	                queryStmt.close();
	            }
	        } catch (Exception e) {
	            log.error("Exception ", e);
	        }
	        try {
	            if (oimDBConnection != null) {
	                oimDBConnection.close();
	            }
	        } catch (Exception e) {
	            log.error("Exception ", e);
	        }
	    }
	}



	/**
	 * Performs a query against the OIM database
	 * @param dateField - user defined field for the date
	 * @return - vector containing list of users who match criteria
         * Changes made to fix bug discovered by Venkat April 13th 2015
         * Original code was querying USR_UDF_TERM_DATE, which never
         * gets a value from Trusted Source Recon! So it was always NULL
         * and terminated users were not getting their entitlements
         * removed. Also added final for grace period as no grace period
         * code was present for that requirement. - Ray Ramos April 17th 2015
	 */
	public boolean query(){
		final int GRACE_PERIOD_IN_DAYS = 60; // Termination Grace Period
		log.info("Running " + CLASS_NAME  + ".query() ----->");
		boolean result = false;
		tcDataSet dsList = new tcDataSet();
		String query = "select USR_KEY from USR " +
			"where USR_STATUS='Active' " +
			"and (upper(USR_UDF_TERM) !='TRUE' " +
			"or USR_UDF_TERM is null) " +
			"and (usr_udf_term_effect_date + " +
			GRACE_PERIOD_IN_DAYS +
			")" +
			"< SYSDATE";
		log.debug(query);

		try {
			dsList.setQuery(getDataBase(), query);
			dsList.executeQuery();

			if (!dsList.isEmpty()){
				log.debug("Total Rows Found:" + dsList.getTotalRowCount());
				result = true;
				for (int i = 0; i < dsList.getTotalRowCount(); i++) {
					dsList.goToRow(i);
					System.out.println(dsList.getLong("USR_KEY"));
					users.add(dsList.getLong("USR_KEY"));
					log.debug("User Key: " + dsList.getLong("USR_KEY"));
				} // end of for
			}else{
				log.debug("No Users Found");
				result = false;
			} // end of else
		} catch (tcDataSetException e) {
			e.printStackTrace();
			result = false;
		}
		return result;
	}

	/**
	 * Updates users
	 * @param users - list of users
	 * @throws tcAPIException
	 */
	public void updateUsers(Vector users){
		log.info("Running " + CLASS_NAME  + ".updateUsers() ----->");
		userIntf = (tcUserOperationsIntf)getUtilityOps("Thor.API.Operations.tcUserOperationsIntf");
		Hashtable userHash = new Hashtable();
		Hashtable updateHash = new Hashtable();
		updateHash.put("USR_UDF_TERM", "TRUE");

		for (int i=0;i<users.size();i++){
			try {
				userHash.clear();
				userHash.put("Users.Key", String.valueOf(users.get(i)));
				tcResultSet userSet = userIntf.findAllUsers(userHash);
				userHash.clear();
				userIntf.updateUser(userSet, updateHash);
				log.debug("User " + userSet.getStringValue("Users.User ID") + " updated");
			} catch (tcAPIException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (tcUserNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (tcStaleDataUpdateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (tcColumnNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

	//required to connect to the database without a login
	public tcUtilityOperationsIntf getUtilityOps(String utilityName){
		try {
			return (tcUtilityOperationsIntf)super.getUtility(utilityName);
		}
		catch (tcAPIException e){
			e.printStackTrace();
		}
		return null;
	}
	protected tcUserOperationsIntf userIntf;

}
