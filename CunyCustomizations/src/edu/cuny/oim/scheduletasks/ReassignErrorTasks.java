package edu.cuny.oim.scheduletasks;

import java.util.Hashtable;
import java.util.Vector;
import java.io.*;
import java.util.*;

import com.thortech.util.logging.Logger;

import Thor.API.Operations.tcITResourceDefinitionOperationsIntf;
import Thor.API.Operations.tcReconciliationOperationsIntf;
import Thor.API.Operations.tcFormInstanceOperationsIntf;
import Thor.API.Operations.tcUserOperationsIntf;
import Thor.API.Operations.tcGroupOperationsIntf;
import Thor.API.Operations.tcITResourceInstanceOperationsIntf;
import Thor.API.Operations.tcLookupOperationsIntf; 
import Thor.API.Operations.tcObjectOperationsIntf;
import Thor.API.Operations.tcOrganizationOperationsIntf; 
import Thor.API.Operations.tcProvisioningOperationsIntf; 

import com.thortech.xl.dataaccess.tcDataSet;
import com.thortech.xl.dataaccess.tcDataSetException;
import com.thortech.xl.dataobj.util.tcEmailNotificationUtil;
import com.thortech.xl.scheduler.tasks.SchedulerBaseTask;
import Thor.API.Base.tcUtilityOperationsIntf;
import Thor.API.Exceptions.tcAPIException;
import Thor.API.tcResultSet;

public class ReassignErrorTasks extends SchedulerBaseTask{
    private String url = "";
    private String from = "";
	private Vector orgs = null;
	public final String CLASS_NAME = "ReassignErrorTasks()";
	/**
	 * Logger
	 */
	
	public static Logger log = Logger.getLogger("CUNY.CUSTOM");
	
	
	//triggered task when performing a reconciliation event
	public void execute(){
		try{
			System.out.println("Starting Scheduled Task ----- " + CLASS_NAME  + " ----->");
			System.out.println("Executing main task");	
			
			provIntf = (tcProvisioningOperationsIntf)getUtilityOps("Thor.API.Operations.tcProvisioningOperationsIntf");
			groupIntf = (tcGroupOperationsIntf)getUtilityOps("Thor.API.Operations.tcGroupOperationsIntf");
			userIntf = (tcUserOperationsIntf)getUtilityOps("Thor.API.Operations.tcUserOperationsIntf");
			
        	url = getAttribute("OIM URL");
        	from = getAttribute("From Address");
        	log.debug("OIM URL[" + url + "]From Address[" + from + "]");
            
			
			orgs = new Vector();
			getOrganizations();
			log.debug("Organization Names[" + orgs.toString() + "]");
			
			for (int i=0;i<orgs.size();i++){
				reassignTasks(orgs.get(i).toString());
			}
			
			System.out.println("Ending main task");			

		}catch(Exception e){

		}

	}
	
	//required to connect to the database without a login
	public tcUtilityOperationsIntf getUtilityOps(String utilityName){
		try {
			return (tcUtilityOperationsIntf)super.getUtility(utilityName);
		}
		catch (tcAPIException e){
			e.printStackTrace();
		}
		return null;
	}

	public void getOrganizations() throws tcDataSetException{
		log.debug("Running " + CLASS_NAME  + ".getOrganizations() ----->");
		tcDataSet dsList = new tcDataSet();	
		String query = "SELECT act.act_name " + 
			"FROM act " + 
			"WHERE act.act_key != 1";
		dsList.setQuery(getDataBase(), query);	
		dsList.executeQuery();	
		if (!dsList.isEmpty()){			
			//log.debug("Total Rows: " + dsList.getTotalRowCount());							
			for (int i = 0; i < dsList.getRowCount(); i++) {
	    		dsList.goToRow(i);  
	    		//log.debug("Organization Name: " + dsList.getString("ACT_NAME"));
				orgs.add(dsList.getString("ACT_NAME"));
			}
		}

	}
	
	/**
	 * Searches for all tasks that are in a rejected state
	 * Reassigns all tasks to appropriate admin group for organization
	 * @throws Exception
	 */
	public void reassignTasks(String orgName) throws Exception{
		log.debug("Running " + CLASS_NAME  + ".reassignTasks() ----->orgName=[" + orgName + "]");
		
		
		
		Hashtable groupHash = new Hashtable();
		groupHash.put("Groups.Group Name", orgName + " Admins");
		tcResultSet groupSet = groupIntf.findGroups(groupHash);	
		
		Vector taskDetails = new Vector();
		Vector emailList = new Vector();
		
		if (!groupSet.isEmpty()){		
			tcDataSet dsList = new tcDataSet();	
			
			String query = "SELECT oti.sch_key, mil.mil_name, obj.obj_name, usr.usr_login, usr.usr_udf_oid, mil.mil_retry_count " +
				"FROM oti, mil, obj, pkg, orc, usr, sch, usr usr1, act, ugp " +
				"WHERE oti.mil_key=mil.mil_key " +
				"AND oti.pkg_key=pkg.pkg_key " +
				"AND pkg.obj_key=obj.obj_key " +
				"AND oti.orc_key=orc.orc_key " +
				"AND orc.usr_key=usr.usr_key " +
				"AND usr.act_key=act.act_key " +
				"AND oti.sch_key=sch.sch_key " +
				"AND oti.osi_assigned_to_usr_key=usr1.usr_key(+) " +
				"AND oti.osi_assigned_to_ugp_key=ugp.ugp_key(+) " +
				"AND act.act_name='" + orgName + "' " +
				"AND (usr1.usr_login='XELSYSADM' or usr1.usr_login='OT_OIMRECON')";
	
			//logger.debug(query);   	
			dsList.setQuery(getDataBase(), query);	
			dsList.executeQuery();	
			
			//log.debug("List Size:" + dsList.getTotalRowCount());
			
			if (!dsList.isEmpty()){			
				log.debug("Total Rows: " + dsList.getTotalRowCount());			
				Vector taskKeys = new Vector();
				//long[] taskKeys = new long[dsList.getTotalRowCount()];
				
				for (int k = 0; k < dsList.getRowCount(); k++) {
		    		dsList.goToRow(k);  
		    		long taskKey = dsList.getLong("SCH_KEY");
		    		String retryCounter = dsList.getString("MIL_RETRY_COUNT");
		    		log.debug("Task Key[" + taskKey + "]Retry Count[" + retryCounter + "]");
		    		if (retryCounter.length()==0){
						//taskKeys[k] = dsList.getLong("SCH_KEY");
						taskKeys.add(taskKey);
			    		taskDetails.add(dsList.getString("USR_LOGIN") + "\t" + dsList.getString("USR_UDF_OID") + "\t" + dsList.getString("OBJ_NAME") + "\t"  + dsList.getString("MIL_NAME"));
		    		}else{
		    			tcDataSet dsList1 = new tcDataSet();			    			
		    			String query1 = "select osi.osi_retry_counter " +
		    				"from osi " +
		    				"where osi.sch_key = " + taskKey + " " + 
		    				"and osi.osi_retry_on <=sysdate";	
		    			log.debug("Query1[" + query1 + "]");
		    			dsList1.setQuery(getDataBase(), query1);	
		    			dsList1.executeQuery();
		    			long retry = dsList1.getLong("OSI_RETRY_COUNTER");
		    			log.debug("Retries Left[" + retry + "]");
		    			if (retry==0){
		    				taskKeys.add(taskKey);
				    		taskDetails.add(dsList.getString("USR_LOGIN") + "\t" + dsList.getString("USR_UDF_OID") + "\t" + dsList.getString("OBJ_NAME") + "\t"  + dsList.getString("MIL_NAME"));
		    			}else{
		    				log.debug("Ignore Task[" + taskKey + "]");
		    			}
		    		}
				}
				
				if (taskKeys.size()!=0){
					long[] taskKeysList = new long[taskKeys.size()];
					for (int i=0;i<taskKeys.size();i++){
						taskKeysList[i] = Long.parseLong(taskKeys.get(i).toString());
					}
					
					try{
						log.debug("Reassigning " + taskKeysList.length + " tasks to group " + groupSet.getStringValue("Groups.Group Name"));
						provIntf.reassignTasksToGroup(taskKeysList, groupSet.getLongValue("Groups.Key"));
					}catch(Exception e){
						e.printStackTrace();
					}
					
					tcResultSet memberList = groupIntf.getAllMembers(groupSet.getLongValue("Groups.Key"));
					if (!memberList.isEmpty()){
						Hashtable userHash = new Hashtable();
						for (int i=0;i<memberList.getTotalRowCount();i++){
							userHash.clear();
							memberList.goToRow(i);
							userHash.put("Users.Key", String.valueOf(memberList.getLongValue("Users.Key")));
							tcResultSet userSet = userIntf.findAllUsers(userHash);
							String email = userSet.getStringValue("Users.Email");
							if (email.trim().length()!=0){
								emailList.add(userSet.getStringValue("Users.Email"));
							
							}
						}
					}
					
				    String subject = "CUNYFirst - Task Assignment";
				    String body = "The following rejected tasks have been assigned to your security group:\n\n";
				    for (int i=0;i<taskDetails.size();i++){
				    	body+= taskDetails.get(i) + "\n";
				    }
				    body+= "\nPlease click the link below to review the tasks and determine if they should be resubmitted.  Contact the Central Security Group with any questions.\n\nThank you.";
				    body+= "\n\n" + url;
			    
				    log.debug("************************** Email Subject **************************");
				    log.debug(subject);
				    log.debug("************************** Email Body *****************************");
				    log.debug(body);
				    log.debug("************************** Email To *******************************");
				    log.debug(emailList.toString());
				    log.debug("************************** Email End**** **************************");
				    
	                if (getAttribute("Send Email").equalsIgnoreCase("TRUE")){
		                tcEmailNotificationUtil sendMail = new tcEmailNotificationUtil(getDataBase());
		                sendMail.setSubject(subject);		         	 
			         	sendMail.setBody(body);
			        	sendMail.setFromAddress(from);                        
				    
		                for (int i=0;i<emailList.size();i++){
		                	String to = emailList.get(i).toString();
		                	log.debug("Sending Email to user[" + to + "]");
		                	sendMail.sendEmail(to);	                	
		                }
				    }else{
				    	log.debug("Ignore Sending Email");
				    }
					
				}	
			}
		}
	}	
	
	protected tcUserOperationsIntf userIntf;
	protected tcGroupOperationsIntf groupIntf; 
	protected tcOrganizationOperationsIntf orgIntf; 
	protected tcProvisioningOperationsIntf provIntf; 

}