/**
 * 
 */
package edu.cuny.oim.scheduletasks;

import java.text.SimpleDateFormat;
import java.util.Hashtable;
import java.util.Vector;

import Thor.API.tcResultSet;
import Thor.API.Base.tcUtilityOperationsIntf;
import Thor.API.Exceptions.tcAPIException;
import Thor.API.Exceptions.tcBulkException;
import Thor.API.Exceptions.tcColumnNotFoundException;
import Thor.API.Exceptions.tcGroupNotFoundException;
import Thor.API.Exceptions.tcInvalidLookupException;
import Thor.API.Exceptions.tcInvalidValueException;
import Thor.API.Exceptions.tcObjectNotFoundException;
import Thor.API.Exceptions.tcStaleDataUpdateException;
import Thor.API.Exceptions.tcTaskNotFoundException;
import Thor.API.Exceptions.tcUserNotFoundException;
import Thor.API.Operations.tcFormInstanceOperationsIntf;
import Thor.API.Operations.tcGroupOperationsIntf;
import Thor.API.Operations.tcLookupOperationsIntf;
import Thor.API.Operations.tcReconciliationOperationsIntf;
import Thor.API.Operations.tcUserOperationsIntf;

import com.thortech.xl.dataaccess.tcDataSet;
import com.thortech.xl.dataaccess.tcDataSetException;
import com.thortech.xl.schedule.tasks.tcTskPasswordWarning;

import com.thortech.util.logging.Logger;
import com.thortech.xl.scheduler.tasks.SchedulerBaseTask;

/**
 * @author kpinsky
 *
 */

public class ViewITResourceDetails extends SchedulerBaseTask{
	private final String CLASS_NAME = "ViewITResourceDetails()";
	
	/**
	 * Logger
	 */
	
	public static Logger log = Logger.getLogger("CUNY.CUSTOM");
	
	
	//triggered task when performing a reconciliation event
	public void execute(){			
			
		System.out.println("Starting Scheduled Task ----- " + CLASS_NAME  + " ----->");
			System.out.println("Executing main task");		
			Hashtable resourceHash;
			try {
				resourceHash = com.thortech.xl.util.adapters.tcUtilXellerateOperations.getITAssetProperties(getDataBase(), getAttribute("IT Resource"));
				System.out.println(resourceHash.toString());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			

			System.out.println("Ending main task");			



	}
	
	//required to connect to the database without a login
	public tcUtilityOperationsIntf getUtilityOps(String utilityName){
		try {
			return (tcUtilityOperationsIntf)super.getUtility(utilityName);
		}
		catch (tcAPIException e){
			e.printStackTrace();
		}
		return null;
	}


}