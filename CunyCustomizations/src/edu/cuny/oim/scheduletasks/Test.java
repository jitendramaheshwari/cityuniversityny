/**
 * 
 */
package edu.cuny.oim.scheduletasks;

import java.text.SimpleDateFormat;
import java.util.Hashtable;
import java.util.Vector;

import Thor.API.tcResultSet;
import Thor.API.Base.tcUtilityOperationsIntf;
import Thor.API.Exceptions.tcAPIException;
import Thor.API.Exceptions.tcBulkException;
import Thor.API.Exceptions.tcColumnNotFoundException;
import Thor.API.Exceptions.tcGroupNotFoundException;
import Thor.API.Exceptions.tcInvalidLookupException;
import Thor.API.Exceptions.tcInvalidValueException;
import Thor.API.Exceptions.tcObjectNotFoundException;
import Thor.API.Exceptions.tcStaleDataUpdateException;
import Thor.API.Exceptions.tcTaskNotFoundException;
import Thor.API.Exceptions.tcUserNotFoundException;
import Thor.API.Operations.tcFormInstanceOperationsIntf;
import Thor.API.Operations.tcGroupOperationsIntf;
import Thor.API.Operations.tcLookupOperationsIntf;
import Thor.API.Operations.tcReconciliationOperationsIntf;
import Thor.API.Operations.tcUserOperationsIntf;

import com.thortech.xl.dataaccess.tcDataSet;
import com.thortech.xl.dataaccess.tcDataSetException;
import com.thortech.xl.schedule.tasks.tcTskPasswordWarning;

import com.thortech.util.logging.Logger;
import com.thortech.xl.scheduler.tasks.SchedulerBaseTask;

/**
 * @author kpinsky
 *
 */

public class Test extends SchedulerBaseTask{
	private final String CLASS_NAME = "Testing()";
	
	/**
	 * Logger
	 */
	
	public static Logger log = Logger.getLogger("CUNY.CUSTOM");
	
	
	//triggered task when performing a reconciliation event
	public void execute(){			
			System.out.println("Starting Scheduled Task ----- " + CLASS_NAME  + " ----->");
			System.out.println("Executing main task");		
			
			userIntf = (tcUserOperationsIntf)getUtilityOps("Thor.API.Operations.tcUserOperationsIntf");
			Hashtable userHash = new Hashtable();
			userHash.put("Users.User ID", "4655");
			
			try{
			tcResultSet userSet = userIntf.findAllUsers(userHash);
			System.out.println(userSet.getTotalRowCount());
			String[] columns = userSet.getColumnNames();
			for (int i=0;i<columns.length;i++){
				System.out.println(columns[i] + ":" + userSet.getStringValueFromColumn(i));
			}
			//System.out.println(userSet.getStringValue("USR_UDF_NTNL_ID"));
			}catch(Exception e){
				
			}
			/*reconIntf = (tcReconciliationOperationsIntf)getUtilityOps("Thor.API.Operations.tcReconciliationOperationsIntf");
			
			java.util.Date date = new java.util.Date(System.currentTimeMillis());
			SimpleDateFormat simpledateformat = new SimpleDateFormat(getAttribute("Format"));
			Hashtable reconHash = new Hashtable();
			reconHash.put("Users.BirthDate", simpledateformat.format(date).toString());
			try {
				reconIntf.createReconciliationEvent("Peoplesoft Trusted Source", reconHash, true);
			} catch (tcAPIException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (tcObjectNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			*/
			System.out.println("Ending main task");			



	}
	
	//required to connect to the database without a login
	public tcUtilityOperationsIntf getUtilityOps(String utilityName){
		try {
			return (tcUtilityOperationsIntf)super.getUtility(utilityName);
		}
		catch (tcAPIException e){
			e.printStackTrace();
		}
		return null;
	}
	
	protected tcReconciliationOperationsIntf reconIntf;
	protected tcUserOperationsIntf userIntf;
	protected tcLookupOperationsIntf lookupIntf;
	//userIntf = (tcUserOperationsIntf)getUtilityOps("Thor.API.Operations.tcUserOperationsIntf");
	protected tcFormInstanceOperationsIntf formIntf; 
	//userIntf = (tcUserOperationsIntf)getUtilityOps("Thor.API.Operations.tcFormInstanceOperationsIntf");
	protected tcGroupOperationsIntf groupIntf; 

}