package edu.cuny.oim.scheduletasks;

import Thor.API.Base.tcUtilityOperationsIntf;
import Thor.API.Exceptions.tcAPIException;
import Thor.API.Operations.tcFormInstanceOperationsIntf;
import Thor.API.Operations.tcGroupOperationsIntf;
import Thor.API.Operations.tcITResourceInstanceOperationsIntf;
import Thor.API.Operations.tcLookupOperationsIntf;
import Thor.API.Operations.tcObjectOperationsIntf;
import Thor.API.Operations.tcOrganizationOperationsIntf;
import Thor.API.Operations.tcProvisioningOperationsIntf;
import Thor.API.Operations.tcReconciliationOperationsIntf;
import Thor.API.Operations.tcUserOperationsIntf;

import com.thortech.util.logging.Logger;
import com.thortech.xl.dataaccess.tcDataSet;
import com.thortech.xl.dataaccess.tcDataSetException;
import com.thortech.xl.scheduler.tasks.SchedulerBaseTask;

public class RogueAccounts extends SchedulerBaseTask{
	public final String CLASS_NAME = "RogueAccounts()";
	private String taskHCMCS = "";
	private String taskPortal = "";
	private String taskELM = "";
	private String taskFinancials = "";
	private String taskEPM = "";
	private String taskCRM = "";
	private String userName = "";
	private long taskHCMCSkey = 0;
	private long taskPortalkey = 0;
	private long taskELMkey = 0;
	private long taskFinancialskey = 0;
	private long taskEPMkey = 0;
	private long taskCRMkey = 0;	
	private String resourceObject = "";
	
	
	/**
	 * Logger
	 */
	
	public static Logger log = Logger.getLogger("CUNY.CUSTOM");
	
	public boolean stop(){
	    log.debug("tcTskTimedRetry:stop:Task being stopped");
	    return true;
  	}
	
	//triggered task when performing a reconciliation event
	public void execute(){
		try{
			System.out.println("Starting Scheduled Task ----- " + CLASS_NAME  + " ----->");
			System.out.println("Executing main task");				
			
			taskHCMCS = getAttribute("HCM-CS Task Name");	
			taskPortal = getAttribute("Portal Task Name");	
			taskELM = getAttribute("ELM Task Name");
			taskFinancials = getAttribute("Financials Task Name");	
			taskEPM = getAttribute("EPM Task Name");
			taskCRM = getAttribute("CRM Task Name");
			resourceObject = getAttribute("Resource Object");
			userName = getAttribute("User Name");			
			
			long key = getProcessInstanceKey(userName);
			if (key!=0){				
				taskHCMCSkey = getTaskKey(taskHCMCS);
				if (taskHCMCSkey!=0){
					if (isStopped()){
						return;
					}				
					insertTask(key,taskHCMCSkey);			
				}
				
				taskPortalkey = getTaskKey(taskPortal);
				if (taskPortalkey!=0){
					if (isStopped()){
						return;
					}
					insertTask(key, taskPortalkey);			
				}
				
				taskELMkey = getTaskKey(taskELM);
				if (taskELMkey!=0){
					if (isStopped()){
						return;
					}
					insertTask(key, taskELMkey);			
				}
				
				taskFinancialskey = getTaskKey(taskFinancials);
				if (taskFinancialskey!=0){
					if (isStopped()){
						return;
					}
					insertTask(key, taskFinancialskey);			
				}
				
				taskEPMkey = getTaskKey(taskEPM);
				if (taskEPMkey!=0){
					if (isStopped()){
						return;
					}
					insertTask(key, taskEPMkey);			
				}
				
				taskCRMkey = getTaskKey(taskCRM);
				if (taskCRMkey!=0){
					if (isStopped()){
						return;
					}
					insertTask(key, taskCRMkey);			
				}
			}
			
			System.out.println("Ending main task");			

		}catch(Exception e){

		}

	}
	
	//required to connect to the database without a login
	public tcUtilityOperationsIntf getUtilityOps(String utilityName){
		try {
			return (tcUtilityOperationsIntf)super.getUtility(utilityName);
		}
		catch (tcAPIException e){
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Retrieves the task key of the supplied task name to insert into the instance
	 * @return
	 * @throws Exception
	 */
	public long getTaskKey(String taskName) throws Exception{
		long taskKey = 0;
		tcDataSet dsList = new tcDataSet();		
		String query = "select mil.mil_key from mil,pkg,tos,obj " + 
		"where mil.mil_name = '" + taskName + "' " +
		"and obj.obj_name='" + resourceObject + "' " + 
		"and obj.obj_key = pkg.obj_key and tos.pkg_key = pkg.pkg_key and mil.tos_key = tos.tos_key";
		
		log.debug(query);
		
		dsList.setQuery(getDataBase(), query);		
		dsList.executeQuery();
		
		if (!dsList.isEmpty()){
			taskKey = dsList.getLong("MIL_KEY");
			log.debug("Task Key: " + taskKey);			
		}
		
		return taskKey;
	}
	
	public long getProcessInstanceKey(String userLogin) throws tcDataSetException{
		tcDataSet dsList = new tcDataSet();		
		long processInstanceKey = 0;
		String query = "select usr.usr_login, obj.obj_name, oiu.orc_key " +
			"from usr, obi, obj, oiu, orc, pkg " +
			"where obi.obi_key=oiu.obi_key " +
			"and oiu.usr_key=usr.usr_key " +
			"and oiu.orc_key=orc.orc_key " +
			"and orc.pkg_key=pkg.pkg_key " +
			"and pkg.obj_key=obj.obj_key " +
			"and obj.obj_name='" + resourceObject + "' " +
			"and usr.usr_login='" + userLogin + "' ";
		
		log.debug(query);
		
		dsList.setQuery(getDataBase(), query);		
		dsList.executeQuery();
		
		if (!dsList.isEmpty()){
			processInstanceKey = dsList.getLong("ORC_KEY");
			log.debug("User Login[" + userLogin +"]Process Instace Key[" + processInstanceKey + "]");			
		}
		
		return processInstanceKey;
		
	}
	
	/**
	 * Inserts the task into the instance of the object
	 * @param processInstanceKey
	 * @throws Exception
	 */
	public void insertTask(long processInstanceKey, long taskKey) throws Exception{
		provIntf = (tcProvisioningOperationsIntf)getUtilityOps("Thor.API.Operations.tcProvisioningOperationsIntf"); 
		provIntf.addProcessTaskInstance(taskKey, processInstanceKey);
	}

	
	//examples of how to create an instance for a specific operation
	protected tcReconciliationOperationsIntf reconIntf;
	//reconIntf = (tcReconciliationOperationsIntf)getUtilityOps("Thor.API.Operations.tcReconciliationOperationsIntf");
	protected tcUserOperationsIntf userIntf;
	//userIntf = (tcUserOperationsIntf)getUtilityOps("Thor.API.Operations.tcUserOperationsIntf");
	protected tcFormInstanceOperationsIntf formIntf; 
	//userIntf = (tcUserOperationsIntf)getUtilityOps("Thor.API.Operations.tcFormInstanceOperationsIntf");
	protected tcGroupOperationsIntf groupIntf; 
	//groupIntf = (tcGroupOperationsIntf)getUtilityOps("Thor.API.Operations.tcGroupOperationsIntf");
	protected tcITResourceInstanceOperationsIntf itresourceIntf;
	//itresourceIntf = (tcITResourceInstanceOperationsIntf)getUtilityOps("Thor.API.Operations.tcITResourceInstanceOperationsIntf");
	protected tcLookupOperationsIntf lookupIntf; 
	//lookupIntf = (tcLookupOperationsIntf)getUtilityOps("Thor.API.Operations.tcLookupOperationsIntf");
	protected tcObjectOperationsIntf objectIntf;
	//objectIntf = (tcObjectOperationsIntf)getUtilityOps("Thor.API.Operations.tcObjectOperationsIntf");
	protected tcOrganizationOperationsIntf orgIntf; 
	//orgIntf = (tcOrganizationOperationsIntf)getUtilityOps("Thor.API.Operations.tcOrganizationOperationsIntf");
	protected tcProvisioningOperationsIntf provIntf; 
	//provIntf = (tcProvisioningOperationsIntf)getUtilityOps("Thor.API.Operations.tcProvisioningOperationsIntf");
	//reqIntf = (tcRequestOperationsIntf)getUtilityOps("Thor.API.Operations.tcRequestOperationsIntf");
}