package edu.cuny.oim.scheduletasks;

import java.util.Hashtable;
import java.util.Vector;
import java.io.*;
import java.util.*;

import com.thortech.util.logging.Logger;

import Thor.API.Operations.tcITResourceDefinitionOperationsIntf;
import Thor.API.Operations.tcReconciliationOperationsIntf;
import Thor.API.Operations.tcFormInstanceOperationsIntf;
import Thor.API.Operations.tcUserOperationsIntf;
import Thor.API.Operations.tcGroupOperationsIntf;
import Thor.API.Operations.tcITResourceInstanceOperationsIntf;
import Thor.API.Operations.tcLookupOperationsIntf; 
import Thor.API.Operations.tcObjectOperationsIntf;
import Thor.API.Operations.tcOrganizationOperationsIntf; 
import Thor.API.Operations.tcProvisioningOperationsIntf; 

import com.thortech.xl.dataaccess.tcDataSet;
import com.thortech.xl.dataaccess.tcDataSetException;
import com.thortech.xl.dataobj.util.tcEmailNotificationUtil;
import com.thortech.xl.scheduler.tasks.SchedulerBaseTask;
import Thor.API.Base.tcUtilityOperationsIntf;
import Thor.API.Exceptions.tcAPIException;
import Thor.API.Exceptions.tcAwaitingApprovalDataCompletionException;
import Thor.API.Exceptions.tcAwaitingObjectDataCompletionException;
import Thor.API.Exceptions.tcTaskNotFoundException;
import Thor.API.tcResultSet;
import Thor.API.tcUtilityFactory;

public class AddProcessTask extends SchedulerBaseTask{
	public final String CLASS_NAME = "RetryScheduledTasks()";
	private String taskName = "";
	private String resourceObject = "";
	private long taskKey = 0;
	
	/**
	 * Logger
	 */
	
	public static Logger log = Logger.getLogger("CUNY.CUSTOM");
	
	public boolean stop(){
	    log.debug("tcTskTimedRetry:stop:Task being stopped");
	    return true;
  	}
	
	//triggered task when performing a reconciliation event
	public void execute(){
		try{
			System.out.println("Starting Scheduled Task ----- " + CLASS_NAME  + " ----->");
			System.out.println("Executing main task");		
			
			taskName = getAttribute("Task Name");
			resourceObject = getAttribute("Resource Object");	
			Vector users = readFile();
			
			getTaskKey();
			if (taskKey!=0){
				for (int i=0;i<users.size();i++){
					if (isStopped()){
						return;
					}
					long key = getProcessInstanceKey(users.get(i).toString());
					if (key>0) insertTask(key);					
				}
			}

			
			System.out.println("Ending main task");			

		}catch(Exception e){

		}

	}
	
	public Vector readFile() throws Exception{
		String fileName = getAttribute("FileName");
		Vector users = new Vector();
		log.info("Reading File " + fileName);
		Hashtable userHash = new Hashtable();
		File f = new File (fileName);
		
	    BufferedReader in = new BufferedReader(new FileReader(f));
	    String str = "";
	    
		int counter = 0;
	    while ((str = in.readLine()) != null) {	
	    	log.debug("***** Reading line " + counter + " *****");
	    	counter++;
	    	String user = str.trim();
	    	if (user.length()!=0){
	    		log.debug("User Id:" + user);
	    		users.add(user);
	    	}
	    }
	    
	    return users;
	}
	
	//required to connect to the database without a login
	public tcUtilityOperationsIntf getUtilityOps(String utilityName){
		try {
			return (tcUtilityOperationsIntf)super.getUtility(utilityName);
		}
		catch (tcAPIException e){
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Retrieves the task key of the supplied task name to insert into the instance
	 * @return
	 * @throws Exception
	 */
	public void getTaskKey() throws Exception{
		tcDataSet dsList = new tcDataSet();		
		String query = "select mil.mil_key from mil,pkg,tos,obj " + 
		"where mil.mil_name = '" + taskName + "' " +
		"and obj.obj_name='" + resourceObject + "' " + 
		"and obj.obj_key = pkg.obj_key and tos.pkg_key = pkg.pkg_key and mil.tos_key = tos.tos_key";
		
		log.debug(query);
		
		dsList.setQuery(getDataBase(), query);		
		dsList.executeQuery();
		
		if (!dsList.isEmpty()){
			taskKey = dsList.getLong("MIL_KEY");
			log.debug("Task Key: " + taskKey);			
		}
		
	}
	
	public long getProcessInstanceKey(String userLogin) throws tcDataSetException{
		tcDataSet dsList = new tcDataSet();		
		long processInstanceKey = 0;
		String query = "select usr.usr_login, obj.obj_name, oiu.orc_key " +
			"from usr, obi, obj, oiu, orc, pkg " +
			"where obi.obi_key=oiu.obi_key " +
			"and oiu.usr_key=usr.usr_key " +
			"and oiu.orc_key=orc.orc_key " +
			"and orc.pkg_key=pkg.pkg_key " +
			"and pkg.obj_key=obj.obj_key " +
			"and obj.obj_name='" + resourceObject + "' " +
			"and usr.usr_login='" + userLogin + "' ";
		
		log.debug(query);
		
		dsList.setQuery(getDataBase(), query);		
		dsList.executeQuery();
		
		if (!dsList.isEmpty()){
			processInstanceKey = dsList.getLong("ORC_KEY");
			log.debug("User Login[" + userLogin +"]Process Instace Key[" + processInstanceKey + "]");			
		}
		
		return processInstanceKey;
		
	}
	
	/**
	 * Inserts the task into the instance of the object
	 * @param processInstanceKey
	 * @throws Exception
	 */
	public void insertTask(long processInstanceKey) throws Exception{
		provIntf = (tcProvisioningOperationsIntf)getUtilityOps("Thor.API.Operations.tcProvisioningOperationsIntf"); 
		provIntf.addProcessTaskInstance(taskKey, processInstanceKey);
	}

	
	//examples of how to create an instance for a specific operation
	protected tcReconciliationOperationsIntf reconIntf;
	//reconIntf = (tcReconciliationOperationsIntf)getUtilityOps("Thor.API.Operations.tcReconciliationOperationsIntf");
	protected tcUserOperationsIntf userIntf;
	//userIntf = (tcUserOperationsIntf)getUtilityOps("Thor.API.Operations.tcUserOperationsIntf");
	protected tcFormInstanceOperationsIntf formIntf; 
	//userIntf = (tcUserOperationsIntf)getUtilityOps("Thor.API.Operations.tcFormInstanceOperationsIntf");
	protected tcGroupOperationsIntf groupIntf; 
	//groupIntf = (tcGroupOperationsIntf)getUtilityOps("Thor.API.Operations.tcGroupOperationsIntf");
	protected tcITResourceInstanceOperationsIntf itresourceIntf;
	//itresourceIntf = (tcITResourceInstanceOperationsIntf)getUtilityOps("Thor.API.Operations.tcITResourceInstanceOperationsIntf");
	protected tcLookupOperationsIntf lookupIntf; 
	//lookupIntf = (tcLookupOperationsIntf)getUtilityOps("Thor.API.Operations.tcLookupOperationsIntf");
	protected tcObjectOperationsIntf objectIntf;
	//objectIntf = (tcObjectOperationsIntf)getUtilityOps("Thor.API.Operations.tcObjectOperationsIntf");
	protected tcOrganizationOperationsIntf orgIntf; 
	//orgIntf = (tcOrganizationOperationsIntf)getUtilityOps("Thor.API.Operations.tcOrganizationOperationsIntf");
	protected tcProvisioningOperationsIntf provIntf; 
	//provIntf = (tcProvisioningOperationsIntf)getUtilityOps("Thor.API.Operations.tcProvisioningOperationsIntf");
	//reqIntf = (tcRequestOperationsIntf)getUtilityOps("Thor.API.Operations.tcRequestOperationsIntf");
}