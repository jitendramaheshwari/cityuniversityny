package edu.cuny.oim.scheduletasks;

import java.util.Hashtable;
import java.util.Vector;
import java.io.*;
import java.util.*;

import com.thortech.util.logging.Logger;

import Thor.API.Operations.tcITResourceDefinitionOperationsIntf;
import Thor.API.Operations.tcReconciliationOperationsIntf;
import Thor.API.Operations.tcFormInstanceOperationsIntf;
import Thor.API.Operations.tcUserOperationsIntf;
import Thor.API.Operations.tcGroupOperationsIntf;
import Thor.API.Operations.tcITResourceInstanceOperationsIntf;
import Thor.API.Operations.tcLookupOperationsIntf; 
import Thor.API.Operations.tcObjectOperationsIntf;
import Thor.API.Operations.tcOrganizationOperationsIntf; 
import Thor.API.Operations.tcProvisioningOperationsIntf; 

import com.thortech.xl.crypto.tcCryptoException;
import com.thortech.xl.crypto.tcCryptoUtil;
import com.thortech.xl.crypto.tcSignatureMessage;
import com.thortech.xl.dataaccess.tcDataProvider;
import com.thortech.xl.dataaccess.tcDataSet;
import com.thortech.xl.dataaccess.tcDataSetException;
import com.thortech.xl.dataobj.util.tcEmailNotificationUtil;
import com.thortech.xl.scheduler.tasks.SchedulerBaseTask;
import com.thortech.xl.util.config.ConfigurationClient;

import Thor.API.Base.tcUtilityOperationsIntf;
import Thor.API.Exceptions.tcAPIException;
import Thor.API.Exceptions.tcAwaitingApprovalDataCompletionException;
import Thor.API.Exceptions.tcAwaitingObjectDataCompletionException;
import Thor.API.Exceptions.tcColumnNotFoundException;
import Thor.API.Exceptions.tcLoginAttemptsExceededException;
import Thor.API.Exceptions.tcPasswordResetAttemptsExceededException;
import Thor.API.Exceptions.tcStaleDataUpdateException;
import Thor.API.Exceptions.tcTaskNotFoundException;
import Thor.API.Exceptions.tcUserAccountDisabledException;
import Thor.API.Exceptions.tcUserAccountInvalidException;
import Thor.API.Exceptions.tcUserAlreadyLoggedInException;
import Thor.API.Exceptions.tcUserNotFoundException;
import Thor.API.tcResultSet;
import Thor.API.tcUtilityFactory;

public class ReapplyGlobalStatus extends SchedulerBaseTask{
	public final String CLASS_NAME = "ReapplyGlobalStatus()";
	private String resObj = "";
	private Vector users = null;
	private Vector studentFields = null;
	private Vector employeeFields = null;
	/**
	 * Logger
	 */
	
	public static Logger log = Logger.getLogger("CUNY.CUSTOM");
	
	public boolean stop(){
	    log.debug("tcTskTimedRetry:stop:Task being stopped");
	    return true;
  	}
	
	//triggered task when performing a reconciliation event
	public void execute(){
		try{
			System.out.println("Starting Scheduled Task ----- " + CLASS_NAME  + " ----->");
			System.out.println("Executing main task");		
			users = new Vector();
			employeeFields = new Vector();
			studentFields = new Vector();
			getFields();			
			log.debug("Users List:" + users.size());
			findUsers();
			if (isStopped()){
				return;
			}
			if (users.size()!=0){
				if (isStopped()){
					return;
				}
				for (int i=0;i<users.size();i++){
					if (isStopped()){
						return;
					}
					setGlobalStatus(users.get(i).toString(), 
					"USR_UDF_GLBL_STU_STATUS", 
					"USR_UDF_GLBL_EMP_STATUS",
					"USR_UDF_GLBL_STATUS");
				}
			}
			


			
			System.out.println("Ending main task");			

		}catch(Exception e){

		}

	}
	
	//required to connect to the database without a login
	public tcUtilityOperationsIntf getUtilityOps(String utilityName){
		try {
			return (tcUtilityOperationsIntf)super.getUtility(utilityName);
		}
		catch (tcAPIException e){
			e.printStackTrace();
		}
		return null;
	}

	public void findUsers() throws tcAPIException{
		log.debug("findUsers() Parameter Variables passed are:");
	    userIntf = (tcUserOperationsIntf)getUtilityOps("Thor.API.Operations.tcUserOperationsIntf");
	    tcDataSet dsList = new tcDataSet();	
		
		try{
				String query = "select usr_login from usr where usr_udf_glbl_status is null and " +
					"usr_login not like 'XEL%'";
				log.debug("Query:" + query);
				dsList.setQuery(getDataBase(), query);
				dsList.executeQuery();
				for (int i = 0; i < dsList.getTotalRowCount(); i++) {
					if (isStopped()){
						return;
					}
					dsList.goToRow(i);
					String login = String.valueOf(dsList.getString("USR_LOGIN"));
					log.debug("User Login:" + login);
					users.add(login);	
				}
			} catch (tcDataSetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
				
		}
	
	public void getFields() throws tcDataSetException{
		log.debug("getFields() Parameter Variables passed are:");
		tcDataSet dsList = new tcDataSet();
		//Retrieve values from lookup for student global status
		String query = "SELECT DISTINCT ORG_UDF_STUDENT FROM ACT WHERE ORG_UDF_STUDENT IS NOT NULL";
		log.debug("Query:" + query);
		dsList.setQuery(getDataBase(), query);
		dsList.executeQuery();			
		for (int i = 0; i < dsList.getTotalRowCount(); i++) {
			dsList.goToRow(i);
			String field = dsList.getString("ORG_UDF_STUDENT");
			log.debug("Field:" + field);
			studentFields.add(field);	
		}	
		
		query = "SELECT DISTINCT ORG_UDF_EMPLOYEE FROM ACT WHERE ORG_UDF_EMPLOYEE IS NOT NULL";
		log.debug("Query:" + query);
		dsList.setQuery(getDataBase(), query);
		dsList.executeQuery();
		for (int i = 0; i < dsList.getTotalRowCount(); i++) {
			dsList.goToRow(i);
			String field = dsList.getString("ORG_UDF_EMPLOYEE");
			log.debug("Field:" + field);
			employeeFields.add(field);	
		}	
		
	}
	public void setGlobalStatus(String userLogin, String studentStatusField, String employeeStatusField, String globalStatusField) throws tcAPIException{
		log.debug("setGlobalStatus() Parameter Variables passed are:" + 
				"userLogin=[" + userLogin + "]" + 
				"studentStatusField=[" + studentStatusField + "]" +
				"employeeStatusField=[" + employeeStatusField + "]" +
				"globalStatusField=[" + globalStatusField + "]");	
		
		String employeeStatus = "";
		String studentStatus = "";
		String globalStatus = "";
		String query = "";
		lookupIntf =(tcLookupOperationsIntf)getUtilityOps("Thor.API.Operations.tcLookupOperationsIntf");
		userIntf =(tcUserOperationsIntf)getUtilityOps("Thor.API.Operations.tcUserOperationsIntf");
		orgIntf =(tcOrganizationOperationsIntf)getUtilityOps("Thor.API.Operations.tcOrganizationOperationsIntf");
		tcDataSet dsList = new tcDataSet();	
		
		try {
			//Get result set for user
			Hashtable userHash = new Hashtable();
			userHash.put("Users.User ID", userLogin);
			tcResultSet userSet = userIntf.findAllUsers(userHash);
			userSet.goToRow(0);
			long userKey = userSet.getLongValue("Users.Key");			
			//Query user for student values
			query = "SELECT * FROM USR WHERE USR_KEY=" + userKey;
			log.debug("Query:" + query);
			dsList.setQuery(getDataBase(), query);
			dsList.executeQuery();			
			dsList.goToRow(0);
			
			//Get all student status values
			Hashtable studentStatusHash = new Hashtable();
			log.debug("Getting status values for students");
			for (int i=0;i<studentFields.size();i++){
				String value = dsList.getString(studentFields.get(i).toString());
				if (value.length()>0){
					log.debug("Value:" + studentFields.get(i) + "|" + value);
				}
				studentStatusHash.put(i, dsList.getString(studentFields.get(i).toString()));
			}
			
			//Get all employee status values
			Hashtable employeeStatusHash = new Hashtable();
			log.debug("Getting status values for employees");
			for (int i=0;i<employeeFields.size();i++){
				String value = dsList.getString(employeeFields.get(i).toString());
				if (value.length()>0){
					log.debug("Value:" + employeeFields.get(i) + "|" + value);
				}
				employeeStatusHash.put(i, dsList.getString(employeeFields.get(i).toString()));
			}
			
			Hashtable orgHash = new Hashtable();
			String orgKey = userSet.getStringValue("Organizations.Key");
			log.debug("User Organization:" + orgKey);
			orgHash.put("Organizations.Key", orgKey);
			tcResultSet orgSet = orgIntf.findOrganizations(orgHash);		
			orgSet.goToRow(0);
			log.debug("Organization Name:" + orgSet.getStringValue("Organizations.Organization Name"));
			String employeeField = orgSet.getStringValue("ORG_UDF_EMPLOYEE");
			String studentField = orgSet.getStringValue("ORG_UDF_STUDENT");
			log.debug("Employee Field:" + employeeField);
			log.debug("Student Field:" + studentField);
			
			//Find status for employees
			log.debug("If statement for employees");
			if (employeeStatusHash.containsValue("A") ||
					employeeStatusHash.containsValue("L") ||
					employeeStatusHash.containsValue("P") ||
					employeeStatusHash.containsValue("S") ||
					employeeStatusHash.containsValue("W")){
				employeeStatus = "Active";
			}else{
				//employeeStatus = userSet.getStringValue(employeeField);
				if (employeeStatusHash.containsValue("R") ||
						employeeStatusHash.containsValue("Q")){
					employeeStatus = "Retired";
				}else if (employeeStatusHash.containsValue("T") ||
						employeeStatusHash.containsValue("D") ||
						employeeStatusHash.containsValue("U") ||
						employeeStatusHash.containsValue("V") ||
						employeeStatusHash.containsValue("X")){
					employeeStatus = "Inactive";
				}
			}
			
				
			log.debug("employeeStatus:" + employeeStatus);
			
			//Find status for Students
			log.debug("If statement for students");
			if (studentStatusHash.containsValue("AC") || 
					studentStatusHash.containsValue("AD") ||
					studentStatusHash.containsValue("AP") ||
					studentStatusHash.containsValue("CM") ||
					studentStatusHash.containsValue("CN") ||
					studentStatusHash.containsValue("DC") ||
					studentStatusHash.containsValue("DM") ||
					studentStatusHash.containsValue("LA") ||
					studentStatusHash.containsValue("PM") ||
					studentStatusHash.containsValue("SP") ||
					studentStatusHash.containsValue("WT")){
				studentStatus = "Active";
			}else if (studentStatusHash.containsValue("DE")){
				studentStatus= "Inactive";
			}
			
			log.debug("studentStatus:" + studentStatus);
			
			if (studentStatus.length()!=0){
				globalStatus = studentStatus;
			}else{
				globalStatus = employeeStatus;
			}
	
			userHash.clear();
			userHash.put(studentStatusField, studentStatus);
			userHash.put(employeeStatusField, employeeStatus);
			userHash.put(globalStatusField, globalStatus);
			if (isStopped()){
				return;
			}
			
			//log.debug("BEFORE:User Global Status Values Updated: studentStatus[" + studentStatus + "]" +
			//		"employeeStatus[" + employeeStatus + "]globalStatus[" + globalStatus + "]");
			userIntf.updateUser(userSet, userHash);
			
			log.debug("AFTER:User Global Status Values Updated: studentStatus[" + studentStatus + "]" +
					"employeeStatus[" + employeeStatus + "]globalStatus[" + globalStatus + "]");
			
		} catch (tcColumnNotFoundException e) {
			// TODO Auto-generated catch block
			log.error("tcColumnNotFoundException");
			e.printStackTrace();
		} catch (tcDataSetException e) {
			// TODO Auto-generated catch block
			log.error("tcDataSetException");
			e.printStackTrace();
		} catch (tcUserNotFoundException e) {
			// TODO Auto-generated catch block
			log.error("tcUserNotFoundException");
			e.printStackTrace();
		} catch (tcStaleDataUpdateException e) {
			// TODO Auto-generated catch block
			log.error("tcStaleDataUpdateException");
			e.printStackTrace();
		}	
		
	}
	//examples of how to create an instance for a specific operation
	protected tcReconciliationOperationsIntf reconIntf;
	//reconIntf = (tcReconciliationOperationsIntf)getUtilityOps("Thor.API.Operations.tcReconciliationOperationsIntf");
	protected tcUserOperationsIntf userIntf;
	//userIntf = (tcUserOperationsIntf)getUtilityOps("Thor.API.Operations.tcUserOperationsIntf");
	protected tcFormInstanceOperationsIntf formIntf; 
	//userIntf = (tcUserOperationsIntf)getUtilityOps("Thor.API.Operations.tcFormInstanceOperationsIntf");
	protected tcGroupOperationsIntf groupIntf; 
	//groupIntf = (tcGroupOperationsIntf)getUtilityOps("Thor.API.Operations.tcGroupOperationsIntf");
	protected tcITResourceInstanceOperationsIntf itresourceIntf;
	//itresourceIntf = (tcITResourceInstanceOperationsIntf)getUtilityOps("Thor.API.Operations.tcITResourceInstanceOperationsIntf");
	protected tcLookupOperationsIntf lookupIntf; 
	//lookupIntf = (tcLookupOperationsIntf)getUtilityOps("Thor.API.Operations.tcLookupOperationsIntf");
	protected tcObjectOperationsIntf objectIntf;
	//objectIntf = (tcObjectOperationsIntf)getUtilityOps("Thor.API.Operations.tcObjectOperationsIntf");
	protected tcOrganizationOperationsIntf orgIntf; 
	//orgIntf = (tcOrganizationOperationsIntf)getUtilityOps("Thor.API.Operations.tcOrganizationOperationsIntf");
	protected tcProvisioningOperationsIntf provIntf; 
	//provIntf = (tcProvisioningOperationsIntf)getUtilityOps("Thor.API.Operations.tcProvisioningOperationsIntf");

}