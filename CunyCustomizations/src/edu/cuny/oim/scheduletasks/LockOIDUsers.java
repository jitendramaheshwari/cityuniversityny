/**
 *
 */
package edu.cuny.oim.scheduletasks;


import Thor.API.Exceptions.tcAPIException;
import Thor.API.Exceptions.tcColumnNotFoundException;
import Thor.API.Exceptions.tcInvalidLookupException;
import Thor.API.Operations.tcITResourceInstanceOperationsIntf;
import Thor.API.Operations.tcLookupOperationsIntf;
import Thor.API.tcResultSet;

import com.thortech.util.logging.Logger;

import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;

import oracle.iam.identity.exception.NoSuchUserException;
import oracle.iam.identity.exception.UserLockException;
import oracle.iam.identity.exception.ValidationFailedException;
import oracle.iam.identity.usermgmt.api.UserManager;
import oracle.iam.platform.Platform;
import oracle.iam.scheduler.vo.TaskSupport;


/**
 * @author kpinsky
 *
 */
public class LockOIDUsers extends TaskSupport {

    /**
     * Logger
     */
    private static Logger log = Logger.getLogger("CUNY.CUSTOM");
    private final String CLASS_NAME = "LockOIDUsers()";
    private String serverAddress;
    private String port;
    private String rootContext;
    private String adminID;
    private String adminPWD;
    private String sslFlag;
    private DirContext directoryContext;
    private LdapContext ldapContext;

    private String itResource;
    private String timeStampField;
    private String lockAttribute;
    private String lockAttributeValue;
    private String svcLookup;

    //triggered task when performing a reconciliation event

    public void execute(HashMap hashMap) {
        Vector users = new Vector();
        Vector svcUsers = new Vector();
        log.debug("Starting Scheduled Task ----- " + CLASS_NAME + " ----->");
        log.debug("Executing main task");

        itResource = "OID Server";
        timeStampField = "cunyedulastlogin";
        lockAttribute = "oblockouttime";
        lockAttributeValue = String.valueOf(System.currentTimeMillis());
        svcLookup = "Lookup.CUNY.Service.Accounts";

        try {
            Hashtable resource = getITResource();
            SetupConnection(resource.get("host").toString(),
                            resource.get("port").toString(),
                            resource.get("baseContexts").toString(),
                            resource.get("principal").toString(),
                            resource.get("credentials").toString(),
                            resource.get("ssl").toString());
            //connectToOID(container);
            users = getUserListTimeStamp(timeStampField, "cn");
            svcUsers = getSVCUsers();
            log.debug("List Size:" + users.size());
            updateUsers(users, svcUsers);
            //disconnectFromOID();
        } catch (Exception e) {
            log.error("Error retrieving hash table");
            e.printStackTrace();
        }
        log.debug("Ending main task");

    }

    private Hashtable getITResource() {
        Hashtable resource = new Hashtable();
        try {
            tcITResourceInstanceOperationsIntf itResOper =
                Platform.getService(tcITResourceInstanceOperationsIntf.class);
            HashMap<String, String> searchcriteria =
                new HashMap<String, String>();
            searchcriteria.put("IT Resources.Name", itResource);
            log.info("IT Resource Name " + itResource);
            tcResultSet resultSet =
                itResOper.findITResourceInstances(searchcriteria);
            log.info("Result Set Count " + resultSet.getTotalRowCount());
            tcResultSet itResSet =
                itResOper.getITResourceInstanceParameters(resultSet.getLongValue("IT Resources.Key"));
            log.info("itResuletSet Count " + itResSet.getRowCount());
            for (int i = 0; i < itResSet.getRowCount(); i++) {
                itResSet.goToRow(i);
                String paramName =
                    itResSet.getStringValue("IT Resources Type Parameter.Name");
                String paramVal =
                    itResSet.getStringValue("IT Resource.Parameter.Value");
                log.info("Parameter Name" + paramName + " VALUE: " + paramVal);
                resource.put(paramName, paramVal);
            }
        } catch (Exception e) {
            log.error("Exception ", e);

        }
        return resource;
    }

    public void updateUsers(Vector users, Vector svcUsers) {
        log.debug(CLASS_NAME + ".updateUsers() -----> inputs=user[" +
                  users.size() + "]");
        log.debug("Updating attribute[" + lockAttribute + "]" + " to value[" +
                  lockAttributeValue + "]");
        UserManager userManager = Platform.getService(UserManager.class);
        for (int i = 0; i < users.size(); i++) {
            String userDN = "cn=" + users.get(i) + "," + rootContext;
            log.debug("User:" + userDN);
            if (svcUsers.contains(users.get(i))) {
                try {
                    BasicAttributes basicattributes =
                        new BasicAttributes(true);
                    basicattributes.put(new BasicAttribute(timeStampField,
                                                           null));
                    ldapContext.modifyAttributes(userDN,
                                                 ldapContext.REMOVE_ATTRIBUTE,
                                                 basicattributes);
                } catch (NamingException e) {
                    log.error("Error:", e);
                }
            } else {
                /*
                try {
                    BasicAttributes basicattributes =
                        new BasicAttributes(true);
                    basicattributes.put(new BasicAttribute(lockAttribute,
                                                           lockAttributeValue));
                    ldapContext.modifyAttributes(userDN,
                                                 ldapContext.REPLACE_ATTRIBUTE,
                                                 basicattributes);
                } catch (NamingException e) {
                    log.error("Error:", e);
                }
                */
                try {
                    userManager.lock((String)users.get(i), true);
                } catch (Exception e) {
                    log.error("Error:", e);
                }
            }
        }
    }

    /**
     * Creates connection to an OID.
     *
     * @param serverAddress
     * @param port
     * @param rootContext
     * @param adminID
     * @param adminPWD
     * @param sslFlag
     */
    private void SetupConnection(String serverAddress, String port,
                                 String rootContext, String adminID,
                                 String adminPWD, String sslFlag) {
        log.debug(CLASS_NAME + " -----> inputs=serverAddress[" +
                  serverAddress + "]port[" + port + "]rootContext[" +
                  rootContext + "]adminID[" + adminID + "]adminPWD[********]" +
                  "sslFlag[" + sslFlag + "]");

        this.serverAddress = serverAddress;
        this.port = port;
        this.rootContext = rootContext;
        this.adminID = adminID;
        this.adminPWD = adminPWD;
        this.sslFlag = sslFlag;
    }

    /**
     * Creates connection to OID at a specific container
     *
     * @param container - container of the users
     */
    private void connectToOID() {
        log.debug(CLASS_NAME + ".connectToOID() ----->");


        String providerURL = "ldap://" + serverAddress + ":" + port;

        Hashtable env = new Hashtable();
        env.put(Context.INITIAL_CONTEXT_FACTORY,
                "com.sun.jndi.ldap.LdapCtxFactory");

        env.put(Context.PROVIDER_URL, providerURL);
        env.put(Context.SECURITY_PRINCIPAL, adminID); // + "," + rootContext);
        ///env.put(Context.SECURITY_PRINCIPAL, adminID);
        env.put(Context.SECURITY_CREDENTIALS, adminPWD);
        env.put(Context.REFERRAL, "ignore");
        env.put("java.naming.ldap.version", "3");
        if (this.sslFlag.equalsIgnoreCase("true")) {
            env.put(Context.SECURITY_PROTOCOL, "ssl");
        }

        try {
            directoryContext = new InitialDirContext(env);

        } catch (NamingException e) {
            log.error("Naming Exception Error:" + e.getExplanation());
            e.printStackTrace();
        }

        try {
            ldapContext = new InitialLdapContext(env, null);
            log.info("Connection to OID Instance Successful");
        } catch (NamingException e) {
            log.error("Naming Exception Error:" + e.getExplanation());
            e.printStackTrace();
        }

    }

    /**
     * Disconnects from OID
     */
    private void disconnectFromOID() {
        log.debug(CLASS_NAME + ".disconnectFromOID() ----->");

        try {
            directoryContext.close();
            log.info("Disconnect from OID Instance Successful");
        } catch (NamingException e) {
            log.error("Naming Exception Error:" + e.getExplanation());
            e.printStackTrace();
        }

    }

    /**
     * Returns a list of all userids that match the criteria of the search
     * string
     *
     * @param timeStamp - container of users
     * @param attribute - value to return (cn)
     * @return
     * @throws Exception
     */
    public Vector getUserListTimeStamp(String timeStamp,
                                       String attribute) throws Exception {
        Vector users = new Vector();
        log.debug(CLASS_NAME + ".getUserListTimeStamp() ----->" +
                  "inputs=timeStamp[" + timeStamp + "]" + "attribute[" +
                  attribute + "]");

        String query =
            "(&(&(objectclass=person)(" + timeStamp + "<=" + get120DaysPrior() +
            ")))";
        log.debug("Query:" + query);
        connectToOID();
        SearchControls ctls = new SearchControls();
        ctls.setSearchScope(SearchControls.SUBTREE_SCOPE);

        try {
            NamingEnumeration answer =
                ldapContext.search(rootContext, query, ctls);
            while (answer.hasMoreElements()) {
                SearchResult searchResult = (SearchResult)answer.next();
                Attributes attributes = searchResult.getAttributes();
                Attribute attr = attributes.get(attribute);
                String attrValue1 = (String)attr.get();
                log.debug(attribute + ":" + attrValue1);
                attr = attributes.get(lockAttribute);
                try {
                    String attrValue2 = (String)attr.get();
                } catch (Exception e) {
                    users.add(attrValue1.toUpperCase());
                }
            }
        } catch (NamingException e) {
            log.error("Exception ", e);
        }
        disconnectFromOID();
        return users;
    }

    /**
     * Returns a string value of the date in milliseconds 120 days prior to
     * system date
     *
     * @return
     */
    public String get120DaysPrior() {
        SimpleDateFormat simpledateformat = new SimpleDateFormat("MM-dd-yyyy");
        java.util.Date currentDate =
            new java.util.Date(System.currentTimeMillis());
        log.debug("Todays Date:" + System.currentTimeMillis() + " " +
                  simpledateformat.format(currentDate));

        Calendar cal = new GregorianCalendar();
        cal.setTime(currentDate);
        cal.add(cal.DAY_OF_YEAR, -120);
        java.util.Date oldDate = new java.util.Date(cal.getTimeInMillis());
        log.debug("120 Days Prior:" + cal.getTimeInMillis() + " " +
                  simpledateformat.format(oldDate));
        String value = String.valueOf(cal.getTimeInMillis());
        value =
                String.valueOf(cal.getTimeInMillis()).substring(0, value.length() -
                                                                3);
        log.debug("Returning value:" + value);
        return (value);
    }

    public Vector getSVCUsers() throws tcAPIException,
                                       tcInvalidLookupException,
                                       tcColumnNotFoundException {
        Vector svcUsers = new Vector();
        tcLookupOperationsIntf lookupIntf =
            Platform.getService(Thor.API.Operations.tcLookupOperationsIntf.class);
        tcResultSet lookupSet = lookupIntf.getLookupValues(svcLookup);
        for (int i = 0; i < lookupSet.getTotalRowCount(); i++) {
            lookupSet.goToRow(i);
            String user =
                lookupSet.getStringValue("Lookup Definition.Lookup Code Information.Code Key").toUpperCase();
            log.debug("Service Account User:" + user);
            svcUsers.add(user);
        }

        return svcUsers;
    }

    @Override
    public HashMap getAttributes() {
        return null;
    }

    @Override
    public void setAttributes() {
    }

}
