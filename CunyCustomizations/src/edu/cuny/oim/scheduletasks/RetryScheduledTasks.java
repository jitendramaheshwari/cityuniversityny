package edu.cuny.oim.scheduletasks;

import java.util.Hashtable;
import java.util.Vector;
import java.io.*;
import java.util.*;

import com.thortech.util.logging.Logger;

import Thor.API.Operations.tcITResourceDefinitionOperationsIntf;
import Thor.API.Operations.tcReconciliationOperationsIntf;
import Thor.API.Operations.tcFormInstanceOperationsIntf;
import Thor.API.Operations.tcUserOperationsIntf;
import Thor.API.Operations.tcGroupOperationsIntf;
import Thor.API.Operations.tcITResourceInstanceOperationsIntf;
import Thor.API.Operations.tcLookupOperationsIntf; 
import Thor.API.Operations.tcObjectOperationsIntf;
import Thor.API.Operations.tcOrganizationOperationsIntf; 
import Thor.API.Operations.tcProvisioningOperationsIntf; 

import com.thortech.xl.dataaccess.tcDataSet;
import com.thortech.xl.dataaccess.tcDataSetException;
import com.thortech.xl.dataobj.util.tcEmailNotificationUtil;
import com.thortech.xl.scheduler.tasks.SchedulerBaseTask;
import Thor.API.Base.tcUtilityOperationsIntf;
import Thor.API.Exceptions.tcAPIException;
import Thor.API.Exceptions.tcAwaitingApprovalDataCompletionException;
import Thor.API.Exceptions.tcAwaitingObjectDataCompletionException;
import Thor.API.Exceptions.tcTaskNotFoundException;
import Thor.API.tcResultSet;
import Thor.API.tcUtilityFactory;

public class RetryScheduledTasks extends SchedulerBaseTask{
	public final String CLASS_NAME = "RetryScheduledTasks()";
	private String resObj = "";
	private Vector taskKeys = null;
	/**
	 * Logger
	 */
	
	public static Logger log = Logger.getLogger("CUNY.CUSTOM");
	
	public boolean stop(){
	    log.debug("tcTskTimedRetry:stop:Task being stopped");
	    return true;
  	}
	
	//triggered task when performing a reconciliation event
	public void execute(){
		try{
			System.out.println("Starting Scheduled Task ----- " + CLASS_NAME  + " ----->");
			System.out.println("Executing main task");		
			taskKeys = new Vector();
			String taskName = getAttribute("Task Name");
			String resourceObject = getAttribute("Resource Object");
			String responseCode = getAttribute("Response Code");
			String startTask = getAttribute("Start Number");
			String endTask = getAttribute("End Number"); 
			resObj = resourceObject;
			
			findTasks(taskName, resourceObject, responseCode, startTask, endTask);
			
			if (taskKeys.size()!=0){
				if (isStopped()){
					return;
				}
				retryRejectedTasks();
			}

			
			System.out.println("Ending main task");			

		}catch(Exception e){

		}

	}
	
	//required to connect to the database without a login
	public tcUtilityOperationsIntf getUtilityOps(String utilityName){
		try {
			return (tcUtilityOperationsIntf)super.getUtility(utilityName);
		}
		catch (tcAPIException e){
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Searches for all tasks that are in a rejected state
	 * Reassigns all tasks to appropriate admin group for organization
	 * @throws Exception
	 */
	public void findTasks(String taskName, String resourceObject, String responseCode, String startTask, String endTask) throws Exception{
		log.info("Running " + CLASS_NAME  + ".findTasks() ----->");
		log.debug("taskName:" + taskName);
		log.debug("resourceObject:" + resourceObject);
		log.debug("responseCode:" + responseCode);
		provIntf = (tcProvisioningOperationsIntf)getUtilityOps("Thor.API.Operations.tcProvisioningOperationsIntf");
		groupIntf = (tcGroupOperationsIntf)getUtilityOps("Thor.API.Operations.tcGroupOperationsIntf");
		userIntf = (tcUserOperationsIntf)getUtilityOps("Thor.API.Operations.tcUserOperationsIntf");

		tcDataSet dsList = new tcDataSet();		
		
		String query = "SELECT oti.sch_key " +
		"FROM sch, osi, mil, pkg, req, usr, ugp, obj, oiu, orc, usr usr1, act, usr usr2, oti " +
		"WHERE sch.sch_status in ('R') " +
		"AND oti.sch_key = sch.sch_key " +
		"AND sch.sch_key = osi.sch_key " +
		"AND osi.mil_key = mil.mil_key "  +
		"AND pkg.pkg_key = osi.pkg_key " +
		"AND req.req_key(+) = osi.req_key " +
		"AND osi.osi_assigned_to_usr_key = usr.usr_key(+) " +
		"AND osi.osi_assigned_to_ugp_key = ugp.ugp_key(+) " +
		"AND oiu.usr_key=usr1.usr_key " +
		"AND act.act_key = usr1.act_key " +
		"AND obj.obj_key = pkg.obj_key " +
		"AND pkg.pkg_type = 'Provisioning' " +
		"AND oiu.orc_key = osi.orc_key " +
		"AND orc.orc_key = osi.orc_key " +
		"AND osi.osi_assigned_to_usr_key = usr2.usr_key " +
		"AND mil.mil_name='" + taskName + "' " +
		"AND obj.obj_name like '" + resourceObject + "' " +
		"AND (sch.sch_data in " + responseCode + " or sch.sch_data is null)";
	
		log.debug(query);   	
		dsList.setQuery(getDataBase(), query);	
		dsList.executeQuery();	
			
		if (!dsList.isEmpty()){			
			log.debug("Total Rows: " + dsList.getTotalRowCount());				
			int end = Integer.parseInt(endTask);
			if (Integer.parseInt(endTask)>dsList.getRowCount()){
				log.debug("End task greater than total rows, setting to total row count");
				end = dsList.getRowCount();
			}
			
			for (int k = 0; k < end; k++) {
	    		dsList.goToRow(k);  
	    		log.debug("Task Key: " + dsList.getLong("SCH_KEY"));
	    		taskKeys.add(String.valueOf(dsList.getLong("SCH_KEY")));
			}

		}	    
	    
	}
	
	public void retryRejectedTasks(){
		log.info("Running " + CLASS_NAME  + ".retryRejectedTasks() ----->");
		provIntf = (tcProvisioningOperationsIntf)getUtilityOps("Thor.API.Operations.tcProvisioningOperationsIntf");
		log.debug("Total Tasks to retry:" + taskKeys.size());
		
		for (int i=0;i<taskKeys.size();i++){	
			if (isStopped()){
				return;
			}			

			long key = Long.parseLong(taskKeys.get(i).toString());			
			try {
				provIntf.retryTask(key);
				log.debug("Task key " + key + " retrying(" + i + " " + resObj + ")");
			} catch (tcAPIException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (tcTaskNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}
	
	//examples of how to create an instance for a specific operation
	protected tcReconciliationOperationsIntf reconIntf;
	//reconIntf = (tcReconciliationOperationsIntf)getUtilityOps("Thor.API.Operations.tcReconciliationOperationsIntf");
	protected tcUserOperationsIntf userIntf;
	//userIntf = (tcUserOperationsIntf)getUtilityOps("Thor.API.Operations.tcUserOperationsIntf");
	protected tcFormInstanceOperationsIntf formIntf; 
	//userIntf = (tcUserOperationsIntf)getUtilityOps("Thor.API.Operations.tcFormInstanceOperationsIntf");
	protected tcGroupOperationsIntf groupIntf; 
	//groupIntf = (tcGroupOperationsIntf)getUtilityOps("Thor.API.Operations.tcGroupOperationsIntf");
	protected tcITResourceInstanceOperationsIntf itresourceIntf;
	//itresourceIntf = (tcITResourceInstanceOperationsIntf)getUtilityOps("Thor.API.Operations.tcITResourceInstanceOperationsIntf");
	protected tcLookupOperationsIntf lookupIntf; 
	//lookupIntf = (tcLookupOperationsIntf)getUtilityOps("Thor.API.Operations.tcLookupOperationsIntf");
	protected tcObjectOperationsIntf objectIntf;
	//objectIntf = (tcObjectOperationsIntf)getUtilityOps("Thor.API.Operations.tcObjectOperationsIntf");
	protected tcOrganizationOperationsIntf orgIntf; 
	//orgIntf = (tcOrganizationOperationsIntf)getUtilityOps("Thor.API.Operations.tcOrganizationOperationsIntf");
	protected tcProvisioningOperationsIntf provIntf; 
	//provIntf = (tcProvisioningOperationsIntf)getUtilityOps("Thor.API.Operations.tcProvisioningOperationsIntf");
	//reqIntf = (tcRequestOperationsIntf)getUtilityOps("Thor.API.Operations.tcRequestOperationsIntf");
}