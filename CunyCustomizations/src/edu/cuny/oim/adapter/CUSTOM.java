package edu.cuny.oim.adapter;


import Thor.API.Exceptions.tcAPIException;
import Thor.API.Exceptions.tcColumnNotFoundException;
import Thor.API.Exceptions.tcLoginAttemptsExceededException;
import Thor.API.Exceptions.tcPasswordResetAttemptsExceededException;
import Thor.API.Exceptions.tcUserAccountDisabledException;
import Thor.API.Exceptions.tcUserAccountInvalidException;
import Thor.API.Exceptions.tcUserAlreadyLoggedInException;
import Thor.API.Exceptions.tcUserNotFoundException;
import Thor.API.Operations.tcGroupOperationsIntf;
import Thor.API.tcResultSet;

import com.thortech.util.logging.Logger;
import com.thortech.xl.crypto.tcCryptoException;

import edu.cuny.oim.OIDFunctions;
import edu.cuny.oim.util.APIUtil;

import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import oracle.iam.identity.orgmgmt.api.OrganizationManager;
import oracle.iam.identity.orgmgmt.api.OrganizationManagerConstants;
import oracle.iam.identity.orgmgmt.vo.Organization;
import oracle.iam.identity.rolemgmt.api.RoleManager;
import oracle.iam.identity.rolemgmt.api.RoleManagerConstants;
import oracle.iam.identity.usermgmt.api.UserManager;
import oracle.iam.identity.usermgmt.api.UserManagerConstants;
import oracle.iam.identity.usermgmt.vo.User;
import oracle.iam.platform.Platform;
import oracle.iam.provisioning.api.ProvisioningService;
import oracle.iam.provisioning.vo.Account;
import oracle.iam.provisioning.vo.AccountData;


public class CUSTOM {
    /**
     * Logger
     */
    private static String CLASS_NAME = "CUSTOM";
    private static Logger log = Logger.getLogger("CUNY.CUSTOM");


    /**
     * Revokes access for Financials, CRM, ELM, and EPM
     * Peoplesoft CRM	Change Global Employee Status
     * Peoplesoft ELM	Change Global Employee Status
     * Peoplesoft EPM	Change Global Employee Status
     * Peoplesoft Financials	Change Global Employee Status
     * @param userKey - Users.Key
     * @param processInstanceKey - Process Instance Key
     * @param employeeStatus - User Definition - Employee Global Status
     * @return
     */
    public String disableResourceCheck(long userKey, long processInstanceKey,
                                       String employeeStatus) {
        log.debug("disableResourceCheck() Parameter Variables passed are:" +
                  "userKey=[" + userKey + "]" + "processInstanceKey=[" +
                  processInstanceKey + "]" + "employeeStatus=[" +
                  employeeStatus + "]");
        String response = "ERROR";
        ProvisioningService provService =
            Platform.getService(ProvisioningService.class);
        List<Account> accounts = null;
        String accountId = null;
        try {
            accounts =
                    provService.getAccountsProvisionedToUser(userKey + "", true);
            for (Account acc : accounts) {
                String procInstInstanceKey = acc.getProcessInstanceKey();
                if (procInstInstanceKey.equals(processInstanceKey + "")) {
                    accountId = acc.getAccountID(); // OIU_KEY
                    if (employeeStatus.equalsIgnoreCase("Inactive") ||
                        employeeStatus.equalsIgnoreCase("Retired")) {
                        provService.disable(Long.parseLong(accountId));
                        log.debug("Application disabled for user");
                        response = "SUCCESS";
                    } else {
                        response = "NO_ACTION_REQUIRED";
                    }
                }
            }
        } catch (Exception e) {
            response = "ERROR";
            log.error(CLASS_NAME + "EXCEPTION ", e);
        }
        log.debug("response:" + response);
        return response;
    }

    /**
     * It will return enable/disable. Used in
     * AD User	Change Terminated
AD User	Change Global Status
EAD User	Change Global Status
EAD User	Change Terminated
OID User	Change Global Status
OID User	Change Terminated
Peoplesoft HCM-CS	Change Global Status
Peoplesoft HCM-CS	Change Terminated
Peoplesoft Portal	Change Global Status
Peoplesoft Portal	Change Terminated
UAD User	Change Global Status
UAD User	Change Terminated

     * @param terminated - Termintaed status
     * @param globalStatus - User-Global Status
     * @return String
     */
    public String terminatedUserCheck(String terminated, String globalStatus) {
        log.debug("terminatedUserCheck() Parameter Variables passed are:" +
                  "terminated=[" + terminated + "]" + "globalStatus=[" +
                  globalStatus + "]");
        String response = "ERROR";
        if (globalStatus != null &&
            globalStatus.equalsIgnoreCase("Inactive")) {
            if (terminated != null && terminated.equalsIgnoreCase("TRUE")) {
                response = "DISABLE";
            } else if (terminated == null || terminated.isEmpty()) {
                response = "DISABLE";
            }
        } else {
            response = "ENABLE";
        }

        log.debug("response:" + response);
        return response;
    }



    /**
     * Function adds a user to an OIM Group
     * PeopleSoft Reporting FIN	Add User to Group
PeopleSoft Reporting HCM	Add User to Group
Peoplesoft CRM	Add User to Group
Peoplesoft EPM	Add User to Group
Peoplesoft Financials	Add User to Group
     * @param userKey - Users.Key
     * @param groupName - OIM group name
     * @return response as USER_ADDED_TO_GROUP
     */
    public String addUserToOIMGroup(long userKey, String groupName) {
        log.debug("addUserToOIMGroup() Parameter Variables passed are:" +
                  "userKey=[" + userKey + "]" + "groupName=[" + groupName +
                  "]");
        RoleManager roleMgr = Platform.getService(RoleManager.class);
        String response = "ERROR";

        try {
            roleMgr.grantRole(RoleManagerConstants.ROLE_NAME, groupName,
                              UserManagerConstants.AttributeName.USER_KEY.getId(),
                              "" + userKey);
            log.debug("User added to group " + groupName);
            response = "SUCCESS";
        } catch (Exception e) {
            log.error("Error 4", e);
            response = "ERROR";
            e.printStackTrace();
        }


        return response;

    }

    /**
     * Function removes a user from an OIM Group
     * PeopleSoft Reporting HCM	Remove User from Group
Peoplesoft CRM	Remove User from Group
Peoplesoft EPM	Remove User from Group
Peoplesoft Financials	Remove User from Group
     * @param userKey - Users.Key
     * @param groupName - OIM group name
     * @return - response
     * @throws tcAPIException
     * @throws tcCryptoException
     * @throws tcUserAlreadyLoggedInException
     * @throws tcUserAccountInvalidException
     * @throws tcLoginAttemptsExceededException
     * @throws tcPasswordResetAttemptsExceededException
     * @throws tcUserAccountDisabledException
     * @throws Exception
     */
    public String removeUserFromOIMGroup(long userKey,
                                         String groupName) throws tcAPIException,
                                                                  tcCryptoException,
                                                                  tcUserAccountDisabledException,
                                                                  tcPasswordResetAttemptsExceededException,
                                                                  tcLoginAttemptsExceededException,
                                                                  tcUserAccountInvalidException,
                                                                  tcUserAlreadyLoggedInException {
        log.debug("removeUserFromOIMGroup() Parameter Variables passed are:" +
                  "userKey=[" + userKey + "]" + "groupName=[" + groupName +
                  "]");

        String response = "ERROR";
        tcGroupOperationsIntf grpIntf =
            Platform.getService(Thor.API.Operations.tcGroupOperationsIntf.class);
        Hashtable mhSearchCriteria = new Hashtable();
        mhSearchCriteria.put("Groups.Group Name", groupName);

        //Result Set of Specified Group
        tcResultSet grpResultSet = grpIntf.findGroups(mhSearchCriteria);

        try {
            grpIntf.removeMemberUser(grpResultSet.getLongValue("Groups.Key"),
                                     userKey);
            log.debug("User removed from group " + groupName);
            response = "SUCCESS";
        } catch (Exception e) {
            log.error(CLASS_NAME + "EXCEPTION ", e);
        }

        return response;

    }

    /**
     * Used for Peoplesoft ELM	Change Disable Date
     * @param userKey - Users.Key
     * @param processInstanceKey - Process Instance Key
     * @return
     * @throws tcAPIException
     * @throws tcUserNotFoundException
     * @throws tcColumnNotFoundException
     */
    public String enableAppForUser(String userKey,
                                   long processInstanceKey, Date oldDisableDate, Date newDisableDate) throws tcAPIException,
                                                                   tcUserNotFoundException,
                                                                   tcColumnNotFoundException {
        log.debug("enableAppForUser() Parameter Variables passed are:" +
                  "userKey=[" + userKey + "]" + "processInstanceKey=[" +
                  processInstanceKey + "]");
        String response = "ERROR";
        
        if(!APIUtil.checkIfDateEquals(oldDisableDate, "1969-12-31", "yyyy-mm-dd")  && APIUtil.checkIfDateEquals(newDisableDate, "1969-12-31", "yyyy-mm-dd")) {
            ProvisioningService provSer =
                Platform.getService(ProvisioningService.class);
            List<Account> accounts = null;
            try {
                accounts =
                        provSer.getAccountsProvisionedToUser(userKey, false);
                for (Account acc : accounts) {
                    if ((acc.getProcessInstanceKey()).equals("" +
                                                             processInstanceKey)) {
                        try {
                            provSer.enable(Long.parseLong(acc.getAccountID()));
                            response = "SUCCESS";
                            log.debug("Application enabled for user");
                        } catch (Exception ee) {
                            log.error(CLASS_NAME + "EXCEPTION ", ee);
                        }
                    }
                }
            } catch (Exception e) {
                log.error(CLASS_NAME + "EXCEPTION ", e);
            }
        } else {
            response = "SUCCESS";
        }
        log.debug("response" + response);
        return response;
    }


    /**
     * Searches for a organization name based on a string input value of the organization key
     * @param orgKey - organization key
     * @return String - organization name
     */
    public String returnOrgName(String orgKey) {
        log.debug("returnOrgName() Parameter Variables passed are:" +
                  "orgKey=[" + orgKey + "]");

        Organization org = null;
        Set<String> setAttrs = new HashSet<String>();
        setAttrs.add(OrganizationManagerConstants.AttributeName.ORG_NAME.getId());

        try {
            OrganizationManager orgIntf =
                Platform.getService(OrganizationManager.class);
            org = orgIntf.getDetails(orgKey, setAttrs, false);
            if (org != null)
                return (String)org.getAttribute(OrganizationManagerConstants.AttributeName.ORG_NAME.getId());
        } catch (Exception e) {
            log.error("Exception returnOrgName ", e);
        }
        return null;
    }

    /**
     * Creates the correct date format for obPasswordCreationDate
     * @return
     */
    public String setPasswdCreationDate(String userKey) {
        // return USR_PWD_CREATION_DATE from user table.
        String METHOD_NAME = " setPasswdCreationDate ";
        log.debug("setPasswdCreationDate() Parameter Variables passed are:" +
                  "userKey=[" + userKey + "]");

        UserManager userManager = Platform.getService(UserManager.class);
        Date pwdCreationDate = new Date(System.currentTimeMillis());
        Set<String> searchAttrs = new HashSet<String>();
        searchAttrs.add(UserManagerConstants.AttributeName.USR_PWD_CREATION_DATE.getId());
        String value = null;
        try {
            User user = userManager.getDetails(userKey, searchAttrs, false);
            pwdCreationDate = user.getPasswordCreationDate();      
            if(pwdCreationDate!=null) {
                SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd");
                SimpleDateFormat sdfTime = new SimpleDateFormat("HH:mm:ss");
//"yyyy-MM-dd'T'hh:mm:ss"
                String s1 = sdfDate.format(pwdCreationDate);
                String s2 = sdfTime.format(pwdCreationDate);
                value = s1 + "T" + s2 + "Z";
            }
        } catch (Exception e) {
            log.error("Exception pwdCreationDate ", e);
        }
        log.debug(CLASS_NAME + METHOD_NAME + "value: " + value);

        return value;
    }

    /**
     * Creates the correct date format for obPasswordCreationDate
     * @return
     */
    public String getLoginRetryCount(String userKey) {
        log.debug("getLoginRetryCount() Parameter Variables passed are:" +
                  "userKey=[" + userKey + "]");
        UserManager userManager = Platform.getService(UserManager.class);
        Set<String> searchAttrs = new HashSet<String>();
        searchAttrs.add(UserManagerConstants.AttributeName.LOGIN_ATTEMPTS_CTR.getId());

        try {
            User user = userManager.getDetails(userKey, searchAttrs, false);
            return user.getLoginAttemptsCounter() + "";
        } catch (Exception e) {
            log.error("Exception pwdCreationDate ", e);
        }
        return "0";

    }

    /**
     * Locks the OID account by setting the oblockouttime value with the current time
     * or unlocks the OID by removing the attribute oblockouttime
     * @param userKey - Users.Key
     * @return
     */
    public String lockUnlockOIDUsers(String userKey) {
        String METHOD_NAME = " lockUnlockOIDUsers ";
        log.debug("lockUnlockOIDUsers() Parameter Variables passed are:" +
                  "userKey=[" + userKey + "]");
        UserManager userManager = Platform.getService(UserManager.class);
        Set<String> searchAttrs = new HashSet<String>();
        searchAttrs.add(UserManagerConstants.AttributeName.ACCOUNT_STATUS.getId());
        searchAttrs.add(UserManagerConstants.AttributeName.USER_LOGIN.getId());
        String userLogin;
        String accStatus;
        try {
            User user = userManager.getDetails(userKey, searchAttrs, false);
            accStatus = user.getAccountStatus();
            userLogin = user.getLogin();
            log.debug(CLASS_NAME + METHOD_NAME + "userLogin: " + userLogin +
                      " accStatus:" + accStatus);
            if (accStatus.equalsIgnoreCase("LOCKED")) {
                APIUtil.setLockoutDate(userKey, "USR_UDF_LOCK_TS");
            }
            APIUtil.updateLockAttributeOID(userLogin,
                                           accStatus.equalsIgnoreCase("LOCKED"));
            return "SUCCESS";
        } catch (Exception e) {
            log.error("Exception lockUnlockOIDUsers ", e);
        }
        return "ERROR";

    }

    /**
     * This method will update the process form data using the User-login & Employeenumber given. Based on if the user-login is less than 20 characted. Used in UAD
     * @param userKey The USer Key
     * @param userPrincipalColumnName  UD column holding the principal name
     * @param processInstanceKey process instance key value
     * @param emailIDDomian  the email domain part
     * @param newUserLogin  the changed user-login
     * @param userIDColumnName  the UD column holding user-ID
     * @param employeeNumber employeeNumber.
     * @return SUCESS or FAILURE
     */
    public String updateOnUserLoginChange(String userKey, String userPrincipalColumnName, long processInstanceKey, String emailIDDomian, String newUserLogin, String userIDColumnName, String employeeNumber){
        log.debug("updateOnUserLoginChange() Parameter Variables passed are:" +
                  "userKey=[" + userKey + "]" + "processInstanceKey=[" +
                  processInstanceKey + "]" + "userPrincipalColumnName=[" +
                  userPrincipalColumnName + "]" + " emailIDDomian : " + emailIDDomian + " newData: " + newUserLogin + " userIDColumnName: " + userIDColumnName + " employeeNumber: " +employeeNumber);
        String response = "ERROR";
        ProvisioningService provService =
            Platform.getService(ProvisioningService.class);
        List<Account> accounts = null;
        String accountId = null;
        try {
            accounts =
                    provService.getAccountsProvisionedToUser(userKey + "", true);
            for (Account acc : accounts) {
                String procInstInstanceKey = acc.getProcessInstanceKey();
                if (procInstInstanceKey.equals(processInstanceKey + "")) {
                    accountId = acc.getAccountID(); // OIU_KEY
                    AccountData accData = acc.getAccountData();
                    
                    Map<java.lang.String, java.lang.Object> data = accData.getData();
                    data.put(userPrincipalColumnName, newUserLogin + "@" + emailIDDomian);
                    if(newUserLogin.length() <=20) {
                        data.put(userIDColumnName, newUserLogin);
                    } else {
                        data.put(userIDColumnName, employeeNumber);
                    }
                    log.debug("updateOnUserLoginChange() data:" + data);

                    acc.setAccountData(accData);
                    try {
                        provService.modify(acc);
                        response = "SUCCESS";
                    } catch (Exception p) {
                        log.error(CLASS_NAME + "EXCEPTION ", p);
                    }
                }
            }
        } catch (Exception e) {
            response = "ERROR";
            log.error(CLASS_NAME + "EXCEPTION ", e);
        }
        log.debug("response:" + response);
        return response;        
    }

    public String returnUserUID(String newUserLogin, String employeeNumber){
        log.debug("returnUserUID() Parameter Variables passed are: newData: " + newUserLogin + " employeeNumber: " +employeeNumber);
        String response = employeeNumber;
        if(newUserLogin.length() <=20) {
            response = newUserLogin;
        }
        log.debug("response:" + response);
        return response;        
    }

    /**
     * This method will return the principal name.
     * @param emailIDDomian  the email domain part
     * @param newUserLogin  the changed user-login
     * @return UserPrincipalName
     */
    public String returnEADUserPrincipal(String emailIDDomian, String newUserLogin){
        log.debug("returnEADUserPrincipal() Parameter Variables passed are:" + " emailIDDomian : " + emailIDDomian + " newData: " + newUserLogin );
        String response = newUserLogin + "@" + emailIDDomian;
        log.debug("response:" + response);
        return response;        
    }


}
