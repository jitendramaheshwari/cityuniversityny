package edu.cuny.oim.adapter;

public class CampusDomain implements Comparable<CampusDomain>{
    private String campusName; // OrgName
    private String campusUSRUDFName;
    private String domainName;
    
    @Override
    public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + ((campusName == null) ? 0 : campusName.hashCode());
            return result;
    }
    
    @Override
    public boolean equals(Object obj) {
            if (this == obj)
                    return true;
            if (obj == null)
                    return false;
            if (getClass() != obj.getClass())
                    return false;
            CampusDomain other = (CampusDomain) obj;
            if (campusName == null) {
                    if (other.campusName != null)
                            return false;
            } else if (!campusName.equals(other.campusName))
                    return false;
            return true;
    }
    
    public String toString() {
        return " campusName : " + campusName +
            " domainName : " + domainName +
            " campusUSRUDFName : " + campusUSRUDFName;
    }
    
    public CampusDomain(String campusName, String domainName, String campusUSRUDFName) {
        this.campusName=campusName;
        this.domainName=domainName;
        this.campusUSRUDFName = campusUSRUDFName;
    }
    
    public String getCampusName(){
        return this.campusName;
    }

    public String getDomainName(){
        return this.domainName;
    }
    
    public String getCampusUSRUDFName(){
        return this.campusUSRUDFName;
    }

    public int compareTo(CampusDomain obj1) {
        CampusDomain that = (CampusDomain)obj1;
        String p1 = this.getDomainName();
        String p2 = that.getDomainName();
        return p1.compareTo(p2);
    }
}
