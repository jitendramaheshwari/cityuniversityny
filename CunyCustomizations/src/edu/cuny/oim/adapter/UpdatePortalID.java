package edu.cuny.oim.adapter;

import Thor.API.Operations.tcITResourceInstanceOperationsIntf;
import Thor.API.tcResultSet;

import com.thortech.util.logging.Logger;

import java.util.HashMap;
import java.util.Hashtable;
import javax.naming.directory.*;

import oracle.iam.platform.Platform;

public class UpdatePortalID
{
/**
 * It will be used to update the OID user-login on chnage of User-login in OIM.
 * 
 */
    private static Logger log = Logger.getLogger("CUNY.CUSTOM");
    
    /**
     * It will change the user-login in OID for any change in OIM.
     * @param OIDITResourceName
     * @param newUserID
     * @param oldUserID
     * @return
     */
    public String updateOIDLogin(String OIDITResourceName, String newUserID, String oldUserID)
    {
        String response = "";
        try
        {
            Hashtable resource = getITResource(OIDITResourceName);
            String serverName = resource.get("host").toString();
            String portNo = resource.get("port").toString();
            String userbaseContext = resource.get("baseContexts").toString();
            String principalDN = resource.get("principal").toString();
            String principalPass = resource.get("credentials").toString();
            String sslFlag = resource.get("ssl").toString();
            log.debug("serverName = " + serverName);
            log.debug("portNo = " + portNo);
            log.debug("userbaseContext = " + userbaseContext);
            log.debug("principalDN = " + principalDN);
            log.debug("oldUserID = " + oldUserID);
            log.debug("newUserID = " + newUserID);
            log.debug("sslFlag = " + sslFlag);
            log.debug("===================================\n");
            String contextFactory = "com.sun.jndi.ldap.LdapCtxFactory";
            String providerURL = new String("ldap://" + serverName + ":" + portNo);
            Hashtable oEnv = new Hashtable();
            oEnv.put("java.naming.provider.url", providerURL);
            oEnv.put("java.naming.factory.initial", contextFactory);
            oEnv.put("java.naming.security.principal", principalDN);
            oEnv.put("java.naming.security.credentials", principalPass);
            oEnv.put("java.naming.security.authentication", "simple");
            if(sslFlag.equalsIgnoreCase("true"))
            {
                oEnv.put("java.naming.security.protocol", "ssl");
            }
            DirContext ctx = new InitialDirContext(oEnv);
            String oldUserDN = "cn=" + oldUserID + "," + userbaseContext;
            String newUserDN = "cn=" + newUserID + "," + userbaseContext;
            log.debug("oldUserDN-->" + oldUserDN);
            log.debug("newUserDN-->" + newUserDN);
            ctx.rename(oldUserDN, newUserDN);
            BasicAttributes basicattributes = new BasicAttributes(true);
            String attribute = "uid";
            basicattributes.put(new BasicAttribute(attribute, newUserID));
            ctx.modifyAttributes(newUserDN, 2, basicattributes);
            response = "USER_UPDATE_SUCCESSFUL";
            ctx.close();
        }
        catch(Exception e)
        {
            response = "USER_UPDATE_FAILED";
            e.printStackTrace();
        }
        return response;
    }
    private Hashtable getITResource(String itResource) {
        Hashtable resource = new Hashtable();
        try {
            tcITResourceInstanceOperationsIntf itResOper
                    = Platform.getService(tcITResourceInstanceOperationsIntf.class);
            HashMap<String, String> searchcriteria
                    = new HashMap<String, String>();
            searchcriteria.put("IT Resources.Name", itResource);
            log.info("IT Resource Name " + itResource);
            tcResultSet resultSet
                    = itResOper.findITResourceInstances(searchcriteria);
            log.info("Result Set Count " + resultSet.getTotalRowCount());
            tcResultSet itResSet
                    = itResOper.getITResourceInstanceParameters(resultSet.getLongValue("IT Resources.Key"));
            log.info("itResuletSet Count " + itResSet.getRowCount());
            for (int i = 0; i < itResSet.getRowCount(); i++) {
                itResSet.goToRow(i);
                String paramName
                        = itResSet.getStringValue("IT Resources Type Parameter.Name");
                String paramVal
                        = itResSet.getStringValue("IT Resource.Parameter.Value");
                log.info("Parameter Name" + paramName + " VALUE: " + paramVal);
                resource.put(paramName, paramVal);
            }
        } catch (Exception e) {
            log.error("Exception ", e);

        }
        return resource;
    }

}