/*
 * Purpose    : Change the CN of a target object in a target directory using JNDI with LDAP.
 *              This is to be used as an OIM 10g Process Task adapter.
 * Author     : Ray Ramos
 * Date       : 02-18-2015
 * References : http://docs.oracle.com/javase/tutorial/jndi/ops/index.html
 *              http://docs.oracle.com/javase/tutorial/jndi/ldap/digest.html
 * Synopsis   : Given an EMPLID and the desired end-state CN, this class will look for the current CN where
 *              employeeID attribute equals the given EMPLID. Then, this current
 *              CN and the end-state CN are used as input for a rename method.
 *              This class uses JNDI, LDAPS, SASL, MD5-DIGEST and does it's work over SSL.
 * Known Bugs : Returns SUCCESS even if only makes it as far as calling ctx.rename.
 *              For 100% certainty, we'd like to search for the end-state CN and confirm that
 *              a single entry is returned. We can repurpose searchForOldCN as searchByCN to do this.
 */
package edu.cuny.oim.adapter;
import java.util.Hashtable;

import javax.naming.AuthenticationException;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

public class NameChanger {
    /* searchForOldDN is used by other classes to find user by oldName
   * If oldName exists in the LDAP, and there is only one entry that
   * matches, then the CN is returned. Otherwise an error of
   * MATCH_NOT_FOUND or MORE_THAN_ONE is returned.
   */

    public static String searchForOldCN(DirContext ctx, String employeeID,
                                        int debugToggle) {
        String returnValue = new String();
        String base = ""; // where to look relative to our PROVIDER_URL
        String[] attributeFilter = { "cn" }; // what to return
        String filter = "(&(employeeID=" + employeeID + "))";
        if (debugToggle != 0)
            System.out.println("DEBUG: filter=" + filter);
        SearchControls sc = new SearchControls();

        sc.setReturningAttributes(attributeFilter);
        sc.setSearchScope(SearchControls.SUBTREE_SCOPE);
        try {
            NamingEnumeration results =
                ctx.search(base, filter, sc); // do the search, assign to results
            if (results.hasMore() == false) // empty result
            {
                System.out.println("ERROR: Method searchForOldCN unable to find an entry that matches our criteria.");
                returnValue = "MATCH_NOT_FOUND";
            } // end of if
            else {
                SearchResult sr = (SearchResult)results.next();
                Attributes attrs = sr.getAttributes();
                Attribute attr = attrs.get("cn");
                // System.out.println(attr.get());
                returnValue = (String)attr.get();
                if (results.hasMore()) {
                    returnValue = "MORE_THAN_ONE";
                } // end of if
            } // end of else
        } // end of try
        catch (NamingException e) {
            System.err.println("ERROR : NamingException occurred in method searchForOldCN:");
            System.err.println(e);
        }
        if (debugToggle != 0)
            System.out.println("DEBUG: Method searchForOldCN is returning returnValue=" +
                               returnValue);
        return returnValue;
    } // end of searchForOldCN

    /* receives old CN, new CN and info to connect to LDAP as arguments */
    /**
     *Hunter Office 365	Modify cn in AD
Queens Office 365	cn Updated
     * @param employeeID
     * @param newName
     * @param directoryServer
     * @param rootContext
     * @param securityPrincipal
     * @param securityCredentials
     * @param debugToggle
     * @param sslToggle
     * @return
     */
    public static String changeName(String employeeID, String newName,
                                    String directoryServer, String rootContext,
                                    String securityPrincipal,
                                    String securityCredentials,
                                    int debugToggle, int sslToggle) {
        String returnValue = "ERROR";
        if (debugToggle != 0) {
            System.out.println("DEBUG: Received employeeID        = " +
                               employeeID);
            System.out.println("DEBUG: Received newName           = " +
                               newName);
            System.out.println("DEBUG: Received directoryServer   = " +
                               directoryServer);
            System.out.println("DEBUG: Received rootContext       = " +
                               rootContext);
            System.out.println("DEBUG: Received securityPrincipal = " +
                               securityPrincipal);
        }
        ;
        // initialize a hashtable to use as input
        // when we make the context(object to represent LDAP)
        Hashtable<String, Object> env = new Hashtable<String, Object>();
        env.put(Context.INITIAL_CONTEXT_FACTORY,
                "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.SECURITY_PRINCIPAL, securityPrincipal);
        env.put(Context.SECURITY_CREDENTIALS, securityCredentials);
        if (sslToggle != 0) {
            if (debugToggle != 0) {
                System.out.println("DEBUG: setting PROVIDER_URL to: " +
                                   "ldaps://" + directoryServer + "/" +
                                   rootContext);
            }
            ;
            env.put(Context.PROVIDER_URL,
                    "ldaps://" + directoryServer + "/" + rootContext);
            env.put(Context.SECURITY_AUTHENTICATION, "DIGEST-MD5");
        } else {
            if (debugToggle != 0) {
                System.out.println("DEBUG: setting PROVIDER_URL to: " +
                                   "ldap://" + directoryServer + "/" +
                                   rootContext);
            }
            ;
            env.put(Context.PROVIDER_URL,
                    "ldap://" + directoryServer + "/" + rootContext);
        }
        ;

        try {
            // create JNDI context for LDAP operations
            DirContext ctx = new InitialDirContext(env);

            // search for an entry where employeeID
            // matches what we were given
            if (debugToggle != 0)
                System.out.println("DEBUG: Calling searchForOldCN with " +
                                   employeeID);
            String searchResult = searchForOldCN(ctx, employeeID, debugToggle);
            // searchResult will be holding the OLD CN VALUE
            if (debugToggle != 0)
                System.out.println("DEBUG: searchResult=" + searchResult);
            // if the result has a true CN, not an error then we can attempt the setting of new name
            if (debugToggle != 0)
                System.out.println("DEBUG: !searchResult.equals(MATCH_NOT_FOUND) evaluates to " +
                                   (!searchResult.equals("MATCH_NOT_FOUND")));
            if (debugToggle != 0)
                System.out.println("DEBUG: !searchResult.equals(MORE_THAN_ONE) evaluates to " +
                                   (!searchResult.equals("MORE_THAN_ONE")));
            if ((!searchResult.equals("MATCH_NOT_FOUND")) &&
                (!searchResult.equals("MORE_THAN_ONE"))) {
                if (debugToggle != 0)
                    System.out.println("DEBUG: searchResult was equal to employeeID provided");
                if (debugToggle != 0)
                    System.out.println("DEBUG: Attempting rename object with employeeID=" +
                                       employeeID + " to " + newName);
                String preppedOldName = "cn=" + searchResult;
                String preppedNewName = "cn=" + newName;
                ctx.rename(preppedOldName, preppedNewName);
                returnValue = "SUCCESS";
            }
            ;
            ctx.close();
        } catch (AuthenticationException e) {
            System.err.println("ERROR: Authentication to LDAP target failed.");
            System.err.println(e);
        } catch (NamingException e) {
            System.err.println("ERROR: NamingException occurred:");
            System.err.println(e);
        }
        if (debugToggle != 0)
            System.out.println("DEBUG: changeName is returning " +
                               returnValue);
        return returnValue;
    } // end of changeName
} // end of testJNDI
