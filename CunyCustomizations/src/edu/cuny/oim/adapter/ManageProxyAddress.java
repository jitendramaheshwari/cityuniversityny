package edu.cuny.oim.adapter;

import com.thortech.util.logging.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import oracle.iam.identity.orgmgmt.api.OrganizationManager;
import oracle.iam.identity.orgmgmt.vo.Organization;
import oracle.iam.identity.usermgmt.api.UserManager;
import oracle.iam.identity.usermgmt.vo.User;
import oracle.iam.platform.Platform;
import oracle.iam.platform.utils.cache.Cache;
import oracle.iam.provisioning.api.ProvisioningService;
import oracle.iam.provisioning.vo.Account;
import oracle.iam.provisioning.vo.AccountData;
import oracle.iam.provisioning.vo.ChildTableRecord;
import oracle.iam.provisioning.vo.ChildTableRecord.ACTION;


public class ManageProxyAddress {
    private static String CLASS_NAME = "ManageProxyAddress";
    private static Logger log = Logger.getLogger("CUNY.CUSTOM");
    private static final String SMTP_DEFAULT_PREFIX = "SMTP:";
    private static final String SMTP_REGULAR_PREFIX = "smtp:";
    private static final String EMAIL_SIGN = "@";
    private static final String OLD_CAMPUS_LOOKUP = "CUNYOldCampusList";
    private static final String NEW_CAMPUS_LOOKUP = "CUNYNewCampusList";


    /**
     * Any change in name of user add data in child table.
     * It will also invoke setDefaultProxyAddress at the end. So that all the data is correctly set in child forms.
     * @param processInstanceKey
     * @param CHILD_PROCESS_FORM_NAME
     * @param CHILD_ATTRIBUTE_NAME
     * @param userLogin
     * @param emailDomain
     * @param userKey
     * @return
     * @throws Exception
     */
    public String addDataChildProcessFormOnLoginChange(long processInstanceKey,
                                                       String CHILD_PROCESS_FORM_NAME,
                                                       String CHILD_ATTRIBUTE_NAME,
                                                       String newUserLogin,
                                                       String oldUserLogin,
                                                       String userKey, String userPrincipalColumnName, String emailIDDomian) throws Exception {
        String METHOD_NAME = " addDataChildProcessFormOnLoginChange ";
        log.debug(CLASS_NAME + METHOD_NAME + " processInstanceKey:" +
                  processInstanceKey + " CHILD_PROCESS_FORM_NAME:" +
                  CHILD_PROCESS_FORM_NAME + " CHILD_ATTRIBUTE_NAME:" +
                  CHILD_ATTRIBUTE_NAME + " newUserLogin:" + newUserLogin +
                  " oldUserLogin:" + oldUserLogin);
        try {
            UserManager userManager = Platform.getService(UserManager.class);
            User us = userManager.getDetails(newUserLogin, null, true);
            userKey = us.getEntityId();
        } catch(Exception e){
            log.error(CLASS_NAME + METHOD_NAME + "EXCEPTION ", e);
        }
        String response = updateOnUserLoginChange(userKey, userPrincipalColumnName, emailIDDomian, processInstanceKey, newUserLogin);
        if(!response.equals("SUCCESS")) return response;
        
        response = "ERROR";                                                                                                                                 
        ProvisioningService provService =
            Platform.getService(ProvisioningService.class);
        List<Account> accounts = null;
        String accountId = null;
        try {
            accounts =
                    provService.getAccountsProvisionedToUser(userKey + "", true);
            for (Account acc : accounts) {
                String procInstInstanceKey = acc.getProcessInstanceKey();
                if (procInstInstanceKey.equals(processInstanceKey + "")) {
                    accountId = acc.getAccountID(); // OIU_KEY
                    AccountData accData = acc.getAccountData();
                    List<String> proxyAddressDomains =
                        returnProxyAddressDomain(accData.getChildData(),
                                                 oldUserLogin,
                                                 CHILD_PROCESS_FORM_NAME,
                                                 CHILD_ATTRIBUTE_NAME);

                    Map<String, ArrayList<ChildTableRecord>> childData =
                        new HashMap<String, ArrayList<ChildTableRecord>>();
                    ArrayList<ChildTableRecord> modRecords =
                        new ArrayList<ChildTableRecord>();


                    for (String domain : proxyAddressDomains) {
                        HashMap<String, Object> addRecordData =
                            new HashMap<String, Object>();
                        addRecordData.put(CHILD_ATTRIBUTE_NAME,
                                          SMTP_REGULAR_PREFIX + newUserLogin + EMAIL_SIGN +
                                          domain);
                        ChildTableRecord addRecord = new ChildTableRecord();
                        addRecord.setAction(ACTION.Add);
                        addRecord.setChildData(addRecordData);
                        modRecords.add(addRecord);
                    }

                    childData.put(CHILD_PROCESS_FORM_NAME,
                                  modRecords); // Put Child Form Name and its modified child data
                    log.debug(CLASS_NAME + METHOD_NAME + "childData:" +
                              childData);
                    accData.setChildData(childData);
                    acc.setAccountData(accData);
                    try {
                        provService.modify(acc);
                        response = "SUCCESS";
                    } catch (Exception p) {
                        log.error(CLASS_NAME + METHOD_NAME + "EXCEPTION ", p);
                    }
                }
            }
        } catch (Exception e) {
            response = "ERROR";
            log.error(CLASS_NAME + METHOD_NAME + "EXCEPTION ", e);
        }
        log.debug(CLASS_NAME + METHOD_NAME + "response:" + response);
        return response;
    }

    public String addDataChildProcessFormOnCampusChange(long processInstanceKey,
                                                        String CHILD_PROCESS_FORM_NAME,
                                                        String CHILD_ATTRIBUTE_NAME,
                                                        String userLogin,
                                                        String campusName,
                                                        String userKey) throws Exception {
        String METHOD_NAME = " addDataChildProcessFormOnCampusChange ";
        log.debug(CLASS_NAME + METHOD_NAME + " processInstanceKey:" +
                  processInstanceKey + " CHILD_PROCESS_FORM_NAME:" +
                  CHILD_PROCESS_FORM_NAME + " CHILD_ATTRIBUTE_NAME:" +
                  CHILD_ATTRIBUTE_NAME + " userLogin:" + userLogin +
                  " campusName:" + campusName);

        String response = "ERROR";
        String proxyDomainForCampus = findDomainForCampus(campusName);
        if (proxyDomainForCampus == null) {
            response = "SUCCESS";
            log.debug(CLASS_NAME + METHOD_NAME + " Domain name not found for : " + campusName);
            return response;
        }
        
        ProvisioningService provService =
            Platform.getService(ProvisioningService.class);
        List<Account> accounts = null;
        String accountId = null;
        try {
            accounts =
                    provService.getAccountsProvisionedToUser(userKey + "", true);
            for (Account acc : accounts) {
                String procInstInstanceKey = acc.getProcessInstanceKey();
                if (procInstInstanceKey.equals(processInstanceKey + "")) {
                    accountId = acc.getAccountID(); // OIU_KEY
                    AccountData accData = acc.getAccountData();
                    List<String> proxyAddressDomains =
                        returnProxyAddressDomain(accData.getChildData(),
                                                 userLogin,
                                                 CHILD_PROCESS_FORM_NAME,
                                                 CHILD_ATTRIBUTE_NAME);

                    Map<String, ArrayList<ChildTableRecord>> childData =
                        new HashMap<String, ArrayList<ChildTableRecord>>();
                    ArrayList<ChildTableRecord> modRecords =
                        new ArrayList<ChildTableRecord>();


                    if (proxyDomainForCampus != null &&
                        !proxyAddressDomains.contains(proxyDomainForCampus)) {
                        HashMap<String, Object> addRecordData =
                            new HashMap<String, Object>();
                        addRecordData.put(CHILD_ATTRIBUTE_NAME,
                                          SMTP_REGULAR_PREFIX + userLogin + EMAIL_SIGN +
                                          proxyDomainForCampus);
                        ChildTableRecord addRecord = new ChildTableRecord();
                        addRecord.setAction(ACTION.Add);
                        addRecord.setChildData(addRecordData);
                        modRecords.add(addRecord);

                        childData.put(CHILD_PROCESS_FORM_NAME,
                                      modRecords); // Put Child Form Name and its modified child data
                        log.debug(CLASS_NAME + METHOD_NAME + "childData:" +
                                  childData);
                        accData.setChildData(childData);
                        acc.setAccountData(accData);
                        try {
                            provService.modify(acc);
                            response = "SUCCESS";
                        } catch (Exception p) {
                            log.error(CLASS_NAME + METHOD_NAME + "EXCEPTION ",
                                      p);
                        }
                    } else if (proxyDomainForCampus != null &&
                        proxyAddressDomains.contains(proxyDomainForCampus)) {
                        // No need to add the campus-domain as it's lready added.
                        response = "SUCCESS";
                        log.debug(CLASS_NAME + METHOD_NAME + " The domain already exists in child form for proxyDomainForCampus:" +
                                      proxyDomainForCampus);

                        } else if (proxyDomainForCampus == null) {
                        // No need to add the campus-domain as it's lready added.
                        response = "SUCCESS";
                        log.debug(CLASS_NAME + METHOD_NAME + " The domain does not exists for the campus.");

                        }
                }
            }
        } catch (Exception e) {
            response = "ERROR";
            log.error(CLASS_NAME + METHOD_NAME + "EXCEPTION ", e);
        }
        log.debug(CLASS_NAME + METHOD_NAME + "response:" + response);
        return response;
    }

    public String setDefaultProxyAddress(long processInstanceKey,
                                         String CHILD_PROCESS_FORM_NAME,
                                         String CHILD_ATTRIBUTE_NAME,
                                         String userLogin,
                                         String userKey) throws Exception {
        String METHOD_NAME = " setDefaultProxyAddress ";
        log.debug(CLASS_NAME + METHOD_NAME + " processInstanceKey:" +
                  processInstanceKey + " CHILD_PROCESS_FORM_NAME:" +
                  CHILD_PROCESS_FORM_NAME + " CHILD_ATTRIBUTE_NAME:" +
                  CHILD_ATTRIBUTE_NAME + " userLogin:" + userLogin);

        String response = "ERROR";

        ProvisioningService provService =
            Platform.getService(ProvisioningService.class);
        List<Account> accounts = null;
        String accountId = null;

        try {
            Map<String, String> userMap = getUserStatus(userKey);
            String userGlobalStudentStatus =
                userMap.get("USR_UDF_GLBL_STU_STATUS");

            accounts =
                    provService.getAccountsProvisionedToUser(userKey + "", true);
            for (Account acc : accounts) {
                String procInstInstanceKey = acc.getProcessInstanceKey();
                if (procInstInstanceKey.equals(processInstanceKey + "")) {
                    accountId = acc.getAccountID(); // OIU_KEY
                    AccountData accData = acc.getAccountData();

                    List<String> proxyAddressDomains =
                        returnProxyAddressDomain(accData.getChildData(),
                                                 userLogin,
                                                 CHILD_PROCESS_FORM_NAME,
                                                 CHILD_ATTRIBUTE_NAME);

                    String defaultProxyAddress = null;
                    String defaultDomain = null;
                    if (userGlobalStudentStatus!=null && userGlobalStudentStatus.equalsIgnoreCase("Active")) {
                        boolean isUserActiveAtNewCampus = false;
                        for (CampusDomain campus : listOfCampusDomains(NEW_CAMPUS_LOOKUP)) {
                            String campSt =
                                userMap.get(campus.getCampusUSRUDFName());
                            if (campSt!=null && campSt.equalsIgnoreCase("AC"))
                                isUserActiveAtNewCampus = true;
                            if (defaultDomain == null &&
                                proxyAddressDomains.contains(campus.getDomainName())) {
                                defaultDomain = campus.getDomainName();
                            }
                        }
                        if (isUserActiveAtNewCampus) {
                            defaultProxyAddress =
                                    SMTP_DEFAULT_PREFIX + userLogin + EMAIL_SIGN + defaultDomain;
                            //Set default proxyAddress to email of first new O365 college when sorted alphabetically.
                        } else {
                            defaultDomain = null;
                            boolean isUserActiveAtOldCampus = false;
                            for (CampusDomain campus : listOfCampusDomains(OLD_CAMPUS_LOOKUP)) {
                                String campSt =
                                    userMap.get(campus.getCampusUSRUDFName());
                                if (campSt!=null && campSt.equalsIgnoreCase("AC"))
                                    isUserActiveAtOldCampus = true;
                                if (defaultDomain == null &&
                                    proxyAddressDomains.contains(campus.getDomainName())) {
                                    defaultDomain = campus.getDomainName();
                                }
                            }
                            if (isUserActiveAtOldCampus) {
                                //Set default proxyAddress to email of first old O365 college when
                                // sorted alphabetically.
                                defaultProxyAddress =
                                        SMTP_DEFAULT_PREFIX + userLogin + EMAIL_SIGN +
                                        defaultDomain;
                            } else {
                                defaultDomain = null;
                                for (CampusDomain campus : listOfCampusDomains(null)) {
                                    if (defaultDomain == null &&
                                        proxyAddressDomains.contains(campus.getDomainName())) {
                                        defaultDomain = campus.getDomainName();
                                    }
                                }
                                defaultProxyAddress =
                                        SMTP_DEFAULT_PREFIX + userLogin + EMAIL_SIGN +
                                        defaultDomain;
                                //Set default proxyAddress to first email when sorted alphabetically.*
                            }
                        }
                    } else {
                        defaultDomain = null;
                        for (CampusDomain campus : listOfCampusDomains(null)) {
                            if (defaultDomain == null &&
                                proxyAddressDomains.contains(campus.getDomainName())) {
                                defaultDomain = campus.getDomainName();
                            }
                        }
                        defaultProxyAddress =
                                SMTP_DEFAULT_PREFIX + userLogin + EMAIL_SIGN + defaultDomain;
                        //Set default proxyAddress to first email when sorted alphabetically.*
                    }

                    log.debug(CLASS_NAME + METHOD_NAME +
                              " Setting defaultProxyAddress: " +
                              defaultProxyAddress);
                    Map<String, ArrayList<ChildTableRecord>> updatedChildData = new HashMap<String, ArrayList<ChildTableRecord>>();
                    Map<String, ArrayList<ChildTableRecord>> childData = accData.getChildData();
                    
                    ArrayList<ChildTableRecord> updatedModRecords = new ArrayList<ChildTableRecord>();
                    ArrayList<ChildTableRecord> modRecords = childData.get(CHILD_PROCESS_FORM_NAME);

                    for (ChildTableRecord childRec : modRecords) {
                        String prevEntry =
                            (String)(childRec.getChildData()).get(CHILD_ATTRIBUTE_NAME);
                        if (prevEntry.equalsIgnoreCase(defaultProxyAddress)) {
                            childRec.getChildData().put(CHILD_ATTRIBUTE_NAME,
                                                        defaultProxyAddress);
                            childRec.setAction(ACTION.Modify);
                            updatedModRecords.add(childRec);
                        } else if(prevEntry.startsWith(SMTP_DEFAULT_PREFIX + userLogin)) {
                            prevEntry = prevEntry.replaceFirst(SMTP_DEFAULT_PREFIX, SMTP_REGULAR_PREFIX);
                            childRec.getChildData().put(CHILD_ATTRIBUTE_NAME,
                                                        prevEntry);
                            childRec.setAction(ACTION.Modify);
                            updatedModRecords.add(childRec);
                        }
                    }
                    updatedChildData.put(CHILD_PROCESS_FORM_NAME,
                                  updatedModRecords); // Put Child Form Name and its modified child data
                    log.debug(CLASS_NAME + METHOD_NAME + "updatedChildData:" +
                              updatedChildData);
                    accData.setChildData(updatedChildData);
                    acc.setAccountData(accData);
                    try {
                        provService.modify(acc);
                        response = "SUCCESS";
                    } catch (Exception p) {
                        log.error(CLASS_NAME + METHOD_NAME + "EXCEPTION ",
                                  p);
                    }
                }
            }
        } catch (Exception e) {
            response = "ERROR";
            log.error(CLASS_NAME + METHOD_NAME + "EXCEPTION ", e);
        }
        log.debug(CLASS_NAME + METHOD_NAME + "response:" + response);
        return response;
    }


    private String findDomainForCampus(String campusName) {
        String METHOD_NAME = " findDomainForCampus ";
        OrganizationManager orgManager =
            Platform.getService(OrganizationManager.class);
        HashSet<String> retAttrs = new HashSet<String>();
        retAttrs.add("Domain");
        String domainName = null;
        Organization org = null;
        try {
            org = orgManager.getDetails(campusName, retAttrs, true);
            if (org != null) {
                domainName = (String)org.getAttribute("Domain");
            }
        } catch (Exception e) {
            log.error(CLASS_NAME + METHOD_NAME + "EXCEPTION ", e);
        }
        log.debug(CLASS_NAME + METHOD_NAME + "Exiting with  domainName " +
                  domainName + " for campusName " + campusName);
        return domainName;
    }


    private List<CampusDomain> listOfCampusDomains(String lookupName) {
        String METHOD_NAME = " listOfCampusDomains ";
        log.debug(CLASS_NAME + METHOD_NAME + "Entering with  lookupName " +lookupName);

        Cache cache = Cache.getInstance();
        String cachKey = "CUNYCampusDomains";
        if(lookupName!=null){
            cachKey = cachKey + lookupName;
        }
        String cacheKeyTasks = Cache.ORG_NAMES + cachKey;
        List<CampusDomain> allCampuses = (List<CampusDomain>) cache.getFromCache(cacheKeyTasks);
        if (allCampuses == null || allCampuses.isEmpty()) {
            allCampuses = new ArrayList<CampusDomain>();

            Connection oimDBConnection = null;
            ResultSet queryResult = null;
            PreparedStatement queryStmt = null;
            String query = null;
            if(lookupName==null) query = "select org_udf_student, act_name, org_udf_domain from act where org_udf_domain is not null";
            else {
                query = "select distinct act.act_name, act.org_udf_student, act.org_udf_domain " +
                    "from lku, lkv, act where lku.lku_key = lkv.lku_key and lkv.lkv_encoded = act.act_name " +
                    "and lku.lku_type_string_key = ? and act.org_udf_domain is not null";
            }
            CampusDomain campusDomain = null;
            try {
                oimDBConnection = Platform.getOperationalDS().getConnection();
                queryStmt = oimDBConnection.prepareStatement(query);
                if(lookupName!=null) queryStmt.setString(1, lookupName);
                
                queryResult = queryStmt.executeQuery();
                if (queryResult != null) {
                    while (queryResult.next()) {
                        campusDomain = new CampusDomain(queryResult.getString("act_name"), queryResult.getString("org_udf_domain"), queryResult.getString("org_udf_student"));
                        allCampuses.add(campusDomain);
                    }
                }
            } catch (SQLException ex) {
                log.error(CLASS_NAME + METHOD_NAME + "Exception ", ex);
            } finally {
                try {
                    if (queryResult != null) {
                        queryResult.close();
                    }
                } catch (Exception e) {
                    log.error(CLASS_NAME + METHOD_NAME + "Exception ", e);
                }
                try {
                    if (queryStmt != null) {
                        queryStmt.close();
                    }
                } catch (Exception e) {
                    log.error(CLASS_NAME + METHOD_NAME + "Exception ", e);
                }
                try {
                    if (oimDBConnection != null) {
                        oimDBConnection.close();
                    }
                } catch (Exception e) {
                    log.error(CLASS_NAME + METHOD_NAME + "Exception ", e);
                }
            }
            Collections.sort(allCampuses);
            cache.putInCache(cacheKeyTasks, allCampuses, Cache.ORG_NAMES);
        }
        log.debug(CLASS_NAME + METHOD_NAME + "Exiting with  allCampuses " +allCampuses);
        return allCampuses;
    }


    private List<String> returnProxyAddressDomain(Map<String, ArrayList<ChildTableRecord>> childDate,
                                                  String userLogin,
                                                  String CHILD_PROCESS_FORM_NAME,
                                                  String CHILD_ATTRIBUTE_NAME) {
        String METHOD_NAME = " returnProxyAddressDomain ";
        log.debug(CLASS_NAME + METHOD_NAME + "Entering with  childDate " +
                  childDate + " userLogin " + userLogin);

        List<String> proxyAddressDomains = new ArrayList<String>();
        ArrayList<ChildTableRecord> chidlTableRecords =
            childDate.get(CHILD_PROCESS_FORM_NAME);
        for (ChildTableRecord childTRecord : chidlTableRecords) {
            String proxyAddress =
                (String)(childTRecord.getChildData()).get(CHILD_ATTRIBUTE_NAME);
            if (proxyAddress.indexOf(":" + userLogin + "@") != -1) {
                String proxyDomain =
                    proxyAddress.substring(proxyAddress.indexOf("@")+1,
                                           proxyAddress.length());
                proxyAddressDomains.add(proxyDomain);
            }
        }
        Collections.sort(proxyAddressDomains);
        log.debug(CLASS_NAME + METHOD_NAME +
                  "Exiting with  proxyAddressDomains " + proxyAddressDomains);
        return proxyAddressDomains;
    }


    private List<String> getStuUDFColumns() {
        String METHOD_NAME = " getStuUDFColumns ";

        Cache cache = Cache.getInstance();
        String cacheKeyTasks = Cache.ORG_NAMES + "CUNYOrgUDFList";
        List<String> columnNames = (List<String>) cache.getFromCache(cacheKeyTasks);
        if (columnNames == null || columnNames.isEmpty()) {
            Connection oimDBConnection = null;
            ResultSet queryResult = null;
            Statement queryStmt = null;
            String query = "select org_udf_student from act where org_udf_student is not null";
            columnNames = new ArrayList<String>();

            try {
                oimDBConnection = Platform.getOperationalDS().getConnection();
                queryStmt = oimDBConnection.createStatement();
                queryResult = queryStmt.executeQuery(query);
                if (queryResult != null) {
                    while (queryResult.next()) {
                        columnNames.add(queryResult.getString("org_udf_student"));
                    }
                }
            } catch (SQLException ex) {
                log.error(CLASS_NAME + METHOD_NAME + "Exception ", ex);
            } finally {
                try {
                    if (queryResult != null) {
                        queryResult.close();
                    }
                } catch (Exception e) {
                    log.error(CLASS_NAME + METHOD_NAME + "Exception ", e);
                }
                try {
                    if (queryStmt != null) {
                        queryStmt.close();
                    }
                } catch (Exception e) {
                    log.error(CLASS_NAME + METHOD_NAME + "Exception ", e);
                }
                try {
                    if (oimDBConnection != null) {
                        oimDBConnection.close();
                    }
                } catch (Exception e) {
                    log.error(CLASS_NAME + METHOD_NAME + "Exception ", e);
                }
            }
            cache.putInCache(cacheKeyTasks, columnNames, Cache.ORG_NAMES);
        }
        log.debug(CLASS_NAME + METHOD_NAME + "Exiting with  columnNames " +columnNames);

        return columnNames;
    }

    public String toCsv(List<String> list) {
        StringBuilder b = new StringBuilder();
        if (list != null) {
            for (int i = 0; i < list.size(); i++) {
                b.append(list.get(i));
                if (i != list.size() - 1) {
                    b.append(",");
                }
            }
        }
        return b.toString();
    }

    private String getUserSearchQuery() {
        String METHOD_NAME = " getUserSearchQuery ";

        Cache cache = Cache.getInstance();
        String cacheKeyTasks = Cache.ORG_NAMES + "CUNYProxyCampusSearchQuery";
        String query = (String) cache.getFromCache(cacheKeyTasks);
        if (query == null || query.isEmpty()) {
            List<String> studentUDFColumns = getStuUDFColumns();
            query = "select " + toCsv(studentUDFColumns) + ", USR_UDF_GLBL_STU_STATUS from usr where usr_key = ?";
            cache.putInCache(cacheKeyTasks, query, Cache.ORG_NAMES);
        }
        log.debug(CLASS_NAME + METHOD_NAME + "Exiting with with  query " +query);
        return query;
    }

    private Map<String, String> getUserStatus(String userKey) {
        String METHOD_NAME = " getUserStatus ";
        log.debug(CLASS_NAME + METHOD_NAME + "Entering with  userKey " +userKey);

        Connection oimDBConnection = null;
        ResultSet queryResult = null;
        PreparedStatement queryStmt = null;
        Map<String, String> map = new HashMap<String, String>();
        List<String> studentUDFColumns = getStuUDFColumns();
        try {
            oimDBConnection = Platform.getOperationalDS().getConnection();
            queryStmt = oimDBConnection.prepareStatement(getUserSearchQuery());
            queryStmt.setString(1, userKey);
            queryResult = queryStmt.executeQuery();

            if (queryResult != null) {
                if (queryResult.next()) {
                    map.put("USR_UDF_GLBL_STU_STATUS",
                            queryResult.getString("USR_UDF_GLBL_STU_STATUS"));
                    for (String udfStuName : studentUDFColumns) {
                        map.put(udfStuName, queryResult.getString(udfStuName));
                    }
                }
            }
        } catch (SQLException ex) {
            log.error(CLASS_NAME + METHOD_NAME + "Exception ", ex);
        } finally {
            try {
                if (queryResult != null) {
                    queryResult.close();
                }
            } catch (Exception e) {
                log.error(CLASS_NAME + METHOD_NAME + "Exception ", e);
            }
            try {
                if (queryStmt != null) {
                    queryStmt.close();
                }
            } catch (Exception e) {
                log.error(CLASS_NAME + METHOD_NAME + "Exception ", e);
            }
            try {
                if (oimDBConnection != null) {
                    oimDBConnection.close();
                }
            } catch (Exception e) {
                log.error(CLASS_NAME + METHOD_NAME + "Exception ", e);
            }
        }
        log.debug(CLASS_NAME + METHOD_NAME + "Exiting with  Map " +map);

        return map;
    }
    
    public String updateOnUserLoginChange(String userKey, String userPrincipalColumnName, String emailIDDomian, long processInstanceKey, String newUserLogin){
        log.debug("updateOnUserLoginChange() Parameter Variables passed are:" +
                  "userKey=[" + userKey + "]" + "processInstanceKey=[" +
                  processInstanceKey + "]" + "userPrincipalColumnName=[" +
                  userPrincipalColumnName + "]" + " emailIDDomian : " + emailIDDomian + " newData: " + newUserLogin);
        String response = "ERROR";
        ProvisioningService provService =
            Platform.getService(ProvisioningService.class);
        List<Account> accounts = null;
        String accountId = null;
        try {
            accounts =
                    provService.getAccountsProvisionedToUser(userKey + "", false);
            for (Account acc : accounts) {
                String procInstInstanceKey = acc.getProcessInstanceKey();
                if (procInstInstanceKey.equals(processInstanceKey + "")) {
                    accountId = acc.getAccountID(); // OIU_KEY
                    AccountData accData = acc.getAccountData();
                    
                    Map<java.lang.String, java.lang.Object> data = accData.getData();
                    data.put(userPrincipalColumnName, newUserLogin + "@" + emailIDDomian);
                    log.debug("updateOnUserLoginChange() data:" + data);
                    acc.setAccountData(accData);
                    try {
                        provService.modify(acc);
                        response = "SUCCESS";
                    } catch (Exception p) {
                        log.error(CLASS_NAME + "EXCEPTION ", p);
                    }
                }
            }
        } catch (Exception e) {
            response = "ERROR";
            log.error(CLASS_NAME + "EXCEPTION ", e);
        }
        log.debug(" updateOnUserLoginChange response:" + response);
        return response;        
    }




}
