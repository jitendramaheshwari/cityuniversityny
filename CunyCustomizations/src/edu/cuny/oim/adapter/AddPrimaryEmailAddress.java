package edu.cuny.oim.adapter;


import com.thortech.util.logging.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import oracle.iam.platform.Platform;
import oracle.iam.provisioning.api.ProvisioningService;
import oracle.iam.provisioning.vo.Account;
import oracle.iam.provisioning.vo.AccountData;
import oracle.iam.provisioning.vo.ChildTableRecord;
import oracle.iam.provisioning.vo.ChildTableRecord.ACTION;


public class AddPrimaryEmailAddress {
    private static String CLASS_NAME = "AddPrimaryEmailAddress";
    private static Logger log = Logger.getLogger("CUNY.CUSTOM");

    
    /**
     * On User Create in PeopleSoft, It will add user-email address as Primary address in PeopleSoft Child table.
     * @param processInstanceKey
     * @param CHILD_PROCESS_FORM_NAME
     * @param CHILD_ATTRIBUTE_NAME
     * @param emailAddress
     * @param userKey
     * @return
     * @throws Exception
     */
    public String addPrimaryEmailAddress(long processInstanceKey,
                                          String CHILD_PROCESS_FORM_NAME,
                                          String CHILD_ATTRIBUTE_NAME, String emailAddress,
                                          String userKey) throws Exception {
        String METHOD_NAME = " addPrimaryEmailAddress ";
        log.debug(CLASS_NAME + METHOD_NAME + " processInstanceKey:" + processInstanceKey
                  + " CHILD_PROCESS_FORM_NAME:" + CHILD_PROCESS_FORM_NAME
                  + " CHILD_ATTRIBUTE_NAME:" + CHILD_ATTRIBUTE_NAME
                  + " emailAddress:" + emailAddress);
        
        String response = "ERROR";

        ProvisioningService provService =
            Platform.getService(ProvisioningService.class);
        List<Account> accounts = null;
        String accountId = null;
        try {
            accounts =
                    provService.getAccountsProvisionedToUser(userKey + "", true);
            for (Account acc : accounts) {
                String procInstInstanceKey = acc.getProcessInstanceKey();
                if (procInstInstanceKey.equals(processInstanceKey + "")) {
                    accountId = acc.getAccountID(); // OIU_KEY
                    AccountData accData = acc.getAccountData();


                    Map<String, ArrayList<ChildTableRecord>> childData =
                        new HashMap<String, ArrayList<ChildTableRecord>>();
                    ArrayList<ChildTableRecord> modRecords =
                        new ArrayList<ChildTableRecord>();

                    // First child record
                    HashMap<String, Object> addRecordData =
                        new HashMap<String, Object>();
                    addRecordData.put(CHILD_ATTRIBUTE_NAME, emailAddress);
                    ChildTableRecord addRecord = new ChildTableRecord();
                    addRecord.setAction(ACTION.Add);
                    addRecord.setChildData(addRecordData);
                    modRecords.add(addRecord);

                    childData.put(CHILD_PROCESS_FORM_NAME,
                                  modRecords); // Put Child Form Name and its modified child data
                    log.debug(CLASS_NAME + METHOD_NAME + "childData:" + childData);

                    accData.setChildData(childData);
                    acc.setAccountData(accData);
                    try {
                        provService.modify(acc);
                        response = "SUCCESS";
                    } catch (Exception p) {
                        log.error(CLASS_NAME + METHOD_NAME + "EXCEPTION ", p);
                    }
                }
            }
        } catch (Exception e) {
            response = "ERROR";
            log.error(CLASS_NAME + METHOD_NAME + "EXCEPTION ", e);
        }
        log.debug("response:" + response);
        return response;
    }
}
