package edu.cuny.oim;


import Thor.API.Exceptions.tcAPIException;
import Thor.API.Exceptions.tcAwaitingApprovalDataCompletionException;
import Thor.API.Exceptions.tcAwaitingObjectDataCompletionException;
import Thor.API.Exceptions.tcColumnNotFoundException;
import Thor.API.Exceptions.tcLoginAttemptsExceededException;
import Thor.API.Exceptions.tcPasswordResetAttemptsExceededException;
import Thor.API.Exceptions.tcStaleDataUpdateException;
import Thor.API.Exceptions.tcTaskNotFoundException;
import Thor.API.Exceptions.tcUserAccountDisabledException;
import Thor.API.Exceptions.tcUserAccountInvalidException;
import Thor.API.Exceptions.tcUserAlreadyLoggedInException;
import Thor.API.Exceptions.tcUserNotFoundException;
import Thor.API.Operations.tcOrganizationOperationsIntf;
import Thor.API.Operations.tcProvisioningOperationsIntf;
import Thor.API.Operations.tcUserOperationsIntf;
import Thor.API.tcResultSet;

import com.thortech.util.logging.Logger;
import com.thortech.xl.crypto.tcCryptoException;
import com.thortech.xl.dataaccess.tcDataProvider;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.Vector;

import oracle.iam.platform.Platform;


public class CUSTOM {
	/**
	 * Logger
	 */
	
	private static Logger log = Logger.getLogger("CUNY.CUSTOM");
	
	
	/**
	 * Returns the system time in milliseconds/1000
	 * @return
	 */
	public String getLockOutTime(){
		SimpleDateFormat simpledateformat = new SimpleDateFormat("MM-dd-yyyy");		
		java.util.Date currentDate = new java.util.Date(System.currentTimeMillis());
		String date = simpledateformat.format(currentDate);
		
		String todaysDate = String.valueOf(Long.parseLong(simpledateformat.format(currentDate))/1000);
		log.debug("LockOutTime:" + todaysDate);
		return todaysDate;
	}
	
	/**
	 * Sets the value of the global status values
	 * @param userKey - Users.Key
	 * @param studentStatusField - User defined field for Student Global Status
	 * @param employeeStatusField - User defined field for Employee Global Status
	 * @param globalStatusField - User defined field for Global Status
	 * @param ioDatabase
	 * @return
	 * @throws tcAPIException
	 * @throws tcCryptoException
	 * @throws tcUserAccountDisabledException
	 * @throws tcPasswordResetAttemptsExceededException
	 * @throws tcLoginAttemptsExceededException
	 * @throws tcUserAccountInvalidException
	 * @throws tcUserAlreadyLoggedInException
	 */
	public boolean setGlobalStatus(long userKey, String studentStatusField, String employeeStatusField, String globalStatusField) throws tcAPIException, tcCryptoException, tcUserAccountDisabledException, tcPasswordResetAttemptsExceededException, tcLoginAttemptsExceededException, tcUserAccountInvalidException, tcUserAlreadyLoggedInException{
		log.debug("setGlobalStatus() Parameter Variables passed are:" + 
				"userKey=[" + userKey + "]" + 
				"studentStatusField=[" + studentStatusField + "]" +
				"employeeStatusField=[" + employeeStatusField + "]" +
				"globalStatusField=[" + globalStatusField + "]");	
		
		boolean response = false;
		String employeeStatus = "";
		String studentStatus = "";
		String globalStatus = "";
		tcUserOperationsIntf userIntf = Platform.getService(Thor.API.Operations.tcUserOperationsIntf.class);
		tcOrganizationOperationsIntf orgIntf = Platform.getService(Thor.API.Operations.tcOrganizationOperationsIntf.class);
		        Connection oimDBConnection = null;
        ResultSet queryResult = null;
        Statement queryStmt = null;

		try {
                                oimDBConnection = Platform.getOperationalDS().getConnection();

			//Get result set for user
			Hashtable userHash = new Hashtable();
			userHash.put("Users.Key", String.valueOf(userKey));
			tcResultSet userSet = userIntf.findAllUsers(userHash);
			userSet.goToRow(0);
			
			//Retrieve values from lookup for student global status
			Vector studentFields = new Vector();
			String query = "SELECT DISTINCT ACT_UDF_STU FROM ACT WHERE ACT_UDF_STU IS NOT NULL";
			log.debug("Query:" + query);
                        queryStmt = oimDBConnection.createStatement();
                        queryResult = queryStmt.executeQuery(query);
			while (queryResult.next()) {
				
				String field = queryResult.getString("ACT_UDF_STU");
				log.debug("Field:" + field);
				studentFields.add(field);	
			}	
			
			Vector employeeFields = new Vector();
			query = "SELECT DISTINCT ACT_UDF_EMP FROM ACT WHERE ACT_UDF_EMP IS NOT NULL";
			log.debug("Query:" + query);
		    try {
		        if (queryResult != null) {
		            queryResult.close();
		        }
		    } catch (Exception e) {
		        log.error("Exception ", e);
		    }
		    try {
		        if (queryStmt != null) {
		            queryStmt.close();
		        }
		    } catch (Exception e) {
		        log.error("Exception ", e);
		    }
                        queryStmt = oimDBConnection.createStatement();
                        queryResult = queryStmt.executeQuery(query);
			while (queryResult.next()) {
				String field = queryResult.getString("ACT_UDF_EMP");
				log.debug("Field:" + field);
				employeeFields.add(field);	
			}	
		    try {
		        if (queryResult != null) {
		            queryResult.close();
		        }
		    } catch (Exception e) {
		        log.error("Exception ", e);
		    }
		    try {
		        if (queryStmt != null) {
		            queryStmt.close();
		        }
		    } catch (Exception e) {
		        log.error("Exception ", e);
		    }	
			//Query user for student values
			query = "SELECT * FROM USR WHERE USR_KEY=" + userKey;
			log.debug("Query:" + query);
                        queryStmt = oimDBConnection.createStatement();
                        queryResult = queryStmt.executeQuery(query);			
                        
                        Hashtable studentStatusHash = new Hashtable();
			Hashtable employeeStatusHash = new Hashtable();
                        
			if(queryResult.next()){
			//Get all student status values
			log.debug("Getting status values for students");
			for (int i=0;i<studentFields.size();i++){
				String value = queryResult.getString(studentFields.get(i).toString());
				log.debug("Value:" + studentFields.get(i) + "|" + value);
				studentStatusHash.put(i, queryResult.getString(studentFields.get(i).toString()));
			}
			
			//Get all employee status values
			log.debug("Getting status values for employees");
			for (int i=0;i<employeeFields.size();i++){
				String value = queryResult.getString(employeeFields.get(i).toString());
				log.debug("Value:" + employeeFields.get(i) + "|" + value);
				employeeStatusHash.put(i, queryResult.getString(employeeFields.get(i).toString()));
			}
                        }
			Hashtable orgHash = new Hashtable();
			String orgKey = userSet.getStringValue("Organizations.Key");
			log.debug("User Organization:" + orgKey);
			orgHash.put("Organizations.Key", orgKey);
			tcResultSet orgSet = orgIntf.findOrganizations(orgHash);		
			orgSet.goToRow(0);
			log.debug("Organization Name:" + orgSet.getStringValue("Organizations.Organization Name"));
			String employeeField = orgSet.getStringValue("ACT_UDF_EMP");
			String studentField = orgSet.getStringValue("ACT_UDF_STU");
			log.debug("Employee Field:" + employeeField);
			log.debug("Student Field:" + studentField);
			
			//Find status for employees
			log.debug("If statement for employees");
			if (employeeStatusHash.containsValue("A") ||
					employeeStatusHash.containsValue("L") ||
					employeeStatusHash.containsValue("P") ||
					employeeStatusHash.containsValue("S") ||
					employeeStatusHash.containsValue("W")){
				employeeStatus = "Active";
			}else{
				//employeeStatus = userSet.getStringValue(employeeField);
				if (employeeStatusHash.containsValue("R") ||
						employeeStatusHash.containsValue("Q")){
					employeeStatus = "Retired";
				}else if (employeeStatusHash.containsValue("T") ||
						employeeStatusHash.containsValue("D") ||
						employeeStatusHash.containsValue("U") ||
						employeeStatusHash.containsValue("V") ||
						employeeStatusHash.containsValue("X")){
					employeeStatus = "Inactive";
				}
			}
			
				
			log.debug("employeeStatus:" + employeeStatus);
			
			//Find status for Students
			log.debug("If statement for students");
			if (studentStatusHash.containsValue("AC") || 
					studentStatusHash.containsValue("AD") ||
					studentStatusHash.containsValue("AP") ||
					studentStatusHash.containsValue("CM") ||
					studentStatusHash.containsValue("CN") ||
					studentStatusHash.containsValue("DC") ||
					studentStatusHash.containsValue("DM") ||
					studentStatusHash.containsValue("LA") ||
					studentStatusHash.containsValue("PM") ||
					studentStatusHash.containsValue("SP") ||
					studentStatusHash.containsValue("WT")){
				studentStatus = "Active";
			}else if (studentStatusHash.containsValue("DE")){
				studentStatus= "Inactive";
			}
			
			log.debug("studentStatus:" + studentStatus);
			
			if (studentStatus.length()!=0){
				globalStatus = studentStatus;
			}else{
				globalStatus = employeeStatus;
			}

			userHash.clear();
			userHash.put(studentStatusField, studentStatus);
			userHash.put(employeeStatusField, employeeStatus);
			userHash.put(globalStatusField, globalStatus);
			userIntf.updateUser(userSet, userHash);
			response = true;
			
			log.debug("User Global Status Values Updated: studentStatus[" + studentStatus + "]" +
					"employeeStatus[" + employeeStatus + "]globalStatus[" + globalStatus + "]");
			
		} catch (Exception e) {
            log.error("Exception ", e);
        } finally {
            try {
                if (queryResult != null) {
                    queryResult.close();
                }
            } catch (Exception e) {
                log.error("Exception ", e);
            }
            try {
                if (queryStmt != null) {
                    queryStmt.close();
                }
            } catch (Exception e) {
                log.error("Exception ", e);
            }
            try {
                if (oimDBConnection != null) {
                    oimDBConnection.close();
                }
            } catch (Exception e) {
                log.error("Exception ", e);
            }
        }

		
		
		return response;
	}

	/**
	 * Updates the task notes when a role does not provision correctly
	 * @param taskInstanceKey - Task Information - Task Instance Key (String)
	 * @param roleName - Role Name
	 * @param action - AddRole or DeleteRole
	 * @param ioDatabase
	 * @throws tcAPIException
	 * @throws tcCryptoException
	 * @throws tcUserAccountDisabledException
	 * @throws tcPasswordResetAttemptsExceededException
	 * @throws tcLoginAttemptsExceededException
	 * @throws tcUserAccountInvalidException
	 * @throws tcUserAlreadyLoggedInException
	 */
	public void updateTaskNotes(String taskInstanceKey, String roleName, String action, tcDataProvider ioDatabase) throws tcAPIException, tcCryptoException, tcUserAccountDisabledException, tcPasswordResetAttemptsExceededException, tcLoginAttemptsExceededException, tcUserAccountInvalidException, tcUserAlreadyLoggedInException{
		log.debug("updateTaskNotes() Parameter Variables passed are:" + 
				"taskInstanceKey=[" + taskInstanceKey + "]" + 
				"roleName=[" + roleName + "]" + 
				"action=[" + action + "]");
		tcProvisioningOperationsIntf provIntf = Platform.getService(Thor.API.Operations.tcProvisioningOperationsIntf.class);

		long key = Long.parseLong(taskInstanceKey);
		Hashtable taskHash = new Hashtable();
		taskHash.put("Process Instance.Task Details.Note", "Failed to " + action + " " + roleName);
		try {
			provIntf.updateTask(key, taskHash);
		} catch (tcTaskNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (tcAwaitingObjectDataCompletionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (tcAwaitingApprovalDataCompletionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	/**
	 * When a user's Global Status becomes "Inactive", the Terminated Date is set to 60
	 * days in the future.
	 * @param userKey - Users.Key
	 * @param termDateField - USR_UDF_TERM_DATE
	 * @return
	 * @throws tcCryptoException
	 * @throws tcUserAccountDisabledException
	 * @throws tcPasswordResetAttemptsExceededException
	 * @throws tcLoginAttemptsExceededException
	 * @throws tcUserAccountInvalidException
	 * @throws tcUserAlreadyLoggedInException
	 * @throws tcAPIException
	 */
	public String setTerminatedDate(long userKey, String termDateField) throws tcCryptoException, tcUserAccountDisabledException, tcPasswordResetAttemptsExceededException, tcLoginAttemptsExceededException, tcUserAccountInvalidException, tcUserAlreadyLoggedInException, tcAPIException{
		log.debug("setTerminatedDate() Parameter Variables passed are:" + 
				"userKey=[" + userKey + "]" + 
				"termDateField=[" + termDateField + "]");
    	        tcUserOperationsIntf userIntf =Platform.getService(Thor.API.Operations.tcUserOperationsIntf.class);
		String response = "";
		
		SimpleDateFormat simpledateformat = new SimpleDateFormat("yyyy-MM-dd");		
		java.util.Date currentDate = new java.util.Date(System.currentTimeMillis());
		log.debug("Todays Date:" + simpledateformat.format(currentDate));	
		
		Calendar cal = new GregorianCalendar();
		cal.setTime(currentDate);
		cal.add(cal.DAY_OF_YEAR, 60);
		java.util.Date newDate = new java.util.Date(cal.getTimeInMillis());
		String sixtyDays = simpledateformat.format(newDate) + " 00:00:00.000000000";
		log.debug("60 Days Later:" + sixtyDays);
		
		Hashtable userHash = new Hashtable();
		userHash.put("Users.Key", String.valueOf(userKey));
		tcResultSet userSet = userIntf.findAllUsers(userHash);
		userHash.clear();		
		userHash.put(termDateField, sixtyDays);
		try {
			userIntf.updateUser(userSet, userHash);
			response = "USER_UPDATED";
		} catch (tcUserNotFoundException e) {
			// TODO Auto-generated catch block
			response = "ERROR";
			e.printStackTrace();
		} catch (tcStaleDataUpdateException e) {
			// TODO Auto-generated catch block
			response = "ERROR";
			e.printStackTrace();
		}
		
		return response;
	}

	/**
	 * Updates a value on a user's profile
	 * @param userKey - Users.Key
	 * @param value - Value
	 * @param userDefinedField - User field
	 * @return
	 * @throws tcCryptoException
	 * @throws tcUserAccountDisabledException
	 * @throws tcPasswordResetAttemptsExceededException
	 * @throws tcLoginAttemptsExceededException
	 * @throws tcUserAccountInvalidException
	 * @throws tcUserAlreadyLoggedInException
	 * @throws tcAPIException
	 */
	public String updateUser(long userKey, String value, String userDefinedField) throws tcCryptoException, tcUserAccountDisabledException, tcPasswordResetAttemptsExceededException, tcLoginAttemptsExceededException, tcUserAccountInvalidException, tcUserAlreadyLoggedInException, tcAPIException{
		log.debug("updateUser() Parameter Variables passed are:" + 
				"userKey=[" + userKey + "]" +
				"value=[" + value + "]" + 
				"userDefinedField=[" + userDefinedField + "]");
		
                tcUserOperationsIntf userIntf =Platform.getService(Thor.API.Operations.tcUserOperationsIntf.class);
                String response = "";
		
		Hashtable userHash = new Hashtable();
		userHash.put("Users.Key", String.valueOf(userKey));
		tcResultSet userSet = userIntf.findAllUsers(userHash);
		userHash.clear();		
		userHash.put(userDefinedField, value.trim());
		try {
			userIntf.updateUser(userSet, userHash);
			response = "USER_UPDATED";
		} catch (tcUserNotFoundException e) {
			// TODO Auto-generated catch block
			response = "ERROR";
			e.printStackTrace();
		} catch (tcStaleDataUpdateException e) {
			// TODO Auto-generated catch block
			response = "ERROR";
			e.printStackTrace();
		}
		
		return response;
	}


	/**
	 * Returns the correct Employee Global Status
	 * @param employeeStatusValues - a Vector containing string arrays for each employee affiliation
	 * @return
	 */
	public String setEmployeeGlobalStatus(Vector employeeStatusValues){
		log.debug("setGlobalStatus() Parameter Variables passed are:");

		String employeeStatus = "";
		
		Hashtable employeeStatusHash = new Hashtable();
		for (int i=0;i<employeeStatusValues.size();i++){
			String[] arryStr = (String[]) employeeStatusValues.get(i);
			if (arryStr[1].toString().trim().length()!=0){
				log.debug("Status Field=" + arryStr[0] + " Value=" + arryStr[1]);
				employeeStatusHash.put(arryStr[0], arryStr[1]);			
			}
		}
		
		//Find status for employees
		log.debug("If statement for employees");
		if (employeeStatusHash.containsValue("A") ||
				employeeStatusHash.containsValue("L") ||
				employeeStatusHash.containsValue("P") ||
				employeeStatusHash.containsValue("S") ||
				employeeStatusHash.containsValue("W")){
			employeeStatus = "Active";
		}else{
			//employeeStatus = userSet.getStringValue(employeeField);
			if (employeeStatusHash.containsValue("R") ||
					employeeStatusHash.containsValue("Q")){
				employeeStatus = "Retired";
			}else if (employeeStatusHash.containsValue("T") ||
					employeeStatusHash.containsValue("D") ||
					employeeStatusHash.containsValue("U") ||
					employeeStatusHash.containsValue("V") ||
					employeeStatusHash.containsValue("X")){
				employeeStatus = "Inactive";
			}
		}
			
		log.debug("employeeStatus:" + employeeStatus);			
		return employeeStatus;
	}

	/**
	 * Returns the correct Student Global Status
	 * @param studentStatusValues - a Vector containing string arrays for each student affiliation
	 * @return
	 */
	public String setStudentGlobalStatus(Vector studentStatusValues) {
		log.debug("setGlobalStatus() Parameter Variables passed are:");		
		String studentStatus = "";
		
		Hashtable studentStatusHash = new Hashtable();
		for (int i=0;i<studentStatusValues.size();i++){
			String[] temp = (String[]) studentStatusValues.get(i);
			String[] arryStr = (String[]) studentStatusValues.get(i);
			if (arryStr[1].toString().trim().length()!=0){
				log.debug("Status Field=" + arryStr[0] + " Value=" + arryStr[1]);
				studentStatusHash.put(arryStr[0], arryStr[1]);			
			}
		}	
		
		if (studentStatusHash.containsValue("AC") || 
				studentStatusHash.containsValue("AD") ||
				studentStatusHash.containsValue("AP") ||
				studentStatusHash.containsValue("CM") ||
				studentStatusHash.containsValue("CN") ||
				studentStatusHash.containsValue("DC") ||
				studentStatusHash.containsValue("DM") ||
				studentStatusHash.containsValue("LA") ||
				studentStatusHash.containsValue("PM") ||
				studentStatusHash.containsValue("SP") ||
				studentStatusHash.containsValue("WT")){
			studentStatus = "Active";
		}else if (studentStatusHash.containsValue("DE")){
			studentStatus= "Inactive";
		}
			
		log.debug("studentStatus:" + studentStatus);			
		return studentStatus;
	}

	/**
	 * Returns the global status based on employee and student global status
	 * @param studentStatus - User Definition - Student Global Status
	 * @param employeeStatus - User Definition - Employee Global Status
	 * @return
	 */
	public String setGlobalStatus(String studentStatus, String employeeStatus){
		log.debug("setGlobalStatus() Parameter Variables passed are:" + 				
				"studentStatus=[" + studentStatus + "]" +
				"employeeStatus=[" + employeeStatus + "]");			
		
		String globalStatus = "";		
			
		if (studentStatus.length()!=0){
			globalStatus = studentStatus;
		}else{
			globalStatus = employeeStatus;
		}
	
		log.debug("globalStatus:" + globalStatus);	
		return globalStatus;
	}
	
	
	
	/**
	 * For an input long, returns the string value
	 * @param key - long value
	 * @return
	 */
	public String toString(long key){
		return String.valueOf(key);
	}
}



