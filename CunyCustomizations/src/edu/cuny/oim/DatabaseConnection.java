package edu.cuny.oim;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Hashtable;
import java.util.Properties;

import com.thortech.util.logging.Logger;

/**
 * NOT USED - future rogue account code
 * @author kpinsky
 *
 */
public class DatabaseConnection {
	private static Logger log = Logger.getLogger("CUNY.CUSTOM");
	private static final String CLASS_NAME = "DatabaseConnection()";
	Object operidHash;
	Connection con;	
	
	/**
	 * NOT USED - future rogue account code
	 * @param hostname
	 * @param port
	 * @param driver
	 * @param sid
	 * @param admin
	 * @param password
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public DatabaseConnection(String hostname, String port, String driver, String sid, String admin, String password) throws ClassNotFoundException, SQLException{
		log.info(CLASS_NAME + " -----> inputs=hostname[" + hostname + 
				"]port[" + port +
				"]driver[" + driver + 
				"]sid[" + sid +
				"]admin[" + admin + 
				"]password[********]");
		
		Properties connectionProps=new Properties();
	    connectionProps.setProperty("user", admin);
	    connectionProps.setProperty("password", password);	 
	    String url = "jdbc:oracle:thin:@(DESCRIPTION=(ADDRESS=(PROTOCOL=tcp)(HOST=" + hostname + 
	    	")(PORT=" + port + 
	    	"))(CONNECT_DATA=(SID=" + sid + ")))";
	    Class.forName(driver);
		con = null;	
	    con = DriverManager.getConnection(url, connectionProps);
	}
	
	/**
	 * NOT USED - future rogue account code
	 */
	public DatabaseConnection(){
		
	}
	
	/**
	 * NOT USED - future rogue account code
	 * @param hostname
	 * @param port
	 * @param driver
	 * @param sid
	 * @param admin
	 * @param password
	 * @return
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	public Object findAccounts(String hostname, String port, String driver, String sid, String admin, String password) throws SQLException, ClassNotFoundException{
		log.info(CLASS_NAME + ".findAccounts() ----->");		
		log.info(CLASS_NAME + " -----> inputs=hostname[" + hostname + 
				"]port[" + port +
				"]driver[" + driver + 
				"]sid[" + sid +
				"]admin[" + admin + 
				"]password[********]");
		
		operidHash = new Hashtable();
		Properties connectionProps=new Properties();
	    connectionProps.setProperty("user", admin);
	    connectionProps.setProperty("password", password);	 
	    String url = "jdbc:oracle:thin:@(DESCRIPTION=(ADDRESS=(PROTOCOL=tcp)(HOST=" + hostname + 
	    	")(PORT=" + port + 
	    	"))(CONNECT_DATA=(SID=" + sid + ")))";
	    Class.forName(driver);
		con = null;	
	    con = DriverManager.getConnection(url, connectionProps);
	    
    	if (con != null){
			String query = "SELECT OPRID, ACCTLOCK FROM PSOPRDEFN";
	    	log.debug("Running Query: "+ query);	
	    	
	    	Statement stmt;
			try {
				stmt = con.createStatement();
		    	ResultSet rs = stmt.executeQuery(query);
		    			    	
		    	while(rs.next()){
		    		//log.debug("OPRID[" + rs.getString("OPRID") + "]ACCTLOCK[" + rs.getString("ACCTLOCK") + "]");
		    		((Hashtable)operidHash).put(rs.getString("OPRID"), rs.getString("ACCTLOCK"));
		    	} 		    	
		    	
		    	rs.close();
		    	stmt.close();
		    	
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
	    	disconnect();
	    	log.debug("Users Found[" + ((Hashtable)operidHash).size() + "]");
    	}
    	
    	return operidHash;
	}
	
	/**
	 * NOT USED - future rogue account code
	 * @throws SQLException
	 */
	private void disconnect() throws SQLException{
		con.close();
	}
	

	
	/**
	 * @param args
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 */
	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		
		//DatabaseConnection psft = new DatabaseConnection("10.10.10.10", "1521", "oracle.jdbc.driver.OracleDriver", "PSFTDB", "sysadm", "oracle");
		//log.debug(psft.findAccounts());
	}

}
