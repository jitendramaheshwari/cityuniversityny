package edu.cuny.oim.eventhandler;


import com.thortech.util.logging.Logger;

import edu.cuny.oim.util.APIUtil;

import java.io.Serializable;

import java.util.HashMap;

import oracle.iam.identity.rolemgmt.api.RoleManager;
import oracle.iam.identity.vo.Identity;
import oracle.iam.platform.Platform;
import oracle.iam.platform.kernel.spi.PostProcessHandler;
import oracle.iam.platform.kernel.vo.AbstractGenericOrchestration;
import oracle.iam.platform.kernel.vo.BulkEventResult;
import oracle.iam.platform.kernel.vo.BulkOrchestration;
import oracle.iam.platform.kernel.vo.EventResult;
import oracle.iam.platform.kernel.vo.Orchestration;


public class CheckForRemovingMembership implements PostProcessHandler {
    Logger logger = Logger.getLogger("CUNY.CUSTOM");
    private String CLASS_NAME = " CheckForRemovingMembership ";


    public EventResult execute(long l, long l1, Orchestration orchestration) {
        String METHOD_NAME = " Execute";
        logger.info(CLASS_NAME + METHOD_NAME + " Executing Execute Method");
        String orchOperation = orchestration.getOperation();
        String userKey = orchestration.getTarget().getEntityId();
        logger.info(CLASS_NAME + METHOD_NAME + "  User Key  " + userKey);
        HashMap parameterHashMap = orchestration.getParameters();


        try {
            HashMap<String, Serializable> interParameters =
                orchestration.getInterEventData();
            Object curUserObject = interParameters.get("CURRENT_USER");
            Identity newUsersState = (Identity)curUserObject;
            executeEvent(parameterHashMap, userKey,
                         orchestration.getTarget().getType(), newUsersState,
                         orchOperation);
        } catch (Exception e) {
            logger.error(CLASS_NAME + METHOD_NAME + " Exception ", e);
        }
        logger.info(CLASS_NAME + METHOD_NAME + " Exiting Execute Method");


        return new EventResult();
    }


    public BulkEventResult execute(long l, long l1,
                                   BulkOrchestration bulkOrchestration) {

        String METHOD_NAME = ".BulkExecute.";
        logger.info(CLASS_NAME + METHOD_NAME +
                    " Executing BulkExecute Method");
        String orchOperation = bulkOrchestration.getOperation();
        String[] userKeys = bulkOrchestration.getTarget().getAllEntityId();

        HashMap[] parameterHashMap = bulkOrchestration.getBulkParameters();
        HashMap<String, Serializable> interParameters =
            bulkOrchestration.getInterEventData();
        Object curUserObject = interParameters.get("CURRENT_USER");
        Identity[] newUsersState = (Identity[])curUserObject;


        for (int i = 0; i < parameterHashMap.length; i++) {
            logger.info(CLASS_NAME + METHOD_NAME + " UserKey " + userKeys[i]);
            try {
                executeEvent(parameterHashMap[i], userKeys[i],
                             bulkOrchestration.getTarget().getType(),
                             newUsersState[i], orchOperation);
            } catch (Exception e) {
                logger.error(CLASS_NAME + METHOD_NAME + "Execute Exception ", e);
            }
        }
        logger.info(CLASS_NAME + METHOD_NAME + " Exiting BulkExecute Method");


        return new BulkEventResult();
    }

    public void executeEvent(HashMap parameters, String userKey,
                             String targetType, Identity currentUser,
                             String orchOperation) {
        String METHOD_NAME = ".executeEvent.";
        String globalEmpStatus = null;
        logger.info(CLASS_NAME + METHOD_NAME + " Entering with parameters " + parameters);
        HashMap<java.lang.String, java.lang.Object> currentUserMap = null;
        try {
            // For any chnages to user re-calculate the user-status
            if (APIUtil.checkIfParameterChanged("GLBL_EMP_STATUS", parameters, currentUser)) {
                globalEmpStatus = APIUtil.getParameterValue(parameters, "GLBL_EMP_STATUS");
                if(globalEmpStatus!=null && globalEmpStatus.equalsIgnoreCase("Inactive")) {
                    removeGroupMembershipFromUser(userKey, "Peoplesoft CRM");
                }
            }
        } catch (Exception e) {
            logger.error(CLASS_NAME + METHOD_NAME + "Execute Exception ", e);
        }

    }

    public boolean cancel(long l, long l1,
                          AbstractGenericOrchestration abstractGenericOrchestration) {
        return false;
    }

    public void initialize(HashMap<String, String> hashMap) {
    }

    public void compensate(long l, long l1,
                           AbstractGenericOrchestration abstractGenericOrchestration) {
    }


    private void removeGroupMembershipFromUser(String userKey, String roleName) {
        String METHOD_NAME = " removeGroupMembershipFromUser ";
        logger.info(CLASS_NAME + METHOD_NAME + " Entering with userKey " + userKey + ", roleName: "+ roleName);
        RoleManager roleMgr =
            (RoleManager)Platform.getService(RoleManager.class);
        try {
            roleMgr.revokeRoleGrant("Role Unique Name", roleName, "usr_key", userKey);
            logger.info(CLASS_NAME + METHOD_NAME + " Revoke complete");            
        } catch (Exception e) {
            logger.error(CLASS_NAME + METHOD_NAME + "Error in role revoke. ", e);
        }
    }
}
