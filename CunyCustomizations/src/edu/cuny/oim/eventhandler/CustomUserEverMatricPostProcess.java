package edu.cuny.oim.eventhandler;


import com.thortech.util.logging.Logger;

import edu.cuny.oim.util.APIUtil;

import java.io.Serializable;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import oracle.iam.identity.usermgmt.api.UserManager;
import oracle.iam.identity.usermgmt.api.UserManagerConstants;
import oracle.iam.identity.usermgmt.vo.User;
import oracle.iam.identity.vo.Identity;
import oracle.iam.platform.Platform;
import oracle.iam.platform.kernel.spi.PostProcessHandler;
import oracle.iam.platform.kernel.vo.AbstractGenericOrchestration;
import oracle.iam.platform.kernel.vo.BulkEventResult;
import oracle.iam.platform.kernel.vo.BulkOrchestration;
import oracle.iam.platform.kernel.vo.EventResult;
import oracle.iam.platform.kernel.vo.Orchestration;


public class CustomUserEverMatricPostProcess implements PostProcessHandler {
    Logger logger = Logger.getLogger("CUNY.CUSTOM");
    private String CLASS_NAME = " CustomUserEverMatricPostProcess ";
    private static Pattern p =
        Pattern.compile("(^Z{3,}[^Z]+)|([^Z]+Z{3,}$)|(^H{3,}[^H]+)|([^H]+H{3,}$)",
                        Pattern.CASE_INSENSITIVE);

    public EventResult execute(long l, long l1, Orchestration orchestration) {
        String METHOD_NAME = " Execute";
        logger.info(CLASS_NAME + METHOD_NAME + " Executing Execute Method");
        String orchOperation = orchestration.getOperation();
        String userKey = orchestration.getTarget().getEntityId();
        logger.info(CLASS_NAME + METHOD_NAME + "  User Key  " + userKey);
        HashMap parameterHashMap = orchestration.getParameters();


        try {
            HashMap<String, Serializable> interParameters =
                orchestration.getInterEventData();
            Object curUserObject = interParameters.get("CURRENT_USER");
            Identity newUsersState = (Identity)curUserObject;
            executeEvent(parameterHashMap, userKey,
                         orchestration.getTarget().getType(), newUsersState,
                         orchOperation);
        } catch (Exception e) {
            logger.error(CLASS_NAME + METHOD_NAME + " Exception ", e);
        }
        logger.info(CLASS_NAME + METHOD_NAME + " Exiting Execute Method");


        return new EventResult();
    }


    public BulkEventResult execute(long l, long l1,
                                   BulkOrchestration bulkOrchestration) {

        String METHOD_NAME = ".BulkExecute.";
        logger.info(CLASS_NAME + METHOD_NAME +
                    " Executing BulkExecute Method");
        String orchOperation = bulkOrchestration.getOperation();
        String[] userKeys = bulkOrchestration.getTarget().getAllEntityId();

        HashMap[] parameterHashMap = bulkOrchestration.getBulkParameters();
        HashMap<String, Serializable> interParameters =
            bulkOrchestration.getInterEventData();
        Object curUserObject = interParameters.get("CURRENT_USER");
        Identity[] newUsersState = (Identity[])curUserObject;


        for (int i = 0; i < parameterHashMap.length; i++) {
            logger.info(CLASS_NAME + METHOD_NAME + " UserKey " + userKeys[i]);
            try {
                executeEvent(parameterHashMap[i], userKeys[i],
                             bulkOrchestration.getTarget().getType(),
                             newUsersState[i], orchOperation);
            } catch (Exception e) {
                logger.error(CLASS_NAME + METHOD_NAME + "Execute Exception ",
                             e);
            }
        }
        logger.info(CLASS_NAME + METHOD_NAME + " Exiting BulkExecute Method");


        return new BulkEventResult();
    }

    public void executeEvent(HashMap parameters, String userKey,
                             String targetType, Identity currentUser,
                             String orchOperation) {
        String METHOD_NAME = ".executeEvent.";
        logger.info(CLASS_NAME + METHOD_NAME + " Entering with parameters " +
                    parameters);
        HashMap<java.lang.String, java.lang.Object> currentUserMap = null;
        try {
            setDuplicateUserStatus(userKey, parameters);

            // For any chnages to user re-calculate the user-status
            if (APIUtil.checkIfStatusChanges(parameters, currentUser)) {
                if (currentUser != null)
                    currentUserMap = currentUser.getAttributes();
                setEverMatriculated(userKey, parameters, currentUser);
            }
        } catch (Exception e) {
            logger.error(CLASS_NAME + METHOD_NAME + "Execute Exception ", e);
        }

    }

    public boolean cancel(long l, long l1,
                          AbstractGenericOrchestration abstractGenericOrchestration) {
        return false;
    }

    public void initialize(HashMap<String, String> hashMap) {
    }

    public void compensate(long l, long l1,
                           AbstractGenericOrchestration abstractGenericOrchestration) {
    }

    private boolean isUserDuplicate(String firstName, String lastName) {
        if (firstName == null)
            firstName = "";
        if (lastName == null)
            lastName = "";

        Matcher mfirst = p.matcher(firstName);
        Matcher mlast = p.matcher(lastName);

        return (mfirst.matches() || mlast.matches());

    }


    private void setDuplicateUserStatus(String userKey,
                                        HashMap changedParameters) {
        String METHOD_NAME = " setDuplicateUserStatus ";
        boolean duplicateUser = false;
        String firstName = null;
        String lastName = null;
        if (changedParameters.containsKey(UserManagerConstants.AttributeName.FIRSTNAME.getId()) ||
            changedParameters.containsKey(UserManagerConstants.AttributeName.LASTNAME.getId())) {
            logger.debug(CLASS_NAME + METHOD_NAME +
                         "setDuplicateUserStatus() Parameter Variables passed are:" +
                         "userKey=[" + userKey + "] changedParameters " + changedParameters);

            if (changedParameters.containsKey(UserManagerConstants.AttributeName.FIRSTNAME.getId())) {
                firstName =
                        (String)changedParameters.get(UserManagerConstants.AttributeName.FIRSTNAME.getId());
            }
            if (changedParameters.containsKey(UserManagerConstants.AttributeName.LASTNAME.getId())) {
                lastName =
                        (String)changedParameters.get(UserManagerConstants.AttributeName.LASTNAME.getId());
            }
            duplicateUser = isUserDuplicate(firstName, lastName);
            if (duplicateUser) {
                try {
                    logger.debug(CLASS_NAME + METHOD_NAME + "duplicateUser: " +
                                 duplicateUser);

                    HashMap<String, Object> atrrMap =
                        new HashMap<String, Object>();
                    UserManager usrMgr =
                        (UserManager)Platform.getService(UserManager.class);

                    atrrMap.put("IsDuplicateUser", "TRUE");
                    User user = new User(userKey, atrrMap);
                    usrMgr.modify(user);
                    logger.debug(CLASS_NAME + METHOD_NAME +
                                 "Setting duplicateUser:" + duplicateUser);
                } catch (Exception e) {
                    logger.error(CLASS_NAME + METHOD_NAME + "Exception ", e);
                }
            }
        }
    }

    private void setEverMatriculated(String userKey, HashMap changedParameters,
                                     Identity currentUser) {
        String METHOD_NAME = " setEverMatriculated ";

        logger.debug(CLASS_NAME + METHOD_NAME +
                     "setGlobalStatus() Parameter Variables passed are:" +
                     "userKey=[" + userKey + "]");

        try {
            Hashtable studentStatusHash = new Hashtable();

            for (Map.Entry<String, Object> entry :
                 ((Map<String, Object>)changedParameters).entrySet()) {
                String columnName = entry.getKey(); // It is the USR ColumnName
                Object value = entry.getValue();
                if (value != null) {
                    if ((columnName.endsWith("_STU") ||
                         columnName.endsWith("Stu")) &&
                        APIUtil.checkIfParameterChanged(columnName,
                                                        changedParameters,
                                                        currentUser)) {
                        studentStatusHash.put(columnName, (String)value);
                    }
                }
            }

            logger.debug(CLASS_NAME + METHOD_NAME + "studentStatusHash: " +
                         studentStatusHash);

            HashMap<String, Object> atrrMap = new HashMap<String, Object>();
            UserManager usrMgr =
                (UserManager)Platform.getService(UserManager.class);


            //EVERMATRIC -- Here it will set the everMatric value.
            String everMatric = null;
            if (currentUser != null)
                everMatric = (String)currentUser.getAttribute("EVERMATRIC");
            logger.debug(CLASS_NAME + METHOD_NAME + "Current EVERMATRIC:" +
                         everMatric);

            if (everMatric == null || !everMatric.equalsIgnoreCase("TRUE")) {
                if (studentStatusHash.containsValue("AC")) {
                    everMatric = "TRUE";
                    atrrMap.put("EVERMATRIC", everMatric);
                    User user = new User(userKey, atrrMap);
                    usrMgr.modify(user);
                    logger.debug(CLASS_NAME + METHOD_NAME +
                                 "Setting EVERMATRIC:" + everMatric);
                }
            }
            if (everMatric == null) {  // If it is null.
                if (!studentStatusHash.containsValue("AC")) {  // User is not everMatriculated.
                    everMatric = "FALSE";
                    atrrMap.put("EVERMATRIC", everMatric);
                    User user = new User(userKey, atrrMap);
                    usrMgr.modify(user);
                    logger.debug(CLASS_NAME + METHOD_NAME +
                                 "Setting EVERMATRIC:" + everMatric);
                }
            }

        } catch (Exception e) {
            logger.error(CLASS_NAME + METHOD_NAME + "Exception ", e);
        }
    }
}
