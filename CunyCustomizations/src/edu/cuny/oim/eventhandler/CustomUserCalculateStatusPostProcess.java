package edu.cuny.oim.eventhandler;


import Thor.API.Exceptions.tcAPIException;
import Thor.API.Exceptions.tcLoginAttemptsExceededException;
import Thor.API.Exceptions.tcPasswordResetAttemptsExceededException;
import Thor.API.Exceptions.tcUserAccountDisabledException;
import Thor.API.Exceptions.tcUserAccountInvalidException;
import Thor.API.Exceptions.tcUserAlreadyLoggedInException;
import Thor.API.Operations.tcOrganizationOperationsIntf;
import Thor.API.Operations.tcUserOperationsIntf;
import Thor.API.tcResultSet;

import com.thortech.util.logging.Logger;
import com.thortech.xl.crypto.tcCryptoException;

import edu.cuny.oim.util.APIUtil;

import java.io.Serializable;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.Vector;

import oracle.iam.identity.usermgmt.api.UserManager;
import oracle.iam.identity.usermgmt.vo.User;
import oracle.iam.identity.vo.Identity;
import oracle.iam.platform.Platform;
import oracle.iam.platform.kernel.spi.PostProcessHandler;
import oracle.iam.platform.kernel.vo.AbstractGenericOrchestration;
import oracle.iam.platform.kernel.vo.BulkEventResult;
import oracle.iam.platform.kernel.vo.BulkOrchestration;
import oracle.iam.platform.kernel.vo.EventResult;
import oracle.iam.platform.kernel.vo.Orchestration;


public class CustomUserCalculateStatusPostProcess implements PostProcessHandler {
    Logger logger = Logger.getLogger("CUNY.CUSTOM");
    private String CLASS_NAME = " CustomUserCalculateStatusPostProcess ";


    public EventResult execute(long l, long l1, Orchestration orchestration) {
        String METHOD_NAME = " Execute";
        logger.info(CLASS_NAME + METHOD_NAME + " Executing Execute Method");
        String orchOperation = orchestration.getOperation();
        String userKey = orchestration.getTarget().getEntityId();
        logger.info(CLASS_NAME + METHOD_NAME + "  User Key  " + userKey);
        HashMap parameterHashMap = orchestration.getParameters();


        try {
            HashMap<String, Serializable> interParameters =
                orchestration.getInterEventData();
            Object curUserObject = interParameters.get("CURRENT_USER");
            Identity newUsersState = (Identity)curUserObject;
            executeEvent(parameterHashMap, userKey,
                         orchestration.getTarget().getType(), newUsersState,
                         orchOperation);
        } catch (Exception e) {
            logger.error(CLASS_NAME + METHOD_NAME + " Exception ", e);
        }
        logger.info(CLASS_NAME + METHOD_NAME + " Exiting Execute Method");


        return new EventResult();
    }


    public BulkEventResult execute(long l, long l1,
                                   BulkOrchestration bulkOrchestration) {

        String METHOD_NAME = ".BulkExecute.";
        logger.info(CLASS_NAME + METHOD_NAME +
                    " Executing BulkExecute Method");
        String orchOperation = bulkOrchestration.getOperation();
        String[] userKeys = bulkOrchestration.getTarget().getAllEntityId();

        HashMap[] parameterHashMap = bulkOrchestration.getBulkParameters();
        HashMap<String, Serializable> interParameters =
            bulkOrchestration.getInterEventData();
        Object curUserObject = interParameters.get("CURRENT_USER");
        Identity[] newUsersState = (Identity[])curUserObject;


        for (int i = 0; i < parameterHashMap.length; i++) {
            logger.info(CLASS_NAME + METHOD_NAME + " UserKey " + userKeys[i]);
            try {
                executeEvent(parameterHashMap[i], userKeys[i],
                             bulkOrchestration.getTarget().getType(),
                             newUsersState[i], orchOperation);
            } catch (Exception e) {
                logger.error(CLASS_NAME + METHOD_NAME + "Execute Exception ", e);
            }
        }
        logger.info(CLASS_NAME + METHOD_NAME + " Exiting BulkExecute Method");


        return new BulkEventResult();
    }

    public void executeEvent(HashMap parameters, String userKey,
                             String targetType, Identity currentUser,
                             String orchOperation) {
        String METHOD_NAME = ".executeEvent.";
        logger.info(CLASS_NAME + METHOD_NAME + " Entering with parameters " + parameters);
        HashMap<java.lang.String, java.lang.Object> currentUserMap = null;
        try {
            // For any chnages to user re-calculate the user-status
            if (APIUtil.checkIfStatusChanges(parameters, currentUser)) {
                if(currentUser!=null)
                    currentUserMap = currentUser.getAttributes();
                setGlobalStatusUpdatedCode(userKey, currentUserMap, parameters);
            }
        } catch (Exception e) {
            logger.error(CLASS_NAME + METHOD_NAME + "Execute Exception ", e);
        }

    }

    public boolean cancel(long l, long l1,
                          AbstractGenericOrchestration abstractGenericOrchestration) {
        return false;
    }

    public void initialize(HashMap<String, String> hashMap) {
    }

    public void compensate(long l, long l1,
                           AbstractGenericOrchestration abstractGenericOrchestration) {
    }

    private void setGlobalStatusUpdatedCode(String userKey,
                                            HashMap<java.lang.String, java.lang.Object> currentUserMap, HashMap changedParameters) {
        String METHOD_NAME = " setGlobalStatusUpdatedCode ";

        logger.debug(CLASS_NAME + METHOD_NAME + "setGlobalStatus() Parameter Variables passed are:" +
                     "userKey=[" + userKey + "] currentUserMap : " + currentUserMap);

        String employeeStatus = "";
        String studentStatus = "";
        String globalStatus = "";
        boolean isUserDeceased = false;
        try {
            Hashtable studentStatusHash = new Hashtable();
            Hashtable employeeStatusHash = new Hashtable();
            String valueInParam = null;
            if (currentUserMap != null && !currentUserMap.isEmpty()) {
                for (Map.Entry<String, Object> entry : currentUserMap.entrySet()) {
                    String columnName = entry.getKey(); // It is the USR ColumnName
                    Object value = entry.getValue();
                    if (columnName.endsWith("_EMP") || columnName.endsWith("Emp")) {
                        valueInParam = APIUtil.getParameterValue(changedParameters, columnName);
                        if(valueInParam!=null)
                            employeeStatusHash.put(columnName, (String)valueInParam);
                        else if(value!=null && !changedParameters.containsKey(columnName))
                            employeeStatusHash.put(columnName, (String)value);
                    } else if (columnName.endsWith("_STU") || columnName.endsWith("Stu")) {
                        valueInParam = APIUtil.getParameterValue(changedParameters, columnName);
                        if(valueInParam!=null)
                            studentStatusHash.put(columnName, (String)valueInParam);
                        else if(value!=null && !changedParameters.containsKey(columnName)) {
                            studentStatusHash.put(columnName, (String)value);
                        }
                        
                    }
                }
            } else {
                for (Map.Entry<String, Object> entry : ((Map<String, Object>)changedParameters).entrySet()) {
                    String columnName = entry.getKey(); // It is the USR ColumnName
                    Object value = entry.getValue();
                    if (columnName.endsWith("_EMP") || columnName.endsWith("Emp")) {
                        employeeStatusHash.put(columnName, (String)value);
                    } else if (columnName.endsWith("_STU") || columnName.endsWith("Stu")) {
                        studentStatusHash.put(columnName, (String)value);
                    }
                }                
            }
            logger.debug(CLASS_NAME + METHOD_NAME + "studentStatusHash: " +studentStatusHash + " employeeStatusHash: " + employeeStatusHash);

            //Find status for employees
            if (employeeStatusHash.containsValue("A") ||
                employeeStatusHash.containsValue("L") ||
                employeeStatusHash.containsValue("P") ||
                employeeStatusHash.containsValue("S") ||
                employeeStatusHash.containsValue("W")) {
                employeeStatus = "Active";
            } else {
                if (employeeStatusHash.containsValue("R") ||
                    employeeStatusHash.containsValue("Q")) {
                    employeeStatus = "Retired";
                } else if (employeeStatusHash.containsValue("T") ||
                           employeeStatusHash.containsValue("U") ||
                           employeeStatusHash.containsValue("V") ||
                           employeeStatusHash.containsValue("X")) {
                    employeeStatus = "Inactive";
                } else if (employeeStatusHash.containsValue("D")) {
                    employeeStatus = "Inactive";
                    isUserDeceased = true;
                }
            }
            logger.debug(CLASS_NAME + METHOD_NAME + "employeeStatus:" + employeeStatus);

            //Find status for Students
            if (studentStatusHash.containsValue("AC") ||
                studentStatusHash.containsValue("AD") ||
                studentStatusHash.containsValue("AP") ||
                studentStatusHash.containsValue("CM") ||
                studentStatusHash.containsValue("CN") ||
                studentStatusHash.containsValue("DC") ||
                studentStatusHash.containsValue("DM") ||
                studentStatusHash.containsValue("LA") ||
                studentStatusHash.containsValue("PM") ||
                studentStatusHash.containsValue("SP") ||
                studentStatusHash.containsValue("WT")) {
                studentStatus = "Active";
            } else if (studentStatusHash.containsValue("DE")) {
                studentStatus = "Inactive";
                isUserDeceased = true;
            }
            logger.debug(CLASS_NAME + METHOD_NAME + "studentStatus:" + studentStatus);

            if (studentStatus.length() != 0) {
                globalStatus = studentStatus;
            } else {
                globalStatus = employeeStatus;
            }
            logger.debug(CLASS_NAME + METHOD_NAME + "globalStatus:" + globalStatus);            
            
            HashMap<String, Object> atrrMap = new HashMap<String, Object>();

            UserManager usrMgr =
                (UserManager)Platform.getService(UserManager.class);
            atrrMap.put("GLBL_STU_STATUS", studentStatus);
            atrrMap.put("GLBL_EMP_STATUS", employeeStatus);
            atrrMap.put("GLBL_STATUS", globalStatus);
            atrrMap.putAll(formStatusMap(studentStatusHash));
            
            if(isUserDeceased) atrrMap.put("IsDeceasedUser", "TRUE");
            else atrrMap.put("IsDeceasedUser", "FALSE");

            User user = new User(userKey, atrrMap);
            usrMgr.modify(user);
            logger.debug(CLASS_NAME + METHOD_NAME + "User Global Status Values Updated: studentStatus[" +
                         studentStatus + "]" + "employeeStatus[" +
                         employeeStatus + "]globalStatus[" + globalStatus +
                         "]");
        } catch (Exception e) {
            logger.error(CLASS_NAME + METHOD_NAME + "Exception ", e);
        }
    }
    
    
    private HashMap<String, Object> formStatusMap(Hashtable studentStatusHash){
        HashMap<String, Object> atrrMap = new HashMap<String, Object>();

        if (studentStatusHash.containsValue("AC")){
            atrrMap.put("StudentMatriculated", "TRUE");
        } else atrrMap.put("StudentMatriculated", "FALSE");

        if (studentStatusHash.containsValue("AD")){
            atrrMap.put("StudentAdmitted", "TRUE");
        } else atrrMap.put("StudentAdmitted", "FALSE");

        if (studentStatusHash.containsValue("AP")){
            atrrMap.put("StudentApplicant", "TRUE");
        } else atrrMap.put("StudentApplicant", "FALSE");

        if (studentStatusHash.containsValue("CN")){
            atrrMap.put("StudentCancelled", "TRUE");
        } else atrrMap.put("StudentCancelled", "FALSE");

        if (studentStatusHash.containsValue("CM")){
            atrrMap.put("StudentCompleted", "TRUE");
        } else atrrMap.put("StudentCompleted", "FALSE");

        if (studentStatusHash.containsValue("DC")){
            atrrMap.put("StudentDiscontinued", "TRUE");
        } else atrrMap.put("StudentDiscontinued", "FALSE");

        if (studentStatusHash.containsValue("DM")){
            atrrMap.put("StudentDismissed", "TRUE");
        } else atrrMap.put("StudentDismissed", "FALSE");

        if (studentStatusHash.containsValue("PM")){
            atrrMap.put("StudentPrematriculated", "TRUE");
        } else atrrMap.put("StudentPrematriculated", "FALSE");

        if (studentStatusHash.containsValue("WT")){
            atrrMap.put("StudentWaitlisted", "TRUE");
        } else atrrMap.put("StudentWaitlisted", "FALSE");

        if (studentStatusHash.containsValue("SP")){
            atrrMap.put("StudentSuspended", "TRUE");
        } else atrrMap.put("StudentSuspended", "FALSE");
                
        
        return atrrMap;
    }
    
}
