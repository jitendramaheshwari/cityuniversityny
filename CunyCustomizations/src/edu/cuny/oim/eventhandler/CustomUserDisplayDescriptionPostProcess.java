package edu.cuny.oim.eventhandler;


import com.thortech.util.logging.Logger;

import edu.cuny.oim.util.APIUtil;

import java.io.Serializable;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import oracle.iam.identity.exception.UserSearchException;
import oracle.iam.identity.usermgmt.api.UserManager;
import oracle.iam.identity.usermgmt.api.UserManagerConstants;
import oracle.iam.identity.usermgmt.vo.User;
import oracle.iam.identity.vo.Identity;
import oracle.iam.notification.api.NotificationService;
import oracle.iam.notification.vo.NotificationEvent;
import oracle.iam.platform.Platform;
import oracle.iam.platform.entitymgr.vo.SearchCriteria;
import oracle.iam.platform.kernel.spi.PostProcessHandler;
import oracle.iam.platform.kernel.vo.AbstractGenericOrchestration;
import oracle.iam.platform.kernel.vo.BulkEventResult;
import oracle.iam.platform.kernel.vo.BulkOrchestration;
import oracle.iam.platform.kernel.vo.EventResult;
import oracle.iam.platform.kernel.vo.Orchestration;


public class CustomUserDisplayDescriptionPostProcess implements PostProcessHandler {
    Logger logger = Logger.getLogger("CUNY.CUSTOM");
    private String CLASS_NAME = " CustomUserDisplayDescriptionPostProcess ";

    public EventResult execute(long l, long l1, Orchestration orchestration) {
        String METHOD_NAME = " Execute";
        logger.info(CLASS_NAME + METHOD_NAME + " Executing Execute Method");
        String orchOperation = orchestration.getOperation();
        String userKey = orchestration.getTarget().getEntityId();
        logger.info(CLASS_NAME + METHOD_NAME + "  User Key  " + userKey);
        HashMap parameterHashMap = orchestration.getParameters();


        try {
            HashMap<String, Serializable> interParameters =
                orchestration.getInterEventData();
            Object curUserObject = interParameters.get("CURRENT_USER");
            Identity newUsersState = (Identity)curUserObject;
            executeEvent(parameterHashMap, userKey,
                         orchestration.getTarget().getType(), newUsersState,
                         orchOperation);
        } catch (Exception e) {
            logger.error(CLASS_NAME + METHOD_NAME + " Exception ", e);
        }
        logger.info(CLASS_NAME + METHOD_NAME + " Exiting Execute Method");


        return new EventResult();
    }


    public BulkEventResult execute(long l, long l1,
                                   BulkOrchestration bulkOrchestration) {

        String METHOD_NAME = ".BulkExecute.";
        logger.info(CLASS_NAME + METHOD_NAME +
                    " Executing BulkExecute Method");
        String orchOperation = bulkOrchestration.getOperation();
        String[] userKeys = bulkOrchestration.getTarget().getAllEntityId();

        HashMap[] parameterHashMap = bulkOrchestration.getBulkParameters();
        HashMap<String, Serializable> interParameters =
            bulkOrchestration.getInterEventData();
        Object curUserObject = interParameters.get("CURRENT_USER");
        Identity[] newUsersState = (Identity[])curUserObject;


        for (int i = 0; i < parameterHashMap.length; i++) {
            logger.info(CLASS_NAME + METHOD_NAME + " UserKey " + userKeys[i]);
            try {
                executeEvent(parameterHashMap[i], userKeys[i],
                             bulkOrchestration.getTarget().getType(),
                             newUsersState[i], orchOperation);
            } catch (Exception e) {
                logger.error("Execute Exception ", e);
            }
        }
        logger.info(CLASS_NAME + METHOD_NAME + " Exiting BulkExecute Method");


        return new BulkEventResult();
    }
    
    public void executeEvent(HashMap parameters, String userKey,
                             String targetType, Identity currentUser,
                             String orchOperation) {
        String METHOD_NAME = ".executeEvent.";
        logger.info(CLASS_NAME + METHOD_NAME + " Entering with parameters " + parameters);

        try {
            if (APIUtil.checkIfParameterChanged("Last Name", parameters, currentUser) ||
               APIUtil.checkIfParameterChanged("First Name", parameters, currentUser)) {
                if (orchOperation.equalsIgnoreCase("MODIFY")) {
                    generateAndSetUserLogin(userKey, parameters, currentUser);
                }
                generateAndSetUserDisplayNameDescription(userKey);
            }
        } catch (Exception e) {
            logger.error(CLASS_NAME + METHOD_NAME + "Execute Exception ", e);
        }

    }

    public boolean cancel(long l, long l1,
                          AbstractGenericOrchestration abstractGenericOrchestration) {
        return false;
    }

    public void initialize(HashMap<String, String> hashMap) {
    }

    public void compensate(long l, long l1,
                           AbstractGenericOrchestration abstractGenericOrchestration) {
    }



    /**
     * It generate User-login for the user and update it.
     * @param userKey
     * @param parameters
     * @param operation
     */
    private void generateAndSetUserLogin(String userKey, HashMap map,
                                         Identity currentUser) {
        //Last Name, First Name Initials.
        String METHOD_NAME = " generateAndSetUserDisplayName ";
        logger.info(CLASS_NAME + METHOD_NAME + "Entering with userKey: " +
                    userKey);

        UserManager usrMgr =
            (UserManager)Platform.getService(UserManager.class);

        String firstName = null;
        String lastName = null;
        String employeeId = null;
        String userid = null;

        //get FirstName
        if (map.get("First Name") != null) {
            firstName =
                    map.get("First Name").toString().trim().replace(" ", "");
            logger.info(CLASS_NAME + METHOD_NAME + "First Name " + firstName);
        } else
            firstName =
                    (String)currentUser.getAttribute(UserManagerConstants.AttributeName.FIRSTNAME.getId());

        //get LastName
        if (map.get("Last Name") != null) {
            lastName = map.get("Last Name").toString().replace(" ", "").trim();
            logger.info(CLASS_NAME + METHOD_NAME + "Last Name " + lastName);
        } else
            lastName =
                    (String)currentUser.getAttribute(UserManagerConstants.AttributeName.LASTNAME.getId());

        //get UserManagerConstants.AttributeName.EMPLOYEE_NUMBER.getId()
        if (map.get(UserManagerConstants.AttributeName.EMPLOYEE_NUMBER.getId()) !=
            null) {
            employeeId =
                    map.get(UserManagerConstants.AttributeName.EMPLOYEE_NUMBER.getId()).toString().replace(" ",
                                                                                                           "").trim();
            logger.info(CLASS_NAME + METHOD_NAME + "Employee Number " +
                        employeeId);
        } else
            employeeId =
                    (String)currentUser.getAttribute(UserManagerConstants.AttributeName.EMPLOYEE_NUMBER.getId());

        try {
            userid = generateOIMUserID(firstName, lastName, employeeId);
        } catch (Exception e) {
            logger.error(CLASS_NAME + METHOD_NAME + "Exception ", e);
            return;
        }

        HashMap<String, Object> atrrMap = new HashMap<String, Object>();
        atrrMap.put(UserManagerConstants.AttributeName.USER_LOGIN.getId(),
                    userid);

        try {
            User u1 = usrMgr.getDetails(userKey, null, false);
            String prevlogin = u1.getLogin();
            if (!prevlogin.equalsIgnoreCase(userid)) {
                User user = new User(userKey, atrrMap);
                usrMgr.modify(user);
                setEmailOnLoginChange(prevlogin, userid, "XELSYSADM");
            }
            logger.info(CLASS_NAME + METHOD_NAME + "Modified with atrrMap: " +
                        atrrMap + ", userKey: " + userKey);

        } catch (Exception e) {
            logger.error("Exception ", e);
        }
    }

    public String generateOIMUserID(String firstName, String lastName,
                                    String employeeId) {
        String METHOD_NAME = " generateOIMUserID ";
        logger.info(CLASS_NAME + METHOD_NAME + "Entering with firstName = " +
                    firstName + " lastName = " + lastName + " employeeId " +
                    employeeId);

        long time = System.currentTimeMillis();

        boolean userIDInUse = false;
        String userId = null;

        if (!APIUtil.isOIMAttributeNull(firstName)) {
            firstName = APIUtil.truncateNonAlphaNumeric(firstName);
        }

        if (!APIUtil.isOIMAttributeNull(employeeId)) {
            employeeId = APIUtil.truncateNonAlphaNumeric(employeeId);
        }

        if (!APIUtil.isOIMAttributeNull(lastName)) {
            lastName = APIUtil.truncateNonAlphaNumeric(lastName);
        }

        logger.info(CLASS_NAME + METHOD_NAME + " firstName = " + firstName +
                    " lastName = " + lastName);

        if (!APIUtil.isOIMAttributeNull(firstName) && !APIUtil.isOIMAttributeNull(lastName) &&
            !APIUtil.isOIMAttributeNull(employeeId)) {
            int count = 2;
            userIDInUse = true;

            while (userIDInUse) {
                userId =
                        firstName + "." + lastName + employeeId.substring(employeeId.length() -
                                                                          count);
                logger.info("userId :" + userId);
                userIDInUse = checkIfUserExistsByLogin(userId);
                logger.info("userIDInUse :" + userIDInUse);
                count++;
                logger.info("******Time taken for next iteration =" +
                            (System.currentTimeMillis() - time));
            }
        }
        logger.info(CLASS_NAME + METHOD_NAME +
                    "******Time taken for completing and return =" +
                    (System.currentTimeMillis() - time));

        return userId;
    }

    public boolean checkIfUserExistsByLogin(String Username) {
        String METHOD_NAME = " checkIfUserExistsByLogin ";
        logger.info(CLASS_NAME + METHOD_NAME + " Entering..  Username " +
                    Username);
        UserManager userManagement = Platform.getService(UserManager.class);
        SearchCriteria criteria1;
        Set<String> retAttrs = new HashSet<String>();
        retAttrs.add(UserManagerConstants.AttributeName.USER_LOGIN.getId());
        criteria1 =
                new SearchCriteria(UserManagerConstants.AttributeName.USER_LOGIN.getId(),
                                   Username, SearchCriteria.Operator.EQUAL);
        List<User> users1;
        try {
            users1 = userManagement.search(criteria1, retAttrs, null);
            if (users1 != null && users1.size() > 0) {
                return true;
            }
        } catch (UserSearchException exuserSearchException) {
            logger.info(CLASS_NAME + METHOD_NAME + "UserSearchException ",
                        exuserSearchException);
        }
        return false;
    }

    private void setEmailOnLoginChange(String prevLogin, String userLogin,
                                       String fromAddress) {
        String METHOD_NAME = " setEmailOnLoginChange ";
        logger.info(CLASS_NAME + METHOD_NAME + " Entering with userName: " +
                    userLogin + " fromAddress: " + fromAddress);

        NotificationService notificationService =
            Platform.getService(oracle.iam.notification.api.NotificationService.class);
        NotificationEvent event = new NotificationEvent();
        event.setTemplateName("CUNYOIMUserLoginChangeTemplate"); // Template

        event.setSender(fromAddress);

        String[] users = new String[1];
        users[0] = userLogin;
        event.setUserIds(users);

        HashMap<String, Object> notificationData =
            new HashMap<String, Object>();
        notificationData.put("userLogin", userLogin);
        notificationData.put("prevLogin", prevLogin);

        event.setParams(notificationData);
        try {
            notificationService.notify(event);
            logger.info(CLASS_NAME + METHOD_NAME +
                        " Notification sent event: " + event);
        } catch (Exception e) {
            logger.error(CLASS_NAME + METHOD_NAME + "Exception", e);
        }

    }


    private void generateAndSetUserDisplayNameDescription(String userKey) {
        String METHOD_NAME = " generateAndSetUserDisplayName ";
        logger.info(CLASS_NAME + METHOD_NAME + "Entering with userKey: " + userKey);

        UserManager usrMgr = (UserManager) Platform.getService(UserManager.class);
        Set<String> retAttrs = new HashSet<String>();
        retAttrs.add(UserManagerConstants.AttributeName.FIRSTNAME.getId());
        retAttrs.add(UserManagerConstants.AttributeName.LASTNAME.getId());
        User user;
        String displayName = "";
        String firstName = "";
        String lastName = "";
        try {
            user = usrMgr.getDetails(userKey, retAttrs, false);
            if (user != null) {
                firstName = user.getFirstName();
                if(firstName == null) firstName = "";
                lastName = user.getLastName();
                if(lastName == null) lastName = "";
                displayName = firstName + " " + lastName;

                Map dispNameMap = new HashMap();     
                dispNameMap.put("base", displayName);

                HashMap<String, Object> atrrMap = new HashMap<String, Object>();
                atrrMap.put(UserManagerConstants.AttributeName.DISPLAYNAME.getId(), dispNameMap);
                atrrMap.put(UserManagerConstants.AttributeName.DESCRIPTION.getId(), updateUserDescription(firstName, lastName));

                User user1 = new User(userKey, atrrMap);
                usrMgr.modify(user1);
                logger.info(CLASS_NAME + METHOD_NAME + "Modified with atrrMap: " + atrrMap + ", userKey: " + userKey);
            }
        } catch (Exception e) {
            logger.error("Exception ", e);
        }
    }

    private String updateUserDescription(String first, String last)
    {
        String METHOD_NAME = " updateUserDescription ";
      final int MAX_LENGTH = 30;
      String returnValue = "INIT_VALUE";
      String[] words = null;
      if(first.length() + last.length() <= MAX_LENGTH) // simplest case
      {
        logger.info(CLASS_NAME + METHOD_NAME + "DEBUG: simplest case as combined string length with space is " + (first.length() + last.length() + 1));
        returnValue = first + " " + last;
      } else 
      {
        // Here we see if we have more than two first names.
        // This will allow us to remove all but the first.
        logger.info(CLASS_NAME + METHOD_NAME + "DEBUG: Current first name length is " + first.length() + " characters");
        logger.info(CLASS_NAME + METHOD_NAME + "DEBUG: Trying to shorten first name by splitting on space character");
        words = first.split(" ");
        first = words[0];
        logger.info(CLASS_NAME + METHOD_NAME + "DEBUG: After trying split on space first name = " + first);
        if(first.length() + last.length() <= MAX_LENGTH)
        {
          logger.info(CLASS_NAME + METHOD_NAME + "DEBUG: Length resolved by shortening first name field");
          returnValue = first + " " + last;
        } else
        {
            // This user has a single first and single last yet
            // still the combination is too long. Have to truncate.
            logger.info(CLASS_NAME + METHOD_NAME + "DEBUG: User Description is still too long");
            logger.info(CLASS_NAME + METHOD_NAME + "DEBUG: Resorting to brutish truncation of leading characters");
            String s = first + " " + last;
            returnValue = s.substring((s.length() - MAX_LENGTH), Math.max(s.length(), MAX_LENGTH));
        }
      }
      logger.info(CLASS_NAME + METHOD_NAME + "DEBUG: Returning \"" + returnValue + "\"");
      logger.info(CLASS_NAME + METHOD_NAME + "DEBUG: Final string has length of " + returnValue.length());
        return returnValue;

    } // end of main


}
