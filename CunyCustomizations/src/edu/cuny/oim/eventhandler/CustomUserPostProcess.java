package edu.cuny.oim.eventhandler;


import com.thortech.util.logging.Logger;

import edu.cuny.oim.util.APIUtil;

import java.io.Serializable;

import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import oracle.iam.identity.usermgmt.api.UserManager;
import oracle.iam.identity.usermgmt.api.UserManagerConstants;
import oracle.iam.identity.usermgmt.api.UserManagerConstants.AttributeName;
import oracle.iam.identity.usermgmt.vo.User;
import static oracle.iam.identity.utils.Constants.MANAGERKEY;
import oracle.iam.identity.vo.Identity;
import oracle.iam.platform.Platform;
import oracle.iam.platform.entitymgr.EntityManager;
import oracle.iam.platform.entitymgr.vo.SearchCriteria;
import oracle.iam.platform.kernel.OrchestrationEngine;
import oracle.iam.platform.kernel.spi.PostProcessHandler;
import oracle.iam.platform.kernel.vo.AbstractGenericOrchestration;
import oracle.iam.platform.kernel.vo.BulkEventResult;
import oracle.iam.platform.kernel.vo.BulkOrchestration;
import oracle.iam.platform.kernel.vo.EventResult;
import oracle.iam.platform.kernel.vo.Orchestration;


public class CustomUserPostProcess implements PostProcessHandler {
    Logger logger = Logger.getLogger("CUNY.CUSTOM");
    private String CLASS_NAME = " CustomUserPostProcess ";

    public EventResult execute(long l, long l1, Orchestration orchestration) {
        String METHOD_NAME = " Execute";
        logger.info(CLASS_NAME + METHOD_NAME + " Executing Execute Method");
        String orchOperation = orchestration.getOperation();
        String userKey = orchestration.getTarget().getEntityId();
        logger.info(CLASS_NAME + METHOD_NAME + "  User Key  " + userKey);
        HashMap parameterHashMap = orchestration.getParameters();


        try {
            HashMap<String, Serializable> interParameters =
                orchestration.getInterEventData();
            Object curUserObject = interParameters.get("CURRENT_USER");
            Identity newUsersState = (Identity)curUserObject;
            executeEvent(parameterHashMap, userKey,
                         orchestration.getTarget().getType(), newUsersState,
                         orchOperation);
        } catch (Exception e) {
            logger.error(CLASS_NAME + METHOD_NAME + " Exception ", e);
        }
        logger.info(CLASS_NAME + METHOD_NAME + " Exiting Execute Method");


        return new EventResult();
    }


    public BulkEventResult execute(long l, long l1,
                                   BulkOrchestration bulkOrchestration) {

        String METHOD_NAME = ".BulkExecute.";
        logger.info(CLASS_NAME + METHOD_NAME +
                    " Executing BulkExecute Method");
        String orchOperation = bulkOrchestration.getOperation();
        String[] userKeys = bulkOrchestration.getTarget().getAllEntityId();

        HashMap[] parameterHashMap = bulkOrchestration.getBulkParameters();
        HashMap<String, Serializable> interParameters =
            bulkOrchestration.getInterEventData();
        Object curUserObject = interParameters.get("CURRENT_USER");
        Identity[] newUsersState = (Identity[])curUserObject;


        for (int i = 0; i < parameterHashMap.length; i++) {
            logger.info(CLASS_NAME + METHOD_NAME + " UserKey " + userKeys[i]);
            try {
                executeEvent(parameterHashMap[i], userKeys[i],
                             bulkOrchestration.getTarget().getType(),
                             newUsersState[i], orchOperation);
            } catch (Exception e) {
                logger.error("Execute Exception ", e);
            }
        }
        logger.info(CLASS_NAME + METHOD_NAME + " Exiting BulkExecute Method");


        return new BulkEventResult();
    }
    
    public void executeEvent(HashMap parameters, String userKey,
                             String targetType, Identity currentUser,
                             String orchOperation) {
        String METHOD_NAME = ".executeEvent.";
        logger.info(CLASS_NAME + METHOD_NAME + " Entering with parameters " + parameters);

        try {
            OrchestrationEngine orchEngine =
                (OrchestrationEngine)Platform.getService(OrchestrationEngine.class);

            String supervisorEmployeeID = APIUtil.getParameterValue(parameters, "Supervisor ID");

            if (orchOperation.equalsIgnoreCase("MODIFY")) {
                if (APIUtil.checkIfParameterChanged("Supervisor ID", parameters, currentUser)) {
                    if (supervisorEmployeeID == null)
                        supervisorEmployeeID = "";
                    if (supervisorEmployeeID.equals(""))
                        removeManager(userKey);
                }
            }

            if (orchOperation.equalsIgnoreCase("CREATE")) {
                // It will update the email address in email from PersonEmail only first time. 
                try {
                    String emailAddressRead = APIUtil.getParameterValue(parameters, "PersonalEmail");
                    UserManager usrMgr = (UserManager)Platform.getService(UserManager.class);
                    HashMap<String, Object> atrrMap = new HashMap<String, Object>();
                    atrrMap.put(oracle.iam.identity.utils.Constants.EMAIL, emailAddressRead);
                    User user = new User(userKey, atrrMap);
                    usrMgr.modify(user);
                } catch(Exception e){
                    logger.error(CLASS_NAME + METHOD_NAME + "Error updating Email address ", e);                    
                }
            }


            // Check if the global status is changing, Check if InActive set the termination date.
            // Check_with_Ray it need to be turned off.
            if (APIUtil.checkIfParameterChanged("GLBL_STATUS", parameters, currentUser)) {
                String globalStatus =
                    APIUtil.getParameterValue(parameters, "GLBL_STATUS");
                if (globalStatus != null &&
                    globalStatus.equalsIgnoreCase("Inactive")) {
                    setTerminatedDate(userKey); // It will set the termination date
                }
            }
            try {
                if (supervisorEmployeeID != null &&
                    !supervisorEmployeeID.isEmpty() && APIUtil.checkIfParameterChanged("Supervisor ID", parameters, currentUser))
                    modifyUserManager(supervisorEmployeeID, userKey,
                                      orchOperation);
            } catch (Exception e) {
                logger.error(CLASS_NAME + METHOD_NAME, e);
            }
        } catch (Exception e) {
            logger.error(CLASS_NAME + METHOD_NAME + "Execute Exception ", e);
        }

    }
    
    public boolean cancel(long l, long l1,
                          AbstractGenericOrchestration abstractGenericOrchestration) {
        return false;
    }

    public void initialize(HashMap<String, String> hashMap) {
    }

    public void compensate(long l, long l1,
                           AbstractGenericOrchestration abstractGenericOrchestration) {
    }


    private void modifyUserManager(String supervisorEmployeeID, String userKey,
                                   String operationType) throws Exception {
        String METHOD_NAME = " modifyUserManager ";
        logger.info(CLASS_NAME + METHOD_NAME +
                    "Entering with supervisorEmployeeID: " +
                    supervisorEmployeeID + ", userKey: " + userKey);

        String managerKey = null;
        SearchCriteria sc = null;
        SearchCriteria scActive = null;
        SearchCriteria criteriaStatus =
            new SearchCriteria(AttributeName.STATUS.getId(),
                               UserManagerConstants.AttributeValues.USER_STATUS_ACTIVE.getId(),
                               SearchCriteria.Operator.EQUAL);

        sc =
 new SearchCriteria(UserManagerConstants.AttributeName.EMPLOYEE_NUMBER.getId(),
                    supervisorEmployeeID, SearchCriteria.Operator.EQUAL);
        scActive =
                new SearchCriteria(criteriaStatus, sc, SearchCriteria.Operator.AND);

        Set<String> searchAttrs = new HashSet<String>();
        searchAttrs.add(AttributeName.USER_LOGIN.getId());

        UserManager usrMgr =
            (UserManager)Platform.getService(UserManager.class);
        EntityManager entityManager = Platform.getService(EntityManager.class);
        List<User> users = usrMgr.search(scActive, searchAttrs, null);
        if (users != null && !users.isEmpty() && users.size() == 1) {
            managerKey = (users.get(0)).getEntityId();
            logger.info(CLASS_NAME + METHOD_NAME + "managerKey: " +
                        managerKey);
        }

        if (managerKey != null) {
            HashMap<String, Object> atrrMap = new HashMap<String, Object>();
            atrrMap.put(MANAGERKEY, Long.parseLong(managerKey));

            if (operationType.equalsIgnoreCase("CREATE")) {
                entityManager.modifyEntity("User", userKey, atrrMap);
            } else {
                User user = new User(userKey, atrrMap);
                usrMgr.modify(user);
            }
            logger.info(CLASS_NAME + METHOD_NAME +
                        "Modified with managerKey: " + managerKey +
                        ", userKey: " + userKey);
        } else {
            // The Manager Employee-id is not found in OIM and then remove the manager ste in OIM.
            removeManager(userKey);
        }
    }

    private void removeManager(String userKey) {
        String METHOD_NAME = " removeManager ";
        logger.info(CLASS_NAME + METHOD_NAME + "Entering with userKey: " +
                    userKey);
        try {
            UserManager usrMgr =
                (UserManager)Platform.getService(UserManager.class);

            HashMap<String, Object> atrrMap = new HashMap<String, Object>();
            atrrMap.put(oracle.iam.identity.utils.Constants.MANAGERKEY, null);
            User user = new User(userKey, atrrMap);
            usrMgr.modify(user);
        } catch (Exception e) {
            logger.error("Exception", e);

        }
        logger.info(CLASS_NAME + METHOD_NAME +
                    "Removed Manager for userKey: " + userKey);

    }

    /**
     * When a user's Global Status becomes "Inactive", the Terminated Date is set to 60
     * days in the future.
     * @param userKey - Users.Key
     */
    private void setTerminatedDate(String userKey) {
        String METHOD_NAME = " setTerminatedDate ";

        logger.debug(CLASS_NAME + METHOD_NAME +
                     "Parameter Variables passed are:" + "userKey=[" +
                     userKey + "]");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 60);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        logger.debug(CLASS_NAME + METHOD_NAME + "60 Days Later:" +
                     cal.getTime());

        UserManager usrMgr =
            (UserManager)Platform.getService(UserManager.class);
        try {
            HashMap<String, Object> atrrMap = new HashMap<String, Object>();
            atrrMap.put("TERM_DATE", cal.getTime());
            User user = new User(userKey, atrrMap);
            usrMgr.modify(user);
        } catch (Exception e) {
            logger.error(CLASS_NAME + METHOD_NAME + "Exception", e);
        }
    }


}
