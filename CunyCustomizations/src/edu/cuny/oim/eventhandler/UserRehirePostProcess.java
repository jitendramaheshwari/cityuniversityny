package edu.cuny.oim.eventhandler;


import com.thortech.util.logging.Logger;

import edu.cuny.oim.util.APIUtil;

import java.io.Serializable;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import oracle.iam.identity.usermgmt.api.UserManager;
import oracle.iam.identity.usermgmt.vo.User;
import oracle.iam.identity.vo.Identity;
import oracle.iam.platform.Platform;
import oracle.iam.platform.kernel.spi.PostProcessHandler;
import oracle.iam.platform.kernel.vo.AbstractGenericOrchestration;
import oracle.iam.platform.kernel.vo.BulkEventResult;
import oracle.iam.platform.kernel.vo.BulkOrchestration;
import oracle.iam.platform.kernel.vo.EventResult;
import oracle.iam.platform.kernel.vo.Orchestration;


public class UserRehirePostProcess implements PostProcessHandler {
    Logger logger = Logger.getLogger("CUNY.CUSTOM");
    private String CLASS_NAME = " UserRehirePostProcess ";


    public EventResult execute(long l, long l1, Orchestration orchestration) {
        String METHOD_NAME = " Execute";
        logger.info(CLASS_NAME + METHOD_NAME + " Executing Execute Method");
        String orchOperation = orchestration.getOperation();
        String userKey = orchestration.getTarget().getEntityId();
        logger.info(CLASS_NAME + METHOD_NAME + "  User Key  " + userKey);
        HashMap parameterHashMap = orchestration.getParameters();


        try {
            HashMap<String, Serializable> interParameters =
                orchestration.getInterEventData();
            Object curUserObject = interParameters.get("CURRENT_USER");
            Identity newUsersState = (Identity)curUserObject;
            executeEvent(parameterHashMap, userKey,
                         orchestration.getTarget().getType(), newUsersState,
                         orchOperation);
        } catch (Exception e) {
            logger.error(CLASS_NAME + METHOD_NAME + " Exception ", e);
        }
        logger.info(CLASS_NAME + METHOD_NAME + " Exiting Execute Method");


        return new EventResult();
    }


    public BulkEventResult execute(long l, long l1,
                                   BulkOrchestration bulkOrchestration) {

        String METHOD_NAME = ".BulkExecute.";
        logger.info(CLASS_NAME + METHOD_NAME +
                    " Executing BulkExecute Method");
        String orchOperation = bulkOrchestration.getOperation();
        String[] userKeys = bulkOrchestration.getTarget().getAllEntityId();

        HashMap[] parameterHashMap = bulkOrchestration.getBulkParameters();
        HashMap<String, Serializable> interParameters =
            bulkOrchestration.getInterEventData();
        Object curUserObject = interParameters.get("CURRENT_USER");
        Identity[] newUsersState = (Identity[])curUserObject;


        for (int i = 0; i < parameterHashMap.length; i++) {
            logger.info(CLASS_NAME + METHOD_NAME + " UserKey " + userKeys[i]);
            try {
                executeEvent(parameterHashMap[i], userKeys[i],
                             bulkOrchestration.getTarget().getType(),
                             newUsersState[i], orchOperation);
            } catch (Exception e) {
                logger.error(CLASS_NAME + METHOD_NAME + "Execute Exception ", e);
            }
        }
        logger.info(CLASS_NAME + METHOD_NAME + " Exiting BulkExecute Method");


        return new BulkEventResult();
    }

    public void executeEvent(HashMap parameters, String userKey,
                             String targetType, Identity currentUser,
                             String orchOperation) {
        String METHOD_NAME = ".executeEvent.";
        logger.info(CLASS_NAME + METHOD_NAME + " Entering with parameters " + parameters);
        HashMap<java.lang.String, java.lang.Object> currentUserMap = null;
        String terminated = "FALSE";
        try {
            // For any chnages to user re-calculate the user-status
            if (APIUtil.checkIfParameterChanged("TERM_EFFECT_DATE", parameters, currentUser)) {
                if(parameters.get("TERM_EFFECT_DATE") != null) {
                    Date terDate = (Date)parameters.get("TERM_EFFECT_DATE");
                    
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(terDate);
                    cal.add(Calendar.DATE, 60);
                    cal.set(Calendar.HOUR_OF_DAY, 0);
                    cal.set(Calendar.MINUTE, 0);
                    cal.set(Calendar.SECOND, 0);
                    cal.set(Calendar.MILLISECOND, 0);
                    if(cal.before(Calendar.getInstance())){
                        terminated = "TRUE";
                    }
                }
                udateUserField(terminated, userKey);
            }
        } catch (Exception e) {
            logger.error(CLASS_NAME + METHOD_NAME + "Execute Exception ", e);
        }

    }

    private void udateUserField(String termValue, String userKey){
        UserManager usrMgr =
            (UserManager)Platform.getService(UserManager.class);
        HashMap<String, Object> atrrMap = new HashMap<String, Object>();

        atrrMap.put("TERM", termValue);
        User user = new User(userKey, atrrMap);
        try {
            usrMgr.modify(user);
            logger.debug(CLASS_NAME + " udateUserField " + "User  changed atrrMap " + atrrMap + " UserKey: " + userKey);
        } catch (Exception e) {
            logger.error(CLASS_NAME + " udateUserField " + "Exception ", e);
        }
    }

    public boolean cancel(long l, long l1,
                          AbstractGenericOrchestration abstractGenericOrchestration) {
        return false;
    }

    public void initialize(HashMap<String, String> hashMap) {
    }

    public void compensate(long l, long l1,
                           AbstractGenericOrchestration abstractGenericOrchestration) {
    }
}
