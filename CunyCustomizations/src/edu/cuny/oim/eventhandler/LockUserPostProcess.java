package edu.cuny.oim.eventhandler;


import Thor.API.Exceptions.tcAPIException;
import Thor.API.Exceptions.tcColumnNotFoundException;
import Thor.API.Exceptions.tcInvalidLookupException;
import Thor.API.Exceptions.tcLoginAttemptsExceededException;
import Thor.API.Exceptions.tcPasswordResetAttemptsExceededException;
import Thor.API.Exceptions.tcUserAccountDisabledException;
import Thor.API.Exceptions.tcUserAccountInvalidException;
import Thor.API.Exceptions.tcUserAlreadyLoggedInException;
import Thor.API.Operations.tcITResourceInstanceOperationsIntf;
import Thor.API.Operations.tcLookupOperationsIntf;
import Thor.API.Operations.tcUserOperationsIntf;
import Thor.API.tcResultSet;

import com.thortech.util.logging.Logger;
import com.thortech.xl.crypto.tcCryptoException;

import edu.cuny.oim.util.APIUtil;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;

import java.net.URLEncoder;

import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;

import oracle.iam.identity.usermgmt.api.UserManager;
import oracle.iam.identity.usermgmt.vo.User;
import oracle.iam.identity.vo.Identity;
import oracle.iam.platform.Platform;
import oracle.iam.platform.context.ContextAware;
import oracle.iam.platform.kernel.OrchestrationEngine;
import oracle.iam.platform.kernel.spi.PostProcessHandler;
import oracle.iam.platform.kernel.vo.AbstractGenericOrchestration;
import oracle.iam.platform.kernel.vo.BulkEventResult;
import oracle.iam.platform.kernel.vo.BulkOrchestration;
import oracle.iam.platform.kernel.vo.EventResult;
import oracle.iam.platform.kernel.vo.Orchestration;


public class LockUserPostProcess implements PostProcessHandler {
    Logger logger = Logger.getLogger("CUNY.CUSTOM");
    private String CLASS_NAME = " LockUserPostProcess ";

    public EventResult execute(long l, long l1, Orchestration orchestration) {
        String METHOD_NAME = " Execute";
        logger.info(CLASS_NAME + METHOD_NAME + " Executing Execute Method");
        String orchOperation = orchestration.getOperation();
        String userKey = orchestration.getTarget().getEntityId();
        logger.info(CLASS_NAME + METHOD_NAME + "  User Key  " + userKey);
        HashMap parameterHashMap = orchestration.getParameters();


        try {
            HashMap<String, Serializable> interParameters =
                orchestration.getInterEventData();
            Object curUserObject = interParameters.get("CURRENT_USER");
            Identity newUsersState = (Identity)curUserObject;
            executeEvent(parameterHashMap, userKey,
                         orchestration.getTarget().getType(), newUsersState,
                         orchOperation);
        } catch (Exception e) {
            logger.error(CLASS_NAME + METHOD_NAME + " Exception ", e);
        }
        logger.info(CLASS_NAME + METHOD_NAME + " Exiting Execute Method");


        return new EventResult();
    }


    public BulkEventResult execute(long l, long l1,
                                   BulkOrchestration bulkOrchestration) {

        String METHOD_NAME = ".BulkExecute.";
        logger.info(CLASS_NAME + METHOD_NAME +
                    " Executing BulkExecute Method");
        String orchOperation = bulkOrchestration.getOperation();
        String[] userKeys = bulkOrchestration.getTarget().getAllEntityId();

        HashMap[] parameterHashMap = bulkOrchestration.getBulkParameters();
        HashMap<String, Serializable> interParameters =
            bulkOrchestration.getInterEventData();
        Object curUserObject = interParameters.get("CURRENT_USER");
        Identity[] newUsersState = (Identity[])curUserObject;


        for (int i = 0; i < parameterHashMap.length; i++) {
            logger.info(CLASS_NAME + METHOD_NAME + " UserKey " + userKeys[i]);
            try {
                executeEvent(parameterHashMap[i], userKeys[i],
                             bulkOrchestration.getTarget().getType(),
                             newUsersState[i], orchOperation);
            } catch (Exception e) {
                logger.error("Execute Exception ", e);
            }
        }
        logger.info(CLASS_NAME + METHOD_NAME + " Exiting BulkExecute Method");


        return new BulkEventResult();
    }

    private void executeEvent(HashMap parameters, String userKey,
                              String targetType, Identity currentUser,
                              String orchOperation) {
        String METHOD_NAME = ".executeEvent.";
        logger.info(CLASS_NAME + METHOD_NAME + " Entering with orchOperation " + orchOperation + " userKey " + userKey + " parameters " + parameters);

        try {
            OrchestrationEngine orchEngine =
                (OrchestrationEngine)Platform.getService(OrchestrationEngine.class);
            logger.info("Current User Attributes" +
                        currentUser.getAttributes());
            
            if (orchOperation.equalsIgnoreCase("LOCK")) {
                APIUtil.setLockoutDate(userKey, "USR_UDF_LOCK_TS");
            }
            UserManager userManager = Platform.getService(UserManager.class);
            User user = userManager.getDetails(userKey, null, false);
            if(user!=null) {
                APIUtil.updateLockAttributeOID(user.getLogin(), orchOperation.equalsIgnoreCase("LOCK"));
            }
        } catch (Exception e) {
            logger.error(CLASS_NAME + METHOD_NAME + "Execute Exception ", e);
        }
    }

    
    public boolean cancel(long l, long l1,
                          AbstractGenericOrchestration abstractGenericOrchestration) {
        return false;
    }

    public void initialize(HashMap<String, String> hashMap) {
    }

    public void compensate(long l, long l1,
                           AbstractGenericOrchestration abstractGenericOrchestration) {
    }
}
