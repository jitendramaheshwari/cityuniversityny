/**
 * 
 */
package edu.cuny.oim;

import com.thortech.util.logging.Logger;

import java.io.UnsupportedEncodingException;

import java.net.URLEncoder;

import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;


/**
 * Code used to make connection to OID and perform various LDAP tasks
 * @author kpinsky
 *
 */
public class OIDFunctions {
	private static Logger log = Logger.getLogger("CUNY.OIDFUNCTIONS");
	private final String CLASS_NAME = "OIDFunctions()";
	private String serverAddress;
	private String port;
	private String rootContext;
	private String adminID;
	private String adminPWD;
	private String sslFlag;
	private DirContext directoryContext;
	private LdapContext ldapContext;
	
	/**For testing purposes.
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {

	}	

	public OIDFunctions(){		
	}
	
	/**
	 * Creates connection to an OID instance
	 * @param serverAddress - IT Resource Parameter
	 * @param port - IT Resource Parameter
	 * @param rootContext - IT Resource Parameter
	 * @param adminID - IT Resource Parameter
	 * @param adminPWD - IT Resource Parameter
	 * @param sslFlag - IT Resource Parameter
	 */
	public OIDFunctions(String serverAddress, String port, String rootContext, String adminID,
			String adminPWD, String sslFlag) {		
		log.debug(CLASS_NAME + " -----> inputs=serverAddress[" + serverAddress + 
				"]port[" + port + 
				"]rootContext[" + rootContext + 
				"]adminID[" + adminID + 
				"]adminPWD[********]" + 
				"sslFlag[" + sslFlag + "]");
		
		this.serverAddress = serverAddress;
		this.port = port;
		this.rootContext = rootContext;
		this.adminID = adminID;
		this.adminPWD = adminPWD;
		this.sslFlag = sslFlag;
		
	}
	
	/**
	 * Creates connection to OID at a specific container
	 * @param container - container of the users
	 */
	public void connectToOID(){
		log.debug(CLASS_NAME + ".connectToOID() ----->"); 
		String providerURL = new String("ldap://" + serverAddress + ":" + port);
		
		
		
		Hashtable env = new Hashtable();
		env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
		env.put(Context.PROVIDER_URL, providerURL);
		env.put(Context.SECURITY_PRINCIPAL, adminID);//+ "," + rootContext);
		///env.put(Context.SECURITY_PRINCIPAL, adminID);
		env.put(Context.SECURITY_CREDENTIALS, adminPWD);
		env.put(Context.REFERRAL,"ignore");
		env.put("java.naming.ldap.version", "3");
		if (this.sslFlag.equalsIgnoreCase("true")){
			env.put(Context.SECURITY_PROTOCOL,"ssl");
		}

		try {
			directoryContext = new InitialDirContext(env);
			
		} catch (NamingException e) {
			log.error("Naming Exception Error:" + e.getExplanation());		
			e.printStackTrace();
		}
		
		try {
			ldapContext = new InitialLdapContext(env, null);
			log.info("Connection to OID Instance Successful");
		} catch (NamingException e) {
			log.error("Naming Exception Error:" + e.getExplanation());	
			e.printStackTrace();
		}
		
		
	}
	
	/**
	 * Disconnects from OID
	 */
	private void disconnectFromOID() {
		log.debug(CLASS_NAME + ".disconnectFromOID() ----->"); 
		
		try {
			directoryContext.close();
			log.info("Disconnect from OID Instance Successful");
		} catch (NamingException e) {
			log.error("Naming Exception Error:" + e.getExplanation());	
			e.printStackTrace();
		}
		
	}
	
	/**
	 * Adds an attributes to a user
	 * @param container - container of the users
	 * @param user - cn of user
	 * @param attribute - attribute name
	 * @param value - new value
	 * @return
	 * @throws Exception
	 */
	public String addAttributes(String user, String attribute, String value){
		String response = "";
		log.debug(CLASS_NAME + ".addAttributes() -----> inputs=" +
				"inputs=user[" + user + "]" +
				"inputs=attribute[" + attribute + "]" +
				"inputs=value[" + value + "]"); 
		
		String userDN = "cn=" + user + "," + rootContext;
		
		connectToOID();
		
		BasicAttributes basicattributes = new BasicAttributes(true);
		basicattributes.put(new BasicAttribute(attribute, value));

		try {
			directoryContext.modifyAttributes(userDN, DirContext.ADD_ATTRIBUTE, basicattributes);
			
			response = "SUCCESS";
		}catch (NamingException e) {
			log.error("NamingException:" + e.getExplanation());
			e.printStackTrace();
			response = "ERROR";
		}
		disconnectFromOID();
		return response;
	}
	
	/**
	 * Modifies an attributes of a user
	 * @param container - container of the users
	 * @param user - cn of user
	 * @param attribute - attribute name
	 * @param value - new value
	 * @return
	 */
	public String modifyAttributes(String container, String user, String attribute, String value){
		String response = "";
		log.debug(CLASS_NAME + ".addAttributes() -----> inputs=container[" + container + "]" +
				"inputs=user[" + user + "]" +
				"inputs=attribute[" + attribute + "]" +
				"inputs=value[" + value + "]"); 
		
		String userDN = "cn=" + user + "," + container + ","+ rootContext;
		
		connectToOID();
		
		BasicAttributes basicattributes = new BasicAttributes(true);
		basicattributes.put(new BasicAttribute(attribute, value));

		try {
			directoryContext.modifyAttributes(userDN, DirContext.REPLACE_ATTRIBUTE, basicattributes);	
			response = "ATTRIBUTE_MODIFIED";
		}catch (NamingException e) {
			log.error("NamingException:" + e.getExplanation());
			e.printStackTrace();
			response = "ERROR_MODIFYING_ATTRIBUTE";
		}
		disconnectFromOID();
		return response;
	}
	
	/**
	 * Removes an attributes from a user
	 * @param container - container of the users
	 * @param user - cn of user
	 * @param attribute - attribute name
	 * @param value - value not required
	 * @return
	 */
	public String removeAttributes(String user, String attribute, String value){
		String response = "";
		log.debug(CLASS_NAME + ".addAttributes() -----> inputs=" +
				"inputs=user[" + user + "]" +
				"inputs=attribute[" + attribute + "]" +
				"inputs=value[" + value + "]"); 
		
		String userDN = "cn=" + user + ","+ rootContext;
		
		connectToOID();
		
		BasicAttributes basicattributes = new BasicAttributes(true);
		basicattributes.put(new BasicAttribute(attribute, null));

		try {
			directoryContext.modifyAttributes(userDN, DirContext.REMOVE_ATTRIBUTE, basicattributes);	
			response = "SUCCESS";
		}catch (NamingException e) {
			log.error("NamingException:" + e.getExplanation());
			e.printStackTrace();
			response = "ERROR";
		}
		disconnectFromOID();
		return response;
	}
	/**
	 * Deletes an attribute from a user
	 * @param container - container of the users
	 * @param user - cn of user
	 * @param attribute - attribute name
	 * @param value - new value
	 * @return
	 */
	public String deleteAttributes(String container, String user, String attribute, String value)  {
		String response = "";
		log.debug(CLASS_NAME + ".deleteAttributes() -----> inputs=container[" + container + "]" +
				"inputs=user[" + user + "]" +
				"inputs=attribute[" + attribute + "]" +
				"inputs=value[" + value + "]"); 
		
		String userDN = "cn=" + user + "," + container + ","+ rootContext;
		
		connectToOID();
		
		
		BasicAttributes basicattributes = new BasicAttributes(true);
		basicattributes.put(new BasicAttribute(attribute, value));

		try {
			directoryContext.modifyAttributes(userDN, DirContext.REMOVE_ATTRIBUTE, basicattributes);	
			response = "ATTRIBUTE_REMOVED";
		}catch (NamingException e) {
			log.error("NamingException:" + e.getExplanation());
			e.printStackTrace();
			response = "ERROR_REMOVING_ATTRIBUTE";
		}
		disconnectFromOID();
		return response;
	}


    /**
     * Searches based on given search string to determine if user exists or not
     * @param container = container to search
     * @param search = search string like "cn=username"
     * @return
     * @throws Exception
     */
    public String getAttribute(String container, String search)     throws Exception {              
            String response = "";
            log.debug(CLASS_NAME + ".getAttribute() -----> inputs=container[" + container + "]" +
                    "inputs=search[" + search + "]"); 
            
            connectToOID();
            SearchControls ctls = new SearchControls();
            ctls.setSearchScope(SearchControls.SUBTREE_SCOPE);
            
            if (container.length()!=0){
                    rootContext = container + "," + rootContext;
            }
            try
            {                       
                    NamingEnumeration answer = ldapContext.search(rootContext, search, ctls);
                    if (answer.hasMoreElements()){
                    response = "USER_FOUND";
                    }else{
                            //log.debug("No entries found");
                            response = "USER_NOT_FOUND";
                    }

            }
            catch (NamingException e)
            {
            e.printStackTrace();
            }               
            disconnectFromOID();
            return response;
    }
        

    public String getEmplId(String container, String search) throws Exception {              
        return this.getLDAPAttrValue(container,search,"cunyeduemplid");
    }
    
    /**
     * Searches based on given search string.  Returns the attrName value
     * @param container = container to search for users
     * @param search = search string like "cn=username"
     * @param attrName = attrName in LDAP target to return the value of
     * @return
     * @throws Exception
     */
	public String getLDAPAttrValue(String container, String search, String attrName) throws Exception {		
		String response = "";
		log.debug(CLASS_NAME + ".getEmplId() -----> inputs=container[" + container + "]" +
			"inputs=search[" + search + "]"); 
		
		connectToOID();
		SearchControls ctls = new SearchControls();
		ctls.setSearchScope(SearchControls.SUBTREE_SCOPE);
		
		if (container.length()!=0){
			rootContext = container + "," + rootContext;
		}
		try
		{			
			NamingEnumeration answer = ldapContext.search(rootContext, search, ctls);
                        
                        if (answer.hasMoreElements()) {
                            
                            SearchResult sr = (SearchResult)answer.next();
                            Attributes attr = sr.getAttributes();
                            
                            for (NamingEnumeration ne = attr.getAll(); ne.hasMore(); ) {

                                Attribute att = (Attribute)ne.next();
                                String attrId = att.getID();
                                
                                if (attrName.equalsIgnoreCase(attrId)) {
                                    for (Enumeration vals = att.getAll(); vals.hasMoreElements(); ) {
                                        response = (String)vals.nextElement();
                                    }
                                }      
                            }
                        }
		}
		catch (NamingException e)
		{
		e.printStackTrace();
		}		
		disconnectFromOID();
		return response;
	}

	/**
	 * NOT USED
	 * @param container
	 * @param search
	 * @param attribute
	 * @return
	 * @throws Exception
	 */
	public Vector getUserList(String container, String search, String attribute)	throws Exception {		
		Vector users = new Vector();
		log.debug(CLASS_NAME + ".getUserList() -----> inputs=container[" + container + "]" +
			"inputs=search[" + search + "]" + 
			"attribute[" + attribute + "]"); 
		
		String query = "(&(&(objectclass=person)(" + search + ")))";
		query = "(&(objectClass=top)(modifytimestamp>=0))";
		log.debug("Query:" + query);
		connectToOID();
		SearchControls ctls = new SearchControls();
		ctls.setSearchScope(SearchControls.SUBTREE_SCOPE);
		
		if (container.length()!=0){
			rootContext = container + "," + rootContext;
		}
		
		try
		{			
			NamingEnumeration answer = ldapContext.search(rootContext, query, ctls);			
			while (answer.hasMoreElements()){
				SearchResult searchResult = (SearchResult) answer.next();					
		        Attributes attributes = searchResult.getAttributes();
		        Attribute attr = attributes.get(attribute);
		        String attrValue = (String) attr.get();		        
		        log.debug(attribute + ":" + attrValue);		        
		        users.add(attrValue);
			}
		}
		catch (NamingException e)
		{
		e.printStackTrace();
		}		
		disconnectFromOID();
		return users;
	}
	
	/**
	 * NOT USED
	 * Returns a list of all userids that match the criteria of the search string
	 * @param container - container of users
	 * @param search - search string for query
	 * @param attribute - value to return (cn)
	 * @return
	 * @throws Exception
	 */
	private Vector getUserListTimeStamp(String container, String timeStamp, String attribute)	throws Exception {		
		Vector users = new Vector();
		log.debug(CLASS_NAME + ".getUserListTimeStamp() -----> inputs=container[" + container + "]" +
			"inputs=timeStamp[" + timeStamp + "]" + 
			"attribute[" + attribute + "]"); 
		
		String query = "(&(&(objectclass=person)(" + timeStamp  + ">=" + get120DaysPrior() + ")))";
		//query = "(&(objectClass=top)(modifytimestamp>=0))";
		log.debug("Query:" + query);
		connectToOID();
		SearchControls ctls = new SearchControls();
		ctls.setSearchScope(SearchControls.SUBTREE_SCOPE);
		
		if (container.length()!=0){
			rootContext = container + "," + rootContext;
		}
		
		try
		{			
			NamingEnumeration answer = ldapContext.search(rootContext, query, ctls);			
			while (answer.hasMoreElements()){
				SearchResult searchResult = (SearchResult) answer.next();					
		        Attributes attributes = searchResult.getAttributes();
		        Attribute attr = attributes.get(attribute);
		        String attrValue1 = (String) attr.get();		        
		        log.debug(attribute + ":" + attrValue1);	
		        attr = attributes.get("cn");
		        String attrValue2 = (String) attr.get();
		        log.debug(attrValue2);
		        users.add(attrValue1);
			}
		}
		catch (NamingException e)
		{
		e.printStackTrace();
		}		
		disconnectFromOID();
		return users;
	}
	
	/**
	 * NOT USED
	 * @param container
	 */
	private void createUser(String container){
		int counter = 100;
		
		connectToOID();		
		for (int i=1;i<=counter;i++){
			BasicAttributes basicattributes = new BasicAttributes(true);
			basicattributes.put(new BasicAttribute("objectclass", "top"));
			basicattributes.put(new BasicAttribute("objectclass", "person"));
			basicattributes.put(new BasicAttribute("objectclass", "organizationalPerson"));			
			basicattributes.put(new BasicAttribute("objectclass", "inetorgperson"));			
			
			basicattributes.put(new BasicAttribute("uid", "AUTOUSER" + String.valueOf(i)));
			basicattributes.put(new BasicAttribute("givenName", "Auto" + String.valueOf(i)));
			basicattributes.put(new BasicAttribute("sn", "User" + String.valueOf(i)));
			basicattributes.put(new BasicAttribute("userPassword", "Passw0rd"));
			//basicattributes.put(new BasicAttribute("cn", "Auto" + String.valueOf(i) + " " + "User" + String.valueOf(i)));
						
			String dn = "cn=AUTOUSER" + String.valueOf(i) + "," + container + "," + rootContext;
			try {
				log.debug("Creating User:" + dn);
				directoryContext.createSubcontext(dn, basicattributes);
			} catch (NamingException e) {
				e.printStackTrace();
			}
		}		

		disconnectFromOID();
		
	}

	/**
	 * NOT USED
	 * @param password
	 */
	private void pwdCheck(String password){
		password = "aAsdfsadfsdaf###";
		CharSequence inputStr = password;
		
		Pattern uppercase = Pattern.compile("[A-Z]");
		Matcher matcher = uppercase.matcher(inputStr);
		boolean matchFound = matcher.find();    // true
		log.debug("Upper Case:" + matchFound);
		
		Pattern numeric = Pattern.compile("[0-9]");
		matcher = numeric.matcher(inputStr);
		matchFound = matcher.find();    // true
		log.debug("Numeric:" + matchFound);
		
		Pattern special = Pattern.compile("[!@#$%^&*()\\[\\]]");
		matcher = special.matcher(inputStr);
		matchFound = matcher.find();    // true
		log.debug("Special:" + matchFound);
		
		log.debug(""+password.matches("[!@#$%^&*()\\[\\]]"));

        
		String regex1 = "^[a-zA-Z0-9_-]{6,24}";
		String regex2 = "^.*[a-zA-Z].*$";
		String regex3 = "^.*[0-9].*$";
		String regex4 = "^(\\)";
		
		//log.debug(password.matches(regex4));
		
		
	}
	
	/**
	 * Returns a string value of the date in milliseconds 120 days prior to system date
	 * @return
	 */
	public String get120DaysPrior(){		
		//format is 1225319761
		SimpleDateFormat simpledateformat = new SimpleDateFormat("MM-dd-yyyy");		
		java.util.Date currentDate = new java.util.Date(System.currentTimeMillis());
		log.debug("Todays Date:" + System.currentTimeMillis() + " " + simpledateformat.format(currentDate));	
		
		Calendar cal = new GregorianCalendar();
		cal.setTime(currentDate);
		cal.add(cal.DAY_OF_YEAR, -120);
		java.util.Date oldDate = new java.util.Date(cal.getTimeInMillis());
		log.debug("120 Days Prior:" + cal.getTimeInMillis() + " " + simpledateformat.format(oldDate));
		String value = String.valueOf(cal.getTimeInMillis());
		value = String.valueOf(cal.getTimeInMillis()).substring(0, value.length()-3);
		log.debug("Returning value:" + value);
		return (String.valueOf(cal.getTimeInMillis()));
	}
	
	/**
	 * Creates the correct date format for obPasswordCreationDate
	 * @return
	 */
	public String setPasswdCreationDate(){
		SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat sdfTime = new SimpleDateFormat("HH:mm:ss");
		java.util.Date day = new java.util.Date(System.currentTimeMillis());
		java.util.Date time = new java.util.Date(System.currentTimeMillis());
		
		String s1 = sdfDate.format(day);
		String s2 = sdfTime.format(time);
		//log.debug(s1 + "T" + s2 + "Z");
		String value = s1 + "T" + s2 + "Z";
		return value;
		
	}
}
