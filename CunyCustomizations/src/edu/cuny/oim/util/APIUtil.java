package edu.cuny.oim.util;


import Thor.API.Exceptions.tcAPIException;
import Thor.API.Exceptions.tcColumnNotFoundException;
import Thor.API.Exceptions.tcLoginAttemptsExceededException;
import Thor.API.Exceptions.tcPasswordResetAttemptsExceededException;
import Thor.API.Exceptions.tcUserAccountDisabledException;
import Thor.API.Exceptions.tcUserAccountInvalidException;
import Thor.API.Exceptions.tcUserAlreadyLoggedInException;
import Thor.API.Operations.tcITResourceInstanceOperationsIntf;
import Thor.API.Operations.tcLookupOperationsIntf;
import Thor.API.Operations.tcUserOperationsIntf;
import Thor.API.tcResultSet;

import com.thortech.util.logging.Logger;
import com.thortech.xl.crypto.tcCryptoException;

import java.io.UnsupportedEncodingException;

import java.net.URLEncoder;

import java.sql.Connection;
import java.sql.Statement;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;

import oracle.iam.identity.vo.Identity;
import oracle.iam.platform.Platform;
import oracle.iam.platform.context.ContextAware;


public class APIUtil {

    private static Logger log = Logger.getLogger("CUNY.CUSTOM");
    private static String CLASS_NAME = " APIUtil ";

    public static String itResourceOID = "OID Server";
    public static String timeStampField = "cunyedulastlogin";
    public static String lockAttribute = "oblockouttime";
    public static String isAttributeEnabled = "orclisenabled"; //set this value to disabled on lock.
    public static String lockAttributeValue =
        String.valueOf(System.currentTimeMillis());
    public static String svcLookup = "Lookup.CUNY.Service.Accounts";
    public static String OID_SERVER_IT_RESOURCE = "OID Server";


    public static void setLockoutDate(String userKey,
                                      String lockOutField) throws tcAPIException,
                                                                  tcCryptoException,
                                                                  tcUserAccountDisabledException,
                                                                  tcPasswordResetAttemptsExceededException,
                                                                  tcLoginAttemptsExceededException,
                                                                  tcUserAccountInvalidException,
                                                                  tcUserAlreadyLoggedInException,
                                                                  tcColumnNotFoundException {
        String METHOD_NAME = ".setLockoutDate.";
        log.info(CLASS_NAME + METHOD_NAME + " Entering with userKey " +
                 userKey);

        tcUserOperationsIntf userIntf =
            Platform.getService(Thor.API.Operations.tcUserOperationsIntf.class);

        Hashtable userHash = new Hashtable();
        userHash.put("Users.Key", String.valueOf(userKey));
        tcResultSet userSet = userIntf.findAllUsers(userHash);
        String locked = userSet.getStringValue("Users.Lock User");
        if (locked.equals("0")) {
            userHash.clear();
            userHash.put(lockOutField, "");
        } else {
            Calendar cal = Calendar.getInstance();
            cal.set(Calendar.HOUR_OF_DAY, 0);
            cal.set(Calendar.MINUTE, 0);
            cal.set(Calendar.SECOND, 0);
            cal.set(Calendar.MILLISECOND, 0);

            log.debug("Todays date:" + cal.getTime());
            userHash.clear();
            userHash.put(lockOutField, cal.getTime());
        }

        try {
            userIntf.updateUser(userSet, userHash);
        } catch (Exception e) {
            log.error(CLASS_NAME + METHOD_NAME + "Execute Exception ", e);
        }
    }

    public static LdapContext connectToOID(String container,
                                           String serverAddress, String port,
                                           String adminID, String adminPWD,
                                           String sslFlag) {
        log.debug(CLASS_NAME + ".connectToOID() -----> inputs=container[" +
                  container + "]");
        LdapContext ldapContext = null;

        try {
            container = URLEncoder.encode(container, "UTF-8");
        } catch (UnsupportedEncodingException e1) {
            log.error("UnsupportedEncodingException:" + e1.getMessage());
            e1.printStackTrace();
            return ldapContext;
        }
        String providerURL = "ldap://" + serverAddress + ":" + port;

        Hashtable env = new Hashtable();
        env.put(Context.INITIAL_CONTEXT_FACTORY,
                "com.sun.jndi.ldap.LdapCtxFactory");

        env.put(Context.PROVIDER_URL, providerURL);
        env.put(Context.SECURITY_PRINCIPAL, adminID);
        env.put(Context.SECURITY_CREDENTIALS, adminPWD);
        env.put(Context.REFERRAL, "ignore");
        env.put("java.naming.ldap.version", "3");
        if (sslFlag.equalsIgnoreCase("true")) {
            env.put(Context.SECURITY_PROTOCOL, "ssl");
        }

        try {
            ldapContext = new InitialLdapContext(env, null);
            log.info("Connection to OID Instance Successful");
        } catch (NamingException e) {
            log.error("Naming Exception Error:" + e.getExplanation());
            e.printStackTrace();
        }
        return ldapContext;
    }


    public static Vector getSVCUsers() {
        Vector svcUsers = new Vector();
        tcResultSet lookupSet;
        try {
            tcLookupOperationsIntf lookupIntf =
                Platform.getService(Thor.API.Operations.tcLookupOperationsIntf.class);

            lookupSet =
                    lookupIntf.getLookupValues("Lookup.CUNY.Service.Accounts");

            for (int i = 0; i < lookupSet.getTotalRowCount(); i++) {
                lookupSet.goToRow(i);
                String user =
                    lookupSet.getStringValue("Lookup Definition.Lookup Code Information.Code Key").toUpperCase();
                log.debug("Service Account User:" + user);
                svcUsers.add(user);
            }

        } catch (Exception e) {
            log.error("Exception Error:", e);
        }

        return svcUsers;
    }


    public static void updateLockAttributeOID(String userLogin,
                                              boolean lockUser) {
        log.debug(CLASS_NAME +
                  ".updateLockAttributeOID() -----> inputs=userLogin[" +
                  userLogin + "]" + " lockUser " + lockUser);
        log.debug("Updating attribute[" + lockAttribute + "]" + " to value[" +
                  lockAttributeValue + "]");
        Vector svcUsers = getSVCUsers();
        Hashtable resource =
            getITResource(OID_SERVER_IT_RESOURCE); // OIDSERVER
        String rootContext = resource.get("baseContexts").toString();
        String adminID = resource.get("principal").toString();
        String adminPWD = resource.get("credentials").toString();
        String sslFlag = resource.get("ssl").toString();
        LdapContext ldapContext =
            connectToOID(rootContext, resource.get("host").toString(),
                         resource.get("port").toString(), adminID, adminPWD,
                         sslFlag);

        String userDN = "cn=" + userLogin + "," + rootContext;
        log.debug("User:" + userDN);
        try {
            BasicAttributes basicattributes = new BasicAttributes(true);
            if (lockUser) {
                log.debug("Locking the User Account");

                if (svcUsers.contains(userLogin)) {
                    log.debug("Service account");

                    basicattributes.put(new BasicAttribute(timeStampField,
                                                           null));
                    ldapContext.modifyAttributes(userDN,
                                                 ldapContext.REMOVE_ATTRIBUTE,
                                                 basicattributes);
                } else {
                    log.debug("Locking the User Account lockAttribute " + lockAttribute);

                    basicattributes.put(new BasicAttribute(lockAttribute,
                                                           lockAttributeValue));
                    basicattributes.put(new BasicAttribute(isAttributeEnabled,"disabled"));
                    ldapContext.modifyAttributes(userDN,
                                                 ldapContext.REPLACE_ATTRIBUTE,
                                                 basicattributes);
                }
            } else {
                try {
                    basicattributes.put(new BasicAttribute(lockAttribute,
                                                           null));
                    basicattributes.put(new BasicAttribute(isAttributeEnabled, null));
                    ldapContext.modifyAttributes(userDN,
                                                 ldapContext.REMOVE_ATTRIBUTE,
                                                 basicattributes);
                } catch (NamingException e) {
                    log.error("Unable to delete :" + lockAttribute, e);
                }
                try {
                    basicattributes = new BasicAttributes(true);
                    basicattributes.put(new BasicAttribute(timeStampField,
                                                           null));
                    ldapContext.modifyAttributes(userDN,
                                                 ldapContext.REMOVE_ATTRIBUTE,
                                                 basicattributes);
                } catch (NamingException e) {
                    log.error("Unable to delete :" + timeStampField, e);
                }
            }
        } catch (NamingException e) {
            log.error("NamingException:", e);
        } finally {
            disconnectFromOID(ldapContext);
        }

    }

    public static Hashtable getITResource(String itResource) {
        Hashtable resource = new Hashtable();
        try {
            tcITResourceInstanceOperationsIntf itResOper =
                Platform.getService(tcITResourceInstanceOperationsIntf.class);
            HashMap<String, String> searchcriteria =
                new HashMap<String, String>();
            searchcriteria.put("IT Resources.Name", itResource);
            log.info("IT Resource Name " + itResource);
            tcResultSet resultSet =
                itResOper.findITResourceInstances(searchcriteria);
            log.info("Result Set Count " + resultSet.getTotalRowCount());
            tcResultSet itResSet =
                itResOper.getITResourceInstanceParameters(resultSet.getLongValue("IT Resources.Key"));
            log.info("itResuletSet Count " + itResSet.getRowCount());
            for (int i = 0; i < itResSet.getRowCount(); i++) {
                itResSet.goToRow(i);
                String paramName =
                    itResSet.getStringValue("IT Resources Type Parameter.Name");
                String paramVal =
                    itResSet.getStringValue("IT Resource.Parameter.Value");
                log.info("Parameter Name" + paramName + " VALUE: " + paramVal);
                resource.put(paramName, paramVal);
            }
        } catch (Exception e) {
            log.error("Exception ", e);

        }
        return resource;
    }

    private static void disconnectFromOID(LdapContext ldapContext) {
        log.debug(CLASS_NAME + ".disconnectFromOID() ----->");

        try {
            ldapContext.close();
            log.info("Disconnect from OID Instance Successful");
        } catch (Exception e) {
            log.error("Naming Exception Error:", e);
        }

    }
    
    /**
     * It parse the CSV line to a list.
     * @param value csv line
     * @return list of values read
     */
    public static List<String> csvToList(String value) {
        List<String> retval = new ArrayList<String>();
        if (value == null)
            return retval;

        String svalue = value.trim();
        if (svalue.length() == 0)
            return retval;

        String[] parts = svalue.split(",");
        for (String part : parts) {
            retval.add(part.trim());
        }
        return retval;
    }
    
    public static boolean checkIfDateEquals(Date dateToCheck, String dateToCOmpare, String format){
        SimpleDateFormat simpledateformat = new SimpleDateFormat(format);         
        String date = simpledateformat.format(dateToCheck);
        return dateToCOmpare.equals(date);
    }


    public static void executeUpdate(String sqlUpdateQuery) {
        String METHOD_NAME = " executeUpdate ";
        log.info("Running " + CLASS_NAME + METHOD_NAME + ".query() ----->" +
                    sqlUpdateQuery);
        Connection oimDBConnection = null;
        Statement queryStmt = null;

        log.debug(sqlUpdateQuery);

        try {
            oimDBConnection = Platform.getOperationalDS().getConnection();
            queryStmt = oimDBConnection.createStatement();
            queryStmt.executeUpdate(sqlUpdateQuery);
        } catch (Exception e) {
            log.error("Exception ", e);
        } finally {
            try {
                if (queryStmt != null) {
                    queryStmt.close();
                }
            } catch (Exception e) {
                log.error("Exception ", e);
            }
            try {
                if (oimDBConnection != null) {
                    oimDBConnection.close();
                }
            } catch (Exception e) {
                log.error("Exception ", e);
            }
        }
    }

    public static String truncateNonAlphaNumeric(String line) {
        StringBuffer convertedString = new StringBuffer();
        for (int i = 0; i < line.length(); i++) {
            char c = line.charAt(i);
            if (Character.isDigit(c) || Character.isLetter(c)) {
                convertedString.append(c);
            }
        }
        return convertedString.toString().trim();
    }

    public static boolean checkIfParameterChanged(String paramName, HashMap parameters, Identity currentUser){
        String METHOD_NAME = ".checkIfParameterChanged.";

        log.info(CLASS_NAME + METHOD_NAME + " Entering with paramName " + paramName);

        if(currentUser == null) return true;

        Object paramNewValue = null;
        Object paramOldValue = null;
        if(parameters.containsKey(paramName)){
            paramNewValue = getParameterValue(parameters, paramName);
            
            paramOldValue = currentUser.getAttribute(paramName);
            if(isOIMAttributeNull(paramOldValue) &&  isOIMAttributeNull(paramNewValue))
                return false;
            else if(isOIMAttributeNull(paramOldValue) &&  !isOIMAttributeNull(paramNewValue))
                return true;
            else if(!isOIMAttributeNull(paramOldValue) &&  isOIMAttributeNull(paramNewValue))
                return true;
            else if(!isOIMAttributeNull(paramOldValue) &&  !isOIMAttributeNull(paramNewValue)) {
                if(paramOldValue.equals(paramNewValue))
                    return false;
                else return true;
            }

        }
        return false;
    }

    public static String getParameterValue(HashMap parameters, String key) {
        Object valueObject = null;
        if(parameters.get(key) instanceof ContextAware){
            valueObject = ((ContextAware)parameters.get(key)).getObjectValue();
        } else {
            valueObject = parameters.get(key);
        }
        if(valueObject!=null) {
            if(valueObject instanceof String)
                return (String)valueObject;
            else return valueObject.toString();
        } else return null;
    }
    public static boolean isOIMAttributeNull(Object attrVal) {
        if(attrVal == null) return true;
        if(attrVal instanceof String) {
            String attrSt = (String)attrVal;
            return (attrSt.isEmpty() || attrSt.equalsIgnoreCase("null"));
        } else return false;
    }

    public static boolean checkIfStatusChanges(HashMap parameters, Identity currentUser) {
        String METHOD_NAME = ".checkIfStatusChanges.";
        log.info(CLASS_NAME + METHOD_NAME + " Entering with parameters " + parameters);
        boolean change = false;
        String upperKey = null;
        Set<String> keySet = (Set<String>)parameters.keySet();
        for (String key : keySet) {
            upperKey = key.toUpperCase();
            if ((upperKey.endsWith("EMP") || upperKey.endsWith("STU")) && APIUtil.checkIfParameterChanged(key, parameters, currentUser)){
                return true;
            }
        }
        return change;
    }


}
