package edu.cuny.oim.transformer;

import com.thortech.util.logging.Logger;

import java.util.HashMap;


public class PSFTTargetReconTransformation  {
    private static final String CLASS_NAME = "PSFTTargetReconTransformation";
    Logger logger = Logger.getLogger("CUNY.CUSTOM");

    public Object transform(HashMap hmUserDetails, HashMap hmEntitlementDetails, String sField) {
        String METHOD_NAME = " transform ";
        logger.info(CLASS_NAME + METHOD_NAME + ", Entering with Map: hmUserDetails" + hmUserDetails);
        String PersonDefn_OPRID = (String)hmUserDetails.get("PersonDefn OPRID");
        String PSRole_OPRID = (String)hmUserDetails.get("PSRole OPRID");
        String EMPLOYEEID = (String)hmUserDetails.get("Employee ID");
        String returnID = (String)hmUserDetails.get("Return ID");

        String User_ID = (String)hmUserDetails.get("User ID");
        if(User_ID ==null && PersonDefn_OPRID!=null) User_ID = PersonDefn_OPRID;
        else if(User_ID==null && PSRole_OPRID!=null) User_ID = PSRole_OPRID;
        else if(User_ID==null && EMPLOYEEID!=null) User_ID = EMPLOYEEID;
        else if(User_ID==null && returnID!=null) User_ID = returnID;

        logger.info(CLASS_NAME + METHOD_NAME + ", Entering with Map: hmEntitlementDetails" + hmEntitlementDetails);
        logger.info(CLASS_NAME + METHOD_NAME + ", Entering with Map: sField" + sField);
        logger.info(CLASS_NAME + METHOD_NAME + ", Returning " + User_ID);

        return User_ID;
    }
}
