/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.cuny.oim.plugin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import oracle.iam.notification.impl.NotificationEventResolver;
import oracle.iam.notification.vo.NotificationAttribute;

import com.thortech.util.logging.Logger;

/**
 *
 * @author jitendra.maheshwari
 */
public class CustomeNotificationResolver implements NotificationEventResolver {

    Logger logger = Logger.getLogger("CUNY.CUSTOM");
    private String CLASS_NAME = " CustomeNotificationResolver ";

    public List<NotificationAttribute> getAvailableData(String eventType, Map<String, Object> params) throws Exception {
        return new ArrayList<NotificationAttribute>();
    }

    /**
     * It returns the replaced data.
     */
    public HashMap<String, Object> getReplacedData(String eventType, Map<String, Object> params) throws Exception {
        String METHOD_NAME = " GetReplaceData ";
        logger.info(CLASS_NAME + METHOD_NAME + " Entering with params" + params);

        HashMap<String, Object> resolvedData = new HashMap<String, Object>();
        Object dataVal = null;

        for (String key : params.keySet()) {
            logger.info(CLASS_NAME + METHOD_NAME + "key = " + key);

            if (params.containsKey(key)) {
                dataVal = params.get(key);
                if (dataVal != null) {
                    logger.info(CLASS_NAME + METHOD_NAME + "key = " + key + " Value   " + dataVal);
                    key = key.replace(' ', '_');
                    resolvedData.put(key, dataVal);

                }
            }
        }
        return resolvedData;
    }
}
