package edu.cuny.oim.plugin;


import Thor.API.Operations.tcITResourceInstanceOperationsIntf;
import Thor.API.tcResultSet;

import com.sun.jndi.ldap.ctl.PagedResultsControl;

import com.thortech.util.logging.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.Control;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;

import oracle.iam.identity.exception.UserSearchException;
import oracle.iam.identity.usermgmt.api.UserManager;
import oracle.iam.identity.usermgmt.api.UserManagerConstants;
import oracle.iam.identity.usermgmt.api.UserNamePolicy;
import oracle.iam.identity.usermgmt.vo.User;
import oracle.iam.platform.Platform;
import oracle.iam.platform.entitymgr.vo.SearchCriteria;


public class GenerateUserLogin implements UserNamePolicy {
    private static final String CLASS_NAME = "GenerateUserLogin";
    Logger logger = Logger.getLogger("CUNY.CUSTOM");


    private static List<String> userIdList = new ArrayList<String>();

    public String getUserNameFromPolicy(Map map) {
        String METHOD_NAME = " getUserNameFromPolicy ";
        logger.info(CLASS_NAME + METHOD_NAME + ", Entering with Map: " + map);
        String firstName = null;
        String lastName = null;
        String employeeId = null;
        String userid = null;

        //get FirstName
        if (map.get("First Name") != null) {
            firstName =
                    map.get("First Name").toString().trim().replace(" ", "");
            logger.info(CLASS_NAME + METHOD_NAME + "First Name " + firstName);
        }


        //get LastName
        if (map.get("Last Name") != null) {
            lastName = map.get("Last Name").toString().replace(" ", "").trim();
            logger.info(CLASS_NAME + METHOD_NAME + "Last Name " + lastName);
        }


        //get UserManagerConstants.AttributeName.EMPLOYEE_NUMBER.getId()
        if (map.get(UserManagerConstants.AttributeName.EMPLOYEE_NUMBER.getId()) !=
            null) {
            employeeId =
                    map.get(UserManagerConstants.AttributeName.EMPLOYEE_NUMBER.getId()).toString().replace(" ",
                                                                                                           "").trim();
            logger.info(CLASS_NAME + METHOD_NAME + "Employee Number " +
                        employeeId);
        }

        userid = generateOIMUserID(firstName, lastName, employeeId);
        return userid.toString().toLowerCase().trim();
    }


    /*********************************************************************************************************************************************************
     * Function/Method       : generateOIMUserID
     * Function Description  : User defined public String method implemented to generate a unique User Login with 10 characters.
     * Arguments             :
     *          INPUT        : firstName,lastName(String)
     * Return Value          : String
     * Assumptions           : NA
     * Exception             :tcColumnNotFoundException,Exception
     * Creation Date         :
     * Author                :
     * Note                  : None
     **********************************************************************************************************************************************************/


    public String generateOIMUserID(String firstName, String lastName,
                                    String employeeId) {
        String METHOD_NAME = " generateOIMUserID ";
        logger.info(CLASS_NAME + METHOD_NAME + "Entering with firstName = " +
                    firstName + " lastName = " + lastName + " employeeId " +
                    employeeId);

        long time = System.currentTimeMillis();

        boolean userIDInUse = false;
        String userId = null;

        if (!isOIMAttributeNull(firstName)) {
            firstName = truncateNonAlphaNumeric(firstName);
        }

        if (!isOIMAttributeNull(employeeId)) {
            employeeId = truncateNonAlphaNumeric(employeeId);
        }

        if (!isOIMAttributeNull(lastName)) {
            lastName = truncateNonAlphaNumeric(lastName);
        }

        logger.info(CLASS_NAME + METHOD_NAME + " firstName = " + firstName +
                    " lastName = " + lastName);
        Hashtable oidITResource =  getITResource();
        if (!isOIMAttributeNull(firstName) && !isOIMAttributeNull(lastName) &&
            !isOIMAttributeNull(employeeId)) {
            int count = 2;
            userIDInUse = true;

            while (userIDInUse) {
                userId =
                        firstName + "." + lastName + employeeId.substring(employeeId.length() -
                                                                          count);
                logger.info("userId :" + userId);
                boolean userExistsInOID = findOIDUser(oidITResource.get("host").toString(), oidITResource.get("port").toString(),
                               oidITResource.get("baseContexts").toString(), oidITResource.get("principal").toString(),
                               oidITResource.get("credentials").toString(), oidITResource.get("ssl").toString(),
                               userId);
                logger.info("userExistsInOID :" + userExistsInOID);

                userIDInUse =
                        oimUserExists(userId) || userIdList.contains(userId) || userExistsInOID;
                logger.info("userIDInUse :" + userIDInUse);
                count++;
                logger.info("******Time taken for next iteration =" +
                            (System.currentTimeMillis() - time));
            }
        }
        logger.info(CLASS_NAME + METHOD_NAME +
                    "******Time taken for completing and return =" +
                    (System.currentTimeMillis() - time));
        logger.info(CLASS_NAME + METHOD_NAME + ":: ArrayList ::" + userIdList);

        userIdList.add(userId);
        return userId;
    }

    public boolean oimUserExists(String Username) {
        String METHOD_NAME = "oimUserExists";
        logger.info(CLASS_NAME + METHOD_NAME + " Entering..  Username " +
                    Username);
        UserManager userManagement = Platform.getService(UserManager.class);
        SearchCriteria criteria1;
        Set<String> retAttrs = new HashSet<String>();
        retAttrs.add(UserManagerConstants.AttributeName.USER_LOGIN.getId());
        criteria1 =
                new SearchCriteria(UserManagerConstants.AttributeName.USER_LOGIN.getId(),
                                   Username, SearchCriteria.Operator.EQUAL);
        List<User> users1;
        try {
            users1 = userManagement.search(criteria1, retAttrs, null);
            if (users1 != null && users1.size() > 0) {
                return true;
            }
        } catch (UserSearchException exuserSearchException) {
            logger.info(CLASS_NAME + METHOD_NAME + "UserSearchException ",
                        exuserSearchException);
        }
        return false;
    }


    private boolean isOIMAttributeNull(String attrVal) {
        return ((attrVal == null) || (attrVal.isEmpty()) ||
                attrVal.equalsIgnoreCase("null"));
    }

    @Override
    public boolean isUserNameValid(String string, Map<String, String> map) {

        return true;
    }

    @Override
    public String getDescription(Locale locale) {
        // TODO Implement this method
        return null;
    }

    /**
     * remove non-alpha numeric from line
     * @param line
     * @return
     */
    private String truncateNonAlphaNumeric(String line) {
        StringBuffer convertedString = new StringBuffer();
        for (int i = 0; i < line.length(); i++) {
            char c = line.charAt(i);
            if (Character.isDigit(c) || Character.isLetter(c)) {
                convertedString.append(c);
            }
        }
        return convertedString.toString().trim();
    }



    private boolean findOIDUser(String serverName, String portNo,
                               String rootContext, String principalDN,
                               String principalPass, String sslFlag,
                               String uniqueUserID) {
        String GETUSERLOGIN_METHOD = " findOIDUser ";
        boolean response = false;

        try {

            logger.info(CLASS_NAME + GETUSERLOGIN_METHOD +
                        "------->serverName = " + serverName);
            logger.info(CLASS_NAME + GETUSERLOGIN_METHOD +
                        "------->portNo = " + portNo);
            logger.info(CLASS_NAME + GETUSERLOGIN_METHOD +
                        "------->rootContext = " + rootContext);
            logger.info(CLASS_NAME + GETUSERLOGIN_METHOD +
                        "------->principalDN = " + principalDN);
            logger.info(CLASS_NAME + GETUSERLOGIN_METHOD +
                        "------->uniqueUserID = " + uniqueUserID);


            String pSearchBase = new String();

            pSearchBase = rootContext;


            logger.info(CLASS_NAME + GETUSERLOGIN_METHOD +
                        "------->sslFlag = " + sslFlag);
            logger.info(CLASS_NAME + GETUSERLOGIN_METHOD +
                        "===================================\n");

            String contextFactory = "com.sun.jndi.ldap.LdapCtxFactory";

            String providerURL =
                new String("ldap://" + serverName + ":" + portNo);


            Hashtable oEnv = new Hashtable();
            oEnv.put(Context.PROVIDER_URL, providerURL);
            oEnv.put(Context.INITIAL_CONTEXT_FACTORY, contextFactory);
            oEnv.put(Context.SECURITY_PRINCIPAL, principalDN);
            oEnv.put(Context.SECURITY_CREDENTIALS, principalPass);

            oEnv.put(Context.SECURITY_AUTHENTICATION, "simple");

            if (sslFlag.equalsIgnoreCase("true")) {
                oEnv.put(Context.SECURITY_PROTOCOL, "ssl");
            }
            DirContext ctx = new InitialDirContext(oEnv);

            LdapContext ldapCtx;

            ldapCtx = new InitialLdapContext(oEnv, null);

            SearchControls searchcontrols = new SearchControls();
            searchcontrols.setCountLimit(0); // to return all the results
            searchcontrols.setSearchScope(SearchControls.SUBTREE_SCOPE);


            Vector vector = new Vector();
            SearchResult searchresult;


            String searchFilter =
                "(&(objectClass=person)(uid=" + uniqueUserID + ") )";


            logger.info(CLASS_NAME + GETUSERLOGIN_METHOD +
                        "rootcontext-------->" + rootContext);
            logger.info(CLASS_NAME + GETUSERLOGIN_METHOD +
                        "searchFilter-------->" + searchFilter);
            logger.info(CLASS_NAME + GETUSERLOGIN_METHOD +
                        "searchcontrols-------->" + searchcontrols);


            String searchAttributes[] = { "uid" };

            searchcontrols.setReturningAttributes(searchAttributes);


            int pageSize = 10000;
            byte[] cookie = null;
            Control[] ctls =
                new PagedResultsControl[] { new PagedResultsControl(pageSize) };
            ldapCtx.setRequestControls(ctls);

            for (NamingEnumeration namingenumeration =
                 ldapCtx.search(rootContext, searchFilter, searchcontrols);
                 namingenumeration.hasMore();
                 vector.addElement(searchresult)) {
                searchresult = (SearchResult)namingenumeration.next();
                searchresult.setRelative(true);


            } // end of for

            logger.info(CLASS_NAME + GETUSERLOGIN_METHOD +
                        "vector values------>" + vector);

            if (vector.size() > 0)
                response = true;
            else
                response = false;

            ldapCtx.close();
            ctx.close();
        } catch (Exception e) {
            logger.error(CLASS_NAME + GETUSERLOGIN_METHOD +
                         "Error in searching ldap::" + e.getMessage());
            e.printStackTrace();

        }


        return response;
    }
    private Hashtable getITResource() {
        String itResource="OID Server";

        Hashtable resource = new Hashtable();
        try {
            tcITResourceInstanceOperationsIntf itResOper =
                Platform.getService(tcITResourceInstanceOperationsIntf.class);
            HashMap<String, String> searchcriteria =
                new HashMap<String, String>();
            searchcriteria.put("IT Resources.Name", itResource);
            logger.info("IT Resource Name " + itResource);
            tcResultSet resultSet =
                itResOper.findITResourceInstances(searchcriteria);
            logger.info("Result Set Count " + resultSet.getTotalRowCount());
            tcResultSet itResSet =
                itResOper.getITResourceInstanceParameters(resultSet.getLongValue("IT Resources.Key"));
            logger.info("itResuletSet Count " + itResSet.getRowCount());
            for (int i = 0; i < itResSet.getRowCount(); i++) {
                itResSet.goToRow(i);
                String paramName =
                    itResSet.getStringValue("IT Resources Type Parameter.Name");
                String paramVal =
                    itResSet.getStringValue("IT Resource.Parameter.Value");
                logger.info("Parameter Name" + paramName + " VALUE: " + paramVal);
                resource.put(paramName, paramVal);
            }
        } catch (Exception e) {
            logger.error("Exception ", e);

        }
        return resource;
    }


}


