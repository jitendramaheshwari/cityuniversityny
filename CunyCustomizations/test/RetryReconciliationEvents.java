package test;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;
import java.util.Vector;

import javax.security.auth.login.LoginException;

import oracle.iam.platform.OIMClient;
import oracle.iam.reconciliation.api.EventMgmtService;
import oracle.iam.reconciliation.api.ReconOperationsService;
import oracle.iam.reconciliation.vo.EventConstants;
import oracle.iam.reconciliation.vo.ReconEvent;
import oracle.iam.reconciliation.vo.ReconSearchCriteria;
import oracle.iam.scheduler.api.SchedulerService;
import oracle.iam.scheduler.vo.JobDetails;
import oracle.iam.scheduler.vo.JobParameter;


public class RetryReconciliationEvents {

    public RetryReconciliationEvents() throws Exception {
        String orclIDMLoc = System.getenv("INSTALL_LOC");
        System.setProperty("XL.HomeDir", orclIDMLoc + "/server");
        System.setProperty("java.security.policy",
                           orclIDMLoc + "/designconsole/config/xl.policy");
        System.setProperty("java.security.auth.login.config",
                           orclIDMLoc + "/designconsole/config/authwl.conf");
        System.setProperty("XL.ExtendedErrorOptions", "TRUE");
        System.setProperty("APPSERVER_TYPE", "wls");

    }

    private static String getValueFromConfig(String prKey) {
        String val = null;
        Properties prop = new Properties();
        InputStream input = null;
        try {
            input = new FileInputStream("config.properties");
            prop.load(input);
            val = (String)prop.getProperty(prKey);
        } catch (Exception ex) {
            System.out.println("Error occured for reading from config.");
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        System.out.println("Value " + val + " Key:" + prKey);
        return val;
    }

    private Connection getOIMDBConnection() {
        Connection oimDBConnection = null;
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            oimDBConnection =
                    DriverManager.getConnection(getValueFromConfig("OIMDB_URL"),
                                                getValueFromConfig("OIM_DB_USER_NAME"),
                                                getValueFromConfig("OIM_DB_USER_PASSWD"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return oimDBConnection;
    }

    public OIMClient loginWithCustomEnv(String OIMUserName, String OIMPassword,
                                        String MANAGED_SERVER_NAMING_URL) throws LoginException {
        OIMClient client;
        Hashtable env = new Hashtable();
        env.put(OIMClient.JAVA_NAMING_FACTORY_INITIAL,
                OIMClient.WLS_CONTEXT_FACTORY);
        env.put(OIMClient.JAVA_NAMING_PROVIDER_URL, MANAGED_SERVER_NAMING_URL);
        client = new OIMClient(env);
        client.login(OIMUserName, OIMPassword.toCharArray());


        return client;
    }

    public static void main(String[] args) throws Exception {
        RetryReconciliationEvents apiTest = new RetryReconciliationEvents();
        apiTest.runJobToInitiateBatchProcess();
        apiTest.retryAllFailedReconEvents();
    }


    public <T> T getService(Class<T> serviceClass,
                            OIMClient client) throws Exception {
        return client.getService(serviceClass);
    }

    private List<String> findPendingBatch(){
        String query = "select rb_key from recon_batches where RB_BATCH_STATUS!='Completed'";
        Statement stmt = null;
        List<String> orcKeyList = new ArrayList<String>();
        ResultSet rs = null;
        Connection oimDBConnection = getOIMDBConnection();

        try {
            stmt = oimDBConnection.createStatement();

            rs = stmt.executeQuery(query);
            while (rs.next()) {
                orcKeyList.add(rs.getString("rb_key"));
            }

        } catch (Exception e1) {
            e1.printStackTrace();
        } finally {
                try {
                if (rs != null)
                    rs.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (stmt != null)
                    stmt.close();
            } catch (Exception e) {
                e.printStackTrace();
            }           
            try {
                if (oimDBConnection != null)
                    oimDBConnection.close();
            } catch (Exception e) {
                e.printStackTrace();
            }           

        }
        return orcKeyList;
    }


    private void runJobToInitiateBatchProcess() throws Exception {
    String jobName = "Retry Reconciliation Batch Job";
                
    int jobStatus = 0;

    JobDetails jobdetails = null;

    List<String> pendingBatches = findPendingBatch();
    for (String batchId : pendingBatches) {
        OIMClient   oimClient = loginWithCustomEnv(getValueFromConfig("ADMIN_USER_LOGIN"), getValueFromConfig("ADMIN_USER_PASSWORD"), getValueFromConfig("MANAGED_SERVER_URL"));


        SchedulerService schservice =
            oimClient.getService(SchedulerService.class);

        try {
            jobdetails = schservice.getJobDetail(jobName);
            HashMap<String, JobParameter> params = jobdetails.getParams();
            JobParameter jobParam = params.get("Batch ID");
            jobParam.setValue(batchId);
            params.put("Batch ID", jobParam);
            schservice.updateJob(jobdetails);
            schservice.triggerNow(jobName);

            while (jobStatus == 5) {
                try {
                    Thread.sleep(60000L);
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
                jobStatus = schservice.getStatusOfJob(jobName);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    }

    public void retryAllFailedReconEvents() {
    try {
        OIMClient oimClient =loginWithCustomEnv(getValueFromConfig("ADMIN_USER_LOGIN"), getValueFromConfig("ADMIN_USER_PASSWORD"), getValueFromConfig("MANAGED_SERVER_URL"));
        ReconOperationsService reconIntf =
            oimClient.getService(ReconOperationsService.class);
        EventMgmtService eventMgmt =
            oimClient.getService(EventMgmtService.class);
        ReconSearchCriteria crit = new ReconSearchCriteria();
        crit.addExpression(EventConstants.RECON_EVENT_STATUS,
                           "Data Received",
                           ReconSearchCriteria.Operator.EQUAL);
        Vector order = new Vector();
        order.add(EventConstants.RECON_EVENT_KEY);

        boolean noEvents = false;
        int success = 0;
        int failed = 0;
        while (!noEvents) {
            int counter = 0;
            List<ReconEvent> reconEvents =
                eventMgmt.search(crit, order, true, 0, 500);
            int size = reconEvents.size();
            for (ReconEvent reconEvent : reconEvents) {
                long reconKey = reconEvent.getReKey();
                System.out.println("************* Recon Event [" + reconKey + "] **************");
                counter++;
                try {
                    reconIntf.processReconciliationEvent(reconKey);
                    success++;
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                    crit.addExpression(EventConstants.RECON_EVENT_KEY,
                                       reconKey,
                                       ReconSearchCriteria.Operator.NOT_EQUAL);
                    failed++;
                }

                System.out.println(counter + "|" + size + "|reconKey[" +
                                   reconKey + "]Success[" + success +
                                   "]Failed[" + failed + "]");
            }

            if (size == 0)
                noEvents = true;

        }


    } catch (Exception e) {
        e.printStackTrace();
    }
    }


}

