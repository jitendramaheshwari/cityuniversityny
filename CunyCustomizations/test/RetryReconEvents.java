package test;


import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import java.util.Hashtable;
import java.util.Properties;

import javax.security.auth.login.LoginException;

import oracle.iam.platform.OIMClient;
import oracle.iam.reconciliation.api.ReconOperationsService;


public class RetryReconEvents {
    private static Connection conn = null;

    public RetryReconEvents() throws Exception {
        String orclIDMLoc = System.getenv("INSTALL_LOC");
        System.setProperty("XL.HomeDir", orclIDMLoc + "/server");
        System.setProperty("java.security.policy",
                           orclIDMLoc + "/designconsole/config/xl.policy");
        System.setProperty("java.security.auth.login.config",
                           orclIDMLoc + "/designconsole/config/authwl.conf");
        System.setProperty("XL.ExtendedErrorOptions", "TRUE");
        System.setProperty("APPSERVER_TYPE", "wls");
    }


    private static String getValueFromConfig(String prKey) {
        String val = null;
        Properties prop = new Properties();
        InputStream input = null;
        try {
            input = new FileInputStream("config.properties");
            prop.load(input);
            val = (String)prop.getProperty(prKey);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        System.out.println("Value " + val + " Key:" + prKey);
        return val;
    }

    public OIMClient loginWithCustomEnv(String OIMUserName, String OIMPassword,
                                        String MANAGED_SERVER_NAMING_URL) throws LoginException {
        OIMClient client;
        Hashtable env = new Hashtable();
        env.put(OIMClient.JAVA_NAMING_FACTORY_INITIAL,
                OIMClient.WLS_CONTEXT_FACTORY);
        env.put(OIMClient.JAVA_NAMING_PROVIDER_URL, MANAGED_SERVER_NAMING_URL);
        client = new OIMClient(env);
        client.login(OIMUserName, OIMPassword.toCharArray());
        return client;
    }

    public static void main(String[] args) throws Exception {

        RetryReconEvents apiTest = new RetryReconEvents();
        apiTest.loadChallenegeQuestionANswerFile();
    }

    public <T> T getService(Class<T> serviceClass,
                            OIMClient client) throws Exception {
        return client.getService(serviceClass);
    }

    private Connection getOIMDBConnection() {
        try {
            if (conn == null || conn.isClosed()) {

                Class.forName("oracle.jdbc.driver.OracleDriver");
                conn =
DriverManager.getConnection(getValueFromConfig("OIMDB_URL"),
                            getValueFromConfig("OIM_DB_USER_NAME"),
                            getValueFromConfig("OIM_DB_USER_PASSWD"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return conn;
    }


    public void loadChallenegeQuestionANswerFile() throws Exception {
        getOIMDBConnection();

        String sql =
            "select re_key from recon_events where re_status = 'Data Received' order by re_key asc";
        Statement stmt = null;
        ResultSet rs = null;
        long counter;
        try {
            stmt = getOIMDBConnection().createStatement();
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                counter = rs.getLong("re_key");
                String managedServerURL = "";
                if (counter % 2 == 0)
                    managedServerURL =
                            getValueFromConfig("MANAGED_SERVER_URL1");
                else
                    managedServerURL =
                            getValueFromConfig("MANAGED_SERVER_URL2");
                RetryReconEventsInternal thread1 =
                    new RetryReconEventsInternal(counter, managedServerURL,
                                                 conn);
                Thread thra1 = new Thread(thread1);
                thra1.start();
                while (thra1.activeCount() > 20) {
                    Thread.sleep(500);
                }
                System.out.println("Active: " + thra1.activeCount());


            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null)
                    rs.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (stmt != null)
                    stmt.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    class RetryReconEventsInternal implements Runnable {
        public String managedServerURL = null;
        public Connection conn = null;
        public long reKey = 0L;

        public RetryReconEventsInternal(long reKey, String managedServerURL,
                                        Connection conn) {
            this.managedServerURL = managedServerURL;
            this.conn = conn;
            this.reKey = reKey;
        }

        public void run() {
            try {
                retryRecon();
            } catch (Exception e) {
                System.out.println(" Encoding issue. " + e.getMessage());
            }
        }

        public void retryRecon() throws Exception {
            OIMClient oimClient =
                loginWithCustomEnv(getValueFromConfig("ADMIN_USER_LOGIN"),
                                   getValueFromConfig("ADMIN_USER_PASSWORD"),
                                   managedServerURL);
            ReconOperationsService reconOp =
                oimClient.getService(ReconOperationsService.class);
            reconOp.processReconciliationEvent(reKey);


        }


    }

}

