//package test;


import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.security.auth.login.LoginException;

import oracle.iam.identity.usermgmt.vo.User;
import oracle.iam.platform.OIMClient;


public class PostLoadUpdateUserAttributes {
    private static Pattern p =
        Pattern.compile("(^Z{3,}[^Z]+)|([^Z]+Z{3,}$)|(^H{3,}[^H]+)|([^H]+H{3,}$)",
                        Pattern.CASE_INSENSITIVE);
    
    private static Connection oimConn = null;

    public PostLoadUpdateUserAttributes() throws Exception {
        String orclIDMLoc = System.getenv("INSTALL_LOC");
        System.setProperty("XL.HomeDir", orclIDMLoc + "/server");
        System.setProperty("java.security.policy",
                           orclIDMLoc + "/designconsole/config/xl.policy");
        System.setProperty("java.security.auth.login.config",
                           orclIDMLoc + "/designconsole/config/authwl.conf");
        System.setProperty("XL.ExtendedErrorOptions", "TRUE");
        System.setProperty("APPSERVER_TYPE", "wls");

    }

    private static String getValueFromConfig(String prKey) {
        String val = null;
        Properties prop = new Properties();
        InputStream input = null;
        try {
            input = new FileInputStream("config.properties");
            prop.load(input);
            val = (String)prop.getProperty(prKey);
        } catch (Exception ex) {
            System.out.println("Error occured for reading from config.");
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        System.out.println("Value " + val + " Key:" + prKey);
        return val;
    }

    private Connection getOIMDBConnection() {
        try {
            if (oimConn == null || oimConn.isClosed()) {

                Class.forName("oracle.jdbc.driver.OracleDriver");
                oimConn =
                        DriverManager.getConnection(getValueFromConfig("OIMDB_URL"),
                                                    getValueFromConfig("OIM_DB_USER_NAME"),
                                                    getValueFromConfig("OIM_DB_USER_PASSWD"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return oimConn;
    }

    public OIMClient loginWithCustomEnv(String OIMUserName, String OIMPassword,
                                        String MANAGED_SERVER_NAMING_URL) throws LoginException {
        OIMClient client;
        Hashtable env = new Hashtable();
        env.put(OIMClient.JAVA_NAMING_FACTORY_INITIAL,
                OIMClient.WLS_CONTEXT_FACTORY);
        env.put(OIMClient.JAVA_NAMING_PROVIDER_URL, MANAGED_SERVER_NAMING_URL);
        client = new OIMClient(env);
        client.login(OIMUserName, OIMPassword.toCharArray());


        return client;
    }

    public static void main(String[] args) throws Exception {
        PostLoadUpdateUserAttributes apiTest = new PostLoadUpdateUserAttributes();
        apiTest.updateUserAttributes();
    }


    public <T> T getService(Class<T> serviceClass,
                            OIMClient client) throws Exception {
        return client.getService(serviceClass);
    }


    public void updateUserAttributes() throws Exception {
        
        String query = "select usr_key, usr_first_name, usr_last_name, USR_UDF_BAR01_EMP,USR_UDF_BAR01_STU,USR_UDF_BCC01_EMP,USR_UDF_BCC01_STU,USR_UDF_BKL01_EMP," +
            "USR_UDF_BKL01_STU,USR_UDF_BMC01_EMP,USR_UDF_BMC01_STU,USR_UDF_COCOM_EMP,USR_UDF_COSEN_EMP,USR_UDF_CSI01_EMP,USR_UDF_CSI01_STU,USR_UDF_CTY01_EMP,USR_UDF_CTY01_STU,USR_UDF_GRD01_EMP," +
            "USR_UDF_GRD01_STU,USR_UDF_GUTTMANCCEMP,USR_UDF_GUTTMANCCSTU,USR_UDF_HCS01_EMP,USR_UDF_HCS01_STU,USR_UDF_HOS01_EMP,USR_UDF_HOS01_STU,USR_UDF_HTR01_EMP,USR_UDF_HTR01_STU,USR_UDF_JJC01_EMP," +
            "USR_UDF_JJC01_STU,USR_UDF_KCC01_EMP,USR_UDF_KCC01_STU,USR_UDF_LAG01_EMP,USR_UDF_LAG01_STU,USR_UDF_LAW01_EMP,USR_UDF_LAW01_STU,USR_UDF_LEH01_EMP,USR_UDF_LEH01_STU,USR_UDF_MEC01_EMP," +
            "USR_UDF_MEC01_STU,USR_UDF_MED01_EMP,USR_UDF_MED01_STU,USR_UDF_MHC01_EMP,USR_UDF_MHC01_STU,USR_UDF_NYT01_EMP,USR_UDF_NYT01_STU,USR_UDF_QCC01_EMP,USR_UDF_QCC01_STU,USR_UDF_QNS01_EMP," +
            "USR_UDF_QNS01_STU,USR_UDF_SOJ01_EMP,USR_UDF_SOJ01_STU,USR_UDF_SPH01_EMP,USR_UDF_SPH01_STU,USR_UDF_SPS01_EMP,USR_UDF_SPS01_STU,USR_UDF_YORKCOLLEGESTU,USR_UDF_YRK01_EMP from usr";
        Statement stmt = null;
        ResultSet rs = null;
        String userKey =null;
        String firstName = null;
        String lastName = null;
        boolean duplicateUser = false;
        List<String> studentStatusList = null;
        List<String> employeeStatusList = null;
        try {
            stmt = getOIMDBConnection().createStatement();
            rs = stmt.executeQuery(query);
            while (rs.next()) {
                studentStatusList = new ArrayList<java.lang.String>();
                employeeStatusList = new ArrayList<java.lang.String>();
                studentStatusList.add(rs.getString("USR_UDF_BAR01_STU"));
                studentStatusList.add(rs.getString("USR_UDF_BCC01_STU"));
                studentStatusList.add(rs.getString("USR_UDF_BKL01_STU"));
                studentStatusList.add(rs.getString("USR_UDF_BMC01_STU"));
                studentStatusList.add(rs.getString("USR_UDF_CSI01_STU"));
                studentStatusList.add(rs.getString("USR_UDF_CTY01_STU"));
                studentStatusList.add(rs.getString("USR_UDF_GRD01_STU"));
                studentStatusList.add(rs.getString("USR_UDF_GUTTMANCCSTU"));
                studentStatusList.add(rs.getString("USR_UDF_HCS01_STU"));
                studentStatusList.add(rs.getString("USR_UDF_HOS01_STU"));
                studentStatusList.add(rs.getString("USR_UDF_HTR01_STU"));
                studentStatusList.add(rs.getString("USR_UDF_JJC01_STU"));
                studentStatusList.add(rs.getString("USR_UDF_KCC01_STU"));
                studentStatusList.add(rs.getString("USR_UDF_LAG01_STU"));
                studentStatusList.add(rs.getString("USR_UDF_LAW01_STU"));
                studentStatusList.add(rs.getString("USR_UDF_LEH01_STU"));
                studentStatusList.add(rs.getString("USR_UDF_MEC01_STU"));
                studentStatusList.add(rs.getString("USR_UDF_MED01_STU"));
                studentStatusList.add(rs.getString("USR_UDF_MHC01_STU"));
                studentStatusList.add(rs.getString("USR_UDF_NYT01_STU"));
                studentStatusList.add(rs.getString("USR_UDF_QCC01_STU"));
                studentStatusList.add(rs.getString("USR_UDF_QNS01_STU"));
                studentStatusList.add(rs.getString("USR_UDF_SOJ01_STU"));
                studentStatusList.add(rs.getString("USR_UDF_SPH01_STU"));
                studentStatusList.add(rs.getString("USR_UDF_SPS01_STU"));
                studentStatusList.add(rs.getString("USR_UDF_YORKCOLLEGESTU"));

                employeeStatusList.add(rs.getString("USR_UDF_BAR01_EMP"));
                employeeStatusList.add(rs.getString("USR_UDF_BCC01_EMP"));
                employeeStatusList.add(rs.getString("USR_UDF_BKL01_EMP"));
                employeeStatusList.add(rs.getString("USR_UDF_BMC01_EMP"));
                employeeStatusList.add(rs.getString("USR_UDF_COCOM_EMP"));
                employeeStatusList.add(rs.getString("USR_UDF_COSEN_EMP"));
                employeeStatusList.add(rs.getString("USR_UDF_CSI01_EMP"));
                employeeStatusList.add(rs.getString("USR_UDF_CTY01_EMP"));
                employeeStatusList.add(rs.getString("USR_UDF_GRD01_EMP"));
                employeeStatusList.add(rs.getString("USR_UDF_GUTTMANCCEMP"));
                employeeStatusList.add(rs.getString("USR_UDF_HCS01_EMP"));
                employeeStatusList.add(rs.getString("USR_UDF_HOS01_EMP"));
                employeeStatusList.add(rs.getString("USR_UDF_HTR01_EMP"));
                employeeStatusList.add(rs.getString("USR_UDF_JJC01_EMP"));
                employeeStatusList.add(rs.getString("USR_UDF_KCC01_EMP"));
                employeeStatusList.add(rs.getString("USR_UDF_LAG01_EMP"));
                employeeStatusList.add(rs.getString("USR_UDF_LAW01_EMP"));
                employeeStatusList.add(rs.getString("USR_UDF_LEH01_EMP"));
                employeeStatusList.add(rs.getString("USR_UDF_MEC01_EMP"));
                employeeStatusList.add(rs.getString("USR_UDF_MED01_EMP"));
                employeeStatusList.add(rs.getString("USR_UDF_MHC01_EMP"));
                employeeStatusList.add(rs.getString("USR_UDF_NYT01_EMP"));
                employeeStatusList.add(rs.getString("USR_UDF_QCC01_EMP"));
                employeeStatusList.add(rs.getString("USR_UDF_QNS01_EMP"));
                employeeStatusList.add(rs.getString("USR_UDF_SOJ01_EMP"));
                employeeStatusList.add(rs.getString("USR_UDF_SPH01_EMP"));
                employeeStatusList.add(rs.getString("USR_UDF_SPS01_EMP"));
                employeeStatusList.add(rs.getString("USR_UDF_YRK01_EMP"));


                userKey = rs.getString("USR_KEY");
                firstName = rs.getString("usr_first_name");
                lastName = rs.getString("usr_last_name");
                duplicateUser = isUserDuplicate(firstName, lastName);
                
                PostLoadUpdateUserAttributesInternal thread1 = new PostLoadUpdateUserAttributesInternal(userKey,duplicateUser, studentStatusList, employeeStatusList);
                Thread thra1 = new Thread(thread1);
                thra1.start();
                while(thra1.activeCount() > 500) {
                    Thread.sleep(5000);
                    
                    // It will change the login to other node as the previous node must be full.
                    //oimClient = loginWithCustomEnv(getValueFromConfig("ADMIN_USER_LOGIN"), getValueFromConfig("ADMIN_USER_PASSWORD"), getValueFromConfig("MANAGED_SERVER_URL"));
                    //usrMgr = (UserManager)oimClient.getService(UserManager.class);
                }
                System.out.println("Active: " + thra1.activeCount());
            
            }
        } catch (Exception e1) {
            System.out.println("Failed query " + query);
            e1.printStackTrace();
        } finally {
            try {
                if (rs != null)
                    rs.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (stmt != null)
                    stmt.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (oimConn != null)
                    oimConn.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }



    private boolean isUserDuplicate(String firstName, String lastName) {
        if (firstName == null)
            firstName = "";
        if (lastName == null)
            lastName = "";

        Matcher mfirst = p.matcher(firstName);
        Matcher mlast = p.matcher(lastName);

        return (mfirst.matches() || mlast.matches());

    }

    class PostLoadUpdateUserAttributesInternal implements Runnable {
        public String userKey = null;
        public boolean duplicateUser = false;
        public List<String> studentStatusList = null;
        public List<String> employeeStatusList = null;
        public PostLoadUpdateUserAttributesInternal(String userKey, boolean duplicateUser, List<String> studentStatusList, List<String> employeeStatusList){
            this.userKey = userKey;
            this.duplicateUser = duplicateUser;
            this.studentStatusList = studentStatusList;
            this.employeeStatusList = employeeStatusList;
        }
        
        public void run() {
            setGlobalStatusUpdatedCode();
        }
        
        private void updateUserRecord(String userKey, String studentStatus, String employeeStatus, String globalStatus, String deceased, String duplicate){
            String updateSQL = "update usr set usr_udf_GLBL_STU_STATUS = ?, usr_udf_GLBL_EMP_STATUS=?, usr_udf_GLBL_STATUS=?, usr_udf_IsDeceasedUser=?, usr_udf_IsDuplicateUser = ? where usr_key = ?";
            PreparedStatement stmt = null;
            try {
                stmt = getOIMDBConnection().prepareStatement(updateSQL);
                stmt.setString(1, studentStatus);
                stmt.setString(2, employeeStatus);
                stmt.setString(3, globalStatus);
                stmt.setString(4, deceased);
                stmt.setString(5, duplicate);
                stmt.setString(6, userKey);
                stmt.executeUpdate();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    if (stmt != null)
                        stmt.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        
        private void setGlobalStatusUpdatedCode() {
            String METHOD_NAME = " setGlobalStatusUpdatedCode ";

            System.out.println(METHOD_NAME + "setGlobalStatusUpdatedCode() Parameter Variables passed are:" +
                         "userKey=[" + userKey + "] ");

            String employeeStatus = "";
            String studentStatus = "";
            String globalStatus = "";
            boolean isUserDeceased = false;

            try {
                //Find status for employees
                if (employeeStatusList.contains("A") ||
                    employeeStatusList.contains("L") ||
                    employeeStatusList.contains("P") ||
                    employeeStatusList.contains("S") ||
                    employeeStatusList.contains("W")) {
                    employeeStatus = "Active";
                } else {
                    if (employeeStatusList.contains("R") ||
                        employeeStatusList.contains("Q")) {
                        employeeStatus = "Retired";
                    } else if (employeeStatusList.contains("T") ||
                               employeeStatusList.contains("U") ||
                               employeeStatusList.contains("V") ||
                               employeeStatusList.contains("X")) {
                        employeeStatus = "Inactive";
                    } else if (employeeStatusList.contains("D")) {
                        employeeStatus = "Inactive";
                        isUserDeceased = true;
                    }
                }
                System.out.println(METHOD_NAME  + "employeeStatus:" + employeeStatus);

                //Find status for Students
                if (studentStatusList.contains("AC") ||
                    studentStatusList.contains("AD") ||
                    studentStatusList.contains("AP") ||
                    studentStatusList.contains("CM") ||
                    studentStatusList.contains("CN") ||
                    studentStatusList.contains("DC") ||
                    studentStatusList.contains("DM") ||
                    studentStatusList.contains("LA") ||
                    studentStatusList.contains("PM") ||
                    studentStatusList.contains("SP") ||
                    studentStatusList.contains("WT")) {
                    studentStatus = "Active";
                } else if (studentStatusList.contains("DE")) {
                    studentStatus = "Inactive";
                    isUserDeceased = true;
                }
                System.out.println(METHOD_NAME  + "studentStatus:" + studentStatus);

                if (studentStatus.length() != 0) {
                    globalStatus = studentStatus;
                } else {
                    globalStatus = employeeStatus;
                }
                System.out.println(METHOD_NAME  + METHOD_NAME + "globalStatus:" + globalStatus);            
                
                HashMap<String, Object> atrrMap = new HashMap<String, Object>();
                String deceased = "FALSE";
                if(isUserDeceased) deceased = "TRUE";

                String duplicate = "FALSE";
                if (duplicateUser) duplicate = "TRUE";

                atrrMap.put("GLBL_STU_STATUS", studentStatus);
                atrrMap.put("GLBL_EMP_STATUS", employeeStatus);
                atrrMap.put("GLBL_STATUS", globalStatus);
                atrrMap.put("IsDeceasedUser", deceased);
                atrrMap.put("IsDuplicateUser", duplicate);
                
                User user = new User(userKey, atrrMap);
                //usrMgr.modify(user);
                updateUserRecord(userKey, studentStatus, employeeStatus, globalStatus, deceased, duplicate);
                
                System.out.println("Updated atrrMap " + atrrMap);
            } catch (Exception e) {
                System.out.println(METHOD_NAME + "Exception "+ e);
            }
        }


    }

}


