package test;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;


public class DeleteUsers {

    public DeleteUsers() throws Exception {
        String orclIDMLoc = System.getenv("INSTALL_LOC");
        System.setProperty("XL.HomeDir", orclIDMLoc + "/server");
        System.setProperty("java.security.policy",
                           orclIDMLoc + "/designconsole/config/xl.policy");
        System.setProperty("java.security.auth.login.config",
                           orclIDMLoc + "/designconsole/config/authwl.conf");
        System.setProperty("XL.ExtendedErrorOptions", "TRUE");
        System.setProperty("APPSERVER_TYPE", "wls");

    }

    private static String getValueFromConfig(String prKey) {
        String val = null;
        Properties prop = new Properties();
        InputStream input = null;
        try {
            input = new FileInputStream("config.properties");
            prop.load(input);
            val = (String)prop.getProperty(prKey);
        } catch (Exception ex) {
            System.out.println("Error occured for reading from config.");
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        System.out.println("Value " + val + " Key:" + prKey);
        return val;
    }

    private Connection getOIMDBConnection() {
        Connection oimDBConnection = null;
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            oimDBConnection =
                    DriverManager.getConnection(getValueFromConfig("OIMDB_URL"),
                                                getValueFromConfig("OIM_DB_USER_NAME"),
                                                getValueFromConfig("OIM_DB_USER_PASSWD"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return oimDBConnection;
    }


    public static void main(String[] args) throws Exception {
        DeleteUsers apiTest = new DeleteUsers();
        apiTest.runReconDelete();
    }

    public void executeQuery(String query, Connection con) {
        Statement stmt = null;
        try {
            stmt = con.createStatement();
            stmt.execute(query);
        } catch (Exception e1) {
            System.out.println("Failed query " + query);
            e1.printStackTrace();
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void executeListQuery(List<String> query, Connection con) {
        for (String q : query) {
            executeQuery(q, con);
        }
    }

    public List<String> returnQueryResult(String query, Connection con) {
        Statement stmt = null;
        ResultSet rs = null;
        List<String> retList = new ArrayList<String>();
        try {
            stmt = con.createStatement();
            rs = stmt.executeQuery(query);
            while (rs.next()) {
                retList.add(rs.getString(1));
            }
        } catch (Exception e1) {
            System.out.println("Failed query " + query);
            e1.printStackTrace();
        } finally {
            try {
                if (rs != null)
                    rs.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (stmt != null)
                    stmt.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        return retList;
    }


    private void runReconDelete() {
        Connection conn = getOIMDBConnection();
        String deletionQ =
            "select distinct('DELETE from ' || recon_table_name) from RECON_tables";
        executeListQuery(returnQueryResult(deletionQ, conn), conn);
        executeQuery("DELETE FROM RECON_USER_MATCH", conn);
        executeQuery("DELETE FROM RECON_ACCOUNT_MATCH", conn);
        executeQuery("DELETE FROM RECON_ORG_MATCH", conn);
        executeQuery("DELETE FROM RECON_ROLE_MATCH", conn);
        executeQuery("DELETE FROM RECON_ROLE_HIERARCHY_MATCH", conn);
        executeQuery("DELETE FROM RECON_ROLE_MEMBER_MATCH", conn);
        executeQuery("DELETE FROM RECON_EVENT_ASSIGNMENT", conn);
        executeQuery("DELETE FROM RECON_CHILD_MATCH", conn);
        executeQuery("DELETE FROM RECON_HISTORY", conn);
        executeQuery("DELETE FROM RECON_BATCHES", conn);
        executeQuery("DELETE FROM RECON_EXCEPTIONS", conn);
        executeQuery("DELETE FROM RECON_EVENTS", conn);
        executeQuery("DELETE FROM RECON_BATCHES", conn);
        executeQuery("DELETE FROM RECON_EXCEPTIONS", conn);
        executeQuery("DELETE FROM RECON_EVENTS", conn);
        executeQuery("DELETE FROM RECON_BATCHES", conn);

        try {
            if (conn != null)
                conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}

