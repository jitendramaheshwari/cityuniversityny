package test;


import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import java.sql.Connection;
import java.sql.DriverManager;

import java.util.Hashtable;
import java.util.List;
import java.util.Properties;

import javax.security.auth.login.LoginException;

import oracle.iam.identity.orgmgmt.api.OrganizationManager;
import oracle.iam.identity.orgmgmt.vo.Organization;
import oracle.iam.identity.usermgmt.vo.User;
import oracle.iam.platform.OIMClient;
import oracle.iam.platform.entitymgr.vo.SearchRule;


public class OrgMembershipRules {

    public OrgMembershipRules() throws Exception {
        String orclIDMLoc = System.getenv("INSTALL_LOC");
        System.setProperty("XL.HomeDir", orclIDMLoc + "/server");
        System.setProperty("java.security.policy",
                           orclIDMLoc + "/designconsole/config/xl.policy");
        System.setProperty("java.security.auth.login.config",
                           orclIDMLoc + "/designconsole/config/authwl.conf");
        System.setProperty("XL.ExtendedErrorOptions", "TRUE");
        System.setProperty("APPSERVER_TYPE", "wls");

    }

    private static String getValueFromConfig(String prKey) {
        String val = null;
        Properties prop = new Properties();
        InputStream input = null;
        try {
            input = new FileInputStream("config.properties");
            prop.load(input);
            val = (String)prop.getProperty(prKey);
        } catch (Exception ex) {
            System.out.println("Error occured for reading from config.");
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        System.out.println("Value " + val + " Key:" + prKey);
        return val;
    }

    private Connection getOIMDBConnection() {
        Connection oimDBConnection = null;
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            oimDBConnection =
                    DriverManager.getConnection(getValueFromConfig("OIMDB_URL"),
                                                getValueFromConfig("OIM_DB_USER_NAME"),
                                                getValueFromConfig("OIM_DB_USER_PASSWD"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return oimDBConnection;
    }

    public OIMClient loginWithCustomEnv(String OIMUserName, String OIMPassword,
                                        String MANAGED_SERVER_NAMING_URL) throws LoginException {
        OIMClient client;
        Hashtable env = new Hashtable();
        env.put(OIMClient.JAVA_NAMING_FACTORY_INITIAL,
                OIMClient.WLS_CONTEXT_FACTORY);
        env.put(OIMClient.JAVA_NAMING_PROVIDER_URL, MANAGED_SERVER_NAMING_URL);
        client = new OIMClient(env);
        client.login(OIMUserName, OIMPassword.toCharArray());


        return client;
    }

    public static void main(String[] args) throws Exception {
        OrgMembershipRules apiTest = new OrgMembershipRules();
        apiTest.createMembershipRule("NYT01", apiTest.udfNotNull("NYT01_STU","NYT01_EMP"));
        apiTest.createMembershipRule("SOJ01", apiTest.udfNotNull("SOJ01_STU","SOJ01_EMP"));
        apiTest.createMembershipRule("QNS01", apiTest.udfNotNull("QNS01_STU","QNS01_EMP"));
        apiTest.createMembershipRule("MEC01", apiTest.udfNotNull("MEC01_STU","MEC01_EMP"));
        apiTest.createMembershipRule("GCC01", apiTest.udfNotNull("GuttmanCCStu","GuttmanCCEmp"));
        apiTest.createMembershipRule("MED01", apiTest.udfNotNull("MED01_STU","MED01_EMP"));
        apiTest.createMembershipRule("HCS01", apiTest.udfNotNull("HCS01_STU","HCS01_EMP"));
        apiTest.createMembershipRule("LEH01", apiTest.udfNotNull("LEH01_STU","LEH01_EMP"));
        apiTest.createMembershipRule("LAW01", apiTest.udfNotNull("LAW01_STU","LAW01_EMP"));
        apiTest.createMembershipRule("LAG01", apiTest.udfNotNull("LAG01_STU","LAG01_EMP"));
        apiTest.createMembershipRule("KCC01", apiTest.udfNotNull("KCC01_STU","KCC01_EMP"));
        apiTest.createMembershipRule("JJC01", apiTest.udfNotNull("JJC01_STU","JJC01_EMP"));
        apiTest.createMembershipRule("HTR01", apiTest.udfNotNull("HTR01_STU","HTR01_EMP"));
        apiTest.createMembershipRule("BCC01", apiTest.udfNotNull("BCC01_STU","BCC01_EMP"));
        apiTest.createMembershipRule("SPS01", apiTest.udfNotNull("SPS01_STU","SPS01_EMP"));
        apiTest.createMembershipRule("HOS01", apiTest.udfNotNull("HOS01_STU","HOS01_EMP"));
        apiTest.createMembershipRule("BAR01", apiTest.udfNotNull("BAR01_STU","BAR01_EMP"));
        apiTest.createMembershipRule("GRD01", apiTest.udfNotNull("GRD01_STU","GRD01_EMP"));
        apiTest.createMembershipRule("CSI01", apiTest.udfNotNull("CSI01_STU","CSI01_EMP"));
        apiTest.createMembershipRule("CTY01", apiTest.udfNotNull("CTY01_STU","CTY01_EMP"));
        apiTest.createMembershipRule("BKL01", apiTest.udfNotNull("BKL01_STU","BKL01_EMP"));
        apiTest.createMembershipRule("YRK01", apiTest.udfNotNull("YorkCollegeStu","YRK01_EMP"));
        apiTest.createMembershipRule("BMC01", apiTest.udfNotNull("BMC01_STU","BMC01_EMP"));
        apiTest.createMembershipRule("SPH01", apiTest.udfNotNull("SPH01_STU","SPH01_EMP"));
        apiTest.createMembershipRule("QCC01", apiTest.udfNotNull("QCC01_STU","QCC01_EMP"));
        apiTest.createMembershipRule("MHC01", apiTest.udfNotNull("MHC01_STU","MHC01_EMP"));
        apiTest.createMembershipRule("COSEN", apiTest.udfNotNull("COSEN_EMP"));
        apiTest.createMembershipRule("COCOM", apiTest.udfNotNull("COCOM_EMP"));
    }


    public <T> T getService(Class<T> serviceClass,
                            OIMClient client) throws Exception {
        return client.getService(serviceClass);
    }


    private void createMembershipRule(String orgName, SearchRule rule) throws Exception {
        OIMClient oimClient = loginWithCustomEnv(getValueFromConfig("ADMIN_USER_LOGIN"), getValueFromConfig("ADMIN_USER_PASSWORD"), getValueFromConfig("MANAGED_SERVER_URL"));

        OrganizationManager orgManager = oimClient.getService(OrganizationManager.class);
        Organization org = orgManager.getDetails(orgName, null, true);
        String orgKey = org.getEntityId();
        SearchRule getRule = orgManager.getUserMembershipRule(orgKey);
        System.out.println(orgName);
        List<User> usersList = orgManager.previewDynamicUserMembership(orgKey, getRule, null, null);
        if(usersList!=null) {
        for (User u :usersList) {
            System.out.println(u.getEntityId());
        }
        } else {
            System.out.println(" No Element for "+orgName);
        }
        orgManager.setUserMembershipRule(orgKey, rule);
    }

    private SearchRule udfNotNull(String udfName){
        return new SearchRule(udfName, null, SearchRule.Operator.NOT_EQUAL);
    }
    
    private SearchRule udfNotNull(String udfName1, String udfName2){
            return combineRules(udfNotNull(udfName1), udfNotNull(udfName2), SearchRule.Operator.OR);
    }    
    private SearchRule combineRules(SearchRule rule1, SearchRule rule2, SearchRule.Operator operator){
        return new SearchRule(rule1, rule2, operator);
    }

}


