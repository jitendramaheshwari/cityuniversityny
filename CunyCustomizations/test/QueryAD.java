package test;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;


public class QueryAD {
    private static String U_AD_URL = "ldap://DC01.DEV.FSU.EDU:389";
    private static String U_AD_SEC_PRIN = "OIMADADMIN";
    private static String U_AD_SEC_PRIN_PASSWORD = "passGREENfsuNoles!2";


    public static void main(String[] args) throws NamingException {
        String domainAdminName = "DEV.FSU.EDU\\OIMADADMIN";
        
        System.out.println(domainAdminName.substring(domainAdminName.indexOf("\\")+1));

        DirContext ldapContext = null;
        try {
            System.out.println("Dbut du test Active Directory");

            Hashtable<String, String> ldapEnv =
                new Hashtable<String, String>(11);
            ldapEnv.put(Context.INITIAL_CONTEXT_FACTORY,
                        "com.sun.jndi.ldap.LdapCtxFactory");
            ldapEnv.put(Context.PROVIDER_URL, U_AD_URL);

            ldapEnv.put(Context.SECURITY_AUTHENTICATION, "simple");
            ldapEnv.put(Context.SECURITY_PRINCIPAL, U_AD_SEC_PRIN);
            ldapEnv.put(Context.SECURITY_CREDENTIALS, U_AD_SEC_PRIN_PASSWORD);
            ldapContext = new InitialDirContext(ldapEnv);
            SearchControls searchCtls = new SearchControls();
            searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);


            String searchFilter = "(&(objectClass=person)(CN=aa08g))";
            String searchBase = "OU=People,DC=DEV,DC=FSU,DC=EDU";
            NamingEnumeration<SearchResult> answer =
                ldapContext.search(searchBase, searchFilter, searchCtls);
            System.out.println("search  du test Active Directory");

            System.out.println("answer:" + answer);


            while (answer.hasMoreElements()) {
                SearchResult result = (SearchResult)answer.next();

                Attributes attribs = result.getAttributes();
                System.out.println("Attributes:" + attribs);
                NamingEnumeration<String> attribsIDs = attribs.getIDs();
                System.out.println(attribsIDs);
                StringBuffer output = new StringBuffer();
                while (attribsIDs.hasMore()) {

                    String attrID = attribsIDs.next();
                    System.out.println("AttributeId: " + attrID);
                    NamingEnumeration values =
                        ((BasicAttribute)attribs.get(attrID)).getAll();
                    System.out.println("Naming Enumertaion Values" + values);


                }
            }


            ldapContext.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ldapContext.close();
        }
    }
}
