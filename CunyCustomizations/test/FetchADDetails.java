package test;

import java.io.FileWriter;
import java.io.IOException;

import java.util.ArrayList;
import java.util.Hashtable;

import java.util.List;

import java.util.Locale;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;


public class FetchADDetails {
    private static String E_AD_URL = "ldap://DCEADDEV1.devlogin.cuny.edu:389";
    private static String E_AD_SEC_PRIN = "oim_user11g@devlogin.cuny.edu";
    private static String E_AD_SEC_PRIN_PASSWORD = "Z01PabC";

    private static String U_AD_URL = "ldap://172.29.2.159:389";
    private static String U_AD_SEC_PRIN = "oim_user11g@devuad.cuny.edu";
    private static String U_AD_SEC_PRIN_PASSWORD = "Z01PabC";


    public static void main(String[] args) throws NamingException {
        DirContext ldapContext = null;
        try {
            System.out.println("Dbut du test Active Directory");

            Hashtable<String, String> ldapEnv =
                new Hashtable<String, String>(11);
            ldapEnv.put(Context.INITIAL_CONTEXT_FACTORY,
                        "com.sun.jndi.ldap.LdapCtxFactory");
            ldapEnv.put(Context.PROVIDER_URL,
                        U_AD_URL);

            ldapEnv.put(Context.SECURITY_AUTHENTICATION, "simple");
            ldapEnv.put(Context.SECURITY_PRINCIPAL, U_AD_SEC_PRIN);
            ldapEnv.put(Context.SECURITY_CREDENTIALS, U_AD_SEC_PRIN_PASSWORD);
            ldapContext = new InitialDirContext(ldapEnv);
            SearchControls searchCtls = new SearchControls();
            String returnedAtts[] =
            { "uid","sn", "cn", "dn","cn","displayName","description","ou","company","memberOf","accountExpires","usnChanged","whenChanged","title", "memberOf","AllowLogon", "baruchCollege-Emp", "baruchCollege-Stu", "boroughOfManhattancc-Emp", "boroughOfManhattancc-Stu", "bronxCC-Emp", "bronxCC-Stu", "brooklynCollege-Emp", "brooklynCollege-Stu", "c", "centralOffice-Emp", "cityCollege-Emp", "cityCollege-Stu", "cn", "collegeOfStatenIsland-Emp", "collegeOfStatenIsland-Stu", "company", "department", "displayName", "employeeId", "facsimileTelephoneNumber", "faculty", "givenName", "globalEmployeeStatus", "globalStudentStatus", "graduateCenter-Emp", "graduateCenter-Stu", "guttmanCC-Emp", "guttmanCC-Stu", "homeDirectory", "homePhone", "hostosCC-Emp", "hostosCC-Stu", "hunterCampusSchools-Emp", "hunterCampusSchools-Stu", "hunterCollege-Emp", "hunterCollege-Stu", "IGNORED", "instructor", "ipPhone", "johnJayCollege-Emp", "johnJayCollege-Stu", "kingsBoroughCC-Emp", "kingsBoroughCC-Stu", "l", "laguardiaCC-Emp", "laguardiaCC-Stu", "lawSchool-Emp", "lawSchool-Stu", "lehmanCollege-Emp", "lehmanCollege-Stu", "mail", "manager", "medgarEversCollege-Emp", "medgarEversCollege-Stu", "medicalSchool-Emp", "medicalSchool-Stu", "middleName", "mobile", "nycCollegeOfTechnology-Emp", "nycCollegeOfTechnology-Stu", "pager", "PasswordNeverExpires", "PasswordNotRequired", "physicalDeliveryOfficeName", "postalCode", "postOfficeBox", "primaryAffiliation", "queensBoroughCC-Emp", "queensBoroughCC-Stu", "queensCollege-Emp", "queensCollege-Stu", "sAMAccountName", "schoolOfJournalism-Emp", "schoolOfJournalism-Stu", "schoolOfProfessionalStudies-Emp", "schoolOfProfessionalStudies-Stu", "schoolOfPublicHealth-Emp", "schoolOfPublicHealth-Stu", "sn", "st", "streetAddress", "telephoneNumber", "TerminalServicesHomeDirectory", "TerminalServicesProfilePath", "title", "userPrincipalName", "yorkCollege-Emp", "yorkCollege-Stu"};
//            searchCtls.setReturningAttributes(returnedAtts);

            //Specify the search scope
            searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);







  


            //specify the LDAP search filter
            String searchFilter =
                "(&(objectClass=person)(sAMAccountName=12207579))";
            String searchBase = "OU=CUNYusers,DC=devlogin,DC=cuny,DC=edu";
            String userDN =// "CN=12207579,OU=CUNYusers,DC=devlogin,DC=cuny,DC=edu";
                "CN=10846144,OU=CUNYusers,DC=devuad,DC=cuny,DC=edu";
            
            List<String> array = new ArrayList<String>();
            array.add("yorkCollege-Emp");
            array.add("schoolOfPublicHealth-Stu");
            array.add("schoolOfPublicHealth-Emp");
            array.add("schoolOfProfessionalStudies-Stu");
            array.add("schoolOfProfessionalStudies-Emp");
            array.add("schoolOfJournalism-Stu");
            array.add("schoolOfJournalism-Emp");
            array.add("queensCollege-Stu");
            array.add("queensCollege-Emp");
            array.add("queensBoroughCC-Stu");
            array.add("queensBoroughCC-Emp");
            array.add("primaryAffiliation");
            array.add("nycCollegeOfTechnology-Stu");
            array.add("nycCollegeOfTechnology-Emp");
            array.add("medicalSchool-Stu");
            array.add("medicalSchool-Emp");
            array.add("medgarEversCollege-Stu");
            array.add("medgarEversCollege-Emp");
            array.add("lehmanCollege-Stu");
            array.add("lehmanCollege-Emp");
            array.add("lawSchool-Stu");
            array.add("lawSchool-Emp");
            array.add("laguardiaCC-Stu");
            array.add("laguardiaCC-Emp");
            array.add("johnJayCollege-Stu");
            array.add("yorkCollege-Stu");
            array.add("johnJayCollege-Emp");
            array.add("instructor");
            array.add("hunterCollege-Stu");
            array.add("hunterCollege-Emp");
            array.add("hunterCampusSchools-Stu");
            array.add("hunterCampusSchools-Emp");
            array.add("hostosCC-Stu");
            array.add("hostosCC-Emp");
            array.add("guttmanCC-Stu");
            array.add("guttmanCC-Emp");
            array.add("faculty");
            array.add("collegeOfStatenIsland-Emp");
            array.add("cityCollege-Stu");
            array.add("cityCollege-Emp");
            array.add("brooklynCollege-Stu");
            array.add("brooklynCollege-Emp");
            array.add("bronxCC-Stu");
            array.add("bronxCC-Emp");
            array.add("boroughOfManhattancc-Stu");
            array.add("boroughOfManhattancc-Emp");
            array.add("baruchCollege-Stu");
            array.add("baruchCollege-Emp");
           array.add("kingsBoroughCC-Stu");
            array.add("kingsBoroughCC-Emp");
            array.add("graduateCenter-Stu");
            array.add("graduateCenter-Emp");
            array.add("globalStudentStatus");
            array.add("globalEmployeeStatus");
            array.add("collegeOfStatenIsland-Stu");
            array.add("centralOffice-Emp");     
            array.add("cOCOMCentralOffice-Emp");            
            array.add("macaulayHC-Emp");            
            array.add("macaulayHC-Stu");     
            array.add("everMatriculated");            

            Locale l = Locale.US;
            for (String att:array){
                BasicAttributes basicattributes = new BasicAttributes(true);
                basicattributes.put(new BasicAttribute(att,
                                                       "TE"));
                System.out.println(att);
                try {
                    ldapContext.modifyAttributes(userDN, ldapContext.REPLACE_ATTRIBUTE, basicattributes);
                } catch(Exception e){
                    System.out.println("Issue " + att);
                    throw e;
                }
            }
            
        // ldapContext.modifyAttributes(userDN,ldapContext.REMOVE_ATTRIBUTE,basicattributes);

            int totalResults = 0;

            // Search for objects using the filter
            NamingEnumeration<SearchResult> answer =
                ldapContext.search(searchBase, searchFilter, searchCtls);
            System.out.println("search  du test Active Directory");



            while (answer.hasMoreElements()) {
                SearchResult result = (SearchResult) answer.next();

                Attributes attribs = result.getAttributes();
                System.out.println("Attributes:"+attribs);
                NamingEnumeration<String> attribsIDs = attribs.getIDs();
               System.out.println(attribsIDs);
                // loop on attributes
                StringBuffer output = new StringBuffer();
                while (attribsIDs.hasMore()) {

                    String attrID = attribsIDs.next();
                    System.out.println("AttributeId: "+attrID);
                    NamingEnumeration values = ((BasicAttribute) attribs.get(attrID)).getAll();
                    //System.out.println("Naming Enumertaion Values"+ values.);


                }
            }





            ldapContext.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ldapContext.close();
        }
    }
}
