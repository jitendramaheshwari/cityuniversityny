package test;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import java.util.Hashtable;
import java.util.Properties;

import javax.security.auth.login.LoginException;

import oracle.iam.identity.usermgmt.api.UserManager;
import oracle.iam.identity.usermgmt.vo.User;
import oracle.iam.identity.usermgmt.vo.UserManagerResult;
import static oracle.iam.identity.utils.Constants.EMPTYPE;
import static oracle.iam.identity.utils.Constants.FIRSTNAME;
import static oracle.iam.identity.utils.Constants.LASTNAME;
import static oracle.iam.identity.utils.Constants.ORGKEY;
import static oracle.iam.identity.utils.Constants.USERTYPE;
import oracle.iam.platform.OIMClient;


public class LoadUserCSVWithLogin {
    public LoadUserCSVWithLogin() throws Exception {
        String orclIDMLoc = System.getenv("INSTALL_LOC");
        System.setProperty("XL.HomeDir", orclIDMLoc + "/server");
        System.setProperty("java.security.policy",
                           orclIDMLoc + "/server/client/config/xl.policy");
        System.setProperty("java.security.auth.login.config",
                           orclIDMLoc + "/server/config/authwl.conf");
        System.setProperty("XL.ExtendedErrorOptions", "TRUE");
        System.setProperty("APPSERVER_TYPE", "wls");
    }

    private static String getValueFromConfig(String prKey) {
        String val = null;
        Properties prop = new Properties();
        InputStream input = null;
        try {
            input = new FileInputStream("config.properties");
            prop.load(input);
            val = (String)prop.getProperty(prKey);
        } catch (Exception ex) {
            System.out.println("Error occured for reading from config.");
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        System.out.println("Value " + val + " Key:" + prKey);
        return val;
    }
    public OIMClient loginWithCustomEnv(String OIMUserName, String OIMPassword,
                                        String MANAGED_SERVER_NAMING_URL) throws LoginException {
        OIMClient client;
        Hashtable env = new Hashtable();
        env.put(OIMClient.JAVA_NAMING_FACTORY_INITIAL,
                OIMClient.WLS_CONTEXT_FACTORY);
        env.put(OIMClient.JAVA_NAMING_PROVIDER_URL, MANAGED_SERVER_NAMING_URL);
        client = new OIMClient(env);
        client.login(OIMUserName, OIMPassword.toCharArray());


        return client;
    }
    private Connection getOIMDBConnection() {
        Connection oimDBConnection = null;
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            oimDBConnection =
                    DriverManager.getConnection(getValueFromConfig("OIMDB_URL"),
                                                getValueFromConfig("OIM_DB_USER_NAME"),
                                                getValueFromConfig("OIM_DB_USER_PASSWD"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return oimDBConnection;
    }

    public static void main(String[] args) throws Exception {
        LoadUserCSVWithLogin apiTest = new LoadUserCSVWithLogin();
        apiTest.loadUsersFromFile();
    }

    public <T> T getService(Class<T> serviceClass,
                            OIMClient client) throws Exception {
        return client.getService(serviceClass);
    }
    private void loadUsersFromFile() throws Exception {
        OIMClient oimClient =
            loginWithCustomEnv(getValueFromConfig("ADMIN_USER_LOGIN"), getValueFromConfig("ADMIN_USER_PASSWORD"),
                               getValueFromConfig("MANAGED_SERVER_URL"));
        UserManager userManager = oimClient.getService(UserManager.class);
        String sql = "select usr_first_name, usr_last_name, usr_emp_no, usr_login, act_key from tengcopy";
        String userLogin = null;
        String firstName = null;
        String lastName = null;
        String empNo = null;
        long actKey = 3;

        Connection oimDBConnection = getOIMDBConnection();
        Statement stmt = null;
        ResultSet rs = null;
        try {
            stmt = oimDBConnection.createStatement();
            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                userLogin = rs.getString("usr_login");
                firstName = rs.getString("usr_first_name");
                lastName = rs.getString("usr_last_name");
                empNo = rs.getString("usr_emp_no");
                actKey = rs.getLong("act_key");

                User user = new User(null);
                String employeeType = "CUNY";
                
                user.setAttribute("User Login", userLogin);
                user.setAttribute(FIRSTNAME, firstName);
                user.setAttribute(LASTNAME, lastName);
                user.setAttribute(ORGKEY, actKey);
                user.setAttribute(EMPTYPE, employeeType);
                user.setAttribute(USERTYPE, "End-User");
                user.setAttribute("Employee Number", "End-User");

                UserManagerResult userResult = null;
                try {
                    userResult = userManager.create(user);

                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null)
                    rs.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (stmt != null)
                    stmt.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (oimDBConnection != null)
                    oimDBConnection.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }



    }
}

