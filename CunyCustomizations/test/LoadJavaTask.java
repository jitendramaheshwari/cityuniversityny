package test;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import java.sql.Connection;
import java.sql.DriverManager;

import java.util.HashSet;
import java.util.Hashtable;
import java.util.Properties;
import java.util.Set;

import javax.security.auth.login.LoginException;

import oracle.iam.platform.OIMClient;
import oracle.iam.platformservice.api.PlatformUtilsService;
import oracle.iam.platformservice.vo.JarElement;


public class LoadJavaTask {

    public LoadJavaTask() throws Exception {
        String orclIDMLoc = System.getenv("INSTALL_LOC");
        System.setProperty("XL.HomeDir", orclIDMLoc + "/server");
        System.setProperty("java.security.policy",
                           orclIDMLoc + "/designconsole/config/xl.policy");
        System.setProperty("java.security.auth.login.config",
                           orclIDMLoc + "/designconsole/config/authwl.conf");
        System.setProperty("XL.ExtendedErrorOptions", "TRUE");
        System.setProperty("APPSERVER_TYPE", "wls");

    }

    private static String getValueFromConfig(String prKey) {
        String val = null;
        Properties prop = new Properties();
        InputStream input = null;
        try {
            input = new FileInputStream("config.properties");
            prop.load(input);
            val = (String)prop.getProperty(prKey);
        } catch (Exception ex) {
            System.out.println("Error occured for reading from config.");
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        System.out.println("Value " + val + " Key:" + prKey);
        return val;
    }

    private Connection getOIMDBConnection() {
        Connection oimDBConnection = null;
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            oimDBConnection =
                    DriverManager.getConnection(getValueFromConfig("OIMDB_URL"),
                                                getValueFromConfig("OIM_DB_USER_NAME"),
                                                getValueFromConfig("OIM_DB_USER_PASSWD"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return oimDBConnection;
    }

    public OIMClient loginWithCustomEnv(String OIMUserName, String OIMPassword,
                                        String MANAGED_SERVER_NAMING_URL) throws LoginException {
        OIMClient client;
        Hashtable env = new Hashtable();
        env.put(OIMClient.JAVA_NAMING_FACTORY_INITIAL,
                OIMClient.WLS_CONTEXT_FACTORY);
        env.put(OIMClient.JAVA_NAMING_PROVIDER_URL, MANAGED_SERVER_NAMING_URL);
        client = new OIMClient(env);
        client.login(OIMUserName, OIMPassword.toCharArray());


        return client;
    }

    public static void main(String[] args) throws Exception {
        LoadJavaTask apiTest = new LoadJavaTask();
        String jarName = "";
        apiTest.loadJavaTasks(jarName, true);
        apiTest.loadJavaTasks(jarName, false);
    }


    public <T> T getService(Class<T> serviceClass,
                            OIMClient client) throws Exception {
        return client.getService(serviceClass);
    }

    public void loadJavaTasks(String jarName,
                              boolean ifDelete) {
        OIMClient oimClient;
        try {
            oimClient =
                    loginWithCustomEnv(getValueFromConfig("ADMIN_USER_LOGIN"), getValueFromConfig("ADMIN_USER_PASSWORD"), getValueFromConfig("MANAGED_SERVER_URL"));
            PlatformUtilsService utilsServiceQA =
                getService(PlatformUtilsService.class, oimClient);
            

            Set<JarElement> jarElement = null;
            try {
                jarElement = new HashSet<JarElement>();
                JarElement elem = new JarElement();
                elem.type = "JavaTasks";
                elem.path =
                        "/opt/oracle/middleware/Oracle_IDM1/server/JavaTasks/" +
                        jarName;
                elem.name = jarName;
                jarElement.add(elem);
                utilsServiceQA.purgeCache("All");
                if (ifDelete)
                    utilsServiceQA.deleteJars(jarElement);
                else
                    utilsServiceQA.uploadJars(jarElement);
                utilsServiceQA.purgeCache("All");
            } catch (Throwable e1) {
                e1.printStackTrace();
            }

        
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

