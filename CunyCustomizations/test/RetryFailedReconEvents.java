package test;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;

import javax.security.auth.login.LoginException;

import oracle.iam.platform.OIMClient;
import oracle.iam.reconciliation.api.ReconOperationsService;


public class RetryFailedReconEvents {

    public RetryFailedReconEvents() throws Exception {
        String orclIDMLoc = System.getenv("INSTALL_LOC");
        System.setProperty("XL.HomeDir", orclIDMLoc + "/server");
        System.setProperty("java.security.policy",
                           orclIDMLoc + "/designconsole/config/xl.policy");
        System.setProperty("java.security.auth.login.config",
                           orclIDMLoc + "/designconsole/config/authwl.conf");
        System.setProperty("XL.ExtendedErrorOptions", "TRUE");
        System.setProperty("APPSERVER_TYPE", "wls");

    }
    public OIMClient loginWithCustomEnv(String OIMUserName, String OIMPassword,
                                        String MANAGED_SERVER_NAMING_URL) throws LoginException {
        OIMClient client;
        Hashtable env = new Hashtable();
        env.put(OIMClient.JAVA_NAMING_FACTORY_INITIAL,
                OIMClient.WLS_CONTEXT_FACTORY);
        env.put(OIMClient.JAVA_NAMING_PROVIDER_URL, MANAGED_SERVER_NAMING_URL);
        client = new OIMClient(env);
        client.login(OIMUserName, OIMPassword.toCharArray());


        return client;
    }

    private static String getValueFromConfig(String prKey) {
        String val = null;
        Properties prop = new Properties();
        InputStream input = null;
        try {
            input = new FileInputStream("config.properties");
            prop.load(input);
            val = (String)prop.getProperty(prKey);
        } catch (Exception ex) {
            System.out.println("Error occured for reading from config.");
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        System.out.println("Value " + val + " Key:" + prKey);
        return val;
    }

    private Connection getOIMDBConnection() {
        Connection oimDBConnection = null;
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            oimDBConnection =
                    DriverManager.getConnection(getValueFromConfig("OIMDB_URL"),
                                                getValueFromConfig("OIM_DB_USER_NAME"),
                                                getValueFromConfig("OIM_DB_USER_PASSWD"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return oimDBConnection;
    }


    public static void main(String[] args) throws Exception {
        RetryFailedReconEvents apiTest = new RetryFailedReconEvents();
        apiTest.retryReconEvents();
    }



    public List<Long> returnQueryResult(String query, Connection con) {
        Statement stmt = null;
        ResultSet rs = null;
        List<Long> retList = new ArrayList<Long>();
        try {
            stmt = con.createStatement();
            rs = stmt.executeQuery(query);
            while (rs.next()) {
                retList.add(rs.getLong(1));
            }
        } catch (Exception e1) {
            System.out.println("Failed query " + query);
            e1.printStackTrace();
        } finally {
            try {
                if (rs != null)
                    rs.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (stmt != null)
                    stmt.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        return retList;
    }


    private void retryReconEvents() {
        Connection con = null;
        List<Long> reconEventsKey = null;
        OIMClient oimClient =null;
        try {
            con = getOIMDBConnection();
            reconEventsKey = returnQueryResult("select re_key from recon_events where re_status not in ('Creation Succeeded', 'Update Succeeded')", con);
            oimClient =
                    loginWithCustomEnv(getValueFromConfig("ADMIN_USER_LOGIN"), getValueFromConfig("ADMIN_USER_PASSWORD"), getValueFromConfig("MANAGED_SERVER_URL"));
            ReconOperationsService reconOp = oimClient.getService(ReconOperationsService.class);
            for (long recKey: reconEventsKey)
            {
             System.out.println(recKey);
                try {
                reconOp.processReconciliationEvent(recKey);
                } catch (Exception e) {
                    System.out.println("Failed: " + recKey + " "+e.getMessage());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally{
            try {
                if (con != null)
                    con.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }
}

