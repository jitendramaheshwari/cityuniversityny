package test;


import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;

import javax.security.auth.login.LoginException;

import oracle.iam.identity.usermgmt.api.UserManager;
import oracle.iam.identity.usermgmt.vo.User;
import oracle.iam.platform.OIMClient;
import oracle.iam.provisioning.api.ProvisioningService;
import oracle.iam.provisioning.vo.EntitlementInstance;


public class UserEntitlements {

    public UserEntitlements() throws Exception {
        String orclIDMLoc = System.getenv("INSTALL_LOC");
        System.setProperty("XL.HomeDir", orclIDMLoc + "/server");
        System.setProperty("java.security.policy",
                           orclIDMLoc + "/designconsole/config/xl.policy");
        System.setProperty("java.security.auth.login.config",
                           orclIDMLoc + "/designconsole/config/authwl.conf");
        System.setProperty("XL.ExtendedErrorOptions", "TRUE");
        System.setProperty("APPSERVER_TYPE", "wls");

    }

    private static String getValueFromConfig(String prKey) {
        String val = null;
        Properties prop = new Properties();
        InputStream input = null;
        try {
            input = new FileInputStream("config.properties");
            prop.load(input);
            val = (String)prop.getProperty(prKey);
        } catch (Exception ex) {
            System.out.println("Error occured for reading from config.");
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        System.out.println("Value " + val + " Key:" + prKey);
        return val;
    }


    public OIMClient loginWithCustomEnv(String OIMUserName, String OIMPassword,
                                        String MANAGED_SERVER_NAMING_URL) throws LoginException {
        OIMClient client;
        Hashtable env = new Hashtable();
        env.put(OIMClient.JAVA_NAMING_FACTORY_INITIAL,
                OIMClient.WLS_CONTEXT_FACTORY);
        env.put(OIMClient.JAVA_NAMING_PROVIDER_URL, MANAGED_SERVER_NAMING_URL);
        client = new OIMClient(env);
        client.login(OIMUserName, OIMPassword.toCharArray());


        return client;
    }

    public static void main(String[] args) throws Exception {
        UserEntitlements apiTest = new UserEntitlements();
        String userLogin = "PANKAJ";
        System.out.println(apiTest.returnUserEntitlements(userLogin));
    }


    public <T> T getService(Class<T> serviceClass,
                            OIMClient client) throws Exception {
        return client.getService(serviceClass);
    }

    public List<String> returnUserEntitlements(String userLogin) throws Exception {
        List<String> entist = new ArrayList<String>();
        OIMClient oimClient =
        loginWithCustomEnv(getValueFromConfig("ADMIN_USER_LOGIN"), getValueFromConfig("ADMIN_USER_PASSWORD"), getValueFromConfig("MANAGED_SERVER_URL"));
        ProvisioningService provservice = getService(ProvisioningService.class, oimClient);
        UserManager userManager = getService(UserManager.class, oimClient);
        List<EntitlementInstance> allEntList = null;
        String userKey = null;
        User u = userManager.getDetails(userLogin, null, true);
        if(u!=null) userKey = u.getEntityId();
        if(userKey!=null) {
            allEntList = provservice.getEntitlementsForUser(userKey);
            
        }
        if(allEntList!=null) {
            for (EntitlementInstance entInstance : allEntList) {
                entist.add(entInstance.getEntitlement().getDisplayName());
            }
        }
        return entist;
    }


}


