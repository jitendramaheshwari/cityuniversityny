//package test;

import com.thortech.xl.crypto.tcCryptoUtil;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.Properties;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import javax.security.auth.login.LoginException;

import javax.xml.bind.DatatypeConverter;

import oracle.iam.identity.usermgmt.api.UserManager;
import oracle.iam.platform.OIMClient;
import oracle.iam.selfservice.self.selfmgmt.api.AuthenticatedSelfService;


public class LoadChallengeQuestions {
    private static SecretKeySpec secretKey;
    private static byte[] key;
    private static Cipher aesCipher;
    private final static String SEPARATOR = "~";
    private static final String SECRET_KEY = "DBSecretKey";
    private static Connection conn = null;
    private static boolean useEncryption=false;

    public LoadChallengeQuestions() throws Exception {
        String orclIDMLoc = System.getenv("INSTALL_LOC");
        System.setProperty("XL.HomeDir", orclIDMLoc + "/server");
        System.setProperty("java.security.policy",
                           orclIDMLoc + "/designconsole/config/xl.policy");
        System.setProperty("java.security.auth.login.config",
                           orclIDMLoc + "/designconsole/config/authwl.conf");
        System.setProperty("XL.ExtendedErrorOptions", "TRUE");
        System.setProperty("APPSERVER_TYPE", "wls");
    }


    private static String getValueFromConfig(String prKey) {
        String val = null;
        Properties prop = new Properties();
        InputStream input = null;
        try {
            input = new FileInputStream("config.properties");
            prop.load(input);
            val = (String)prop.getProperty(prKey);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        System.out.println("Value " + val + " Key:" + prKey);
        return val;
    }

    public OIMClient loginWithCustomEnv(String OIMUserName, String OIMPassword,
                                        String MANAGED_SERVER_NAMING_URL) throws LoginException {
        OIMClient client;
        Hashtable env = new Hashtable();
        env.put(OIMClient.JAVA_NAMING_FACTORY_INITIAL,
                OIMClient.WLS_CONTEXT_FACTORY);
        env.put(OIMClient.JAVA_NAMING_PROVIDER_URL, MANAGED_SERVER_NAMING_URL);
        client = new OIMClient(env);
        client.login(OIMUserName, OIMPassword.toCharArray());
        return client;
    }

    public String encrypt(String val) throws Exception {

        String encrypted = null;
        String encstr = val;
        encrypted = tcCryptoUtil.encrypt(encstr, SECRET_KEY);
        return encrypted;
    }

    /**
     * Decrypt a string
     * @param val
     * @return
     * @throws Exception
     */
    public String decrypt(String val) throws Exception {
        String encrypted = null;
        String encstr = val;
        encrypted = tcCryptoUtil.decrypt(encstr, SECRET_KEY);
        return encrypted;
    }
/*
    public static void main(String[] args) throws Exception {
        String plainText = "5BBC4CC2BCEB99F3C467ED7EA1859A52315DEE6179F247631515DD9C02AE9202";
        String decryptedText = decryptText(plainText);
        
        System.out.println("Original Text:" + plainText);
        System.out.println("Descrypted Text:"+decryptedText);
        
    }
*/

    public static void main(String[] args) throws Exception {
setKey();
        LoadChallengeQuestions apiTest = new LoadChallengeQuestions();
        apiTest.loadChallenegeQuestionANswerFile(getValueFromConfig("USERCHALLENGEQ_FILE_PATH"));
    }

    public <T> T getService(Class<T> serviceClass,
                            OIMClient client) throws Exception {
        return client.getService(serviceClass);
    }

    private Connection getOIMDBConnection() {
        try {
            if (conn == null || conn.isClosed()) {

                Class.forName("oracle.jdbc.driver.OracleDriver");
                conn =
DriverManager.getConnection(getValueFromConfig("OIMDB_URL"),
                            getValueFromConfig("OIM_DB_USER_NAME"),
                            getValueFromConfig("OIM_DB_USER_PASSWD"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return conn;
    }

    public boolean checkIfQuestionLoaded(String userLogin) {
        if(!useEncryption) return false; 
        String sql =
            "select count(*) as counter from pcq, usr where usr.usr_key = pcq.usr_key and UPPER(usr.usr_login) = ?";
        PreparedStatement stmt = null;
        ResultSet rs = null;
        int counter = 0;
        try {
            stmt = getOIMDBConnection().prepareStatement(sql);
            stmt.setString(1, userLogin.toUpperCase());
            rs = stmt.executeQuery();
            if (rs.next())
                counter = rs.getInt("counter");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null)
                    rs.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (stmt != null)
                    stmt.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return (counter == 5);
    }

    public boolean checkIfUserLoaded(String userLogin) {
/*        String sql =
            "select count(*) as counter from usr where UPPER(usr_login) = ?";
        PreparedStatement stmt = null;
        ResultSet rs = null;
        int counter = 0;
        try {
            stmt = getOIMDBConnection().prepareStatement(sql);
            stmt.setString(1, userLogin.toUpperCase());
            rs = stmt.executeQuery();
            if (rs.next())
                counter = rs.getInt("counter");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null)
                    rs.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (stmt != null)
                    stmt.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return (counter == 1);
	*/ return true;
    }

    public void loadChallenegeQuestionANswerFile(String fileName) throws Exception {
        getOIMDBConnection();
        File outputFile = new File("Report_Challenge.txt");
        if (outputFile.exists())
            outputFile.delete();
        PrintWriter outputWriter = null;


        BufferedReader saveFile = null;
        try {
            outputFile.createNewFile();
            outputWriter = new PrintWriter(new FileWriter(outputFile), true);

            saveFile = new BufferedReader(new FileReader(fileName));
            String line = null;
            saveFile.readLine();
            int counter = 1;
            while ((line = saveFile.readLine()) != null) {
                String managedServerURL = "";
                if (counter % 2 == 0)
                    managedServerURL =
                            getValueFromConfig("MANAGED_SERVER_URL1");
                else
                    managedServerURL =
                            getValueFromConfig("MANAGED_SERVER_URL2");
                counter++;

                LoadChallengeQuestionsInternal thread1 =
                    new LoadChallengeQuestionsInternal(line, outputWriter,
                                                       managedServerURL, conn);
                Thread thra1 = new Thread(thread1);
                thra1.start();
                while (thra1.activeCount() > 100) {
                    Thread.sleep(500);
                }
                outputWriter.println("Active: " + thra1.activeCount());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            saveFile.close();
            outputWriter.close();
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void setKey() {
        String myKey = "mySecretKey";

        MessageDigest sha = null;
        try {
            key = myKey.getBytes("UTF-8");
            sha = MessageDigest.getInstance("SHA-1");
            key = sha.digest(key);
            key = Arrays.copyOf(key, 16);
            secretKey = new SecretKeySpec(key, "AES");
            aesCipher = Cipher.getInstance("AES");
            aesCipher.init(Cipher.DECRYPT_MODE, secretKey);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String encryptText(String plainText) throws Exception {
        //setKey();
        Cipher aesCipher = Cipher.getInstance("AES");
        aesCipher.init(Cipher.ENCRYPT_MODE, secretKey);
        byte[] byteCipherText = aesCipher.doFinal(plainText.getBytes());
        return bytesToHex(byteCipherText);
    }

    public static String decryptText(String hash) {
        if(!useEncryption) return hash;
        
	//System.out.println("String to decrypt '" + hash+"'");
	if(hash==null || hash.isEmpty()) return null;  
      try {
        //setKey();
        byte[] byteCipherText = hexToByte(hash);
        //Cipher aesCipher = Cipher.getInstance("AES");
        //aesCipher.init(Cipher.DECRYPT_MODE, secretKey);
        byte[] bytePlainText = aesCipher.doFinal(byteCipherText);
        String decrypted = new String(bytePlainText);
//	System.out.println("Decrypted '" + decrypted+"'");
	return decrypted;
        } catch(Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private static String bytesToHex(byte[] hash) {
        return DatatypeConverter.printHexBinary(hash);
    }

    private static byte[] hexToByte(String hex) {
        return DatatypeConverter.parseHexBinary(hex);
    }


    class LoadChallengeQuestionsInternal implements Runnable {
        public String line = null;
        public PrintWriter outputWriter = null;
        public String managedServerURL = null;
        public Connection conn = null;

        public LoadChallengeQuestionsInternal(String line,
                                              PrintWriter outputWriter,
                                              String managedServerURL,
                                              Connection conn) {
            this.line = line;
            this.outputWriter = outputWriter;
            this.managedServerURL = managedServerURL;
            this.conn = conn;
        }

        public void run() {
            try {
                loadQuestionAnswers();
            } catch (Exception e) {
                outputWriter.println(" Encoding issue. " + e.getMessage());
            }
        }

        public void loadQuestionAnswers() throws UnsupportedEncodingException {
            String userLogin = null;
            String userPassword = null;
            String question1 = null;
            String answer1 = null;
            String question2 = null;
            String answer2 = null;
            String question3 = null;
            String answer3 = null;
            String question4 = null;
            String answer4 = null;
            String question5 = null;
            String answer5 = null;


            Map<String, Object> quesans = new HashMap<String, Object>();

            long timeStart = System.currentTimeMillis();
            if (line != null && !line.isEmpty()) {
                String[] parts =
                    line.split(SEPARATOR); //changing separator to #
                outputWriter.println(line);
                outputWriter.println(parts.length);
                if (parts.length == 2) {
                    userLogin = parts[0];
                    userPassword =
                            decryptText(parts[1]);
                    try {
                        if (checkIfUserLoaded(userLogin))
                            updateUserPassword(userLogin, userPassword,
                                               managedServerURL);
                    } catch (Exception e) {
                        outputWriter.println(userLogin +
                                             " Unable to set password. " +
                                             e.getMessage());
                    }
                } else if (parts.length == 12) {
                    userLogin = parts[0];
                    quesans = new HashMap<String, Object>();


                    userPassword =
                            decryptText(parts[1]);
                    question1 = decryptText(parts[2]);
                    answer1 = decryptText(parts[3]);

                    quesans.put(question1, answer1);

                    question2 = decryptText(parts[4]);

                    answer2 = decryptText(parts[5]);
                    quesans.put(question2, answer2);

                    question3 = decryptText(parts[6]);
                    answer3 = decryptText(parts[7]);
                    quesans.put(question3, answer3);

                    question4 = decryptText(parts[8]);
                    answer4 = decryptText(parts[9]);
                    quesans.put(question4, answer4);

                    question5 = decryptText(parts[10]);
                    answer5 = decryptText(parts[11]);
                    quesans.put(question5, answer5);

                    if (!checkIfUserLoaded(userLogin)) {
                        outputWriter.println("User not in system " +
                                             userLogin);

                    }

                    else if (checkIfQuestionLoaded(userLogin))
                        outputWriter.println("Already Loaded  " + userLogin);
                    else {
                        try {
                            OIMClient oimClient =
                                updateUserPassword(userLogin, userPassword,
                                                   managedServerURL);
                            AuthenticatedSelfService authenticatedSelfService =
                                oimClient.getService(AuthenticatedSelfService.class);
                            authenticatedSelfService.setChallengeValues(quesans);
                        } catch (Exception ep) {
                            outputWriter.println(userLogin +
                                                 " Unable to set password/challenege question. " +
                                                 ep.getMessage());
                        }
                        outputWriter.println("Done  " + userLogin);
                    }
                } else {
                    outputWriter.println(userLogin + " Incorrect Data. ");
                }
            }
            long EndStart = System.currentTimeMillis();
            outputWriter.println(userLogin + " Time Taken" +
                                 (EndStart - timeStart));

        }

        private OIMClient updateUserPassword(String userLogin,
                                             String userPassword,
                                             String managedServerURL) throws Exception {
            OIMClient oimClient = null;
            try {
                oimClient =
                        loginWithCustomEnv(userLogin, userPassword, managedServerURL);
            } catch (Exception e) {
                OIMClient oimClientadmin =
                    loginWithCustomEnv(getValueFromConfig("ADMIN_USER_LOGIN"),
                                       getValueFromConfig("ADMIN_USER_PASSWORD"),
                                       managedServerURL);
                UserManager usrmgr =
                    oimClientadmin.getService(UserManager.class);

                usrmgr.changePassword(userLogin, userPassword.toCharArray(),
                                      true, java.util.Locale.US, false, false);
                oimClient =
                        loginWithCustomEnv(userLogin, userPassword, managedServerURL);
            }
            return oimClient;
        }

    }

}

