package test;


import Thor.API.Operations.tcExportOperationsIntf;
import Thor.API.Operations.tcImportOperationsIntf;

import com.thortech.xl.vo.ddm.RootObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;

import java.sql.Connection;
import java.sql.DriverManager;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;

import javax.security.auth.login.LoginException;

import oracle.iam.platform.OIMClient;


public class ImportExport {

    public ImportExport() throws Exception {
        String orclIDMLoc = System.getenv("INSTALL_LOC");
        System.setProperty("XL.HomeDir", orclIDMLoc + "/server");
        System.setProperty("java.security.policy",
                           orclIDMLoc + "/designconsole/config/xl.policy");
        System.setProperty("java.security.auth.login.config",
                           orclIDMLoc + "/designconsole/config/authwl.conf");
        System.setProperty("XL.ExtendedErrorOptions", "TRUE");
        System.setProperty("APPSERVER_TYPE", "wls");

    }

    private static String getValueFromConfig(String prKey) {
        String val = null;
        Properties prop = new Properties();
        InputStream input = null;
        try {
            input = new FileInputStream("config.properties");
            prop.load(input);
            val = (String)prop.getProperty(prKey);
        } catch (Exception ex) {
            System.out.println("Error occured for reading from config.");
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        System.out.println("Value " + val + " Key:" + prKey);
        return val;
    }

    private Connection getOIMDBConnection() {
        Connection oimDBConnection = null;
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            oimDBConnection =
                    DriverManager.getConnection(getValueFromConfig("OIMDB_URL"),
                                                getValueFromConfig("OIM_DB_USER_NAME"),
                                                getValueFromConfig("OIM_DB_USER_PASSWD"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return oimDBConnection;
    }

    public OIMClient loginWithCustomEnv(String OIMUserName, String OIMPassword,
                                        String MANAGED_SERVER_NAMING_URL) throws LoginException {
        OIMClient client;
        Hashtable env = new Hashtable();
        env.put(OIMClient.JAVA_NAMING_FACTORY_INITIAL,
                OIMClient.WLS_CONTEXT_FACTORY);
        env.put(OIMClient.JAVA_NAMING_PROVIDER_URL, MANAGED_SERVER_NAMING_URL);
        client = new OIMClient(env);
        client.login(OIMUserName, OIMPassword.toCharArray());


        return client;
    }

    public static void main(String[] args) throws Exception {
        ImportExport apiTest = new ImportExport();
        String fileName = "/opt/oracle/javaproj/export.xml";
        apiTest.exportObjectToFile(fileName);
    }


    public <T> T getService(Class<T> serviceClass,
                            OIMClient client) throws Exception {
        return client.getService(serviceClass);
    }

    public void importXML(String xmlFileName) throws Exception {
        OIMClient oimClient =
        loginWithCustomEnv(getValueFromConfig("ADMIN_USER_LOGIN"), getValueFromConfig("ADMIN_USER_PASSWORD"), getValueFromConfig("MANAGED_SERVER_URL"));

        tcImportOperationsIntf importIntf =
            (tcImportOperationsIntf)getService(tcImportOperationsIntf.class,
                                               oimClient);
        importIntf.acquireLock(true);
        StringBuffer dmXml = new StringBuffer();
        FileReader fr = new FileReader(xmlFileName);
        BufferedReader br = new BufferedReader(fr);
        String line = null;
        while ((line = br.readLine()) != null) {
            dmXml.append(line);
        }
        Collection items =
            importIntf.addXMLFile(xmlFileName, dmXml.toString());
        importIntf.performImport(items);
    }

    public void exportObjectToFile(String fileName) throws Exception {
        OIMClient oimClient;
            oimClient =
                    loginWithCustomEnv(getValueFromConfig("ADMIN_USER_LOGIN"), getValueFromConfig("ADMIN_USER_PASSWORD"), getValueFromConfig("MANAGED_SERVER_URL"));

        tcExportOperationsIntf exportIntf =
            (tcExportOperationsIntf)getService(tcExportOperationsIntf.class,
                                               oimClient);
        List<String> resList = new ArrayList<String>();
        Collection<String> categories = exportIntf.retrieveCategories();
        for(String category : categories)
          System.out.println(category);
        
        String name = "Form";
        Collection<RootObject> objectCollection = new ArrayList<RootObject>();
        Collection<RootObject> objectCollectionChanged =
            new ArrayList<RootObject>();
        String filter = "UD_CRM_BAS";

        BufferedWriter allobjects_out =
            new BufferedWriter(new FileWriter(fileName));
        resList.add(name);
        for (String resource : resList) {

            Collection<RootObject> lstObjects =
                exportIntf.findObjects(resource, filter);
            lstObjects.addAll(exportIntf.getDependencies(lstObjects));
            lstObjects.addAll(exportIntf.retrieveChildren(lstObjects));
            lstObjects.addAll(exportIntf.retrieveDependencyTree(lstObjects));

            lstObjects.addAll(exportIntf.getDependencies(lstObjects));
            lstObjects.addAll(exportIntf.retrieveChildren(lstObjects));
            lstObjects.addAll(exportIntf.retrieveDependencyTree(lstObjects));
            
            lstObjects.addAll(exportIntf.getDependencies(lstObjects));
            lstObjects.addAll(exportIntf.retrieveChildren(lstObjects));
            lstObjects.addAll(exportIntf.retrieveDependencyTree(lstObjects));

            objectCollection.addAll(lstObjects);
        }
        for (RootObject robj : objectCollection) {
            if (!robj.getName().equalsIgnoreCase("SYSTEM ADMINISTRATORS") &&
                !robj.getName().equalsIgnoreCase("ALL USERS") &&
                !robj.getName().equalsIgnoreCase("SELF OPERATORS") &&
                !robj.getName().equalsIgnoreCase("DEFAULT") &&
                !robj.getName().equalsIgnoreCase("OPERATORS") &&
                !robj.getName().equalsIgnoreCase("Administrators") &&
                !robj.getName().equalsIgnoreCase("BIReportAdministrator")) {
                if (!objectCollectionChanged.contains(robj))
                    if(robj.getPhysicalType().equalsIgnoreCase("Form"))
                        objectCollectionChanged.add(robj);

            }
        }
        for (RootObject robj : objectCollectionChanged) {
            System.out.println(robj.getName()  +  " TYPE "+ robj.getPhysicalType());
        }


        String testXML =
            exportIntf.getExportXML(objectCollectionChanged, "testXML");
        allobjects_out.write(testXML);
        allobjects_out.flush();
        allobjects_out.close();    
    }

}


