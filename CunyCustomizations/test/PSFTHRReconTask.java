package test;


import Thor.API.Operations.tcITResourceInstanceOperationsIntf;
import Thor.API.Operations.tcLookupOperationsIntf;
import Thor.API.Operations.tcOrganizationOperationsIntf;
import Thor.API.Operations.tcReconciliationOperationsIntf;
import Thor.API.Operations.tcSchedulerOperationsIntf;
import Thor.API.Operations.tcUserOperationsIntf;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import java.util.Hashtable;
import java.util.Map;
import java.util.Properties;

import javax.security.auth.login.LoginException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import oracle.iam.connectors.psft.common.handler.HandlerFactory;
import oracle.iam.connectors.psft.common.handler.MessageHandler;
import oracle.iam.connectors.psft.common.util.Utility;
import oracle.iam.platform.OIMClient;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import org.xml.sax.SAXException;


public class PSFTHRReconTask {

    public PSFTHRReconTask() throws Exception {
        String orclIDMLoc = System.getenv("INSTALL_LOC");
        System.setProperty("XL.HomeDir", orclIDMLoc + "/server");
        System.setProperty("java.security.policy",
                           orclIDMLoc + "/designconsole/config/xl.policy");
        System.setProperty("java.security.auth.login.config",
                           orclIDMLoc + "/designconsole/config/authwl.conf");
        System.setProperty("XL.ExtendedErrorOptions", "TRUE");
        System.setProperty("APPSERVER_TYPE", "wls");

    }

    private static String getValueFromConfig(String prKey) {
        String val = null;
        Properties prop = new Properties();
        InputStream input = null;
        try {
            input = new FileInputStream("config.properties");
            prop.load(input);
            val = (String)prop.getProperty(prKey);
        } catch (Exception ex) {
            System.out.println("Error occured for reading from config.");
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        System.out.println("Value " + val + " Key:" + prKey);
        return val;
    }


    public OIMClient loginWithCustomEnv(String OIMUserName, String OIMPassword,
                                        String MANAGED_SERVER_NAMING_URL) throws LoginException {
        OIMClient client;
        Hashtable env = new Hashtable();
        env.put(OIMClient.JAVA_NAMING_FACTORY_INITIAL,
                OIMClient.WLS_CONTEXT_FACTORY);
        env.put(OIMClient.JAVA_NAMING_PROVIDER_URL, MANAGED_SERVER_NAMING_URL);
        client = new OIMClient(env);
        client.login(OIMUserName, OIMPassword.toCharArray());


        return client;
    }

    public static void main(String[] args) throws Exception {
        PSFTHRReconTask apiTest = new PSFTHRReconTask();
        apiTest.execute();
    }


    public <T> T getService(Class<T> serviceClass,
                            OIMClient client) throws Exception {
        return client.getService(serviceClass);
    }

    private String itResourceName = null;
    private Map itResourceDetails = null;
    private String archiveMode = null;
    private String archivePath = null;
    private String filePath = null;
    private boolean isValid = false;


    public void execute() {
        System.out.println("In Execute");

        Utility fileUtil = new Utility();
        boolean reconFlag = false;
        boolean filesPresent = true;
        String messageName = "CU_PSFT_OIM_ER_MSG";
        this.filePath = "/opt/oracle/PSFT/RECORD";
        this.archiveMode = "YES";
        if (this.archiveMode.equalsIgnoreCase("YES")) {
            this.archivePath = "/opt/oracle/PSFT/RECORD/archive";
        }

        
        
        try {
        //    if (this.isValid) {
                File file = new File(this.filePath);
                File[] fileList = file.listFiles();
                if (filesPresent) {
                    int noOfFiles = fileList.length;
                    for (int i = 0; i < noOfFiles; i++) {
			System.out.println(fileList[i].getName());

                        InputStream inputStream =
                            getInputStream(this.filePath, fileList[i].getName());
                        if (inputStream != null) {
                            reconFlag =
                                    init(messageName).handleMessage(inputStream,
                                                                      messageName,
                                                                      this.itResourceName);
                        }
                        if (reconFlag) {
                            if ("YES".equalsIgnoreCase(this.archiveMode)) {
                                fileUtil.copyFilesToArchive(this.filePath +
                                                            File.separatorChar +
                                                            fileList[i].getName(),
                                                            this.archivePath);
                                //fileUtil.deleteFile(fileList[i].getAbsolutePath());
                            }
                        }
                    }
                }
          //  }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void doFileReconciliation(String connectURL, String xmlName, String messageType, String messageName, String sITResourceName)
      {
        String methodName = "doFileReconciliation";
        //this.logger.entering(getClass().getName(), methodName);
        
        URL url = null;
        HttpURLConnection conn = null;
        try
        {
          url = new URL(connectURL);
        }
        catch (MalformedURLException e)
        {
          //this.logger.severe(this.className + ": " + methodName + ": " + "Invalid Listener URL. " + "XML couldn't be processed.");
          
          return;
        }
        try
        {
          conn = (HttpURLConnection)url.openConnection();
          conn.setDoInput(true);
          conn.setDoOutput(true);
          conn.setAllowUserInteraction(false);
          conn.setRequestProperty("messagetype", messageType);
          conn.setRequestProperty("messagename", messageName);
          conn.setRequestProperty("Location", sITResourceName);
          
          //this.logger.fine(this.className + ": " + methodName + ": " + "Got the URL connection successfully.");
          
          DataOutputStream dos = new DataOutputStream(conn.getOutputStream());
          
          //this.logger.fine(this.className + ": " + methodName + ": " + "Going to read file: " + xmlName);
          File file = new File(xmlName);
          DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
          factory.setNamespaceAware(false);
          DocumentBuilder builder = factory.newDocumentBuilder();
          Document xml = builder.parse(file);
          
          String dataFile = xmlToString(xml);
          if ((dataFile != null) && (dos != null))
          {
            dos.writeBytes(dataFile);
            dos.close();
          }
          else
          {
            //this.logger.severe(this.className + ": " + methodName + ": " + "Couldn't get output stream to post message");
            
            return;
          }
          //this.logger.exiting(getClass().getName(), methodName);
          int iStatus = conn.getResponseCode();
          //this.logger.info(this.className + ": " + methodName + ": " + "Response code received " + iStatus);
          if (iStatus == 202) {
            //this.logger.info(this.className + ": " + methodName + ": " + "XML processed successfully.");
          } else {
            //this.logger.severe(this.className + ": " + methodName + ": " + "error in processing xml");
          }
          return;
        }
        catch (IOException e)
        {
          //this.logger.severe(this.className + ": " + methodName + ": " + "Error occurred while processing xml");
        }
        catch (ParserConfigurationException e)
        {
          //this.logger.severe(this.className + ": " + methodName + ": " + "Error occurred while processing xml");
        }
        catch (SAXException e)
        {
          //this.logger.severe(this.className + ": " + methodName + ": " + "Error occurred while processing xml");
        }
        catch (Exception e)
        {
          //this.logger.severe(this.className + ": " + methodName + ": " + "Error occurred while processing xml");
        }
        //this.logger.severe(this.className + ": " + methodName + ": " + "Error is processing message");
      }


    public MessageHandler init(String messageName) {
        tcUserOperationsIntf userAPI = null;
        tcITResourceInstanceOperationsIntf resAPI = null;
        tcSchedulerOperationsIntf schedulerAPI = null;
        tcLookupOperationsIntf lookIntf = null;
        tcReconciliationOperationsIntf reconOperAPI = null;
        tcOrganizationOperationsIntf orgOperAPI = null;
        MessageHandler messageHandler = null;
        try {
            OIMClient oimClient =
            loginWithCustomEnv(getValueFromConfig("ADMIN_USER_LOGIN"), getValueFromConfig("ADMIN_USER_PASSWORD"), getValueFromConfig("MANAGED_SERVER_URL"));

            userAPI =
                    ((tcUserOperationsIntf)oimClient.getService(Thor.API.Operations.tcUserOperationsIntf.class));
            resAPI =
                    ((tcITResourceInstanceOperationsIntf)oimClient.getService(Thor.API.Operations.tcITResourceInstanceOperationsIntf.class));
            schedulerAPI =
                    ((tcSchedulerOperationsIntf)oimClient.getService(Thor.API.Operations.tcSchedulerOperationsIntf.class));
            lookIntf =
                    ((tcLookupOperationsIntf)oimClient.getService(Thor.API.Operations.tcLookupOperationsIntf.class));
            reconOperAPI =
                    ((tcReconciliationOperationsIntf)oimClient.getService(Thor.API.Operations.tcReconciliationOperationsIntf.class));
            orgOperAPI =
                    ((tcOrganizationOperationsIntf)oimClient.getService(Thor.API.Operations.tcOrganizationOperationsIntf.class));
            this.itResourceDetails =
                    new Utility().getITResourceDetails("PSFT HRMS",
                                                       resAPI);
            System.out.println(this.itResourceDetails);
	    HandlerFactory handlerFactoryInstance =
                HandlerFactory.getInstance();
            handlerFactoryInstance.setFullReconMode();

            messageHandler = handlerFactoryInstance.getMessageHandler(messageName,
                                                             this.itResourceName,
                                                             lookIntf,
                                                             resAPI);
            messageHandler.setLookupAPI(lookIntf);
            messageHandler.setReconOperAPI(reconOperAPI);
            messageHandler.setResAPI(resAPI);
            messageHandler.setOrgOperAPI(orgOperAPI);
            messageHandler.setUserAPI(userAPI);
            messageHandler.setItResourceDetails(this.itResourceDetails);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return messageHandler;
    }


    protected InputStream getInputStream(String filePath, String fileName) {
        InputStream inputStream = null;
        try {
            File file = new File(filePath);
            File[] fileList = file.listFiles();
            for (int i = 0; i < fileList.length; i++) {
                if (fileList[i].getName().equalsIgnoreCase(fileName)) {
                    File xmlFile = fileList[i].getAbsoluteFile();
                    inputStream = new FileInputStream(xmlFile);
                }
            }
        } catch (FileNotFoundException e) {
            return null;
        }
        return inputStream;
    }
    public String xmlToString(Node node)
      {
        String methodName = "xmlToString";
        try
        {
          Source source = new DOMSource(node);
          StringWriter stringWriter = new StringWriter();
          Result result = new StreamResult(stringWriter);
          TransformerFactory factory = TransformerFactory.newInstance();
          Transformer transformer = factory.newTransformer();
          transformer.transform(source, result);
          
          return stringWriter.getBuffer().toString();
        }
        catch (TransformerConfigurationException e)
        {
          //this.logger.severe(this.className + ": " + methodName + ": " + "Error occurred while trying to parse xml");
        }
        catch (TransformerException e)
        {
        //  this.logger.severe(this.className + ": " + methodName + ": " + "Error occurred while trying to parse xml");
        }
        return null;
      }
      

}

