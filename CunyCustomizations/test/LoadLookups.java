import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

import java.util.Hashtable;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.security.auth.login.LoginException;

import oracle.iam.platform.OIMClient;
import oracle.iam.platformservice.api.PlatformUtilsService;
import oracle.iam.scheduler.api.SchedulerService;


public class LoadLookups {

    public LoadLookups() throws Exception {
        String orclIDMLoc = System.getenv("INSTALL_LOC");
        System.setProperty("XL.HomeDir", orclIDMLoc + "/server");
        System.setProperty("java.security.policy",
                           orclIDMLoc + "/designconsole/config/xl.policy");
        System.setProperty("java.security.auth.login.config",
                           orclIDMLoc + "/designconsole/config/authwl.conf");
        System.setProperty("XL.ExtendedErrorOptions", "TRUE");
        System.setProperty("APPSERVER_TYPE", "wls");

    }

    private static String getValueFromConfig(String prKey) {
        String val = null;
        Properties prop = new Properties();
        InputStream input = null;
        try {
            input = new FileInputStream("config.properties");
            prop.load(input);
            val = (String)prop.getProperty(prKey);
        } catch (Exception ex) {
            System.out.println("Error occured for reading from config.");
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        System.out.println("Value " + val + " Key:" + prKey);
        return val;
    }

    private Connection getOIMDBConnection() {
        Connection oimDBConnection = null;
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            oimDBConnection =
                    DriverManager.getConnection(getValueFromConfig("OIMDB_URL"),
                                                getValueFromConfig("OIM_DB_USER_NAME"),
                                                getValueFromConfig("OIM_DB_USER_PASSWD"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return oimDBConnection;
    }


    public static void main(String[] args) throws Exception {
        LoadLookups apiTest = new LoadLookups();
        apiTest.unZipFile("/opt/oracle/PSFTUM/LookupRecon/CRM_OIM_PSFT_properties_exports_CNYCMPT3.zip", "/opt/oracle/PSFTUM/LookupRecon/");
        apiTest.runLookUp("CRM");

        apiTest.unZipFile("/opt/oracle/PSFTUM/LookupRecon/EPM_OIM_PSFT_properties_exports_CNYPMPT3.zip", "/opt/oracle/PSFTUM/LookupRecon/");
        apiTest.runLookUp("EPM");

        apiTest.unZipFile("/opt/oracle/PSFTUM/LookupRecon/RFI_OIM_PSFT_properties_exports_CNYFSDEV.zip", "/opt/oracle/PSFTUM/LookupRecon/");
        apiTest.runLookUp("RFI");

        apiTest.unZipFile("/opt/oracle/PSFTUM/LookupRecon/ELM_OIM_PSFT_properties_exports_CNYLMPT3.zip", "/opt/oracle/PSFTUM/LookupRecon/");
        apiTest.runLookUp("ELM");

        apiTest.unZipFile("/opt/oracle/PSFTUM/LookupRecon/FIN_OIM_PSFT_properties_exports_CNYFSCNV.zip", "/opt/oracle/PSFTUM/LookupRecon/");
        apiTest.runLookUp("FIN");

        apiTest.unZipFile("/opt/oracle/PSFTUM/LookupRecon/RHC_OIM_PSFT_properties_exports_CNYHCFIT.zip", "/opt/oracle/PSFTUM/LookupRecon/");
        apiTest.runLookUp("RHC");

        apiTest.unZipFile("/opt/oracle/PSFTUM/LookupRecon/ENP_OIM_PSFT_properties_exports_CNYEPPT3.zip", "/opt/oracle/PSFTUM/LookupRecon/");
        apiTest.runLookUp("ENP");

        apiTest.unZipFile("/opt/oracle/PSFTUM/LookupRecon/HCM_OIM_PSFT_properties_exports.zip", "/opt/oracle/PSFTUM/LookupRecon/");
        apiTest.runLookUp("HCM");


    }

    public void executeQuery(String query, Connection con) {
        Statement stmt = null;
        try {
            stmt = con.createStatement();
            stmt.execute(query);
        } catch (Exception e1) {
            System.out.println("Failed query " + query);
            e1.printStackTrace();
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void emptyArchiveDirectory(String topology) {
        File dir =
            new File("/opt/oracle/PSFTUM/LookupRecon/" + topology + "Archive");
        for (File f : dir.listFiles()) {
            f.delete();
        }
    }

    private void runLookUp(String topology) throws Exception {
        Connection conn = getOIMDBConnection();
        executeQuery("delete from lkv where lku_key in (select lku_key from lku where lku_type_string_key = 'Lookup." +
                     topology + "PSFT.UM.Roles')", conn);
        executeQuery("delete from lkv where lku_key in (select lku_key from lku where lku_type_string_key = 'Lookup." +
                     topology + "PSFT.UM.PermissionList')", conn);
        executeQuery("delete from lkv where lku_key in (select lku_key from lku where lku_type_string_key = 'Lookup." +
                     topology + "PSFT.UM.LanguageCode')", conn);
        executeQuery("delete from lkv where lku_key in (select lku_key from lku where lku_type_string_key = 'Lookup." +
                     topology + "PSFT.UM.CurrencyCode')", conn);
        executeQuery("delete from lkv where lku_key in (select lku_key from lku where lku_type_string_key = 'Lookup." +
                     topology + "PSFT.UM.EmailType')", conn);

        emptyArchiveDirectory(topology);
        OIMClient oimClientadmin =
            loginWithCustomEnv(getValueFromConfig("ADMIN_USER_LOGIN"),
                               getValueFromConfig("ADMIN_USER_PASSWORD"),
                               getValueFromConfig("MANAGED_SERVER_URL"));
        SchedulerService schservice =
            oimClientadmin.getService(SchedulerService.class);

        try {
            runJob(topology + " Peoplesoft Currency Code Lookup Reconciliation", schservice);
            runJob(topology + " Peoplesoft Email Type Lookup Reconciliation", schservice);
            runJob(topology + " Peoplesoft Language Code Lookup Reconciliation", schservice);
            runJob(topology + " Peoplesoft Permission List Lookup Reconciliation", schservice);
            runJob(topology + " Peoplesoft Roles Lookup Reconciliation", schservice);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            if (conn != null)
                conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void runJob(String jobName, SchedulerService schservice){
        int jobStatus = 0;
        try {
            schservice.triggerNow(jobName);
            while (jobStatus == 5) {
                try {
                    Thread.sleep(1000L);
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
                jobStatus = schservice.getStatusOfJob(jobName);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public OIMClient loginWithCustomEnv(String OIMUserName, String OIMPassword,
                                        String MANAGED_SERVER_NAMING_URL) throws LoginException {
        OIMClient client;
        Hashtable env = new Hashtable();
        env.put(OIMClient.JAVA_NAMING_FACTORY_INITIAL,
                OIMClient.WLS_CONTEXT_FACTORY);
        env.put(OIMClient.JAVA_NAMING_PROVIDER_URL, MANAGED_SERVER_NAMING_URL);
        client = new OIMClient(env);
        client.login(OIMUserName, OIMPassword.toCharArray());
        PlatformUtilsService service =
            (PlatformUtilsService)client.getService(oracle.iam.platformservice.api.PlatformUtilsService.class);
        try {
            service.purgeCache("All");
            System.out.println("Purge Cache");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return client;
    }

    public void unZipFile(String zipFileLocation, String outputFolder) {
        File dir = new File(outputFolder);
        if(!dir.exists()) dir.mkdirs();
        FileInputStream fis;
        byte[] buffer = new byte[1024];
        try {
            fis = new FileInputStream(zipFileLocation);
            ZipInputStream zis = new ZipInputStream(fis);
            ZipEntry ze = zis.getNextEntry();
            while(ze != null){
                String fileName = ze.getName();
                File newFile = new File(outputFolder + File.separator + fileName);
                System.out.println("Unzipping to "+newFile.getAbsolutePath());
                new File(newFile.getParent()).mkdirs();
                FileOutputStream fos = new FileOutputStream(newFile);
                int len;
                while ((len = zis.read(buffer)) > 0) {
                    fos.write(buffer, 0, len);
                }
                fos.close();
                zis.closeEntry();
                ze = zis.getNextEntry();
            }
            zis.closeEntry();
            zis.close();
            fis.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}

