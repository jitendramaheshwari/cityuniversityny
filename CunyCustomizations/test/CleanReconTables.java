package test;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;


public class CleanReconTables {

    public CleanReconTables() throws Exception {
        String orclIDMLoc = System.getenv("INSTALL_LOC");
        System.setProperty("XL.HomeDir", orclIDMLoc + "/server");
        System.setProperty("java.security.policy",
                           orclIDMLoc + "/designconsole/config/xl.policy");
        System.setProperty("java.security.auth.login.config",
                           orclIDMLoc + "/designconsole/config/authwl.conf");
        System.setProperty("XL.ExtendedErrorOptions", "TRUE");
        System.setProperty("APPSERVER_TYPE", "wls");

    }

    private static String getValueFromConfig(String prKey) {
        String val = null;
        Properties prop = new Properties();
        InputStream input = null;
        try {
            input = new FileInputStream("config.properties");
            prop.load(input);
            val = (String)prop.getProperty(prKey);
        } catch (Exception ex) {
            System.out.println("Error occured for reading from config.");
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        System.out.println("Value " + val + " Key:" + prKey);
        return val;
    }

    private Connection getOIMDBConnection() {
        Connection oimDBConnection = null;
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            oimDBConnection =
                    DriverManager.getConnection(getValueFromConfig("OIMDB_URL"),
                                                getValueFromConfig("OIM_DB_USER_NAME"),
                                                getValueFromConfig("OIM_DB_USER_PASSWD"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return oimDBConnection;
    }


    public static void main(String[] args) throws Exception {
        CleanReconTables apiTest = new CleanReconTables();
        apiTest.runReconDelete();
    }

    public void executeQuery(String query, Connection con) {
        Statement stmt = null;
        try {
            stmt = con.createStatement();
            stmt.execute(query);
        } catch (Exception e1) {
            System.out.println("Failed query " + query);
            e1.printStackTrace();
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void executeListQuery(List<String> query, Connection con) {
        for (String q : query) {
            executeQuery(q, con);
        }
    }

    public List<String> returnQueryResult(String query, Connection con) {
        Statement stmt = null;
        ResultSet rs = null;
        List<String> retList = new ArrayList<String>();
        try {
            stmt = con.createStatement();
            rs = stmt.executeQuery(query);
            while (rs.next()) {
                retList.add(rs.getString(1));
            }
        } catch (Exception e1) {
            System.out.println("Failed query " + query);
            e1.printStackTrace();
        } finally {
            try {
                if (rs != null)
                    rs.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (stmt != null)
                    stmt.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        return retList;
    }


    private void runReconDelete() {
        Connection conn = getOIMDBConnection();
        String deletionQ =
            "select distinct('DELETE from ' || recon_table_name) from RECON_tables";
        executeListQuery(returnQueryResult(deletionQ, conn), conn);
        executeQuery("DELETE FROM RECON_USER_MATCH", conn);
        executeQuery("DELETE FROM RECON_ACCOUNT_MATCH", conn);
        executeQuery("DELETE FROM RECON_ORG_MATCH", conn);
        executeQuery("DELETE FROM RECON_ROLE_MATCH", conn);
        executeQuery("DELETE FROM RECON_ROLE_HIERARCHY_MATCH", conn);
        executeQuery("DELETE FROM RECON_ROLE_MEMBER_MATCH", conn);
        executeQuery("DELETE FROM RECON_EVENT_ASSIGNMENT", conn);
        executeQuery("DELETE FROM RECON_CHILD_MATCH", conn);
        executeQuery("DELETE FROM RECON_HISTORY", conn);
        executeQuery("DELETE FROM RECON_BATCHES", conn);
        executeQuery("DELETE FROM RECON_EXCEPTIONS", conn);
        executeQuery("DELETE FROM RECON_EVENTS", conn);
        executeQuery("DELETE FROM RECON_BATCHES", conn);
        executeQuery("DELETE FROM RECON_EXCEPTIONS", conn);
        executeQuery("DELETE FROM RECON_EVENTS", conn);
        executeQuery("DELETE FROM RECON_BATCHES", conn);
/*
 * 
 * 
 * 
 * 	  
CREATE INDEX FDX_USR_STATUS ON USR (UPPER(USR_STATUS));
CREATE INDEX FDX_USR_EMP_NO ON USR (UPPER(USR_EMP_NO));
CREATE INDEX FDX_AUD_JMS_FAILED ON AUD_JMS (FAILED);
CREATE INDEX FDX_USR_CHAR_KEY ON USR (TO_CHAR(USR_KEY));
CREATE INDEX FDX_UPPER_USR_EMP_TYPE ON USR (upper(usr_emp_type));
CREATE INDEX IDX_USR_UPPER_GUID ON USR (UPPER(usr_udf_obguid));

CREATE INDEX "IDX_USG_UGP_KEY" ON "USG" ("UGP_KEY") ;
CREATE INDEX RDX_RU_GROUPNAMEBFB9FBCC ON RA_UADUSRC(RA_GROUPNAMEBFB9FBCC);
CREATE INDEX RDX_RU_ITRESOURCENAME70C9F928 ON RA_UADUSER622DCBD3(RA_ITRESOURCENAME70C9F928);
CREATE INDEX RDX_RU_UNIQUEID575B37CA ON RA_UADUSER622DCBD3(RA_UNIQUEID575B37CA);
CREATE INDEX RDX_UPPERRU_EMPLOYEEID1E671C56 ON RA_UADUSER622DCBD3(UPPER(RA_EMPLOYEEID));
CREATE INDEX RDX_UPPERRU_UNIQUEID57B884BEE2 ON RA_UADUSER622DCBD3(UPPER(RA_UNIQUEID575B37CA));
CREATE INDEX RDX_UPPERUSR_EMP_NOD0C7DB40 ON USR(UPPER(usr_emp_no));
CREATE INDEX "IDX_RA_HCMPEOP_RE_KEY10723" ON "RA_HCMPEOPLESOFTUS6B758AE4" ("RE_KEY") ;
CREATE INDEX "RDX_RA_HCMITRESOURCENAME1812" ON "RA_HCMPEOPLESOFTUS6B758AE4" ("RA_ITRESOURCENAME22D11812") ;
CREATE INDEX "RDX_RA_HCMUSERID5A729570" ON "RA_HCMPEOPLESOFTUS6B758AE4" ("RA_USERID5A729570") ;
CREATE INDEX "RDX_RA_HCMEMAILTYPEC1E5E" ON "RA_HCM_MAIL" ("RA_EMAILTYPEC1E5657E");
CREATE INDEX "IDX_RA_CRMPEOP_RE_KEY10723" ON "RA_CRMPEOPLESOFTUSEBADE1F0" ("RE_KEY") ;
CREATE INDEX "RDX_RA_CRMITRESOURCENAME1812" ON "RA_CRMPEOPLESOFTUSEBADE1F0" ("RA_ITRESOURCENAME22D11812") ;
CREATE INDEX "RDX_RA_CRMUSERID5A729570" ON "RA_CRMPEOPLESOFTUSEBADE1F0" ("RA_USERID5A729570") ;
CREATE INDEX "RDX_RA_CRMEMAILTYPEC1E5E" ON "RA_CRM_MAIL" ("RA_EMAILTYPEC1E5657E");
CREATE INDEX "IDX_RA_EPMPEOP_RE_KEY10723" ON "RA_EPMPEOPLESOFTUSFDDEE134" ("RE_KEY") ;
CREATE INDEX "RDX_RA_EPMITRESOURCENAME1812" ON "RA_EPMPEOPLESOFTUSFDDEE134" ("RA_ITRESOURCENAME22D11812") ;
CREATE INDEX "RDX_RA_EPMUSERID5A729570" ON "RA_EPMPEOPLESOFTUSFDDEE134" ("RA_USERID5A729570") ;
CREATE INDEX "RDX_RA_EPMEMAILTYPEC1E5E" ON "RA_EPM_MAIL" ("RA_EMAILTYPEC1E5657E");
CREATE INDEX "IDX_RA_ENPPEOP_RE_KEY10723" ON "RA_ENPPEOPLESOFTUS66B8B6F9" ("RE_KEY") ;
CREATE INDEX "RDX_RA_ENPITRESOURCENAME1812" ON "RA_ENPPEOPLESOFTUS66B8B6F9" ("RA_ITRESOURCENAME22D11812") ;
CREATE INDEX "RDX_RA_ENPUSERID5A729570" ON "RA_ENPPEOPLESOFTUS66B8B6F9" ("RA_USERID5A729570") ;
CREATE INDEX "RDX_RA_ENPEMAILTYPEC1E5E" ON "RA_ENP_MAIL" ("RA_EMAILTYPEC1E5657E");
CREATE INDEX "IDX_RA_RFIPEOP_RE_KEY10723" ON "RA_RFIPEOPLESOFTUS7F2B8AC7" ("RE_KEY") ;
CREATE INDEX "RDX_RA_RFIITRESOURCENAME1812" ON "RA_RFIPEOPLESOFTUS7F2B8AC7" ("RA_ITRESOURCENAME22D11812") ;
CREATE INDEX "RDX_RA_RFIUSERID5A729570" ON "RA_RFIPEOPLESOFTUS7F2B8AC7" ("RA_USERID5A729570") ;
CREATE INDEX "RDX_RA_RFIEMAILTYPEC1E5E" ON "RA_RFI_MAIL" ("RA_EMAILTYPEC1E5657E");
CREATE INDEX "IDX_RA_RHCPEOP_RE_KEY10723" ON "RA_RHCPEOPLESOFTUS24541AFF" ("RE_KEY") ;
CREATE INDEX "RDX_RA_RHCITRESOURCENAME1812" ON "RA_RHCPEOPLESOFTUS24541AFF" ("RA_ITRESOURCENAME22D11812") ;
CREATE INDEX "RDX_RA_RHCUSERID5A729570" ON "RA_RHCPEOPLESOFTUS24541AFF" ("RA_USERID5A729570") ;
CREATE INDEX "RDX_RA_RHCEMAILTYPEC1E5E" ON "RA_RHC_MAIL" ("RA_EMAILTYPEC1E5657E");
CREATE INDEX "IDX_RA_ELMPEOP_RE_KEY10723" ON "RA_ELMPEOPLESOFTUSEB9758B8" ("RE_KEY") ;
CREATE INDEX "RDX_RA_ELMITRESOURCENAME1812" ON "RA_ELMPEOPLESOFTUSEB9758B8" ("RA_ITRESOURCENAME22D11812") ;
CREATE INDEX "RDX_RA_ELMUSERID5A729570" ON "RA_ELMPEOPLESOFTUSEB9758B8" ("RA_USERID5A729570") ;
CREATE INDEX "RDX_RA_ELMEMAILTYPEC1E5E" ON "RA_ELM_MAIL" ("RA_EMAILTYPEC1E5657E");
CREATE INDEX "IDX_RA_FINPEOP_RE_KEY10723" ON "RA_FINPEOPLESOFTUSBC35F21D" ("RE_KEY") ;
CREATE INDEX "RDX_RA_FINITRESOURCENAME1812" ON "RA_FINPEOPLESOFTUSBC35F21D" ("RA_ITRESOURCENAME22D11812") ;
CREATE INDEX "RDX_RA_FINUSERID5A729570" ON "RA_FINPEOPLESOFTUSBC35F21D" ("RA_USERID5A729570") ;
CREATE INDEX "RDX_RA_FINEMAILTYPEC1E5E" ON "RA_FIN_MAIL" ("RA_EMAILTYPEC1E5657E");
CREATE INDEX "FDX_USR_STATUS" ON "USR" (UPPER("USR_STATUS"));
CREATE INDEX "FDX_USR_EMP_NO" ON "USR" (UPPER("USR_EMP_NO"));
CREATE INDEX "FDX_AUD_JMS_FAILED" ON "AUD_JMS" ("FAILED");
CREATE INDEX "FDX_USR_CHAR_KEY" ON "USR" (TO_CHAR("USR_KEY"));
CREATE INDEX "IDX_USR_STATUS" ON "USR" (("USR_STATUS")) ;
CREATE INDEX RDX_R_GROUPNAMEBFB9FBCC ON RA_ADUSRC(RA_GROUPNAMEBFB9FBCC);
CREATE INDEX RDX_R_ITRESOURCENAME70C9F928 ON RA_ADUSERE469E5C8(RA_ITRESOURCENAME70C9F928);
CREATE INDEX RDX_R_UNIQUEID575B37CA ON RA_ADUSERE469E5C8(RA_UNIQUEID575B37CA);
CREATE INDEX RDX_UPPERR_EMPLOYEEID1E671C56 ON RA_ADUSERE469E5C8(UPPER(RA_EMPLOYEEID));
CREATE INDEX RDX_UPPERR_UNIQUEID57B884BEE2 ON RA_ADUSERE469E5C8(UPPER(RA_UNIQUEID575B37CA));


 */

        String rebuildIndex =
            "SELECT 'alter index ' || index_name || ' rebuild' FROM  USER_INDEXES where table_owner = 'VIOLET_OIM' and table_name in ('USR','RECON_USER_MATCH', 'RECON_ACCOUNT_MATCH', " +
            "'RECON_ORG_MATCH',  'RECON_ROLE_MATCH', 'RECON_ROLE_HIERARCHY_MATCH', 'RECON_ROLE_MEMBER_MATCH', 'RECON_EVENT_ASSIGNMENT', 'RECON_CHILD_MATCH', 'RECON_HISTORY', 'RECON_BATCHES', " +
            "'RECON_EXCEPTIONS', 'RECON_EVENTS', 'RECON_BATCHES') UNION ALL " +
            "SELECT 'alter index ' || index_name || ' rebuild' FROM  USER_INDEXES where table_owner = 'VIOLET_OIM' and table_name in (select recon_table_name from RECON_tables)";
        executeListQuery(returnQueryResult(rebuildIndex, conn), conn);

        try {
            if (conn != null)
                conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}

