//package test;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import java.sql.Connection;
import java.sql.DriverManager;

import java.util.Hashtable;
import java.util.Properties;

import javax.security.auth.login.LoginException;

import oracle.iam.identity.rolemgmt.api.RoleManager;
import oracle.iam.identity.rolemgmt.vo.Role;
import oracle.iam.identity.usermgmt.api.UserManagerConstants;
import oracle.iam.platform.OIMClient;
import oracle.iam.platform.entitymgr.vo.SearchCriteria;
import oracle.iam.platform.entitymgr.vo.SearchRule;


public class RoleMembershipRules {

    public RoleMembershipRules() throws Exception {
        String orclIDMLoc = System.getenv("INSTALL_LOC");
        System.setProperty("XL.HomeDir", orclIDMLoc + "/server");
        System.setProperty("java.security.policy",
                           orclIDMLoc + "/designconsole/config/xl.policy");
        System.setProperty("java.security.auth.login.config",
                           orclIDMLoc + "/designconsole/config/authwl.conf");
        System.setProperty("XL.ExtendedErrorOptions", "TRUE");
        System.setProperty("APPSERVER_TYPE", "wls");

    }

    private static String getValueFromConfig(String prKey) {
        String val = null;
        Properties prop = new Properties();
        InputStream input = null;
        try {
            input = new FileInputStream("config.properties");
            prop.load(input);
            val = (String)prop.getProperty(prKey);
        } catch (Exception ex) {
            System.out.println("Error occured for reading from config.");
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        System.out.println("Value " + val + " Key:" + prKey);
        return val;
    }

    private Connection getOIMDBConnection() {
        Connection oimDBConnection = null;
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            oimDBConnection =
                    DriverManager.getConnection(getValueFromConfig("OIMDB_URL"),
                                                getValueFromConfig("OIM_DB_USER_NAME"),
                                                getValueFromConfig("OIM_DB_USER_PASSWD"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return oimDBConnection;
    }

    public OIMClient loginWithCustomEnv(String OIMUserName, String OIMPassword,
                                        String MANAGED_SERVER_NAMING_URL) throws LoginException {
        OIMClient client;
        Hashtable env = new Hashtable();
        env.put(OIMClient.JAVA_NAMING_FACTORY_INITIAL,
                OIMClient.WLS_CONTEXT_FACTORY);
        env.put(OIMClient.JAVA_NAMING_PROVIDER_URL, MANAGED_SERVER_NAMING_URL);
        client = new OIMClient(env);
        client.login(OIMUserName, OIMPassword.toCharArray());


        return client;
    }

    public static void main(String[] args) throws Exception {
        RoleMembershipRules apiTest = new RoleMembershipRules();
                apiTest.createMembershipRule("Baruch College", apiTest.Employees_Active_AND_Retired_AND_Phase1_Provisioning_OR_Students_Active_WITH_ORG("BAR01"));
                apiTest.createMembershipRule("Borough of Manhattan CC", apiTest.Employees_Active_AND_Retired_AND_Phase1_Provisioning_OR_Students_Active_WITH_ORG("BMC01"));
                apiTest.createMembershipRule("Bronx CC", apiTest.Employees_Active_AND_Retired_AND_Phase1_Provisioning_OR_Students_Active_WITH_ORG("BCC01"));
                apiTest.createMembershipRule("Brooklyn College", apiTest.Employees_Active_AND_Retired_AND_Phase1_Provisioning_OR_Students_Active_WITH_ORG("BKL01"));
                apiTest.createMembershipRule("Central Office", apiTest.Central_Office());
                apiTest.createMembershipRule("City College", apiTest.Employees_Active_AND_Retired_AND_Phase1_Provisioning_OR_Students_Active_WITH_ORG("CTY01"));
                apiTest.createMembershipRule("College of Staten Island", apiTest.Employees_Active_AND_Retired_AND_Phase1_Provisioning_OR_Students_Active_WITH_ORG("CSI01"));
                apiTest.createMembershipRule("CUNY Student Admitted", apiTest.CUNY_Student_Status_Group_Membership("StudentAdmitted"));
                apiTest.createMembershipRule("CUNY Student Applicant", apiTest.CUNY_Student_Status_Group_Membership("StudentApplicant"));
                apiTest.createMembershipRule("CUNY Student Cancelled", apiTest.CUNY_Student_Status_Group_Membership("StudentCancelled"));
                apiTest.createMembershipRule("CUNY Student Completed",  new SearchRule("StudentCompleted", "TRUE", SearchRule.Operator.EQUAL));
                apiTest.createMembershipRule("CUNY Student Discontinued", apiTest.CUNY_Student_Status_Group_Membership("StudentDiscontinued"));
                apiTest.createMembershipRule("CUNY Student Dismissed", new SearchRule("StudentDismissed", "TRUE", SearchRule.Operator.EQUAL));
                apiTest.createMembershipRule("CUNY Student Matriculated", apiTest.CUNY_Student_Status_Group_Membership("StudentMatriculated"));
                apiTest.createMembershipRule("CUNY Student Prematriculated", apiTest.CUNY_Student_Status_Group_Membership("StudentPrematriculated"));
                apiTest.createMembershipRule("CUNY Student Waitlisted", apiTest.CUNY_Student_Status_Group_Membership("StudentWaitlisted"));
                apiTest.createMembershipRule("Employees (Active & Retired)", apiTest.Employees_Active_or_Retired_or_Inactive_Term_not_TRUE());
                apiTest.createMembershipRule("Employees (Active) & Faculty & Phase 1 Provisioning", apiTest.Employees_TRUE_AND_Faculty_TRUE_AND_Phase1_Provisioning());
                apiTest.createMembershipRule("York College", apiTest.Employees_Active_AND_Retired_AND_Phase1_Provisioning_OR_Students_Active_WITH_ORG("YRK01"));
                apiTest.createMembershipRule("School of Public Health", apiTest.Employees_Active_AND_Retired_AND_Phase1_Provisioning_OR_Students_Active_WITH_ORG("SPH01"));
                apiTest.createMembershipRule("School of Professional Studies", apiTest.Employees_Active_AND_Retired_AND_Phase1_Provisioning_OR_Students_Active_WITH_ORG("SPS01"));
                apiTest.createMembershipRule("School of Journalism", apiTest.Employees_Active_AND_Retired_AND_Phase1_Provisioning_OR_Students_Active_WITH_ORG("SOJ01"));
                apiTest.createMembershipRule("Queensborough CC", apiTest.Employees_Active_AND_Retired_AND_Phase1_Provisioning_OR_Students_Active_WITH_ORG("QCC01"));
                apiTest.createMembershipRule("Queens College", apiTest.Employees_Active_AND_Retired_AND_Phase1_Provisioning_OR_Students_Active_WITH_ORG("QNS01"));
                apiTest.createMembershipRule("Graduate Center", apiTest.Employees_Active_AND_Retired_AND_Phase1_Provisioning_OR_Students_Active_WITH_ORG("GRD01"));
                apiTest.createMembershipRule("Hostos CC", apiTest.Employees_Active_AND_Retired_AND_Phase1_Provisioning_OR_Students_Active_WITH_ORG("HOS01"));
                apiTest.createMembershipRule("Hunter Campus Schools", apiTest.Employees_Active_AND_Retired_AND_Phase1_Provisioning_OR_Students_Active_WITH_ORG("HCS01"));
                apiTest.createMembershipRule("Hunter College", apiTest.Employees_Active_AND_Retired_AND_Phase1_Provisioning_OR_Students_Active_WITH_ORG("HTR01"));
                apiTest.createMembershipRule("John Jay College", apiTest.Employees_Active_AND_Retired_AND_Phase1_Provisioning_OR_Students_Active_WITH_ORG("JJC01"));
                apiTest.createMembershipRule("Kingsborough CC", apiTest.Employees_Active_AND_Retired_AND_Phase1_Provisioning_OR_Students_Active_WITH_ORG("KCC01"));
                apiTest.createMembershipRule("LaGuardia CC", apiTest.Employees_Active_AND_Retired_AND_Phase1_Provisioning_OR_Students_Active_WITH_ORG("LAG01"));
                apiTest.createMembershipRule("Law School", apiTest.Employees_Active_AND_Retired_AND_Phase1_Provisioning_OR_Students_Active_WITH_ORG("LAW01"));
                apiTest.createMembershipRule("Lehman College", apiTest.Employees_Active_AND_Retired_AND_Phase1_Provisioning_OR_Students_Active_WITH_ORG("LEH01"));
                apiTest.createMembershipRule("Medgar Evers College", apiTest.Employees_Active_AND_Retired_AND_Phase1_Provisioning_OR_Students_Active_WITH_ORG("MEC01"));
                apiTest.createMembershipRule("Medical School", apiTest.Employees_Active_AND_Retired_AND_Phase1_Provisioning_OR_Students_Active_WITH_ORG("MED01"));
                apiTest.createMembershipRule("Guttman Community College", apiTest.Employees_Active_AND_Retired_AND_Phase1_Provisioning_OR_Students_Active_WITH_ORG("NCC01"));
                apiTest.createMembershipRule("NYC College of Technology", apiTest.Employees_Active_AND_Retired_AND_Phase1_Provisioning_OR_Students_Active_WITH_ORG("NYT01"));
                apiTest.createMembershipRule("Employees (Active) & Phase 1 Provisioning", new SearchRule("GLBL_EMP_STATUS", "Active", SearchRule.Operator.EQUAL));
        apiTest.createMembershipRule("COCOM Employee", apiTest.Employees_Active_AND_Retired_AND_Phase1_Provisioning_OR_Students_Active_WITH_ORG("COCOM"));
        apiTest.createMembershipRule("Macaulay Honors College", apiTest.Employees_Active_AND_Retired_AND_Phase1_Provisioning_OR_Students_Active_WITH_ORG("MHC01"));
        apiTest.createMembershipRule("CUNY Student Suspended", new SearchRule("StudentSuspended", "TRUE", SearchRule.Operator.EQUAL));
        apiTest.createMembershipRule("BAR01 Employee Group", apiTest.campus_Employee_Membership_rule("BAR01", "BAR01_EMP"));

        apiTest.createMembershipRule("BCC01 Employee Group", apiTest.campus_Employee_Membership_rule("BCC01", "BCC01_EMP"));    
        apiTest.createMembershipRule("BKL01 Employee Group", apiTest.campus_Employee_Membership_rule("BKL01", "BKL01_EMP"));    
        apiTest.createMembershipRule("BMC01 Employee Group", apiTest.campus_Employee_Membership_rule("BMC01", "BMC01_EMP"));    
        apiTest.createMembershipRule("COSEN Employee Group", apiTest.campus_Employee_Membership_rule("COSEN", "COSEN_EMP"));    
        apiTest.createMembershipRule("CSI01 Employee Group", apiTest.campus_Employee_Membership_rule("CSI01", "CSI01_EMP"));    
        apiTest.createMembershipRule("CTY01 Employee Group", apiTest.campus_Employee_Membership_rule("CTY01", "CTY01_EMP"));    
        apiTest.createMembershipRule("GRD01 Employee Group", apiTest.campus_Employee_Membership_rule("GRD01", "GRD01_EMP"));    
        apiTest.createMembershipRule("HOS01 Employee Group", apiTest.campus_Employee_Membership_rule("HOS01", "HOS01_EMP"));    
        apiTest.createMembershipRule("HTR01 Employee Group", apiTest.campus_Employee_Membership_rule("HTR01", "HTR01_EMP"));    
        apiTest.createMembershipRule("JJC01 Employee Group", apiTest.campus_Employee_Membership_rule("JJC01", "JJC01_EMP"));    
        apiTest.createMembershipRule("KCC01 Employee Group", apiTest.campus_Employee_Membership_rule("KCC01", "KCC01_EMP"));    
        apiTest.createMembershipRule("LAG01 Employee Group", apiTest.campus_Employee_Membership_rule("LAG01", "LAG01_EMP"));    
        apiTest.createMembershipRule("LAW01 Employee Group", apiTest.campus_Employee_Membership_rule("LAW01", "LAW01_EMP"));    
        apiTest.createMembershipRule("LEH01 Employee Group", apiTest.campus_Employee_Membership_rule("LEH01", "LEH01_EMP"));    
        apiTest.createMembershipRule("MEC01 Employee Group", apiTest.campus_Employee_Membership_rule("MEC01", "MEC01_EMP"));    
        apiTest.createMembershipRule("MED01 Employee Group", apiTest.campus_Employee_Membership_rule("MED01", "MED01_EMP"));    
        apiTest.createMembershipRule("GCC01 Employee Group", apiTest.campus_Employee_Membership_rule("NCC01", "GuttmanCCEmp"));    
        apiTest.createMembershipRule("NYT01 Employee Group", apiTest.campus_Employee_Membership_rule("NYT01", "NYT01_EMP"));    
        apiTest.createMembershipRule("QCC01 Employee Group", apiTest.campus_Employee_Membership_rule("QCC01", "QCC01_EMP"));    
        apiTest.createMembershipRule("QNS01 Employee Group", apiTest.campus_Employee_Membership_rule("QNS01", "QNS01_EMP"));    
        apiTest.createMembershipRule("SOJ01 Employee Group", apiTest.campus_Employee_Membership_rule("SOJ01", "SOJ01_EMP"));    
        apiTest.createMembershipRule("SPH01 Employee Group", apiTest.campus_Employee_Membership_rule("SPH01", "SPH01_EMP"));    
        apiTest.createMembershipRule("SPS01 Employee Group", apiTest.campus_Employee_Membership_rule("SPS01", "SPS01_EMP"));    
        apiTest.createMembershipRule("YRK01 Employee Group", apiTest.campus_Employee_Membership_rule("YRK01", "YRK01_EMP"));    
        apiTest.createMembershipRule("EverMatriculated Group", new SearchRule("EVERMATRIC", "TRUE", SearchRule.Operator.EQUAL));    
        apiTest.createMembershipRule("Hunter Office 365", new SearchRule("HTR01_STU", "AC", SearchRule.Operator.EQUAL));    
        apiTest.createMembershipRule("Medgar Evers EAD Employees", new SearchRule("MEC01_EMP", "A", SearchRule.Operator.EQUAL));    
        
        apiTest.createMembershipRule("CUNY Employee Group with basic PeopleSoft Access in EP and HC", apiTest.CUNY_EMPLOYEE_GROUP_WITH_BASIC_ACCESS());    

    }


    public <T> T getService(Class<T> serviceClass,
                            OIMClient client) throws Exception {
        return client.getService(serviceClass);
    }

    

    
    //Employee Inactive and Not Termed
    private SearchRule Employee_Inactive_and_Not_Termed(){
        SearchRule rule1 = new SearchRule("GLBL_EMP_STATUS", "Inactive", SearchRule.Operator.EQUAL);
        SearchRule rule2 = new SearchRule("TERM", "TRUE", SearchRule.Operator.NOT_EQUAL);
        return combineRules(rule1, rule2, SearchRule.Operator.AND);
    }
    
    //Employees - Active or Retired or (Inactive & Term not TRUE)
    private SearchRule Employees_Active_or_Retired_or_Inactive_Term_not_TRUE(){
        SearchRule[] rules = new SearchRule[3];
        rules[0] = new SearchRule("GLBL_EMP_STATUS", "Active", SearchRule.Operator.EQUAL);
        rules[1] = new SearchRule("GLBL_EMP_STATUS", "Retired", SearchRule.Operator.EQUAL);
        rules[2] = Employee_Inactive_and_Not_Termed();
        return combineRules(rules, SearchRule.Operator.OR);
    }
    
    
    
    //Employees (Active & Retired) & Phase 1 Provisioning OR Students (Active)
    private SearchRule Employees_Active_AND_Retired_AND_Phase1_Provisioning_OR_Students_Active(){
        SearchRule[] rules = new SearchRule[2];
        rules[0] = new SearchRule("GLBL_STU_STATUS", "Active", SearchRule.Operator.EQUAL);
        rules[1] = Employees_Active_or_Retired_or_Inactive_Term_not_TRUE();
        return combineRules(rules, SearchRule.Operator.OR);
    }
    
    //Central Office
    private SearchRule Central_Office(){
        SearchRule[] rules = new SearchRule[2];
        rules[0] = new SearchRule(UserManagerConstants.AttributeName.USER_ORGANIZATION.getId(), "COSEN", SearchRule.Operator.EQUAL);
        rules[1] = Employees_Active_or_Retired_or_Inactive_Term_not_TRUE();
        return combineRules(rules, SearchRule.Operator.AND);
    }
    
    private SearchRule Employees_Active_AND_Retired_AND_Phase1_Provisioning_OR_Students_Active_WITH_ORG(String orgName){
        SearchRule[] rules = new SearchRule[2];
        rules[0] = new SearchRule(UserManagerConstants.AttributeName.USER_ORGANIZATION.getId(), orgName, SearchRule.Operator.EQUAL);
        rules[1] = Employees_Active_AND_Retired_AND_Phase1_Provisioning_OR_Students_Active();
        return combineRules(rules, SearchRule.Operator.AND);         
    }
    
    
    //Employees (TRUE) & Faculty (TRUE) & Phase 1 Provisioning
    private SearchRule Employees_TRUE_AND_Faculty_TRUE_AND_Phase1_Provisioning(){
        SearchRule[] rules = new SearchRule[2];
        rules[0] = new SearchRule("FACULTY", "TRUE", SearchRule.Operator.EQUAL);
        rules[1] = Employees_Active_or_InActive_AND_Term_Not_True();
        return combineRules(rules, SearchRule.Operator.AND);         
    }
    
    
    //Employees-Active or (InActive & Term Not True)
    private SearchRule Employees_Active_or_InActive_AND_Term_Not_True(){
        SearchRule[] rules = new SearchRule[2];
        rules[0] = new SearchRule("GLBL_EMP_STATUS", "Active", SearchRule.Operator.EQUAL);
        rules[1] = Employee_Inactive_and_Not_Termed();
        return combineRules(rules, SearchRule.Operator.OR); 
    }
    
    //CUNY Student Admit Group Membership
    private SearchRule CUNY_Student_Status_Group_Membership(String ststusUDFName){
        SearchRule[] rules = new SearchRule[2];
        rules[0] = new SearchRule("IsDeceasedUser", "TRUE", SearchRule.Operator.NOT_EQUAL);
        rules[1] = new SearchRule(ststusUDFName, "TRUE", SearchRule.Operator.EQUAL);
        return combineRules(rules, SearchRule.Operator.AND); 
    }
    private SearchRule combineRules(SearchRule rule1, SearchRule rule2, SearchRule.Operator operator){
        return new SearchRule(rule1, rule2, operator);
    }
    private SearchRule combineRules(SearchRule[] rules, SearchRule.Operator operator){
        SearchRule combRule = rules[0];
        SearchRule combRule1 = null;
        for(int i=1; i<rules.length;i++){
            combRule1 = combineRules(rules[i], combRule, operator);
            combRule = combRule1;
        }
        return combRule;
    }

    private void createMembershipRule(String roleName, SearchRule rule) throws Exception {
        OIMClient oimClient = loginWithCustomEnv(getValueFromConfig("ADMIN_USER_LOGIN"), getValueFromConfig("ADMIN_USER_PASSWORD"), getValueFromConfig("MANAGED_SERVER_URL"));

        SearchRule[] rules = new SearchRule[2];
        rules[0] = new SearchRule("IsDuplicateUser", "TRUE", SearchRule.Operator.NOT_EQUAL);
        rules[1] = rule;

        RoleManager roleManager = oimClient.getService(RoleManager.class);
        Role role = roleManager.getDetails(oracle.iam.identity.rolemgmt.api.RoleManagerConstants.RoleAttributeName.NAME.getId(), roleName, null);
        String roleKey = role.getEntityId();
        roleManager.setUserMembershipRule(roleKey, combineRules(rules, SearchRule.Operator.AND));
    }

    //BAR01 Employee Membership Rule
    private SearchRule campus_Employee_Membership_rule(String orgName, String udf_campus_employee){
        SearchRule[] rules = new SearchRule[3];
        rules[0] = new SearchRule(udf_campus_employee, "A", SearchRule.Operator.EQUAL);
        rules[1] = new SearchRule(UserManagerConstants.AttributeName.USER_ORGANIZATION.getId(), orgName, SearchRule.Operator.EQUAL);
        rules[2] = new SearchRule("IsDeceasedUser", "TRUE", SearchRule.Operator.NOT_EQUAL);
        return combineRules(rules, SearchRule.Operator.AND); 
    }

    //CUNY Employee Group with basic PeopleSoft Access in EP and HC
    private SearchRule CUNY_EMPLOYEE_GROUP_WITH_BASIC_ACCESS(){
        SearchRule[] rules = new SearchRule[2];
        rules[0] = new SearchRule("GLBL_EMP_STATUS", "Active", SearchRule.Operator.EQUAL);
        rules[1] = cuny_employee_in_grace_period();
        SearchRule first = combineRules(rules, SearchRule.Operator.OR); 
        
        rules = new SearchRule[2];
        rules[0] = first;
        rules[1] = new SearchRule("IsDeceasedUser", "TRUE", SearchRule.Operator.NOT_EQUAL);
        return combineRules(rules, SearchRule.Operator.AND);
        
    }


    private SearchRule cuny_employee_in_grace_period() {
        SearchRule[] rules = new SearchRule[2];
        rules[0] = new SearchRule("GLBL_EMP_STATUS", "Inactive", SearchRule.Operator.EQUAL);
        rules[1] = new SearchRule("TERM", "TRUE", SearchRule.Operator.NOT_EQUAL);
        return combineRules(rules, SearchRule.Operator.AND); 
    }
}


