package test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;

import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;

import java.sql.Connection;
import java.sql.DriverManager;

import java.sql.PreparedStatement;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.sql.SQLIntegrityConstraintViolationException;
import static oracle.iam.identity.utils.Constants.EMPTYPE;
import static oracle.iam.identity.utils.Constants.FIRSTNAME;
import static oracle.iam.identity.utils.Constants.LASTNAME;
import static oracle.iam.identity.utils.Constants.ORGKEY;
import oracle.iam.certification.api.CertificationService;

import Thor.API.Operations.tcExportOperationsIntf;
import Thor.API.Operations.tcImportOperationsIntf;
import Thor.API.Operations.tcUnauthenticatedOperationsIntf;
import Thor.API.tcResultSet;

import com.thortech.xl.vo.ddm.RootObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStreamReader;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import java.text.ParseException;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Pattern;

import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import javax.security.auth.login.LoginException;

import oracle.iam.catalog.api.CatalogService;
import oracle.iam.catalog.api.MetaDataDefinitionManager;
import oracle.iam.catalog.vo.Catalog;
import oracle.iam.catalog.vo.MetaData;
import oracle.iam.catalog.vo.MetaDataDefinition;
import oracle.iam.certification.api.CertificationService;
import oracle.iam.certification.vo.IDCEndPointAttributeValue;
import oracle.iam.certification.vo.IDCEndPointUser;
import oracle.iam.certification.vo.PaginationContext;
import oracle.iam.configservice.api.ConfigManager;
import oracle.iam.configservice.api.Constants.Encryption;
import oracle.iam.configservice.vo.AttributeDefinition;
import oracle.iam.identity.exception.NoSuchUserException;
import oracle.iam.identity.exception.UserManagerException;
import oracle.iam.identity.rolemgmt.api.RoleManager;
import oracle.iam.identity.rolemgmt.api.RoleManagerConstants;
import oracle.iam.identity.rolemgmt.vo.Role;
import oracle.iam.identity.usermgmt.api.UserManager;
import oracle.iam.identity.usermgmt.api.UserManagerConstants;
import oracle.iam.identity.usermgmt.api.UserManagerConstants.AttributeName;
import oracle.iam.identity.usermgmt.vo.User;
import oracle.iam.identity.usermgmt.vo.UserManagerResult;
import static oracle.iam.identity.utils.Constants.EMPTYPE;
import static oracle.iam.identity.utils.Constants.FIRSTNAME;
import static oracle.iam.identity.utils.Constants.LASTNAME;
import static oracle.iam.identity.utils.Constants.ORGKEY;
import oracle.iam.notification.api.NotificationService;
import oracle.iam.notification.vo.NotificationEvent;
import oracle.iam.passwordmgmt.api.ChallengeResponseService;
import oracle.iam.platform.OIMClient;
import oracle.iam.platform.Platform;
import oracle.iam.platform.authopss.api.PolicyConstants;
import oracle.iam.platform.authopss.vo.EntityPublication;
import oracle.iam.platform.entitymgr.EntityManager;
import oracle.iam.platform.entitymgr.vo.SearchCriteria;
import oracle.iam.platform.utils.vo.OIMType;
import oracle.iam.platformservice.api.ClientLoginSessionService;
import oracle.iam.platformservice.api.EntityPublicationService;
import oracle.iam.platformservice.api.PlatformService;
import oracle.iam.platformservice.api.PlatformUtilsService;
import oracle.iam.platformservice.vo.JarElement;
import oracle.iam.provisioning.api.EntitlementService;
import oracle.iam.provisioning.api.ProvisioningService;
import oracle.iam.provisioning.vo.Entitlement;
import oracle.iam.provisioning.vo.EntitlementInstance;
import oracle.iam.request.api.RequestService;
import oracle.iam.request.vo.Beneficiary;
import oracle.iam.request.vo.Request;
import oracle.iam.request.vo.RequestBeneficiaryEntity;
import oracle.iam.request.vo.RequestComment;
import oracle.iam.request.vo.RequestConstants;
import oracle.iam.request.vo.RequestData;
import oracle.iam.selfservice.self.selfmgmt.api.AuthenticatedSelfService;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Pattern;

import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.security.auth.login.LoginException;

import oracle.iam.catalog.api.CatalogService;
import oracle.iam.catalog.api.MetaDataDefinitionManager;
import oracle.iam.catalog.vo.Catalog;
import oracle.iam.catalog.vo.MetaData;
import oracle.iam.catalog.vo.MetaDataDefinition;
import oracle.iam.configservice.api.ConfigManager;
import oracle.iam.configservice.api.Constants.Encryption;
import oracle.iam.configservice.vo.AttributeDefinition;
import oracle.iam.identity.rolemgmt.api.RoleManager;
import oracle.iam.identity.rolemgmt.api.RoleManagerConstants;
import oracle.iam.identity.rolemgmt.vo.Role;
import oracle.iam.identity.usermgmt.api.UserManager;
import oracle.iam.identity.usermgmt.vo.User;
import oracle.iam.identity.usermgmt.vo.UserManagerResult;
import oracle.iam.notification.api.NotificationService;
import oracle.iam.notification.vo.NotificationEvent;
import oracle.iam.passwordmgmt.api.ChallengeResponseService;
import oracle.iam.platform.OIMClient;
import oracle.iam.platform.authopss.api.PolicyConstants;
import oracle.iam.platform.authopss.vo.EntityPublication;
import oracle.iam.platform.entitymgr.vo.SearchCriteria;
import oracle.iam.platform.utils.vo.OIMType;
import oracle.iam.platformservice.api.ClientLoginSessionService;
import oracle.iam.platformservice.api.EntityPublicationService;
import oracle.iam.platformservice.api.PlatformService;
import oracle.iam.platformservice.api.PlatformUtilsService;
import oracle.iam.platformservice.vo.JarElement;
import oracle.iam.provisioning.api.EntitlementService;
import oracle.iam.provisioning.api.ProvisioningService;
import oracle.iam.provisioning.vo.Account;
import oracle.iam.provisioning.vo.AccountData;
import oracle.iam.provisioning.vo.Entitlement;
import oracle.iam.request.api.RequestService;
import oracle.iam.request.vo.Beneficiary;
import oracle.iam.request.vo.RequestBeneficiaryEntity;
import oracle.iam.request.vo.RequestComment;
import oracle.iam.request.vo.RequestConstants;
import oracle.iam.request.vo.RequestData;
import oracle.iam.scheduler.api.SchedulerService;
import oracle.iam.scheduler.vo.JobHistory;
import oracle.iam.selfservice.self.selfmgmt.api.AuthenticatedSelfService;
import Thor.API.tcResultSet;
import Thor.API.Operations.tcExportOperationsIntf;
import Thor.API.Operations.tcITResourceInstanceOperationsIntf;
import Thor.API.Operations.tcImportOperationsIntf;
import Thor.API.Operations.tcUnauthenticatedOperationsIntf;

import com.thortech.xl.vo.ddm.RootObject;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;

import java.util.Properties;

import javax.security.auth.login.LoginException;

import oracle.iam.identity.usermgmt.api.UserManager;
import oracle.iam.identity.usermgmt.api.UserManagerConstants;
import oracle.iam.identity.usermgmt.vo.User;
import oracle.iam.identity.usermgmt.vo.UserManagerResult;
import static oracle.iam.identity.utils.Constants.EMPTYPE;
import static oracle.iam.identity.utils.Constants.FIRSTNAME;
import static oracle.iam.identity.utils.Constants.LASTNAME;
import static oracle.iam.identity.utils.Constants.ORGKEY;
import static oracle.iam.identity.utils.Constants.USERTYPE;
import oracle.iam.platform.OIMClient;

import oracle.iam.platform.Platform;

import oracle.iam.platform.context.ContextManager;



public class LoadPlugins {

    public LoadPlugins() throws Exception {
        String orclIDMLoc = System.getenv("INSTALL_LOC");
        System.setProperty("XL.HomeDir", orclIDMLoc + "/server");
        System.setProperty("java.security.policy",
                           orclIDMLoc + "/designconsole/config/xl.policy");
        System.setProperty("java.security.auth.login.config",
                           orclIDMLoc + "/designconsole/config/authwl.conf");
        System.setProperty("XL.ExtendedErrorOptions", "TRUE");
        System.setProperty("APPSERVER_TYPE", "wls");
    }

    private static String getValueFromConfig(String prKey) {
        String val = null;
        Properties prop = new Properties();
        InputStream input = null;
        try {
            input = new FileInputStream("config.properties");
            prop.load(input);
            val = (String)prop.getProperty(prKey);
        } catch (Exception ex) {
            System.out.println("Error occured for reading from config.");
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        System.out.println("Value " + val + " Key:" + prKey);
        return val;
    }

    private Connection getOIMDBConnection(){
        Connection oimDBConnection = null;
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            oimDBConnection = DriverManager.getConnection(getValueFromConfig("OIMDB_URL"), getValueFromConfig("OIM_DB_USER_NAME"), 
                                                          getValueFromConfig("OIM_DB_USER_PASSWD"));
        } catch (Exception e) {
                e.printStackTrace();
        }
        return oimDBConnection;
    }
    
    public OIMClient loginWithCustomEnv(String OIMUserName, String OIMPassword,
                                        String MANAGED_SERVER_NAMING_URL) throws LoginException {
        OIMClient client;
        Hashtable env = new Hashtable();
        env.put(OIMClient.JAVA_NAMING_FACTORY_INITIAL,
                OIMClient.WLS_CONTEXT_FACTORY);
        env.put(OIMClient.JAVA_NAMING_PROVIDER_URL, MANAGED_SERVER_NAMING_URL);
        client = new OIMClient(env);
        client.login(OIMUserName, OIMPassword.toCharArray());

        PlatformUtilsService service = (PlatformUtilsService) client.getService(
            oracle.iam.platformservice.api.PlatformUtilsService.class);
	try{
        	  service.purgeCache("All");
        	System.out.println("Purge Cache");  
	}
        catch(Exception e){
        	e.printStackTrace();
        }
        return client;
    }
    

    
    public static void main(String[] args) throws Exception {
        LoadPlugins apiTest = new LoadPlugins();
        apiTest.loadPlugins(getValueFromConfig("PLUGINS_DIR_PATH"));
    }


    public <T> T getService(Class<T> serviceClass,
                            OIMClient client) throws Exception {
        return client.getService(serviceClass);
    }

    private void loadPlugins(String directory) throws Exception {
    
        File dir = new File(directory);
        File[] filesList = dir.listFiles();
        OIMClient oimClient = loginWithCustomEnv(getValueFromConfig("ADMIN_USER_LOGIN"), getValueFromConfig("ADMIN_USER_PASSWORD"),
                               getValueFromConfig("MANAGED_SERVER_URL"));
     PlatformService serviceQA =
      oimClient.getService(PlatformService.class);
        for (File zipFile : filesList) {
            if (zipFile.isFile()) {
                try {
                    if (zipFile.getName().endsWith(".zip")) {
                        FileInputStream fis = new FileInputStream(zipFile);
                        int size = (int)zipFile.length();
                        byte[] b = new byte[size];
                        int bytesRead = fis.read(b, 0, size);
                        while (bytesRead < size) {
                            bytesRead +=
                                    fis.read(b, bytesRead, size - bytesRead);
                        }
                        fis.close();
                        System.out.println(zipFile.getName());
                       serviceQA.registerPlugin(b);
                    }
                } catch (Throwable e) {
                    System.out.println("Not Done "+zipFile.getName());
		    e.printStackTrace();
                }
            }
        }


    }

}

