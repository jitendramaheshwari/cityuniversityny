package test;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;


public class DeleteDeletedUsers {

    private Connection getOIMDBConnection(){
        Connection oimDBConnection = null;
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            oimDBConnection = DriverManager.getConnection("jdbc:oracle:thin:@//imsdevdbv.cuny.edu:1521/vdIMOIM.imsdevdbv.cuny.edu", "violet_oim", "zoora7Ee");
        } catch (Exception e) {
                e.printStackTrace();
        }
        return oimDBConnection;
    }

    public DeleteDeletedUsers() throws Exception {
    }


    public static void main(String[] args) {
        DeleteDeletedUsers deleteUsers =null;

        try {
            deleteUsers = new DeleteDeletedUsers();
        } catch (Exception e) {
        }
        deleteUsers.findDeleted();
        //deleteUsers.findCustomDeleted();
    }


    public void findDeleted() {
        Statement statement = null;
        String deletedOIMUsers =
            "select usr_key from USR where usr_key >25";
        Connection connection = getOIMDBConnection();
        ResultSet rs = null;
        try {
            statement = connection.createStatement();
            rs = statement.executeQuery(deletedOIMUsers);
            while (rs.next()) {
                long usrKey = rs.getLong("USR_KEY");
                deleteByKey(usrKey, connection);
            }
            rs.close();
            statement.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null)
                    statement.close();
            } catch (SQLException se2) {
            }
            try {
                if (rs != null)
                    rs.close();
            } catch (SQLException se2) {
            }

            try {
                if (connection != null)
                    connection.close();
            } catch (SQLException se2) {
            }
        }
    }

    public void deleteByKey(long usrKey, Connection connection) throws Exception {
        Statement statement = null;

        try {
            statement = connection.createStatement();

            String RECON_USER_MATCH = "delete from RECON_USER_MATCH where usr_key =" + usrKey;
            String RECON_ACCOUNT_MATCH = "delete from RECON_ACCOUNT_MATCH where re_key in (select re_key from recon_events where usr_key = "+ usrKey + ")";
            String RECON_ACCOUNT_MATCH1 = "delete from RECON_ACCOUNT_MATCH where orc_KEY in (select orc_key from orc where usr_key =" + usrKey + ")";

            String RECON_ORG_MATCH = "delete from RECON_ORG_MATCH where re_key in (select re_key from recon_events where usr_key = "+ usrKey + ")";
            String RECON_ROLE_MATCH = "delete from RECON_ROLE_MATCH where re_key in (select re_key from recon_events where usr_key = "+ usrKey + ")";
            String RECON_ROLE_HIERARCHY_MATCH = "delete from RECON_ROLE_HIERARCHY_MATCH where re_key in (select re_key from recon_events where usr_key = "+ usrKey + ")";
            String RECON_ROLE_MEMBER_MATCH1 = "delete from RECON_ROLE_MEMBER_MATCH where re_key in (select re_key from recon_events where usr_key = "+ usrKey + ")";
            String RECON_ROLE_MEMBER_MATCH2 = "delete from RECON_ROLE_MEMBER_MATCH where usr_key = "+ usrKey;
            String RECON_EVENT_ASSIGNMENT1 = "delete from RECON_EVENT_ASSIGNMENT where REA_ADMIN_USR_KEY = "+ usrKey;
            String RECON_EVENT_ASSIGNMENT2 = "delete from RECON_EVENT_ASSIGNMENT where re_key in (select re_key from recon_events where usr_key = "+ usrKey + ")";
            String RECON_CHILD_MATCH = "delete from RECON_CHILD_MATCH where re_key in (select re_key from recon_events where usr_key = "+ usrKey + ")";
            String RECON_HISTORY = "delete from RECON_HISTORY where re_key in (select re_key from recon_events where usr_key = "+ usrKey + ")";
            String RECON_EXCEPTIONS1 = "delete from RECON_EXCEPTIONS where re_key in (select re_key from recon_events where usr_key = "+ usrKey + ")";
            String RECON_EXCEPTIONS2 = "delete from RECON_EXCEPTIONS where usr_key =" + usrKey;
            
            String ENT_ASSIGN = "delete from ent_assign where usr_key = " + usrKey;
            
            String ENT_ASSIGN_HIST = "delete from ent_assign_hist where usr_key = " + usrKey;

            
            
            String OUD =
                "delete from oud where oud.OUD_PARENT_OIU_KEY in " + "(select oiu_key from oiu where oiu.usr_key = " +
                usrKey + ")";
            String OUD1 =
                "delete from oud where oiu_key in " + "(select oiu_key from oiu where oiu.usr_key = " +
                usrKey + ")";


            String OIU =
                "delete from oiu where oiu.usr_key = " + usrKey ;

            String OIU2 =
                "delete from oiu where oiu.usr_key = " + usrKey;

            String OSI =
                "delete from osi where osi.orc_key in (select orc_key from orc where " +
                " orc.usr_key =" + usrKey + ")";

            String OSI5 = "DELETE FROM osi WHERE req_key IN (SELECT req_key FROM req WHERE orc_key IN (SELECT orc_key FROM orc WHERE usr_key = " + usrKey+ "))";


            String OIO =
                "delete from oio where oio.orc_KEY in (select orc_key from orc where " +
                "orc.usr_key =" +
                usrKey + ")";

            String RA_LDAPUSER =
                "delete from RA_LDAPUSER where re_key in (select re_key from recon_events where usr_key = "+ usrKey + ")";

            String recon_EVENTS =
                "delete from recon_events where usr_key=" +
                usrKey;
            String RECON_USER_OLDSTATE =
                    "delete from RECON_USER_OLDSTATE where usr_key=" +
                    usrKey;

            String UD_ADUSRC = "DELETE FROM UD_ADUSRC WHERE orc_key IN (SELECT orc_key FROM orc WHERE usr_key = " + usrKey +")";
            String UD_ADUSER = "DELETE FROM UD_ADUSER WHERE orc_key IN (SELECT orc_key FROM orc WHERE  usr_key = " + usrKey +")";

            String UD_EBS_RESP = "DELETE FROM UD_EBS_RESP WHERE orc_key IN (SELECT orc_key FROM orc WHERE usr_key = " + usrKey +")";
            String UD_EBS_RLS = "DELETE FROM UD_EBS_RLS WHERE orc_key IN (SELECT orc_key FROM orc WHERE  usr_key = " + usrKey +")";
            String UD_EBS_USER = "DELETE FROM UD_EBS_USER WHERE orc_key IN (SELECT orc_key FROM orc WHERE  usr_key = " + usrKey +")";
            
            String UD_EMADUSRC = "DELETE FROM UD_EMADUSRC WHERE orc_key IN (SELECT orc_key FROM orc WHERE usr_key = " + usrKey +")";
            String UD_EMADUSER = "DELETE FROM UD_EMADUSER WHERE orc_key IN (SELECT orc_key FROM orc WHERE  usr_key = " + usrKey +")";
            
            String UD_EXCHANGE = "DELETE FROM UD_EXCHANGE WHERE orc_key IN (SELECT orc_key FROM orc WHERE usr_key = " + usrKey +")";


            
            String ORC =
                "delete from orc where orc.usr_key =" + usrKey;

            String USG =
                "delete from usg where usg.usr_key =" + usrKey;

            String PCQ =
                "delete from pcq where pcq.usr_key=" + usrKey;
            String EMD = "DELETE FROM emd WHERE usr_key = " + usrKey;
            String ORG_USER_MEMBERSHIPS = "delete from ORG_USER_MEMBERSHIPS where usr_key = " + usrKey;
            String PENDING_ROLE_GRANTS = "delete from PENDING_ROLE_GRANTS where usr_key = " + usrKey;
            String PWH = "delete from PWH where usr_key = " + usrKey;
            String REQ = "delete from REQ where usr_key = " + usrKey;
            String RML = "delete from RML where usr_key = " + usrKey;

            String OSH =
                "delete from osh where osh.OSH_ASSIGNED_BY_USR_KEY =" + usrKey;

            String OSH2 =
                "delete from osh where osh.OSH_ASSIGNED_TO_USR_KEY =" + usrKey;

            String UPD =
                "delete from upd where upd.upp_key in (select upp_key from upp where " +
                "upp.usr_key =" + usrKey + ")";

            String UPP =
                "delete from upp where upp.usr_key =" + usrKey;

            String OSI2 =
                "delete from osi where osi.OSI_ASSIGNED_TO_USR_KEY =" + usrKey;

            String RQU =
                "delete from rqu where rqu.usr_key =" + usrKey;
            
            String OTI = "DELETE FROM oti WHERE orc_key IN (SELECT orc_key FROM orc WHERE usr_key = "+ usrKey + ")";

            String UHD =
                "delete from uhd where uhd.uph_key in (select uph_key from uph where " +
                "uph.usr_key=" +  usrKey + ")";

            String UPH =
                "delete from uph where uph.usr_key =" + usrKey;

            String USG2 =
                "delete from usg where usg.usr_key =" + usrKey;

            String OUD2 =
                "delete from oud where oud.OUD_PARENT_OIU_KEY in (select oiu_key from " +
                "oiu where oiu.usr_key =" + usrKey + ")";

            String OIU3 =
                "delete from oiu where oiu.usr_key =" + usrKey;

            String OIO2 =
                "delete from oio where oio.orc_KEY in (select orc_key from orc where " +
                "orc.usr_key =" +                usrKey + ")";

            String OSI3 =
                "delete from osi where osi.orc_KEY in (select orc_key from orc where " +
                "orc.usr_key =" +                usrKey + ")";

            String ORC2 =
                "delete from orc where orc.usr_key =" + usrKey;

            String UPD2 =
                "delete from upd where upd.upp_key in (select upp_key from upp where " +
                "upp.usr_key =" +                usrKey + ")";

            String UPP2 =
                "delete from upp where upp.usr_key =" + usrKey;

            String OSH3 =
                "delete from osh where OSH_ASSIGNED_BY_USR_KEY =" + usrKey;

            String OSH4 =
                "delete from osh where OSH_ASSIGNED_TO_USR_KEY =" + usrKey;

            String PCQ2 =
                "delete from pcq where pcq.usr_key =" + usrKey;

            String OSI4 =
                "delete from osi where OSi_ASSIGNED_TO_USR_KEY=" + usrKey;

            String userprov = "delete from user_provisioning_attrs where usr_key="+usrKey;

        	String UPA = "DELETE FROM UPA WHERE usr_key =" + usrKey;
        	String upa_fields = "DELETE FROM upa_fields WHERE upa_usr_key IN (SELECT upa_usr_key FROM upa_usr WHERE usr_key ="+usrKey+")";
        	String UPA_GRP_MEMBERSHIP = "DELETE FROM UPA_GRP_MEMBERSHIP WHERE upa_usr_key IN (SELECT upa_usr_key FROM upa_usr WHERE usr_key = "+usrKey+")";
        	String upa_ud_formfields = "DELETE FROM upa_ud_formfields WHERE upa_ud_forms_key IN (SELECT upa_ud_forms_key FROM upa_ud_forms WHERE upa_resource_key IN (SELECT upa_resource_key FROM upa_resource WHERE upa_usr_key IN (SELECT upa_usr_key FROM upa_usr WHERE usr_key ="+usrKey+")))";
        	String 	upa_ud_forms =	"DELETE FROM upa_ud_forms WHERE upa_resource_key IN (SELECT upa_resource_key FROM upa_resource WHERE upa_usr_key IN (SELECT upa_usr_key FROM upa_usr WHERE usr_key ="+usrKey+"))";
        	String UPA_RESOURCE = "DELETE FROM UPA_RESOURCE WHERE upa_usr_key IN (SELECT upa_usr_key FROM upa_usr WHERE usr_key ="+usrKey+")";
        	String UPA_USR = "DELETE FROM UPA_USR WHERE usr_key =" + usrKey; 

            String PXD1 = "delete from pxd where pxd_orig_usr_key = " + usrKey;
            String PXD2 = "delete from pxd where pxd_proxy_key =" + usrKey;
            
            String updateUGP = "update ugp set ugp_role_owner_key = null where ugp_role_owner_key = " + usrKey;
            String org_user_memberships = "delete from org_user_memberships where usr_key=" + usrKey;
            String USR = "delete from usr where usr_key=" + usrKey;
            System.out.println("Query updateUGP = " + updateUGP);
            statement.execute(updateUGP);


            System.out.println("Query RECON_USER_MATCH = " + RECON_USER_MATCH);
            statement.execute(RECON_USER_MATCH);
            System.out.println("Query RECON_ACCOUNT_MATCH = " + RECON_ACCOUNT_MATCH);
            statement.execute(RECON_ACCOUNT_MATCH);
            System.out.println("Query RECON_ACCOUNT_MATCH1 = " + RECON_ACCOUNT_MATCH1);
            statement.execute(RECON_ACCOUNT_MATCH1);

            System.out.println("Query RECON_ORG_MATCH = " + RECON_ORG_MATCH);
            statement.execute(RECON_ORG_MATCH);
            System.out.println("Query RECON_ROLE_MATCH = " + RECON_ROLE_MATCH);
            statement.execute(RECON_ROLE_MATCH);
            System.out.println("Query RECON_ROLE_HIERARCHY_MATCH = " + RECON_ROLE_HIERARCHY_MATCH);
            statement.execute(RECON_ROLE_HIERARCHY_MATCH);
            System.out.println("Query RECON_ROLE_MEMBER_MATCH1 = " + RECON_ROLE_MEMBER_MATCH1);
            statement.execute(RECON_ROLE_MEMBER_MATCH1);
            System.out.println("Query RECON_ROLE_MEMBER_MATCH2 = " + RECON_ROLE_MEMBER_MATCH2);
            statement.execute(RECON_ROLE_MEMBER_MATCH2);
            System.out.println("Query RECON_EVENT_ASSIGNMENT1 = " + RECON_EVENT_ASSIGNMENT1);
            statement.execute(RECON_EVENT_ASSIGNMENT1);
            System.out.println("Query RECON_EVENT_ASSIGNMENT2 = " + RECON_EVENT_ASSIGNMENT2);
            statement.execute(RECON_EVENT_ASSIGNMENT2);
            System.out.println("Query RECON_CHILD_MATCH = " + RECON_CHILD_MATCH);
            statement.execute(RECON_CHILD_MATCH);
            System.out.println("Query RECON_HISTORY = " + RECON_HISTORY);
            statement.execute(RECON_HISTORY);
            System.out.println("Query RECON_EXCEPTIONS1 = " + RECON_EXCEPTIONS1);
            statement.execute(RECON_EXCEPTIONS1);
            System.out.println("Query RECON_EXCEPTIONS2 = " + RECON_EXCEPTIONS2);
            statement.execute(RECON_EXCEPTIONS2);
            System.out.println("Query ENT_ASSIGN = " + ENT_ASSIGN);
            statement.execute(ENT_ASSIGN);
            System.out.println("Query ENT_ASSIGN_HIST = " + ENT_ASSIGN_HIST);
            statement.execute(ENT_ASSIGN_HIST);


            System.out.println("Query UPA = " + UPA);
            statement.execute(UPA);
            System.out.println("Query upa_fields = " + upa_fields);
            statement.execute(upa_fields);
            System.out.println("Query UPA_GRP_MEMBERSHIP = " + UPA_GRP_MEMBERSHIP);
            statement.execute(UPA_GRP_MEMBERSHIP);
            System.out.println("Query upa_ud_formfields = " + upa_ud_formfields);
            statement.execute(upa_ud_formfields);
            System.out.println("Query upa_ud_forms = " + upa_ud_forms);
            statement.execute(upa_ud_forms);
            System.out.println("Query UPA_RESOURCE = " + UPA_RESOURCE);
            statement.execute(UPA_RESOURCE);
            System.out.println("Query UPA_USR = " + UPA_USR);
            statement.execute(UPA_USR);
            
            System.out.println("Query OUD = " + OUD);
            statement.execute(OUD);
            System.out.println("Query OUD1 = " + OUD1);
            statement.execute(OUD1);
            System.out.println("Query OIU = " + OIU);
            statement.execute(OIU);
            System.out.println("Query OIU2 = " + OIU2);
            statement.execute(OIU2);
            System.out.println("Query OSI = " + OSI);
            statement.execute(OSI);
            System.out.println("Query OSI5 = " + OSI5);
            statement.execute(OSI5);

            System.out.println("Query OIO = " + OIO);
            statement.execute(OIO);
            System.out.println("Query RA_LDAPUSER = " + RA_LDAPUSER);
            statement.execute(RA_LDAPUSER);

            System.out.println("Query RECON_EVENTS = " + recon_EVENTS);
            statement.execute(recon_EVENTS);
            System.out.println("Query RECON_USER_OLDSTATE = " + RECON_USER_OLDSTATE);
            statement.execute(RECON_USER_OLDSTATE);
            System.out.println("Query UD_ADUSRC = " + UD_ADUSRC);
            //statement.execute(UD_ADUSRC);

            System.out.println("Query UD_ADUSER = " + UD_ADUSER);
            //statement.execute(UD_ADUSER);

            System.out.println("Query UD_EBS_RESP = " + UD_EBS_RESP);
            //statement.execute(UD_EBS_RESP);

            System.out.println("Query UD_EBS_RLS = " + UD_EBS_RLS);
            //statement.execute(UD_EBS_RLS);

            System.out.println("Query UD_EBS_USER = " + UD_EBS_USER);
            //statement.execute(UD_EBS_USER);

            System.out.println("Query UD_EMADUSRC = " + UD_EMADUSRC);
            //statement.execute(UD_EMADUSRC);

            System.out.println("Query UD_EMADUSER = " + UD_EMADUSER);
            //statement.execute(UD_EMADUSER);

            System.out.println("Query UD_EXCHANGE = " + UD_EXCHANGE);
            //statement.execute(UD_EXCHANGE);

            System.out.println("Query OTI = " + OTI);
            statement.execute(OTI);

            System.out.println("Query ORC = " + ORC);
            statement.execute(ORC);
            System.out.println("Query USG = " + USG);
            statement.execute(USG);
            System.out.println("Query PCQ = " + PCQ);
            statement.execute(PCQ);
            System.out.println("Query EMD = " + EMD);
            statement.execute(EMD);
            System.out.println("Query ORG_USER_MEMBERSHIPS = " + ORG_USER_MEMBERSHIPS);
            statement.execute(ORG_USER_MEMBERSHIPS);
            System.out.println("Query PENDING_ROLE_GRANTS = " + PENDING_ROLE_GRANTS);
            statement.execute(PENDING_ROLE_GRANTS);
            System.out.println("Query PWH = " + PWH);
            statement.execute(PWH);
            System.out.println("Query REQ = " + REQ);
            statement.execute(REQ);
            System.out.println("Query RML = " + RML);
            statement.execute(RML);
            
            System.out.println("Query OSH = " + OSH);
            statement.execute(OSH);
            System.out.println("Query OSH2 = " + OSH2);
            statement.execute(OSH2);
            System.out.println("Query UPD = " + UPD);
            statement.execute(UPD);
            System.out.println("Query UPP = " + UPP);
            statement.execute(UPP);
            System.out.println("Query OSI2 = " + OSI2);
            statement.execute(OSI2);
            System.out.println("Query RQU = " + RQU);

            statement.execute(RQU);
            System.out.println("Query UHD = " + UHD);
            statement.execute(UHD);
            System.out.println("Query USG2 = " + USG2);
            statement.execute(USG2);
            System.out.println("Query UPH = " + UPH);
            statement.execute(UPH);
            System.out.println("Query OUD2 = " + OUD2);
            statement.execute(OUD2);
            System.out.println("Query OIU3 = " + OIU3);
            statement.execute(OIU3);
            System.out.println("Query OIO2 = " + OIO2);
            statement.execute(OIO2);
            System.out.println("Query OSI3 = " + OSI3);
            statement.execute(OSI3);
            System.out.println("Query ORC2 = " + ORC2);
            statement.execute(ORC2);
            System.out.println("Query UPD2 = " + UPD2);
            statement.execute(UPD2);
            System.out.println("Query OSH3 = " + OSH3);
            statement.execute(OSH3);
            System.out.println("Query UPP2 = " + UPP2);
            statement.execute(UPP2);
            System.out.println("Query OSH4 = " + OSH4);
            statement.execute(OSH4);
            System.out.println("Query PCQ2 = " + PCQ2);
            statement.execute(PCQ2);
            System.out.println("Query OSI4 = " + OSI4);
            statement.execute(OSI4);
            System.out.println("Query userprov = " + userprov);
            statement.execute(userprov);
            System.out.println("Query PXD1 = " + PXD1);
            statement.execute(PXD1);
            System.out.println("Query PXD2 = " + PXD2);
            statement.execute(PXD2);
            
            System.out.println("Query org_user_memberships = " + org_user_memberships);
            statement.execute(org_user_memberships);
            

            System.out.println("Query USR = " + USR);
            statement.execute(USR);

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
            
        } finally {
            try {
                if (statement != null)
                    statement.close();
            } catch (Exception se2) {
            }
        }
    }
/**
 * '
select distinct('DELETE from ' || recon_table_name || ';') from RECON_tables

DELETE from RA_QMAI_LIC;
DELETE from RA_QMAILOFFICE365U915CA544;
DELETE from RA_HCMPSRLE;
DELETE from RA_HCM_MAIL;
DELETE from RA_ADGROUPA80D3C22;
DELETE from RA_ADUSERTRUSTED990BE67F;
DELETE from RA_OFFICE365USERC6A27F5;
DELETE from RA_QMAILOFFICE365GE3AD60FD;
DELETE from RA_RHC_MAIL;
DELETE from RA_FINPSRLE;
DELETE from RA_OID_GRP;
DELETE from RA_OIDGROUPA834E469;
DELETE from RA_QUADUSRC;
DELETE from RA_HUADUSRC;
DELETE from RA_UADGROUPE2C61777;
DELETE from RA_O365_UGP;
DELETE from RA_OFFICE365USERTR75B1ABAC;
DELETE from RA_QMAI_UGP;
DELETE from RA_EPM_MAIL;
DELETE from RA_CRMPEOPLESOFTUSEBADE1F0;
DELETE from RA_HUNTERADORGANIZA76897E9;
DELETE from RA_ADORGANIZATIONA9299A449;
DELETE from RA_ADUSERE469E5C8;
DELETE from RA_OFFICE365GROUP80153F95;
DELETE from RA_OIDTRUSTEDUSERBCBD344A;
DELETE from RA_CRMPSRLE;
DELETE from RA_CRM_MAIL;
DELETE from RA_UADUSER622DCBD3;
DELETE from RA_UADUSRC;
DELETE from RA_QMAI_URO;
DELETE from RAMLS_QMAILOFFI915CA544;
DELETE from RA_PEOPLESOFTUSERAC085C52;
DELETE from RA_FIN_MAIL;
DELETE from RA_HCMPEOPLESOFTUS6B758AE4;
DELETE from RA_EAD_UIDS;
DELETE from RA_OIDORGANISATIONC6151414;
DELETE from RA_ENPPEOPLESOFTUS66B8B6F9;
DELETE from RA_ENP_MAIL;
DELETE from RA_QUEENSADORGANIZ75890A32;
DELETE from RA_QUEENSADUSERTRU21B364B6;
DELETE from RA_UADORGANIZATION351186DE;
DELETE from RA_UADUSERTRUSTED7E7A918A;
DELETE from RA_XELLERATE_ORG;
DELETE from RA_QMAILOFFICE365UDE14398D;
DELETE from RA_PS_EMAIL;
DELETE from RA_PEOPLESOFTHRMSAC01F497;
DELETE from RA_RFIPSRLE;
DELETE from RA_RFI_MAIL;
DELETE from RA_ELMPSRLE;
DELETE from RA_EPMPEOPLESOFTUSFDDEE134;
DELETE from RA_EPMPSRLE;
DELETE from RA_QUEENSADGROUP7AB93DCB;
DELETE from RA_QUEENSADUSER5ED28AFF;
DELETE from RA_HUNTERADUSEREA462628;
DELETE from RA_ADUSRC;
DELETE from RA_O365_LIC;
DELETE from RAMLS_OFFICE36575B1ABAC;
DELETE from RA_PSROLES;
DELETE from RA_RFIPEOPLESOFTUS7F2B8AC7;
DELETE from RA_RHCPEOPLESOFTUS24541AFF;
DELETE from RA_RHCPSRLE;
DELETE from RA_ELMPEOPLESOFTUSEB9758B8;
DELETE from RA_ELM_MAIL;
DELETE from RA_OIDUSER9A1898A1;
DELETE from RA_ENPPSRLE;
DELETE from RA_HUNTERADGROUP5DB907C2;
DELETE from RA_O365_URO;
DELETE from RA_FINPEOPLESOFTUSBC35F21D;
DELETE from RA_HUNTERADUSERTRUB851C6DF;


        DELETE FROM RECON_USER_MATCH;
	DELETE FROM RECON_ACCOUNT_MATCH;
	DELETE FROM RECON_ORG_MATCH;
	DELETE FROM RECON_ROLE_MATCH;
	DELETE FROM RECON_ROLE_HIERARCHY_MATCH;
	DELETE FROM RECON_ROLE_MEMBER_MATCH;
	DELETE FROM RECON_EVENT_ASSIGNMENT;
	DELETE FROM RECON_CHILD_MATCH;
	DELETE FROM RECON_HISTORY;
        DELETE FROM RECON_BATCHES;
        DELETE FROM RECON_EXCEPTIONS;
	DELETE FROM RECON_EVENTS;
        DELETE FROM RECON_BATCHES;
        commit;
SELECT 'alter index ' || index_name || ' rebuild;' FROM  USER_INDEXES where table_owner = 'VIOLET_OIM' and table_name in ('USR','RECON_USER_MATCH', 'RECON_ACCOUNT_MATCH', 'RECON_ORG_MATCH',  'RECON_ROLE_MATCH', 'RECON_ROLE_HIERARCHY_MATCH', 'RECON_ROLE_MEMBER_MATCH', 'RECON_EVENT_ASSIGNMENT', 'RECON_CHILD_MATCH', 'RECON_HISTORY', 'RECON_BATCHES', 'RECON_EXCEPTIONS', 'RECON_EVENTS', 'RECON_BATCHES')
UNION ALL
SELECT 'alter index ' || index_name || ' rebuild;' FROM  USER_INDEXES where table_owner = 'VIOLET_OIM' and table_name in (
select recon_table_name from RECON_tables
)


--Default tables from 9g
	DELETE FROM oud WHERE oiu_key IN (SELECT oiu_key FROM oiu WHERE usr_key IN (SELECT usr_key FROM usr WHERE usr_key NOT IN ('1', '2', '3', '4', '5')));
	DELETE FROM oiu WHERE usr_key IN (SELECT usr_key FROM usr WHERE usr_key NOT IN ('1', '2', '3', '4', '5'));
	DELETE FROM oio WHERE orc_key IN (SELECT orc_key FROM orc,usr WHERE orc.usr_key = usr.usr_key and usr.usr_key NOT IN ('1', '2', '3', '4', '5'));
	DELETE FROM osi WHERE orc_key IN (SELECT orc_key FROM orc,usr WHERE orc.usr_key = usr.usr_key and usr.usr_key NOT IN ('1', '2', '3', '4', '5'));
	DELETE FROM osi WHERE req_key IN (SELECT req_key FROM req WHERE orc_key IN (SELECT orc_key FROM orc,usr WHERE orc.usr_key = usr.usr_key and usr.usr_key NOT IN ('1', '2', '3', '4', '5')));
	DELETE FROM osi WHERE osi_assigned_to_usr_key IN (SELECT usr_key FROM usr WHERE usr_key NOT IN ('1', '2', '3', '4', '5'));
	DELETE FROM osh WHERE osh_assigned_to_usr_key IN (SELECT usr_key FROM usr WHERE usr_key NOT IN ('1', '2', '3', '4', '5'));
	DELETE FROM rcd WHERE rce_key IN (SELECT rce_key FROM rce,orc,usr WHERE rce.orc_key = orc.orc_key and orc.usr_key = usr.usr_key and usr.usr_key NOT IN ('1', '2', '3', '4', '5'));
	DELETE FROM rch WHERE rce_key IN (SELECT rce_key FROM rce,orc,usr WHERE rce.orc_key = orc.orc_key and orc.usr_key = usr.usr_key and usr.usr_key NOT IN ('1', '2', '3', '4', '5'));
	DELETE FROM rcu WHERE rce_key IN (SELECT rce_key FROM rce,orc,usr WHERE rce.orc_key = orc.orc_key and orc.usr_key = usr.usr_key and usr.usr_key NOT IN ('1', '2', '3', '4', '5'));
	DELETE FROM rcb WHERE rce_key IN (SELECT rce_key FROM rce,orc,usr WHERE rce.orc_key = orc.orc_key and orc.usr_key = usr.usr_key and usr.usr_key NOT IN ('1', '2', '3', '4', '5'));
	DELETE FROM rcp WHERE orc_key IN (SELECT orc_key FROM orc,usr WHERE orc.usr_key = usr.usr_key and usr.usr_key NOT IN ('1', '2', '3', '4', '5'));
	DELETE FROM rpc WHERE rce_key IN (SELECT rce_key FROM rce,orc,usr WHERE rce.orc_key = orc.orc_key and orc.usr_key = usr.usr_key and usr.usr_key NOT IN ('1', '2', '3', '4', '5'));
	DELETE FROM rcm WHERE rce_key IN (SELECT rce_key FROM rce,orc,usr WHERE rce.orc_key = orc.orc_key and orc.usr_key = usr.usr_key and usr.usr_key NOT IN ('1', '2', '3', '4', '5'));
	DELETE FROM rce WHERE orc_key IN (SELECT orc_key FROM orc,usr WHERE orc.usr_key = usr.usr_key and usr.usr_key NOT IN ('1', '2', '3', '4', '5'));
	DELETE FROM rqu WHERE usr_key IN (SELECT usr_key FROM usr WHERE usr_key NOT IN ('1', '2', '3', '4', '5'));
	DELETE FROM oti WHERE orc_key IN (SELECT orc_key FROM orc,usr WHERE orc.usr_key = usr.usr_key and usr.usr_key NOT IN ('1', '2', '3', '4', '5'));
	DELETE FROM orc WHERE usr_key IN (SELECT usr_key FROM usr WHERE usr_key NOT IN ('1', '2', '3', '4', '5'));
	DELETE FROM upd WHERE upp_key IN (SELECT upp_key FROM upp,usr WHERE upp.usr_key = usr.usr_key and usr.usr_key NOT IN ('1', '2', '3', '4', '5'));
	DELETE FROM upp WHERE usr_key IN (SELECT usr_key FROM usr WHERE usr_key NOT IN ('1', '2', '3', '4', '5'));
	DELETE FROM usg WHERE usr_key IN (SELECT usr_key FROM usr WHERE usr_key NOT IN ('1', '2', '3', '4', '5'));
	DELETE FROM uhd WHERE uph_key IN (SELECT uph_key FROM uph,usr WHERE uph.usr_key = usr.usr_key and usr.usr_key NOT IN ('1', '2', '3', '4', '5'));
	DELETE FROM uph WHERE usr_key IN (SELECT usr_key FROM usr WHERE usr.usr_key NOT IN ('1', '2', '3', '4', '5'));
	DELETE FROM pcq WHERE usr_key IN (SELECT usr_key FROM usr WHERE usr.usr_key NOT IN ('1', '2', '3', '4', '5'));
	DELETE FROM rcu WHERE usr_key IN (SELECT usr_key FROM usr WHERE usr.usr_key NOT IN ('1', '2', '3', '4', '5'));
-- Required additions to 11gR2PS2
	DELETE FROM emd WHERE usr_key IN (SELECT usr_key FROM usr WHERE usr_key NOT IN ('1', '2', '3', '4', '5'));
	DELETE FROM ORG_USER_MEMBERSHIPS WHERE usr_key IN (SELECT usr_key FROM usr WHERE usr_key NOT IN ('1', '2', '3', '4', '5'));
	DELETE FROM PENDING_ROLE_GRANTS WHERE usr_key IN (SELECT usr_key FROM usr WHERE usr_key NOT IN ('1', '2', '3', '4', '5'));
	DELETE FROM PWH WHERE usr_key IN (SELECT usr_key FROM usr WHERE usr_key NOT IN ('1', '2', '3', '4', '5'));
	DELETE FROM REQ WHERE usr_key IN (SELECT usr_key FROM usr WHERE usr_key NOT IN ('1', '2', '3', '4', '5'));
	DELETE FROM RML WHERE usr_key IN (SELECT usr_key FROM usr WHERE usr_key NOT IN ('1', '2', '3', '4', '5'));
	DELETE FROM USER_PROVISIONING_ATTRS WHERE usr_key IN (SELECT usr_key FROM usr WHERE usr_key NOT IN ('1', '2', '3', '4', '5'));
	DELETE FROM RECON_EVENTS WHERE usr_key IN (SELECT usr_key FROM usr WHERE usr_key NOT IN ('1', '2', '3', '4', '5'));
	DELETE FROM RECON_USER_MATCH WHERE usr_key IN (SELECT usr_key FROM usr WHERE usr_key NOT IN ('1', '2', '3', '4', '5'));
	DELETE FROM RECON_USER_OLDSTATE WHERE usr_key IN (SELECT usr_key FROM usr WHERE usr_key NOT IN ('1', '2', '3', '4', '5'));

--Default tables from 9g
	DELETE FROM UD_ADUSRC WHERE orc_key IN (SELECT orc_key FROM orc WHERE usr_key IN (SELECT usr_key FROM usr WHERE usr_key NOT IN ('1', '2', '3', '4', '5')));
	DELETE FROM UD_ADUSER WHERE orc_key IN (SELECT orc_key FROM orc WHERE usr_key IN (SELECT usr_key FROM usr WHERE usr_key NOT IN ('1', '2', '3', '4', '5')));

	DELETE FROM UPA WHERE usr_key IN (SELECT usr_key FROM usr WHERE usr_key NOT IN ('1', '2', '3', '4', '5'));

	DELETE FROM upa_fields WHERE upa_usr_key IN (SELECT upa_usr_key FROM upa_usr WHERE usr_key IN (SELECT usr_key FROM usr WHERE usr_key NOT IN ('1', '2', '3', '4', '5')));
	DELETE FROM UPA_GRP_MEMBERSHIP WHERE upa_usr_key IN (SELECT upa_usr_key FROM upa_usr WHERE usr_key IN (SELECT usr_key FROM usr WHERE usr_key NOT IN ('1', '2', '3', '4', '5')));
	DELETE FROM upa_ud_formfields WHERE upa_ud_forms_key IN (SELECT upa_ud_forms_key FROM upa_ud_forms WHERE upa_resource_key IN (SELECT upa_resource_key FROM upa_resource WHERE upa_usr_key IN (SELECT upa_usr_key FROM upa_usr WHERE usr_key IN (SELECT usr_key FROM usr WHERE usr_key NOT IN ('1', '2', '3', '4', '5')))));
	DELETE FROM upa_ud_forms WHERE upa_resource_key IN (SELECT upa_resource_key FROM upa_resource WHERE upa_usr_key IN (SELECT upa_usr_key FROM upa_usr WHERE usr_key IN (SELECT usr_key FROM usr WHERE usr_key NOT IN ('1', '2', '3', '4', '5'))));

	DELETE FROM UPA_RESOURCE WHERE upa_usr_key IN (SELECT upa_usr_key FROM upa_usr WHERE usr_key IN (SELECT usr_key FROM usr WHERE usr_key NOT IN ('1', '2', '3', '4', '5')));
	DELETE FROM UPA_USR WHERE usr_key IN (SELECT usr_key FROM usr WHERE usr_key NOT IN ('1', '2', '3', '4', '5'));

	DELETE FROM usr WHERE usr_key NOT IN ('1', '2', '3', '4', '5');

--Clearing all Audit messages
	TRUNCATE TABLE AUD_JMS;
 */
}

