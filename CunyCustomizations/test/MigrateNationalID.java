package test;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;

import java.sql.*;

import java.util.Properties;


public class MigrateNationalID {

    public static void main(String[] args) throws Exception {
        String xel_home = getValueFromConfig("XELLERATE_HOME");
        System.setProperty("securityMgrPolicy",
                           xel_home + "/config/xl.policy");

        System.setProperty("java.security.auth.login.config",
                           xel_home + "/config/authws.conf");
        printPasswordsFromDB();

    }
    
    private static void printPasswordsFromDB(){
        Connection oimDBConnection = getOIM10gDBConnection();
        String query ="select usr_udf_oid,  USR_UDF_NATIONAL_ID from usr order by usr.usr_key asc";
        Statement stmt = null;
        ResultSet rs = null;
        File outputFile = new File(getValueFromConfig("EXPORTED_CHALLENGE_QUESTION_ANSWER_FILE"));
        if (outputFile.exists())
            outputFile.delete();
        PrintWriter outputWriter = null;
        Connection oimNewDBConnection = getOIM11gDBConnection();

        try {
            outputFile.createNewFile();
            outputWriter = new PrintWriter(new FileWriter(outputFile), true);
            stmt = oimDBConnection.createStatement();
            rs = stmt.executeQuery(query);

            while (rs.next()) {
                String userLogin = rs.getString("usr_udf_oid");
		String encryptedPassword = rs.getString("USR_UDF_NATIONAL_ID");
                String userPassword = null;
		if(encryptedPassword!=null)
                    userPassword = com.thortech.xl.crypto.tcCryptoUtil.decrypt(encryptedPassword,"DBSecretKey" );
                if(userLogin!=null)
                updateOIMNationID(userLogin, userPassword, oimNewDBConnection);                
                                
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null)
                    rs.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (stmt != null)
                    stmt.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (oimDBConnection != null)
                    oimDBConnection.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (oimNewDBConnection != null)
                    oimNewDBConnection.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                outputWriter.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    private static Connection getOIM10gDBConnection() {
        Connection oimDBConnection = null;
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            oimDBConnection =
                    DriverManager.getConnection(getValueFromConfig("OIMDB_URL"),
                                                getValueFromConfig("OIMDB_USER"), getValueFromConfig("OIMDB_PASSWORD"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return oimDBConnection;
    }

    private static Connection getOIM11gDBConnection() {
        Connection oimDBConnection = null;
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            oimDBConnection =
                    DriverManager.getConnection(getValueFromConfig("OIMNEWDB_URL"),
                                                getValueFromConfig("OIMNEWDB_USER"), getValueFromConfig("OIMNEWDB_PASSWORD"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return oimDBConnection;
    }

    
    private static String getValueFromConfig(String prKey) {
        String val = null;
        Properties prop = new Properties();
        InputStream input = null;
        try {
            input = new FileInputStream("config.properties");
            prop.load(input);
            val = (String)prop.getProperty(prKey);
        } catch (Exception ex) {
    ex.printStackTrace();
    } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        System.out.println("Value " + val + " Key:" + prKey);
        return val;
    }

    private static void updateOIMNationID(String userLogin,
                                          String nationID, Connection oimDBConnection) {
        System.out.println(userLogin);
        String query ="update usr set USR_UDF_NATIONAL_ID = ? where usr_login = ?";
        PreparedStatement stmt = null;
        try {
            stmt = oimDBConnection.prepareStatement(query);
            stmt.setString(1, nationID);
            stmt.setString(2, userLogin.toUpperCase());
            stmt.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }
}

