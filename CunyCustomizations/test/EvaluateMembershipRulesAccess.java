package test;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.CallableStatement;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;


public class EvaluateMembershipRulesAccess {
    private static Connection oimDBConnection = null;

    public EvaluateMembershipRulesAccess() throws Exception {
        String orclIDMLoc = System.getenv("INSTALL_LOC");
        System.setProperty("XL.HomeDir", orclIDMLoc + "/server");
        System.setProperty("java.security.policy",
                           orclIDMLoc + "/designconsole/config/xl.policy");
        System.setProperty("java.security.auth.login.config",
                           orclIDMLoc + "/designconsole/config/authwl.conf");
        System.setProperty("XL.ExtendedErrorOptions", "TRUE");
        System.setProperty("APPSERVER_TYPE", "wls");

    }

    private static String getValueFromConfig(String prKey) {
        String val = null;
        Properties prop = new Properties();
        InputStream input = null;
        try {
            input = new FileInputStream("config.properties");
            prop.load(input);
            val = (String)prop.getProperty(prKey);
        } catch (Exception ex) {
            System.out.println("Error occured for reading from config.");
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        System.out.println("Value " + val + " Key:" + prKey);
        return val;
    }

    private Connection getOIMDBConnection() {
        try {
            if (oimDBConnection != null && !oimDBConnection.isClosed())
                return oimDBConnection;
            Class.forName("oracle.jdbc.driver.OracleDriver");
            oimDBConnection =
                    DriverManager.getConnection(getValueFromConfig("OIMDB_URL"),
                                                getValueFromConfig("OIM_DB_USER_NAME"),
                                                getValueFromConfig("OIM_DB_USER_PASSWD"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return oimDBConnection;
    }


    public static void main(String[] args) throws Exception {
        EvaluateMembershipRulesAccess apiTest =
            new EvaluateMembershipRulesAccess();
        apiTest.runUpdate();
        try {
            if (oimDBConnection != null)
                oimDBConnection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void executeProcedure(String procedName) {
        System.out.println("Calling Procedure: " + procedName);
        CallableStatement stmt = null;
        try {
            stmt =
getOIMDBConnection().prepareCall("{call " + procedName + "}");
            stmt.execute();

        } catch (Exception e1) {
            e1.printStackTrace();
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        System.out.println("Finished Procedure: " + procedName);
    }

    public void executeQuery(String query) {
        Statement stmt = null;
        try {
            stmt = getOIMDBConnection().createStatement();
            System.out.println("Executing query " + query);

            stmt.execute(query);
        } catch (Exception e1) {
            System.out.println("Failed query " + query);
            e1.printStackTrace();
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void executeListQuery(List<String> query) {
        for (String q : query) {
            executeQuery(q);
        }
    }

    public List<String> returnQueryResult(String query) {
        Statement stmt = null;
        ResultSet rs = null;
        List<String> retList = new ArrayList<String>();
        try {
            stmt = getOIMDBConnection().createStatement();
            rs = stmt.executeQuery(query);
            while (rs.next()) {
                retList.add(rs.getString(1));
            }
        } catch (Exception e1) {
            System.out.println("Failed query " + query);
            e1.printStackTrace();
        } finally {
            try {
                if (rs != null)
                    rs.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (stmt != null)
                    stmt.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        return retList;
    }

    public String getQueryValue(String query) {
        Statement stmt = null;
        ResultSet rs = null;
        try {
            stmt = getOIMDBConnection().createStatement();
            rs = stmt.executeQuery(query);
            if (rs.next()) {
                return (rs.getString(1));
            }
        } catch (Exception e1) {
            System.out.println("Failed query " + query);
            e1.printStackTrace();
        } finally {
            try {
                if (rs != null)
                    rs.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (stmt != null)
                    stmt.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        return null;
    }

    private void runCopyUSGMapping() {
        String deletionQ =
            "select 'insert into usg(usr_key, ugp_key, usg_update, usg_create,usg_rule_based) values(' ||usrnew.usr_key ||', '||ugpnew.ugp_key||', sysdate, sysdate,''' ||usg.usg_rule_based || ''')' " +
            "from usg@orclteng, ugp@orclteng, usr@orclteng, usr usrnew, ugp ugpnew where usg.ugp_key= ugp.ugp_key and usg.usr_key = usr.usr_key and usrnew.usr_emp_no= usr.usr_login " +
            "and ugp.ugp_name=ugpnew.ugp_name and usrnew.usr_key ||','||ugpnew.ugp_key not in (select usr_key ||','||ugp_key from usg)";
        executeListQuery(returnQueryResult(deletionQ));
        executeQuery("update usg set USG_PROV_MECHANISM='Rule-Based Role-Assignment', usg_prov_by='1', usg_createby='1', usg_updateby='1'  where usg_rule_based='1' and USG_PROV_MECHANISM is null");
    }

    private void processAccessPolicyHarvesting(String tengUDTableName,
                                               String newUDTableName) {
        String sql =
            "select pol.pol_name, usr.usr_login  from " + tengUDTableName +
            "@orclteng, oiu@orclteng, usr@orclteng, pol@orclteng where oiu.orc_key = " +
            tengUDTableName +
            ".orc_key and oiu.usr_key = usr.usr_key and pol.pol_key = oiu.pol_key";
        Statement stmt = null;
        ResultSet rs = null;
        try {
            stmt = getOIMDBConnection().createStatement();
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                String polName = rs.getString("pol_name");
                String empNo = rs.getString("usr_login");
                String policyKey =
                    getQueryValue("select pol_key from pol where pol_name = '" +
                                  polName + "'");
                String userKey =
                    getQueryValue("select usr_key from usr where usr_emp_no = '" +
                                  empNo + "'");
                String orcKey =
                    getQueryValue("SELECT oiu.orc_key from " + newUDTableName +
                                  ", oiu where oiu.orc_key = " +
                                  newUDTableName +
                                  ".orc_key and oiu.usr_key=" + userKey);
                
                if(orcKey!=null && userKey!=null && policyKey!=null && empNo!=null && polName!=null) {
                executeQuery("update " + newUDTableName + " set pol_key =" +
                             policyKey + " where orc_key=" + orcKey);
                executeQuery("update OIU set POL_KEY=" + policyKey +
                             ",oiu_prov_mechanism='AP HARVESTED', OIU_POLICY_BASED=1,OIU_POLICY_REVOKE=2 where orc_key=" +
                             orcKey);
                }
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        } finally {
            try {
                if (rs != null)
                    rs.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (stmt != null)
                    stmt.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void runAfterTargetRecon() {
        runCopyUSGMapping();
        processAccessPolicyHarvesting("ud_Eaduser", "ud_aduser");
        processAccessPolicyHarvesting("ud_oid_usr",
                                      "ud_oid_usr"); //String tengUDTableName, String newUDTableName
        processAccessPolicyHarvesting("ud_Uaduser", "ud_Uaduser");
        processAccessPolicyHarvesting("UD_ELM", "UD_ELM_BAS");
        processAccessPolicyHarvesting("UD_CRM", "UD_CRM_BAS");
        processAccessPolicyHarvesting("UD_EPM", "UD_EPM_BAS");
        processAccessPolicyHarvesting("UD_HCMREP", "UD_RHC_BAS");
        processAccessPolicyHarvesting("UD_FINREP", "UD_RFI_BAS");
        processAccessPolicyHarvesting("UD_FIN", "UD_FIN_BAS");
        processAccessPolicyHarvesting("UD_PRTL", "UD_ENP_BAS");
        processAccessPolicyHarvesting("UD_PSFT", "UD_HCM_BAS");
        processAccessPolicyHarvesting("UD_EMAILHTR", "UD_HUADUSER");
        processAccessPolicyHarvesting("UD_EMAILQNS", "UD_QUADUSER");

    }

    private void runUpdate() {
        executeProcedure("update_USR");

        executeProcedure("update_OIDUSR");
        executeProcedure("update_UADUSR");
        executeProcedure("update_EADUSR");
        executeProcedure("update_adminrolememb");
        runAfterTargetRecon(); //executeProcedure("update_usg");
    }


}


