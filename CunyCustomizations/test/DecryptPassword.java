package test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import java.util.Arrays;
import java.util.Properties;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import javax.xml.bind.DatatypeConverter;


public class DecryptPassword {
    //https://community.oracle.com/thread/2349879
    private static SecretKeySpec secretKey;
    private static byte[] key;

    private final static String SEPARATOR = "~";
    public static void main(String[] args) throws Exception {
        String xel_home = getValueFromConfig("XELLERATE_HOME");
        System.setProperty("securityMgrPolicy",
                           xel_home + "/config/xl.policy");

        System.setProperty("java.security.auth.login.config",
                           xel_home + "/config/authws.conf");
        printPasswordsFromDB();

    }
   public static boolean checkIfUserLoaded(String userLogin, Connection conn) {
        String sql =
            "select count(*) as counter from usr where UPPER(usr_login) = ?";
        PreparedStatement stmt = null;
        ResultSet rs = null;
        int counter = 0;
        try {
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, userLogin.toUpperCase());
            rs = stmt.executeQuery();
            if (rs.next())
                counter = rs.getInt("counter");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null)
                    rs.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (stmt != null)
                    stmt.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return (counter == 1);
    }
 
    private static void printPasswordsFromDB(){
        Connection oimDBConnection = getOIM10gDBConnection();
        Connection oimDBNewConnection = getOIM11gDBConnection();

        String query ="select usr.usr_udf_oid,  usr.usr_password, pcq.pcq_question, pcq.pcq_answer from usr, pcq where usr.usr_key = pcq.usr_key(+) order by usr.usr_key asc";
        Statement stmt = null;
        ResultSet rs = null;
        File outputFile = new File(getValueFromConfig("EXPORTED_CHALLENGE_QUESTION_ANSWER_FILE"));
        if (outputFile.exists())
            outputFile.delete();
        PrintWriter outputWriter = null;

        try {
            outputFile.createNewFile();
            outputWriter = new PrintWriter(new FileWriter(outputFile), true);
            stmt = oimDBConnection.createStatement();
            rs = stmt.executeQuery(query);
            String lastUserLogin = null;
            String lastUserPassword = null;
            
            while (rs.next()) {
                String userLogin = rs.getString("usr_udf_oid");
                if(userLogin == null) continue;
                if(!checkIfUserLoaded(userLogin, oimDBNewConnection)) continue;

		String encryptedPassword = rs.getString("usr_password");
                String userPassword = null;
		if(encryptedPassword!=null)
			userPassword = com.thortech.xl.crypto.tcCryptoUtil.decrypt(encryptedPassword,"DBSecretKey" );
                
		String encryptedQuestion = rs.getString("pcq_question");
                String question = null;
		if(encryptedQuestion!=null)
			question = com.thortech.xl.crypto.tcCryptoUtil.decrypt(encryptedQuestion,"DBSecretKey" );
                String encryptedAnswer = rs.getString("pcq_answer");
                String answer = null;
		
		if(encryptedAnswer!=null)
			answer = com.thortech.xl.crypto.tcCryptoUtil.decrypt(encryptedAnswer,"DBSecretKey" );
               
		//if(answer.indexOf(SEPARATOR)!=-1 && userPassword.indexOf(SEPARATOR)!=-1 && question.indexOf(SEPARATOR)!=-1)
 		//	throw new Exception("Issue");
		if(lastUserLogin !=null && userLogin.equalsIgnoreCase(lastUserLogin)){
                    if(question != null && answer != null)
			outputWriter.print(SEPARATOR + encryptText(question) +  SEPARATOR + encryptText(answer)); 
                } else {
                    outputWriter.println("");
		    if(question == null || answer == null)
			outputWriter.print(userLogin + SEPARATOR + encryptText(userPassword));	
                    else 
			outputWriter.print(userLogin + SEPARATOR + encryptText(userPassword) + SEPARATOR + encryptText(question) +  SEPARATOR + encryptText(answer));  
                    lastUserLogin = userLogin;
                    lastUserPassword = userPassword;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null)
                    rs.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (stmt != null)
                    stmt.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (oimDBConnection != null)
                    oimDBConnection.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                outputWriter.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    private static Connection getOIM10gDBConnection() {
        Connection oimDBConnection = null;
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            oimDBConnection =
                    DriverManager.getConnection(getValueFromConfig("OIMDB_URL"),
                                                getValueFromConfig("OIMDB_USER"), getValueFromConfig("OIMDB_PASSWORD"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return oimDBConnection;
    }
   private static Connection getOIM11gDBConnection() {
        Connection oimDBConnection = null;
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            oimDBConnection =
                    DriverManager.getConnection(getValueFromConfig("OIMNEWDB_URL"),
                                                getValueFromConfig("OIMNEWDB_USER"), getValueFromConfig("OIMNEWDB_PASSWORD"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return oimDBConnection;
    }
 
    private static String getValueFromConfig(String prKey) {
        String val = null;
        Properties prop = new Properties();
        InputStream input = null;
        try {
            input = new FileInputStream("config.properties");
            prop.load(input);
            val = (String)prop.getProperty(prKey);
        } catch (Exception ex) {
    ex.printStackTrace();
    } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        System.out.println("Value " + val + " Key:" + prKey);
        return val;
    }    
    public static void setKey()
        {    String myKey = "mySecretKey";

            MessageDigest sha = null;
            try {
                key = myKey.getBytes("UTF-8");
                sha = MessageDigest.getInstance("SHA-1");
                key = sha.digest(key);
                key = Arrays.copyOf(key, 16);
                secretKey = new SecretKeySpec(key, "AES");
            }
            catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
            catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
    
    public static String encryptText(String plainText) throws Exception{
        setKey();
        Cipher aesCipher = Cipher.getInstance("AES");
        aesCipher.init(Cipher.ENCRYPT_MODE, secretKey);
        byte[] byteCipherText = aesCipher.doFinal(plainText.getBytes());
        return bytesToHex(byteCipherText);
    }

    public static String decryptText(String hash) throws Exception {
        setKey();
        byte[] byteCipherText = hexToByte(hash);
        Cipher aesCipher = Cipher.getInstance("AES");
        aesCipher.init(Cipher.DECRYPT_MODE, secretKey);
        byte[] bytePlainText = aesCipher.doFinal(byteCipherText);
        return new String(bytePlainText);
    }
    
    private static String  bytesToHex(byte[] hash) {
        return DatatypeConverter.printHexBinary(hash);
    }
    private static byte[]  hexToByte(String hex) {
        return DatatypeConverter.parseHexBinary(hex);
    }
    
}

