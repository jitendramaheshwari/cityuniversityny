package test;

import java.io.FileWriter;
import java.io.IOException;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;


public class FetchOIDDetails {

    public static void main(String[] args) throws NamingException {
        DirContext ldapContext = null;
        try {
            System.out.println("Dbut du test Active Directory");

            Hashtable<String, String> ldapEnv =
                new Hashtable<String, String>(11);
            ldapEnv.put(Context.INITIAL_CONTEXT_FACTORY,
                        "com.sun.jndi.ldap.LdapCtxFactory");
            ldapEnv.put(Context.PROVIDER_URL,
                        "ldap://idstoreviolet.cuny.edu:389");

            ldapEnv.put(Context.SECURITY_AUTHENTICATION, "simple");
            ldapEnv.put(Context.SECURITY_PRINCIPAL, "cn=orcladmin");
            ldapEnv.put(Context.SECURITY_CREDENTIALS, "zoora7Ee");
            ldapContext = new InitialDirContext(ldapEnv);
            SearchControls searchCtls = new SearchControls();
            String returnedAtts[] =
            { "cunyeduemplid","guid" , "baruchCollege-Emp","baruchCollege-Stu","boroughOfManhattanCC-Emp","boroughOfManhattanCC-Stu","bronxCC-Emp","bronxCC-Stu","brooklynCollege-Emp","brooklynCollege-Stu","centralOffice-Emp","cityCollege-Emp","cityCollege-Stu","collegeofStatenIsland-Emp","collegeofStatenIsland-Stu","graduateCenter-Emp","graduateCenter-Stu","gutmannCC-Emp","guttmanCC-Stu","hostosCC-Emp","hostosCC-Stu","hunterCampusSchools-Emp","hunterCampusSchools-Stu","hunterCollege-Emp","hunterCollege-Stu","johnJayCollege-Emp","johnJayCollege-Stu","kingsboroughCC-Emp","kingsboroughCC-Stu","laGuardiaCC-Emp","laGuardiaCC-Stu","lawSchool-Emp","lawSchool-Stu","lehmanCollege-Emp","lehmanCollege-Stu","medgarEversCollege-Emp","medgarEversCollege-Stu","medicalSchool-Emp","medicalSchool-Stu","nycCollegeOfTechnology-Emp","nycCollegeOfTechnology-Stu","queensboroughCC-Emp","queensboroughCC-Stu","queensCollege-Emp","queensCollege-Stu","schoolofJournalism-Emp","schoolofJournalism-Stu","schoolofProfStudies-Emp","schoolofProfStudies-Stu","schoolofPublicHealth-Emp","schoolofPublicHealth-Stu","yorkCollege-Emp","yorkCollege-Stu"};
            searchCtls.setReturningAttributes(returnedAtts);

            //Specify the search scope
            searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);

            //specify the LDAP search filter
            //String searchFilter ="(&(objectClass=person)(cn=ROBERTO.WILLIAMS68))";
           // ldapContext.destroySubcontext("cn=yuly.upegulyuly89,cn=users,dc=cuny,dc=edu");

            String searchFilter ="(&(objectClass=person)(uid=Yuly.Upegul*))";
            String searchBase = "cn=Users,dc=cuny,dc=edu";
            String userDN =
                "cn=" + "Shakiera.Duren51" + "," + "cn=Users,dc=cuny,dc=edu";
            BasicAttributes basicattributes = new BasicAttributes(true);
            basicattributes.put(new BasicAttribute("baruchCollege-Emp",
                                                   "A"));


//         ldapContext.modifyAttributes(userDN, ldapContext.REPLACE_ATTRIBUTE, basicattributes);

       //  ldapContext.modifyAttributes(userDN,ldapContext.REMOVE_ATTRIBUTE,basicattributes);

            int totalResults = 0;

            // Search for objects using the filter
            NamingEnumeration<SearchResult> answer =
                ldapContext.search(searchBase, searchFilter, searchCtls);
            System.out.println("search  du test Active Directory");

            //Loop through the search results
            while (answer.hasMoreElements()) {
                SearchResult sr = (SearchResult)answer.next();

                totalResults++;

                System.out.println(">>>" + sr.getName());


                Attributes attrs = sr.getAttributes();
                System.out.println(">>>>>>" + attrs);
            }
            ldapContext.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ldapContext.close();
        }
    }
}
