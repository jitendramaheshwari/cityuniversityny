package test;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;


public class CopyMembershipAccessPolicy {

    public CopyMembershipAccessPolicy() throws Exception {
        String orclIDMLoc = System.getenv("INSTALL_LOC");
        System.setProperty("XL.HomeDir", orclIDMLoc + "/server");
        System.setProperty("java.security.policy",
                           orclIDMLoc + "/designconsole/config/xl.policy");
        System.setProperty("java.security.auth.login.config",
                           orclIDMLoc + "/designconsole/config/authwl.conf");
        System.setProperty("XL.ExtendedErrorOptions", "TRUE");
        System.setProperty("APPSERVER_TYPE", "wls");

    }

    private static String getValueFromConfig(String prKey) {
        String val = null;
        Properties prop = new Properties();
        InputStream input = null;
        try {
            input = new FileInputStream("config.properties");
            prop.load(input);
            val = (String)prop.getProperty(prKey);
        } catch (Exception ex) {
            System.out.println("Error occured for reading from config.");
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        System.out.println("Value " + val + " Key:" + prKey);
        return val;
    }

    private Connection getOIMDBConnection() {
        Connection oimDBConnection = null;
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            oimDBConnection =
                    DriverManager.getConnection(getValueFromConfig("OIMDB_URL"),
                                                getValueFromConfig("OIM_DB_USER_NAME"),
                                                getValueFromConfig("OIM_DB_USER_PASSWD"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return oimDBConnection;
    }


    public static void main(String[] args) throws Exception {
        CopyMembershipAccessPolicy apiTest = new CopyMembershipAccessPolicy();
        apiTest.runCopyUSGMapping();
    }

    public void executeQuery(String query, Connection con) {
        Statement stmt = null;
        try {
            stmt = con.createStatement();
            stmt.execute(query);
        } catch (Exception e1) {
            System.out.println("Failed query " + query);
            e1.printStackTrace();
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void executeListQuery(List<String> query, Connection con) {
        for (String q : query) {
            executeQuery(q, con);
        }
    }

    public List<String> returnQueryResult(String query, Connection con) {
        Statement stmt = null;
        ResultSet rs = null;
        List<String> retList = new ArrayList<String>();
        try {
            stmt = con.createStatement();
            rs = stmt.executeQuery(query);
            while (rs.next()) {
                retList.add(rs.getString(1));
            }
        } catch (Exception e1) {
            System.out.println("Failed query " + query);
            e1.printStackTrace();
        } finally {
            try {
                if (rs != null)
                    rs.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (stmt != null)
                    stmt.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        return retList;
    }

    public String getQueryValue(String query, Connection con) {
        Statement stmt = null;
        ResultSet rs = null;
        try {
            stmt = con.createStatement();
            rs = stmt.executeQuery(query);
            if (rs.next()) {
                return (rs.getString(1));
            }
        } catch (Exception e1) {
            System.out.println("Failed query " + query);
            e1.printStackTrace();
        } finally {
            try {
                if (rs != null)
                    rs.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (stmt != null)
                    stmt.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        return null;
    }


    
    private void processAccessPolicyHarvesting(Connection conn, String tengUDTableName, String newUDTableName){
        String sql = "select pol.pol_name, usr.usr_login  from "+tengUDTableName+"@orclteng, oiu@orclteng, usr@orclteng, pol@orclteng where oiu.orc_key = "+tengUDTableName+".orc_key and oiu.usr_key = usr.usr_key and pol.pol_key = oiu.pol_key";
        Statement stmt = null;
        ResultSet rs = null;
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                String polName = rs.getString("pol_name");
                String empNo = rs.getString("usr_login");
                String policyKey = getQueryValue("select pol_key from pol where pol_name = '" + polName + "'", conn);
                String userKey = getQueryValue("select usr_key from usr where usr_emp_no = '" + empNo + "'", conn);
                String orcKey = getQueryValue("SELECT oiu.orc_key from "+newUDTableName+", oiu where oiu.orc_key = "+newUDTableName+".orc_key and oiu.usr_key=" + userKey, conn);
                executeQuery("update "+newUDTableName+" set pol_key =" + policyKey + " where orc_key="+orcKey, conn);
                executeQuery("update OIU set POL_KEY=" + policyKey + ",oiu_prov_mechanism='AP HARVESTED', OIU_POLICY_BASED=1,OIU_POLICY_REVOKE=2 where orc_key="+orcKey, conn);

            }
        } catch (Exception e1) {
            e1.printStackTrace();
        } finally {
            try {
                if (rs != null)
                    rs.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (stmt != null)
                    stmt.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    

    private void runCopyUSGMapping() {
        Connection conn = getOIMDBConnection();
        String deletionQ =
            "select 'insert into usg(usr_key, ugp_key, usg_update, usg_create,usg_rule_based) values(' ||usrnew.usr_key ||', '||ugpnew.ugp_key||', sysdate, sysdate,''' ||usg.usg_rule_based || ''')' " +
            "from usg@orclteng, ugp@orclteng, usr@orclteng, usr usrnew, ugp ugpnew where usg.ugp_key= ugp.ugp_key and usg.usr_key = usr.usr_key and usrnew.usr_emp_no= usr.usr_login " +
            "and ugp.ugp_name=ugpnew.ugp_name and usrnew.usr_key ||','||ugpnew.ugp_key not in (select usr_key ||','||ugp_key from usg)";
        executeListQuery(returnQueryResult(deletionQ, conn), conn);
        try {
            if (conn != null)
                conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}

