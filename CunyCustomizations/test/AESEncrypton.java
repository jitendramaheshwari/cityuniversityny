package test;

import java.io.UnsupportedEncodingException;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import javax.xml.bind.DatatypeConverter;


public class AESEncrypton {
    private static SecretKeySpec secretKey;
    private static byte[] key;

    public static void main(String[] args) throws Exception {
        String plainText = "760D49940837C19F08A3BA3F0543796099FA13D1606103EEADDFCE7198820905";
        //String encryptedText = encryptText(plainText);
        String decryptedText = decryptText(plainText);
        
        System.out.println("Original Text:" + plainText);
        //System.out.println("Encrypted Text (Hex Form):"+encryptedText);
        System.out.println("Descrypted Text:"+decryptedText);
        
    }
    
        
    public static void setKey()
        {    String myKey = "mySecretKey";

            MessageDigest sha = null;
            try {
                key = myKey.getBytes("UTF-8");
                sha = MessageDigest.getInstance("SHA-1");
                key = sha.digest(key);
                key = Arrays.copyOf(key, 16);
                secretKey = new SecretKeySpec(key, "AES");
            }
            catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
            catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
    
    public static String encryptText(String plainText) throws Exception{
        setKey();
        Cipher aesCipher = Cipher.getInstance("AES");
        aesCipher.init(Cipher.ENCRYPT_MODE, secretKey);
        byte[] byteCipherText = aesCipher.doFinal(plainText.getBytes());
        return bytesToHex(byteCipherText);
    }

    public static String decryptText(String hash) throws Exception {
        setKey();
        byte[] byteCipherText = hexToByte(hash);
        Cipher aesCipher = Cipher.getInstance("AES");
        aesCipher.init(Cipher.DECRYPT_MODE, secretKey);
        byte[] bytePlainText = aesCipher.doFinal(byteCipherText);
        return new String(bytePlainText);
    }
    
    private static String  bytesToHex(byte[] hash) {
        return DatatypeConverter.printHexBinary(hash);
    }
    private static byte[]  hexToByte(String hex) {
        return DatatypeConverter.parseHexBinary(hex);
    }
}