
package com.aptecllc.jps;

//~--- non-JDK imports --------------------------------------------------------

import oracle.security.jps.JpsContext;
import oracle.security.jps.JpsContextFactory;
import oracle.security.jps.JpsException;
import oracle.security.jps.internal.api.runtime.ServerContextFactory;
import oracle.security.jps.service.credstore.Credential;
import oracle.security.jps.service.credstore.CredentialStore;
import oracle.security.jps.service.credstore.GenericCredential;
import oracle.security.jps.service.credstore.PasswordCredential;


//~--- JDK imports ------------------------------------------------------------



import java.security.AccessController;
import java.security.PrivilegedActionException;
import java.security.PrivilegedExceptionAction;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This library class provides a set of utilities methods to retrieve credentials and settings from
 * the WLS credential store framework.  Any application using this library must also be granted the correct 
 * JAZN permissions to the CSF maps required.
 * 
 * Uses the SLF4J abstract logger.
 *
 * @author fforester, eafisher
 */
public class CSFUtilities {
    private static final Logger logger = LoggerFactory.getLogger(CSFUtilities.class.getName());

    /**
     * Retrieves the text string based property from CSF given the map and key values.
     *
     *
     * @param mapName
     * @param csfKey
     *
     * @return
     */
    public static String getCSFStringProperty(final String mapName, final String csfKey) {
        GenericCredential credential = (GenericCredential) getCredentialsFromCSF(mapName, csfKey);
        Object            obj        = credential.getCredential();

        if (obj == null) {
            return null;
        }

        String val = null;

        if (obj instanceof String) {
            val = (String) obj;

            if (val.trim().length() == 0) {
                return null;
            }
        }

        return val;
    }

    /**
     * Retrieves the credential pair from CSF for a map and key.  This pair includes username and password.
     *
     *
     * @param mapName
     * @param csfKey
     *
     * @return
     */
    public static Object getCredentialsFromCSF(final String mapName, final String csfKey) {
        Object passwordCredential = null;

        try {
            if (null != csfKey) {
                final CredentialStore credStore = getCredStore();

                if (null != credStore) {
                    passwordCredential =
                        (Object) AccessController.doPrivileged(new PrivilegedExceptionAction<Credential>() {
                        public Credential run() throws Exception {
                            return (credStore.getCredential(mapName, csfKey));
                        }
                    });
                } else {
                    logger.error("Exception occured while obtaining the password credentials. Credentials are null");
                }
            }
        } catch (final PrivilegedActionException ex) {
            logger.error(ex.getMessage(), ex);
        } catch (final JpsException jpse) {
            logger.error(jpse.getMessage(), jpse);
        } catch (final java.security.AccessControlException ex) {
            logger.error(ex.getMessage(), ex);
        }

        return passwordCredential;
    }

    /**
     * Retrieves only the password part of a credential pair from CSF given map and key.
     *
     *
     * @param mapName
     * @param csfKey
     *
     * @return
     */
    public static char[] getPasswordforCredential(final String mapName, final String csfKey) {
        PasswordCredential credential = (PasswordCredential) getCredentialsFromCSF(mapName, csfKey);

        if (credential == null) {
            return ("").toCharArray();
        }

        return credential.getPassword();
    }

    /**
     * Retrieves only the username part of a credential pair from CSF given map and key.
     *
     *
     * @param mapName
     * @param csfKey
     *
     * @return
     */
    public static String getUserIdforCredential(final String mapName, final String csfKey) {
        PasswordCredential credential = (PasswordCredential) getCredentialsFromCSF(mapName, csfKey);

        if (credential == null) {
            return null;
        }

        return credential.getName();
    }

    private static CredentialStore getCredStore() throws JpsException {
        CredentialStore            csfStore;
        CredentialStore            appCsfStore         = null;
        CredentialStore            systemCsfStore      = null;
        final ServerContextFactory factory             = (ServerContextFactory) JpsContextFactory.getContextFactory();
        final JpsContext           jpsCtxSystemDefault = factory.getContext(ServerContextFactory.Scope.SYSTEM);
        final JpsContext           jpsCtxAppDefault    = factory.getContext(ServerContextFactory.Scope.APPLICATION);

        appCsfStore = (jpsCtxAppDefault != null)
                      ? jpsCtxAppDefault.getServiceInstance(CredentialStore.class)
                      : null;

        if (appCsfStore == null) {
            systemCsfStore = jpsCtxSystemDefault.getServiceInstance(CredentialStore.class);
            csfStore       = systemCsfStore;
        } else {

            // use Credential Store defined in app-level jps-config.xml
            csfStore = appCsfStore;
        }

        return csfStore;
    }
}
