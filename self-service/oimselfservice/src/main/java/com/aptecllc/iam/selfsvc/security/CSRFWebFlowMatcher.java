package com.aptecllc.iam.selfsvc.security;

import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.security.web.util.matcher.RequestMatcher;

public class CSRFWebFlowMatcher implements RequestMatcher {

    private final static Logger LOGGER = LoggerFactory.getLogger(CSRFWebFlowMatcher.class);

    private Pattern allowedMethods = Pattern.compile("^(HEAD|TRACE|OPTIONS)$");
    private Pattern okQueryString = Pattern.compile("^execution=[0-9A-Za-z]*(?:&_eventId=[0-9A-Za-z]+)?$");

    @Override
    public boolean matches(HttpServletRequest request) {
        LOGGER.debug("Request Method : " + request.getMethod());
        LOGGER.debug("Request Query String: " + request.getQueryString());

        if (allowedMethods.matcher(request.getMethod()).matches()) {
            return false;
        } else if (request.getMethod().equals("GET")) {
            //Only gets with parameters other than execution should be checked
            if (request.getQueryString() == null) {
                return false;
            } else if (request.getQueryString().indexOf("loginID=") != -1 && request.getQueryString().indexOf("tokenKey=") != -1) {
                return false;
            } else if (request.getQueryString().indexOf("loginID=") != -1) {
                return false;
            } else if (okQueryString.matcher(request.getQueryString()).matches()) {
                return false;
            }
        }
        return true;
    }
}
