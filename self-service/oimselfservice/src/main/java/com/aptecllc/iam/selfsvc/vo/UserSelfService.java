package com.aptecllc.iam.selfsvc.vo;

//~--- non-JDK imports --------------------------------------------------------


//~--- JDK imports ------------------------------------------------------------

import com.aptecllc.iam.selfsvc.utils.BaseSelfServiceUtilities;
import java.io.Serializable;

import java.util.HashMap;

/**
 * Class description
 *
 *
 * @version        1.0, 14/05/06
 * @author         Eric A. Fisher, APTEC LLC    
 */
public class UserSelfService implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String            userLogin = "@login.cuny.edu";
    private String            oldPassword;
    private String            newPassword;
    private String            confirmPassword;
    private HashMap           questions;
    private Integer            incorrectAsnwerCount = new Integer(0);
    private String            passwordToken;
    private String employeeID;
    private Integer passwordChangeWaitCounter = new Integer(0);
    private String            currentPassword;

    public Integer getPasswordChangeWaitCounter() {
        return passwordChangeWaitCounter;
    }

    public void setPasswordChangeWaitCounter(Integer passwordChangeWaitCounter) {
        this.passwordChangeWaitCounter = passwordChangeWaitCounter;
    }
    
    /**
     * Constructs ...
     *
     */
    public UserSelfService() {}

    /**
     * Constructs ...
     *
     *
     * @param pUserLogin
     */
    public UserSelfService(String pUserLogin) {
        this.userLogin = pUserLogin;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getUserLogin() {
        //this.userLogin = BaseSelfServiceUtilities.truncateLoginIDSuffix(this.userLogin);
        return userLogin;
    }

    /**
     * Method description
     *
     *
     * @param userLogin
     */
    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getOldPassword() {
        return oldPassword;
    }

    /**
     * Method description
     *
     *
     * @param oldPassword
     */
    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getNewPassword() {
        return newPassword;
    }

    /**
     * Method description
     *
     *
     * @param newPassword
     */
    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getConfirmPassword() {
        return confirmPassword;
    }

    /**
     * Method description
     *
     *
     * @param confirmPassword
     */
    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public HashMap getQuestions() {
        return questions;
    }

    /**
     * Method description
     *
     *
     * @param questions
     */
    public void setQuestions(HashMap questions) {
        this.questions = questions;
    }

    public Integer getIncorrectAsnwerCount() {
        return incorrectAsnwerCount;
    }

    public void setIncorrectAsnwerCount(Integer incorrectAsnwerCount) {
        this.incorrectAsnwerCount = incorrectAsnwerCount;
    }

    public String getPasswordToken() {
        return passwordToken;
    }

    public void setPasswordToken(String passwordToken) {
        this.passwordToken = passwordToken;
    }

    public String getEmployeeID() {
        return employeeID;
    }

    public void setEmployeeID(String employeeID) {
        this.employeeID = employeeID;
    }

    public String getCurrentPassword() {
        return currentPassword;
    }

    public void setCurrentPassword(String currentPassword) {
        this.currentPassword = currentPassword;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    @Override
    public String toString() {
        return "UserSelfService [userLogin=" + userLogin + ", oldPassword=****" + ", newPassword=****" + ", questions="
               + questions + "]";
    }
}
