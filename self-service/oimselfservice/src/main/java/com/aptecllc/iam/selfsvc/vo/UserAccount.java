package com.aptecllc.iam.selfsvc.vo;

//~--- non-JDK imports --------------------------------------------------------

import org.codehaus.jackson.annotate.JsonIgnore;

//~--- JDK imports ------------------------------------------------------------

import java.io.Serializable;

import java.util.HashMap;
import java.util.Set;

public class UserAccount implements Serializable {

    /**
     *
     */
    private static final long       serialVersionUID = 1L;
    private HashMap<String, Object> mAttributes;
    private String                  mUniqueID;

    public UserAccount() {
        mAttributes = new HashMap<String, Object>();
    }

    public String getOrganizationName() {
        return (String) mAttributes.get("organizationName");
    }

    public void setOrganizationName(String orgName) {
        mAttributes.put("organizationName", orgName);
    }

    public String getRole() {
        return (String) mAttributes.get("role");
    }

    public void setRole(String role) {
        mAttributes.put("role", role);
    }

    public String getTitle() {
        return (String) mAttributes.get("title");
    }

    public void setTitle(String title) {
        mAttributes.put("title", title);
    }

    public String getEmailAddress() {
        return (String) mAttributes.get("emailAddress");
    }

    public void setEmailAddress(String emailAddress) {
        mAttributes.put("emailAddress", emailAddress);
    }

    public String getPhoneNumber() {
        return (String) mAttributes.get("phoneNumber");
    }

    public void setPhoneNumber(String phoneNumber) {
        mAttributes.put("phoneNumber", phoneNumber);
    }



    public String getDepartmentNumber() {
        return (String) mAttributes.get("departmentNumber");
    }

    public void setDepartmentNumber(String departmentNumber) {
        mAttributes.put("departmentNumber", departmentNumber);
    }

    public String getOfficeLocation() {
        return (String) mAttributes.get("officeLocation");
    }

    public void setOfficeLocation(String officeLocation) {
        mAttributes.put("officeLocation", officeLocation);
    }

    public String getLastName() {
        return (String) this.mAttributes.get("lastName");
    }

    public void setLastName(String lastName) {
        this.mAttributes.put("lastName", lastName);
    }

    public String getMiddleInitial() {
        return (String) this.mAttributes.get("middleInitial");
    }

    public void setMiddleInitial(String middleInitial) {
        this.mAttributes.put("middleInitial", middleInitial);
    }

    public String getPrefFirstName() {
        return (String) this.mAttributes.get("prefFirstName");
    }

    public void setPrefFirstName(String prefFirstName) {
        this.mAttributes.put("prefFirstName", prefFirstName);
    }

    public void setAttribute(String attr, Object val) {
        if (attr == null) {
            return;
        }

        if (val == null) {
            this.mAttributes.remove(attr);
        }

        this.mAttributes.put(attr, val);
    }

    public Object getAttribute(String attr) {
        if (attr == null) {
            return null;
        }

        return this.mAttributes.get(attr);
    }

    @JsonIgnore
    public Set getAttributeNames() {
        return this.mAttributes.keySet();
    }

    public HashMap<String, Object> getAttributes() {
        return this.mAttributes;
    }

    public void setAttributes(HashMap attribs) {
        mAttributes = attribs;
    }

    public String toString() {
        StringBuffer buff = new StringBuffer();

        if (this.mUniqueID != null) {
            buff.append(this.mUniqueID);
            buff.append("\n");
        }

        if (this.mAttributes != null) {
            buff.append(this.mAttributes.toString());
        }

        return buff.toString();
    }

    public String getUserName() {
        return mUniqueID;
    }

    public void setUserName(String userName) {
        mUniqueID = userName;
    }

    public String getAccountId() {
        return (String) this.mAttributes.get("accountId");
    }

    public void setAccountId(String pAccountID) {
        this.mAttributes.put("accountId", pAccountID);
    }

    public String getFirstName() {
        return (String) this.mAttributes.get("firstName");
    }

    public void setFirstName(String pFirstName) {
        this.mAttributes.put("firstName", pFirstName);
    }

    public void setStatus(String pStatus) {
        this.mAttributes.put("status", pStatus);
    }

    public String getStatus() {
        return (String) this.mAttributes.get("status");
    }

    public void setLocked(String pLocked) {
        this.mAttributes.put("locked", pLocked);
    }

    public String getLocked() {
        return (String) this.mAttributes.get("locked");
    }

    public void setManualLock(String pLocked) {
        this.mAttributes.put("manualLock", pLocked);
    }

    public String getManualLock() {
        return (String) this.mAttributes.get("manualLock");
    }

    public void setAcctClaimed(String pLocked) {
        this.mAttributes.put("acctClaimed", pLocked);
    }

    public String getAcctClaimed() {
        return (String) this.mAttributes.get("acctClaimed");
    }

}
