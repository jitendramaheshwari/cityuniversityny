package com.aptecllc.iam.selfsvc.vo;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.commons.validator.routines.EmailValidator;

import org.springframework.binding.message.MessageBuilder;
import org.springframework.binding.message.MessageContext;
import org.springframework.binding.validation.ValidationContext;

//~--- JDK imports ------------------------------------------------------------

import java.io.Serializable;

public class EMAILUserUpdate implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -3227556438871514476L;
    private String            emailAddress;
    private String            newEmailAddress;
    private String            confirmNewEmailAddress;
    private String            userName;
    private String failed;

    public EMAILUserUpdate() {
        emailAddress = null;
        userName  = null;
        newEmailAddress = null;
        confirmNewEmailAddress = null;
        failed = null;
    }

    public EMAILUserUpdate(String pUserName) {
        emailAddress = null;
        userName  = pUserName;
        newEmailAddress = null;
        confirmNewEmailAddress = null;
        failed = null;
    }

    public String getNewEmailAddress() {
        return newEmailAddress;
    }

    public void setNewEmailAddress(String newEmailAddress) {
        this.newEmailAddress = newEmailAddress;
    }

    /**
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    public String getFailed() {
        return failed;
    }

    public void setFailed(String failed) {
        this.failed = failed;
    }

    /**
     * @param userName the userName to set
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * @return the smsNumber
     */
    public String getEmailAddress() {
        return emailAddress;
    }

    /*
     *  (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "UserUpdate [emailAddress=" + emailAddress + ", userName=" + userName + "]";
    }

    /**
     * @param smsNumber the smsNumber to set
     */
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getConfirmNewEmailAddress() {
        return confirmNewEmailAddress;
    }

    public void setConfirmNewEmailAddress(String confirmNewEmailAddress) {
        this.confirmNewEmailAddress = confirmNewEmailAddress;
    }

    public void validateGetUpdates(ValidationContext context) {
        MessageContext messages = context.getMessageContext();
        String oldEmailAddress  = this.getEmailAddress();
        if(oldEmailAddress ==null) oldEmailAddress = "";

        String newEmailAddress = this.getNewEmailAddress();
        String confirmNewEmailAddress = this.getConfirmNewEmailAddress();
        if(newEmailAddress ==null) newEmailAddress = "";
        if(confirmNewEmailAddress ==null) confirmNewEmailAddress = "";
        if(!confirmNewEmailAddress.equalsIgnoreCase(newEmailAddress)) {
                messages.addMessage(
                        new MessageBuilder().error().source("newEmailAddress").code("error_email_confirm_no_match").defaultText("Email Address & Confirm Email Address do not match.").build());
        } else if (!this.getNewEmailAddress().isEmpty()) {
            boolean valid = EmailValidator.getInstance().isValid(this.getNewEmailAddress());
            if (!valid) {
                messages.addMessage(
                        new MessageBuilder().error().source("newEmailAddress").code("error_email_invalid").defaultText("Email Address is not valid.").build());
            }
        } else if(oldEmailAddress.isEmpty() && newEmailAddress.isEmpty()) {
                messages.addMessage(
                        new MessageBuilder().error().source("newEmailAddress").code("error_email_must_provide").defaultText("You must provide an email address to claim your account.").build());
        } 
    }

}
