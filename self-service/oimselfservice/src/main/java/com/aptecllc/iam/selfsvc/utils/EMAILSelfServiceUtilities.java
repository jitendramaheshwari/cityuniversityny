package com.aptecllc.iam.selfsvc.utils;

//~--- non-JDK imports --------------------------------------------------------


import com.aptecllc.iam.selfsvc.OIMRestClient;
import com.aptecllc.iam.selfsvc.vo.UserAccount;


import com.aptecllc.iam.selfsvc.vo.EMAILUserUpdate;
import com.aptecllc.iam.selfsvc.vo.MobileNumberUserUpdate;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//~--- JDK imports ------------------------------------------------------------



import java.util.List;
import org.springframework.web.client.HttpClientErrorException;

public class EMAILSelfServiceUtilities extends BaseSelfServiceUtilities {
    private final static Logger LOGGER = LoggerFactory.getLogger(EMAILSelfServiceUtilities.class);

    
    public String determineRegistrationStatus(UserAccount oimUser) {
        String result = "Unknown Status";

        LOGGER.trace("determineRegStatus: {}", oimUser);

        String status = (String) oimUser.getAttribute("acctClaimed");

        if ("Disabled".equalsIgnoreCase(status)) {
            result = "Security Disabled";
        } else if ("L".equals(status) || "A".equals(status)) {
            result = "Registration Locked";
        } else if ("1".equals(status) || "Y".equals(status) || "true".equalsIgnoreCase(status)) {
            result = "Account Registered";
        } else {
            result = "Register Account";
        }

        LOGGER.trace("determineRegStatus: {} ", result);

        return result;
    }

    public String determineResetProcess(UserAccount oimUser) {
        String result = "Unknown Status";

        LOGGER.trace("determineResetProcess: {}", oimUser);

        if ("Disabled".equalsIgnoreCase(oimUser.getStatus())) {
            result = "Security Disabled";
        } else if ("1".equals(oimUser.getAcctClaimed()) || "Y".equals(oimUser.getAcctClaimed())
                   || "true".equalsIgnoreCase(oimUser.getAcctClaimed())) {
                result = "Account Registered";
        } else {
            result = "Register Account";
        }

        LOGGER.trace("determineResetProcess: {} ", result);

        return result;
    }

    public EMAILUserUpdate getUserUpdate(UserAccount user) {
        EMAILUserUpdate upd = new EMAILUserUpdate(user.getUserName());

        upd.setEmailAddress((String) user.getAttribute("emailAddress"));

        LOGGER.trace("userupdate: {}", upd);

        return upd;
    }

    public MobileNumberUserUpdate getMobileNumberUserUpdate(UserAccount user) {
        MobileNumberUserUpdate upd = new MobileNumberUserUpdate(user.getUserName());
        
        String mobileNumberDB = user.getPhoneNumber();
        if(mobileNumberDB == null || mobileNumberDB.isEmpty()) {
        } else if(mobileNumberDB.indexOf("-")!=-1) {
            upd.setMobileNumber(mobileNumberDB.substring(mobileNumberDB.indexOf("-")+1));
            upd.setCountryCode(mobileNumberDB.substring(0, mobileNumberDB.indexOf("-")));
        } else {
            upd.setMobileNumber(mobileNumberDB);
        }
        upd.setMobileNumberInDB(mobileNumberDB);
        LOGGER.trace("MobileNumberUserUpdate: {}", upd);

        return upd;
    }

    
    
    public EMAILUserUpdate getUserUpdate(UserAccount user, String emailAddress) {
        EMAILUserUpdate upd = new EMAILUserUpdate(user.getUserName());

        user.setAttribute("emailAddress", emailAddress);
        LOGGER.trace("userupdate: {}", upd);

        return upd;
    }
    public UserAccount getAccountForEMAILUpdate(EMAILUserUpdate userUp) {
        UserAccount account = new UserAccount();

        account.setUserName(userUp.getUserName());
        String oldEmailAddress = userUp.getEmailAddress();
        String newEmailAddress = userUp.getNewEmailAddress();
        if(oldEmailAddress == null) {
            oldEmailAddress = "";
        }
        if(newEmailAddress!=null && !newEmailAddress.equalsIgnoreCase(oldEmailAddress)) {
            account.setAttribute("emailAddress", userUp.getNewEmailAddress());
        }
        LOGGER.trace("!!!!userAccount: {}", account);

        return account;
    }

    public UserAccount getAccountforStatusUpdate(UserAccount user, String regStatus) {
        UserAccount updAccount = new UserAccount();

        user.setAttribute("registrationStatus", regStatus);
        updAccount.setUserName(user.getUserName());
        updAccount.setAttribute("registrationStatus",regStatus);

        if ("Y".equals(regStatus)) {
            updAccount.setAcctClaimed("1");
        }

        return updAccount;
    }


}
