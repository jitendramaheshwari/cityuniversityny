package com.aptecllc.iam.selfsvc.vo;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Class description
 *
 *
 * @version        1.0, 13/10/21
 * @author         Eric A. Fisher, APTEC LLC    
 */
public class SecurityQuestions implements Serializable {
    private String question1;
    private String answer1;
    private String question2;
    private String answer2;
    private String question3;
    private String answer3;
    private String question4;
    private String answer4;
    private String question5;
    private String answer5;
    private String question6;
    private String answer6;

    private String question7;
    private String answer7;
    private String question8;
    private String answer8;
    /**
     * Constructs ...
     *
     */
    public SecurityQuestions() {

        // TODO Auto-generated constructor stub
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getQuestion1() {
        return question1;
    }

    /**
     * Method description
     *
     *
     * @param question1
     */
    public void setQuestion1(String question1) {
        this.question1 = question1;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getAnswer1() {
        return answer1;
    }

    /**
     * Method description
     *
     *
     * @param answer1
     */
    public void setAnswer1(String answer1) {
        this.answer1 = answer1;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getQuestion2() {
        return question2;
    }

    /**
     * Method description
     *
     *
     * @param question2
     */
    public void setQuestion2(String question2) {
        this.question2 = question2;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getAnswer2() {
        return answer2;
    }

    /**
     * Method description
     *
     *
     * @param answer2
     */
    public void setAnswer2(String answer2) {
        this.answer2 = answer2;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getQuestion3() {
        return question3;
    }

    public String getQuestion4() {
        return question4;
    }

    public void setQuestion4(String question4) {
        this.question4 = question4;
    }

    public String getAnswer4() {
        return answer4;
    }

    public void setAnswer4(String answer4) {
        this.answer4 = answer4;
    }

    public String getQuestion5() {
        return question5;
    }

    public void setQuestion5(String question5) {
        this.question5 = question5;
    }

    public String getAnswer5() {
        return answer5;
    }

    public void setAnswer5(String answer5) {
        this.answer5 = answer5;
    }

    public String getQuestion6() {
        return question6;
    }

    public void setQuestion6(String question6) {
        this.question6 = question6;
    }

    public String getAnswer6() {
        return answer6;
    }

    public void setAnswer6(String answer6) {
        this.answer6 = answer6;
    }

    public String getQuestion7() {
        return question7;
    }

    public void setQuestion7(String question7) {
        this.question7 = question7;
    }

    public String getAnswer7() {
        return answer7;
    }

    public void setAnswer7(String answer7) {
        this.answer7 = answer7;
    }

    public String getQuestion8() {
        return question8;
    }

    public void setQuestion8(String question8) {
        this.question8 = question8;
    }

    public String getAnswer8() {
        return answer8;
    }

    public void setAnswer8(String answer8) {
        this.answer8 = answer8;
    }

    /**
     * Method description
     *
     *
     * @param question3
     */
    public void setQuestion3(String question3) {
        this.question3 = question3;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getAnswer3() {
        return answer3;
    }

    /**
     * Method description
     *
     *
     * @param answer3
     */
    public void setAnswer3(String answer3) {
        this.answer3 = answer3;
    }
    
    public HashMap getQuestionMap() {
    	HashMap qaMap = new HashMap();
    	
    	if (!"".equals(question1) && question1!=null && !"".equals(answer1) && answer1!=null) {
    		qaMap.put(question1, answer1);
    	}
    	if (!"".equals(question2)&& question2!=null && !"".equals(answer2) && answer2!=null) {
    		qaMap.put(question2, answer2);
    	}
    	if (!"".equals(question3)&& question3!=null && !"".equals(answer3) && answer3!=null) {
    		qaMap.put(question3, answer3);
    	}

        if (!"".equals(question4)&& question4!=null && !"".equals(answer4) && answer4!=null) {
    		qaMap.put(question4, answer4);
    	}

    	if (!"".equals(question5)&& question5!=null && !"".equals(answer5) && answer5!=null) {
    		qaMap.put(question5, answer5);
    	}

    	if (!"".equals(question6)&& question6!=null && !"".equals(answer6) && answer6!=null) {
    		qaMap.put(question6, answer6);
    	}

    	if (!"".equals(question7)&& question7!=null && !"".equals(answer7) && answer7!=null) {
    		qaMap.put(question7, answer7);
    	}

    	if (!"".equals(question8)&& question8!=null && !"".equals(answer8) && answer8!=null) {
    		qaMap.put(question8, answer8);
    	}
        
    	return qaMap;
    }

	@Override
	public String toString() {
		return "SecurityQuestions [question1=" + question1 + ", answer1="
				+ answer1 + ", question2=" + question2 + ", answer2=" + answer2
				+ ", question3=" + question3 + ", answer3=" + answer3 
                        + ", question4=" + question4 + ", answer4=" + answer4
                        + ", question5=" + question5 + ", answer5=" + answer5
                        + ", question6=" + question6 + ", answer6=" + answer6
                        + ", question7=" + question7 + ", answer7=" + answer7
                        + ", question8=" + question8 + ", answer8=" + answer8
                        + "]";
	}
}
