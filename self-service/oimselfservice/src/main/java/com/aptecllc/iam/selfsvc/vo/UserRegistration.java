package com.aptecllc.iam.selfsvc.vo;

//~--- non-JDK imports --------------------------------------------------------

import org.springframework.binding.message.MessageBuilder;
import org.springframework.binding.message.MessageContext;
import org.springframework.binding.validation.ValidationContext;
import org.springframework.format.annotation.DateTimeFormat;

//~--- JDK imports ------------------------------------------------------------



import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class description
 *
 *
 * @version        1.0, 13/11/08
 * @author         Eric A. Fisher, APTEC LLC
 */
public class UserRegistration implements Serializable {
    private final static Logger      LOGGER = LoggerFactory.getLogger(UserRegistration.class);

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String            LookupKey;
    private String            SSN;
    @DateTimeFormat(pattern = "MM-dd-yyyy")
    private Date              DOB;
    private String            dateOfBirth;
    private String            firstName;
    private String            middleName;
    private String            lastFourSSN;
    private String            lastName;
    private String            recaptcha;
    private Pattern alphaPattern = Pattern.compile("[a-zA-Z\\-']+");

    /**
     * Constructs ...
     *
     */
    public UserRegistration() {
        this.LookupKey = "";
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getLookupKey() {
        return LookupKey;
    }

    /**
     * Method description
     *
     *
     * @param lookupKey
     */
    public void setLookupKey(String lookupKey) {
        LookupKey = lookupKey;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getSSN() {
        return SSN;
    }

    /**
     * Method description
     *
     *
     * @param sSN
     */
    public void setSSN(String sSN) {
        SSN = sSN;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public Date getDOB() {
        return DOB;
    }

    /**
     * Method description
     *
     *
     * @param dOB
     */
    public void setDOB(Date dOB) {
        DOB = dOB;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     * Method description
     *
     *
     * @param dateOfBirth
     */
    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Method description
     *
     *
     * @param firstName
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getMiddleName() {
        return middleName;
    }

    /**
     * Method description
     *
     *
     * @param middleName
     */
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Method description
     *
     *
     * @param lastName
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Method description
     *
     *
     * @param context
     */
    public void validateEnterRegInfo(ValidationContext context) {
        MessageContext messages = context.getMessageContext();
        if (this.getFirstName().isEmpty()) {
            messages.addMessage(
                new MessageBuilder().error().source("FirstName").code("error_firstname_required").defaultText("Enter your first name.").build());
        } else {
        	Matcher regexMatcher = alphaPattern.matcher(this.getFirstName());

        	if (!regexMatcher.matches()) {
        		messages.addMessage(
                        new MessageBuilder().error().source("FirstName").code("error_firstname_invalid").defaultText("First name contains invalid characters.").build());
        	}
        }



        if (this.getLastName().isEmpty()) {
            messages.addMessage(
                new MessageBuilder().error().source("LastName").code("error_lastname_required").defaultText("Enter your last name.").build());
        }	else {
        	Matcher regexMatcher = alphaPattern.matcher(this.getLastName());

        	if (!regexMatcher.matches()) {
        		messages.addMessage(
                        new MessageBuilder().error().source("LastName").code("error_lastname_invalid").defaultText("Last name contains invalid characters.").build());
        	}
        }


        if (this.getLastFourSSN().isEmpty()) {
            messages.addMessage(
                new MessageBuilder().error().source("LastFourSSN").code("error_lastfourssn_required").defaultText("Enter your LastFourSSN.").build());
        }
        checkDateOfBirth(messages);
    }

    private void checkDateOfBirth(MessageContext messages) {
        boolean          validateDate = true;
        SimpleDateFormat dobFormat    = new SimpleDateFormat("MM-dd-yyyy");
        LOGGER.trace(" checkDateOfBirth Value of  this.getDateOfBirth() " + this.getDateOfBirth());

        if (this.getDateOfBirth().isEmpty()) {
            messages.addMessage(
                new MessageBuilder().error().source("DateOfBirth").code("error_dateofbirth_required").defaultText("Enter your birth date.").build());
            validateDate = false;
        }
        if (validateDate) {
            try {
                Date dobDate = dobFormat.parse(this.getDateOfBirth());

                        LOGGER.trace(" checkDateOfBirth Value of dobDate " + dobDate);

                this.setDOB(dobDate);
            } catch (ParseException e) {
                messages.addMessage(
                    new MessageBuilder().error().source("dateOfBirth").code("error_dateofbirth_invalid").defaultText("Date of Birth is Invalid.").build());
            }
        }
    }

    public String getLastFourSSN() {
        return lastFourSSN;
    }

    public void setLastFourSSN(String lastFourSSN) {
        this.lastFourSSN = lastFourSSN;
    }

    public String getRecaptcha() {
        return recaptcha;
    }

    public void setRecaptcha(String recaptcha) {
        this.recaptcha = recaptcha;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String buildSearchQuery() {
        StringBuilder    query = new StringBuilder();
        SimpleDateFormat sdf   = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");

        query.append("firstName==").append(this.getFirstName()).append(";lastName==").append(this.getLastName()).append(";lastFourSSN==").append(com.aptecllc.iam.selfsvc.utils.HashUtil.getSHA1Hash(this.getLastFourSSN()));
        query.append(";dateOfBirth==").append(sdf.format(this.getDOB()));

        return query.toString();
    }
}
