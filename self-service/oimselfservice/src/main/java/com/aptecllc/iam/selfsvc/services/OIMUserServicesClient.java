package com.aptecllc.iam.selfsvc.services;

//~--- non-JDK imports --------------------------------------------------------
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;

import com.aptecllc.iam.selfsvc.OIMRestClient;
import com.aptecllc.iam.selfsvc.exception.UserCreateException;
import com.aptecllc.iam.selfsvc.exception.UserNotFoundException;
import com.aptecllc.iam.selfsvc.exception.UserUpdateException;
import com.aptecllc.iam.selfsvc.utils.*;
import com.aptecllc.iam.selfsvc.vo.EMAILUserUpdate;
import com.aptecllc.iam.selfsvc.vo.MobileNumberUserUpdate;
import com.aptecllc.iam.selfsvc.vo.Notification;
import com.aptecllc.iam.selfsvc.vo.OTP;
import com.aptecllc.iam.selfsvc.vo.SecurityQuestions;
import com.aptecllc.iam.selfsvc.vo.SponsoredAccount;
import com.aptecllc.iam.selfsvc.vo.UserAccount;
import com.aptecllc.iam.selfsvc.vo.UserRegistration;
import com.aptecllc.iam.selfsvc.vo.UserRequest;
import com.aptecllc.iam.selfsvc.vo.UserSelfService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.binding.message.MessageBuilder;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.webflow.execution.RequestContext;

//~--- JDK imports ------------------------------------------------------------
import java.net.URI;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * Class description
 *
 *
 * @version 1.1, 13/10/18
 * @author Eric A. Fisher, APTEC LLC
 */
public class OIMUserServicesClient {

    private final static Logger LOGGER = LoggerFactory.getLogger(OIMUserServicesClient.class);
    private OIMRestClient oimAdminClient;
    private String serverUrl;
    private String apiKey;
    private String sharedSecret;
    private String adminUserLogin;
    private BaseSelfServiceUtilities selfServiceUtilities;
    private String googleCaptchaSecretKey;
    private String googleRecaptchaEnabled;
    private String googleCaptchaClientKey;
    public final static String LOGIN_ID_SUFFIX = "@login.cuny.edu";
    /**
     * Constructs ...
     *
     */
    public OIMUserServicesClient() {
    }

    /**
     * Constructs ...
     *
     *
     * @param pServerUrl
     * @param pApiKey
     * @param pSharedSecret
     * @param pAdminLogin
     * @param selfUtilities
     */
    public OIMUserServicesClient(String pServerUrl, String pApiKey, String pSharedSecret, String pAdminLogin, String googleCaptchaSecretKey, String googleCaptchaClientKey, String googleRecaptchaEnabled,
            BaseSelfServiceUtilities selfUtilities) {
        serverUrl = pServerUrl;
        apiKey = pApiKey;
        sharedSecret = pSharedSecret;
        adminUserLogin = pAdminLogin;
        selfServiceUtilities = selfUtilities;
        this.googleCaptchaSecretKey = googleCaptchaSecretKey;
        this.googleCaptchaClientKey = googleCaptchaClientKey;
        this.googleRecaptchaEnabled = googleRecaptchaEnabled;

        LOGGER.trace("***URL" + serverUrl + " KEY " + pApiKey + " Secret " + pSharedSecret + " login " + pAdminLogin);
        oimAdminClient = new OIMRestClient(serverUrl, apiKey, sharedSecret, adminUserLogin);
    }

    /**
     * Method description
     *
     *
     * @param userReg
     *
     * @return
     *
     * @throws Exception
     */
    public UserAccount createNewOIMUser(UserRegistration userReg) throws Exception {
        UserAccount oimUser = null;
        UserAccount newUser = selfServiceUtilities.getNewAccountFromRegistration(userReg);

        try {
            LOGGER.trace("createNewOIMUser: Attempting Post");

            URI objUri = oimAdminClient.getRestTemplate().postForLocation(serverUrl
                    + OIMRestClient.ServiceURLs.CREATE_USER.getUrl(), newUser);

            LOGGER.trace("createNewOIMUser: {} ", objUri);
            oimUser = oimAdminClient.getRestTemplate().getForObject(objUri, UserAccount.class);
            LOGGER.trace("{}", oimUser);
        } catch (HttpClientErrorException e) {
            LOGGER.trace("HCEE PUT ERROR " + e.getResponseBodyAsString());

            throw new UserCreateException(e.getStatusText());
        } catch (Exception e) {
            LOGGER.trace("PUT ERROR " + e.getMessage());

            throw new Exception(e.getMessage());
        }

        return oimUser;
    }

    /**
     * Method description
     *
     *
     *
     * @param identifier
     *
     * @return
     *
     * @throws Exception
     * @throws UserNotFoundException
     */
    public UserAccount findOIMUserByID(String identifier) throws UserNotFoundException, Exception {
        String METHOD_NAME = " findOIMUserByID ";
        String remoteAddress = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest().getRemoteAddr();
        UserAccount oimUser = null;
        identifier = BaseSelfServiceUtilities.truncateLoginIDSuffix(identifier);

        try {
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "Searching for : " + identifier);
            oimUser = oimAdminClient.getRestTemplate().getForObject(serverUrl
                    + OIMRestClient.ServiceURLs.GET_USER_BY_ID.getUrl(), UserAccount.class, identifier);
        } catch (HttpClientErrorException e) {
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "HCEE ERROR " + e.getResponseBodyAsString() + e.getStatusText());

            MessageBuilder builder = new MessageBuilder();

            // context.getMessageContext().addMessage(builder.error().code("Error." + e.getResponseBodyAsString().replaceAll(" ", "_")).build());
            throw new UserNotFoundException(e.getStatusText());
        } catch (Exception e) {
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "ERROR " + e.getMessage());

            throw new Exception(e.getMessage());
        }

        LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "{}", oimUser);

        return oimUser;
    }

    /**
     * Method description
     *
     *
     * @param oimUser
     *
     * @return
     */
    public UserSelfService getNewUserSelfService(UserAccount oimUser) {
        String METHOD_NAME = " getNewUserSelfService ";
        String remoteAddress = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest().getRemoteAddr();

        LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "Setting USS" + oimUser.getUserName());

        return new UserSelfService(oimUser.getUserName());
    }

    /**
     * Method description
     *
     *
     * @param userLogin
     *
     * @return
     *
     * @throws Exception
     * @throws UserNotFoundException
     */
    public UserAccount getOIMUser(String userLogin) throws UserNotFoundException, Exception {
        String METHOD_NAME = " getOIMUser ";
        String remoteAddress = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest().getRemoteAddr();

        UserAccount oimUser = null;

        try {
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "Searching for : " + userLogin);
            userLogin = BaseSelfServiceUtilities.truncateLoginIDSuffix(userLogin);
            
            oimUser = oimAdminClient.getRestTemplate().getForObject(serverUrl
                    + OIMRestClient.ServiceURLs.GET_USER_BY_LOGIN.getUrl(), UserAccount.class, userLogin);
        } catch (HttpClientErrorException e) {
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "HCEE ERROR " + e.getResponseBodyAsString() + e.getStatusText());

            MessageBuilder builder = new MessageBuilder();

            // context.getMessageContext().addMessage(builder.error().code("Error." + e.getResponseBodyAsString().replaceAll(" ", "_")).build());
            throw new UserNotFoundException(e.getStatusText());
        } catch (Exception e) {
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "ERROR " + e.getMessage());

            throw new Exception(e.getMessage());
        }

        LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "{}", oimUser);

        return oimUser;
    }

    public UserAccount getOIMUser(UserSelfService userSelfService, RequestContext context) throws UserNotFoundException{
        String METHOD_NAME = " getOIMUser ";
        String remoteAddress = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest().getRemoteAddr();
        String userLogin = userSelfService.getUserLogin();
        String employeeId = userSelfService.getEmployeeID();
       
        UserAccount oimUser = null;
        userLogin = BaseSelfServiceUtilities.truncateLoginIDSuffix(userLogin);
        LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "Searching for : " + userLogin + " OR using employeeId " + employeeId);
        if (employeeId != null && !employeeId.isEmpty()) {
            try {
                oimUser = findOIMUserByID(employeeId);
            } catch (Exception e) {
            }
        } 
        
        if (oimUser==null && userLogin != null && !userLogin.isEmpty()) {
            try {
                oimUser = getOIMUser(userLogin);
            } catch (Exception e) {
            }
        }
        LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "{}", oimUser);
        if(oimUser == null) {
            context.getMessageContext().addMessage(
                        new MessageBuilder().error().source("employeeID").code("error_employeeID_not_given").defaultText("Please provide employee id or user-login correctly.").build());
            throw new UserNotFoundException("User is not found");
        }
        return oimUser;    
    }
    /**
     * Method description
     *
     *
     * @param userLogin
     * @param employeeId
     * @return
     *
     * @throws Exception
     * @throws UserNotFoundException
     */
    public UserAccount getOIMUser(String userLogin, String employeeId) throws UserNotFoundException, Exception {
        String METHOD_NAME = " getOIMUser ";
        String remoteAddress = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest().getRemoteAddr();

        UserAccount oimUser = null;
        userLogin = BaseSelfServiceUtilities.truncateLoginIDSuffix(userLogin);
        LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "Searching for : " + userLogin + " OR using employeeId " + employeeId);
        if (userLogin != null && !userLogin.isEmpty()) {
            oimUser = getOIMUser(userLogin);
        } else if (employeeId != null && !employeeId.isEmpty()) {
            oimUser = findOIMUserByID(employeeId);
        } 
        LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "{}", oimUser);

        return oimUser;
    }

    public String getDisplayEmailAddress(String email) {
        int atRateIndex = email.indexOf("@");
        email = email.substring(0, atRateIndex - 3) + "***" + email.substring(atRateIndex);
        return email;

    }

    /**
     * Method description
     *
     *
     * @return
     *
     * @throws Exception
     */
    public List getSystemChallenges() throws Exception {
        String METHOD_NAME = " getSystemChallenges ";
        String remoteAddress = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest().getRemoteAddr();

        List challenges = null;

        try {
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "getSystemChallenges");
            challenges = oimAdminClient.getRestTemplate().getForObject(serverUrl
                    + OIMRestClient.ServiceURLs.GET_SYSTEM_CHALLENGES.getUrl(), List.class);
        } catch (HttpClientErrorException e) {
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "HCEE  ERROR " + e.getResponseBodyAsString());

            throw new Exception(e.getMessage());
        } catch (Exception e) {
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "PUT ERROR " + e.getMessage());

            throw new Exception(e.getMessage());
        }

        LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "{}", challenges);

        return challenges;
    }

    /**
     * Method description
     *
     *
     * @return
     *
     * @throws Exception
     */
    public List getSystemChallengesForUser(String pUserID) throws Exception {
        String METHOD_NAME = " getSystemChallengesForUser ";
        String remoteAddress = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest().getRemoteAddr();

        List challenges = null;
        pUserID = BaseSelfServiceUtilities.truncateLoginIDSuffix(pUserID);
        try {
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "getSystemChallengesForUser pUserID " + pUserID);
            challenges = oimAdminClient.getRestTemplate().getForObject(serverUrl
                    + OIMRestClient.ServiceURLs.GET_USER_CHALLENGEQSLIST.getUrl(), List.class, pUserID);
        } catch (Exception e) {
            LOGGER.error("HCEE  ERROR ", e);

            throw new Exception(e.getMessage());
        }
        LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "{}", challenges);
        return challenges;
    }

    /**
     * Method description
     *
     *
     * @param pUserID
     *
     * @return
     * @throws UserNotFoundException
     */
    public UserSelfService getUserSelfService(String pUserID) throws UserNotFoundException {
        String METHOD_NAME = " getUserSelfService ";
        String remoteAddress = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest().getRemoteAddr();

        LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "getUserSelfService" + pUserID);

        UserSelfService uss = new UserSelfService();
        pUserID = BaseSelfServiceUtilities.truncateLoginIDSuffix(pUserID);
        uss.setUserLogin(pUserID);
        LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + serverUrl + OIMRestClient.ServiceURLs.GET_USER_CHALLENGES.getUrl());

        ArrayList<String> challenges = null;

        try {
            challenges = oimAdminClient.getRestTemplate().getForObject(serverUrl
                    + OIMRestClient.ServiceURLs.GET_USER_CHALLENGES.getUrl(), ArrayList.class, pUserID);
        } catch (HttpClientErrorException e) {
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "HCEE  ERROR " + e.getResponseBodyAsString() + e.getStatusText());

            MessageBuilder builder = new MessageBuilder();

            // context.getMessageContext().addMessage(builder.error().code("Error."
            // + e.getResponseBodyAsString().replaceAll(" ", "_")).defaultText(e.getResponseBodyAsString()).build());
            throw new UserNotFoundException("User Not Found");
        } catch (Exception e) {
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + e.getMessage());
        }

        //LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "{}", challenges);
        HashMap questionMap = new HashMap();

        if (challenges != null) {
            for (String question : challenges) {
                questionMap.put(question, "");
            }
        }

        uss.setQuestions(questionMap);
        //LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + uss.toString());

        return uss;
    }

    /**
     * Method description
     *
     *
     * @param sponsoredAcct
     * @param requestingUser
     *
     * @return
     *
     * @throws Exception
     */
    public String requestNewOIMUser(SponsoredAccount sponsoredAcct, String requestingUser) throws Exception {
        String METHOD_NAME = " requestNewOIMUser ";
        String remoteAddress = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest().getRemoteAddr();

        String reqId = null;
        UserAccount newUser = selfServiceUtilities.getNewAccountFromSponsoredAccount(sponsoredAcct);
        OIMRestClient oimClient = getUserOIMRestClient(requestingUser);

        try {
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "requestNewOIMUser: Attempting Post");

            UserRequest req = new UserRequest();

            req.setRequestObject(newUser);
            req.setRequestingUser(requestingUser);

            URI objUri = oimClient.getRestTemplate().postForLocation(this.serverUrl
                    + OIMRestClient.ServiceURLs.REQUEST_USER.getUrl(), req, new Object[0]);

            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "requestNewOIMUser: " + objUri.toString());

            String reqUrl = objUri.toString();

            reqId = "/identity/faces/home?tf=request_details&requestId="
                    + reqUrl.substring(reqUrl.lastIndexOf("/") + 1);
        } catch (HttpClientErrorException e) {
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "HCEE PUT ERROR " + e.getResponseBodyAsString());

            throw new UserCreateException(e.getStatusText());
        } catch (Exception e) {
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "PUT ERROR " + e.getMessage());

            throw new Exception(e.getMessage());
        }

        return reqId;
    }

    /**
     * Method description
     *
     *
     * @param pUSS
     * @param context
     *
     * @return
     */
    public String resetUserPassword(UserSelfService pUSS, RequestContext context) {
        String METHOD_NAME = " resetUserPassword ";
        String remoteAddress = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest().getRemoteAddr();

        try {
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "resetUserPassword: Attempting Reset");
            oimAdminClient.getRestTemplate().put(serverUrl + OIMRestClient.ServiceURLs.CHANGE_USER_PASSWORD.getUrl(),
                    pUSS, pUSS.getUserLogin());
        } catch (HttpClientErrorException e) {
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "PUT ERROR ", e);
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "PUT ERROR " + e.getResponseBodyAsString());

            MessageBuilder builder = new MessageBuilder();

            context.getMessageContext().addMessage(builder.error().code("Error."
                    + e.getResponseBodyAsString().replaceAll(" ",
                            "_")).defaultText(e.getResponseBodyAsString()).build());

            return e.getResponseBodyAsString();
        } catch (Exception e) {
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "PUT ERROR ", e);

            return "Error";
        }

        LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "resetUserPassword: Success");

        return "Success";
    }

    public String resetUserPassword(UserSelfService pUSS) {
        String METHOD_NAME = " resetUserPassword ";
        String remoteAddress = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest().getRemoteAddr();

        int count = (pUSS.getIncorrectAsnwerCount()).intValue();
        try {
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "resetUserPassword: Attempting Reset count " + count);
            oimAdminClient.getRestTemplate().put(serverUrl + OIMRestClient.ServiceURLs.CHANGE_USER_PASSWORD.getUrl(),
                    pUSS, pUSS.getUserLogin());

        } catch (HttpClientErrorException e) {
            if (count >= 5) {
                LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "resetUserPassword: User will be sent email to reset password");
                return "Take Alternate EmailAddress";
            }
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "PUT ERROR ", e);
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "PUT ERROR " + e.getResponseBodyAsString());
            MessageBuilder builder = new MessageBuilder();
            return e.getResponseBodyAsString();
        } catch (Exception e) {
            if (count >= 5) {
                LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "resetUserPassword: User will be sent email to reset password");
                return "Take Alternate EmailAddress";
            }
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "PUT ERROR ", e);
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "PUT ERROR " + e.getMessage());
            return "Error";
        }
        LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "resetUserPassword: Success");
        return "Success";
    }

    /**
     * Method description
     *
     *
     *
     * @param searchQuery
     * @param context
     *
     * @return
     *
     * @throws Exception
     * @throws UserNotFoundException
     */
//  public String normalizeAddress(UserRegistration userReg, RequestContext context) {
//      Address outAddress = null;
//
//      logger.trace("Normalize Address" + userReg.getAddress().getAddressLine1());
//
//      try {
//          outAddress = AddressServiceDelegate.validateAddress(userReg.getAddress());
//      } catch (Exception e) {
//          logger.trace(e.getMessage());
//      }
//
//      logger.trace("Format Status" + outAddress.getFormatStatusCode());
//
//      if ("U".equals(outAddress.getFormatStatusCode())) {
//
//          // Unable to Validate
//          return "UnableToNormalize";
//      }
//
//      userReg.getAddress().setFormatAddress(outAddress.getFormatAddress());
//      userReg.getAddress().setZipCode5(outAddress.getZipCode5());
//      userReg.getAddress().setZipCode4(outAddress.getZipCode4());
//
//      return "Normalized";
//  }
    public UserAccount searchForOIMUser(String searchQuery, RequestContext context)
            throws UserNotFoundException, Exception {
        String METHOD_NAME = " searchForOIMUser ";
        String remoteAddress = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest().getRemoteAddr();

        UserAccount oimUser = null;

        try {
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "Searching for : " + searchQuery);

            UserAccount[] oimUserArray = oimAdminClient.getRestTemplate().getForObject(serverUrl
                    + OIMRestClient.ServiceURLs.SEARCH_USERS.getUrl(), UserAccount[].class,
                    searchQuery);
            List<UserAccount> oimUsers = Arrays.asList(oimUserArray);

            if (oimUsers.isEmpty()) {
                MessageBuilder builder = new MessageBuilder();

                context.getMessageContext().addMessage(
                        builder.error().code("Error.UserNotFound").defaultText("User Not Found").build());

                throw new UserNotFoundException("User Not Found");
            } else if (oimUsers.size() > 1) {
                throw new Exception("Multiple Users Found");
            } else {
                oimUser = (UserAccount) oimUsers.get(0);
            }
        } catch (HttpClientErrorException e) {
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "HCEE ERROR " + e.getResponseBodyAsString() + e.getStatusText());

            MessageBuilder builder = new MessageBuilder();

            // context.getMessageContext().addMessage(builder.error().code("Error." + e.getResponseBodyAsString().replaceAll(" ", "_")).build());
            throw new UserNotFoundException(e.getStatusText());
        } catch (UserNotFoundException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "REST ERROR " + e.getMessage());

            throw new Exception(e.getMessage());
        }

        LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "{}", oimUser);

        return oimUser;
    }

    /**
     * Method description
     *
     *
     * @param account
     * @param otp
     *
     * @return
     *
     * @throws Exception
     */
    public String sendEMAIL(UserAccount account, OTP otp) throws Exception {
        String METHOD_NAME = " sendEMAIL ";
        String remoteAddress = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest().getRemoteAddr();

        UserAccount updateAccount = new UserAccount();
        updateAccount.setUserName(account.getUserName());
        updateAccount.setAttribute("PasswordTken", otp.getAuthCode());
        updateAccount.setAttribute("PassTokenGenTme", otp.getAuthCodeGeneratedTime());

        try {
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "sendSMS: Attempting Put account " + updateAccount + " OTP " + otp);
            oimAdminClient.getRestTemplate().put(serverUrl + OIMRestClient.ServiceURLs.UPDATE_USER_BY_LOGIN.getUrl(),
                    updateAccount, updateAccount.getUserName());
        } catch (HttpClientErrorException e) {
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "PUT ERROR ", e);

            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "HCEE PUT ERROR " + e.getResponseBodyAsString());
            return "ERROR";
        } catch (Exception e) {
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "PUT ERROR ", e);

            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "PUT ERROR " + e.getMessage());

            return "ERROR";
        }

        return "Message Sent";
    }

    public String sendEmailForChangePassword(UserAccount oimUser, String purpose) {
        String METHOD_NAME = " sendEmailForChangePassword ";
        String remoteAddress = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest().getRemoteAddr();

        String result = "ERROR";
        try {
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "Attempting Put account " + oimUser.getUserName() + " purpose" + purpose);
            UserAccount updateAccount = new UserAccount();
            updateAccount.setUserName(oimUser.getUserName());

            oimAdminClient.getRestTemplate().put(serverUrl + OIMRestClient.ServiceURLs.SEND_EMAIL_FOR_PASSWORD_CHANGE.getUrl(), purpose, oimUser.getUserName());
            result = "success";
        } catch (HttpClientErrorException e) {
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "PUT ERROR ", e);

            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "HCEE PUT ERROR " + e.getResponseBodyAsString());
            return "ERROR";
        } catch (Exception e) {
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "PUT ERROR ", e);

            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "PUT ERROR " + e.getMessage());

            return "ERROR";
        }

        return result;
    }

    public String sendEmailChangeLinkForActivation(UserAccount oimUser, String purpose) {
        return sendEmailForChangePassword(oimUser, purpose + oimUser.getEmailAddress());
    }

    public String sendEMAIL(OTP otp) throws Exception {
        String METHOD_NAME = " sendEMAIL ";
        String remoteAddress = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest().getRemoteAddr();

        Notification notification = new Notification();

        notification.setRecipient(otp.getEmailAddress());
        notification.setMessage(otp.getMessage());

        try {
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "sendSMS: Attempting Put {}", otp.getEmailAddress());
            oimAdminClient.getRestTemplate().postForLocation(serverUrl + OIMRestClient.ServiceURLs.SEND_SMS.getUrl(),
                    notification);
        } catch (HttpClientErrorException e) {
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "HCEE PUT ERROR " + e.getResponseBodyAsString());

            throw new Exception(e.getStatusText());
        } catch (Exception e) {
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "PUT ERROR " + e.getMessage());

            throw new Exception(e.getMessage());
        }

        return "Message Sent";
    }

    /**
     * Method description
     *
     *
     * @param pUSS
     * @param questions
     * @param context
     *
     * @return
     */
    public String setUserChallenges(UserSelfService pUSS, SecurityQuestions questions, RequestContext context) {
        String METHOD_NAME = " setUserChallenges ";
        String remoteAddress = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest().getRemoteAddr();

        pUSS.setQuestions(questions.getQuestionMap());

        LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "Setting questions: " + pUSS.getQuestions());
        LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "UserLogin: " + pUSS.getUserLogin());
        LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "pUSS: " + pUSS);

        OIMRestClient oimClient = getUserOIMRestClient(pUSS.getUserLogin());

        try {
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "setUserChallenges: Attempting Set using pUSS" + pUSS);
            oimClient.getRestTemplate().put(serverUrl + OIMRestClient.ServiceURLs.SET_USER_CHALLENGES.getUrl(), pUSS,
                    pUSS.getUserLogin());
        } catch (HttpClientErrorException e) {
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "PUT ERROR ", e);
            MessageBuilder builder = new MessageBuilder();

            context.getMessageContext().addMessage(builder.error().code("Error."
                    + e.getResponseBodyAsString().replaceAll(" ",
                            "_")).defaultText(e.getResponseBodyAsString()).build());

            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "Returning the value as : QuestionSetFailed");

            return "QuestionSetFailed";
        } catch (Exception e) {
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "PUT ERROR ", e);
            return "Error";
        }

        return "QuestionsSet";
    }

    /**
     * Method description
     *
     *
     * @param pUSS
     * @param context
     *
     * @return
     */
    public String checkPasswordChangeStatus(UserSelfService pUSS, String tokenKey, RequestContext context) {
        String METHOD_NAME = " setUserPassword ";
        String remoteAddress = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest().getRemoteAddr();
        pUSS.setPasswordChangeWaitCounter(pUSS.getPasswordChangeWaitCounter() + 1);
        try {
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "tokenKey " + tokenKey);
            return ChangePasswordThread.getChangeStatus(tokenKey);
        } catch (HttpClientErrorException e) {
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "HCEE PUT ERROR " + e.getResponseBodyAsString() + e.getStatusText());

            MessageBuilder builder = new MessageBuilder();

            context.getMessageContext().addMessage(builder.error().code("Error."
                    + e.getResponseBodyAsString().replaceAll(" ",
                            "_")).defaultText(e.getResponseBodyAsString()).build());

            return "PasswordSetFailed";
        }
    }

    /**
     * Method description
     *
     *
     * @param pUSS
     * @param context
     *
     * @return
     */
    public String setUserPassword(UserSelfService pUSS, RequestContext context) {
        String METHOD_NAME = " setUserPassword ";
        String remoteAddress = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest().getRemoteAddr();
        pUSS.setPasswordChangeWaitCounter(new Integer(0));
        OIMRestClient oimClient = getUserOIMRestClient(pUSS.getUserLogin());
        String randomToken = System.currentTimeMillis() + pUSS.getUserLogin();
        pUSS.setQuestions(null);

        try {
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "setUserPassword: Attempting Set pUSS" + pUSS);
            Thread t = new Thread(new ChangePasswordThread(oimClient, pUSS, randomToken, serverUrl));
            LOGGER.trace(METHOD_NAME + "created the thread t " + t);
            t.start();
            LOGGER.trace(METHOD_NAME + "Invoked the thread  " + t.getName());

        } catch (Exception e) {
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "PUT ERROR " + e.getMessage());
        }
        return randomToken;
    }

    /**
     * Method description
     *
     *
     *
     * @param account
     *
     * @return
     *
     * @throws Exception
     * @throws UserUpdateException
     */
    public String updateOIMUser(UserAccount account) throws UserUpdateException, Exception {
        String METHOD_NAME = " updateOIMUser ";
        String remoteAddress = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest().getRemoteAddr();

        try {
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "updateOIMUser: Attempting Put {}", account);
            oimAdminClient.getRestTemplate().put(serverUrl + OIMRestClient.ServiceURLs.UPDATE_USER_BY_LOGIN.getUrl(),
                    account, account.getUserName());
        } catch (HttpClientErrorException e) {
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "HCEE PUT ERROR " + e.getResponseBodyAsString());

            throw new UserUpdateException(e.getStatusText());
        } catch (Exception e) {
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "PUT ERROR " + e.getMessage());

            throw new Exception(e.getMessage());
        }

        return "Update Successful";
    }

    /**
     * Method description
     *
     *
     *
     * @param account
     *
     * @return
     *
     * @throws Exception
     * @throws UserUpdateException
     */
    public String updateOIMUserEmailAddress(EMAILUserUpdate userUp, RequestContext context) throws UserUpdateException, Exception {
        String METHOD_NAME = " updateOIMUserEmailAddress ";
        String remoteAddress = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest().getRemoteAddr();

        LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "updateOIMUserEmailAddress: Attempting Put {}", userUp);
        UserAccount account = new UserAccount();

        account.setUserName(userUp.getUserName());
        String oldEmailAddress = userUp.getEmailAddress();
        String newEmailAddress = userUp.getNewEmailAddress();
        if (oldEmailAddress == null) {
            oldEmailAddress = "";
        }

        if (newEmailAddress != null && !newEmailAddress.equalsIgnoreCase(oldEmailAddress) && !newEmailAddress.isEmpty()) {
            account.setAttribute("emailAddress", userUp.getNewEmailAddress());
            try {
                return updateOIMUser(account);
            } catch (Exception e) {
                LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "PUT ERROR " + e.getMessage());
                MessageBuilder builder = new MessageBuilder();
                context.getMessageContext().addMessage(builder.error().source("newEmailAddress").defaultText("Email Address is not valid." + e.getMessage()).build());
                return "Update Failed";
            }
        }
        return "Update Successful";
    }

    /**
     * Method description
     *
     *
     * @param pUSS
     * @param context
     *
     * @return
     */
    public String validatePassword(UserSelfService pUSS, RequestContext context) {
        String METHOD_NAME = " validatePassword ";
        String remoteAddress = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest().getRemoteAddr();

        try {
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "validatePassword: Attempting Validate");
            oimAdminClient.getRestTemplate().put(serverUrl + OIMRestClient.ServiceURLs.VALIDATE_USER_PASSWORD.getUrl(),
                    pUSS, pUSS.getUserLogin());
        } catch (HttpClientErrorException e) {
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "HCEE PUT ERROR " + e.getResponseBodyAsString());

            MessageBuilder builder = new MessageBuilder();
            String[] violatedRules = e.getResponseBodyAsString().split("\\|");

            for (String rule : violatedRules) {
                context.getMessageContext().addMessage(builder.error().source("newPassword").defaultText(rule).build());
            }

            return "PasswordNotValid";
        } catch (Exception e) {
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "PUT ERROR " + e.getMessage());

            return "Error";
        }

        return "PasswordValid";
    }

    /**
     * Method description
     *
     *
     * @param pUSS
     * @param context
     *
     * @return
     */
    public String changePasswordTokenBased(UserSelfService pUSS, String loginID, RequestContext context) {
        String METHOD_NAME = " changePasswordTokenBased ";
        String remoteAddress = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest().getRemoteAddr();

        try {
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "changePasswordTokenBased: Attempting Validate loginID" + loginID);
            pUSS.setUserLogin(loginID);
            oimAdminClient.getRestTemplate().put(serverUrl + OIMRestClient.ServiceURLs.VALIDATE_USER_PASSWORD_TOKEN.getUrl(),
                    pUSS, pUSS.getUserLogin());
        } catch (HttpClientErrorException e) {
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "HCEE PUT ERROR " + e.getResponseBodyAsString());
            if (e.getResponseBodyAsString().equals("TOKENExpired") || e.getResponseBodyAsString().equals("TOKENIssue") || e.getResponseBodyAsString().equals("UserNOTFOUND")) {
                return "TokenExpired";
            }
            MessageBuilder builder = new MessageBuilder();
            String[] violatedRules = e.getResponseBodyAsString().split("\\|");
            for (String rule : violatedRules) {
                context.getMessageContext().addMessage(builder.error().source("resetPassword").defaultText(rule).build());
            }
            return "PasswordNotValid";
        } catch (Exception e) {
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "PUT ERROR " + e.getMessage());

            return "Error";
        }
        return "PasswordValid";
    }

    public String getClientKey() {
        return googleCaptchaClientKey;
    }

    private JsonObject validateCaptcha(String secret, String response, String remoteip) {
        String METHOD_NAME = " validateCaptcha ";
        String remoteAddress = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest().getRemoteAddr();

        JsonObject jsonObject = null;
        java.net.URLConnection connection = null;
        java.io.InputStream is = null;
        String charset = java.nio.charset.StandardCharsets.UTF_8.name();
        LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "secret " + secret);

        String url = "https://www.google.com/recaptcha/api/siteverify";
        try {
            String query = String.format("secret=%s&response=%s&remoteip=%s",
                    java.net.URLEncoder.encode(secret, charset),
                    java.net.URLEncoder.encode(response, charset),
                    java.net.URLEncoder.encode(remoteip, charset));

            connection = new java.net.URL(url + "?" + query).openConnection();
            is = connection.getInputStream();
            JsonReader rdr = Json.createReader(is);
            jsonObject = rdr.readObject();

        } catch (Exception ex) {
            LOGGER.error(METHOD_NAME + "RemoteAddress: " + remoteAddress + "PUT ERROR ", ex);
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (Exception e) {
                    LOGGER.error(METHOD_NAME + "RemoteAddress: " + remoteAddress + "PUT ERROR ", e);
                }

            }
        }
        return jsonObject;
    }

    public String validateRecaptcha(RequestContext context) {
        String METHOD_NAME = " validateRecaptcha ";
        String remoteAddress = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest().getRemoteAddr();

        String response = "error";
        try {
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "validateRecaptcha: Attempting Validate");
            String gRecaptchaResponse = context.getRequestParameters().get("recaptcha");
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "validateRecaptcha: Attempting Validate gRecaptchaResponse: " + gRecaptchaResponse);
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "validateRecaptcha: Attempting Validate remoteAddress: " + remoteAddress);
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "validateRecaptcha: Attempting Validate googleCaptchaSecretKey: " + googleCaptchaSecretKey);
            if(googleRecaptchaEnabled.equalsIgnoreCase("false")){
                LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + " Performance testing and the googleRecaptchaEnabled is false from application.properties");
                return "success";
            }
            
            if (gRecaptchaResponse == null || "".equals(gRecaptchaResponse)) {
                return response;
            }
            JsonObject jsonObject = validateCaptcha(googleCaptchaSecretKey, gRecaptchaResponse, remoteAddress);

            boolean t = jsonObject.getBoolean("success");
            if (t) {
                LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "/ " + jsonObject);

                return "success";
            } else {
                return response;
            }
        } catch (Exception e) {
            LOGGER.error("PUT ERROR ", e);

            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "PUT ERROR " + e.getMessage());
            return response;
        }
    }

    private OIMRestClient getUserOIMRestClient(String userLogin) {
        return new OIMRestClient(serverUrl, apiKey, sharedSecret, userLogin);
    }

    public List getRandomAddresses(int pNumber) throws Exception {
        String METHOD_NAME = " getRandomAddresses ";
        String remoteAddress = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest().getRemoteAddr();

        List addresses = null;

        try {
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "get Random Addresses");
            addresses = oimAdminClient.getRestTemplate().getForObject(serverUrl
                    + OIMRestClient.ServiceURLs.GET_RANDOM_ADDRESSES.getUrl(), List.class, pNumber);
        } catch (HttpClientErrorException e) {
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "HCEE PUT ERROR " + e.getResponseBodyAsString());

            throw new Exception(e);
        } catch (Exception e) {
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "PUT ERROR " + e.getMessage());

            throw new Exception(e);
        }

        LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "Addresses: {}", addresses);

        return addresses;
    }

    /**
     * Method description
     *
     *
     * @param pUSS
     * @param context
     *
     * @return
     */
    public String validateTokens(UserSelfService pUSS, String loginID, String tokenKey, RequestContext context, String purpose) {
        String METHOD_NAME = " validateTokens ";
        String remoteAddress = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest().getRemoteAddr();

        try {
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "validateTokens: Attempting Validate loginID" + loginID + " tokenKey " + tokenKey + " Purpose: " + purpose);
            pUSS.setUserLogin(loginID);
            pUSS.setPasswordToken(tokenKey + purpose);
            oimAdminClient.getRestTemplate().put(serverUrl + OIMRestClient.ServiceURLs.VALIDATE_TOKEN.getUrl(),
                    pUSS, pUSS.getUserLogin());
        } catch (Exception e) {
            LOGGER.error(METHOD_NAME + "RemoteAddress: " + remoteAddress + "PUT ERROR ", e);
            return "TokenExpired";
        }

        return "TokenValid";
    }

    /**
     * Method description
     *
     *
     * @param pUSS
     * @param context
     *
     * @return
     */
    public String validateApproveEmailChangeTokens(UserSelfService pUSS, String loginID, String tokenKey, RequestContext context) {
        String METHOD_NAME = " validateApproveEmailChangeTokens ";
        String remoteAddress = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest().getRemoteAddr();

        try {
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "validateApproveEmailChangeTokens: Attempting Validate loginID" + loginID + " tokenKey " + tokenKey);
            pUSS.setUserLogin(loginID);
            pUSS.setPasswordToken(tokenKey + "EMAIL");
            oimAdminClient.getRestTemplate().put(serverUrl + OIMRestClient.ServiceURLs.VALIDATE_EMAIL_CHANGE_TOKEN.getUrl(),
                    pUSS, pUSS.getUserLogin());
        } catch (HttpClientErrorException e) {
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "PUT ERROR ", e);
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "PUT ERROR " + e.getResponseBodyAsString());
            return e.getResponseBodyAsString();
        } catch (Exception e) {
            LOGGER.error(METHOD_NAME + "RemoteAddress: " + remoteAddress + "PUT ERROR ", e);
            return "TOKENExpired";
        }
        return "TokenValid";
    }

    /**
     * Method description
     *
     *
     * @return
     *
     * @throws Exception
     */
    public String getPasswordPolicyForUser(String pUserID) throws Exception {
        String METHOD_NAME = " getPasswordPolicyForUser ";
        String remoteAddress = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest().getRemoteAddr();

        String challenges = null;

        try {
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "getPasswordPolicyForUser pUserID" + pUserID);
            challenges = oimAdminClient.getRestTemplate().getForObject(serverUrl
                    + OIMRestClient.ServiceURLs.GET_USER_PASSWORDPOLICY.getUrl(), String.class, pUserID);
        } catch (HttpClientErrorException e) {
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "HCEE  ERROR " + e.getResponseBodyAsString());

            throw new Exception(e.getMessage());
        } catch (Exception e) {
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "PUT ERROR " + e.getMessage());

            throw new Exception(e.getMessage());
        }

        LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "{}", challenges);

        return challenges;
    }

    public String checkChalengeQuestionAnswersSet(String loginID) {
        String METHOD_NAME = " checkChalengeQuestionAnswersSet ";
        String remoteAddress = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest().getRemoteAddr();

        ArrayList<String> challenges = null;
        try {
            challenges = oimAdminClient.getRestTemplate().getForObject(serverUrl
                    + OIMRestClient.ServiceURLs.GET_USER_CHALLENGES.getUrl(), ArrayList.class, loginID);
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "challenges " + challenges);

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return "ChallenegQANotSet";
        }
        if (challenges != null && !challenges.isEmpty()) {
            return "ChallenegQASet";
        } else {
            return "ChallenegQANotSet";
        }
    }

    public String sendEmailUsingTemplateName(String userId, String templateName, RequestContext context) {
        String METHOD_NAME = " sendEmailUsingTemplateName ";
        String remoteAddress = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest().getRemoteAddr();

        String result = "ERROR";
        try {
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "Attempting Put account " + userId);
            oimAdminClient.getRestTemplate().put(serverUrl + OIMRestClient.ServiceURLs.SEND_EMAIL_USING_TEMPLATE_NAME.getUrl(), templateName, userId);
            result = "success";
        } catch (HttpClientErrorException e) {
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "PUT ERROR ", e);

            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "HCEE PUT ERROR " + e.getResponseBodyAsString());
            return "ERROR";
        } catch (Exception e) {
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "PUT ERROR ", e);

            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "PUT ERROR " + e.getMessage());

            return "ERROR";
        }

        return result;
    }

    public String updateSSClaimedFlag(String loginID) throws UserUpdateException, Exception {
        String METHOD_NAME = " updateSSClaimedFlag ";
        String remoteAddress = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest().getRemoteAddr();

        LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "loginID " + loginID);
        UserAccount account = new UserAccount();
        account.setUserName(loginID);
        account.setAcctClaimed("Y");
        return updateOIMUser(account);
    }

    public String returnValue(String changeResults) throws UserUpdateException, Exception {
        String METHOD_NAME = " returnValue ";
        String remoteAddress = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest().getRemoteAddr();

        LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "changeResults " + changeResults);
        return changeResults;
    }

    public String validateUserAuthentication(UserSelfService pUSS, RequestContext context) {
        String METHOD_NAME = " validateUserAuthentication ";
        RequestAttributes requestAttributes = RequestContextHolder
                .currentRequestAttributes();
        ServletRequestAttributes attributes = (ServletRequestAttributes) requestAttributes;
        HttpServletRequest request = attributes.getRequest();
        HttpSession httpSession = request.getSession(true);
        
        String password = pUSS.getCurrentPassword();
        String loginID = pUSS.getUserLogin();
        if (loginID != null) {
            if (loginID.indexOf(LOGIN_ID_SUFFIX) == -1) {
                context.getMessageContext().addMessage(
                        new MessageBuilder().error().source("userLogin").code("error_userLogin_domain_not_given").defaultText("Please add the domain name.").build());
                return "ValueError";
            }
        } else if(loginID == null){
                context.getMessageContext().addMessage(
                        new MessageBuilder().error().source("userLogin").code("error_userLogin_not_given").defaultText("Username field cannot be blank").build());
                return "ValueError";
        }
        if(password == null || password.isEmpty()){
                context.getMessageContext().addMessage(
                        new MessageBuilder().error().source("currentPassword").code("error_currentPassword_not_given").defaultText("Password field cannot be blank").build());
                return "ValueError";
        }
        
        String remoteAddress = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest().getRemoteAddr();
        String userLogin = BaseSelfServiceUtilities.truncateLoginIDSuffix(pUSS.getUserLogin());
        
        if(userLogin == null || userLogin.isEmpty()){
                context.getMessageContext().addMessage(
                        new MessageBuilder().error().source("userLogin").code("error_userLogin_not_given").defaultText("Please provide user-login.").build());
                return "ValueError";
        }
        try {
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "validateUserAuthentication: Attempting Validate loginID" + pUSS.getUserLogin());
            //pUSS.setUserLogin(loginID);
            oimAdminClient.getRestTemplate().put(serverUrl + OIMRestClient.ServiceURLs.VALIDATE_USER_AUTHENTICATION.getUrl(),
                    pUSS, userLogin);
        } catch (Exception e) {
            LOGGER.error(METHOD_NAME + "RemoteAddress: " + remoteAddress + "PUT ERROR ", e);
            return "AuthFailed";
        }
        httpSession.setAttribute("userLogin", userLogin);
        return "Authenticated";
    }

    public String validateUserAuthentication(RequestContext context) {
        String METHOD_NAME = " validateUserAuthentication ";
        RequestAttributes requestAttributes = RequestContextHolder
                .currentRequestAttributes();
        ServletRequestAttributes attributes = (ServletRequestAttributes) requestAttributes;
        HttpServletRequest request = attributes.getRequest();
        HttpSession httpSession = request.getSession(true);
        String userID = null;

        String remoteAddress = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest().getRemoteAddr();

        try {
            Object user = null;
            user = httpSession.getAttribute("userLogin");
            if (user != null) {
                userID = (String) user;
            }

            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "validateUserAuthentication: Loggedin loginID" + userID);
        } catch (Exception e) {
            LOGGER.error(METHOD_NAME + "RemoteAddress: " + remoteAddress + "ERROR ", e);
        }
        return userID;

    }
    public String checkIfDoaminEntered(UserSelfService userSelfService, RequestContext context) {
        String METHOD_NAME = " checkIfDoaminEntered ";
        String remoteAddress = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest().getRemoteAddr();
        LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "userSelfService " + userSelfService);
        String loginID = userSelfService.getUserLogin();
        String employeeID = userSelfService.getEmployeeID();
        if((employeeID != null && !employeeID.isEmpty())){
            return "DomainEntered";
        }
        if (loginID != null) {
            if (loginID.indexOf(LOGIN_ID_SUFFIX) == -1) {
                context.getMessageContext().addMessage(
                        new MessageBuilder().error().source("userLogin").code("error_userLogin_domain_not_given").defaultText("Please add the domain name.").build());
                return "DomainNotEntered";
            }
        } else if((loginID == null || loginID.isEmpty() || loginID.equalsIgnoreCase(LOGIN_ID_SUFFIX)) && (employeeID == null || employeeID.isEmpty())){
                context.getMessageContext().addMessage(
                        new MessageBuilder().error().source("employeeID").code("error_employeeID_not_given").defaultText("Please provide employee id or user-login.").build());
                return "DomainNotEntered";

        }
        return "DomainEntered";
    }
    
    public void logoutAuthenticatedUser(RequestContext context) {
        String METHOD_NAME = " logoutAuthenticatedUser ";
        RequestAttributes requestAttributes = RequestContextHolder
                .currentRequestAttributes();
        ServletRequestAttributes attributes = (ServletRequestAttributes) requestAttributes;
        HttpServletRequest request = attributes.getRequest();
        HttpSession httpSession = request.getSession(true);

        String remoteAddress = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest().getRemoteAddr();

        try {
            httpSession.setAttribute("userLogin", null);
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + METHOD_NAME + ": Session invalidated.");
        } catch (Exception e) {
            LOGGER.error(METHOD_NAME + "RemoteAddress: " + remoteAddress + "ERROR ", e);
        }
    }

    public String setMobileNumberInOIM(MobileNumberUserUpdate mobileNumberUserUpdate, String loginID, RequestContext context) {
        String METHOD_NAME = " setMobileNumberInOIM ";
        String remoteAddress = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest().getRemoteAddr();
        LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + METHOD_NAME + ": loginID " + loginID + " mobileNumberUserUpdate " +mobileNumberUserUpdate);
        
        
        boolean checkAnyError = mobileNumberUserUpdate.checkIfCorrectUpdates(context.getMessageContext());
        if(checkAnyError) return "mobileNumberUpdateFailed";
        
        UserAccount account = new UserAccount();

        account.setUserName(loginID);
        String mobileNumber = mobileNumberUserUpdate.getMobileNumber();
        if (mobileNumber == null) {
            mobileNumber = "";
        }
        String countryCode = mobileNumberUserUpdate.getCountryCode();
        if (countryCode == null) {
            countryCode = "";
        }
        
        
        if (mobileNumber != null && !mobileNumber.isEmpty()) {
            if(!countryCode.isEmpty())
                account.setPhoneNumber(countryCode+"-"+mobileNumber);
            else account.setPhoneNumber(mobileNumber); 
            LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + METHOD_NAME + ": updating user with account  " +account);

            try {
                updateOIMUser(account);
            } catch (Exception e) {
                LOGGER.trace(METHOD_NAME + "RemoteAddress: " + remoteAddress + "PUT ERROR " + e.getMessage());
            }
        }
        return "UpdateSuccessful";
    }

}
