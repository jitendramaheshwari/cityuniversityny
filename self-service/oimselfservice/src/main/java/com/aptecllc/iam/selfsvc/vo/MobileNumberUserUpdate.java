package com.aptecllc.iam.selfsvc.vo;

//~--- non-JDK imports --------------------------------------------------------


import org.springframework.binding.message.MessageBuilder;
import org.springframework.binding.message.MessageContext;
import org.springframework.binding.validation.ValidationContext;

//~--- JDK imports ------------------------------------------------------------

import java.io.Serializable;

public class MobileNumberUserUpdate implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -3227556438871514476L;
    private String            userName;
    private String            mobileNumber;
    private String            confirmMobileNumber;

    private String            countryCode;
    private String            confirmCountryCode;
    private String mobileNumberInDB;
    
    private String failed;

    public MobileNumberUserUpdate() {
        userName  = null;
        failed = null;
        mobileNumber = null;
        confirmMobileNumber = null;
        confirmCountryCode = null;
        countryCode = null;
mobileNumberInDB = null;

    }

    public MobileNumberUserUpdate(String pUserName) {
        userName  = pUserName;
        failed = null;
        mobileNumber = null;
        confirmMobileNumber = null;
        confirmCountryCode = null;
        countryCode = null;
        mobileNumberInDB = null;
    }


    /**
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    public String getFailed() {
        return failed;
    }

    public void setFailed(String failed) {
        this.failed = failed;
    }

    /**
     * @param userName the userName to set
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * @return the smsNumber
     */

    /*
     *  (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "UserUpdate [mobileNumber=" + mobileNumber + ", userName=" + userName + "]";
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getConfirmMobileNumber() {
        return confirmMobileNumber;
    }

    public void setConfirmMobileNumber(String confirmMobileNumber) {
        this.confirmMobileNumber = confirmMobileNumber;
    }


    public void validateGetUpdates(ValidationContext context) {
        MessageContext messages = context.getMessageContext();
        checkIfCorrectUpdates(messages);
    }

    
    
    
    
    
    public boolean checkIfCorrectUpdates(MessageContext messages) {
        boolean error = false;
        String newEmailAddress = this.getMobileNumber();
        String confirmNewEmailAddress = this.getConfirmMobileNumber();
        if(newEmailAddress ==null) newEmailAddress = "";
        if(confirmNewEmailAddress ==null) confirmNewEmailAddress = "";
        if(!confirmNewEmailAddress.equalsIgnoreCase(newEmailAddress)) {
            error = true;
                messages.addMessage(
                        new MessageBuilder().error().source("mobileNumber").code("error_mobile_number_confirm_no_match").defaultText("Phone Number & Confirm Phone Number do not match.").build());
        } else if (!this.getMobileNumber().isEmpty()) {
            boolean valid = validatePhoneNumber(this.getMobileNumber());
            if (!valid) {
                error = true;
                messages.addMessage(
                        new MessageBuilder().error().source("mobileNumber").code("error_mobile_number_invalid").defaultText("Phone Number is not valid.").build());
            }
        }
        
        
        String countryCode = this.getCountryCode();
        String confirmCountryCode = this.getConfirmCountryCode();
        if(countryCode ==null) countryCode = "";
        if(confirmCountryCode ==null) confirmCountryCode = "";
        if(!confirmCountryCode.equalsIgnoreCase(countryCode)) {
            error = true;
                messages.addMessage(
                        new MessageBuilder().error().source("countryCode").code("error_country_code_confirm_no_match").defaultText("Country Code & Confirm Country Code do not match.").build());
        } else if (!this.getCountryCode().isEmpty()) {
            boolean valid = validateCountryCode(this.getCountryCode());
            if (!valid) {
                error = true;
                messages.addMessage(
                        new MessageBuilder().error().source("countryCode").code("error_country_code_invalid").defaultText("Country Code is not valid.").build());
            }
        } 
        return error;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getConfirmCountryCode() {
        return confirmCountryCode;
    }

    public void setConfirmCountryCode(String confirmCountryCode) {
        this.confirmCountryCode = confirmCountryCode;
    }
	private boolean validatePhoneNumber(String phoneNo) {
		//validate phone numbers of format "1234567890"
		if(phoneNo.length()>15) return false;
                else if (phoneNo.matches("[0-9]+")) return true;
		//validating phone number with -, . or spaces
		//else if(phoneNo.matches("\\d{3}[-\\.\\s]\\d{3}[-\\.\\s]\\d{4}")) return true;
		//validating phone number with extension length from 3 to 5
		//else if(phoneNo.matches("\\d{3}-\\d{3}-\\d{4}\\s(x|(ext))\\d{3,5}")) return true;
		//validating phone number where area code is in braces ()
		//else if(phoneNo.matches("\\(\\d{3}\\)-\\d{3}-\\d{4}")) return true;
		//return false if nothing matches the input
		else return false;
		
	}
	private boolean validateCountryCode(String phoneNo) {
		//validate phone numbers of format "1234567890"
		if(phoneNo.length()>3) return false;
                else if (phoneNo.matches("[0-9]+")) return true;
		//validating phone number with -, . or spaces
		//else if(phoneNo.matches("\\d{3}[-\\.\\s]\\d{3}[-\\.\\s]\\d{4}")) return true;
		//validating phone number with extension length from 3 to 5
		//else if(phoneNo.matches("\\d{3}-\\d{3}-\\d{4}\\s(x|(ext))\\d{3,5}")) return true;
		//validating phone number where area code is in braces ()
		//else if(phoneNo.matches("\\(\\d{3}\\)-\\d{3}-\\d{4}")) return true;
		//return false if nothing matches the input
		else return false;
		
	}

    public String getMobileNumberInDB() {
        return mobileNumberInDB;
    }

    public void setMobileNumberInDB(String mobileNumberInDB) {
        this.mobileNumberInDB = mobileNumberInDB;
    }

}
