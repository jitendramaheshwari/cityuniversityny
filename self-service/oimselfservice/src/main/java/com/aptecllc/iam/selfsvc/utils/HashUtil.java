/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aptecllc.iam.selfsvc.utils;
import java.io.UnsupportedEncodingException;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jitendra.maheshwari
 */
public class HashUtil {
        private static String convertToHex(byte[] data) {
         StringBuffer buf = new StringBuffer();
         for (int i = 0; i < data.length; i++) {
                 int halfbyte = (data[i] >>> 4) & 0x0F;
                 int two_halfs = 0;
                 do {
                     if ((0 <= halfbyte) && (halfbyte <= 9))
                         buf.append((char) ('0' + halfbyte));
                     else
                         buf.append((char) ('a' + (halfbyte - 10)));
                     halfbyte = data[i] & 0x0F;
                 } while(two_halfs++ < 1);
         }
         return buf.toString();
    }
    
    public static String getSHA1Hash(String poValue) {
        MessageDigest md;
        try {
            md = MessageDigest.getInstance("SHA-1");
            byte[] hash = md.digest(poValue.getBytes());
            return HashUtil.convertToHex(hash);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(HashUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
        return poValue;
    }

}
