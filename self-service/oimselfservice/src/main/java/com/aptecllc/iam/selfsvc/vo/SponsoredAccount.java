package com.aptecllc.iam.selfsvc.vo;

//~--- non-JDK imports --------------------------------------------------------

import org.springframework.binding.message.MessageBuilder;
import org.springframework.binding.message.MessageContext;
import org.springframework.binding.validation.ValidationContext;
import org.springframework.format.annotation.DateTimeFormat;

//~--- JDK imports ------------------------------------------------------------

import java.io.Serializable;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Class description
 *
 *
 * @version        1.0, 14/04/06
 * @author         Eric A. Fisher, APTEC LLC
 */
public class SponsoredAccount implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @DateTimeFormat(pattern = "MM/dd/yyyy")
    private Date              DOB;
    private String            dobMonth;
    private String            dobDay;
    private String            dobYear;
    private String            firstName;
    private String            middleName;
    private String            lastName;
    private String            office;
    private String            departmentNumber;
    private String            telephone;
    private String            emailAddress;
    private String            orgName;
    @DateTimeFormat(pattern = "MM/dd/yyyy")
    private Date              startDate;
    @DateTimeFormat(pattern = "MM/dd/yyyy")
    private Date              endDate;
    private String            title;

    /**
     * Constructs ...
     *
     */
    public SponsoredAccount() {
        orgName   = "Guests";
        startDate = new Date();
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getTitle() {
        return title;
    }

    /**
     * Method description
     *
     *
     * @param title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public Date getStartDate() {
        return startDate;
    }

    /**
     * Method description
     *
     *
     * @param startDate
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public Date getEndDate() {
        return endDate;
    }

    /**
     * Method description
     *
     *
     * @param endDate
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getOrgName() {
        return orgName;
    }

    /**
     * Method description
     *
     *
     * @param orgName
     */
    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getOffice() {
        return office;
    }

    /**
     * Method description
     *
     *
     * @param office
     */
    public void setOffice(String office) {
        this.office = office;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getDepartmentNumber() {
        return departmentNumber;
    }

    /**
     * Method description
     *
     *
     * @param departmentNumber
     */
    public void setDepartmentNumber(String departmentNumber) {
        this.departmentNumber = departmentNumber;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getTelephone() {
        return telephone;
    }

    /**
     * Method description
     *
     *
     * @param telephone
     */
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getEmailAddress() {
        return emailAddress;
    }

    /**
     * Method description
     *
     *
     * @param emailAddress
     */
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    /**
     * Method description
     *
     *
     * @param context
     */
    public void validateEnterRequestInfo(ValidationContext context) {
        MessageContext messages = context.getMessageContext();

        checkDateOfBirth(messages);
    }

    /**
     * Method description
     *
     *
     * @param context
     */
    public void validateEnterSponsorshipInfo(ValidationContext context) {
        MessageContext    messages = context.getMessageContext();
        GregorianCalendar cal      = new GregorianCalendar();

        cal.add(Calendar.YEAR, 1);

        if (this.getStartDate() == null) {
            messages.addMessage(
                new MessageBuilder().error().source("StartDate").defaultText("Start Date is required.").build());
        } 

        if (this.getEndDate() == null) {
            messages.addMessage(
                new MessageBuilder().error().source("EndDate").defaultText("End Date is required.").build());
        } else if (this.getEndDate().after(cal.getTime())) {
            messages.addMessage(
                new MessageBuilder().error().source("EndDate").defaultText(
                    "End date can not be more than one year in the future.").build());
        }
    }

    private void checkDateOfBirth(MessageContext messages) {
        boolean          validateDate = true;
        SimpleDateFormat dobFormat    = new SimpleDateFormat("yyyyMMd");

        if (this.getDobMonth().isEmpty() || this.getDobMonth().equals("Month")) {
            messages.addMessage(
                new MessageBuilder().error().source("dobMonth").defaultText("Select your birth month.").build());
            validateDate = false;
        }

        if (this.getDobDay().isEmpty()) {
            messages.addMessage(
                new MessageBuilder().error().source("dobDay").defaultText("Enter your birth day.").build());
            validateDate = false;
        }

        if (this.getDobYear().isEmpty()) {
            messages.addMessage(
                new MessageBuilder().error().source("dobYear").defaultText("Enter your birth year.").build());
            validateDate = false;
        }

        if (validateDate) {
            String dob = this.getDobYear() + this.getDobMonth() + this.getDobDay();

            // System.out.println(dob);
            try {
                Date dobDate = dobFormat.parse(dob);

                this.setDOB(dobDate);

                // System.out.println(dobDate);
            } catch (ParseException e) {
                messages.addMessage(
                    new MessageBuilder().error().source("DOB").defaultText("Date of Birth is Invalid.").build());
            }
        }
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public Date getDOB() {
        return DOB;
    }

    /**
     * Method description
     *
     *
     * @param dOB
     */
    public void setDOB(Date dOB) {
        DOB = dOB;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getDobMonth() {
        return dobMonth;
    }

    /**
     * Method description
     *
     *
     * @param dobMonth
     */
    public void setDobMonth(String dobMonth) {
        this.dobMonth = dobMonth;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getDobDay() {
        return dobDay;
    }

    /**
     * Method description
     *
     *
     * @param dobDay
     */
    public void setDobDay(String dobDay) {
        this.dobDay = dobDay;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getDobYear() {
        return dobYear;
    }

    /**
     * Method description
     *
     *
     * @param dobYear
     */
    public void setDobYear(String dobYear) {
        this.dobYear = dobYear;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Method description
     *
     *
     * @param firstName
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getMiddleName() {
        return middleName;
    }

    /**
     * Method description
     *
     *
     * @param middleName
     */
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Method description
     *
     *
     * @param lastName
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
