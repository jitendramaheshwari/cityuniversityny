package com.aptecllc.iam.selfsvc;

//~--- non-JDK imports --------------------------------------------------------

import com.aptecllc.http.client.TokenAuthHeaderRequestInterceptor;

import org.apache.http.client.HttpClient;
import org.apache.http.conn.ssl.AllowAllHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContexts;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.HttpClientBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

//~--- JDK imports ------------------------------------------------------------

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

import java.util.Collections;

import javax.net.ssl.SSLContext;

/**
 * Class description
 *
 *
 * @version        1.0, 13/10/09
 * @author         Eric A. Fisher, APTEC LLC
 */
public class OIMRestClient {
    private static final Logger LOGGER = LoggerFactory.getLogger(OIMRestClient.class);
    private RestTemplate        restTemplate;
    private String              operationUsername;
    private String              serverUrl;

    /**
     * Constructs ...
     *
     */
    public OIMRestClient() {
        restTemplate = new RestTemplate();
    }

    /**
     * Constructs ...
     *
     *
     *
     * @param pServerUrl
     * @param pApiKey
     * @param pSharedSecret
     * @param pOperationUsername
     */
    public OIMRestClient(String pServerUrl, String pApiKey, String pSharedSecret, String pOperationUsername) {
        restTemplate      = new RestTemplate();
        operationUsername = pOperationUsername;
        serverUrl         = pServerUrl;

        ClientHttpRequestInterceptor tokenAuthInterceptor = new TokenAuthHeaderRequestInterceptor(pApiKey,
                                                                pSharedSecret, pOperationUsername);

        restTemplate.setInterceptors(Collections.singletonList(tokenAuthInterceptor));

        // Use to disable hostname verification
        SSLContext sslContext = null;

        try {
            sslContext = SSLContexts.custom().loadTrustMaterial(null, new TrustSelfSignedStrategy()).useTLS().build();
        } catch (KeyManagementException e) {
            LOGGER.error("Key Management Error", e);
        } catch (NoSuchAlgorithmException e) {
            LOGGER.error("No Such Algorithm Exception", e);
        } catch (KeyStoreException e) {
            LOGGER.error("Key Store Exception", e);
        }

        SSLConnectionSocketFactory connectionFactory = new SSLConnectionSocketFactory(sslContext,
                                                           new AllowAllHostnameVerifier());
        HttpClient               httpClient     =
            HttpClientBuilder.create().setSSLSocketFactory(connectionFactory).build();
        ClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory(httpClient);

        // Use normally - do not disable hostname verification
        // HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
        restTemplate.setRequestFactory(requestFactory);
    }

    public enum ServiceURLs {
        GET_USER_BY_LOGIN("/api/v1/user/{userName}"), GET_USER_BY_ID("/api/v1/user/acctid/{accountId}"),
        GET_USER_BY_KEY("/api/v1/user/key/{userKey}"), CREATE_USER("/api/v1/user"),
        UPDATE_USER_BY_LOGIN("/api/v1/user/{userName}"), UPDATE_USER_BY_ID("/api/v1/user/acctid/{accountId}"),
        GET_USER_CHALLENGES("/api/v1/user/{userName}/challenges"),GET_USER_CHALLENGEQSLIST("/api/v1/user/{userName}/challengeqslist"),
        SET_USER_CHALLENGES("/api/v1/user/{userName}/challenges"),
        CHANGE_USER_PASSWORD("/api/v1/user/{userName}/password"), GET_SYSTEM_CHALLENGES("/api/v1/challenges"),
        VALIDATE_USER_PASSWORD("/api/v1/user/{userName}/validatepassword"),
        VALIDATE_USER_PASSWORD_TOKEN("/api/v1/user/{userName}/validatepasswordwithtoken"),
        SEND_EMAIL_FOR_PASSWORD_CHANGE("/api/v1/user/{userName}/sendEMailForPasswordChange"),
        SEARCH_USERS("/api/v1/users?query={query}"), REQUEST_USER("/api/v1/request/user"),
        VALIDATE_TOKEN("/api/v1/user/{userName}/validatetoken"),
        VALIDATE_EMAIL_CHANGE_TOKEN("/api/v1/user/{userName}/validateemailchangetoken"),
        VALIDATE_USER_AUTHENTICATION("/api/v1/user/{userName}/validateautentication"),

        SEND_EMAIL_USING_TEMPLATE_NAME("/api/v1/user/{userName}/sendemailusingtemplate"),
        GET_USER_PASSWORDPOLICY("/api/v1/user/{userName}/passwordpolicytext"),
        SEND_SMS("/notify/v1/sendsms"), GET_RANDOM_ADDRESSES("/api/v1/fau/randomaddress?num={numAddress}");

        private String id;
        private String url;

        private ServiceURLs(String id) {
            this.id  = id;
            this.url = id;
        }

        private ServiceURLs(String id, String url) {
            this.id  = id;
            this.url = url;
        }

        /**
         * Method description
         *
         *
         * @return
         */
        public String getId() {
            return this.id;
        }

        /**
         * Method description
         *
         *
         * @return
         */
        public String getUrl() {
            return this.url;
        }
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public RestTemplate getRestTemplate() {
        return this.restTemplate;
    }
}
