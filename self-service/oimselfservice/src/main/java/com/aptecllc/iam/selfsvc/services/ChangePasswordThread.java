/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aptecllc.iam.selfsvc.services;

import com.aptecllc.iam.selfsvc.OIMRestClient;
import com.aptecllc.iam.selfsvc.vo.UserSelfService;
import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.HttpClientErrorException;

/**
 *
 * @author jitendra.maheshwari
 */
public class ChangePasswordThread implements Runnable {

    private static Map<String, HttpClientErrorException> exceptionMap = new HashMap<String, HttpClientErrorException>();
    private static Map<String, String> statusMap = new HashMap<String, String>();

    private OIMRestClient oimClient;
    private UserSelfService pUSS;
    private String randomToken;
    private String serverUrl;
    private static Logger LOGGER = LoggerFactory.getLogger(OIMUserServicesClient.class);

    public ChangePasswordThread(OIMRestClient oimClient, UserSelfService pUSS, String randomToken, String serverUrl) {
        this.oimClient = oimClient;
        this.pUSS = pUSS;
        this.randomToken = randomToken;
        this.serverUrl = serverUrl;
        LOGGER.trace("Enetring constructor of ChangePasswordThread ");

    }

    public void run() {
        String METHOD_NAME = " run ";
        String returnValue = "PasswordSet";
        LOGGER.trace("Enetring run method of ChangePasswordThread ");

        try {
            oimClient.getRestTemplate().put(serverUrl + OIMRestClient.ServiceURLs.CHANGE_USER_PASSWORD.getUrl(), pUSS,
                    pUSS.getUserLogin());
            LOGGER.trace("invoked the rest call");
        } catch (HttpClientErrorException e) {
            LOGGER.trace(METHOD_NAME + "HCEE PUT ERROR " + e.getResponseBodyAsString() + e.getStatusText());
            returnValue = "PasswordSetFailed";
            notifyException(e, randomToken);
        } catch (Throwable e) {
            LOGGER.trace(METHOD_NAME + "PUT ERROR " + e.getMessage());
            returnValue = "Error";
        }
        notifyCompletion(returnValue, randomToken);
    }

    public void notifyCompletion(String returnValue, String randomToken) {
        String METHOD_NAME = " notifyCompletion ";
        statusMap.put(randomToken, returnValue);
    }

    public void notifyException(HttpClientErrorException e, String randomToken) {
        String METHOD_NAME = " notifyException ";
        exceptionMap.put(randomToken, e);
    }

    public static String getChangeStatus(String tokenKey) throws HttpClientErrorException {
        String METHOD_NAME = " getChangeStatus ";
        HttpClientErrorException exception = null;
        String status = null;
        LOGGER.trace(METHOD_NAME + "Entering with tokenKey " + tokenKey); //To change body of generated methods, choose Tools | Templates.
        if (exceptionMap.containsKey(tokenKey)) {
            exception = exceptionMap.get(tokenKey);
            exceptionMap.remove(exception);
            throw exception;
        } else if (statusMap.containsKey(tokenKey)) {
            status = statusMap.get(tokenKey);
            statusMap.remove(status);
            return status;
        } else {
            return "waiting";
        }
    }
}
