package com.aptecllc.iam.selfsvc.utils;

//~--- non-JDK imports --------------------------------------------------------

import com.aptecllc.iam.selfsvc.exception.NoQuestionException;
import com.aptecllc.iam.selfsvc.exception.ResetAttemptsExceeded;
import static com.aptecllc.iam.selfsvc.services.OIMUserServicesClient.LOGIN_ID_SUFFIX;
import com.aptecllc.iam.selfsvc.vo.SecurityQuestions;
import com.aptecllc.iam.selfsvc.vo.SponsoredAccount;
import com.aptecllc.iam.selfsvc.vo.UserAccount;
import com.aptecllc.iam.selfsvc.vo.UserRegistration;
import com.aptecllc.iam.selfsvc.vo.UserSelfService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//~--- JDK imports ------------------------------------------------------------


import java.util.Random;

/**
 * Class description
 *
 *
 * @version        1.0, 14/05/12
 * @author         Eric A. Fisher, APTEC LLC
 */
public abstract class BaseSelfServiceUtilities {
    private final static Logger LOGGER = LoggerFactory.getLogger(BaseSelfServiceUtilities.class);
    private static Random              rand   = new Random();

    /**
     * Constructs ...
     *
     */
    public BaseSelfServiceUtilities() {}

    /**
     * Method description
     *
     *
     * @param oimUser
     *
     * @return
     */
    public String determineRegistrationStatus(UserAccount oimUser) {
        String result = "Unknown Status";

        LOGGER.trace("determineRegStatus: {}", oimUser);

        if ("Disabled".equalsIgnoreCase(oimUser.getStatus())) {
            result = "Security Disabled";
        } else if ("Y".equals(oimUser.getAcctClaimed()) || "1".equals(oimUser.getAcctClaimed())) {
            result = "Account Registered";
        } else if ("R".equals(oimUser.getAcctClaimed())) {
            result = "ReRegister Account";
        } else {
            if (oimUser.getPhoneNumber() == null) {
                result = "Register Account";
            } else if (oimUser.getPhoneNumber() != null) {
                if (!oimUser.getPhoneNumber().isEmpty()) {
                    result = "Register Account with EMAIL";
                } else {
                    result = "Register Account";
                }
            }
        }

        LOGGER.trace("determineRegStatus: {} ", result);

        return result;
    }

    /**
     * Method description
     *
     *
     * @param user
     * @param regStatus
     *
     * @return
     */
    public UserAccount getAccountforStatusUpdate(UserAccount user, String regStatus) {
        UserAccount updAccount = new UserAccount();

        updAccount.setUserName(user.getUserName());

        if (!regStatus.isEmpty()) {
            updAccount.setAcctClaimed(regStatus);
        }

        return updAccount;
    }

    /**
     * Method description
     *
     *
     * @param userReg
     *
     * @return
     */
    public UserAccount getNewAccountFromRegistration(UserRegistration userReg) {
        UserAccount user = new UserAccount();

        user.setFirstName(userReg.getFirstName());
        user.setLastName(userReg.getLastName());

        return user;
    }

    /**
     * Method description
     *
     *
     * @param acct
     *
     * @return
     */
    public UserAccount getNewAccountFromSponsoredAccount(SponsoredAccount acct) {
        UserAccount user = new UserAccount();

        user.setFirstName(acct.getFirstName());
        user.setLastName(acct.getLastName());
        user.setOrganizationName(acct.getOrgName());
        user.setAttribute("dateOfBirth", acct.getDOB());
        user.setAttribute("title", acct.getTitle());
        user.setAttribute("phoneNumber", acct.getTelephone());
        user.setAttribute("startDate", acct.getStartDate());
        user.setAttribute("endDate", acct.getEndDate());
        user.setAttribute("departmentNumber", acct.getDepartmentNumber());

        return user;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public static String getRandomCode() {
        String randomCode = "";
        int    randomNum  = rand.nextInt((9999999 - 1000000) + 1) + 1000000;

        randomCode = Integer.toString(randomNum);

        return randomCode;
    }

    /**
     * Method description
     *
     *
     * @param pUSS
     *
     * @return
     */
    public SecurityQuestions getSecurityQuestionsFromUSS(UserSelfService pUSS) throws NoQuestionException, ResetAttemptsExceeded {
        int count = (pUSS.getIncorrectAsnwerCount()).intValue();
        count = count +1; // First Time showing questions is 1.
        pUSS.setIncorrectAsnwerCount(count);
        if(count > 5) {
            LOGGER.trace("resetUserPassword: User will be sent email to reset password");
            throw new ResetAttemptsExceeded();
        }            

        SecurityQuestions questions     = new SecurityQuestions();
        Object[] userQuestions = pUSS.getQuestions().keySet().toArray();
        String question1 = null;
        String question2 = null;
        String question3 = null;
        if (userQuestions.length >= 3) {
            question1 = (String) userQuestions[new Random().nextInt(userQuestions.length)];
            do {
                question2 = (String) userQuestions[new Random().nextInt(userQuestions.length)];
            } while(question2.equals(question1));
            do {
                question3 = (String) userQuestions[new Random().nextInt(userQuestions.length)];
            } while(question3.equals(question1) || question3.equals(question2));

            questions.setQuestion1(question1);
            questions.setQuestion2(question2);
            questions.setQuestion3(question3);
        }
        if (userQuestions.length == 0) {
        	throw new NoQuestionException();
        }
        return questions;
    }

    /**
     * Method description
     *
     *
     * @param pUSS
     * @param questions
     */
    public void setAnswersInUSS(UserSelfService pUSS, SecurityQuestions questions) {
        if (questions.getQuestion1() != null && questions.getAnswer1()!=null && !questions.getAnswer1().isEmpty()) {
            pUSS.getQuestions().put(questions.getQuestion1(), questions.getAnswer1());
        }

        if (questions.getQuestion2() != null && questions.getAnswer2()!=null && !questions.getAnswer2().isEmpty()) {
            pUSS.getQuestions().put(questions.getQuestion2(), questions.getAnswer2());
        }

        if (questions.getQuestion3() != null && questions.getAnswer3()!=null && !questions.getAnswer3().isEmpty()) {
            pUSS.getQuestions().put(questions.getQuestion3(), questions.getAnswer3());
        }

        if (questions.getQuestion4() != null && questions.getAnswer4()!=null && !questions.getAnswer4().isEmpty()) {
            pUSS.getQuestions().put(questions.getQuestion4(), questions.getAnswer4());
        }

        if (questions.getQuestion5() != null && questions.getAnswer5()!=null && !questions.getAnswer5().isEmpty()) {
            pUSS.getQuestions().put(questions.getQuestion5(), questions.getAnswer5());
        }
/*
        if (questions.getQuestion6() != null && questions.getAnswer6()!=null && !questions.getAnswer6().isEmpty()) {
            pUSS.getQuestions().put(questions.getQuestion6(), questions.getAnswer6());
        }

        if (questions.getQuestion7() != null && questions.getAnswer7()!=null && !questions.getAnswer7().isEmpty()) {
            pUSS.getQuestions().put(questions.getQuestion7(), questions.getAnswer7());
        }

        if (questions.getQuestion8() != null && questions.getAnswer8()!=null && !questions.getAnswer8().isEmpty()) {
            pUSS.getQuestions().put(questions.getQuestion8(), questions.getAnswer8());
        }
*/
        pUSS.setNewPassword("");
    }
    
    
    public static String truncateLoginIDSuffix(String userLogin) {
        if (userLogin.equalsIgnoreCase(LOGIN_ID_SUFFIX)) {
            return "";
        } else {
            int loginIdIndex = userLogin.indexOf(LOGIN_ID_SUFFIX);
            if (loginIdIndex != -1) {
                userLogin = userLogin.substring(0, loginIdIndex);
            }
        }
        return userLogin;
    }
}
