Installation:

See the wiki in bitbucket for installation and configuration information.


Create a map in CSF, by default it is looking for a map called "oim". You can change this in the root-context.xml, update the 
csfOimMap value in the csfPropertiesHolder bean definition.

In this map create the properties for:
oimProviderUrl - Which contains the end point for OIM.  Start with t3://localhost:14000/oim 

oimAdminUser - A pair that contains the name of the admin user to use. Start with 'xelsysadm'

Grant JAZN permissions in WLS to this Map for the deployed war filepath.

