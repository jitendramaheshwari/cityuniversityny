package com.aptecllc.oim.property;

//~--- non-JDK imports --------------------------------------------------------

import com.aptecllc.jps.CSFUtilities;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author fforester
 */
public class CSFPropertiesHolder {
    private final static Logger logger = LoggerFactory.getLogger(CSFPropertiesHolder.class);
    private String           csfOimMap;
    private String           csfSMSMap;

    // OIM
    private String oimCf;
    private String oimProviderUrl;
    private String oimAdmUsr;
    private String oimAdmPwd;

    // SMS
    private String smsApiId;
    private String smsSenderId;
    private String smsUserId;
    private String smsPassword;

    /**
     * Method description
     *
     *
     * @return
     */
    public String getCsfOimMap() {
        return csfOimMap;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getCsfSMSMap() {
        return csfSMSMap;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getOimAdmPwd() {
        return oimAdmPwd;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getOimAdmUsr() {
        return oimAdmUsr;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getOimCf() {
        return oimCf;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getOimProviderUrl() {
        return oimProviderUrl;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getSmsApiId() {
        return smsApiId;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getSmsPassword() {
        return smsPassword;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getSmsSenderId() {
        return smsSenderId;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getSmsUserId() {
        return smsUserId;
    }

    /**
     * Method description
     *
     *
     * @param csfOimMap
     */
    public void setCsfOimMap(String csfOimMap) {
        this.csfOimMap = csfOimMap;
    }

    /**
     * Method description
     *
     *
     * @param csfSMSMap
     */
    public void setCsfSMSMap(String csfSMSMap) {
        this.csfSMSMap = csfSMSMap;
    }

    /**
     * Method description
     *
     *
     * @param oimAdmPwd
     */
    public void setOimAdmPwd(String oimAdmPwd) {
        this.oimAdmPwd = oimAdmPwd;
    }

    /**
     * Method description
     *
     *
     * @param oimAdmUsr
     */
    public void setOimAdmUsr(String oimAdmUsr) {
        this.oimAdmUsr = oimAdmUsr;
    }

    /**
     * Method description
     *
     *
     * @param oimCf
     */
    public void setOimCf(String oimCf) {
        this.oimCf = oimCf;
    }

    /**
     * Method description
     *
     *
     * @param oimProviderUrl
     */
    public void setOimProviderUrl(String oimProviderUrl) {
        this.oimProviderUrl = oimProviderUrl;
    }

    /**
     * Method description
     *
     *
     * @param smsApiId
     */
    public void setSmsApiId(String smsApiId) {
        this.smsApiId = smsApiId;
    }

    /**
     * Method description
     *
     *
     * @param smsPassword
     */
    public void setSmsPassword(String smsPassword) {
        this.smsPassword = smsPassword;
    }

    /**
     * Method description
     *
     *
     * @param smsSenderId
     */
    public void setSmsSenderId(String smsSenderId) {
        this.smsSenderId = smsSenderId;
    }

    /**
     * Method description
     *
     *
     * @param smsUserId
     */
    public void setSmsUserId(String smsUserId) {
        this.smsUserId = smsUserId;
    }

    /**
     * Method description
     *
     *
     * @throws Exception
     */
    public void setupCsf() throws Exception {
        logger.debug("loading map:" + csfOimMap);

        char[] tmp = null;


        oimProviderUrl = CSFUtilities.getCSFStringProperty(csfOimMap, "oimProviderUrl");
        logger.debug("oimProviderUrl:" + oimProviderUrl);

        if ((oimProviderUrl == null) || (oimProviderUrl.trim().length() == 0)) {
            throw new Exception("csf:" + csfOimMap + " oimProviderUrl is invalid");
        }

        oimAdmUsr = CSFUtilities.getUserIdforCredential(csfOimMap, "oimAdminUser");
        logger.debug("oimAdminUser:" + oimAdmUsr);

        if ((oimAdmUsr == null) || (oimAdmUsr.trim().length() == 0)) {
            throw new Exception("csf:" + csfOimMap + " oimAdmUsr is invalid");
        }

        if (!"".equals(csfSMSMap)) {
            smsApiId = CSFUtilities.getCSFStringProperty(csfSMSMap, "smsApiId");
            logger.debug("smsApiId:" + smsApiId);

            if ((smsApiId == null) || (smsApiId.trim().length() == 0)) {
                throw new Exception("csf:" + csfSMSMap + " smsApiId is invalid");
            }

            smsSenderId = CSFUtilities.getCSFStringProperty(csfSMSMap, "smsSenderId");
            logger.debug("smsSenderId:" + smsSenderId);

            if ((smsSenderId == null) || (smsSenderId.trim().length() == 0)) {
                throw new Exception("csf:" + csfSMSMap + " smsSenderId is invalid");
            }

            smsUserId = CSFUtilities.getUserIdforCredential(csfSMSMap, "smsAccount");
            logger.debug("smsUserId:" + smsUserId);

            if ((smsUserId == null) || (smsUserId.trim().length() == 0)) {
                throw new Exception("csf:" + csfSMSMap + " smsUserId is invalid");
            }

            tmp = CSFUtilities.getPasswordforCredential(csfSMSMap, "smsAccount");

            if ((tmp == null) || (tmp.length == 0)) {
                logger.error("Invalid smsAccount PW");
            }

            smsPassword = new String(tmp);
            logger.debug("apiSecret:" + smsPassword);

            if ((smsPassword == null) || (smsPassword.trim().length() == 0)) {
                throw new Exception("csf:" + csfSMSMap + " smsPassword is invalid");
            }
        }
    }
}
