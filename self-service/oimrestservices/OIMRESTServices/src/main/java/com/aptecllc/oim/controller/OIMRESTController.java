package com.aptecllc.oim.controller;

//~--- non-JDK imports --------------------------------------------------------

import com.aptecllc.oim.exception.QuestionsNotFoundException;
import com.aptecllc.oim.exception.UserException;
import com.aptecllc.oim.exception.UserNotFoundException;
import com.aptecllc.oim.property.PropertyUtil;
import com.aptecllc.oim.services.OIMRequestServices;
import com.aptecllc.oim.services.OIMUserServices;
import com.aptecllc.oim.transformer.ObjectTransformerIntf;
import com.aptecllc.oim.util.ServiceConstants;
import com.aptecllc.oim.vo.ServiceError;
import com.aptecllc.oim.vo.UserAccount;
import com.aptecllc.oim.vo.UserRequest;
import com.aptecllc.oim.vo.UserSelfService;

import oracle.iam.identity.usermgmt.vo.User;
import oracle.iam.platform.entitymgr.vo.SearchCriteria;
import oracle.iam.platform.utils.NoSuchServiceException;




import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

//~--- JDK imports ------------------------------------------------------------






import java.security.Principal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;


/**
 * Class description
 *
 *
 * @version        1.0, 13/11/06
 * @author         Eric A. Fisher, APTEC LLC
 */
@Controller
@RequestMapping("api/v1")
public class OIMRESTController {
    private static final Logger logger = LoggerFactory.getLogger(OIMRESTController.class);
    @Autowired
    private OIMUserServices       userServices;
    @Autowired
    private OIMRequestServices    requestServices;
    @Autowired
    private ObjectTransformerIntf objTransformer;

    /**
     * This method allows for user search based on any of the defined attributes in the attributeMap.
     * It will return a list of UserAccounts that match the passed criteria.  Criteria should be
     * formatted in RSQL format for example.
     *
     * lastName==Smith;firstName==John (Matches Users with First Name of John AND Last Name of Smith
     *
     * Operators
     * EQUAL ==
     * NOT EQUAL !=
     * GT >
     * GTE >=
     * LT <
     * LTE <=
     *
     * AND ; or 'and'
     * OR , or 'or'
    
     * @param searchCriteria
     *
     * @return List of UserAccount objects
     *
     * @throws UserException
     */
    @RequestMapping(
        value    = "users",
        method   = RequestMethod.GET
    )
    public @ResponseBody
    List<UserAccount> findUsers(@RequestParam(
        value    = "query",
        required = true
    ) String searchCriteria) throws UserException {
        List<UserAccount> accounts = new ArrayList<UserAccount>();

        logger.info("Search Criteria: " + searchCriteria);

        SearchCriteria oimSC = objTransformer.parseRSQL(searchCriteria);

        logger.info("Parsed OIM Search: " + oimSC);

        List<User> foundUsers = userServices.searchUsers(oimSC,
                                    new HashSet(objTransformer.getAllAttributesMap().values()));

        for (User oimUser : foundUsers) {
            accounts.add(objTransformer.convertUser(oimUser));
        }

        return accounts;
    }

    /**
     * REST Get Method that returns an UserAccount VO object given the User Login.
     * Response is JSON by default.
     *
     *
     *
     *
     * @param userName - Typically mapped to User Login value in OIM
     *
     * @return UserAccount object
     *
     * @throws UserException 404-Not Found
     */
    @RequestMapping(
        value  = "user/{userName}",
        method = RequestMethod.GET
    )
    public @ResponseBody
    UserAccount getUserByUserName(@PathVariable String userName) throws UserException {
        logger.debug(objTransformer.getAllAttributesMap().values().toString());

        User user = userServices.getUserByIdentifier(PropertyUtil.getProperty(ServiceConstants.USER_NAME.getName()),
                        userName, new HashSet(objTransformer.getAllAttributesMap().values()));

        if (user == null) {
            throw new UserNotFoundException("Username " + userName + " Not Found");
        }

        UserAccount uAcct = objTransformer.convertUser(user);

        return uAcct;
    }

    /**
     * REST Get Method that returns an UserAccount VO object given the Entity ID/USR_KEY.
     * Response is JSON by default.
     *
     * @param userKey
     *
     * @return UserAccount object
     *
     * @throws UserException 404-Not Found
     */
    @RequestMapping(
        value  = "user/key/{userKey}",
        method = RequestMethod.GET
    )
    public @ResponseBody
    UserAccount getUserByKey(@PathVariable String userKey) throws UserException {
        logger.error(objTransformer.getAllAttributesMap().values().toString());

        User user = userServices.getUserByIdentifier("usr_key", userKey,
                        new HashSet(objTransformer.getAllAttributesMap().values()));

        if (user == null) {
            throw new UserNotFoundException("Not Found");
        }

        UserAccount uAcct = objTransformer.convertUser(user);

        return uAcct;
    }

    /**
     * REST Method that returns an UserAccount VO object given the Unique Account ID.
     * The OIM field that contains this value is defined in the attributeMap.properties and is
     * typically an Employee ID, or some other unique value different from the User Login
     *
     * Response is JSON by default.
     *
     *
     *
     * @param accountID
     *
     * @return UserAccount object
     *
     * @throws UserException 404-Not Found
     */
    @RequestMapping(
        value  = "user/acctid/{accountID}",
        method = RequestMethod.GET
    )
    public @ResponseBody
    UserAccount getUserByAccountID(@PathVariable String accountID) throws UserException {
        User user =
            userServices.getUserByIdentifier(PropertyUtil.getProperty(ServiceConstants.USER_ACCOUNTID.getName()),
                accountID, new HashSet(objTransformer.getAllAttributesMap().values()));

        if (user == null) {
            throw new UserException("Null User");
        }

        UserAccount uAcct = objTransformer.convertUser(user);

        return uAcct;
    }

    /**
     * REST Method that is used for creating a new OIM user via POST.  Expects the RequestBody to by a JSON request matching the UserAccount
     * VO.  Will return the URL to the user created.
     *
     *
     *
     * @param account
     *
     * @return 201-Created on success and Header with location to created user.
     *
     * @throws UserException
     */
    @RequestMapping(
        value  = "user",
        method = RequestMethod.POST
    )
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<byte[]> createUser(@RequestBody UserAccount account) throws UserException {

        // String userAccountID = account.getAccountId();
        User user = objTransformer.convertUserAccount(account);

        logger.debug("User Data:" + user.toString());

        String      response = userServices.createUser(user, account.getOrganizationName());
        HttpHeaders headers  = new HttpHeaders();

        headers.add("Location",
                    ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/v1/user/key/"
                        + response).build().toUriString());

        return new ResponseEntity<byte[]>(null, headers, HttpStatus.CREATED);
    }

    /**
     * Request PUT Method that updates an OIM User given the User Login and values to be updated.
     * Performs either a direct modification as the admin user or a self modify user profile if the request user
     * is te same as the authenticated user.
     *
     *
     *
     * @param account
     * @param userName
     * @param principal
     *
     * @return 204-No Content on Success or 400-Bad Request of Failure
     *
     * @throws UserException
     */
    @RequestMapping(
        value  = "user/{userName}",
        method = RequestMethod.PUT
    )
    public @ResponseBody
    ResponseEntity<byte[]> updateUserByLogin(@RequestBody UserAccount account, @PathVariable String userName,
            Principal principal)
            throws UserException {
        String requestingUser = principal.getName();
        String returnCode     = "";

        logger.debug("updateUserbyLogin: {}", account);

        if (userName.equalsIgnoreCase(requestingUser)) {
            returnCode = userServices.modifyUserProfile(account.getAttributes());
        } else {
            returnCode = updateUser(account, userName, PropertyUtil.getProperty(ServiceConstants.USER_NAME.getName()));
        }

        if ("OK".equals(returnCode)) {
            return new ResponseEntity<byte[]>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<byte[]>(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Request PUT Method that updates an OIM User given the Account ID and values to be updated.
     * Performs either a direct modification as the admin user or a self modify user profile if the request user
     * is te same as the authenticated user.
     *
     *
     *
     *
     * @param account
     * @param accountID
     *
     * @return 204-No Content on Success or 400-Bad Request of Failure
     *
     * @throws UserException
     */
    @RequestMapping(
        value  = "user/acctid/{accountID}",
        method = RequestMethod.PUT
    )
    public @ResponseBody
    ResponseEntity<byte[]> updateUserByAccountID(@RequestBody UserAccount account, @PathVariable String accountID)
            throws UserException {
        UserAccount updatedUser = null;
        String      returnCode  = updateUser(account, accountID,
                                      PropertyUtil.getProperty(ServiceConstants.USER_ACCOUNTID.getName()));

        if ("OK".equals(returnCode)) {
            return new ResponseEntity<byte[]>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<byte[]>(HttpStatus.BAD_REQUEST);
        }
    }

    // Returns the list of set challenges for a given user login

    /**
     * Retrieves the configured user challenge questions either as the user or using
     * unauthenticated service.
     *
     *
     * @param userName
     * @param principal
     *
     * @return List of challenges set for the user.
     *
     * @throws UserException 500 Error on any error, for lack of a better response.
     */
    @RequestMapping(
        value  = "user/{userName}/challenges",
        method = RequestMethod.GET
    )
    public @ResponseBody
    List getUserChallengesByLogin(@PathVariable String userName, Principal principal) throws UserException {
        String requestingUser = principal.getName();
        List   challenges     = null;

        try {
            if (userName.equalsIgnoreCase(requestingUser)) {
                logger.debug("SelfRequest");
                challenges = userServices.getUserChallenges();
            } else {
                logger.debug("3rdPartyRequest");
                challenges = getUserChallenges(userName);
            }
        } catch (UserException e) {
            logger.error("UserException:" + e.getMessage(),e);

            throw e;
        }

        return challenges;
    }

    /**
     * Method description
     *
     *
     * @param userName
     *
     * @return
     *
     * @throws UserException
     */
    @RequestMapping(
        value  = "user/{userName}/challengeanswers",
        method = RequestMethod.GET
    )
    public @ResponseBody
    HashMap getUserChallengeAnswersByLogin(@PathVariable String userName,Principal principal) throws QuestionsNotFoundException, UserException {
        HashMap challenges = null;
        
        if (userName.equalsIgnoreCase(principal.getName())) {
	        try {
	            challenges = userServices.getUserChallengesAnswers();
	            
	        }catch (UserException e) {
	            logger.error(e.getMessage());
	
	            throw e;
	        }
        }
        return challenges;
    }

    /**
     * Updates the users challenge questions and answers, this must be performed as the
     * user to be updated.
     *
     *
     * @param userName
     * @param uSS
     * @param principal
     *
     * @return 204-No Content on success.  400-Bad Request on failure.  403-Forbidden if user asserted is not the user being changed.
     */
    @RequestMapping(
        value  = "user/{userName}/challenges",
        method = RequestMethod.PUT
    )
    public @ResponseBody
    ResponseEntity<byte[]> setUserChallenges(@PathVariable String userName, @RequestBody UserSelfService uSS,
            Principal principal) {
        logger.trace("userName:" + userName);

        String requestingUser = principal.getName();
        logger.trace("requestingUser:" + requestingUser);
        logger.trace("uSS:" + uSS);

        if (userName.equalsIgnoreCase(requestingUser)) {
            try {
                userServices.setUserChallenges(uSS.getQuestions());
            } catch (UserException e) {
                logger.error("UserException:", e);
                return new ResponseEntity<byte[]>(e.getMessage().getBytes(), HttpStatus.BAD_REQUEST);
            }
        } else {
            return new ResponseEntity<byte[]>(HttpStatus.FORBIDDEN);
        }

        return new ResponseEntity<byte[]>(HttpStatus.NO_CONTENT);
    }

    /**
     * Performs validation on password prior to password change.  Requires user login and
     * new password.
     *
     *
     * @param userName
     * @param uSS
     *
     * @return 200-OK on success.  400-Bad Request on failure.
     */
    @RequestMapping(
        value  = "user/{userName}/validatepassword",
        method = RequestMethod.PUT
    )
    public @ResponseBody
    ResponseEntity<byte[]> validatePassword(@PathVariable String userName, @RequestBody UserSelfService uSS) {
        String passwordResult = userServices.validatePassword(userName, uSS.getNewPassword().toCharArray());

        if ("Valid".equals(passwordResult)) {
            return new ResponseEntity<byte[]>(HttpStatus.OK);
        } else {
            return new ResponseEntity<byte[]>(passwordResult.getBytes(), HttpStatus.BAD_REQUEST);
        }
    }

    // Uses different methods based on the calling user. Self or 3rd Party

    /**
     * Changes the user password.  This method performs various operations based on the input information,
     * if no QA are defined and and old and new password is defined, the system will change the password for the
     * authenticated user.
     *
     * If QA are defined and there is no old password, the system attempts a forgot password reset.
     *
     * Otherwise if the authenticated user is not the user to be update it attempts an administrator password set, this is
     * of course subject to the permissions in OIM for the administrator account being used.
     *
     *
     * @param userName
     * @param uSS
     * @param principal
     *
     * @return 204-No Content on success
     */
    @RequestMapping(
        value  = "user/{userName}/password",
        method = RequestMethod.PUT
    )
    public @ResponseBody
    ResponseEntity<byte[]> changeUserPassword(@PathVariable String userName, @RequestBody UserSelfService uSS,
            Principal principal) {
        String requestingUser = principal.getName();

        if ((uSS.getQuestions() == null) && (uSS.getOldPassword() != null)) {

            // Normal Password Change
            try {
                userServices.changePasswordAsUser(uSS.getOldPassword(), uSS.getNewPassword(), uSS.getConfirmPassword());
            } catch (UserException e) {
                logger.error("UserException:" + e.getMessage(),e);
                ResponseEntity re = new ResponseEntity<byte[]>(e.getMessage().getBytes(), HttpStatus.METHOD_FAILURE);
                return re;
            }
        } else if ((uSS.getQuestions() != null) && (uSS.getOldPassword() == null)) {

            // QUestion Reset
            try {
                userServices.resetPasswordWithChallenges(uSS.getUserLogin(), uSS.getQuestions(), uSS.getNewPassword());
            } catch (UserException e) {
                logger.error("UserException:" + e.getMessage(),e);
                ResponseEntity re = new ResponseEntity<byte[]>(e.getMessage().getBytes(), HttpStatus.METHOD_FAILURE);
                return re;
            }
        } else if (!requestingUser.equalsIgnoreCase(userName)) {

            // Admin Password Set
            try {
                userServices.setUserPassword(userName, uSS.getNewPassword(), uSS.getForceChange());
            } catch (UserException e) {
                logger.error("UserException:" + e.getMessage(),e);
                ResponseEntity re = new ResponseEntity<byte[]>(e.getMessage().getBytes(), HttpStatus.METHOD_FAILURE);

                return re;
            }
        } else {
            return new ResponseEntity<byte[]>(HttpStatus.NOT_ACCEPTABLE);
        }

        return new ResponseEntity<byte[]>(HttpStatus.NO_CONTENT);
    }

    // Returns the list of system challenges

    /**
     * This service returns the list of defined questions in the system for use in creating input forms
     * for user selection if security questions.
     *
     *
     * @return
     */
    @RequestMapping(
        value  = "challenges",
        method = RequestMethod.GET
    )
    public @ResponseBody
    List getSystemChallenges() {
        return userServices.getSystemChallenges("xelsysadm");
    }

    
        /**
     * Method description
     *
     *
     * @param userName
     *
     * @return
     *
     * @throws UserException
     */
    @RequestMapping(
        value  = "user/{userName}/challengeqslist",
        method = RequestMethod.GET
    )
    public @ResponseBody
    List getUserChallengeQuestionsByLogin(@PathVariable String userName) {
        return userServices.getSystemChallenges(userName);
    }


    
        /**
     * Method description
     *
     *
     * @param userName
     *
     * @return
     *
     * @throws UserException
     */
    @RequestMapping(
        value  = "user/{userName}/passwordpolicytext",
        method = RequestMethod.GET
    )
    public @ResponseBody
    String getUserPasswordPolicyByLogin(@PathVariable String userName) throws UserException {
        String text = userServices.getPasswordPolicy(userName);
        logger.trace("PasswordPolicy Text: " + text);

        return text;
    }

    
    
    /**
     * Initiates a create user request rather than a direct creation.  This would trigger any approvals if required.
     * Returns the request identifier for the generated request.  The requesting user is set as the User Manager on the requested
     * object.
     *
     * @param request
     * @param principal
     *
     * @return 202-Accepted on Request Receipt
     *
     * @throws UserException
     */
    @RequestMapping(
        value  = "request/user",
        method = RequestMethod.POST
    )
    @ResponseStatus(HttpStatus.ACCEPTED)
    public ResponseEntity<byte[]> createUserRequest(@RequestBody UserRequest request, Principal principal)
            throws UserException {
        String requestingUser = principal.getName();

        // String userAccountID = account.getAccountId();
        UserAccount account = (UserAccount) request.getRequestObject();
        User        user    = objTransformer.convertUserAccount(account);
        User        manager =
            userServices.getUserByIdentifier(PropertyUtil.getProperty(ServiceConstants.USER_NAME.getName()),
                requestingUser, new HashSet(objTransformer.getAllAttributesMap().values()));

        user.setManagerKey(manager.getEntityId());
        logger.error(user.toString());

        String response = requestServices.requestNewUser(request.getJustification(), user,
                              account.getOrganizationName(), requestingUser);
        HttpHeaders headers = new HttpHeaders();

        headers.add("Location",
                    ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/v1/request/"
                        + response).build().toUriString());

        return new ResponseEntity<byte[]>(null, headers, HttpStatus.ACCEPTED);
    }

    private List getUserChallenges(String userName) throws UserException {
        return userServices.getUserChallenges(userName);
    }

    private String updateUser(UserAccount account, String identifier, String keyField) throws UserException {
        User   inputUser  = objTransformer.convertUserAccount(account);
        String returnCode = userServices.modifyUser(inputUser, keyField, identifier);
        return returnCode;
    }

    /**
     * Default response handler for UserNotFOundExceptions,
     * returns a 404 response.
     *
     *
     * @return
     */
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(value = { UserNotFoundException.class })
    protected @ResponseBody
    ServiceError handleNotFoundException() {
        return new ServiceError("Not Found", "");
    }

    /**
     * Method description
     *
     *
     * @param e
     *
     * @return
     */
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(value = { UserException.class })
    protected @ResponseBody
    ServiceError handleUserException(Exception e) {
        return new ServiceError(e.getMessage(), "");
    }
    
    /**
     * Method description
     *
     *
     * @param e
     *
     * @return
     */
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @ExceptionHandler(value = { QuestionsNotFoundException.class })
    protected @ResponseBody
    ServiceError handleUserQuestionException(Exception e) {
        return new ServiceError(e.getMessage(), "");
    }


    /**
     * Method description
     *
     *
     * @param e
     *
     * @return
     */
    @ExceptionHandler(value = { NoSuchServiceException.class })
    @ResponseStatus(HttpStatus.SERVICE_UNAVAILABLE)
    protected @ResponseBody
    ServiceError handleNoServiceException(Exception e) {
        ServiceError re = new ServiceError(e.getMessage(), "");

        return re;
    }
    
    /**
     * Performs validation on password prior to password change.  Requires user login and
     * new password.
     *
     *
     * @param userName
     * @param uSS
     *
     * @return 200-OK on success.  400-Bad Request on failure.
     */
    @RequestMapping(
        value  = "user/{userName}/validatepasswordwithtoken",
        method = RequestMethod.PUT
    )
    public @ResponseBody
    ResponseEntity<byte[]> changePasswordWithToken(@PathVariable String userName, @RequestBody UserSelfService uSS) {
        logger.debug("userName " + userName + " UserSelfService " + uSS);

        String passwordResult = userServices.changePasswordWithToken(userName, uSS.getNewPassword());
        logger.debug("passwordResult " + passwordResult);

        if ("Valid".equals(passwordResult)) {
            return new ResponseEntity<byte[]>(HttpStatus.OK);
        } else {
            return new ResponseEntity<byte[]>(passwordResult.getBytes(), HttpStatus.BAD_REQUEST);
        }
    }



    
        /**
     * Performs validation on password prior to password change.  Requires user login and
     * new password.
     *
     *
     * @param userName
     * @param uSS
     *
     * @return 200-OK on success.  400-Bad Request on failure.
     */
    @RequestMapping(
        value  = "user/{userName}/validatetoken",
        method = RequestMethod.PUT
    )
    public @ResponseBody
    ResponseEntity<byte[]> validateToken(@PathVariable String userName, @RequestBody UserSelfService uSS) {
        logger.debug("userName " + userName + " UserSelfService " + uSS);

        String passwordResult = userServices.validateToken(userName, uSS.getPasswordToken());
        logger.debug("passwordResult " + passwordResult);

        if ("Valid".equals(passwordResult)) {
            return new ResponseEntity<byte[]>(HttpStatus.OK);
        } else {
            return new ResponseEntity<byte[]>(passwordResult.getBytes(), HttpStatus.BAD_REQUEST);
        }
    }

    
    

    @RequestMapping(
        value  = "user/{userName}/sendEMailForPasswordChange",
        method = RequestMethod.PUT
    )
    public @ResponseBody
    ResponseEntity<byte[]> sendEMailForPasswordChange(@PathVariable String userName, @RequestBody String purpose) {
        logger.debug("userName " + userName + " Purpose " + purpose);

        String sent = userServices.sendEMailForPasswordChange(userName, purpose);
        logger.debug("sent " + sent);

        if ("Valid".equals(sent)) {
            return new ResponseEntity<byte[]>(HttpStatus.OK);
        } else {
            return new ResponseEntity<byte[]>(sent.getBytes(), HttpStatus.BAD_REQUEST);
        }
    }
    
        @RequestMapping(
        value  = "user/{userName}/sendemailusingtemplate",
        method = RequestMethod.PUT
    )
    public @ResponseBody
    ResponseEntity<byte[]> sendEmailUsingTemplate(@PathVariable String userName, @RequestBody String templateName) {
        logger.debug("userName " + userName);

        String sent = userServices.sendEmailToUser(userName, templateName);
        logger.debug("sent " + sent);

        if ("Valid".equals(sent)) {
            return new ResponseEntity<byte[]>(HttpStatus.OK);
        } else {
            return new ResponseEntity<byte[]>(sent.getBytes(), HttpStatus.BAD_REQUEST);
        }
    }
    
    
        /**
     * Performs validation on password prior to password change.  Requires user login and
     * new password.
     *
     *
     * @param userName
     * @param uSS
     *
     * @return 200-OK on success.  400-Bad Request on failure.
     */
    @RequestMapping(
        value  = "user/{userName}/validateemailchangetoken",
        method = RequestMethod.PUT
    )
    public @ResponseBody
    ResponseEntity<byte[]> validatEmailChangeToken(@PathVariable String userName, @RequestBody UserSelfService uSS) {
        logger.debug("userName " + userName + " UserSelfService " + uSS);

        String passwordResult = userServices.validatEmailChangeToken(userName, uSS.getPasswordToken());
        logger.debug("passwordResult " + passwordResult);

        if ("Valid".equals(passwordResult)) {
            return new ResponseEntity<byte[]>(HttpStatus.OK);
        } else {
            return new ResponseEntity<byte[]>(passwordResult.getBytes(), HttpStatus.BAD_REQUEST);
        }
    }
    

    @RequestMapping(
        value  = "user/{userName}/validateautentication",
        method = RequestMethod.PUT
    )
    public @ResponseBody
    ResponseEntity<byte[]> validatUserAuthentication(@PathVariable String userName, @RequestBody UserSelfService uSS) {
        logger.debug("userName " + userName + " UserSelfService " + uSS);

        String passwordResult = userServices.checkUserAuthentication(userName, uSS.getCurrentPassword());
        logger.debug("passwordResult " + passwordResult);

        if ("AUTHENTICATED".equals(passwordResult)) {
            return new ResponseEntity<byte[]>(HttpStatus.OK);
        } else {
            return new ResponseEntity<byte[]>(passwordResult.getBytes(), HttpStatus.BAD_REQUEST);
        }
    }
    
    
}
