package com.aptecllc.oim.util.v2;

//~--- non-JDK imports --------------------------------------------------------

import Thor.API.Operations.tcUserOperationsIntf;

import com.aptecllc.oim.property.ServiceProperty;

import oracle.iam.identity.orgmgmt.api.OrganizationManager;
import oracle.iam.identity.usermgmt.api.UserManager;
import oracle.iam.passwordmgmt.api.ChallengeResponseService;
import oracle.iam.passwordmgmt.api.PasswordMgmtService;
import oracle.iam.platform.OIMClient;
import oracle.iam.request.api.RequestService;
import oracle.iam.selfservice.self.selfmgmt.api.AuthenticatedSelfService;
import oracle.iam.selfservice.uself.uselfmgmt.api.UnauthenticatedSelfService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class description
 *
 *
 * @version        2.0, 20.Jan 2015
 * @author        Eric Fisher
 */
public class OimClient {
    private static final Logger IdentityProfileServiceLogger = LoggerFactory.getLogger(OimClient.class);
    private ServiceProperty     idlServiceProperty;
    private OIMClient           identityClient;

    /**
     * Constructs ...
     *
     */
    public OimClient() {}

    /**
     * Constructs ...
     *
     *
     * @param pSP
     */
    public OimClient(ServiceProperty pSP) {
        idlServiceProperty = pSP;
        identityClient     = new OIMClient(pSP.getOimEnvMap());
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public AuthenticatedSelfService getAuthSelfSvc() {
        return identityClient.getService(AuthenticatedSelfService.class);
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public OrganizationManager getOrgManager() {
        return identityClient.getService(OrganizationManager.class);
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public PasswordMgmtService getPwdMgr() {
        return identityClient.getService(PasswordMgmtService.class);
    }
    
    public ChallengeResponseService getChallenegRespService(){
        return identityClient.getService(ChallengeResponseService.class);
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public RequestService getRequestService() {
        return identityClient.getService(RequestService.class);
    }

    /**
     * Method description
     *
     *
     * @param serviceClass
     * @param <T>
     *
     * @return
     */
    public <T> T getService(Class<T> serviceClass) {
        return identityClient.getService(serviceClass);
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public tcUserOperationsIntf getTcUsrOpInt() {
        return identityClient.getService(tcUserOperationsIntf.class);
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public UnauthenticatedSelfService getUnAuthSelfSvc() {
        return identityClient.getService(UnauthenticatedSelfService.class);
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public UserManager getUsrMgr() {
        return identityClient.getService(UserManager.class);
    }
}
