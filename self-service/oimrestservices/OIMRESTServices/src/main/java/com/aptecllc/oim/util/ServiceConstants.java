package com.aptecllc.oim.util;


	public enum ServiceConstants {

		
		//Service name
		SERVICE_NAME("IdentityDataLayerService"),
		SERVICE_VERSION("v1"),
		
		//security Service constants
		APIKEY("idl.service.apikey."),
		NOT_AUTHORIZED("Not Authorized to perform this operation on the API"),
		APIKEY_CONSTANT("APIKEY"),

		//Generic Constants
		EMPTY_STRING(""),
		COMMA(","),
		
		//Identity Management Service Operations
		ADD_IDENTITY("addIdentity"),
		MODIFY_IDENTITY("modifyIdentity"),
		RETRIEVE_IDENTITY("retrieveIdentity"),
		SEARCH_IDENTITY("searchIdentity"),

		//Request Types
		ACCOUNT_INFO("ACCOUNT_INFO"),
		
		NEW_PROVISIONED_USER("New User through IDL Service"),

		IDL_SERVICE_EXCEPTION("IDL Service Exception"),

		//Severity Codes
		ERROR("Error"),
		FATAL("Fatal"),

		GENERIC_ERROR_MESSAGE("Unable to Process the Request"),
		PWD_MATCH_ERROR("idl.validation.pwdMatch"),
		
		//Regular expression for date format (YYYY-MM-DD)
		XML_DATE_FROMAT("([0-9][0-9][0-9][0-9])-(0[1-9]|1[0-2])-(0[1-9]|[12][0-9]|3[01])"),

		YES("Y"),
		NO("N"),
		STATUS_SUCCESS("COMPLETED"),

		MALE("M"),
		FEMALE("F"),
		
		//Property Configure bean name
		DEFAULT_PROPERTIES_BEAN_NAME("propertyConfigurer"),

		//Error Validation Constant
		VAL_ERROR_CONSTANT("idl.validation."),
		
		//Configuration Attributes
		USER_ORGANIZATIONNAME("service.useraccount.attribute.organizationname"),
		USER_NAME("service.useraccount.attribute.userName"),
		USER_ACCOUNTID("service.useraccount.attribute.accountId"),
		USER_ROLE("service.useraccount.attribute.role"),
		USER_FIRSTNAME("service.useraccount.attribute.firstName"),
		USER_LASTNAME("service.useraccount.attribute.lastName"),
		USER_MIDDLEINITIAL("service.useraccount.attribute.middleInitial"),
		USER_PREF_FIRSTNAME("service.useraccount.attribute.prefFirstName"),
		USER_TITLE("service.useraccount.attribute.title"),
		USER_EMAIL("service.useraccount.attribute.emailAddress"),
		USER_PHONE("service.useraccount.attribute.phoneNumber"),
		USER_DEPTNUM("service.useraccount.attribute.departmentNumber"),
		USER_OFFICELOC("service.useraccount.attribute.officeLocation"),
		USER_MANAGER("service.useraccount.attribute.manager"),
		USER_CLAIMEXPDATE("service.useraccount.attribute.acctClaimExpDate"),
		USER_CLAIMSTATUS("service.useraccount.attribute.acctClaimed"),
		USER_STATUS("service.useraccount.attribute.status"),
		USER_LOCKED("service.useraccount.attribute.locked"),
		USER_MANUALLOCK("service.useraccount.attribute.manualLock"),
		
		
		
	

		//OIM Attributes
		OIM_CONTEXT_FACTORY("idl.service.oimCf"),
		OIM_PROVIDER_URL("idl.service.oimProviderUrl"),
		
		//MC Employee and Student Constants
		MC_JOB_TYPE_PRIMARY("P"),
		MC_JOB_STATUS_ACTIVE("A"),
		MC_JOB_STATUS_TERMINATED("T"),
		MC_COURSE_DISCONTINUE_IND("D"),
		MC_COURSE_CANCEL_IND("C"),
		
		IS_USE_OIM("idl.service.is_use_oim"),
		IS_DEV_MODE("idl.service.is_dev_mode");
		
		 private String id;
	    private String name;

	    private ServiceConstants(String id) {
	      this.id = id;
	      this.name = id;
	    }

	    private  ServiceConstants(String id, String name) {
	      this.id = id;
	      this.name = name;
	    }

	    public String getId() {
	      return this.id;
	    }

	    public String getName() {
	      return this.name;
	    }
	}


