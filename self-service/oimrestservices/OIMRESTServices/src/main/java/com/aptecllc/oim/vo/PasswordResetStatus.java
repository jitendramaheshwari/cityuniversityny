/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aptecllc.oim.vo;

import java.io.Serializable;

/**
 *
 * @author fforester
 */
public class PasswordResetStatus implements Serializable{
    
    
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final String NAME_FAILURE="NAME-FAILURE";
    public static final String PASSWORD_FAILURE="PASSWORD-FAILURE";
    public static final String PASSWORD_SUCCESS="PASSWORD-SUCCESS";
    
    private Long auditRecordId;
    private String message="";
    private String passwordResetStatus;
    private String nameVerificationStatus;

    public Long getAuditRecordId() {
        return auditRecordId;
    }

    public void setAuditRecordId(Long auditRecordId) {
        this.auditRecordId = auditRecordId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPasswordResetStatus() {
        return passwordResetStatus;
    }

    public void setPasswordResetStatus(String passwordResetStatus) {
        this.passwordResetStatus = passwordResetStatus;
    }

    public String getNameVerificationStatus() {
        return nameVerificationStatus;
    }

    public void setNameVerificationStatus(String nameVerificationStatus) {
        this.nameVerificationStatus = nameVerificationStatus;
    }
    
    
}
