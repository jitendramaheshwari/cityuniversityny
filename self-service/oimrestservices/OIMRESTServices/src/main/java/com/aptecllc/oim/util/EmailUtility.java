/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aptecllc.oim.util;

import java.util.HashMap;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import Thor.API.tcResultSet;
import Thor.API.Operations.tcITResourceInstanceOperationsIntf;
import Thor.API.Operations.tcLookupOperationsIntf;
import com.aptecllc.oim.util.v2.OimClient;
import com.aptecllc.oim.vo.Notification;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import oracle.iam.notification.api.NotificationService;
import oracle.iam.notification.exception.MultipleTemplateException;
import oracle.iam.notification.exception.NotificationManagementException;
import oracle.iam.notification.exception.TemplateNotFoundException;
import oracle.iam.notification.vo.LocalTemplate;
import oracle.iam.notification.vo.NotificationEvent;
import oracle.iam.notification.vo.NotificationTemplate;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import weblogic.apache.org.apache.velocity.Template;
import weblogic.apache.org.apache.velocity.VelocityContext;
import weblogic.apache.org.apache.velocity.runtime.RuntimeServices;
import weblogic.apache.org.apache.velocity.runtime.RuntimeSingleton;
import weblogic.apache.org.apache.velocity.runtime.parser.node.SimpleNode;

/**
 *
 * @author jitendra.maheshwari
 */
public class EmailUtility {

    private static Logger logger = LoggerFactory.getLogger(OimClient.class);
    private static String CLASS_NAME = "EmailUtility";
    private static String mailUserLogin = null;
    private static String mailUserPassword = null;
    private static String mailSMTPHost = null;
    private static Session mailSession = null;

    /**
     * It returns parameter value from lookup
     *
     * @param strInputParameter the parameter name
     * @param lookupName the lookup name in which to lookup
     * @return parameter value
     */
    private static String getRequiredParametersFromLookup(String strInputParameter, String lookupName, OimClient oimClient) {
        String METHOD_NAME = "getRequiredParametersFromLookup";
        logger.info(CLASS_NAME + METHOD_NAME + " Entering with InputParameter: " + strInputParameter);
        String strLookupValue = "";
        try {
            tcLookupOperationsIntf lookUpIntf = oimClient.getService(tcLookupOperationsIntf.class);
            strLookupValue
                    = lookUpIntf.getDecodedValueForEncodedValue(lookupName, strInputParameter);
        } catch (Exception e) {
            logger.error(CLASS_NAME + METHOD_NAME + "Exception", e);
        }
        logger.info(CLASS_NAME + METHOD_NAME + " Exiting with LookupValue: " + strLookupValue);
        return strLookupValue;
    }

    /**
     * It returns email address for sending email.
     *
     * @return
     */
    private static String returnUserLoginEmailClient(OimClient oimClient) {
        String METHOD_NAME = " returnUserLoginEmailClient ";
        logger.info(CLASS_NAME + METHOD_NAME + "Entering.");

        if (mailUserLogin == null) {
            try {
                tcITResourceInstanceOperationsIntf itResOper
                        = oimClient.getService(tcITResourceInstanceOperationsIntf.class);
                HashMap<String, String> searchcriteria = new HashMap<String, String>();
                searchcriteria.put("IT Resources.Name", "Email Server");//Email Server
                tcResultSet resultSet = itResOper.findITResourceInstances(searchcriteria);
                Long itResKey = resultSet.getLongValue("IT Resources.Key");
                tcResultSet itResSet = itResOper.getITResourceInstanceParameters(itResKey);
                for (int i = 0; i < itResSet.getRowCount(); i++) {
                    itResSet.goToRow(i);
                    String paramName = "";
                    paramName = itResSet.getStringValue("IT Resources Type Parameter.Name");
                    if ("User Login".equalsIgnoreCase(paramName)) {
                        mailUserLogin = itResSet.getStringValue("IT Resources Type Parameter Value.Value");
                        logger.info(CLASS_NAME + METHOD_NAME + "mailUserLogin: " + mailUserLogin);

                    }
                }
            } catch (Exception e) {
                logger.error(CLASS_NAME + METHOD_NAME + " Exception ", e);
            }
        }
        return mailUserLogin;
    }

    /**
     * It returns the password for the email id.
     *
     * @return
     */
    private static String returnUserPasswordEmailClient(OimClient oimClient) {
        String METHOD_NAME = " returnUserPasswordEmailClient ";
        logger.info(CLASS_NAME + METHOD_NAME + "Entering.");
        mailUserPassword = "testpassword";
        if (mailUserPassword == null) {
            try {
                tcITResourceInstanceOperationsIntf itResOper
                        = oimClient.getService(tcITResourceInstanceOperationsIntf.class);
                HashMap<String, String> searchcriteria = new HashMap<String, String>();
                searchcriteria.put("IT Resources.Name", "Email Server");//Email Server
                tcResultSet resultSet = itResOper.findITResourceInstances(searchcriteria);
                Long itResKey = resultSet.getLongValue("IT Resources.Key");
                tcResultSet itResSet = itResOper.getITResourceInstanceParameters(itResKey);
                for (int i = 0; i < itResSet.getRowCount(); i++) {
                    itResSet.goToRow(i);
                    String paramName = "";
                    paramName = itResSet.getStringValue("IT Resources Type Parameter.Name");
                    if ("User Password".equalsIgnoreCase(paramName)) {
                        mailUserPassword = itResSet.getStringValue("IT Resources Type Parameter Value.Value");
                    }
                }
            } catch (Exception e) {
                logger.error(CLASS_NAME + METHOD_NAME + " Exception ", e);
            }
        }
        return mailUserPassword;
    }

    /**
     * It returns the SMTP server name.
     *
     * @return
     */
    private static String returnUserSMTPSERVEREmailClient(OimClient oimClient) {
        String METHOD_NAME = " returnUserSMTPSERVEREmailClient ";
        logger.info(CLASS_NAME + METHOD_NAME + "Entering.");
        if (mailSMTPHost == null) {
            try {
                tcITResourceInstanceOperationsIntf itResOper
                        = oimClient.getService(tcITResourceInstanceOperationsIntf.class);
                HashMap<String, String> searchcriteria = new HashMap<String, String>();
                searchcriteria.put("IT Resources.Name", "Email Server");//Email Server
                tcResultSet resultSet = itResOper.findITResourceInstances(searchcriteria);
                Long itResKey = resultSet.getLongValue("IT Resources.Key");
                tcResultSet itResSet = itResOper.getITResourceInstanceParameters(itResKey);
                for (int i = 0; i < itResSet.getRowCount(); i++) {
                    itResSet.goToRow(i);
                    String paramName = "";
                    paramName = itResSet.getStringValue("IT Resources Type Parameter.Name");
                    if ("Server Name".equalsIgnoreCase(paramName)) {
                        mailSMTPHost = itResSet.getStringValue("IT Resources Type Parameter Value.Value");
                        logger.info(CLASS_NAME + METHOD_NAME + "mailSMTPHost: " + mailSMTPHost);

                    }
                }
            } catch (Exception e) {
                logger.error(CLASS_NAME + METHOD_NAME + " Exception ", e);
            }
        }
        return mailSMTPHost;
    }

    private static Session getEmailSession(final OimClient oimClient) {
        if (mailSession != null) {
            return mailSession;
        }
        Properties props = System.getProperties();
        props.setProperty("mail.smtps.host", returnUserSMTPSERVEREmailClient(oimClient));
        props.setProperty("mail.smtp.port", "25");
        props.put("mail.smtp.auth", "false");

  //      props.put("mail.smtp.ssl.enable", "true");
    //    props.put("mail.smtp.starttls.enable", "true");
      //  props.put("mail.debug", "true");
        //props.put("mail.smtp.EnableSSL.enable", "true");
        Session session = Session.getDefaultInstance(props);  
/*
        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
            protected javax.mail.PasswordAuthentication getPasswordAuthentication() {

                return new javax.mail.PasswordAuthentication(returnUserLoginEmailClient(oimClient),
                        returnUserPasswordEmailClient(oimClient));
            }
        });
*/
        return session;
    }

    public static void sendEmailMessage(OimClient oimClient, String templateName, Map<String, String> inputs) { // customerLogin = colelm, 

        String METHOD_NAME = " sendEmailMessage ";
        logger.info(CLASS_NAME + METHOD_NAME + "Entering.");
        logger.info(CLASS_NAME + METHOD_NAME + " templateName " + templateName);
        
        NotificationService notificationService = oimClient.getService(NotificationService.class);
        try {

            
            NotificationEvent event = new NotificationEvent();
        event.setTemplateName(templateName); // Template
        
        event.setSender("XELSYSADM");

        String[] users = new String[1];
        users[0] = inputs.get("userLogin");
        event.setUserIds(users);

        HashMap<String, Object> notificationData = new HashMap<String, Object>();
        notificationData.putAll(inputs);
        event.setParams(notificationData);
        notificationService.notify(event);
        logger.info(CLASS_NAME + METHOD_NAME +
                        " Notification sent event: " + event);
        } catch (Exception ex) {
            logger.error("Error", ex);
        }
    }


    public static void sendEmailMessage(OimClient oimClient, String templateName, Map<String, String> inputs, String receipientAddress) { // customerLogin = colelm, 

        String METHOD_NAME = " sendEmailMessage ";
        logger.info(CLASS_NAME + METHOD_NAME + "Entering.");
        logger.info(CLASS_NAME + METHOD_NAME + " templateName " + templateName);
        logger.info(CLASS_NAME + METHOD_NAME + " receipientAddress " + receipientAddress);

        NotificationService notificationService = oimClient.getService(NotificationService.class);
        NotificationTemplate notificationTemplate;
        try {
            notificationTemplate = notificationService.lookupTemplate(templateName, null);
            Map<String, LocalTemplate> collection = notificationTemplate.getLocaltemplateCollection();
            logger.info(CLASS_NAME + METHOD_NAME + " collection " + collection);

            LocalTemplate localTemplate = (LocalTemplate) notificationTemplate.getLocaltemplateCollection().get(Locale.US.toString());
            String subjectTemplate = localTemplate.getSubject();
            String longMessageTemplate = localTemplate.getLongmessage();
            String subjectText = convertString(subjectTemplate, inputs);
            String messageText = convertString(longMessageTemplate, inputs);
                        logger.info(CLASS_NAME + METHOD_NAME + " subjectText " + subjectText);
                                                logger.info(CLASS_NAME + METHOD_NAME + " messageText " + messageText);



            Message message = new MimeMessage(getEmailSession(oimClient));
            message.setFrom(new InternetAddress(returnUserLoginEmailClient(oimClient)));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(receipientAddress));
            message.setSubject(subjectText);
            message.setContent(messageText, "text/html; charset=utf-8");
            javax.mail.Transport.send(message);
            logger.info(CLASS_NAME + METHOD_NAME + " email sent.");
        } catch (Exception ex) {
            logger.error("Error", ex);
        }
    }

    private static String convertString(String messageTemplate, Map<String, String> input) {
        logger.info("Entering with messageTemplate " + messageTemplate + " input " + input);
        String myOutput = messageTemplate;
        for (Map.Entry<String, String> entry : input.entrySet()) {
            myOutput = messageTemplate.replace("$"+entry.getKey(), entry.getValue());
            messageTemplate = myOutput;
        }
        logger.info("Exiting with messageTemplate " + myOutput);
        return myOutput;
    }
    
    private static String convertStringVelocity(String messageTemplate, Map<String, String> input) {
        logger.info("Entering with messageTemplate " + messageTemplate + " input " + input);

        SimpleNode sn = null;
        StringWriter sw = null;
        String ret = messageTemplate;
        try {
            RuntimeServices rs = RuntimeSingleton.getRuntimeServices();
            StringReader sr = new StringReader(messageTemplate);
            sn = rs.parse(sr, "User Information");
            Template t = new Template();
            t.setRuntimeServices(rs);
            t.setData(sn);
            t.initDocument();
            VelocityContext vc = new VelocityContext();
            for (Map.Entry<String, String> entry : input.entrySet()) {
                vc.put(entry.getKey(), entry.getValue());
            }
            sw = new StringWriter();
            t.merge(vc, sw);
            ret = sw.toString();
            logger.info("Exiting with messageTemplate " + ret);
        } catch (Exception ex) {
            logger.error("Error", ex);
        } finally {
            if (sw != null) {
                sw.flush();
                try {
                    sw.close();
                } catch (IOException ex) {
                    logger.error("Error", ex);
                }
            }
        }
        return ret;
    }

}
