package com.aptecllc.oim.vo;

//~--- JDK imports ------------------------------------------------------------

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Class description
 *
 *
 * @version        1.0, 14/05/05
 * @author         Eric A. Fisher, APTEC LLC    
 */
public class Notification implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String            recipient;
    private String            resetCode;
    private String            userName;

    /**
     * @return the recipient
     */
    public String getRecipient() {
        return recipient;
    }

    /**
     * @param recipient the recipient to set
     */
    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public String getResetCode() {
        return resetCode;
    }

    public void setResetCode(String resetCode) {
        this.resetCode = resetCode;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
   
}
