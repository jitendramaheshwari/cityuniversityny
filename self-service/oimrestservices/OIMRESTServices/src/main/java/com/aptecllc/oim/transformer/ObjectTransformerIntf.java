package com.aptecllc.oim.transformer;

import java.util.HashMap;

import com.aptecllc.oim.vo.UserAccount;

import oracle.iam.identity.usermgmt.vo.User;
import oracle.iam.platform.entitymgr.vo.SearchCriteria;

public interface ObjectTransformerIntf {
	public User convertUserAccount(UserAccount pUserAccount);
	public UserAccount convertUser(User pUser);
	public HashMap getAllAttributesMap();
	public SearchCriteria parseRSQL(String searchExpression);
}
