package com.aptecllc.oim.vo;

//~--- JDK imports ------------------------------------------------------------

import java.io.Serializable;

/**
 * Service Error value object for returning error details
 * to the calling REST service.
 *
 *
 * @version        1.0, 14/02/05
 * @author         Eric A. Fisher, APTEC LLC    
 */
public class ServiceError implements Serializable {

    public ServiceError(String errorCode, String errorDetails) {
		super();
		this.errorCode = errorCode;
		this.errorDetails = errorDetails;
	}

	/**
     *
     */
    private static final long serialVersionUID = 1L;
    private String            errorCode;
    private String            errorDetails;

    /**
     * ErrorCode Getter
     *
     *
     * @return ErrorCode
     */
    public String getErrorCode() {
        return errorCode;
    }

    /**
     * ErrorCode Setter
     *
     *
     * @param errorCode Sets the error code string value.
     */
    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    /**
     * ErrorDetails Getter
     *
     *
     * @return ErrorDetailed message for troubleshooting or other uses.
     */
    public String getErrorDetails() {
        return errorDetails;
    }

    /**
     * ErrorDetails Setter
     *
     *
     * @param errorDetails Sets the error detail string value.
     */
    public void setErrorDetails(String errorDetails) {
        this.errorDetails = errorDetails;
    }
}
