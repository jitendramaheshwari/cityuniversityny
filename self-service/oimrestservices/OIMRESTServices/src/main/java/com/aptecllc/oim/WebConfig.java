package com.aptecllc.oim;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

@Configuration
@EnableWebMvc

public class WebConfig extends WebMvcConfigurationSupport{
 @Bean
 public RequestMappingHandlerMapping requestMappingHandlerMapping() {
  RequestMappingHandlerMapping handlerMapping = super.requestMappingHandlerMapping();
  handlerMapping.setUseSuffixPatternMatch(false);
 
  return handlerMapping;
 } 
}
