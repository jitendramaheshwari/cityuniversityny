
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package com.aptecllc.oim.controller;

//~--- non-JDK imports --------------------------------------------------------

import com.aptecllc.oim.exception.PropertiesException;
import com.aptecllc.oim.services.OIMProperties;




import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

//~--- JDK imports ------------------------------------------------------------



import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author foresfr
 */
@Controller
@RequestMapping("oimproperties/v1")
public class OIMPropertiesController {
    private static final Logger logger = LoggerFactory.getLogger(OIMPropertiesController.class);
    @Autowired
    private OIMProperties    oimProperties;

    /**
     * Method description
     *
     *
     * @param itResourceName
     *
     * @return
     *
     * @throws PropertiesException
     */
    @RequestMapping(
        value  = "name/{itResourceName}/itresource",
        method = RequestMethod.GET
    )
    public @ResponseBody
    Map<String, String> getITResourceProperties(@PathVariable String itResourceName) throws PropertiesException {
        try {
            Map<String, String> resProps = oimProperties.getITResourceProperties(itResourceName);

            return resProps;
        } catch (Exception e) {
            throw new PropertiesException(e);
        }
    }

    /**
     * Method description
     *
     *
     * @param lookupName
     *
     * @return
     *
     * @throws PropertiesException
     */
    @RequestMapping(
        value  = "name/{lookupName}/multimap",
        method = RequestMethod.GET
    )
    public @ResponseBody
    TreeMap<String, List<String>> getLookupMultiMapProperties(@PathVariable String lookupName)
            throws PropertiesException {
        try {
            TreeMap<String, List<String>> lookup = oimProperties.getLookupMultiMapProperties(lookupName);

            return lookup;
        } catch (Exception e) {
            throw new PropertiesException(e);
        }
    }

    /**
     * Method description
     *
     *
     * @param lookupName
     *
     * @return
     *
     * @throws PropertiesException
     */
    @RequestMapping(
        value  = "name/{lookupName}/lookup",
        method = RequestMethod.GET
    )
    public @ResponseBody
    Map<String, String> getLookupProperties(@PathVariable String lookupName) throws PropertiesException {
        try {
            Map<String, String> lookup = oimProperties.getLookupProperties(lookupName);

            return lookup;
        } catch (Exception e) {
            throw new PropertiesException(e);
        }
    }

    /**
     * Method description
     *
     *
     * @param propertyName
     *
     * @return
     *
     * @throws PropertiesException
     */
    @RequestMapping(
        value  = "name/{propertyName}/systemproperties",
        method = RequestMethod.GET
    )
    public @ResponseBody
    String getSystemProperty(@PathVariable String propertyName) throws PropertiesException {
        try {
            String prop = oimProperties.getSystemProperty(propertyName);

            return prop;
        } catch (Exception e) {
            throw new PropertiesException(e);
        }
    }

    /**
     * Method description
     *
     *
     * @param taskName
     *
     * @return
     *
     * @throws PropertiesException
     */
    @RequestMapping(
        value  = "name/{taskName}/taskproperties",
        method = RequestMethod.GET
    )
    public @ResponseBody
    Map<String, String> getTaskProperties(@PathVariable String taskName) throws PropertiesException {
        try {
            Map<String, String> props = oimProperties.getTaskProperties(taskName);

            return props;
        } catch (Exception e) {
            throw new PropertiesException(e);
        }
    }
}
