package com.aptecllc.oim.property;


import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;



/**
 * This class is responsible for holding all the properties defined
 * for the IDL service during the Service start up
 * 
 * @author skandagadla
 *
 */
public class PropertyUtil extends PropertyPlaceholderConfigurer {
	private static final Logger logger = LoggerFactory.getLogger(PropertyUtil.class); 
	private static Map<String, String> appProps;

	@Override
	protected void processProperties(ConfigurableListableBeanFactory beanFactoryToProcess,
	      Properties props) throws BeansException {
		logger.debug("Inside the Process Properties");
		super.processProperties(beanFactoryToProcess, props);
	      appProps = new HashMap<String, String>();
	      for (Object key : props.keySet()) {
	          String keyStr = key.toString();
	          appProps.put(keyStr, parseStringValue(props.getProperty(keyStr), props,
	                  new HashSet()));
	      }
	      logger.debug("Successfully obtained the Properties");
	      
	}

	/**
	 * The getter to get the properties captured during start up
	 * 
	 * @param keyName	The Name of the property
	 * @return	The value of the property keyName
	 */
	public static String getProperty(String keyName)
	{
		if(null != keyName)
			return appProps.get(keyName);
		else 
			return null;
	}

	public static Map<String, String> getAppProps() {
		return appProps;
	}
	
	
}
