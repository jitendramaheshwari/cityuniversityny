/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aptecllc.oim.services;

import Thor.API.Exceptions.tcAPIException;
import Thor.API.Exceptions.tcColumnNotFoundException;
import Thor.API.Exceptions.tcITResourceNotFoundException;
import Thor.API.Exceptions.tcInvalidLookupException;
import Thor.API.Operations.tcITResourceInstanceOperationsIntf;
import Thor.API.Operations.tcLookupOperationsIntf;
import Thor.API.tcResultSet;

import com.aptecllc.oim.property.ServiceProperty;
import com.aptecllc.oim.util.v2.OimClient;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//import com.thortech.util.logging.Logger;
import java.util.Set;
import java.util.TreeMap;

import oracle.iam.conf.api.SystemConfigurationService;
import oracle.iam.conf.exception.SystemConfigurationServiceException;
import oracle.iam.conf.vo.SystemProperty;
import oracle.iam.platform.OIMClient;
import oracle.iam.platform.Platform;
import oracle.iam.scheduler.api.SchedulerService;
import oracle.iam.scheduler.exception.SchedulerException;
import oracle.iam.scheduler.vo.JobDetails;
import oracle.iam.scheduler.vo.JobParameter;

import org.apache.commons.lang3.StringUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class OIMProperties  {
    
    @Autowired
    private ServiceProperty     serviceProperties;

    private final static Logger logger = LoggerFactory.getLogger(OIMProperties.class.getName());
   
   
   
    
    
    private OimClient oimLogin() throws Exception
    {
    	OimClient oimClient;
        oimClient = new OimClient(serviceProperties);
       // oimClient.authenticate();
        return oimClient;
    }

     /**
     * Returns a map of the named ITResource Properties
     *
     * @param itResourceName
     * @exception Exception
     */
    public Map<String, String>  getITResourceProperties(String itResourceName) throws Exception {
    	tcITResourceInstanceOperationsIntf resInstOps = null;
       
        HashMap<String, String> resourceMap = new HashMap<String, String>();
        try {
        	OimClient oimClient= oimLogin();
            resInstOps = oimClient.getService(tcITResourceInstanceOperationsIntf.class);
            HashMap<String, String> srchMap = new HashMap<String, String>();
            srchMap.put("IT Resources.Name", itResourceName);
            tcResultSet rst = resInstOps.findITResourceInstances(srchMap);

            if (rst.getRowCount() <= 0)
            {
                logger.error("ITResource " + itResourceName + " not found");
                throw new Exception("ITResource " + itResourceName + " Not Found");
            }

            for (int i = 0; i < rst.getTotalRowCount(); i++) {
                rst.goToRow(i);
                long key = rst.getLongValue("IT Resources.Key");
                tcResultSet paramSet = resInstOps.getITResourceInstanceParameters(key);
                int amRow = paramSet.getRowCount();
                if (amRow == 0) {
                    logger.error("ITResource " + itResourceName + " not found");
                    throw new Exception("ITResource " + itResourceName + " Not Found");
                }

                for (int j = 0; j < paramSet.getTotalRowCount(); j++) {
                    paramSet.goToRow(j);
                    //Setting login credentials from ITResource
                    String parmkey = paramSet.getStringValue("IT Resources Type Parameter.Name");
                    String parmval = paramSet.getStringValue("IT Resource.Parameter.Value");
                    resourceMap.put(parmkey, parmval);
                    
                }
                //logger.debug("ITResource Parms: " + srchMap);
            }
            logger.info("Finished Retrieving ITResource parameters");
        } catch (tcAPIException e) {
            logger.error("tcAPIException while retrieving ITResource parameters: " + e.getMessage(), e);
            throw new Exception(e);
        } catch (tcColumnNotFoundException e) {
            logger.error("tcColumnNotFoundException while retrieving ITResource parameters: " + e.getMessage(), e);
            throw new Exception(e);
        } catch (tcITResourceNotFoundException e) {
            logger.error("tcITResourceNotFoundException while retrieving ITResource parameters: " + e.getMessage(), e);
            throw new Exception(e);
        } finally {
        	resInstOps.close();
        }
        return resourceMap;
    }

    /**
     * Returns a map of the named Lookup table.
     *
     * @param lookupName
     * @exception Exception
     */
    public Map<String, String> getLookupProperties(String lookupName) throws Exception {
    	tcLookupOperationsIntf lookupOps= null;
        HashMap<String, String> props = new HashMap<String, String>();
        try {
        	OimClient oimClient= oimLogin();
            lookupOps = oimClient.getService(tcLookupOperationsIntf.class);
            tcResultSet resultSet = lookupOps.getLookupValues(lookupName);
            int amRow = resultSet.getRowCount();
            if (amRow == 0) {
                logger.error("Lookup Code " + lookupName + " not found");
                throw new Exception("Lookup Not Found");
            }

            for (int i = 0; i < resultSet.getRowCount(); i++) {
                resultSet.goToRow(i);
                String codeKeyfromResultSet = resultSet.getStringValue("Lookup Definition.Lookup Code Information.Code Key");
                String decodeValue = resultSet.getStringValue("Lookup Definition.Lookup Code Information.Decode");
                props.put(codeKeyfromResultSet, decodeValue);

            }
        } catch (tcAPIException e) {
            logger.error("tcAPIException ", e);
            throw new Exception(e);
        } catch (tcInvalidLookupException e) {
            logger.error("tcInvalidLookupException ", e);
            throw new Exception(e);
        } catch (tcColumnNotFoundException e) {
            logger.error("tcColumnNotFoundException ", e);
            throw new Exception(e);
        } finally {
        	lookupOps.close();
        }

        return props;

    }
    
    /**
     * allow for a lookup with multi value keys
     * @param lookupName
     * @return
     * @throws Exception 
     */
    public TreeMap<String, List<String>> getLookupMultiMapProperties(String lookupName) throws Exception {
    	tcLookupOperationsIntf lookupOps= null;
        TreeMap<String, List<String>> props = new TreeMap<String, List<String>>();
        try {
        	OimClient oimClient= oimLogin();
            lookupOps = oimClient.getService(tcLookupOperationsIntf.class);
            tcResultSet resultSet = lookupOps.getLookupValues(lookupName);
            int amRow = resultSet.getRowCount();
            if (amRow == 0) {
                logger.error("Lookup Code " + lookupName + " not found");
                throw new Exception("Lookup Not Found");
            }

            for (int i = 0; i < resultSet.getRowCount(); i++) {
                resultSet.goToRow(i);
                String codeKeyfromResultSet = resultSet.getStringValue("Lookup Definition.Lookup Code Information.Code Key");
                String decodeValue = resultSet.getStringValue("Lookup Definition.Lookup Code Information.Decode");
                List l = props.get(codeKeyfromResultSet);
                if (l == null)
                    l = new ArrayList<String>();
                l.add(decodeValue);
                props.put(codeKeyfromResultSet, l);

            }
        } catch (tcAPIException e) {
            logger.error("tcAPIException ", e);
            throw new Exception(e);
        } catch (tcInvalidLookupException e) {
            logger.error("tcInvalidLookupException ", e);
            throw new Exception(e);
        } catch (tcColumnNotFoundException e) {
            logger.error("tcColumnNotFoundException ", e);
            throw new Exception(e);
        } finally {
        	lookupOps.close();
        }

        return props;

    }

    /**
     * Returns a map of the named Scheduled Task Attributes.
     *
     * @param taskName
     * @exception Exception
     */
    public Map<String, String> getTaskProperties(String taskName) throws Exception {

        Map jobProps = new HashMap();
        SchedulerService scheduleOps = null;

        try {
        	OimClient oimClient= oimLogin();
            scheduleOps = oimClient.getService(SchedulerService.class);
            //logger.debug("Get Job " + taskName);
            JobDetails jd = scheduleOps.getJobDetail(taskName);

            if (jd == null)
            {
                logger.error("Job Not Found for " + taskName);
                throw new Exception("Job Not Found for " + taskName);
            }

            Map<String,JobParameter> parms = jd.getAttributes();

            if (parms == null)
            {
                logger.debug("No Parms for " + taskName);
                return jobProps;
            }
            Set<String> keys = parms.keySet();

            for(String key : keys)
            {
                JobParameter jp = parms.get(key);
                //logger.debug("DataType " + jp.getDataType());
                //logger.debug("Name " + jp.getName());
                //logger.debug("Val " + jp.getValue());
                jobProps.put(jp.getName(),jp.getValue().toString());

            }
        } catch (SchedulerException ex) {
            logger.error("SchedulerException",ex);
            throw new Exception("SchedulerException",ex);
        }
        return jobProps;
        
        
    }

    /**
     * Returns The String Value of the System Property
     *
     * @param propName
     * @exception Exception
     */
    public String getSystemProperty(String propName) throws Exception {
        SystemProperty sysProp = null;
        String ptyValue = null;
        SystemConfigurationService confService = null;
        
        try {
        	OimClient oimClient= oimLogin();
            confService = oimClient.getService(SystemConfigurationService.class);
            sysProp = confService.getSystemProperty(propName);
        } catch (SystemConfigurationServiceException SCSE) {
            logger.error("SystemConfigurationServiceException",SCSE);
            throw new Exception("SystemConfigurationServiceException",SCSE);
        }

        if (sysProp != null) {
            ptyValue = sysProp.getPtyValue();
        }

        if ((ptyValue != null) && (ptyValue.length() > 0)) {
            //ptyValue = "@".concat(ptyValue);
        } else {
            ptyValue = "";
        }
        
        return ptyValue;
    }

    /**
     * Returns a String Array of all Scheduled Job Names
     *
     * @exception Exception
     */
    public String[] getAllJobs() throws Exception
    {
    	SchedulerService scheduleOps=null;
        try {
        	OimClient oimClient= oimLogin();
            scheduleOps = oimClient.getService(SchedulerService.class);
            return scheduleOps.getAllJobs();
        } catch (SchedulerException ex) {
            logger.error("SchedulerException",ex);
            throw new Exception("SchedulerException",ex);
        }
    }

    /**
     * Gets a string value of an attribute.
     *
     * @param attrMap of the Attributes
     * @param Attribute The name of the task attribute to retrieve.
     * @param Default The default value to use if the attribute is not present.
     * @return The value of that attribute.
     */
    public final String getDefaultAttribute(Map<String, String> attrMap, String Attribute, String Default) {
        String value = attrMap.get(Attribute);
        String result = StringUtils.isBlank(value) ? Default : value;
        return result;
    }

    /**
     * Gets a string value of an attribute, and throws an exception if that attribute is not present.
     *
     * @param attrMap of the Attributes
     * @param Attribute The name of the task attribute to retrieve.
     * @return The value of that attribute.
     * @exception missingAttributeException
     */
    public final String getCriticalAttribute(Map<String, String> attrMap, String Attribute) throws Exception {
        String result = attrMap.get(Attribute);
        if (result == null || result.length() == 0) {
            throw new Exception(Attribute);
        }
        return result;
    }

    /**
     * Gets a boolean value based on a task attribute.
     *
     * @param attrMap of the Attributes
     * @param Attribute The name of the task attribute to retrieve.
     * @param Default A default value to return if the attribute is not present.
     * @return The value of that attribute.
     */
    public final boolean getBooleanAttribute(Map<String, String> attrMap, String Attribute, boolean Default) {
        String value = attrMap.get(Attribute);
        boolean result = StringUtils.isBlank(value) ? Default : value.equalsIgnoreCase("true");
        return result;
    }


}
