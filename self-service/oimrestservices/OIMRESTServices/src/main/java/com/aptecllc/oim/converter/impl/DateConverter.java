/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aptecllc.oim.converter.impl;

import com.aptecllc.oim.converter.DataConverterIntf;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author jitendra.maheshwari
 */
public class DateConverter implements DataConverterIntf {
        private static final Logger  logger  = LoggerFactory.getLogger(DateConverter.class);

    	@Override
	public Object convertInbound(Object inbound) {
            try {
                if(inbound instanceof Long)
                    return new Date((Long)inbound);
            } catch(Exception e) {
                logger.error("Error converting: ", e);
            }
            return inbound;
	}

	@Override
	public Object convertOutbound(Object outbound) {
            try {
              if(outbound instanceof Date)
                return ((Date)outbound).getTime();
            } catch(Exception e) {
                logger.error("Error converting: ", e);
            }
            return outbound;
	}
}
