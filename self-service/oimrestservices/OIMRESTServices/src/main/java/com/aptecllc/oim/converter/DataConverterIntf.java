package com.aptecllc.oim.converter;

public interface DataConverterIntf {
	
	public Object convertInbound(Object inbound);
	public Object convertOutbound(Object outbound);
}
