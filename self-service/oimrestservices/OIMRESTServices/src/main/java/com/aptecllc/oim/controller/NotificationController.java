package com.aptecllc.oim.controller;

//~--- non-JDK imports --------------------------------------------------------

import com.aptecllc.oim.util.v2.OimClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aptecllc.oim.vo.Notification;

/**
 * Class description
 *
 *
 * @version        1.0, 14/05/05
 * @author         Eric A. Fisher, APTEC LLC    
 */
@Controller
@RequestMapping("notify/v1")
public class NotificationController {
    private static final Logger logger = LoggerFactory.getLogger(NotificationController.class);
        
    @RequestMapping(
            value  = "sendsms",
            method = RequestMethod.POST
        )
    
    public @ResponseBody ResponseEntity<byte[]> sendSMSMessage(@RequestBody Notification notification) {
       	return new ResponseEntity<byte[]>(null, null, HttpStatus.NO_CONTENT);
    }
}
