package com.aptecllc.oim.services;

//~--- non-JDK imports --------------------------------------------------------

import com.aptecllc.oim.controller.OIMRESTController;
import com.aptecllc.oim.property.ServiceProperty;
import com.aptecllc.oim.util.v2.OimClient;

import oracle.iam.identity.exception.OrganizationManagerException;
import oracle.iam.identity.orgmgmt.api.OrganizationManager;
import oracle.iam.identity.orgmgmt.vo.Organization;
import oracle.iam.identity.usermgmt.api.UserManagerConstants;
import oracle.iam.identity.usermgmt.vo.User;
import oracle.iam.platform.authz.exception.AccessDeniedException;
import oracle.iam.platform.utils.vo.OIMType;
import oracle.iam.request.api.RequestService;
import oracle.iam.request.exception.BulkBeneficiariesAddException;
import oracle.iam.request.exception.BulkEntitiesAddException;
import oracle.iam.request.exception.InvalidRequestDataException;
import oracle.iam.request.exception.InvalidRequestException;
import oracle.iam.request.exception.RequestServiceException;
import oracle.iam.request.vo.RequestConstants;
import oracle.iam.request.vo.RequestData;
import oracle.iam.request.vo.RequestEntity;
import oracle.iam.request.vo.RequestEntityAttribute;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

//~--- JDK imports ------------------------------------------------------------

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Class description
 *
 *
 * @version        1.0, 14/04/06
 * @author         Eric A. Fisher, APTEC LLC
 */
@Service
public class OIMRequestServices {
    private static final Logger logger = LoggerFactory.getLogger(OIMRESTController.class);
    @Autowired
    private ServiceProperty     serviceProperties;

    /**
     * Method description
     *
     *
     *
     *
     * @param reason
     * @param user
     * @param orgName
     */
    public String requestNewUser(String reason, User user, String orgName, String requestingUser) {
        OimClient      oimClient = new OimClient(serviceProperties);
        RequestService reqsrvc   = oimClient.getRequestService();
        String reqID = null;

        System.out.println("IN REQUEST NEW USER");
        System.out.println(user.toString());

        RequestData requestData = new RequestData();

        requestData.setJustification(reason);

        RequestEntity ent = new RequestEntity();

        ent.setRequestEntityType(OIMType.User);
        ent.setOperation(RequestConstants.MODEL_CREATE_OPERATION);    // New in R2

        List<RequestEntityAttribute> attrs = new ArrayList<RequestEntityAttribute>();
        RequestEntityAttribute       attr  =
            new RequestEntityAttribute(UserManagerConstants.AttributeName.LASTNAME.getId(), user.getLastName(),
                                       RequestEntityAttribute.TYPE.String);

        attrs.add(attr);
        attr = new RequestEntityAttribute(UserManagerConstants.AttributeName.FIRSTNAME.getId(), user.getFirstName(),
                                          RequestEntityAttribute.TYPE.String);
        attrs.add(attr);
        attr = new RequestEntityAttribute("Organization", 24L,
                                          RequestEntityAttribute.TYPE.Long);
        attrs.add(attr);
        attr = new RequestEntityAttribute("User Type", false, RequestEntityAttribute.TYPE.Boolean);
        attrs.add(attr);
        attr = new RequestEntityAttribute(UserManagerConstants.AttributeName.EMPTYPE.getId(), "Contractor",
                                          RequestEntityAttribute.TYPE.String);
        attrs.add(attr);
        attr = new RequestEntityAttribute(UserManagerConstants.AttributeName.MIDDLENAME.getId(), user.getMiddleName(),
                                          RequestEntityAttribute.TYPE.String);
        attrs.add(attr);
        attr = new RequestEntityAttribute(UserManagerConstants.AttributeName.ACCOUNT_START_DATE.getId(),
                                          new Date((Long)user.getAttribute("Start Date")), RequestEntityAttribute.TYPE.Date);
        attrs.add(attr);
        attr = new RequestEntityAttribute(UserManagerConstants.AttributeName.ACCOUNT_END_DATE.getId(),
        		new Date((Long)user.getAttribute("End Date")), RequestEntityAttribute.TYPE.Date);
        attrs.add(attr);
        attr = new RequestEntityAttribute("Email", (String) user.getAttribute("Email"),
                                          RequestEntityAttribute.TYPE.String);
        attrs.add(attr);
        
        attr = new RequestEntityAttribute(UserManagerConstants.AttributeName.EMAIL.getId(), (String) user.getAttribute("Email"),
                RequestEntityAttribute.TYPE.String);
attrs.add(attr);
        
        attr = new RequestEntityAttribute("User Manager", Long.getLong(user.getManagerKey()), RequestEntityAttribute.TYPE.Long);
        attrs.add(attr);
        attr = new RequestEntityAttribute(UserManagerConstants.AttributeName.TITLE.getId(),
                                          (String) user.getAttribute("Title"), RequestEntityAttribute.TYPE.String);
        attrs.add(attr);
       
        attr = new RequestEntityAttribute(UserManagerConstants.AttributeName.DEPARTMENT_NUMBER.getId(),
                                          (String) user.getAttribute("Department Number"),
                                          RequestEntityAttribute.TYPE.String);
        attrs.add(attr);
        
        Long dob = Long.parseLong(user.getAttribute("DateOfBirth").toString());
        attr = new RequestEntityAttribute("DateOfBirth", new Date(dob),
                                          RequestEntityAttribute.TYPE.Date);
        attrs.add(attr);
        attr = new RequestEntityAttribute("Mobile", (String) user.getAttribute("Mobile"),
                                          RequestEntityAttribute.TYPE.String);
        attrs.add(attr);
        attr = new RequestEntityAttribute(UserManagerConstants.AttributeName.GENERATION_QUALIFIER.getId(),
                                          user.getGenerationQualifier(), RequestEntityAttribute.TYPE.String);
        attrs.add(attr);
        ent.setEntityData(attrs);

        List<RequestEntity> entities = new ArrayList<RequestEntity>();

        entities.add(ent);
        requestData.setTargetEntities(entities);

        try {
           reqID= reqsrvc.submitRequest(requestData);
            System.out.println("REQUEST SUBMITTED");
        } catch (InvalidRequestException e) {
            System.out.println("InvalidRequestException: " + e);
        } catch (InvalidRequestDataException e) {
            System.out.println("InvalidRequestDataException: " + e);
        } catch (RequestServiceException e) {
            System.out.println("RequestServiceException: " + e);
        } catch (BulkBeneficiariesAddException e) {
            System.out.println("BulkBeneficiariesAddException: " + e);
        } catch (BulkEntitiesAddException e) {
            System.out.println("BulkEntitiesAddException: " + e);
        }
        
        return reqID;
    }

    /**
     * Method description
     *
     *
     * @param orgName
     *
     * @return
     */
    private Long getOrgKeyforOrganization(String orgName) {
        OimClient           oimClient   = new OimClient(serviceProperties);
        OrganizationManager orgMgr      = oimClient.getOrgManager();
        Long                orgKey      = null;
        Set<String>         returnAttrs = new HashSet<String>();

        returnAttrs.add("act_key");
        returnAttrs.add(oracle.iam.identity.orgmgmt.api.OrganizationManagerConstants.AttributeName.ID_FIELD.getId());
        returnAttrs.add(oracle.iam.identity.orgmgmt.api.OrganizationManagerConstants.AttributeName.ORG_NAME.getId());

        Organization org = null;

        try {
            org    = orgMgr.getDetails(orgName, returnAttrs, true);
            orgKey = (Long) org.getAttribute(
                oracle.iam.identity.orgmgmt.api.OrganizationManagerConstants.AttributeName.ID_FIELD.getId());
        } catch (OrganizationManagerException e) {

            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (AccessDeniedException e) {

            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return orgKey;
    }
}
