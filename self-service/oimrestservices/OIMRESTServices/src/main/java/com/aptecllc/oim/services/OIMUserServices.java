package com.aptecllc.oim.services;

//~--- non-JDK imports --------------------------------------------------------
import java.util.Date;
import com.aptecllc.oim.exception.UserException;
import com.aptecllc.oim.exception.UserNotFoundException;
import com.aptecllc.oim.property.PropertyUtil;
import com.aptecllc.oim.property.ServiceProperty;
import com.aptecllc.oim.util.ServiceConstants;
import com.aptecllc.oim.util.v2.OimClient;
import com.aptecllc.oim.vo.Notification;
import java.net.URLEncoder;

import oracle.iam.identity.exception.NoSuchUserException;
import oracle.iam.identity.exception.OrganizationManagerException;
import oracle.iam.identity.exception.SearchKeyNotUniqueException;
import oracle.iam.identity.exception.UserAlreadyExistsException;
import oracle.iam.identity.exception.UserCreateException;
import oracle.iam.identity.exception.UserLookupException;
import oracle.iam.identity.exception.UserManagerException;
import oracle.iam.identity.exception.UserModifyException;
import oracle.iam.identity.exception.UserSearchException;
import oracle.iam.identity.exception.UserUnlockException;
import oracle.iam.identity.exception.ValidationFailedException;
import oracle.iam.identity.orgmgmt.api.OrganizationManager;
import oracle.iam.identity.orgmgmt.vo.Organization;
import oracle.iam.identity.usermgmt.api.UserManager;
import oracle.iam.identity.usermgmt.vo.User;
import oracle.iam.identity.usermgmt.vo.UserManagerResult;
import oracle.iam.passwordmgmt.api.ChallengeResponseService;
import oracle.iam.passwordmgmt.api.PasswordMgmtService;
import oracle.iam.passwordmgmt.exception.ChallengeResponseException;
import oracle.iam.passwordmgmt.vo.ValidationResult;
import oracle.iam.passwordmgmt.vo.rules.PasswordRuleDescription;
import oracle.iam.platform.authz.exception.AccessDeniedException;
import oracle.iam.platform.entitymgr.vo.SearchCriteria;
import oracle.iam.selfservice.exception.AuthSelfServiceException;
import oracle.iam.selfservice.exception.ChangePasswordException;
import oracle.iam.selfservice.exception.InvalidLookupException;
import oracle.iam.selfservice.exception.NumberOfChallengesMismatchException;
import oracle.iam.selfservice.exception.PasswordIncorrectException;
import oracle.iam.selfservice.exception.PasswordMismatchException;
import oracle.iam.selfservice.exception.PasswordPolicyException;
import oracle.iam.selfservice.exception.PasswordResetAttemptsExceededException;
import oracle.iam.selfservice.exception.QuestionsNotDefinedException;
import oracle.iam.selfservice.exception.SetChallengeValueException;
import oracle.iam.selfservice.exception.UserAccountDisabledException;
import oracle.iam.selfservice.exception.UserAccountInvalidException;
import oracle.iam.selfservice.exception.UserAlreadyLoggedInException;
import oracle.iam.selfservice.self.selfmgmt.api.AuthenticatedSelfService;
import oracle.iam.selfservice.uself.uselfmgmt.api.UnauthenticatedSelfService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

//~--- JDK imports ------------------------------------------------------------






import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.logging.Level;
import oracle.iam.identity.usermgmt.api.UserManagerConstants;
import oracle.iam.identity.usermgmt.api.UserManagerConstants.AttributeName;
import oracle.iam.passwordmgmt.vo.Challenge;
import oracle.iam.passwordmgmt.vo.PasswordPolicyDescription;
import oracle.iam.passwordmgmt.vo.PasswordPolicyInfo;
import oracle.iam.platform.OIMClient;


/**
 * Class description
 *
 *
 * @version        1.0, 13/11/06
 * @author         Eric A. Fisher, APTEC LLC
 */
@Service
public class OIMUserServices {
    private static final Logger logger = LoggerFactory.getLogger(OIMUserServices.class);
    @Autowired
    private ServiceProperty     serviceProperties;

    /**
     * Method description
     *
     *
     * @param fieldName
     * @param value
     * @param attributesToReturn
     *
     * @return
     *
     * @throws UserException
     */
    public User getUserByIdentifier(String fieldName, String value, Set attributesToReturn) throws UserException {
        User        user      = null;
        OimClient   oimClient = new OimClient(serviceProperties);
        UserManager usrMgr    = oimClient.getUsrMgr();

        logger.debug("Attributes to Return: " + attributesToReturn.toString());

        try {
            user = usrMgr.getDetails(fieldName, value, attributesToReturn);
        } catch (NoSuchUserException e) {
            logger.error(e.getMessage());

            throw new UserNotFoundException("Not Found");
        } catch (UserLookupException e) {
            logger.error(e.getMessage());

            throw new UserException("Lookup Error");
        } catch (SearchKeyNotUniqueException e) {
            logger.error(e.getMessage());

            throw new UserException("Duplicates Found");
        } catch (AccessDeniedException e) {
            logger.error(e.getMessage());

            throw new UserException("Access Denied");
        }

        return user;
    }

    /**
     * Method description
     *
     *
     * @param searchCriteria
     * @param attributesToReturn
     *
     * @return
     *
     * @throws UserException
     */
    public List searchUsers(SearchCriteria searchCriteria, Set attributesToReturn) throws UserException {
        List        foundUsers = null;
        OimClient   oimClient  = new OimClient(serviceProperties);
        UserManager usrMgr     = oimClient.getUsrMgr();

        try {
            foundUsers = usrMgr.search(searchCriteria, attributesToReturn, null);
        } catch (UserSearchException e) {
            e.printStackTrace();

            throw new UserException(e);
        } catch (AccessDeniedException e) {
            e.printStackTrace();

            throw new UserException(e);
        }

        return foundUsers;
    }

    /**
     * Method description
     *
     *
     * @param user
     * @param orgName
     *
     * @return
     *
     * @throws UserException
     */
    public String createUser(User user, String orgName) throws UserException {
        String      response  = null;
        OimClient   oimClient = new OimClient(serviceProperties);
        UserManager usrMgr    = oimClient.getUsrMgr();

        try {
            user.setAttribute("act_key", getOrgKeyforOrganization(orgName));
            logger.error(user.toString());

            UserManagerResult result = usrMgr.create(user);

            if (result.getStatus().equals("COMPLETED")) {
                response = result.getEntityId();
            }
        } catch (ValidationFailedException e) {

            // TODO Auto-generated catch block
            response = "INVALID";
            logger.error(e.getMessage());

            throw new UserException(e.getMessage());
        } catch (UserAlreadyExistsException e) {

            // TODO Auto-generated catch block
            response = "EXISTS";
            logger.error(e.getMessage());

            throw new UserException(e.getMessage());
        } catch (UserCreateException e) {

            // TODO Auto-generated catch block
            response = "ERROR";
            logger.error(e.getMessage());

            throw new UserException(e.getMessage());
        } catch (AccessDeniedException e) {

            // TODO Auto-generated catch block
            response = "ERROR";
            logger.error(e.getMessage());

            throw new UserException(e.getMessage());
        }

        return response;
    }

    /**
     * Method description
     *
     *
     * @param attributeMap
     *
     * @return
     *
     * @throws UserException
     */
    public String modifyUserProfile(HashMap attributeMap) throws UserException {
        OimClient                oimClient = new OimClient(serviceProperties);
        AuthenticatedSelfService selfOps   = oimClient.getAuthSelfSvc();

        try {
            selfOps.modifyProfileDetails(attributeMap);
        } catch (oracle.iam.selfservice.exception.ValidationFailedException e) {
            e.printStackTrace();

            throw new UserException(e.getMessage());
        } catch (AccessDeniedException e) {
            e.printStackTrace();

            throw new UserException(e.getMessage());
        } catch (AuthSelfServiceException e) {
            e.printStackTrace();

            throw new UserException(e.getMessage());
        }

        return "OK";
    }

    /**
     * Method description
     *
     *
     * @param user
     * @param keyField
     * @param keyValue
     *
     * @return
     *
     * @throws UserException
     */
    public String modifyUser(User user, String keyField, String keyValue) throws UserException {
        OimClient   oimClient  = new OimClient(serviceProperties);
        UserManager usrMgr     = oimClient.getUsrMgr();
        String      returnCode = null;

        try {
            UserManagerResult result = usrMgr.modify(keyField, keyValue, user);

            if (result.getStatus().equals("COMPLETED")) {
                returnCode = "OK";
            }
        } catch (ValidationFailedException e) {
        	logger.error("modifyUser error: {}", e);
        } catch (UserModifyException e) {
        	logger.error("modifyUser error: {}", e);
        } catch (NoSuchUserException e) {
        	logger.error("modifyUser error: {}", e);
        } catch (SearchKeyNotUniqueException e) {
        	logger.error("modifyUser error: {}", e);
        } catch (AccessDeniedException e) {
        	logger.error("modifyUser error: {}", e);
        }
        return returnCode;
    }

    /**
     * Method description
     *
     *
     * @param userLogin
     *
     * @return
     *
     * @throws UserException
     */
    public List getUserChallenges(String userLogin) throws UserException {
        ArrayList<String>          userChallenges = new ArrayList<String>();
        OimClient                  oimClient      = new OimClient(serviceProperties);
        UnauthenticatedSelfService selfOps        = oimClient.getUnAuthSelfSvc();

        try {
            String[] challenges = selfOps.getChallengeQuestions(userLogin);

            userChallenges.addAll(Arrays.asList(challenges));
        } catch (UserAccountDisabledException e) {

            // e.printStackTrace();
            throw new UserException("User Disabled");
        } catch (UserAccountInvalidException e) {
            e.printStackTrace();

            throw new UserNotFoundException("User Invalid");
        } catch (AuthSelfServiceException e) {
            e.printStackTrace();

            if (e.getMessage().equalsIgnoreCase("User challenge questions have not been set.")) {
                throw new UserException("Questions not Set.");
            }

            throw new UserException("Self Service Exception");
        }

        return userChallenges;
    }




    /**
     * Method description
     *
     *
     * @return
     *
     * @throws UserException
     */
    public List getUserChallenges() throws UserException {
        ArrayList<String>        userChallenges = new ArrayList<String>();
        OimClient                oimClient      = new OimClient(serviceProperties);
        AuthenticatedSelfService selfOps        = oimClient.getAuthSelfSvc();

        try {
            HashMap challenges = selfOps.getChallengeValuesForSelf();

            userChallenges.addAll(challenges.keySet());
        } catch (UserAccountDisabledException e) {
            e.printStackTrace();

            throw new UserException("User Disabled");
        } catch (UserAccountInvalidException e) {
            e.printStackTrace();

            throw new UserNotFoundException("User Invalid");
        } catch (AuthSelfServiceException e) {
            e.printStackTrace();

            if (e.getMessage().equalsIgnoreCase("User challenge questions have not been set.")) {
                throw new UserNotFoundException("Questions not Set");
            }

            throw new UserException("Self Service Exception");
        }

        return userChallenges;
    }


    public HashMap getUserChallengesAnswers() throws UserException {
        HashMap userChallenges = new HashMap();
        OimClient                oimClient      = new OimClient(serviceProperties);
        AuthenticatedSelfService selfOps        = oimClient.getAuthSelfSvc();

        try {
            HashMap challenges = selfOps.getChallengeValuesForSelf();

            userChallenges.putAll(challenges);
        } catch (UserAccountDisabledException e) {
            e.printStackTrace();

            throw new UserException("User Disabled");
        } catch (UserAccountInvalidException e) {
            e.printStackTrace();

            throw new UserNotFoundException("User Invalid");
        } catch (AuthSelfServiceException e) {
            e.printStackTrace();

            if (e.getMessage().equalsIgnoreCase("User challenge questions have not been set.")) {
                throw new UserNotFoundException("Questions not Set");
            }

            throw new UserException("Self Service Exception");
        }

        return userChallenges;
    }
    // User Authenticated Service

    /**
     * Method description
     *
     *
     * @param questionMap
     *
     * @throws UserException
     */
    public void setUserChallenges(HashMap questionMap) throws UserException {
        OimClient                oimClient = new OimClient(serviceProperties);
        AuthenticatedSelfService selfOps   = oimClient.getAuthSelfSvc();

        try {
            selfOps.setChallengeValues(questionMap);
        } catch (oracle.iam.selfservice.exception.ValidationFailedException e) {
            e.printStackTrace();

            throw new UserException(e.getMessage());
        } catch (SetChallengeValueException e) {
            e.printStackTrace();

            throw new UserException(e.getMessage());
        }
    }

    // User Authenticated Service

    /**
     * Method description
     *
     *
     * @param oldPassword
     * @param newPassword
     * @param confirmPassword
     *
     * @throws UserException
     */
    public void changePasswordAsUser(String oldPassword, String newPassword, String confirmPassword)
            throws UserException {
        OimClient                oimClient = new OimClient(serviceProperties);
        AuthenticatedSelfService selfOps   = oimClient.getAuthSelfSvc();

        try {
            selfOps.changePassword(oldPassword.toCharArray(), newPassword.toCharArray(), confirmPassword.toCharArray());
        } catch (oracle.iam.selfservice.exception.ValidationFailedException e) {
            e.printStackTrace();

            throw new UserException(e.getMessage());
        } catch (ChangePasswordException e) {
            e.printStackTrace();

            throw new UserException(e.getMessage());
        }
    }

    /**
     * Method description
     *
     *
     * @param userName
     * @param questions
     * @param newPassword
     *
     * @return
     *
     * @throws UserException
     */
    public boolean resetPasswordWithChallenges(String userName, HashMap questions, String newPassword)
            throws UserException {
        
        logger.trace("Entering resetPasswordWithChallenges with userName" + userName + " questions " + questions);

        OimClient                  oimClient = new OimClient(serviceProperties);
        UnauthenticatedSelfService selfOps   = oimClient.getUnAuthSelfSvc();
        boolean                    result    = false;
        HashMap modifyMap = new HashMap();
        Iterator entries = questions.entrySet().iterator();
        while (entries.hasNext()) {
            Map.Entry entry = (Map.Entry) entries.next();
            String key = (String)entry.getKey();
            String value = (String)entry.getValue();
            if(key!=null && value !=null && !key.isEmpty() && !value.isEmpty()){
                modifyMap.put(key, value);
            }
        }
        logger.trace("resetPasswordWithChallenges Updated questions Map" + modifyMap);

        try {
            result = selfOps.resetPassword(userName, modifyMap, newPassword.toCharArray());
        } catch (UserAccountDisabledException e) {
            logger.error("Error resetPasswordWithChallenges ", e);
            throw new UserException("User Disabled");
        } catch (UserAccountInvalidException e) {
            logger.error("Error resetPasswordWithChallenges ", e);
            throw new UserException("User Invalid");
        } catch (NumberOfChallengesMismatchException e) {
            logger.error("Error resetPasswordWithChallenges ", e);
            throw new UserException("Incorrect Answers");
        } catch (QuestionsNotDefinedException e) {
            logger.error("Error resetPasswordWithChallenges ", e);
            throw new UserException("No Questions Defined");
        } catch (PasswordIncorrectException e) {
            logger.error("Error resetPasswordWithChallenges ", e);
            throw new UserException("Bad Password");
        } catch (PasswordMismatchException e) {
            logger.error("Error resetPasswordWithChallenges ", e);
            throw new UserException("Password Mismatch");
        } catch (PasswordPolicyException e) {
            logger.error("Error resetPasswordWithChallenges ", e);
            throw new UserException("Password Policy Failed");
        } catch (PasswordResetAttemptsExceededException e) {
            logger.error("Error resetPasswordWithChallenges ", e);
            throw new UserException("Reset Limit Exceeded");
        } catch (UserAlreadyLoggedInException e) {
            logger.error("Error resetPasswordWithChallenges ", e);
            throw new UserException("Already Logged In");
        } catch (AuthSelfServiceException e) {
            logger.error("Error resetPasswordWithChallenges ", e);
            throw new UserException("Self Service Exception");
        }
        return result;
    }

    /**
     * Method description
     *
     *
     * @param userName
     * @param newPassword
     * @param forceChange
     *
     * @return
     *
     * @throws UserException
     */
    public boolean setUserPassword(String userName, String newPassword, boolean forceChange) throws UserException {
        OimClient   oimClient = new OimClient(serviceProperties);
        UserManager usrMgr    = oimClient.getUsrMgr();

        try {
            usrMgr.changePassword(userName, newPassword.toCharArray(), true, null, forceChange, false);
        } catch (NoSuchUserException e) {
            e.printStackTrace();

            throw new UserNotFoundException("User Invalid");
        } catch (UserManagerException e) {

            // TODO Auto-generated catch block
            e.printStackTrace();

            throw new UserException("Self Service Exception");
        } catch (AccessDeniedException e) {

            // TODO Auto-generated catch block
            e.printStackTrace();

            throw new UserException("Self Service Exception");
        }

        return true;
    }

    /**
     * Method description
     *
     *
     * @param userName
     *
     * @return
     *
     * @throws UserException
     */
    public String unlockUser(String userName) throws UserException {
        OimClient   oimClient = new OimClient(serviceProperties);
        UserManager usrMgr    = oimClient.getUsrMgr();
        String      response  = null;

        try {
            UserManagerResult result = usrMgr.unlock(userName, true);

            if (result.getStatus().equals("COMPLETED")) {
                response = "OK";
            }
        } catch (ValidationFailedException e) {
            response = "ERROR";
            logger.error(e.getMessage());

            throw new UserException(e.getMessage());
        } catch (UserUnlockException e) {
            response = "ERROR";
            logger.error(e.getMessage());

            throw new UserException(e.getMessage());
        } catch (NoSuchUserException e) {
            throw new UserNotFoundException("User Invalid");
        } catch (AccessDeniedException e) {
            response = "ERROR";
            logger.error(e.getMessage());

            throw new UserException(e.getMessage());
        }

        return response;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public List getSystemChallenges(String userLogin) {
        ArrayList<String>          sysChallenges = new ArrayList<String>();
        OimClient                  oimClient     = new OimClient(serviceProperties);

        PasswordMgmtService pwdSvc = oimClient.getService(PasswordMgmtService.class);
	Map challenges =pwdSvc.getApplicableDefaultChallengeQuestions(userLogin, true,Locale.getDefault());
	sysChallenges.addAll(challenges.keySet());
        return sysChallenges;
    }

    /**
     * Method description
     *
     *
     * @param orgName
     *
     * @return
     */
    public Long getOrgKeyforOrganization(String orgName) {
        OimClient           oimClient   = new OimClient(serviceProperties);
        OrganizationManager orgMgr      = oimClient.getOrgManager();
        Long                orgKey      = null;
        Set<String>         returnAttrs = new HashSet<String>();

        returnAttrs.add("act_key");
        returnAttrs.add(oracle.iam.identity.orgmgmt.api.OrganizationManagerConstants.AttributeName.ID_FIELD.getId());
        returnAttrs.add(oracle.iam.identity.orgmgmt.api.OrganizationManagerConstants.AttributeName.ORG_NAME.getId());

        Organization org = null;

        try {
            org    = orgMgr.getDetails(orgName, returnAttrs, true);
            orgKey = (Long) org.getAttribute(
                oracle.iam.identity.orgmgmt.api.OrganizationManagerConstants.AttributeName.ID_FIELD.getId());
        } catch (OrganizationManagerException e) {

            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (AccessDeniedException e) {

            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return orgKey;
    }

    /**
     * Method description
     *
     *
     * @param userName
     * @param password
     *
     * @return
     */
    public String validatePassword(String userName, char[] password) {
        OimClient           oimClient = new OimClient(serviceProperties);
        PasswordMgmtService pwdSvc    = oimClient.getPwdMgr();
        ValidationResult    results   = pwdSvc.validatePasswordAgainstPolicy(password, userName, Locale.US);

        if (results.isPasswordValid()) {
            return "Valid";
        } else {
            StringBuffer                  failedRules   = new StringBuffer();
            List<PasswordRuleDescription> rulesViolated =
                results.getPolicyViolationsDescription().getPasswordRulesDescription();

            for (PasswordRuleDescription rule : rulesViolated) {
                failedRules.append(rule.getDisplayValue()).append("|");
            }

            return failedRules.toString();
        }
    }
        private static Random              rand   = new Random();

    private static String getRandomCode() {
        String randomCode = "";
        int    randomNum  = rand.nextInt((9999999 - 1000000) + 1) + 1000000;

        randomCode = Integer.toString(randomNum);

        return randomCode;
    }

    // Token in database will  have the Token_key+Purpose
    public String sendEMailForPasswordChange(String userName, String purpose) {
        logger.debug("Entering with userName " + userName + " purpose" + purpose);
        OimClient oimClient = new OimClient(serviceProperties);
        UserManager usrMgr = oimClient.getUsrMgr();
        String passwordTken = PropertyUtil.getProperty("service.useraccount.attribute.custom." + "PasswordTken");
        String passwordTkenGenTime = PropertyUtil.getProperty("service.useraccount.attribute.custom." + "PassTokenGenTme");
        String personalEMailAtt = PropertyUtil.getProperty("service.useraccount.attribute.emailAddress");
        String userLogin = PropertyUtil.getProperty(ServiceConstants.USER_NAME.getName());
        String returnCode = "Valid";
        
        String receipientAddress = null;
        String passToken = getRandomCode();
        Date passTokenGen = new Date();

        Set<String> attributesToReturn = new HashSet<String>();
        attributesToReturn.add(personalEMailAtt);

        User user = null;
        try {
            user = usrMgr.getDetails(userLogin, userName, attributesToReturn);
        } catch (Exception ex) {
            logger.error("Error searching user ", ex);
        }
        if (user != null) {
            receipientAddress = (String)user.getAttribute(personalEMailAtt);
            HashMap<String, Object> atrrMap = new HashMap<String, Object>();
            atrrMap.put(passwordTken, passToken+purpose);
            atrrMap.put(passwordTkenGenTime, passTokenGen);
            logger.debug("Modifying user" + user.getEntityId() + " with atrrMap " + atrrMap);
            User user1 = new User(user.getEntityId(), atrrMap);
            try {
                usrMgr.modify(user1);
            } catch (Exception ex) {
                logger.error("Error updating user ", ex);
            }
        }
        Map<String, String> inputs = new HashMap<String, String>();
        inputs.put("userLogin", userName);
        String templateName = null;
        String encodedUserLogin = userName;
        try {
            encodedUserLogin = URLEncoder.encode(userName, "UTF-8");
        } catch (Exception e){
            logger.error("Error updating user ", e);
        }
        if(purpose.startsWith("EMAIL")){
            receipientAddress = purpose.substring(5);
            templateName = "CUNY New Email Change Account Management Email Address";
            inputs.put("emailChangeLink", PropertyUtil.getProperty("selfservices.url") + "/emailActivate?loginID=" + encodedUserLogin + "&tokenKey=" + passToken);
        } else if(purpose.equals("ACTIVATION")){
            templateName = "CUNY Account Activation Email";
            inputs.put("activationContinueLink", PropertyUtil.getProperty("selfservices.url") + "/acctActivat?loginID=" + encodedUserLogin + "&tokenKey=" + passToken);
        } else {
            templateName = "CUNY Password Reset Link";
            inputs.put("passwordLink", PropertyUtil.getProperty("selfservices.url") + "/resetPassword?loginID=" + encodedUserLogin + "&tokenKey=" + passToken);
        }
        if(receipientAddress!=null)
            com.aptecllc.oim.util.EmailUtility.sendEmailMessage(oimClient, templateName, inputs, receipientAddress);
        return returnCode;
    }
    
    public String changePasswordWithToken(String userName, String newPassword) {
        logger.debug("Entering with userName " + userName);

        OimClient oimClient = new OimClient(serviceProperties);
        PasswordMgmtService pwdSvc = oimClient.getPwdMgr();
        ValidationResult results = pwdSvc.validatePasswordAgainstPolicy(newPassword.toCharArray(), userName, Locale.US);
        if (!results.isPasswordValid()) {
            StringBuffer failedRules = new StringBuffer();
            List<PasswordRuleDescription> rulesViolated
                    = results.getPolicyViolationsDescription().getPasswordRulesDescription();

            for (PasswordRuleDescription rule : rulesViolated) {
                failedRules.append(rule.getDisplayValue()).append("|");
            }
            logger.debug("Password invalid, returning: " + failedRules);

            return failedRules.toString();
        }
        UserManager usrMgr = oimClient.getUsrMgr();
        String passwordTken = PropertyUtil.getProperty("service.useraccount.attribute.custom." + "PasswordTken");
        String passwordTkenGenTime = PropertyUtil.getProperty("service.useraccount.attribute.custom." + "PassTokenGenTme");

        User user = null;
        try {
            user = usrMgr.getDetails(PropertyUtil.getProperty(ServiceConstants.USER_NAME.getName()), userName, null);
        } catch (Exception e) {
            logger.error("Error", e);
            return "UserNOTFOUND";
        }
        if (user != null) {
            try {
                usrMgr.changePassword(userName, newPassword.toCharArray(), true, null, false, false);
                logger.debug("Password changed");

                HashMap<String, Object> atrrMap = new HashMap<String, Object>();
                atrrMap.put(passwordTken, null);
                atrrMap.put(passwordTkenGenTime, null);

                User user1 = new User(user.getEntityId(), atrrMap);
                usrMgr.modify(user1);
                return "Valid";
            } catch (Exception e) {
                logger.error("Error changing Password ", e);
            }
        } else {
            return "UserNOTFOUND";
        }
        return "FAILURE";
    }

    public String sendEmailToUser(String userName, String templateName) {
        logger.debug("Entering with userName " + userName + ", templateName: " +templateName);
        OimClient oimClient = new OimClient(serviceProperties);
        UserManager usrMgr = oimClient.getUsrMgr();
        String personalEMailAtt = PropertyUtil.getProperty("service.useraccount.attribute.emailAddress");
        String userLogin = PropertyUtil.getProperty(ServiceConstants.USER_NAME.getName());
        
        String receipientAddress = null;

        Set<String> attributesToReturn = new HashSet<String>();
        attributesToReturn.add(personalEMailAtt);

        User user = null;
        try {
            user = usrMgr.getDetails(userLogin, userName, attributesToReturn);
        } catch (Exception ex) {
            logger.error("Error searching user ", ex);
        }
        if (user != null) {
            receipientAddress = (String)user.getAttribute(personalEMailAtt);
        }
        
        
        Map<String, String> inputs = new HashMap<String, String>();
        inputs.put("userLogin", userName);
        inputs.put("email", receipientAddress);

        //com.aptecllc.oim.util.EmailUtility.sendEmailMessage(oimClient, templateName, inputs);
        if(receipientAddress!=null)
            com.aptecllc.oim.util.EmailUtility.sendEmailMessage(oimClient, templateName, inputs, receipientAddress);

        return "Valid";
    }
    
    public String validateToken(String userName, String passwordToken) {
        logger.debug("Entering with userName " + userName + " passwordToken: " + passwordToken);

        OimClient oimClient = new OimClient(serviceProperties);
        UserManager usrMgr = oimClient.getUsrMgr();
        String passwordTken = PropertyUtil.getProperty("service.useraccount.attribute.custom." + "PasswordTken");
        String passwordTkenGenTime = PropertyUtil.getProperty("service.useraccount.attribute.custom." + "PassTokenGenTme");

        Set<String> attributesToReturn = new HashSet<String>();
        attributesToReturn.add(passwordTken);
        attributesToReturn.add(passwordTkenGenTime);

        logger.debug("Attributes to Return: " + attributesToReturn.toString());
        User user = null;
        try {
            user = usrMgr.getDetails(PropertyUtil.getProperty(ServiceConstants.USER_NAME.getName()), userName, attributesToReturn);
        } catch (Exception e) {
            logger.error("Error", e);
            return "TOKENExpired";
        }
        if (user != null) {
            String passwordTokenInUser = (String) user.getAttribute(passwordTken);
            Date tokenTime = (Date) user.getAttribute(passwordTkenGenTime);
            String validateToken = PropertyUtil.getProperty("service.useraccount.passwordtoken.validateToken"); // default or null is TRUE
            if(validateToken!=null && validateToken.equalsIgnoreCase("FALSE")) {
                return "Valid";
            } else if (passwordTokenInUser != null && passwordToken != null && passwordTokenInUser.equals(passwordToken)) {
                if (tokenTime != null) {
                    long difference = (System.currentTimeMillis() - tokenTime.getTime()) / 60000;
                    logger.debug("difference: " + difference);
                    String timeToLive = PropertyUtil.getProperty("service.useraccount.passwordtoken.timetolive");
                    int timeToLiveInt = Integer.parseInt(timeToLive);
                    if (difference <= timeToLiveInt) {   // TIMETOLIVE JITENDRA
                        return "Valid";
                    }
                }
            }
        }
        return "TOKENExpired";
    }

        /**
     * Method description
     *
     *
     * @param userLogin
     *
     * @return
     *
     * @throws UserException
     */
    public String getPasswordPolicy(String userLogin) throws UserException {
        OimClient                  oimClient      = new OimClient(serviceProperties);
        PasswordMgmtService pwdSvc    = oimClient.getPwdMgr();
        UserManager userManager = oimClient.getUsrMgr();
        StringBuffer passwordPolicyRules = new StringBuffer("<table>");
        
        try {
            User u = userManager.getDetails(userLogin, null, true);

            PasswordPolicyDescription ppDesc = pwdSvc.getApplicablePasswordPolicyDescription(u);
            logger.debug("password policy description returned by OIM is "+ppDesc.toString());
            
                for (PasswordRuleDescription passwordRuleDescription : ppDesc.getPasswordRulesDescription()) {
                    passwordPolicyRules.append("<TR><TD><li><p>");
                    passwordPolicyRules.append(passwordRuleDescription.getDisplayValue());
                    passwordPolicyRules.append("</p></li></TD></TR>");

                }
                    passwordPolicyRules.append("</table>");

            return passwordPolicyRules.toString();
        } catch (Exception e) {
            logger.error("Error", e);
            return null;
        }
    }



        public String validatEmailChangeToken(String userName, String passwordToken) {
        logger.debug("Entering with userName " + userName + " passwordToken: " + passwordToken);

        OimClient oimClient = new OimClient(serviceProperties);
        UserManager usrMgr = oimClient.getUsrMgr();
        String passwordTken = PropertyUtil.getProperty("service.useraccount.attribute.custom." + "PasswordTken");
        String passwordTkenGenTime = PropertyUtil.getProperty("service.useraccount.attribute.custom." + "PassTokenGenTme");
        String emailField = PropertyUtil.getProperty("service.useraccount.attribute.emailAddress");

        Set<String> attributesToReturn = new HashSet<String>();
        attributesToReturn.add(passwordTken);
        attributesToReturn.add(passwordTkenGenTime);

        logger.debug("Attributes to Return: " + attributesToReturn.toString());
        User user = null;
        try {
            user = usrMgr.getDetails(PropertyUtil.getProperty(ServiceConstants.USER_NAME.getName()), userName, attributesToReturn);
        } catch (Exception e) {
            logger.error("Error", e);
            return "TOKENExpired";
        }
        if (user != null) {
            String passwordTokenInUser = (String) user.getAttribute(passwordTken);
            Date tokenTime = (Date) user.getAttribute(passwordTkenGenTime);
            if (passwordTokenInUser != null && passwordToken != null) {
                if (tokenTime != null && passwordTokenInUser.startsWith(passwordToken)) {
                    long difference = (System.currentTimeMillis() - tokenTime.getTime()) / 60000;
                    logger.debug("difference: " + difference);
                    String timeToLive = PropertyUtil.getProperty("service.useraccount.passwordtoken.timetolive");
                    int timeToLiveInt = Integer.parseInt(timeToLive);
                    if (difference <= timeToLiveInt) {   // TIMETOLIVE JITENDRA
                        String mailValue = passwordTokenInUser.substring(passwordToken.length());
                        
                        HashMap<String, Object> atrrMap = new HashMap<String, Object>();
                        atrrMap.put(passwordTken, null);
                        atrrMap.put(passwordTkenGenTime, null);
                        atrrMap.put(emailField, mailValue);
                        try {
                            User user1 = new User(user.getEntityId(), atrrMap);
                            usrMgr.modify(user1);
                        } catch(Exception e){
                            logger.error("Error", e);
                            return "InValidEmail";
                        }
                        return "Valid";
                    }
                }
            }
        }
        return "TOKENExpired";
    }

        
    public String checkUserAuthentication(String userName, String userPassword) {
        logger.debug("Entering with userName " + userName);
        String providerURL = serviceProperties.getOimProviderUrl();
/*        String orclIDMLoc = System.getenv("INSTALL_LOC");
        System.setProperty("XL.HomeDir", orclIDMLoc + "/server");
        System.setProperty("java.security.policy",
                           orclIDMLoc + "/designconsole/config/xl.policy");
        System.setProperty("java.security.auth.login.config",
                           orclIDMLoc + "/designconsole/config/authwl.conf");
        System.setProperty("XL.ExtendedErrorOptions", "TRUE");
        System.setProperty("APPSERVER_TYPE", "wls");
*/
        OIMClient client;
        Hashtable env = new Hashtable();
        env.put(OIMClient.JAVA_NAMING_FACTORY_INITIAL,
                OIMClient.WLS_CONTEXT_FACTORY);
        env.put(OIMClient.JAVA_NAMING_PROVIDER_URL, providerURL);
        client = new OIMClient(env);
        try {
            client.login(userName, userPassword.toCharArray());
        } catch (Exception e) {
            logger.error("Error", e);
            return "UNABLETOAUTHENTICATE";
        }
        return "AUTHENTICATED";
    }

    
}
