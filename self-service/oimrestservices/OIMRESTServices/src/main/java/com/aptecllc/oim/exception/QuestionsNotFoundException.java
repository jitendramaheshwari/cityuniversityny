package com.aptecllc.oim.exception;

public class QuestionsNotFoundException extends UserException{
	
	public QuestionsNotFoundException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public QuestionsNotFoundException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public QuestionsNotFoundException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public QuestionsNotFoundException() {
		// TODO Auto-generated constructor stub
	}

}
