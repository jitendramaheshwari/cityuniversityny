package com.aptecllc.wls.providers;

public class TokenAPIAsserterTokenTypes {
	public static final String USERNAMETOKEN="SimpleUsernameToken";
	public static final String APTECAPITOKEN="APIToken";
	public static final String REMOTE_USER="REMOTE_USER";
}
