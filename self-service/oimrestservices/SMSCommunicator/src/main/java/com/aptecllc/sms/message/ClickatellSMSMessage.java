package com.aptecllc.sms.message;

//~--- non-JDK imports --------------------------------------------------------

import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

/**
 * Class description
 *
 *
 * @version        1.0, 14/04/22
 * @author         Eric A. Fisher, APTEC LLC
 */
public class ClickatellSMSMessage extends AbstractSMSMessage {
    private String api_id;
    private String username;
    private String password;
    private String priority = "3";
    private String callback;
    private String messageID;

    /**
     * Method description
     *
     *
     * @return
     */
    public String getApi_id() {
        return api_id;
    }

    /**
     * Method description
     *
     *
     * @param api_id
     */
    public void setApi_id(String api_id) {
        this.api_id = api_id;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getUsername() {
        return username;
    }

    /**
     * Method description
     *
     *
     * @param username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getPassword() {
        return password;
    }

    /**
     * Method description
     *
     *
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getCallback() {
        return callback;
    }

    /**
     * Method description
     *
     *
     * @param callback
     */
    public void setCallback(String callback) {
        this.callback = callback;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getPriority() {
        return priority;
    }

    /**
     * Method description
     *
     *
     * @param priority
     */
    public void setPriority(String priority) {
        this.priority = priority;
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public String getMessageID() {
        return messageID;
    }

    /**
     * Method description
     *
     *
     * @param messageID
     */
    public void setMessageID(String messageID) {
        this.messageID = messageID;
    }

    @Override
    public Object getTransmittalObject() {
        MultiValueMap<String, String> form = new LinkedMultiValueMap<String, String>();

        form.add("api_id", this.getApi_id());
        form.add("user", this.getUsername());
        form.add("password",this.getPassword());
        form.add("to", this.getRecipientID());
        form.add("from", this.getSenderID());
        form.add("text", this.getMessageBody());
        form.add("queue", this.getPriority());
        form.add("mo","1");

        return form;
    }
}
