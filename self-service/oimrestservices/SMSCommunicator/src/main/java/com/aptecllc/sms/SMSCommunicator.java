package com.aptecllc.sms;

//~--- non-JDK imports --------------------------------------------------------

import com.aptecllc.sms.gateway.ISMSGateway;
import com.aptecllc.sms.message.AbstractSMSMessage;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * Class description
 *
 *
 * @version        1.0, 14/04/22
 * @author         Eric A. Fisher, APTEC LLC    
 */
public class SMSCommunicator {
    @Autowired
    private ISMSGateway smsGatewayClient;

    /**
     * Method description
     *
     *
     * @param recipientID
     * @param messageText
     *
     * @return
     */
    public String sendSMSMessage(String recipientID, String messageText) {
        return smsGatewayClient.sendMessage(buildMessage(recipientID, messageText));
    }

    /**
	 * @return the smsGatewayClient
	 */
	public ISMSGateway getSmsGatewayClient() {
		return smsGatewayClient;
	}

	/**
	 * @param smsGatewayClient the smsGatewayClient to set
	 */
	public void setSmsGatewayClient(ISMSGateway smsGatewayClient) {
		this.smsGatewayClient = smsGatewayClient;
	}

	private AbstractSMSMessage buildMessage(String recipientID, String messageText) {
        AbstractSMSMessage message = smsGatewayClient.getNewMessage();

        message.setMessageBody(messageText);
        message.setRecipientID(recipientID);
        
      
        return message;
    }
}
