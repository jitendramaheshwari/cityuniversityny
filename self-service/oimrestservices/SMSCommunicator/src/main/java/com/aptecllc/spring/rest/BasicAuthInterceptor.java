package com.aptecllc.spring.rest;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.commons.codec.binary.Base64;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

//~--- JDK imports ------------------------------------------------------------


import java.io.IOException;
import java.nio.charset.Charset;
import java.util.logging.Logger;

/**
 * Class description
 *
 *
 * @version        1.0, 14/06/17
 * @author         Eric A. Fisher, APTEC LLC    
 */
public class BasicAuthInterceptor implements ClientHttpRequestInterceptor {
    private static final Logger logger = Logger.getLogger(BasicAuthInterceptor.class.getName());
    private final String        username;
    private final String        password;

    /**
     * Constructs ...
     *
     *
     * @param username
     * @param password
     */
    public BasicAuthInterceptor(String username, String password) {
        this.username = username;
        this.password = password;
    }

    /**
     * Method description
     *
     *
     * @param request
     * @param body
     * @param execution
     *
     * @return
     *
     * @throws IOException
     */
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution)
            throws IOException {

        // Build the auth-header
        final String auth        = username + ":" + password;
        final byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("US-ASCII")));
        final String authHeader  = "Basic " + new String(encodedAuth);

        // Add the auth-header
        request.getHeaders().add("Authorization", authHeader);

        	
        	
        return execution.execute(request, body);
    }

    private String mask(String toMask) {
        return toMask.replaceAll(".", "*");
    }
}
